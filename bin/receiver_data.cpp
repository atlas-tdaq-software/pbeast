#include <sstream>

#include <pbeast/output-data-file.h>
#include <pbeast/exceptions.h>

#include "receiver_attribute.h"
#include "receiver_class.h"
#include "receiver_data.h"
#include "receiver_data_provider.h"
#include "receiver_errors.h"

std::atomic<uint_least64_t> pbeast::receiver::BaseData::s_max_data_bucket_size(0);
std::atomic<uint_least64_t> pbeast::receiver::BaseData::s_id_count(1);

bool
pbeast::receiver::BaseData::commit(const std::filesystem::path& dir, const std::string& partition, const std::string& partition_dir_name, Class& c, Attribute& a)
{
  uint32_t secs_min(pbeast::ts2secs(m_min_ts));

  std::filesystem::path file_path;

  try
    {
      file_path = pbeast::OutputDataFile::make_file_name(dir, partition_dir_name, c.get_dir_path(), a.get_dir_name(), secs_min, pbeast::ts2secs(m_max_ts), ".pb", true);
    }
  catch (daq::pbeast::Exception &ex)
    {
      ers::error(ex);
      return false;
    }

  std::filesystem::path tmp_file_path(file_path);
  tmp_file_path.replace_extension(".tmp");

  if (get_num_of_series() > 0)
    {
      try
        {
          ERS_DEBUG(1, "Saving " << get_num_of_series() << " series of " << a.get_name() << '@' << c.get_name() << " to " << tmp_file_path);

          pbeast::OutputDataFile file(tmp_file_path.native(), partition, c.get_name(), c.get_dir_path(), a.get_name(), pbeast::mk_ts(secs_min, 0), get_type(), a.get_is_array(), get_num_of_series(), false, false, false, 0);

          file.write_header();

          write(file);

          file.write_footer();

          int64_t bytes = file.close();

          std::filesystem::rename(tmp_file_path, file_path);

          ERS_LOG("Wrote " << get_number_of_items() << ' ' << pbeast::as_string(get_type()) << (a.get_is_array() ? "[]" : "") << " items in "
              << get_num_of_series() << " series of " << a.get_name() << '@' << c.get_name() << " to " << file_path << " (" << bytes << " bytes)");

          DataProvider::get_monitoring_object().save_file(bytes);
        }
      catch (std::filesystem::filesystem_error& ex)
        {
          ers::error(daq::pbeast::FileSystemOperationError(ERS_HERE, "rename file", tmp_file_path, file_path, ex.code().message()));
          return false;
        }
      catch (std::exception& ex)
        {
          ers::error(daq::pbeast::CannotCommitDataFile(ERS_HERE, file_path, ex));
          return false;
        }
    }
  else
    {
      ERS_LOG("Skip creation of empty data file " << file_path);
    }

  return true;
}
