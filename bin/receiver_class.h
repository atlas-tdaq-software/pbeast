#ifndef PBEAST_RECEIVER_CLASS_H
#define PBEAST_RECEIVER_CLASS_H

#include <stdint.h>
#include <algorithm>
#include <ostream>
#include <filesystem>

#include "receiver_attribute.h"

namespace pbeast
{
  namespace receiver
  {

    class Class
    {

      friend class Attribute;
      friend class DataProvider;

    public:

      Class(const Class& c, Attribute * parent);

      virtual
      ~Class();

      Class() = delete;

      Class(const Class&) = delete;

      Class&
      operator=(const Class&) = delete;

      virtual Class *
      make_new(Attribute * a) = 0;

      inline bool
      operator==(const Class& c) const
      {
        return (
          (m_name == c.m_name) &&
          (m_attributes.size() == c.m_attributes.size()) &&
          std::equal(m_attributes.begin(), m_attributes.end(), c.m_attributes.begin(), [](const Attribute * a, const Attribute * b) { return *a == *b; })
        );
      }

      const std::string&
      get_name() const
      {
        return m_name;
      }

      const std::filesystem::path&
      get_dir_path() const
      {
        return m_dir_path;
      }

      unsigned int
      get_number_of_attributes() const
      {
        return m_attributes.size();
      }

      const std::vector<Attribute*>&
      get_attributes() const
      {
        return m_attributes;
      }

      Attribute&
      get_attribute(unsigned int idx)
      {
        return *m_attributes[idx];
      }

      Attribute *
      get_attribute(const std::string& name);

      bool
      is_incomplete_schema() const
      {
        return m_incomplete_schema;
      }

      void
      set_incomplete_schema()
      {
        m_incomplete_schema = true;
      }

      bool
      is_nested() const
      {
        return (m_parent != 0);
      }

      std::ostream&
      print(std::ostream& s, const std::string& prefix) const;

      const std::map<std::string, std::set<uint64_t>*> *
      get_eov() const
      {
        return &m_eov;
      }

      uint64_t
      rebuid_eov(uint64_t low_limit);


    protected:

      Class(const std::string& name);

      void
      add_eov(const std::string& name, uint64_t ts)
      {
        static_cast<DataHolder<Void,true> *>(m_end_of_validity_attribute->get_raw_data())->add_data(name, ts, Void());
      }

      void
      add_updated(const std::string& name, uint64_t ts)
      {
        static_cast<DataHolder<Void> *>(m_updated_info_attribute->get_raw_data())->add_data(name, ts, Void());
      }

      std::string m_name;
      std::filesystem::path m_dir_path;
      std::vector<Attribute*> m_attributes;
      Attribute * m_end_of_validity_attribute;
      Attribute * m_updated_info_attribute;
      bool m_directory_created;
      std::map<std::string, std::set<uint64_t>*> m_eov;
      bool m_incomplete_schema;
      Attribute * m_parent;

    };

    std::ostream& operator<< ( std::ostream& , const pbeast::receiver::Class & );

  }
}

#endif
