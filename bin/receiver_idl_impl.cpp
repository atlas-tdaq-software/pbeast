#include <pbeast/downsample-data.h>

#include "receiver_data_provider.h"
#include "receiver_partition.h"
#include "utils.h"


static bool
does_match(boost::regex * re, const std::string& str)
{
  try
    {
      return (re == nullptr || boost::regex_match(str, *re));
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << pbeast::utils::s_was_caused_separator << "boost::regex_match() failed for string \'" << str << "\': " << ex.what() << std::endl;
      throw pbeast::Failure(text.str().c_str());
    }
}


static boost::regex *
make_regexp(const char * mask)
{
  if (pbeast::utils::is_regexp(mask))
    {
      try
        {
          return new boost::regex(mask);
        }
      catch (std::exception& ex)
        {
          std::ostringstream text;
          text << pbeast::utils::s_was_caused_separator << "error parsing regular expression \'" << mask << "\': " << ex.what();
          throw pbeast::BadRequest(text.str().c_str());
        }
    }

  return nullptr;
}

pbeast::receiver::Partition *
pbeast::receiver::DataProvider::check_partition(const char* partition_name)
{
  if (pbeast::receiver::Partition * p = get_partition(partition_name))
    {
      return p;
    }
  else
    {
      std::ostringstream text;
      text << "there is no partition \"" << partition_name << '\"';
      throw pbeast::NotFound(text.str().c_str());
    }
}

pbeast::receiver::Class *
pbeast::receiver::DataProvider::check_and_get_class(const char* partition_name, const char* class_name)
{
  if (pbeast::receiver::Class * c = get_class(class_name, check_partition(partition_name)))
    {
      return c;
    }
  else
    {
      std::ostringstream text;
      text << "there is no class \"" << class_name << '@' << partition_name << '\"';
      throw pbeast::NotFound(text.str().c_str());
    }
}

static pbeast::receiver::Attribute *
check_nested_attribute(pbeast::receiver::Class * c, const char* path)
{
  const char * an_ptr = strchr(path, '/');

  if(an_ptr != nullptr)
   {
     const char * cn_ptr = strchr(an_ptr+1, '/');

     if(cn_ptr != nullptr)
       {
         std::string an(path, an_ptr - path);

         if( pbeast::receiver::Attribute * a = c->get_attribute(an))
           {
             if(a->get_type_class())
               {
                 std::string cn(an_ptr+1, cn_ptr - an_ptr - 1);

                 if(cn == a->get_type_class()->get_name())
                   {
                      return check_nested_attribute(a->get_type_class(), cn_ptr+1);
                   }
               }
           }
       }
   }
  else
    {
      return c->get_attribute(path);
    }

  return nullptr;
}

pbeast::receiver::Attribute*
pbeast::receiver::DataProvider::check_and_get_attribute(const char *partition_name, const char *class_name, const char *attribute_name)
{
  pbeast::receiver::Class *c = check_and_get_class(partition_name, class_name);

  if (pbeast::receiver::Attribute *a = c->get_attribute(attribute_name))
    {
      return a;
    }
  else
    {
      if (pbeast::receiver::Attribute *a = check_nested_attribute(c, attribute_name))
        {
          return a;
        }

      std::ostringstream text;
      text << "there is no attribute \"" << attribute_name << '@' << class_name << '@' << partition_name << '\"';
      throw pbeast::NotFound(text.str().c_str());
    }
}

void
pbeast::receiver::DataProvider::check_attribute(const pbeast::receiver::Attribute * a, const char* partition_name, const char* class_name, const char* attribute_name, pbeast::DataType type, bool is_array)
{
  if (pbeast::are_compartible(a->m_type,type) == false)
    {
      std::ostringstream text;
      text << pbeast::utils::s_was_caused_separator << "attribute \"" << attribute_name << '@' << class_name << '@' << partition_name << "\" is not " << pbeast::as_string(type) << " type";
      throw pbeast::BadRequest(text.str().c_str());
    }

  if (a->m_is_array != is_array)
    {
      std::ostringstream text;
      text << pbeast::utils::s_was_caused_separator << "attribute \"" << attribute_name << '@' << class_name << '@' << partition_name << "\" is " << (is_array ? "multi" : "single") << "-value";
      throw pbeast::BadRequest(text.str().c_str());
    }
}

static void
validate_is_empty(const char* select_functions)
{
  if (strlen(select_functions) != 0)
    throw pbeast::BadRequest("functions are not supported by the receiver application");
}

static void
validate_ds_interval(uint32_t interval)
{
  if (interval == 0)
    throw pbeast::BadRequest("downsampling interval cannot be 0");
}


#define ASSIGN_PRIMITIVE_OBJ_TYPE    (*data)[count].data[idx].data = x.get_value();

#define ASSIGN_STRING_OBJ_TYPE       (*data)[count].data[idx].data = CORBA::string_dup(x.get_value().c_str());

#define ASSIGN_PRIMITIVE_OBJ_VECTOR  int idx2(0); (*data)[count].data[idx].data.length(x.get_value().size()); for(const auto & y : x.get_value()) { ((*data)[count].data[idx].data)[idx2++] = y; }

#define ASSIGN_STRING_OBJ_VECTOR     int idx2(0); (*data)[count].data[idx].data.length(x.get_value().size()); for(const auto & y : x.get_value()) { ((*data)[count].data[idx].data)[idx2++] = CORBA::string_dup(y.c_str()); }



#define GET_ALL( LIST_TYPE, PBEAST_TYPE, IS_ARRAY, CPP_TYPE, SERIE, RECEIVER_DATA, ASSIGN_CODE )                                                \
{                                                                                                                                               \
  validate_is_empty(select_functions);                                                                                                          \
  get_eovs(partition_name, class_name, since, until, prev_interval, next_interval, object_mask, is_regexp, eov);                                \
  get_upds(fill_upd, partition_name, class_name, since, until, prev_interval, next_interval, object_mask, is_regexp, upd);                      \
                                                                                                                                                \
  data = new pbeast::LIST_TYPE();                                                                                                               \
                                                                                                                                                \
  if ( (until + next_interval) < m_started_at ) return;                                                                                         \
                                                                                                                                                \
  pbeast::receiver::Attribute * a = check_and_get_attribute(partition_name, class_name, attribute_name);                                        \
                                                                                                                                                \
  std::lock_guard<std::mutex> data_lock(a->m_compact_data_mutex);                                                                               \
                                                                                                                                                \
  if (a->m_compact_data)                                                                                                                        \
    {                                                                                                                                           \
      /* check attribute type only if there are received data */                                                                                \
      check_attribute(a, partition_name, class_name, attribute_name, PBEAST_TYPE, IS_ARRAY);                                                    \
                                                                                                                                                \
      pbeast::receiver::RECEIVER_DATA<CPP_TYPE>& file_data = static_cast<pbeast::receiver::RECEIVER_DATA<CPP_TYPE>&>(*a->m_compact_data);       \
      unsigned int count(0);                                                                                                                    \
                                                                                                                                                \
      const uint64_t w_since = pbeast::mk_wide_since(since, prev_interval);                                                                     \
      const uint64_t w_until = pbeast::mk_wide_until(until, next_interval);                                                                     \
                                                                                                                                                \
      if (is_regexp)                                                                                                                            \
        {                                                                                                                                       \
          a->tmp_result.clear();                                                                                                                \
                                                                                                                                                \
          std::unique_ptr<boost::regex> re(make_regexp(object_mask));                                                                           \
                                                                                                                                                \
          for (auto & x : file_data.get_data())                                                                                                 \
            {                                                                                                                                   \
              if (does_match(re.get(), x.first))                                                                                                \
                {                                                                                                                               \
                  unsigned int begin_idx, end_idx;                                                                                              \
                                                                                                                                                \
                  if (pbeast::get_raw_data_bounds(x.second->m_data, since, until, w_since, w_until, begin_idx, end_idx))                        \
                    a->tmp_result.emplace_back(&x.first, begin_idx, end_idx);                                                                   \
                }                                                                                                                               \
            }                                                                                                                                   \
                                                                                                                                                \
          if(a->tmp_result.empty()) return;                                                                                                     \
          data->length(a->tmp_result.size());                                                                                                   \
                                                                                                                                                \
          for (auto & y : file_data.get_data())                                                                                                 \
            {                                                                                                                                   \
              if(y.second->m_data.empty() == false)                                                                                             \
                {                                                                                                                               \
                  if(&y.first == a->tmp_result[count].m_str)                                                                                    \
                    {                                                                                                                           \
                      const unsigned int begin_idx = a->tmp_result[count].m_begin_idx;                                                          \
                      const unsigned int end_idx = a->tmp_result[count].m_end_idx;                                                              \
                                                                                                                                                \
                      (*data)[count].object_id = CORBA::string_dup(y.first.c_str());                                                            \
                      (*data)[count].data.length(end_idx-begin_idx+1);                                                                          \
                      unsigned int idx(0);                                                                                                      \
                      for (unsigned int j = begin_idx; j <= end_idx; ++j)                                                                       \
                        {                                                                                                                       \
                          auto & x(y.second->m_data[j]);                                                                                        \
                          (*data)[count].data[idx].ts.created = x.get_ts_created();                                                             \
                          (*data)[count].data[idx].ts.updated = x.get_ts_last_updated();                                                        \
                          ASSIGN_CODE                                                                                                           \
                          idx++;                                                                                                                \
                        }                                                                                                                       \
                      if(++count == a->tmp_result.size()) break;                                                                                \
                    }                                                                                                                           \
                }                                                                                                                               \
            }                                                                                                                                   \
        }                                                                                                                                       \
      else                                                                                                                                      \
        {                                                                                                                                       \
          if (const SERIE<CPP_TYPE> * data_points = file_data.find_serie(object_mask))                                                          \
            {                                                                                                                                   \
              unsigned int begin_idx, end_idx;                                                                                                  \
              if (pbeast::get_raw_data_bounds(data_points->m_data, since, until, w_since, w_until, begin_idx, end_idx))                         \
                {                                                                                                                               \
                  data->length(1);                                                                                                              \
                  (*data)[0].object_id = CORBA::string_dup(object_mask);                                                                        \
                  (*data)[0].data.length(end_idx-begin_idx+1);                                                                                  \
                  unsigned int idx(0);                                                                                                          \
                  for (unsigned int j = begin_idx; j <= end_idx; ++j)                                                                           \
                    {                                                                                                                           \
                      auto & x(data_points->m_data[j]);                                                                                         \
                      (*data)[0].data[idx].ts.created = x.get_ts_created();                                                                     \
                      (*data)[0].data[idx].ts.updated = x.get_ts_last_updated();                                                                \
                      ASSIGN_CODE                                                                                                               \
                      idx++;                                                                                                                    \
                    }                                                                                                                           \
                }                                                                                                                               \
            }                                                                                                                                   \
        }                                                                                                                                       \
    }                                                                                                                                           \
}                                                                                                                                               \


#define GET_SIMPLE_MANY(LIST_TYPE, PBEAST_TYPE, CPP_TYPE, ASSIGN_CODE)  \
  GET_ALL( LIST_TYPE, PBEAST_TYPE, false, CPP_TYPE, pbeast::Serie, Data, ASSIGN_CODE )

#define GET_VECTOR_MANY(LIST_TYPE, PBEAST_TYPE, CPP_TYPE, ASSIGN_CODE)  \
  GET_ALL( LIST_TYPE, PBEAST_TYPE, true, CPP_TYPE, pbeast::receiver::VectorSerie, VectorData, ASSIGN_CODE )

///////////////////////////

#define ASSIGN_DS_PRIMITIVE_OBJ_TYPE                                                                    \
    (*data)[count].data[idx].data.min = tmp[idx].data().get_min_value();                                \
    (*data)[count].data[idx].data.val = tmp[idx].data().get_value();                                    \
    (*data)[count].data[idx].data.max = tmp[idx].data().get_max_value();

#define ASSIGN_DS_STRING_OBJ_TYPE                                                                       \
    (*data)[count].data[idx].data.min = CORBA::string_dup(tmp[idx].data().get_min_value().c_str());     \
    (*data)[count].data[idx].data.val = CORBA::string_dup(tmp[idx].data().get_value().c_str());         \
    (*data)[count].data[idx].data.max = CORBA::string_dup(tmp[idx].data().get_max_value().c_str());

#define ASSIGN_DS_PRIMITIVE_OBJ_VECTOR                                                                  \
    int idx2(0);                                                                                        \
    (*data)[count].data[idx].data.length(tmp[idx].data().size());                                       \
    for(const auto & y : tmp[idx].data()) {                                                             \
      ((*data)[count].data[idx].data)[idx2].min = y.get_min_value();                                    \
      ((*data)[count].data[idx].data)[idx2].val = y.get_value();                                        \
      ((*data)[count].data[idx].data)[idx2].max = y.get_max_value();                                    \
      idx2++;                                                                                           \
    }

#define ASSIGN_DS_STRING_OBJ_VECTOR                                                                     \
    int idx2(0);                                                                                        \
    (*data)[count].data[idx].data.length(tmp[idx].data().size());                                       \
    for(const auto & y : tmp[idx].data()) {                                                             \
      ((*data)[count].data[idx].data)[idx2].min = CORBA::string_dup(y.get_min_value().c_str());         \
      ((*data)[count].data[idx].data)[idx2].val = CORBA::string_dup(y.get_value().c_str());             \
      ((*data)[count].data[idx].data)[idx2].max = CORBA::string_dup(y.get_max_value().c_str());         \
      idx2++;                                                                                           \
    }


#define GET_DS_ALL( LIST_TYPE, PBEAST_TYPE, IS_ARRAY, CPP_TYPE, SERIE, DS_SERIE, RECEIVER_DATA, ASSIGN_CODE )                                   \
{                                                                                                                                               \
  validate_is_empty(select_functions);                                                                                                          \
  validate_ds_interval(interval);                                                                                                               \
  get_downsample_eovs(partition_name, class_name, since, until, prev_interval, next_interval, interval, object_mask, is_regexp, eov);           \
  get_downsample_upds(partition_name, class_name, since, until, prev_interval, next_interval, interval, object_mask, is_regexp, fill_upd, upd); \
                                                                                                                                                \
  data = new pbeast::LIST_TYPE();                                                                                                               \
                                                                                                                                                \
  if ( (until + next_interval) < m_started_at ) return;                                                                                         \
                                                                                                                                                \
  pbeast::receiver::Attribute * a = check_and_get_attribute(partition_name, class_name, attribute_name);                                        \
                                                                                                                                                \
  std::lock_guard<std::mutex> data_lock(a->m_compact_data_mutex);                                                                               \
                                                                                                                                                \
  if (a->m_compact_data)                                                                                                                        \
    {                                                                                                                                           \
      /* check attribute type only if there are received data */                                                                                \
      check_attribute(a, partition_name, class_name, attribute_name, PBEAST_TYPE, IS_ARRAY);                                                    \
                                                                                                                                                \
      pbeast::receiver::RECEIVER_DATA<CPP_TYPE>& file_data = static_cast<pbeast::receiver::RECEIVER_DATA<CPP_TYPE>&>(*a->m_compact_data);       \
      unsigned int count(0);                                                                                                                    \
                                                                                                                                                \
      const uint64_t w_since = pbeast::mk_wide_since(since, prev_interval);                                                                     \
      const uint64_t w_until = pbeast::mk_wide_until(until, next_interval);                                                                     \
                                                                                                                                                \
      if (is_regexp)                                                                                                                            \
        {                                                                                                                                       \
          a->tmp_result.clear();                                                                                                                \
                                                                                                                                                \
          std::unique_ptr<boost::regex> re(make_regexp(object_mask));                                                                           \
                                                                                                                                                \
          for (auto & x : file_data.get_data())                                                                                                 \
            {                                                                                                                                   \
              if (does_match(re.get(), x.first))                                                                                                \
                {                                                                                                                               \
                  unsigned int begin_idx, end_idx;                                                                                              \
                                                                                                                                                \
                  if (pbeast::get_raw_data_bounds(x.second->m_data, since, until, w_since, w_until, begin_idx, end_idx))                        \
                    a->tmp_result.emplace_back(&x.first, begin_idx, end_idx);                                                                   \
                }                                                                                                                               \
            }                                                                                                                                   \
                                                                                                                                                \
          if(a->tmp_result.empty()) return;                                                                                                     \
          data->length(a->tmp_result.size());                                                                                                   \
                                                                                                                                                \
          for (auto & y : file_data.get_data())                                                                                                 \
            {                                                                                                                                   \
              if (y.second->m_data.empty() == false)                                                                                            \
                {                                                                                                                               \
                  if (&y.first == a->tmp_result[count].m_str)                                                                                   \
                    {                                                                                                                           \
                      const unsigned int begin_idx = a->tmp_result[count].m_begin_idx;                                                          \
                      const unsigned int end_idx = a->tmp_result[count].m_end_idx;                                                              \
                      std::vector<DS_SERIE<CPP_TYPE>> tmp;                                                                                      \
                      pbeast::downsample<CPP_TYPE>(interval, y.second->m_data, tmp, begin_idx, end_idx, static_cast<pbeast::FillGaps::Type>(fill_gaps)); \
                                                                                                                                                \
                      (*data)[count].object_id = CORBA::string_dup(y.first.c_str());                                                            \
                      (*data)[count].data.length(tmp.size());                                                                                   \
                      for (unsigned int idx(0); idx < tmp.size(); ++idx)                                                                        \
                        {                                                                                                                       \
                          (*data)[count].data[idx].ts = tmp[idx].get_ts();                                                                      \
                          ASSIGN_CODE                                                                                                           \
                        }                                                                                                                       \
                      if (++count == a->tmp_result.size()) break;                                                                               \
                    }                                                                                                                           \
                }                                                                                                                               \
            }                                                                                                                                   \
        }                                                                                                                                       \
      else                                                                                                                                      \
        {                                                                                                                                       \
          if(const SERIE<CPP_TYPE> * data_points = file_data.find_serie(object_mask))                                                           \
            {                                                                                                                                   \
              unsigned int begin_idx, end_idx;                                                                                                  \
              if (pbeast::get_raw_data_bounds(data_points->m_data, since, until, w_since, w_until, begin_idx, end_idx))                         \
                {                                                                                                                               \
                  std::vector<DS_SERIE<CPP_TYPE>> tmp;                                                                                          \
                  pbeast::downsample<CPP_TYPE>(interval, data_points->m_data, tmp, begin_idx, end_idx, static_cast<pbeast::FillGaps::Type>(fill_gaps)); \
                  if (tmp.size())                                                                                                               \
                    {                                                                                                                           \
                      data->length(1);                                                                                                          \
                      (*data)[0].object_id = CORBA::string_dup(object_mask);                                                                    \
                      (*data)[0].data.length(tmp.size());                                                                                       \
                      for (unsigned int idx(0); idx < tmp.size(); ++idx)                                                                        \
                        {                                                                                                                       \
                          (*data)[0].data[idx].ts = tmp[idx].get_ts();                                                                          \
                          ASSIGN_CODE                                                                                                           \
                        }                                                                                                                       \
                    }                                                                                                                           \
                }                                                                                                                               \
            }                                                                                                                                   \
        }                                                                                                                                       \
    }                                                                                                                                           \
}

#define GET_DS_SIMPLE_MANY(LIST_TYPE, PBEAST_TYPE, CPP_TYPE, ASSIGN_CODE)  \
  GET_DS_ALL( LIST_TYPE, PBEAST_TYPE, false, CPP_TYPE, pbeast::Serie, pbeast::DownsampledSeriesData, Data, ASSIGN_CODE )

#define GET_DS_VECTOR_MANY(LIST_TYPE, PBEAST_TYPE, CPP_TYPE, ASSIGN_CODE)  \
  GET_DS_ALL( LIST_TYPE, PBEAST_TYPE, true, CPP_TYPE, pbeast::receiver::VectorSerie, pbeast::DownsampledSeriesVectorData, VectorData, ASSIGN_CODE )


void
pbeast::receiver::DataProvider::get_v64(pbeast::receiver::Attribute * a, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out& data)
{
  data = new pbeast::V64ObjectList();

  if (a)
    {
      std::lock_guard<std::mutex> data_lock(a->m_compact_data_mutex);

      if (a->m_compact_data)
        {
          pbeast::receiver::Data<pbeast::Void>& file_data = static_cast<pbeast::receiver::Data<pbeast::Void>&>(*a->m_compact_data);

          const uint64_t w_since = pbeast::mk_wide_since(since, prev_interval);
          const uint64_t w_until = pbeast::mk_wide_until(until, next_interval);

          if (is_regexp)
            {
              a->tmp_result.clear();

              std::unique_ptr<boost::regex> re(make_regexp(object_mask));

              for (auto & x : file_data.get_data())
                {
                  if (does_match(re.get(), x.first))
                    {
                      unsigned int begin_idx, end_idx;

                      if (pbeast::get_raw_data_bounds(x.second->m_data, since, until, w_since, w_until, begin_idx, end_idx))
                        {
                          a->tmp_result.emplace_back(&x.first, begin_idx, end_idx);
                        }
                    }
                }

              if (a->tmp_result.empty())
                return;

              data->length(a->tmp_result.size());

              unsigned int count(0);

              for (auto & y : file_data.get_data())
                {
                  if (y.second->m_data.empty() == false)
                    {
                      if (&y.first == a->tmp_result[count].m_str)
                        {
                          const unsigned int begin_idx = a->tmp_result[count].m_begin_idx;
                          const unsigned int end_idx = a->tmp_result[count].m_end_idx;

                          (*data)[count].object_id = CORBA::string_dup(y.first.c_str());
                          (*data)[count].data.length(end_idx - begin_idx + 1);
                          unsigned int idx(0);
                          for (unsigned int j = begin_idx; j <= end_idx; ++j)
                            {
                              (*data)[count].data[idx++] = y.second->m_data[j].get_ts_created();
                            }
                          if (++count == a->tmp_result.size())
                            break;
                        }
                    }
                }
            }
          else
            {
              if (const pbeast::Serie<pbeast::Void> * data_points = file_data.find_serie(object_mask))
                {
                  unsigned int begin_idx, end_idx;
                  if (pbeast::get_raw_data_bounds(data_points->m_data, since, until, w_since, w_until, begin_idx, end_idx))
                    {
                      data->length(1);
                      (*data)[0].object_id = CORBA::string_dup(object_mask);
                      (*data)[0].data.length(end_idx - begin_idx + 1);
                      unsigned int idx(0);
                      for (unsigned int j = begin_idx; j <= end_idx; ++j)
                        {
                          (*data)[0].data[idx++] = data_points->m_data[j].get_ts_created();
                        }
                    }
                }
            }
        }
    }
}


void
pbeast::receiver::DataProvider::get_eovs(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out data)
{
  get_v64(check_and_get_class(partition_name, class_name)->m_end_of_validity_attribute, since, until, prev_interval, next_interval, object_mask, is_regexp, data);
}

void
pbeast::receiver::DataProvider::get_upds(bool fill_upd, const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out data)
{
  get_v64(fill_upd ? check_and_get_class(partition_name, class_name)->m_updated_info_attribute : nullptr, since, until, prev_interval, next_interval, object_mask, is_regexp, data);
}


void
pbeast::receiver::DataProvider::get_v32(pbeast::receiver::Attribute * a, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, const char* object_mask, bool is_regexp, pbeast::V32ObjectList_out& data)
{
  data = new pbeast::V32ObjectList();

  if (a)
    {
      std::lock_guard<std::mutex> data_lock(a->m_compact_data_mutex);

      if (a->m_compact_data)
        {
          pbeast::receiver::Data<pbeast::Void>& file_data = static_cast<pbeast::receiver::Data<pbeast::Void>&>(*a->m_compact_data);

          const uint64_t w_since = pbeast::mk_wide_since(since, prev_interval);
          const uint64_t w_until = pbeast::mk_wide_until(until, next_interval);

          if (is_regexp)
            {
              a->tmp_result.clear();

              std::unique_ptr<boost::regex> re(make_regexp(object_mask));

              for (auto & x : file_data.get_data())
                {
                  if (does_match(re.get(), x.first))
                    {
                      unsigned int begin_idx, end_idx;

                      if (pbeast::get_raw_data_bounds(x.second->m_data, since, until, w_since, w_until, begin_idx, end_idx))
                        {
                          a->tmp_result.emplace_back(&x.first, begin_idx, end_idx);
                        }
                    }
                }

              if (a->tmp_result.empty())
                return;
              data->length(a->tmp_result.size());

              const uint32_t half_interval(interval / 2);

              unsigned int count(0);

              for (auto & y : file_data.get_data())
                {
                  if (y.second->m_data.empty() == false)
                    {
                      if (&y.first == a->tmp_result[count].m_str)
                        {
                          const unsigned int begin_idx = a->tmp_result[count].m_begin_idx;
                          const unsigned int end_idx = a->tmp_result[count].m_end_idx;

                          std::vector<uint32_t> tmp;
                          tmp.reserve(end_idx - begin_idx + 1);

                          uint32_t last(pbeast::mk_downsample_ts(pbeast::ts2secs(y.second->m_data[begin_idx].get_ts_created()), interval));

                          for (unsigned int j = begin_idx; j <= end_idx; ++j)
                            {
                              uint32_t x = pbeast::ts2secs(y.second->m_data[j].get_ts_created());
                              if ((x - last) > interval)
                                {
                                  tmp.push_back(last + half_interval);
                                  last = pbeast::mk_downsample_ts(x, interval);
                                }
                            }

                          tmp.push_back(last + half_interval);

                          (*data)[count].object_id = CORBA::string_dup(y.first.c_str());
                          (*data)[count].data.length(tmp.size());

                          for (unsigned int idx = 0; idx < tmp.size(); ++idx)
                            {
                              (*data)[count].data[idx] = tmp[idx];
                            }

                          if (++count == a->tmp_result.size())
                            break;
                        }
                    }
                }
            }
          else
            {
              if (const pbeast::Serie<pbeast::Void> * data_points = file_data.find_serie(object_mask))
                {
                  unsigned int begin_idx, end_idx;
                  if (pbeast::get_raw_data_bounds(data_points->m_data, since, until, w_since, w_until, begin_idx, end_idx))
                    {
                      std::vector<uint32_t> tmp;
                      tmp.reserve(end_idx - begin_idx + 1);

                      const uint32_t half_interval(interval / 2);
                      uint32_t last(pbeast::mk_downsample_ts(pbeast::ts2secs(data_points->m_data[begin_idx].get_ts_created()), interval));

                      for (unsigned int j = begin_idx; j <= end_idx; ++j)
                        {
                          uint32_t x = pbeast::ts2secs(data_points->m_data[j].get_ts_created());
                          if ((x - last) > interval)
                            {
                              tmp.push_back(last + half_interval);
                              last = pbeast::mk_downsample_ts(x, interval);
                            }
                        }

                      tmp.push_back(last + half_interval);

                      data->length(1);
                      (*data)[0].object_id = CORBA::string_dup(object_mask);
                      (*data)[0].data.length(tmp.size());
                      for (unsigned int idx = 0; idx < tmp.size(); ++idx)
                        {
                          (*data)[0].data[idx] = tmp[idx];
                        }
                    }
                }
            }
        }
    }

}

void
pbeast::receiver::DataProvider::get_downsample_eovs(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, const char* object_mask, bool is_regexp, pbeast::V32ObjectList_out data)
{
  get_v32(check_and_get_class(partition_name, class_name)->m_end_of_validity_attribute, since, until, prev_interval, next_interval, interval, object_mask, is_regexp, data);
}

void
pbeast::receiver::DataProvider::get_downsample_upds(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, const char* object_mask, bool is_regexp, bool fill_upd, pbeast::V32ObjectList_out data)
{
  get_v32(fill_upd ? check_and_get_class(partition_name, class_name)->m_updated_info_attribute : nullptr, since, until, prev_interval, next_interval, interval, object_mask, is_regexp, data);
}


void
pbeast::receiver::DataProvider::get_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanObjectList_out data)
{
  GET_SIMPLE_MANY(BooleanObjectList, pbeast::BOOL, bool, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ObjectList_out data)
{
  GET_SIMPLE_MANY(S8ObjectList, pbeast::S8, int8_t, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ObjectList_out data)
{
  GET_SIMPLE_MANY(U8ObjectList, pbeast::U8, uint8_t, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ObjectList_out data)
{
  GET_SIMPLE_MANY(S16ObjectList, pbeast::S16, int16_t, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ObjectList_out data)
{
  GET_SIMPLE_MANY(U16ObjectList, pbeast::U16, uint16_t, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ObjectList_out data)
{
  GET_SIMPLE_MANY(S32ObjectList, pbeast::S32, int32_t, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ObjectList_out data)
{
  GET_SIMPLE_MANY(U32ObjectList, pbeast::U32, uint32_t, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ObjectList_out data)
{
  GET_SIMPLE_MANY(S64ObjectList, pbeast::S64, int64_t, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ObjectList_out data)
{
  GET_SIMPLE_MANY(U64ObjectList, pbeast::U64, uint64_t, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatObjectList_out data)
{
  GET_SIMPLE_MANY(FloatObjectList, pbeast::FLOAT, float, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleObjectList_out data)
{
  GET_SIMPLE_MANY(DoubleObjectList, pbeast::DOUBLE, double, ASSIGN_PRIMITIVE_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringObjectList_out data)
{
  GET_SIMPLE_MANY(StringObjectList, pbeast::STRING, std::string, ASSIGN_STRING_OBJ_TYPE )
}

void
pbeast::receiver::DataProvider::get_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanArrayObjectList_out data)
{
  GET_VECTOR_MANY(BooleanArrayObjectList, pbeast::BOOL, bool, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S8ArrayObjectList, pbeast::S8, int8_t, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U8ArrayObjectList, pbeast::U8, uint8_t, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S16ArrayObjectList, pbeast::S16, int16_t, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U16ArrayObjectList, pbeast::U16, uint16_t, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S32ArrayObjectList, pbeast::S32, int32_t, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U32ArrayObjectList, pbeast::U32, uint32_t, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S64ArrayObjectList, pbeast::S64, int64_t, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U64ArrayObjectList, pbeast::U64, uint64_t, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatArrayObjectList_out data)
{
  GET_VECTOR_MANY(FloatArrayObjectList, pbeast::FLOAT, float, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleArrayObjectList_out data)
{
  GET_VECTOR_MANY(DoubleArrayObjectList, pbeast::DOUBLE, double, ASSIGN_PRIMITIVE_OBJ_VECTOR )
}

void
pbeast::receiver::DataProvider::get_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringArrayObjectList_out data)
{
  GET_VECTOR_MANY(StringArrayObjectList, pbeast::STRING, std::string, ASSIGN_STRING_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(BooleanDownSampleObjectList, BOOL, bool, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S8DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S8DownSampleObjectList, S8, int8_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U8DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U8DownSampleObjectList, U8, uint8_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S16DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S16DownSampleObjectList, S16, int16_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U16DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U16DownSampleObjectList, U16, uint16_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S32DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S32DownSampleObjectList, S32, int32_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U32DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U32DownSampleObjectList, U32, uint32_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S64DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S64DownSampleObjectList, S64, int64_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U64DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U64DownSampleObjectList, U64, uint64_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(FloatDownSampleObjectList, FLOAT, float, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(DoubleDownSampleObjectList, DOUBLE, double, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::StringDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(StringDownSampleObjectList, STRING, std::string, ASSIGN_DS_STRING_OBJ_TYPE)
}

void
pbeast::receiver::DataProvider::get_downsample_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(BooleanDownSampleArrayObjectList, BOOL, bool, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S8DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S8DownSampleArrayObjectList, S8, int8_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U8DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U8DownSampleArrayObjectList, U8, uint8_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S16DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S16DownSampleArrayObjectList, S16, uint16_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U16DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U16DownSampleArrayObjectList, U16, uint16_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S32DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S32DownSampleArrayObjectList, S32, int32_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U32DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U32DownSampleArrayObjectList, U32, uint32_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S64DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S64DownSampleArrayObjectList, S64, int64_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U64DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U64DownSampleArrayObjectList, U64, uint64_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(FloatDownSampleArrayObjectList, FLOAT, float, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(DoubleDownSampleArrayObjectList, DOUBLE, double, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::get_downsample_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::StringDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(StringDownSampleArrayObjectList, STRING, std::string, ASSIGN_DS_STRING_OBJ_VECTOR)
}

void
pbeast::receiver::DataProvider::send_command(const char* command_name, const ::pbeast::StringArray& command_args, ::CORBA::String_out command_output)
{
  std::ostringstream output;

  if (!strcmp(command_name, "commit"))
    {
      auto len = command_args.length();

      if (len < 3)
        {
          std::ostringstream text;
          text << "expected parameters of commit command: partition class [+attribute]";
          throw pbeast::BadRequest(text.str().c_str());
        }

      const std::string partition_name(command_args[0]);
      const std::string class_name(command_args[1]);

      std::vector<std::string> attribute_names;

      attribute_names.reserve(len-2);

      for (unsigned int i = 2; i < len; i++)
        attribute_names.emplace_back(command_args[i]);

      try
        {
          commit_command(partition_name, class_name, attribute_names, output);
        }
      catch (const std::exception& ex)
        {
          throw pbeast::Failure(ex.what());
        }
    }
  else
    {
      std::ostringstream text;
      text << "command \'" << command_name << "\' is not supported";
      throw pbeast::BadRequest(text.str().c_str());
    }

  command_output = pbeast::utils::string_dup(output.str());
}

void
pbeast::receiver::DataProvider::get_partitions(StringArray_out partitions)
{
  std::shared_lock p_lock(m_partitions_mutex);

  partitions = new pbeast::StringArray();
  partitions->length(m_partitions.size());

  unsigned int idx = 0;
  for (auto &p : m_partitions)
    (*partitions)[idx++] = CORBA::string_dup(p.first.c_str());
}

void
pbeast::receiver::DataProvider::get_classes(const char *partition_name, pbeast::StringArray_out classes)
{
  Partition *p = check_partition(partition_name);

  classes = new pbeast::StringArray();
  classes->length(p->get_classes().size());

  unsigned int idx = 0;
  for (auto &c : p->get_classes())
    (*classes)[idx++] = CORBA::string_dup(c.first.c_str());
}

void
pbeast::receiver::DataProvider::get_attributes(const char *partition_name, const char *class_name, pbeast::StringDataPointArrayList_out attributes)
{
  pbeast::receiver::Class *c = check_and_get_class(partition_name, class_name);

  attributes = new pbeast::StringDataPointArrayList();
  attributes->length(1);

  (*attributes)[0].ts.created = m_started_at;
  (*attributes)[0].ts.updated = pbeast::mk_ts(time(0), 0);

  (*attributes)[0].data.length(c->m_attributes.size());
  unsigned int idx = 0;
  for (auto &a : c->m_attributes)
    (*attributes)[0].data[idx++] = CORBA::string_dup(a->get_name().c_str());
}


void
pbeast::receiver::DataProvider::get_attribute_info(const char* partition_name, const char* class_name, const char* attribute_name, pbeast::S32DataPointList_out type, pbeast::BooleanDataPointList_out is_array, pbeast::StringDataPointList_out description)
{
  pbeast::receiver::Attribute * a = check_and_get_attribute(partition_name, class_name, attribute_name);

  type = new pbeast::S32DataPointList();
  type->length(1);

  is_array = new pbeast::BooleanDataPointList();
  is_array->length(1);

  description  = new pbeast::StringDataPointList();
  description->length(1);

  (*type)[0].ts.created = (*is_array)[0].ts.created = (*description)[0].ts.created = m_started_at;
  (*type)[0].ts.updated = (*is_array)[0].ts.updated = (*description)[0].ts.updated = m_started_at; // see ADAMATLAS-404, do not use "now": pbeast::mk_ts(time(0), 0);

  (*type)[0].data = a->get_type();
  (*is_array)[0].data = a->get_is_array();
  (*description)[0].data = CORBA::string_dup(a->get_description().c_str());
}

void
pbeast::receiver::DataProvider::get_attribute_type(const char* partition_name, const char* class_name, const char* attribute_name, AttributeTypeList_out data)
{
  pbeast::receiver::Attribute * a = check_and_get_attribute(partition_name, class_name, attribute_name);

  data = new pbeast::AttributeTypeList();
  data->length(1);

  (*data)[0].ts.created = m_started_at;
  (*data)[0].ts.updated = m_started_at; // see ADAMATLAS-404, do not use "now": pbeast::mk_ts(time(0), 0);
  (*data)[0].type = CORBA::string_dup(a->get_type_class() ? a->get_type_class()->get_name().c_str() : a->get_type_name().c_str());
  (*data)[0].is_array = a->get_is_array();
}

void
pbeast::receiver::DataProvider::get_schema(const char* partition_name, ClassInfoList_out schema)
{
  schema = new ClassInfoList();
  schema->length(0);
}


void
pbeast::receiver::DataProvider::get_updated_objects(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong /*since*/, ::CORBA::ULongLong /*until*/, const char* object_mask, StringArray_out objects)
{
  pbeast::receiver::Attribute * a = check_and_get_attribute(partition_name, class_name, attribute_name);

  objects = new StringArray();

  std::unique_ptr<boost::regex> re(make_regexp(object_mask));

  std::vector<std::string> data;

  {
    std::lock_guard<std::mutex> data_lock(a->m_compact_data_mutex);

    if(a->m_compact_data)
      {
        a->m_compact_data->get_objects(data, re.get());
      }
  }

  if(const unsigned int len = data.size())
    {
      objects->length(len);
      for(unsigned int idx(0); idx < len; ++idx)
        {
          objects[idx] = CORBA::string_dup(data[idx].c_str());
        }
    }
}
