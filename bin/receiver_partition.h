#ifndef PBEAST_RECEIVER_PARTITION_H
#define PBEAST_RECEIVER_PARTITION_H

#include "receiver_class.h"

namespace pbeast
{
  namespace receiver
  {

    class DataProvider;

    class Partition
    {

      friend class DataProvider;

    public:

      Partition(const std::string& name) :
          m_name(name), m_dir_name(name), m_dir_created(false)
      {
        ;
      }

      ~Partition()
      {
        for(auto& x : m_classes)
          delete x.second;

        m_classes.clear();
      }

      Partition() = delete;

      Partition(const Partition&) = delete;

      Partition&
      operator=(const Partition&) = delete;

      const std::string&
      get_name() const
      {
        return m_name;
      }

      const std::string&
      get_dir_name() const
      {
        return m_dir_name;
      }

      const std::map<std::string, Class *>&
      get_classes() const
      {
        return m_classes;
      }

      void
      define_class(const std::string& name, Class * c)
      {
        m_classes[name] = c;
      }


    private:

      std::map<std::string, Class *> m_classes;
      std::string m_name;
      std::string m_dir_name;
      bool m_dir_created;

    };

  }
}

#endif
