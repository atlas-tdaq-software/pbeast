#include <cstdint>

#include <chrono>
#include <iomanip>
#include <string>
#include <filesystem>

#include <boost/program_options.hpp>

#include "pbeast/input-data-file.h"
#include "pbeast/output-data-file.h"
#include "pbeast/copy-file.h"

namespace po = boost::program_options;


int
main(int argc, char ** argv)
{
  po::options_description desc("P-BEAST utility to copy data files using latest data format and various compression options. It might also be used to downsample data.");

  std::string file_in;
  std::string file_out;

  uint64_t in_size, out_size;

  uint32_t downsample_interval = 0;
  bool zip_catalog, zip_data, catalogize_strings;

  auto ts = std::chrono::steady_clock::now();

  try
    {
      desc.add_options()
          ("input-file,f", po::value<std::string>(&file_in)->required(), "name of input file")
          ("output-file,o", po::value<std::string>(&file_out)->required(), "name of output file (different from input)")
          ("downsample-interval,e", po::value<uint32_t>(&downsample_interval)->default_value(downsample_interval), "downsample data with this interval")
          ("zip-catalog,c", "compress catalog")
          ("zip-data,d", "compress data")
          ("unzip-catalog,C", "store catalog uncompressed")
          ("unzip-data,D", "store data uncompressed")
          ("catalogize-strings,s", "catalogize string data")
          ("zip,z", "zip all (i.e. catalog, data, catalogize strings data)")
          ("unzip,u", "unzip all")
          ("help,h","Print help message");

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);

      pbeast::InputDataFile in(file_in);

      if (vm.count("zip"))
        {
          zip_catalog = zip_data = catalogize_strings = true;
        }
      else if (vm.count("unzip"))
        {
          zip_catalog = zip_data = catalogize_strings = false;
        }
      else
        {
          if (vm.count("zip-data"))
            zip_data = true;
          else if (vm.count("unzip-data"))
            zip_data = false;
          else
            zip_data = in.get_zip_data_flag();

          if (vm.count("zip-catalog"))
            zip_catalog = true;
          else if (vm.count("unzip-catalog"))
            zip_catalog = false;
          else
            zip_catalog = in.get_zip_catalog_flag();

          if (vm.count("catalogize-strings"))
            catalogize_strings = true;
          else
            catalogize_strings = in.get_catalogize_strings_flag();
        }

      if (in.is_downsampled() && downsample_interval == 0)
        downsample_interval = in.get_downsample_interval();

      pbeast::OutputDataFile out(file_out, in.get_partition(), in.get_class(), in.get_class_path(),
          in.get_attribute(), in.get_base_timestamp(), in.get_data_type(), in.get_is_array(),
          in.get_number_of_series(), zip_catalog, zip_data, catalogize_strings, downsample_interval);

      pbeast::copy_file(in, out);

      out_size = out.close();
    }
  catch (std::exception& ex)
    {
      std::cerr << "ERROR: " << ex.what() << std::endl;
      return 1;
    }

  in_size = std::filesystem::file_size(file_in);

  std::cout << "copy file \"" << file_in << "\" (" << in_size << " bytes) to \"" << file_out << "\" (" << out_size << " bytes) with compression ratio " << std::fixed << std::setfill('0') << std::setprecision(2) << ((float)in_size - (float)out_size) * 100.0 / (float)in_size << "% in " << std::setprecision(3) << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-ts).count() / 1000. << " seconds"<< std::endl;

  return 0;
}
