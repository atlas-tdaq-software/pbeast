#include <unistd.h>

#include <chrono>
#include <filesystem>
#include <thread>
#include <iomanip>

#include <ipc/signal.h>

#include <pbeast/meta.h>
#include <pbeast/repository.h>

#include "application_exceptions.h"
#include "receiver_data_provider.h"
#include "receiver_partition.h"
#include "receiver_class.h"
#include "receiver_config.h"
#include "receiver_monitoring.h"
#include "receiver_errors.h"


boost::shared_mutex pbeast::receiver::DataProvider::s_data_provider_mutex;
pbeast::receiver::DataProvider * pbeast::receiver::DataProvider::s_data_provider(nullptr);

pbeast::receiver::Monitoring * pbeast::receiver::DataProvider::s_monitor(nullptr);

std::multiset<uint64_t> pbeast::receiver::DataProvider::m_data_size;
uint64_t pbeast::receiver::DataProvider::m_total_data_size(0);

pbeast::receiver::Partition * pbeast::receiver::DataProvider::m_default_partition(nullptr);

pbeast::receiver::DataProvider::DataProvider(const std::string& name, const std::filesystem::path& path, const IPCPartition& p, int thread_pool_size) :
    IPCNamedObject<POA_pbeast::server>(p, name),
    m_repository_path(path),
    m_thread_pool(thread_pool_size),
    m_partition(p),
    m_default_dir_partition_name(pbeast::DataFile::encode(m_partition.name())),
    m_monitor(m_partition.name(), name)
{
  m_default_partition = define_partition(m_partition.name());

  s_monitor = &m_monitor;

  m_started_at = pbeast::mk_ts(time(0), 0);
  pbeast::Meta::set_info_ts(m_started_at);
}

void
pbeast::receiver::DataProvider::link_class_types(Partition *partition)
{
  const auto& classes = partition->get_classes();

  for (auto &c : classes)
    {
      for (auto &a : c.second->get_attributes())
        {
          if (a->get_type() == pbeast::OBJECT)
            {
              try
                {
                  a->link_type(classes);
                }
              catch (daq::pbeast::BadInfoType& ex)
                {
                  ers::error(daq::pbeast::UnexpectedAttributeClassType(ERS_HERE, a->get_name(), c.first, partition->get_name(), ex));
                  c.second->set_incomplete_schema();
                }
            }
        }
    }

  for (auto &c : classes)
    {
      for (auto &a : c.second->get_attributes())
        {
          if (a->get_type_class() != 0)
            {
              a->create_nested();
            }
        }

      ERS_DEBUG(1, *(c.second));
    }
}



void
pbeast::receiver::DataProvider::ipc_withdraw()
{
  // remove service from IPC
  try
    {
      withdraw();
    }
  catch (ers::Issue & ex)
    {
      ers::error(daq::pbeast::WithdrawFailed(ERS_HERE, ex));
    }
}

void
pbeast::receiver::DataProvider::save_all_data()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  // save collected data
  try
    {
      auto t1 = std::chrono::steady_clock::now();
      std::shared_lock p_lock(m_partitions_mutex);

      rebuild_all_eov(s_iov_infinity);
      compact_all_data(s_iov_infinity);
      commit_all_data(s_iov_infinity);

      ERS_LOG("commit all data in " << std::fixed << std::setprecision(3) << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count() / 1000.  << " s");
    }
  catch (ers::Issue &ex)
    {
      ers::fatal(ex);
    }

  for (auto i = m_partitions.begin(); i != m_partitions.end(); ++i)
    {
      delete i->second;
    }
}

void
pbeast::receiver::DataProvider::enqueue_for_compaction(Class& c, uint64_t ts_cb_delay, std::vector<std::future<std::pair<uint64_t, const pbeast::receiver::Attribute *>>>& results)
{
  results.emplace_back( m_thread_pool.enqueue(std::bind(&pbeast::receiver::DataProvider::compact_data, this, c.m_end_of_validity_attribute, ts_cb_delay, nullptr)) );
  results.emplace_back( m_thread_pool.enqueue(std::bind(&pbeast::receiver::DataProvider::compact_data, this, c.m_updated_info_attribute, ts_cb_delay, nullptr)) );

  for (auto & j : c.m_attributes)
    results.emplace_back( m_thread_pool.enqueue(std::bind(&pbeast::receiver::DataProvider::compact_data, this, j, ts_cb_delay, c.get_eov())) );
}

void
pbeast::receiver::DataProvider::save_class(Partition& p, Class& c)
{
  try
    {
      c.rebuid_eov(s_iov_infinity);

        {
          std::vector<std::future<std::pair<uint64_t, const pbeast::receiver::Attribute *>>> results;

          std::lock_guard<std::mutex> lock(m_thread_pool_mutex);

          enqueue_for_compaction(c, s_iov_infinity, results);

          for (auto && result : results)
            result.get();
        }

      write_class(p, c, s_iov_infinity);

      m_writer_lock.unlock();
    }
  catch (ers::Issue &ex)
    {
      ers::fatal(ex);
    }
}

pbeast::receiver::Attribute *
pbeast::receiver::DataProvider::find_attribute(Partition& p, Class& c, const std::string& name)
{
  check_directory(p, c);

  for (auto & x : c.m_attributes)
    {
      if (c.is_nested())
        {
          const std::filesystem::path path(c.get_dir_path() / x->m_name);
          if (path.native() == name)
            return x;
        }
      else if (x->m_name == name)
        {
          return x;
        }

      if (Class * attribute_class = x->get_type_class())
        {
          if (auto a = find_attribute(p, *attribute_class, name))
            return a;
        }
    }

  return nullptr;
}

void
pbeast::receiver::DataProvider::commit_command(const std::string& partition_name, const std::string& class_name, const std::vector<std::string>& attribute_names, std::ostringstream& log)
{
  std::lock_guard<std::mutex> lock(m_mutex);

  try
    {
      auto t1 = std::chrono::steady_clock::now();
      std::shared_lock p_lock(m_partitions_mutex);

      auto p_it = m_partitions.find(partition_name);

      if (p_it == m_partitions.end())
        throw std::runtime_error(std::string("cannot find partition \'") + partition_name + "\"");

      auto class_it = p_it->second->m_classes.find(class_name);

      if (class_it == p_it->second->m_classes.end())
        throw std::runtime_error(std::string("cannot find class \'") + class_name + "\" in partition \"" + partition_name + "\"");

      Attribute * a = nullptr;

      for (const auto& x : attribute_names)
        {
          if (x == pbeast::Repository::get_eov_attribute_name())
            a = class_it->second->m_end_of_validity_attribute;
          else if (x == pbeast::Repository::get_upd_attribute_name())
            a = class_it->second->m_updated_info_attribute;
          else
            a = find_attribute(*(p_it->second), *(class_it->second), x);

          if (a == nullptr)
            throw std::runtime_error(std::string("cannot find attribute \"") + x + "\" in class \'" + class_name + "\" of partition \"" + partition_name + "\"");

          a->write_ready(m_writer_lock, m_repository_path, p_it->second->get_name(), p_it->second->get_dir_name(), *(class_it->second), s_iov_infinity);

          log << "committed \"" << a->get_name() << "\" attribute data\n";
        }

      ERS_LOG("commit on user request in " << std::fixed << std::setprecision(3) << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count() / 1000.  << " s");
    }
  catch (ers::Issue &ex)
    {
      ers::fatal(ex);
    }
}

pbeast::receiver::DataProvider::~DataProvider()
{
  ;
}

void
pbeast::receiver::DataProvider::shutdown()
{
  ERS_DEBUG(0, "shutdown request received");
  daq::ipc::signal::raise();
}

pbeast::receiver::Partition *
pbeast::receiver::DataProvider::get_partition(const std::string& name) const
{
  std::shared_lock p_lock(m_partitions_mutex);

  std::map<std::string, Partition *>::const_iterator i = m_partitions.find(name);
  return (i != m_partitions.end() ? i->second : nullptr);
}

pbeast::receiver::Partition *
pbeast::receiver::DataProvider::define_partition(const std::string& name)
{
  std::unique_lock p_lock(m_partitions_mutex);

  std::map<std::string, Partition *>::const_iterator i = m_partitions.find(name);
  if (i != m_partitions.end()) return i->second;

  Partition * p = new Partition(name);
  m_partitions[name] = p;

  return p;
}

pbeast::receiver::Class *
pbeast::receiver::DataProvider::get_class(const std::string& name, const Partition * partition) const
{
  std::map<std::string, Class *>::const_iterator i = partition->m_classes.find(name);
  return (i != partition->m_classes.end() ? i->second : nullptr);
}

std::pair<uint64_t, const pbeast::receiver::Attribute *>
pbeast::receiver::DataProvider::compact_data(Attribute * a, uint64_t ts_cb_delay, const std::map<std::string, std::set<uint64_t>*> * eov)
{
  auto t1 = std::chrono::steady_clock::now();

  if (Class * attribute_class = a->get_type_class())
    {
      for (auto &x : attribute_class->m_attributes)
        compact_data(x, ts_cb_delay, eov);
    }
  else
    {
      a->compact_data(ts_cb_delay, eov);
    }

  auto dt = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count();

  return std::pair<uint64_t, const Attribute *>(static_cast<uint64_t>(dt), a);
}

void
pbeast::receiver::DataProvider::write_ready(Partition& p, Class& c, Attribute& a, uint64_t ts_save_delay)
{
  if (a.has_compacted_data())
    {
      check_directory(p, c);
      a.write_ready(m_writer_lock, m_repository_path, p.get_name(), p.get_dir_name(), c, ts_save_delay);
    }

  if (Class * attribute_class = a.get_type_class())
    {
      check_directory(p, c);

      for (auto &a : attribute_class->m_attributes)
        write_ready(p, *attribute_class, *a, ts_save_delay);
    }
}

void
pbeast::receiver::DataProvider::rebuild_all_eov(uint64_t ts_eov_delay)
{
  std::vector<std::future<uint64_t> > results;

  // avoid possible interference with check_readers() method
  std::lock_guard<std::mutex> lock(m_thread_pool_mutex);

  auto t1 = std::chrono::steady_clock::now();

  for (auto &p : m_partitions)
    for (auto &i : p.second->get_classes())
      results.emplace_back(m_thread_pool.enqueue(std::bind(&pbeast::receiver::Class::rebuid_eov, i.second, ts_eov_delay)));

  for (auto && result : results)
    result.get();

  const auto rebuild_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count();
  if (rebuild_time > 0)
    ERS_DEBUG(0, "REBUILD EOV: " << rebuild_time << " ms");

  DataProvider::get_monitoring_object().rebuild_eov_time(rebuild_time);
}

void
pbeast::receiver::DataProvider::compact_all_data(uint64_t ts_cb_delay)
{
  std::vector<std::future<std::pair<uint64_t, const pbeast::receiver::Attribute *>>> results;

  // avoid possible interference with check_readers() method
  std::lock_guard<std::mutex> lock(m_thread_pool_mutex);

  auto t1 = std::chrono::steady_clock::now();

  for (auto &p : m_partitions)
    for (auto &i : p.second->get_classes())
      enqueue_for_compaction(*i.second, ts_cb_delay, results);

  std::map<const Class *, std::multimap<uint64_t, const Attribute *> > times;

  for (auto && result : results)
    {
      std::pair<uint64_t, const pbeast::receiver::Attribute *> r = result.get();
      if (r.first)
        times[r.second->m_parent].insert(r);
    }

  const auto compact_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count();

  std::multimap<uint64_t, const Class *> times2classes;

  for (auto & x : times)
    times2classes.insert(std::pair<uint64_t, const Class *>(x.second.rbegin()->first, x.first));

  std::ostringstream text;

  if (ers::debug_level() > 0)
    {
      for (std::multimap<uint64_t, const Class *>::reverse_iterator i = times2classes.rbegin(); i != times2classes.rend(); ++i)
        {
          text << "\n * class ";
          if (i->second)
            text << i->second->get_name();
          else
            text << "(null)";
          text << " => " << i->first << " ms: ";
          std::multimap<uint64_t, const Attribute *>& attrs = times[i->second];
          for (std::multimap<uint64_t, const Attribute *>::reverse_iterator j = attrs.rbegin(); j != attrs.rend(); ++j)
            {
              if (j != attrs.rbegin())
                text << ", ";
              text << j->second->get_name() << ' ' << j->first;
            }
        }
    }

  if (compact_time > 100)
    ERS_DEBUG(0, "COMPACT: " << compact_time << " ms" << text.str());
  else if (compact_time > 0)
    ERS_DEBUG(1, "COMPACT: " << compact_time << " ms" << text.str());

  DataProvider::get_monitoring_object().compact_time(compact_time);
}

void
pbeast::receiver::DataProvider::write_class(Partition& p, Class& c, uint64_t ts_save_delay)
{
  write_ready(p, c, *c.m_end_of_validity_attribute, ts_save_delay);
  write_ready(p, c, *c.m_updated_info_attribute, ts_save_delay);

  for (auto & j : c.m_attributes)
    write_ready(p, c, *j, ts_save_delay);
}

void
pbeast::receiver::DataProvider::commit_all_data(uint64_t ts_save_delay)
{
  auto t1 = std::chrono::steady_clock::now();

  for (auto &p : m_partitions)
    for (auto &i : p.second->get_classes())
      write_class(*p.second, *i.second, ts_save_delay);

  m_writer_lock.unlock();

  const auto commit_time = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count();
  if (commit_time > 0)
    ERS_DEBUG(0, "COMMIT: " << commit_time << " ms");

  DataProvider::get_monitoring_object().commit_time(commit_time);
}

void
pbeast::receiver::DataProvider::write_ready_data()
{
  std::lock_guard<std::mutex> lock(m_mutex);

  m_data_size.clear();
  m_total_data_size = 0;

    {
      time_t now = time(0);
      std::shared_lock p_lock(m_partitions_mutex);

      rebuild_all_eov(pbeast::mk_ts(now - get_config().get_big_data_delay() - get_config().get_max_data_delay() - get_config().get_save_check_interval(), 0));
      compact_all_data(pbeast::mk_ts(now - get_config().get_max_data_delay(), 0));
      commit_all_data(pbeast::mk_ts(now - get_config().get_max_save_delay(), 0));
    }

  DataProvider::get_monitoring_object().data_size(m_total_data_size);

  // if the max_data_size limit is exceeded, flush some data
  if (m_total_data_size > get_config().get_max_data_size())
    {
      const uint64_t size_limit = (get_config().get_max_data_size() * (100 - get_config().get_flush_data_threshold())) / 100;
      uint64_t size(0);

      for (auto &x : m_data_size)
        {
          size += x;
          if (size > size_limit)
            {
              pbeast::receiver::BaseData::set_max_data_bucket_size(x);
              ERS_DEBUG(0, "set max_data_bucket_size " << x);
              break;
            }
        }
    }

  // else set limit to max_file_size
  else
    {
      pbeast::receiver::BaseData::set_max_data_bucket_size(get_config().get_max_file_size());
    }
}


static void
create_directory(const std::filesystem::path& path)
{
  bool directory_exists;

  try
    {
      directory_exists = std::filesystem::exists(path);
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "check existence of directory", path, ex.code().message());
    }

  if (!directory_exists)
    {
      try
        {
          std::filesystem::create_directories(path);
        }
      catch (const std::filesystem::filesystem_error & ex)
        {
          throw daq::pbeast::FileSystemError(ERS_HERE, "create directory", path, ex.code().message());
        }
    }
  else if(std::filesystem::is_directory(path) == false)
    {
      throw daq::pbeast::FileSystemPathIsNotDirectory(ERS_HERE, path);
    }
}


void
pbeast::receiver::DataProvider::check_directory(Partition& p, Class& c)
{
  if (p.m_dir_created == false)
    {
      std::filesystem::path partition_dir_path(m_repository_path / p.get_dir_name());
      ::create_directory(partition_dir_path);
      p.m_dir_created = true;
    }

  if (c.m_directory_created == false)
    {
      std::filesystem::path class_path(m_repository_path / p.get_dir_name() / c.get_dir_path());
      unsigned int number_of_attributes = c.get_number_of_attributes();

      ::create_directory(class_path);

      if (c.is_nested() == false)
        {
          ::create_directory(class_path / c.m_end_of_validity_attribute->get_dir_name());
          ::create_directory(class_path / c.m_updated_info_attribute->get_dir_name());

          pbeast::Meta info(class_path);

          for (unsigned int i = 0; i < number_of_attributes; ++i)
            {
              const pbeast::receiver::Attribute& a(c.get_attribute(i));
              info.update_attribute(a.get_name(), pbeast::Meta::encode_type(a.get_type_name(), a.get_is_array()), a.get_description());

              // check that meta for real class of nested class is created (the class itself may have no instances)
              if (pbeast::receiver::Class * nested = a.get_type_class())
                {
                  Class * real_class_of_nested = get_class(nested->get_name());
                  check_directory(p, *real_class_of_nested);
                }
            }

          try
            {
              info.commit(p.get_name(), c.get_name());
            }
          catch (daq::pbeast::Exception& ex)
            {
              throw daq::pbeast::CannotCommitMetaInformation(ERS_HERE, c.get_name(), ex);
            }
        }

      for (unsigned int i = 0; i < number_of_attributes; ++i)
        {
          std::filesystem::path attribute_path(class_path / c.get_attribute(i).get_dir_name());
          ::create_directory(attribute_path);
        }

      c.m_directory_created = true;
    }
}


struct SaveDataThread
{
  // all in milliseconds
  unsigned int m_sleep_interval;
  unsigned int m_last_write_duration;

  SaveDataThread(unsigned int n) :
      m_sleep_interval(n * 1000), m_last_write_duration(0)
  {
    ;
  }

  void
  operator()()
  {
    while (true)
      {
        unsigned int sleep_interval(m_sleep_interval);

        if (m_last_write_duration >= m_sleep_interval)
          sleep_interval = 1000;
        else
          sleep_interval -= m_last_write_duration;

        if (m_last_write_duration > 500)
          ERS_DEBUG(0, "set sleep interval = " << sleep_interval << " ms");

        m_last_write_duration = 0;

        std::this_thread::sleep_for(std::chrono::milliseconds(sleep_interval));

        try
          {
            boost::shared_lock<boost::shared_mutex> lock(pbeast::receiver::DataProvider::s_data_provider_mutex);
            if (pbeast::receiver::DataProvider::s_data_provider)
              {
                auto t1 = std::chrono::steady_clock::now();

                pbeast::receiver::DataProvider::s_data_provider->write_ready_data();

                m_last_write_duration = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count();
              }
          }
        catch (ers::Issue &ex)
          {
            ers::fatal(ex);
          }
      }
  }
};

void
pbeast::receiver::DataProvider::start_write_thread(unsigned int sleep_interval)
{
  SaveDataThread command(sleep_interval);
  std::thread thrd(command);
  thrd.detach();
}

struct MonitoringThread
{
  unsigned int m_sleep_interval;

  MonitoringThread(unsigned int n) :
      m_sleep_interval(n)
  {
    ;
  }

  void
  operator()()
  {
    while (true)
      {
        sleep(m_sleep_interval);

          {
            boost::shared_lock<boost::shared_mutex> lock(pbeast::receiver::DataProvider::s_data_provider_mutex);
            if (pbeast::receiver::DataProvider::s_data_provider)
              {
                pbeast::receiver::DataProvider::get_monitoring_object().publish();
              }
          }
      }
  }
};

void
pbeast::receiver::DataProvider::start_monitoring_thread(unsigned int sleep_interval)
{
  MonitoringThread command(sleep_interval);
  std::thread thrd(command);
  thrd.detach();
}

void
pbeast::receiver::DataProvider::set_global_object()
{
  s_data_provider = get_this();
  ERS_LOG("start data provider");
}

void
pbeast::receiver::DataProvider::unset_global_object()
{
  boost::unique_lock<boost::shared_mutex> lock(pbeast::receiver::DataProvider::s_data_provider_mutex);
  s_data_provider = nullptr;
  ERS_LOG("stop data provider");
}
