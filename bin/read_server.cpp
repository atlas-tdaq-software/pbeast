#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include "boost/date_time/local_time/local_time.hpp"

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/partition.h>

#include "pbeast/def-intervals.h"
#include <pbeast/functions.h>
#include <pbeast/pbeast.hh>
#include <pbeast/series-print.h>
#include <pbeast/series-data.h>
#include <pbeast/series-idl.h>
#include <pbeast/data-type.h>

#include "application_exceptions.h"
#include "utils.h"

namespace po = boost::program_options;


// string to timestamp

static uint64_t
str2ts(std::string& in, boost::local_time::time_zone_ptr tz_ptr, const char * name)
{
  static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));

  std::string s(in);
  std::replace(s.begin(), s.end(), 'T', ' ');
  boost::posix_time::ptime t;

  try
    {
      t = boost::posix_time::time_from_string(s);
    }
  catch (std::exception& e)
    {
      std::ostringstream text;
      text << "cannot parse " << name << " = \'" << in << "\': \"" << e.what() << '\"';
      throw std::runtime_error(text.str().c_str());
    }

    // convert local time to UTC, if the time zone was provided

  if (tz_ptr)
    {
      try
        {
          boost::local_time::local_date_time lt(t.date(), t.time_of_day(), tz_ptr, boost::local_time::local_date_time::EXCEPTION_ON_ERROR);
          ERS_DEBUG(1,"Build zone\'s time \'" << in << "\' => \'" << lt.to_string() << "\' using \'" << tz_ptr->to_posix_string() << '\'');
          t = lt.utc_time();
        }
      catch (std::exception& e)
        {
          std::ostringstream text;
          text << "cannot parse " << name << " = \'" << in << "\' as zone\'s time: \"" << e.what() << '\"';
          throw std::runtime_error(text.str().c_str());
        }
    }

  return (t - epoch).total_microseconds();
}

static boost::local_time::time_zone_ptr
get_time_zone_ptr(const std::string& time_zone)
{
  const char * tz_spec_file = ::getenv("BOOST_DATE_TIME_TZ_SPEC");

  if (!tz_spec_file || !*tz_spec_file)
    {
      throw std::runtime_error("cannot read value of BOOST_DATE_TIME_TZ_SPEC environment variable to parse timezone parameter");
    }
  else
    {
      ERS_DEBUG(1, "Boost time-zone specification file is \'" << tz_spec_file << '\'');
    }

  boost::local_time::tz_database tz_db;

  try
    {
      tz_db.load_from_file(tz_spec_file);
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "cannot read Boost time-zone specification file " << tz_spec_file << ": " << ex.what();
      throw std::runtime_error(text.str().c_str());
    }

  if (time_zone == "list-regions")
    {
      for (auto & r : tz_db.region_list())
        {
          std::cout << r << std::endl;
        }

      exit(0);
    }

  if(boost::local_time::time_zone_ptr tz_ptr = tz_db.time_zone_from_region(time_zone))
    {
      return tz_ptr;
    }
  else
    {
      std::ostringstream text;
      text << "cannot find time-zone \'" << time_zone << '\'';
      throw std::runtime_error(text.str().c_str());
    }
}

namespace pbeast
{
  template<typename F, typename T>
    void
    convert(const F& from, std::vector<pbeast::SeriesVectorData<T>>& to)
    {
      unsigned int len = from.length();
      to.reserve(len);
      for (unsigned int i = 0; i < from.length(); ++i)
        {
          std::vector<T> vec;
          vec.reserve(from[i].data.length());
          for (unsigned int idx = 0; idx < from[i].data.length(); ++idx)
            {
              vec.push_back(static_cast<T>(from[i].data[idx]));
            }

          to.emplace_back(from[i].ts.created, from[i].ts.updated, vec);
        }
    }
}

#define GET_AND_PRINT(TYPE, FUNC, CPP)                                                                                                                                                                                                            \
{                                                                                                                                                                                                                                                 \
  if (interval == 0)                                                                                                                                                                                                                              \
    {                                                                                                                                                                                                                                             \
      pbeast::V64ObjectList_var eovs, upds;                                                                                                                                                                                                       \
      std::map<std::string, std::set<uint64_t>> map_eov, map_upd;                                                                                                                                                                                 \
      std::map<std::string, std::set<uint64_t>> *map_upd_ptr(print_every_update ? &map_upd : nullptr);                                                                                                                                            \
                                                                                                                                                                                                                                                  \
      if (x.m_is_array == false)                                                                                                                                                                                                                  \
        {                                                                                                                                                                                                                                         \
          pbeast::TYPE##ObjectList_var val;                                                                                                                                                                                                       \
          db->get_##FUNC##_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev_interval, next_interval, object_name.c_str(), object_name_is_regexp, fc, print_every_update, eovs, upds, val); \
          pbeast::cvt_void(eovs, map_eov, "");                                                                                                                                                                                                    \
          pbeast::cvt_void(upds, map_upd, "");                                                                                                                                                                                                    \
                                                                                                                                                                                                                                                  \
          std::map<std::string,std::vector<pbeast::SeriesData< CPP >>> data;                                                                                                                                                                      \
          pbeast::convert(val.in(), data);                                                                                                                                                                                                        \
                                                                                                                                                                                                                                                  \
          pbeast::print(std::cout, data, map_eov, map_upd_ptr, tz_ptr, !print_raw_ts, "    ");                                                                                                                                                    \
        }                                                                                                                                                                                                                                         \
      else                                                                                                                                                                                                                                        \
        {                                                                                                                                                                                                                                         \
          pbeast::TYPE##ArrayObjectList_var val;                                                                                                                                                                                                  \
          db->get_##FUNC##_array_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev_interval, next_interval, object_name.c_str(), object_name_is_regexp, fc, print_every_update, eovs, upds, val); \
          pbeast::cvt_void(eovs, map_eov, "");                                                                                                                                                                                                    \
          pbeast::cvt_void(upds, map_upd, "");                                                                                                                                                                                                    \
                                                                                                                                                                                                                                                  \
          std::map<std::string,std::vector<pbeast::SeriesVectorData< CPP >>> data;                                                                                                                                                                \
          pbeast::convert(val.in(), data);                                                                                                                                                                                                        \
                                                                                                                                                                                                                                                  \
          pbeast::print(std::cout, data, map_eov, map_upd_ptr, tz_ptr, !print_raw_ts, "    ");                                                                                                                                                    \
        }                                                                                                                                                                                                                                         \
    }                                                                                                                                                                                                                                             \
  else                                                                                                                                                                                                                                            \
    {                                                                                                                                                                                                                                             \
      pbeast::V32ObjectList_var eovs, upds;                                                                                                                                                                                                       \
      std::map<std::string, std::set<uint32_t>> map_eov, map_upd;                                                                                                                                                                                 \
                                                                                                                                                                                                                                                  \
      if(x.m_is_array == false)                                                                                                                                                                                                                   \
        {                                                                                                                                                                                                                                         \
          pbeast::TYPE##DownSampleObjectList_var val;                                                                                                                                                                                             \
          db->get_downsample_##FUNC##_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev_interval, next_interval, interval, fill_gaps, object_name.c_str(), object_name_is_regexp, fc, print_every_update, eovs, upds, val); \
          pbeast::cvt_void(eovs, map_eov, "");                                                                                                                                                                                                    \
          pbeast::cvt_void(upds, map_upd, "");                                                                                                                                                                                                    \
                                                                                                                                                                                                                                                  \
          std::map<std::string,std::vector<pbeast::DownsampledSeriesData< CPP >>> data;                                                                                                                                                           \
          pbeast::convert(val.in(), data);                                                                                                                                                                                                        \
                                                                                                                                                                                                                                                  \
          pbeast::print(std::cout, data, map_eov, map_upd, tz_ptr, !print_raw_ts, "    ", fc_ptr);                                                                                                                                                \
        }                                                                                                                                                                                                                                         \
      else                                                                                                                                                                                                                                        \
        {                                                                                                                                                                                                                                         \
          pbeast::TYPE##DownSampleArrayObjectList_var val;                                                                                                                                                                                        \
          db->get_downsample_##FUNC##_array_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev_interval, next_interval, interval, fill_gaps, object_name.c_str(), object_name_is_regexp, fc, print_every_update, eovs, upds, val); \
          pbeast::cvt_void(eovs, map_eov, "");                                                                                                                                                                                                    \
          pbeast::cvt_void(upds, map_upd, "");                                                                                                                                                                                                    \
                                                                                                                                                                                                                                                  \
          std::map<std::string,std::vector<pbeast::DownsampledSeriesVectorData< CPP >>> data;                                                                                                                                                     \
          pbeast::convert(val.in(), data);                                                                                                                                                                                                        \
                                                                                                                                                                                                                                                  \
          pbeast::print(std::cout, data, map_eov, map_upd, tz_ptr, !print_raw_ts, "    ", fc_ptr);                                                                                                                                                \
        }                                                                                                                                                                                                                                         \
    }                                                                                                                                                                                                                                             \
}


int
main(int argc, char ** argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (ers::Issue & ex)
    {
      ers::fatal(ers::Message(ERS_HERE, ex));
      return 1;
    }

  po::options_description desc(
      "Data reader from pbeast server over CORBA.\n\nDeprecated. Use pbeast_read instead.\n\n"
      "For timestamps use ISO 8601 date-time format, i.e. YYYY-MM-DD HH:MM:SS[,...]\n"
      "For available timezones run with -z list-regions\n\n"
      "Available options are"
  );

  std::string server_name;
  std::string ipc_partition_name("initial");
  std::string partition_name;
  std::string class_name;
  std::string attribute_name;
  std::string object_name;
  bool object_name_is_regexp = false;
  boost::local_time::time_zone_ptr tz_ptr;
  uint64_t since(pbeast::get_def_since());
  uint64_t until(pbeast::get_def_until());
  uint64_t prev_interval(pbeast::def_lookup_prev_const);
  uint64_t next_interval(pbeast::def_lookup_next_const);
  uint32_t interval(0);
  bool print_every_update = false;
  bool print_raw_ts = false;
  bool print_raw_date_time = false;
  bool list_updated_objects = false;
  bool list_partitions = false;
  bool list_classes = false;
  bool list_attributes = false;
  pbeast::FunctionConfig function_config;
  pbeast::FunctionConfig * fc_ptr = nullptr;
  ::CORBA::Long fill_gaps;


  // parse command line
  try
    {
      std::string since_tmp;
      std::string until_tmp;
      std::string time_zone;
      std::string object_names_regexp;
      std::vector<std::string> functions;
      std::string fill_gaps_tmp("near");
      std::streamsize precision(std::cout.precision());

      std::string functions_desc = std::string("Space-separated ") + pbeast::FunctionConfig::make_description_of_functions();

      desc.add_options()
      (
          "server-name,n",
          po::value<std::string>(&server_name)->required(),
          "Name of pbeast server"
      )
      (
          "ipc-partition-name,P",
          po::value<std::string>(&ipc_partition_name)->default_value(ipc_partition_name),
          "Name of IPC partition (i.e. where the server is running)"
      )
      (
          "partition-name,p",
          po::value<std::string>(&partition_name),
          "Name of partition"
      )
      (
          "class-name,c",
          po::value<std::string>(&class_name),
          "Name of class"
      )
      (
          "attribute-name,a",
          po::value<std::string>(&attribute_name),
          "Name of attribute or path in case of nested type"
      )
      (
          "object-name,o",
          po::value<std::string>(&object_name),
          "Name of object"
      )
      (
          "object-name-regexp,O",
          po::value<std::string>(&object_names_regexp),
          "Regular expression for names of objects"
      )
      (
          "list-updated-objects,l",
          "List names of updated objects"
      )
      (
          "list-partitions,L",
          "List partitions"
      )
      (
          "list-classes,C",
          "List classes"
      )
      (
          "list-attributes,A",
          "List attributes"
      )
      (
          "since,s",
          po::value<std::string>(&since_tmp)->default_value(since_tmp),
          "Show data since given timestamp"
      )
      (
          "until,t",
          po::value<std::string>(&until_tmp)->default_value(until_tmp),
          "Show data until given timestamp"
      )
      (
          "since-raw-timestamp,S",
          po::value<uint64_t>(&since)->default_value(since),
          "Show data since given raw timestamp (number of micro-seconds since Epoch)"
      )
      (
          "until-raw-timestamp,T",
          po::value<uint64_t>(&until)->default_value(until),
          "Show data until given raw timestamp (number of micro-seconds since Epoch)"
      )
      (
          "lookup-previous-value-time-lapse,R",
          po::value<uint64_t>(&prev_interval)->default_value(prev_interval),
          "Time lapse to lookup closest previous data point outside query time interval (in microseconds)"
      )
      (
          "lookup-next-value-time-lapse,N",
          po::value<uint64_t>(&next_interval)->default_value(next_interval),
          "Time lapse to lookup closest next data point outside query time interval (in microseconds)"
      )
      (
          "downsample-interval,i",
          po::value<uint32_t>(&interval),
          "Downsampling interval (if not set or zero, print raw data)"
      )
      (
          "fill-gaps,g",
          po::value<std::string>(&fill_gaps_tmp)->default_value(fill_gaps_tmp),
          "When downsample, approximate missing values: \"none\", \"near\" or \"all\""
      )
      (
          "function,f",
          po::value<std::vector<std::string> >(&functions)->multitoken(),
          functions_desc.c_str()
      )
      (
          "keep-aggregated-data,K",
          "Keep original data after aggregation"
      )
      (
          "time-zone,z",
          po::value<std::string>(&time_zone)->default_value(time_zone),
          "Name of time zone (use \"-z list-regions\" to see known time zone names)"
      )
      (
          "print-raw-timestamp,w",
          "Print raw timestamp (number of micro-seconds since Epoch) instead of formatted one"
      )
      (
          "print-raw-date-time,W",
          "Print date and time attributes in raw format (number of micro-seconds since Epoch) instead of formatted one"
      )
      (
          "precision,V",
          po::value<std::streamsize>(&precision)->default_value(precision),
          "Set floating point data output precision"
      )
      (
          "print-value-per-update,u",
          "Print value per IS object update even if the value remained unchanged"
      )
      (
          "help,h",
          "Print help message"
      );

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);

      if (vm.count("object-name-regexp"))
        object_name_is_regexp = true;

      if (vm.count("list-updated-objects"))
        list_updated_objects = true;

      if (vm.count("list-partitions"))
        list_partitions = true;
      else if (partition_name.empty())
        throw std::runtime_error("partition name is required parameter");

      if (vm.count("list-classes"))
        list_classes = true;
      else if (!list_partitions && class_name.empty())
        throw std::runtime_error("class name is required parameter");

      if (vm.count("list-attributes"))
        list_attributes = true;
      else if (!list_partitions && !list_classes && attribute_name.empty())
        throw std::runtime_error("attribute name is required parameter");

      if (vm.count("print-raw-timestamp"))
        print_raw_ts = true;

      if (vm.count("print-raw-date-time"))
        print_raw_date_time = true;

      if (vm.count("print-value-per-update"))
        print_every_update = true;

      if (vm["until-raw-timestamp"].defaulted() == false && vm["until"].defaulted() == false)
        throw std::runtime_error("both \"until\" and \"until-raw-timestamp\" parameters are defined");

      if (vm["since-raw-timestamp"].defaulted() == false && vm["since"].defaulted() == false)
        throw std::runtime_error("both \"since\" and \"since-raw-timestamp\" parameters are defined");

      if (!time_zone.empty())
        tz_ptr = get_time_zone_ptr(time_zone);

      if (!since_tmp.empty())
        since = str2ts(since_tmp, tz_ptr, "since");

      if (!until_tmp.empty())
        until = str2ts(until_tmp, tz_ptr, "until");

      if (precision != std::cout.precision())
        std::cout.precision(precision);

      if (!object_names_regexp.empty())
        if (object_names_regexp != ".*")
          object_name = object_names_regexp;

      if (functions.empty() == false)
        {
          function_config.construct(functions, interval);
          fc_ptr = &function_config;
        }

      if (vm.count("keep-aggregated-data"))
        {
          if(function_config.no_aggregations())
            {
              throw std::runtime_error("cannot use \"keep-aggregated-data\" parameter without \"aggregate\" functions");
            }

          function_config.m_keep_data = true;
        }

      fill_gaps = static_cast<CORBA::Long>(pbeast::FillGaps::mk(fill_gaps_tmp));
    }
  catch (std::exception& ex)
    {
      ers::fatal( daq::pbeast::CommandLineError( ERS_HERE, ex.what() ) );
      return 1;
    }

  try
    {
      std::string fc_string = function_config.to_string();
      const char * fc = fc_string.c_str();

      IPCPartition p(ipc_partition_name);

      pbeast::server_var db = p.lookup<pbeast::server>(server_name);

      if(list_partitions)
        {
          pbeast::StringArray_var partitions;
          db->get_partitions(partitions);

          for(unsigned int idx = 0; idx < partitions->length(); idx++)
            {
              std::cout << partitions[idx] << '\n';
            }

          return 0;
        }

      if (list_classes)
        {
          pbeast::StringArray_var classes;
          db->get_classes(partition_name.c_str(), classes);

          for(unsigned int idx = 0; idx < classes->length(); idx++)
            {
              std::cout << classes[idx] << '\n';
            }

          return 0;
        }

      if (list_attributes)
        {
          pbeast::StringDataPointArrayList_var attributes;
          db->get_attributes(partition_name.c_str(), class_name.c_str(), attributes);
          std::vector<pbeast::SeriesVectorData<std::string>> data;
          pbeast::convert(attributes.in(), data);
          std::set<uint64_t> empty_eov;

          pbeast::print(std::cout, data, empty_eov, nullptr, tz_ptr, !print_raw_ts, "    ");

          return 0;
        }

      if (list_updated_objects)
        {
          pbeast::StringArray_var objects;
          db->get_updated_objects(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), since, until, object_name.c_str(), objects);

          for(unsigned int idx = 0; idx < objects->length(); idx++)
            {
              std::cout << objects[idx] << '\n';
            }

          return 0;
        }


      pbeast::AttributeTypeList_var type_var;

      db->get_attribute_type(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), type_var);

      // check length of type and is-array information

      const unsigned long len(type_var->length());

      if(len == 0)
        {
          throw std::runtime_error("cannot find data type attribute information");
        }


      // split search time interval on several intervals in accordance with attribute description

      struct AttrInfo
      {
        uint64_t m_since, m_until;
        const char * m_type;
        bool m_is_array;
        AttrInfo(uint64_t s, uint64_t u, const char * t, bool a) : m_since(s), m_until(u), m_type(t), m_is_array(a) {;}
      };

      std::vector<AttrInfo> info;

      if(len == 1)
        {
          info.emplace_back(since, until, type_var[0].type, type_var[0].is_array);
        }
      else
        {
          for(unsigned int idx = 0; idx < len; ++idx)
            {
              //
              // s, u - map current knowledges about attribute info validity on full time-line
              //
              //        [x1]...[y1]   [x2...y2]    [x3.......y3]
              //
              //  [s1]------------[u1][s2]-----[u2][s3]-------------------------[u3]
              //

              uint64_t s = (idx == 0 ? pbeast::min_ts_const : type_var[idx].ts.created);
              uint64_t u = (idx == (len-1) ? pbeast::max_ts_const : type_var[idx+1].ts.created-1);

              //
              // since, until are shown as x, y
              //
              //             [s].............[u]
              //
              //  (A)    [x]--------[y]
              //  (B)    [x]------------------------[y]
              //  (C)            [x]---[y]
              //  (D)            [x]----------------[y]
              //

              if(until >= s)
                {
                  if(since <= s)
                    {
                      // case (A)
                      if(until <= u)
                        {
                          info.emplace_back(s, until, type_var[idx].type, type_var[idx].is_array);
                        }
                      // case (B)
                      else
                        {
                          info.emplace_back(s, u, type_var[idx].type, type_var[idx].is_array);
                        }
                    }
                  else if(since <= u)
                    {
                      // case (C)
                      if(until <= u)
                        {
                          info.emplace_back(since, until, type_var[idx].type, type_var[idx].is_array);
                        }
                      // case (D)
                      else
                        {
                          info.emplace_back(since, u, type_var[idx].type, type_var[idx].is_array);
                        }
                    }
                }
            }
        }


      for(auto & x : info)
        {
          pbeast::print_tss(std::cout, x.m_since, x.m_until, tz_ptr, !print_raw_ts, "(INFO) - ");
          std::cout << " attribute type: " << x.m_type << ", is array: " << std::boolalpha << x.m_is_array << std::endl;

          switch(pbeast::str2type(x.m_type))
            {
              case pbeast::BOOL:    GET_AND_PRINT(Boolean, boolean, bool);      break;
              case pbeast::S8:      GET_AND_PRINT(S8, s8, int8_t);              break;
              case pbeast::U8:      GET_AND_PRINT(U8, u8, uint8_t);             break;
              case pbeast::S16:     GET_AND_PRINT(S16, s16, int16_t);           break;
              case pbeast::U16:     GET_AND_PRINT(U16, u16, uint16_t);          break;
              case pbeast::ENUM:
              case pbeast::S32:     GET_AND_PRINT(S32, s32, int32_t);           break;
              case pbeast::U32:     GET_AND_PRINT(U32, u32, uint32_t);          break;
              case pbeast::S64:     GET_AND_PRINT(S64, s64, int64_t);           break;
              case pbeast::U64:     GET_AND_PRINT(U64, u64, uint64_t);          break;
              case pbeast::FLOAT:   GET_AND_PRINT(Float, float, float);         break;
              case pbeast::DOUBLE:  GET_AND_PRINT(Double, double, double);      break;
              case pbeast::STRING:  GET_AND_PRINT(String, string, std::string); break;
              case pbeast::TIME:    if(print_raw_date_time) { GET_AND_PRINT(U64, u64, uint64_t); } else { GET_AND_PRINT(U64, u64, pbeast::Time); } break;
              case pbeast::DATE:    if(print_raw_date_time) { GET_AND_PRINT(U32, u32, uint32_t); } else { GET_AND_PRINT(U32, u32, pbeast::Date); } break;
              case pbeast::OBJECT:
                {
                  std::ostringstream text;
                  text << "cannot print object of nested type " << x.m_type << "; provide path to an attribute of this type";
                  throw std::runtime_error(text.str().c_str());
                }
              default:
                {
                  std::ostringstream text;
                  text << "data type " << x.m_type << " is not supported";
                  throw std::runtime_error(text.str().c_str());
                }
            }
        }
    }
  catch ( pbeast::NotFound& ex )
    {
      ers::fatal(daq::pbeast::Error(ERS_HERE, "not found", ex.text));
      return 1;
    }
  catch ( pbeast::Failure& ex )
    {
      ers::fatal(daq::pbeast::Error(ERS_HERE, "failure", ex.text));
      return 1;
    }
  catch ( pbeast::BadRequest& ex )
    {
      ers::fatal(daq::pbeast::Error(ERS_HERE, "error", ex.text));
      return 1;
    }
  catch ( daq::ipc::Exception & ex )
    {
      ers::fatal(ex);
      return 1;
    }
  catch ( CORBA::SystemException & ex )
    {
      ers::fatal(daq::ipc::CorbaSystemException(ERS_HERE, &ex));
      return 1;
    }
  catch ( std::exception & ex )
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE));
      return 1;
    }

  return 0;
}
