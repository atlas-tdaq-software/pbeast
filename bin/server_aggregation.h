#ifndef PBEAST_SERVER_AGGREGATION_H
#define PBEAST_SERVER_AGGREGATION_H

#include <map>
#include <set>
#include <sstream>

#include "pbeast/pbeast.hh"
#include "pbeast/series-data.h"

namespace pbeast
{
  template<class T>
    void
    get_query_interval(const std::map<std::string, std::set<T>>& data, uint32_t& since, uint32_t& until)
    {
      since = 0xffffffff;
      until = 0;

      for (const auto& x : data)
        {
          if (!x.second.empty())
            {
              const uint32_t first = (*x.second.cbegin()).get_ts();

              if (first < since)
                since = first;

              const uint32_t last = (*x.second.crbegin()).get_ts();

              if (last > until)
                until = last;
            }
        }

      if (until == 0)
        since = 0;
    }


  template<class T>
  std::set<T> *
  check_and_get(const std::string& s, std::map<std::string, std::set<T>>& data)
  {
    auto ret = data.insert( std::pair<std::string,std::set<T>>(s,std::set<T>()) );

    if(ret.second == false)
      {
        std::ostringstream text;
        text << "bad aggregation request parameter \"" << s << "\" (requested data already exist)";
        throw pbeast::BadRequest(text.str().c_str());
      }
    else
      {
        return &((*ret.first).second);
      }
  }


  template<class T>
    std::set<T> *
    get_function_ptr(const pbeast::FunctionConfig& fc, long type, std::map<std::string, std::set<T>>& data)
      {
        for (const auto& x : fc.m_data_names)
          if (x.second == type)
            return check_and_get(x.first, data);

        return nullptr;
      }


  static constexpr uint32_t s_invalid_idx = 0xffffffff;

  // FIXME: replace by standard???
  template<typename T>
    struct AggregatedSumType
    {
      typedef std::vector<double> aggregated_sum_t;
    };

  template<>
    struct AggregatedSumType<std::string>
    {
      typedef std::vector<std::map<std::string,uint32_t>> aggregated_sum_t;
    };


  static std::set<uint32_t> empty32;

  struct AggregatedDataBase
  {
    AggregatedDataBase(uint32_t interval, uint32_t first, uint32_t len) :
        m_interval(interval), m_first(first), m_len(len), m_count(0)
    {
      ;
    }

    uint32_t
    idx2ts(uint32_t idx)
    {
      return (m_first + m_interval * idx);
    }

    uint32_t
    ts2idx(uint32_t ts)
    {
      return (ts - m_first) / m_interval;
    }

    /**
     * Adjust start index, if there is more than one started-before index
     */

    uint32_t
    ajust_start_idx(::CORBA::ULongLong since)
    {
      uint32_t start_idx = 0;

      if (m_len > 2)
        {
          const uint32_t start_ts = pbeast::ts2secs(since);
          while (idx2ts(start_idx) <= start_ts && idx2ts(start_idx + 1) <= start_ts && start_idx < m_len)
            start_idx++;
        }

      return start_idx;
    }


    /**
     * Adjust end index, if there is more than one ended-after index
     */

    uint32_t
    ajust_end_idx(::CORBA::ULongLong until)
    {
      uint32_t end_idx = m_len;

      if (m_len > 2)
        {
          const uint32_t end_ts = pbeast::ts2secs(until);
          while (idx2ts(end_idx) >= end_ts && idx2ts(end_idx - 1) >= end_ts && end_idx > 1)
            end_idx--;
        }

      return end_idx;
    }


    /**
     * Calculate index for interpolated data, if it is limited by EoV
     */

    uint32_t
    idx_by_eov(const std::set<uint32_t>& obj_eov, uint32_t idx, uint32_t last_idx)
    {
      std::set<uint32_t>::const_iterator it = obj_eov.upper_bound(idx2ts(last_idx));

      if (it != obj_eov.end())
        {
          const uint32_t eov_idx = ts2idx(*it);
          if (eov_idx < idx)
            return eov_idx;
        }

      return idx;
    }

    uint32_t m_interval;
    uint32_t m_first;
    uint32_t m_len;
    uint32_t m_count;
  };

  template<class T>
    struct AggregatedPrimitiveData : public AggregatedDataBase
    {
      AggregatedPrimitiveData(uint32_t interval, uint32_t first, uint32_t len) :
        AggregatedDataBase(interval, first, len),
        m_min_values(len), m_max_values(len), m_sum_values(len), m_num_values(len, 0)
      {
        ;
      }

      void
      add_interpolated(const pbeast::DownsampledSeriesData<T>& v, const pbeast::DownsampledSeriesData<T>& last, uint32_t idx, uint32_t next)
      {
        double average = last.data().get_value() + (v.data().get_value() - last.data().get_value()) * static_cast<double>(idx - last.get_ts()) / static_cast<double>(next - last.get_ts());
        add(last.data().get_min_value(), average, last.data().get_max_value(), idx);
      }

      void
      process(const std::map<std::string, std::set<pbeast::DownsampledSeriesData<T>>>& data, std::map<std::string, std::set<uint32_t>> eov)
        {
          for (const auto& x : data)
            {
              std::map<std::string, std::set<uint32_t>>::const_iterator obj_eov_it = pbeast::find(eov,x.first);
              const std::set<uint32_t>& obj_eov(obj_eov_it != eov.end() ? obj_eov_it->second : empty32);

              pbeast::DownsampledSeriesData<T> last(s_invalid_idx);
              uint32_t last_idx(s_invalid_idx);

              for (const auto& v : x.second)
                {
                  const unsigned long idx = (v.get_ts() - m_first) / m_interval;

                  if (last_idx != idx && idx != s_invalid_idx) // 'idx != s_invalid_idx' check is to avoid compiler warning
                    {
                      last_idx = idx;

                      if (last.get_ts() != s_invalid_idx && ((idx - last.get_ts()) != 1))
                        {
                          const uint32_t idx_eov_limit = idx_by_eov(obj_eov, idx, last.get_ts());
                          for (uint32_t i = last.get_ts()+1; i < idx_eov_limit; ++i)
                            add_interpolated(v, last, i, idx);
                        }

                      add(v.data(), idx);
                      last = v;
                      last.set_ts(idx);
                    }
                }
            }
        }

      void
      put_sum(std::set<pbeast::DownsampledSeriesData<T>>& to, uint32_t ts, uint32_t idx)
      {
        to.insert(pbeast::DownsampledSeriesData<T>(ts, static_cast<T>(m_sum_values[idx]), m_sum_values[idx], static_cast<T>(m_sum_values[idx])));
      }

      void
      put_avg(std::set<pbeast::DownsampledSeriesData<T>>& to, uint32_t ts, uint32_t idx)
      {
        double average = m_sum_values[idx] / m_num_values[idx];
        to.insert(pbeast::DownsampledSeriesData<T>(ts, static_cast<T>(average), average, static_cast<T>(average)));
      }

      template<class K>
      void
      store(const K& functions, std::map<std::string, std::set<pbeast::DownsampledSeriesData<T>>>& data, ::CORBA::ULongLong since, ::CORBA::ULongLong until)
        {
          if(m_count)
            {
              std::set<pbeast::DownsampledSeriesData<T>> * min_ptr = get_function_ptr<pbeast::DownsampledSeriesData<T>>(functions, pbeast::AggregationMin, data);
              std::set<pbeast::DownsampledSeriesData<T>> * max_ptr = get_function_ptr<pbeast::DownsampledSeriesData<T>>(functions, pbeast::AggregationMax, data);
              std::set<pbeast::DownsampledSeriesData<T>> * sum_ptr = get_function_ptr<pbeast::DownsampledSeriesData<T>>(functions, pbeast::AggregationSum, data);
              std::set<pbeast::DownsampledSeriesData<T>> * avg_ptr = get_function_ptr<pbeast::DownsampledSeriesData<T>>(functions, pbeast::AggregationAverage, data);

              const uint32_t end_idx = ajust_end_idx(until);
              for(uint32_t idx = ajust_start_idx(since); idx < end_idx; ++idx)
                {
                  if(m_num_values[idx] != 0)
                    {
                      uint32_t ts = idx2ts(idx);

                      if (min_ptr)
                        min_ptr->insert(pbeast::DownsampledSeriesData<T>(ts, m_min_values[idx], m_min_values[idx], m_min_values[idx]));

                      if (max_ptr)
                        max_ptr->insert(pbeast::DownsampledSeriesData<T>(ts, m_max_values[idx], m_max_values[idx], m_max_values[idx]));

                      if (sum_ptr)
                        put_sum(*sum_ptr, ts, idx);

                      if (avg_ptr)
                        put_avg(*avg_ptr, ts, idx);
                    }
                }
            }
        }

      void
      add(const T& min_value, typename pbeast::AverageType<T>::average_t average_value, const T& max_value, uint32_t idx)
      {
        m_num_values[idx]++;
        if((m_num_values[idx] == 1 || min_value < m_min_values[idx])) m_min_values[idx] = min_value;
        if((m_num_values[idx] == 1 || max_value > m_max_values[idx])) m_max_values[idx] = max_value;
        add2sum(average_value, idx);
        m_count++;
      }

      void
      add2sum(const double& value, uint32_t idx)
      {
        m_sum_values[idx] += value;
      }

      void
      add2sum(const std::string& value, uint32_t idx)
      {
        m_sum_values[idx][value]++;
      }

      void
      add(const pbeast::DownsampleValue<T>& v, uint32_t idx)
      {
        add(v.get_min_value(), v.get_value(), v.get_max_value(), idx);
      }

      std::vector<T> m_min_values, m_max_values;
      typename AggregatedSumType<T>::aggregated_sum_t m_sum_values;
      std::vector<uint32_t> m_num_values;
    };

  template<>
    inline void
    AggregatedPrimitiveData<std::string>::add_interpolated(const pbeast::DownsampledSeriesData<std::string>& v, const pbeast::DownsampledSeriesData<std::string>& last, uint32_t idx, uint32_t next)
    {
      add(last.data().get_min_value(), last.data().get_value(), last.data().get_max_value(), idx);
    }


  template<>
    inline void
    AggregatedPrimitiveData<std::string>::put_sum(std::set<pbeast::DownsampledSeriesData<std::string>>& to, uint32_t ts, uint32_t idx)
    {
      const std::string * ptr = nullptr;

      uint32_t max = 0;

      for (const auto& x : m_sum_values[idx])
        {
          if (x.second > max)
            {
              max = x.second;
              ptr = &x.first;
            }
        }

      if (ptr)
        to.insert(pbeast::DownsampledSeriesData<std::string>(ts, *ptr, *ptr, *ptr));
    }

  template<>
    inline void
    AggregatedPrimitiveData<std::string>::put_avg(std::set<pbeast::DownsampledSeriesData<std::string>>& to, uint32_t ts, uint32_t idx)
    {
      put_sum(to, ts, idx);
    }

  template<class T>
    struct AggregatedVectorData : public AggregatedDataBase
    {
      AggregatedVectorData(uint32_t interval, uint32_t first, uint32_t len) :
        AggregatedDataBase(interval, first, len),
        m_min_values(len), m_max_values(len), m_sum_values(len), m_num_values(len)
      {
        ;
      }

      void
      add_interpolated(const pbeast::DownsampledSeriesVectorData<T>& v, const pbeast::DownsampledSeriesVectorData<T>& last, uint32_t idx, uint32_t next)
      {
        const uint32_t size = std::min<uint32_t>(last.get_size(), v.data().size());

        for (uint32_t i = 0; i < size; ++i)
          {
            double average = last.data(i).get_value() + (v.data()[i].get_value() - last.data(i).get_value()) * static_cast<double>(idx - last.get_ts()) / static_cast<double>(next - last.get_ts());
            add(last.data(i).get_min_value(), average, last.data(i).get_max_value(), idx, i);
          }

        if (size < last.get_size())
          for (uint32_t i = size; i < last.get_size(); ++i)
            add(last.data(i).get_min_value(), last.data(i).get_value(), last.data(i).get_max_value(), idx, i);
      }

      void
      process(const std::map<std::string, std::set<pbeast::DownsampledSeriesVectorData<T>>>& data, std::map<std::string, std::set<uint32_t>> eov)
        {
          for (const auto& x : data)
            {
              std::map<std::string, std::set<uint32_t>>::const_iterator obj_eov_it = pbeast::find(eov,x.first);
              const std::set<uint32_t>& obj_eov(obj_eov_it != eov.end() ? obj_eov_it->second : empty32);

              pbeast::DownsampledSeriesVectorData<T> last(s_invalid_idx);
              for (const auto& v : x.second)
                {
                  const unsigned long idx = (v.get_ts() - m_first) / m_interval;

                  if (last.get_ts() != s_invalid_idx && ((idx - last.get_ts()) != 1))
                    {
                      const uint32_t idx_eov_limit = idx_by_eov(obj_eov, idx, last.get_ts());
                      for (uint32_t i = last.get_ts()+1; i < idx_eov_limit; ++i)
                        add_interpolated(v, last, i, idx);
                    }

                  add(v.data(), idx);
                  last = v;
                  last.set_ts(idx);
                }
            }
        }

      template<class K>
      std::vector<pbeast::DownsampleValue<T>>
      mk_data(const std::vector<K>& data, uint32_t idx)
        {
          std::vector<pbeast::DownsampleValue<T>> out;
          out.reserve(data.size());

          for (uint32_t i = 0; i < data.size(); ++i)
            {
              if (m_num_values[idx][i] == 0)
                break;
              out.emplace_back(static_cast<T>(data[i]), data[i], static_cast<T>(data[i]));
            }

          return out;
        }

      void
      put_sum(std::set<pbeast::DownsampledSeriesVectorData<T>>& to, uint32_t ts, uint32_t idx)
        {
          to.insert(pbeast::DownsampledSeriesVectorData<T>(ts, mk_data(m_sum_values[idx], idx)));
        }

      void
      put_avg(std::set<pbeast::DownsampledSeriesVectorData<T>>& to, uint32_t ts, uint32_t idx)
        {
          auto average_values = m_sum_values[idx];

          for (uint32_t i = 0; i < average_values.size(); ++i)
            if (m_num_values[idx][i])
              average_values[i] = average_values[i] / m_num_values[idx][i];

          to.insert(pbeast::DownsampledSeriesVectorData<T>(ts, mk_data(average_values, idx)));
        }

      template<class K>
      void
      store(const K& functions, std::map<std::string, std::set<pbeast::DownsampledSeriesVectorData<T>>>& data, ::CORBA::ULongLong since, ::CORBA::ULongLong until)
        {
          if(m_count)
            {
              std::set<pbeast::DownsampledSeriesVectorData<T>> * min_ptr = get_function_ptr<pbeast::DownsampledSeriesVectorData<T>>(functions, pbeast::AggregationMin, data);
              std::set<pbeast::DownsampledSeriesVectorData<T>> * max_ptr = get_function_ptr<pbeast::DownsampledSeriesVectorData<T>>(functions, pbeast::AggregationMax, data);
              std::set<pbeast::DownsampledSeriesVectorData<T>> * sum_ptr = get_function_ptr<pbeast::DownsampledSeriesVectorData<T>>(functions, pbeast::AggregationSum, data);
              std::set<pbeast::DownsampledSeriesVectorData<T>> * avg_ptr = get_function_ptr<pbeast::DownsampledSeriesVectorData<T>>(functions, pbeast::AggregationAverage, data);

              const uint32_t end_idx = ajust_end_idx(until);
              for(uint32_t idx = ajust_start_idx(since); idx < end_idx; ++idx)
                {
                  if(m_num_values[idx].empty() == false)
                    {
                      uint32_t ts = idx2ts(idx);

                      if (min_ptr)
                        min_ptr->insert(pbeast::DownsampledSeriesVectorData<T>(ts, mk_data(m_min_values[idx], idx)));

                      if (max_ptr)
                        max_ptr->insert(pbeast::DownsampledSeriesVectorData<T>(ts, mk_data(m_max_values[idx], idx)));

                      if (sum_ptr)
                        put_sum(*sum_ptr, ts, idx);

                      if (avg_ptr)
                        put_avg(*avg_ptr, ts, idx);
                    }
                }
            }
        }

      void
      add(const T& min_value, typename pbeast::AverageType<T>::average_t average_value, const T& max_value, uint32_t idx, uint32_t pos)
      {
        expand(idx, pos+1);

        m_num_values[idx][pos]++;
        if((m_num_values[idx][pos] == 1 || min_value < m_min_values[idx][pos])) m_min_values[idx][pos] = min_value;
        if((m_num_values[idx][pos] == 1 || max_value > m_max_values[idx][pos])) m_max_values[idx][pos] = max_value;
        add2sum(average_value, idx, pos);
        m_count++;
      }

      void
      add2sum(const double& value, uint32_t idx, uint32_t pos)
      {
        m_sum_values[idx][pos] += value;
      }

      void
      add2sum(const std::string& value, uint32_t idx, uint32_t pos)
      {
        m_sum_values[idx][pos][value]++;
      }

      void
      add(const std::vector<pbeast::DownsampleValue<T>>& v, uint32_t idx)
      {
        for(uint32_t i = 0; i < v.size(); ++i)
          add(v[i].get_min_value(), v[i].get_value(), v[i].get_max_value(), idx, i);
      }

      void
      expand(uint32_t idx, uint32_t size)
      {
        if(m_num_values[idx].size() < size)
          {
            m_num_values[idx].resize(size);
            m_min_values[idx].resize(size);
            m_max_values[idx].resize(size);
            m_sum_values[idx].resize(size);
          }
      }

      // vector[time-interval-index][data-index]
      std::vector<std::vector<T>> m_min_values, m_max_values;
      std::vector<typename AggregatedSumType<T>::aggregated_sum_t> m_sum_values;
      std::vector<std::vector<uint32_t>> m_num_values;
    };

  template<>
    inline void
    AggregatedVectorData<std::string>::add_interpolated(const pbeast::DownsampledSeriesVectorData<std::string>& v, const pbeast::DownsampledSeriesVectorData<std::string>& last, uint32_t idx, uint32_t next)
    {
      for (uint32_t i = 0; i < last.get_size(); ++i)
        add(last.data(i).get_min_value(), last.data(i).get_value(), last.data(i).get_max_value(), idx, i);
    }


  template<>
    inline void
    AggregatedVectorData<std::string>::put_sum(std::set<pbeast::DownsampledSeriesVectorData<std::string>>& to, uint32_t ts, uint32_t idx)
    {
      std::vector<std::string> data;
      data.reserve(m_sum_values[idx].size());

      for (const auto& x : m_sum_values[idx])
        {
          const std::string * ptr = nullptr;
          uint32_t max = 0;

          for(const auto& i : x)
            {
              if (i.second > max)
                {
                  max = i.second;
                  ptr = &i.first;
                }
            }

          if (ptr)
            data.push_back(*ptr);
        }

      if (data.empty() == false)
        to.insert(pbeast::DownsampledSeriesVectorData<std::string>(ts, mk_data(data, idx)));
    }

  template<>
    inline void
    AggregatedVectorData<std::string>::put_avg(std::set<pbeast::DownsampledSeriesVectorData<std::string>>& to, uint32_t ts, uint32_t idx)
    {
      put_sum(to, ts, idx);
    }
}

#endif
