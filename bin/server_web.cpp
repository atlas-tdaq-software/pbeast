#include <chrono>
#include <iomanip>
#include <regex>
#include <algorithm>

#include <boost/algorithm/string.hpp>
#include <boost/spirit/include/karma.hpp>

#include <libmicrohttpd/microhttpd.h>

#include <pbeast/def-intervals.h>
#include <pbeast/downsample-algo.h>
#include <pbeast/downsample-data.h>
#include <pbeast/file-response.h>
#include <pbeast/output-data-file.h>
#include <pbeast/series-print.h>
#include <pbeast/repository.h>
#include <pbeast/json-serialize.h>

#include "application_exceptions.h"
#include "server_monitoring.h"
#include "server.h"
#include "utils.h"

#include "../src/query-internal.h"

static const std::string s_rest_autocomplete_series("autocompleteSeries");
static const std::string s_rest_get_partition_names("getPartitionNames");
static const std::string s_rest_get_class_names("getClassNames");
static const std::string s_rest_get_attribute_names("getAttributeNames");
static const std::string s_rest_get_object_names("getObjectNames");
static const std::string s_rest_get_schema("getSchema");
static const std::string s_rest_read_series("readSeries");
static const std::string s_rest_get_annotations("getAnnotations");
static const std::string s_rest_get_data("getData");
static const std::string s_rest_health("health");


static MHD_Result
add_key(void *cls, enum MHD_ValueKind /*kind*/, const char *key, const char *value)
{
  std::map<std::string, std::string> * method_params = reinterpret_cast<std::map<std::string, std::string> *>(cls);
  if (key && *key)
    (*method_params)[key] = (value ? value : "");
  return MHD_YES;
}

static void
put_encoded(std::ostringstream& out, const std::string& s)
{
  for (auto & c : s)
    {
      if (c == '&')
        out << "&amp;";
      else if (c == '<')
        out << "&lt;";
      else if (c == '>')
        out << "&gt;";
      else
        out << c;
    }
}


struct MultiDataFileResponseServer : pbeast::MultiDataFileResponse
{
  MultiDataFileResponseServer() : MultiDataFileResponse(std::ios_base::out) { ; }

  // used by server
  static void
  free(void *cls)
  {
    MultiDataFileResponseServer * th = reinterpret_cast<MultiDataFileResponseServer *>(cls);
    delete th;
  }

  static char
  empty2char(bool is_empty)
  {
    return (is_empty ? '0' : '1');
  }

  // used by server
  static ssize_t
  put(void *cls, uint64_t pos, char *buf, size_t max)
  {
    MultiDataFileResponseServer * th = reinterpret_cast<MultiDataFileResponseServer *>(cls);

    if (th->m_ss.empty())
      {
        th->m_ss.emplace_back("");  // reserved for "first"
        th->m_ss.emplace_back("");  // reserved for "header"

        std::stringstream first, header;

        const std::string eov_str(th->m_eov.m_data.str());
        const std::string upd_str(th->m_upd.m_data.str());

        header << th->m_data.size() << ' ' << empty2char(eov_str.empty()) << ' ' << empty2char(upd_str.empty()) << ' ';

        for (auto &x : th->m_data)
          {
            x->put(header);
            th->m_ss.emplace_back(x->m_data.str());
          }

        if (!eov_str.empty())
          {
            th->m_eov.put(header);
            th->m_ss.emplace_back(eov_str);
          }

        if (!upd_str.empty())
          {
            th->m_upd.put(header);
            th->m_ss.emplace_back(upd_str);
          }

        th->m_ss[1] = header.str();

        first.width(pbeast::MultiDataFileResponse::num_width);
        first << th->m_ss[1].size();
        th->m_ss[0] = first.str();
      }

    size_t written(0);
    size_t it_counter(0);
    size_t it_pointer(0);

    while (pos > 0)
      {
        if (th->m_ss[it_counter].size() <= pos)
          {
            pos -= th->m_ss[it_counter].size();
            it_counter++;
          }
        else
          {
            it_pointer = pos;
            break;
          }

        if (it_counter == th->m_ss.size())
          {
            return MHD_CONTENT_READER_END_OF_STREAM;
          }
      }

    while (written < max)
      {
        if ((th->m_ss[it_counter].size() - it_pointer) >= (max - written))
          {
            size_t write_now = max - written;
            memcpy(buf + written, th->m_ss[it_counter].data() + it_pointer, write_now);
            it_pointer += write_now;
            written += write_now;
            break;
          }
        else
          {
            size_t write_now = th->m_ss[it_counter].size() - it_pointer;
            memcpy(buf + written, th->m_ss[it_counter].data() + it_pointer, write_now);
            written += write_now;
            it_counter++;
            it_pointer = 0;
            if (it_counter >= th->m_ss.size())
              break;
          }
      }

    return (written ? written : MHD_CONTENT_READER_END_OF_STREAM);
  }

  std::vector<std::string> m_ss;
};


namespace pbeast
{
  class BadUserRequest : public std::exception
  {
  public:
    explicit
    BadUserRequest(const std::string& message) :
        m_msg(message)
    {
    }

    virtual
    ~BadUserRequest() noexcept
    {
    }

    virtual const char*
    what() const noexcept
    {
      return m_msg.c_str();
    }

    const std::string&
    message() const noexcept
    {
      return m_msg;
    }

  protected:
    std::string m_msg;
  };
}

static void
report_http_error(std::ostringstream& text, const char * title, const char * description, const std::string& what)
{
  text.str("");
  text.clear();

  text << "<html><title>" << title << "</title>\n<body> " << description << ": <p><pre>\n";
  put_encoded(text, what);
  text << "</pre></p></body></html>";
}

static void
print_keys(std::ostringstream& s, const std::map<std::string, std::string>& params, const char * kind)
{
  s << ' ' << kind << ": ";

  if (params.empty())
    {
      s << "(null)";
    }
  else
    {
      bool first(true);
      for (auto& x : params)
        {
          if (first)
            first = false;
          else
            s << ',';
          s << "[\"" << x.first << "\",\"" << x.second << "\"]";
        }
    }
}

static const std::string&
get_param(const std::map<std::string, std::string>& parameters, const std::string& p)
{
  static const std::string __empty;

  std::map<std::string, std::string>::const_iterator it = parameters.find(p);
  if (it != parameters.end())
    return it->second;
  else
    return __empty;
}

static const std::string&
get_param(const std::map<std::string, std::string>& parameters, const std::string& p1, const std::string& p2)
{
  const std::string& s = get_param(parameters, p1);
  if (!s.empty())
    return s;
  else
    return get_param(parameters, p2);
}

MHD_Result
pbeast::Server::answer_to_connection(void * /*cls*/, struct MHD_Connection *connection, const char *url, const char *method, const char * /*version*/, const char * /*upload_data*/, size_t * /*upload_data_size*/, void **con_cls)
{
  // use address of dummu as connection ID
  static void * dummu;

  // only respond on GET methods
  if (0 != strcmp(method, "GET"))
    {
      ERS_DEBUG( 3, "Reject method \"" << method << "\" url: \"" << url << "\"" );
      return MHD_NO;
    }

  // do never respond on first call
  if (&dummu != *con_cls)
    {
      ERS_DEBUG( 3, "First call for method \"" << method << "\" url: \"" << url << "\"");
      *con_cls = &dummu;
      return MHD_YES;
    }
  *con_cls = nullptr;

  static std::atomic<uint_least64_t> s_call_count(0);
  uint64_t call_id = ++s_call_count;

  // get method name and parameters
  std::string method_name(url ? url : "");

  std::ostringstream log_text;
  log_text << "http request " << call_id << " \"" << method_name << '\"';

  std::map<std::string,std::string> method_params;
  MHD_get_connection_values (connection,  MHD_GET_ARGUMENT_KIND, add_key, reinterpret_cast<void *>(&method_params));
  print_keys(log_text, method_params, "arguments");

  std::map<std::string,std::string> header_params;
  MHD_get_connection_values (connection,  MHD_HEADER_KIND, add_key, reinterpret_cast<void *>(&header_params));


  // add user details
  pbeast::srv::UserStat * user_stat;

    {
      std::string user_id;

      const std::string& login(get_param(header_params, pbeast::HttpHeaderFields::user_login, pbeast::HttpHeaderFields::x_forwarded_login));
      const std::string& full_name(get_param(header_params, pbeast::HttpHeaderFields::full_username, pbeast::HttpHeaderFields::x_forwarded_fullname));
      const std::string& grafana_name(get_param(header_params, pbeast::HttpHeaderFields::x_grafana_username));
      std::string host(get_param(header_params, pbeast::HttpHeaderFields::user_host, pbeast::HttpHeaderFields::x_forwarded_for));

      size_t proxy = host.find_first_of(',');
      if (std::string::npos != proxy)
        {
          host.erase(proxy);
        }

      if (!login.empty())
        {
          user_id = login + " (" + full_name + ")";

          if (!grafana_name.empty())
            user_id += " as " + grafana_name;

          log_text << " by " << user_id << " on " << host;
        }
      else
        {
          if (host.empty())
            host = "???";

          user_id = std::string("unknown on ") + host;

          log_text << " from " << host;
        }

      std::shared_lock lock(s_srv->m_mon_thread_mutex);

      user_stat = (s_srv->m_monitor ? s_srv->m_monitor->get_user_stat(user_id).get() : nullptr);
    }

  if (ers::debug_level() > 0)
    {
      print_keys(log_text, header_params, "header");
    }

  if (ers::debug_level() > 1)
    {
      std::map<std::string, std::string> cookie_params;
      MHD_get_connection_values(connection, MHD_COOKIE_KIND, add_key, reinterpret_cast<void *>(&cookie_params));
      print_keys(log_text, cookie_params, "cookie");
    }

  ers::log(ers::Message(ERS_HERE, log_text.str()));


  // json data or error text
  std::ostringstream text;

  // file
  MultiDataFileResponseServer * files = nullptr;

  const char * content_type = nullptr;
  unsigned int status_code = 0;

  try
    {
      if (user_stat)
        {
          status_code = s_srv->process_http_request(call_id, *user_stat, method_name, method_params, text, files); // return status MHD_HTTP_OK in mos cases

          if (!text.str().empty())
            content_type = "application/json";
        }
      else
        {
          ERS_DEBUG( 0, "skip http request " << call_id << ": server is stopping" );
          report_http_error(text, "P-BEAST Maintenance", "Server is stopping...", "caught kill signal");
          status_code = MHD_HTTP_SERVICE_UNAVAILABLE;
        }
    }
  catch (pbeast::Failure & ex)
    {
      report_http_error(text, "P-BEAST Failure", "An error has occurred", (const char *)ex.text);
      status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
    }
  catch (pbeast::NotFound & ex)
    {
      report_http_error(text, "P-BEAST NotFound", "No data found", (const char *)ex.text);
      status_code = MHD_HTTP_NOT_FOUND;
    }
  catch (pbeast::BadRequest & ex)
    {
      report_http_error(text, "P-BEAST BadRequest", "Bad User Request", (const char *)ex.text);
      status_code = MHD_HTTP_BAD_REQUEST;
    }
  catch (pbeast::BadUserRequest & ex)
    {
      report_http_error(text, "P-BEAST BadRequest", "Bad User Request", ex.message());
      status_code = MHD_HTTP_BAD_REQUEST;
    }
  catch (const std::exception& ex)
    {
      report_http_error(text, "P-BEAST Error", "An error has occurred", ex.what());
      status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
    }


  struct MHD_Response * response;
  const std::string text_str(text.str());

  if (text_str.empty())
    {
      response = MHD_create_response_from_callback(MHD_SIZE_UNKNOWN, 128 * 1024, &MultiDataFileResponseServer::put, files, &MultiDataFileResponseServer::free);
    }
  else
    {
      delete files;
      response = MHD_create_response_from_buffer(text_str.length(), (void *) (text_str.c_str()), MHD_RESPMEM_MUST_COPY);
    }

  if (response == nullptr)
    {
      ers::error(daq::pbeast::FailedCreateHttpResponse(ERS_HERE, text_str.length()));
      return MHD_NO;
    }

  if (content_type)
    MHD_add_response_header(response, "Content-Type", content_type);

  MHD_Result ret = MHD_queue_response (connection, status_code, response);
  MHD_destroy_response (response);
  return ret;
}

static uint64_t
str2int(const std::string& s, const std::string& p)
{
  char * __sanity;
  uint64_t v = strtoull(s.c_str(), &__sanity, 0);
  if (*__sanity != 0 || errno == ERANGE)
    {
      std::ostringstream text;
      text << "cannot read value of integer parameter \"" << p << "\" because strtoull(\'" << s << "\') failed";
      if (*__sanity)
        text << " on unrecognized characters \'" << __sanity << "\'";
      if (errno)
        {
          char buffer[1024];
          buffer[0] = 0;
          strerror_r(errno, buffer, 1024);
          text << " with code " << errno << ", reason = \'" << buffer << '\'';
        }

      throw std::runtime_error(text.str().c_str());
    }

  return v;
}

static uint64_t
param2int(const std::map<std::string, std::string>& parameters, const std::string& p, uint64_t default_value = 0)
{
  const std::string& v = get_param(parameters, p);

  if (!v.empty())
    {
      if (v == "-1")
        return std::numeric_limits<uint64_t>::max();
      else
        return str2int(v, p);
    }
  else
    {
      return default_value;
    }
}

static void
validate_mv_param(const std::string& p, const std::string& values, const std::string& requests, int count)
{
  if (values.empty() == false)
    {
      std::ostringstream text;
      text << "mismatch between number of query requests and values of \"" << p << "\" parameter for request number " << count << ":\n"
          "query requests  : \"" << requests << "\"\n"
          "parameter       : \"" << values << "\"\n";
      throw std::runtime_error(text.str().c_str());
    }
}

static bool
str2bool(const std::string& v, bool default_value, const std::string& p, const std::string& values, const std::string& requests, int count)
{
  if (!v.empty())
    {
      std::string s = boost::to_lower_copy<std::string>(v);

      if (s == "true")
        return true;
      else if (s == "false")
        return false;
      else
        {
          std::ostringstream text;
          text << "Value of parameter \"" << p << "\" = \"" << v << "\" (is not a boolean)";
          throw std::runtime_error(text.str().c_str());
        }
    }
  else
    {
      validate_mv_param(p, values, requests, count);

      return default_value;
    }
}

static bool
get_bool(const std::map<std::string, std::string>& parameters, const std::string& p, bool default_value)
{
  return str2bool(get_param(parameters, p),default_value,p,"","",0);
}


static void
parse(const std::string& request, std::string& partition_name, std::string& class_name, std::string& attribute_name_path, std::string& object_mask)
{
  static const std::string __empty;

  std::vector<std::string> strs;
  boost::split(strs, request, boost::is_any_of("."));

  partition_name = (strs.size() > 0 ? strs[0] : __empty);
  class_name = (strs.size() > 1 ? strs[1] : __empty);
  attribute_name_path = (strs.size() > 2 ? strs[2] : __empty);
  object_mask = (strs.size() > 3 ? request.substr(partition_name.size() + class_name.size() + attribute_name_path.size() + 3) : __empty);
}

inline void
set_if_defined(const std::map<std::string, std::string>& parameters, const std::string& name, uint64_t& param)
{
  constexpr uint64_t no_val = std::numeric_limits<uint64_t>::max();

  uint64_t v = param2int(parameters, name, no_val);
  if (v != no_val)
    param = v;
}

unsigned int
pbeast::Server::process_http_request(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, std::string_view name, const std::map<std::string,std::string>& parameters, std::ostringstream& out, MultiDataFileResponseServer*& mdfr)
{
  unsigned int status = MHD_HTTP_OK;
  const char s_tdaq_pbeast[] = "tdaq/pbeast/";

  // trim leading slashes
  const auto pos(name.find_first_not_of('/'));
  if (pos != std::string_view::npos)
    name.remove_prefix(pos);

  if (name.find(s_tdaq_pbeast) != 0)
    throw pbeast::BadRequest((std::string("the method name does not start from ") + s_tdaq_pbeast).c_str());
  else
    name.remove_prefix(sizeof(s_tdaq_pbeast)-1);

  const std::string& stub = get_param(parameters, "stub");
  const std::string& ids = get_param(parameters, "id");

  uint64_t num_max_points = param2int(parameters, "maxDataPoints");
  uint64_t since = pbeast::mk_ts(param2int(parameters, "from"));
  uint64_t until = pbeast::mk_ts(param2int(parameters, "to"));

  if (since == 0)
    since = pbeast::get_def_since();

  if (until == 0)
    until = pbeast::get_def_until();

  set_if_defined(parameters, "since", since);
  set_if_defined(parameters, "until", until);

  uint64_t prev_interval = pbeast::mk_ts(param2int(parameters, "prevInterval", pbeast::def_lookup_prev_const32));
  uint64_t next_interval = pbeast::mk_ts(param2int(parameters, "nextInterval", pbeast::def_lookup_next_const32));

  set_if_defined(parameters, "prevLookupLapse", prev_interval);
  set_if_defined(parameters, "nextLookupLapse", next_interval);

  std::string functions(get_param(parameters, "functions"));

  if (name == s_rest_autocomplete_series)
    {
      http_autocomplete_series(call_id, user_call_stat, stub, since, until, (num_max_points ? num_max_points : 10), out);
    }
  else if (name == s_rest_get_partition_names)
    {
      http_get_partition_names(call_id, user_call_stat, get_param(parameters, "id"), since, until, (num_max_points ? num_max_points : 64), out);
    }
  else if (name == s_rest_get_class_names)
    {
      http_get_class_names(call_id, user_call_stat, get_param(parameters, "partition"), get_param(parameters, "id"), since, until, (num_max_points ? num_max_points : 64), out);
    }
  else if (name == s_rest_get_attribute_names)
    {
      http_get_attribute_names(call_id, user_call_stat, get_param(parameters, "partition"), get_param(parameters, "class"), get_param(parameters, "id"), since, until, (num_max_points ? num_max_points : 64), out);
    }
  else if (name == s_rest_get_object_names)
    {
      http_get_object_names(call_id, user_call_stat, get_param(parameters, "partition"), get_param(parameters, "class"), get_param(parameters, "attribute"), get_param(parameters, "id"), since, until, (num_max_points ? num_max_points : 64), out);
    }
  else if (name == s_rest_get_schema)
    {
      http_get_schema(call_id, user_call_stat, get_param(parameters, "partition"), prev_interval, out);
    }
  else if (name == s_rest_read_series)
    {
      http_read_series(call_id, user_call_stat, ids, get_param(parameters, "plotType"), since, until, prev_interval, next_interval, get_param(parameters, "showAllUpdates"), (num_max_points ? num_max_points : 100), static_cast<uint32_t>(param2int(parameters, "sampleInterval")), get_param(parameters, "fillGaps"), get_param(parameters, "readRawData"), get_param(parameters, "isRegexp"), get_param(parameters, "useFQN"), functions, out);
    }
  else if (name == s_rest_get_annotations)
    {
      http_get_annotations(call_id, user_call_stat, get_param(parameters, "partition"), get_param(parameters, "class"), get_param(parameters, "attribute"), get_param(parameters, "object"), get_bool(parameters, "isRegexp", false), get_bool(parameters, "mergeResult", false), since, until, prev_interval, next_interval, get_bool(parameters, "showAllUpdates", false), num_max_points, get_param(parameters, "filter"), get_bool(parameters, "useFQN", false), functions, out);
    }
  else if (name == s_rest_get_data)
    {
      http_get_data(call_id, user_call_stat, get_param(parameters, "partition"), get_param(parameters, "class"), get_param(parameters, "attribute"), get_param(parameters, "object"), get_bool(parameters, "isRegexp", false), since, until, prev_interval, next_interval, get_bool(parameters, "showAllUpdates", false), static_cast<uint32_t>(param2int(parameters, "sampleInterval")), get_param(parameters, "fillGaps"), get_bool(parameters, "zipCatalog", false), get_bool(parameters, "zipData", false), get_bool(parameters, "catalogizeStrings", true), get_bool(parameters, "useFQN", false), functions, mdfr);
    }
  else if (name == s_rest_health)
    {
      status = http_health(call_id, user_call_stat, out);
    }
  else
    {
      ERS_LOG("reject unexpected method " << call_id);
      out << "the method \"" << name << "\" does not exist\n";
      throw pbeast::BadRequest(out.str().c_str());
    }

  return status;
}


static void
validate_parameter(const std::string& value, const char * name, const std::string& function, const char * parameter = nullptr, const std::string& request = "")
{
  if (value.empty())
    {
      std::ostringstream error_text;
      error_text << "missing \"" << name << "\" parameter in " << function;
      if (parameter)
        error_text << " request \"" << parameter << '=' << request << '\"';
      throw pbeast::BadUserRequest(error_text.str());
    }
}


void
pbeast::Server::http_get_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, const std::string& re_str, uint64_t since, uint64_t until, uint64_t max_num, const std::string& function, const std::string& request, std::ostringstream& out)
{
  auto start = std::chrono::steady_clock::now();

  std::set<std::string> data;

  // list objects of an attribute
  if (!attribute_name.empty())
    {
      get_updated_objects(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), since, until, re_str.c_str(), data);
    }
  else
    {
      // list attributes of a class
      if (!class_name.empty())
        get_attributes_with_nested(partition_name.c_str(), class_name.c_str(), data);

      // list classes of a partition
      else if (!partition_name.empty())
        get_classes(partition_name.c_str(), data);

      // list partitions
      else
        get_partitions(data);

      if (!re_str.empty() && re_str != ".*")
        try
          {
            std::regex re(re_str);

            for (auto it = data.begin(); it != data.end();)
              if (!std::regex_match(*it, re))
                it = data.erase(it);
              else
                it++;
          }
        catch (const std::regex_error &ex)
          {
            std::ostringstream error_text;

            error_text << "failed to create regex \"" << re_str << "\" in " << function;
            if (!request.empty())
              error_text << " request \"" << request << '\"';
            error_text << "\": " << ex.what();

            throw pbeast::BadUserRequest(error_text.str());
          }
    }

  out << "{\"list\":[";

  // put limited number of items

  uint64_t num = (data.size() < max_num ? data.size() : max_num);
  const auto num_of_items(num);

  for (auto & x : data)
    {
      out << '\"' << x << '\"';
      if (--num)
        out << ',';
      else
        break;
    }

  out << "]}";

  const auto ti = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-start).count() / 1000.;
  const uint64_t sz = out.tellp();

  add_rest(user_call_stat, static_cast<uint64_t>(ti), sz);

  std::ostringstream log_text;

  log_text << "generate json for request " << call_id << " in " << ti << " ms (" << num_of_items;
  if(num_of_items != data.size())
    log_text << " of " << data.size();
  log_text << " items, " << sz << " bytes)";

  ers::log(ers::Message(ERS_HERE, log_text.str()));
}


void
pbeast::Server::http_autocomplete_series(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& request, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out)
{
  std::string partition_name, class_name, attribute_name_path, object_mask;
  parse(request, partition_name, class_name, attribute_name_path, object_mask);

  // list objects of an attribute
  if (!attribute_name_path.empty())
    {
      validate_parameter(partition_name, "partition", s_rest_autocomplete_series, "stub", request);
      validate_parameter(class_name, "class", s_rest_autocomplete_series, "stub", request);

      if (object_mask.find_first_of('*') == std::string::npos)
        object_mask.append(".*");
    }
  else
    {
      if (!class_name.empty())
        validate_parameter(partition_name, "partition", s_rest_autocomplete_series, "stub", request);

      object_mask = ".*";
    }

  http_get_names(call_id, user_call_stat, partition_name, class_name, attribute_name_path, object_mask, since, until, max_num, "autocompleteSeries", request, out);
}

void
pbeast::Server::http_get_partition_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& query, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out)
{
  http_get_names(call_id, user_call_stat, "", "", "", query, since, until, max_num, s_rest_get_partition_names, "", out);
}

void
pbeast::Server::http_get_class_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& query, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out)
{
  validate_parameter(partition_name, "partition", s_rest_get_class_names);
  http_get_names(call_id, user_call_stat, partition_name, "", "", query, since, until, max_num, s_rest_get_class_names, "", out);
}

void
pbeast::Server::http_get_attribute_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& query, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out)
{
  validate_parameter(partition_name, "partition", s_rest_get_class_names);
  validate_parameter(class_name, "class", s_rest_get_class_names);
  http_get_names(call_id, user_call_stat, partition_name, class_name, "", query, since, until, max_num, s_rest_get_class_names, "", out);
}

void
pbeast::Server::http_get_object_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, const std::string& query, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out)
{
  validate_parameter(partition_name, "partition", s_rest_get_class_names);
  validate_parameter(class_name, "class", s_rest_get_class_names);
  validate_parameter(attribute_name, "attribute", s_rest_get_attribute_names);
  http_get_names(call_id, user_call_stat, partition_name, class_name, attribute_name, query, since, until, max_num, s_rest_get_class_names, "", out);
}


unsigned int
pbeast::Server::http_health(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, std::ostringstream& out)
{
  std::map<std::string, pbeast::server_var> servers;

  auto start = std::chrono::steady_clock::now();

  try
    {
      static std::string all;
      get_servers(all, servers, false);
    }
  catch (daq::pbeast::Exception &ex)
    {
      throw pbeast::Failure(ex.what());
    }

  const bool status(!servers.empty());

  auto ti = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-start).count() / 1000.;

  out << std::boolalpha <<
      "{\n"
      "  \"healthy\": " << status << ",\n"
      "  \"message id\": " << call_id << ",\n"
      "  \"processing time (ms)\": " << ti << ",\n"
      "  \"servers\": [";

  bool is_first = true;
  for (const auto &x : servers)
    {
      if (is_first)
        is_first = false;
      else
        out << ',';

      out << "\n"
          "    {\n"
          "      \"name\": \"" << x.first << "\"\n"
          "    }";
    }

  out << "\n"
      "  ]\n"
      "}";

  add_rest(user_call_stat, static_cast<uint64_t>(ti), out.tellp());

  return (status ? MHD_HTTP_OK : MHD_HTTP_SERVICE_UNAVAILABLE);
}



static const char *
jend(std::size_t& len)
{
  return (--len ? ",\n" : "\n");
}

void
pbeast::Server::http_get_schema(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, uint64_t prev_interval, std::ostringstream& out)
{
  auto start = std::chrono::steady_clock::now();

  std::map<std::string, pbeast::srv::ClassInfo> info = get_schema(partition_name.c_str());

  out << "[\n";

  std::size_t num_of_classes = info.size();

  for (const auto& x : info)
    {
      out <<
          " {\n"
          "  \"name\":" << pbeast::json::serisalize(x.first) << ",\n"
          "  \"attributes\":\n"
          "   [\n";

      std::size_t num_of_attributes = x.second.m_attributes.size();

      for (const auto& a : x.second.m_attributes)
        out <<
            "    {\n"
            "     \"name\":" << pbeast::json::serisalize(a.first) << ",\n"
            "     \"type\":" << pbeast::json::serisalize(a.second.m_type) << ",\n"
            "     \"is-array\":" << ( a.second.m_is_array ? "true" : "false" ) << ",\n"
            "     \"description\":" << pbeast::json::serisalize(a.second.m_description) << "\n"
            "    }" << jend(num_of_attributes);

      out <<
          "   ]\n"
          " }" << jend(num_of_classes);
    }

  out << "]";

  auto ti = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-start).count() / 1000.;

  uint64_t sz = out.tellp();

  add_rest(user_call_stat, static_cast<uint64_t>(ti), sz);
  ERS_LOG("generate json for request " << call_id << " in " << ti << " ms (" << sz << " bytes)");
}


struct LinesData
{
  std::string m_json_value;
  bool m_is_string;
  bool m_is_array;

  inline std::string&
  init(bool is_string, bool is_array)
  {
    m_is_string = is_string;
    m_is_array = is_array;
    return m_json_value;
  }
};

template<class T>
void
add_value(std::string& to, std::ostringstream& ss, uint64_t ts, const T& value)
{
  if (!to.empty())
    to.push_back(',');

  ss.str("");
  ss << '[' << ts << ',';
  value.print_json_value(ss);
  ss << ']';
  to.append(ss.str());
}

template<class T>
void
add_ds_value(std::string& to, std::ostringstream& ss, uint64_t ts, const T& value, const pbeast::AggregationType at)
{
  if (!to.empty())
    to.push_back(',');

  ss.str("");
  ss << '[' << ts << ',';
  value.print_json_value(ss, at);
  ss << ']';
  to.append(ss.str());
}

static inline void
add_null(std::string& to, uint64_t ts)
{
  if (!to.empty())
    to.push_back(',');

  to.push_back('[');

  char buf[16];
  char * ptr = buf;
  boost::spirit::karma::generate(ptr, boost::spirit::ulong_, static_cast<unsigned long>(ts));
  to.append(buf,ptr - buf);

  to.append(",null]");
}


static inline uint64_t
sec2msec(uint32_t secs)
{
  return static_cast<uint64_t>(secs) * 1000L;
}

static inline uint64_t
ts2mks(uint64_t nsecs)
{
  return nsecs / 1000L;
}


template<class T>
  unsigned long
  put_raw_lines_data(std::map<std::string, LinesData>& lines_data, const uint64_t max_num, uint32_t ds_interval, uint64_t since, uint64_t until, const std::map<std::string, std::set<T>>& data, const std::map<std::string, std::set<uint64_t>>& map_eov, const std::map<std::string, std::set<uint64_t>>& map_upd, const bool is_string, const bool is_array, pbeast::FillGaps::Type fill_gaps)
  {
    since = ts2mks(since);
    until = ts2mks(until);

    unsigned long num_of_formatted_data_points(0);

    if (data.empty() == false)
      {
        std::ostringstream ss;
        std::vector<typename T::ds_type> ds_data;
        std::set<uint64_t> empty;

        for (auto &s : data)
          {
            std::map<std::string, std::set<uint64_t>>::const_iterator e = pbeast::find(map_eov, s.first);
            const std::set<uint64_t>& eov(e != map_eov.end() ? e->second : empty);

            std::map<std::string, std::set<uint64_t>>::const_iterator u = pbeast::find(map_upd, s.first);
            const std::set<uint64_t>& upd(u != map_upd.end() ? u->second : empty);

            std::string& val = lines_data[s.first].init(is_string, is_array);

            // return raw data, if serie size is below threshold
            if (upd.size() <= max_num && s.second.size() <= max_num && ds_interval == 0)
              {
                std::set<uint64_t>::const_iterator eov_it = eov.begin();
                uint64_t eov_next = (eov_it != eov.end()) ? ts2mks(*eov_it) : 0;

                std::set<uint64_t>::const_iterator upd_it = upd.begin();
                uint64_t upd_next = (upd_it != upd.end()) ? ts2mks(*upd_it) : 0;

                for (auto it = s.second.begin(); it != s.second.end(); ++it)
                  {
                    uint64_t t1(ts2mks(it->get_ts_created()));
                    uint64_t t2(ts2mks(it->get_ts_last_updated()));

                    // avoid double data points before or after query time interval
                    if (t2 < since)
                      t1 = t2;
                    if (t1 > until)
                      t2 = t1;

                    while (eov_next != 0 && eov_next < t1)
                      {
                        add_null(val, eov_next);
                        eov_it++;
                        eov_next = (eov_it != eov.end()) ? ts2mks(*eov_it) : 0;
                      }

                    if (upd_next != 0)
                      {
                        while (upd_next != 0 && upd_next <= t1)
                          {
                            upd_it++;
                            upd_next = (upd_it != upd.end()) ? ts2mks(*upd_it) : 0;
                          }

                        add_value(val, ss, t1, *it);
                        num_of_formatted_data_points++;

                        auto it2 = std::next(it);

                        if (it2 != s.second.end())
                          t2 = ts2mks(it2->get_ts_created());
                        else
                          t2 = until;

                        if (eov_next && eov_next < t2)
                          t2 = eov_next;

                        while (upd_next != 0 && upd_next < t2)
                          {
                            add_value(val, ss, upd_next, *it);
                            num_of_formatted_data_points++;

                            upd_it++;
                            upd_next = (upd_it != upd.end()) ? ts2mks(*upd_it) : 0;
                          }
                      }
                    else
                      {
                        add_value(val, ss, t1, *it);
                        num_of_formatted_data_points++;

                        if (t1 != t2)
                          {
                            add_value(val, ss, t2, *it);
                            num_of_formatted_data_points++;
                          }
                      }
                  }

                if (eov_next != 0)
                  add_null(val, eov_next);
              }
            else
              {
                const uint64_t min_ts((*s.second.cbegin()).get_ts_created());
                const uint64_t max_ts((*s.second.crbegin()).get_ts_last_updated());

                const uint32_t dt = ds_interval ? ds_interval : std::max(pbeast::ts2secs((max_ts - min_ts) / max_num), static_cast<uint32_t>(1));

                ERS_DEBUG(1, "downsample " << std::max(upd.size(), s.second.size()) << " raw data points of " << s.first << " with ds interval " << dt);

                ds_data.clear();

                pbeast::downsample(dt, s.second, ds_data, fill_gaps);

                std::set<uint32_t> upd_ds, eov_ds;

                pbeast::downsample_void(dt, upd, upd_ds);
                pbeast::downsample_void(dt, eov, eov_ds);

                #define EOV_ADD_CODE       add_null(val, sec2msec(eov_next));
                #define DATA_ADD_CODE(TS)  add_ds_value(val, ss, sec2msec(TS), *it, pbeast::AggregationNone); num_of_formatted_data_points++;

                APPLY_DS_ALGO(ds_data, eov_ds, upd_ds, EOV_ADD_CODE, DATA_ADD_CODE)
              }
          }
      }

    return num_of_formatted_data_points;
  }


template<class T>
  unsigned long
  put_ds_lines_data(std::map<std::string, LinesData>& lines_data, const std::map<std::string, std::set<T>>& data, const std::map<std::string, std::set<uint32_t>>& map_eov, const std::map<std::string, std::set<uint32_t>>& map_upd, bool is_string, bool is_array, const pbeast::FunctionConfig& fc)
  {
    unsigned long num_of_formatted_data_points(0);

    if (data.empty() == false)
      {
        std::set<uint32_t> empty32;

        std::ostringstream ss;

        for (auto& s : data)
          {
            std::string& val = lines_data[s.first].init(is_string, is_array);

            pbeast::AggregationType a_type = (fc.no_aggregations() ? pbeast::AggregationNone : fc.get_type(s.first));

            std::map<std::string, std::set<uint32_t>>::const_iterator e = pbeast::find(map_eov, s.first);
            const std::set<uint32_t>& eov(e != map_eov.end() ? e->second : empty32);

            std::map<std::string, std::set<uint32_t>>::const_iterator u = pbeast::find(map_upd, s.first);
            const std::set<uint32_t>& upd(u != map_upd.end() ? u->second : empty32);

            const std::set<T>& ds_data = s.second;

            #define EOV_DS_ADD_CODE       add_null(val, sec2msec(eov_next));
            #define DATA_DS_ADD_CODE(TS)  add_ds_value(val, ss, sec2msec(TS), *it, a_type); num_of_formatted_data_points++;

            APPLY_DS_ALGO(ds_data, eov, upd, EOV_DS_ADD_CODE, DATA_DS_ADD_CODE)
          }
      }

    return num_of_formatted_data_points;
  }

template<class T>
  class HistogramData
  {

  public:

    HistogramData() :
        m_ts(0), m_is_eov(true)
    {
      ;
    }

    void
    reset(uint64_t ts, const T& value)
    {
      m_value = value;
      m_ts = ts;
      m_is_eov = false;
    }

    void
    reset(uint64_t ts)
    {
      m_ts = ts;
      m_is_eov = true;
    }

    bool
    is_inited() const
    {
      return (m_ts != 0);
    }

    bool
    is_eov() const
    {
      return m_is_eov;
    }

    const T&
    get_value() const
    {
      return m_value;
    }

    uint64_t
    get_ts() const
    {
      return m_ts;
    }

  protected:

    uint64_t m_ts;
    T m_value;
    bool m_is_eov;

  };

template<class T>
  void
  make_json_label(std::string & val)
  {
    ;
  }

template<>
  void inline
  make_json_label<std::string>(std::string & word)
  {
    // remove quotes breaking json
    word.erase(std::remove_if(word.begin(), word.end(), [](char ch)
      { return (ch == '\"');}), word.end());
  }

template<class T>
  unsigned long
  put_histogram_data(std::map<std::string, uint64_t>& histogram_data, uint64_t& histogram_eov_size, uint64_t since, uint64_t until, const std::map<std::string, std::set<pbeast::SeriesData<T>>>& data, const std::map<std::string, std::set<uint64_t>>& map_eov)
  {
    std::set<uint64_t> empty_eov;

    if (data.empty() == false)
      {
        std::map<T,uint64_t> histogram;

        for (auto& s : data)
          {
            std::map<std::string,std::set<uint64_t>>::const_iterator e = pbeast::find(map_eov, s.first);
            const std::set<uint64_t>& eov(e != map_eov.end() ? e->second : empty_eov);
            std::set<uint64_t>::const_iterator eov_it = eov.begin();
            uint64_t eov_next = (eov_it != eov.end()) ? *eov_it : 0;

            HistogramData<T> last;

            for (auto& v : s.second)
              {
                while (eov_next != 0 && eov_next < v.get_ts_created())
                  {
                    if (last.is_inited() == true)
                      {
                        const uint64_t dt = eov_next - last.get_ts();
                        if (last.is_eov())
                          histogram_eov_size += dt;
                        else
                        histogram[last.get_value()] += dt;
                      }

                    last.reset(eov_next < since ? since : eov_next);

                    eov_it++;
                    eov_next = (eov_it != eov.end()) ? *eov_it : 0;
                  }

                if (last.is_inited() == true)
                  {
                    uint64_t dt = (v.get_ts_created() < since ? since : v.get_ts_created()) - last.get_ts();
                    if (last.is_eov())
                      histogram_eov_size += dt;
                    else
                      histogram[last.get_value()] += dt;
                  }

                last.reset(v.get_ts_created(), v.get_value());
              }

            if (last.is_eov())
              {
                uint64_t u = eov_next ? eov_next : last.get_ts();
                if (u > until) u = until;
                  histogram_eov_size += (u - last.get_ts());
              }
            else
              {
                uint64_t last_updated = (*s.second.crbegin()).get_ts_last_updated();
                if (eov_next != 0)
                  {
                    histogram[last.get_value()] += (last_updated - last.get_ts());
                    last.reset(eov_next);
                  }
                else
                  {
                    histogram[last.get_value()] += ((last_updated > until ? until : last_updated) - last.get_ts());
                  }
              }
          }

        std::ostringstream ss;

        for (auto& x : histogram)
          {
            ss.str("");
            pbeast::print(ss,x.first);
            std::string label(ss.str());
            make_json_label<T>(label);
            histogram_data[label] += x.second;
          }

        return histogram.size();
      }

    return 0;
  }


struct NamesPool
{
  const std::string m_base;
  std::vector<std::string> m_data;

  NamesPool(const std::string& s) : m_base(s) { ; }
  const std::string& get(unsigned int i);
};

const std::string&
NamesPool::get(unsigned int i)
{
  if (m_data.size() == i)
    {
      std::string s(m_base);
      char buf[12];
      s.push_back('[');
      char * ptr = buf;
      boost::spirit::karma::generate(ptr, boost::spirit::uint_, i);
      s.append(buf, ptr - buf);
      s.push_back(']');
      m_data.emplace_back(s);
    }

  if (m_data.size() <= i)
    {
      abort();
    }

  return m_data[i];
}

template<class T>
  void
  array2series(const std::map<std::string, std::set<pbeast::SeriesVectorData<T>>>& in, std::map<std::string, std::set<pbeast::SeriesData<T>>>& out)
  {
    if (in.empty() == false)
      {
        for (auto& x : in)
          {
            NamesPool names(x.first);
            for (auto& y : x.second)
              {
                unsigned int idx(0);

                for (const auto& v : y.get_value())
                  {
                    out[names.get(idx++)].emplace(y.get_ts_created(),y.get_ts_last_updated(),v);
                  }
              }
          }
      }
  }


template<typename T, typename F>
  unsigned long
  count_primitive_items(const std::map<std::string, T>& data, std::map<std::string, std::set<F>> eov)
  {
    unsigned long sum(0);

    for (auto& x : data)
      {
        sum += x.second.size();
      }

    for (auto& x : eov)
      sum += x.second.size();

    return sum;
  }

template<typename T, typename F>
  unsigned long
  count_container_items(const std::map<std::string, T>& data, std::map<std::string, std::set<F>> eov)
  {
    unsigned long sum(0);

    for (auto& x : data)
      {
        for (auto& y : x.second)
          {
            sum += y.get_size();
          }
      }

    for (auto& x : eov)
      sum += x.second.size();

    return sum;
  }

static std::string
make_log(std::chrono::time_point<std::chrono::steady_clock>& t1, std::chrono::time_point<std::chrono::steady_clock>& t2, std::chrono::time_point<std::chrono::steady_clock> t3, uint64_t call_id, int request_count, const char * type, bool is_array, unsigned long dp_num, unsigned long sr_num, const std::string extra_verbose_out)
{
  std::ostringstream text;

  text << "request " << call_id;
  if (request_count)
    text << '/' << request_count;

  text << " reads " << dp_num << " \"" << type << (is_array ? "[]" : "") << "\" data points in " << std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count() / 1000. << " ms and writes ";

  if (sr_num)
    text << sr_num << " values ";

  text << "in " << std::chrono::duration_cast<std::chrono::microseconds>(t3-t2).count() / 1000. << " ms";

  if (!extra_verbose_out.empty())
    text << ':' << extra_verbose_out;

  return text.str();
}

#define READ_LINES_DATA(CPP_TYPE, NAME)                                                                                                                                                                                                                                                                        \
{                                                                                                                                                                                                                                                                                                              \
  std::chrono::time_point<std::chrono::steady_clock> t1 = std::chrono::steady_clock::now(), t2;                                                                                                                                                                                                                \
                                                                                                                                                                                                                                                                                                               \
  unsigned long dp_num;                                                                                                                                                                                                                                                                                        \
  unsigned long sr_nums;                                                                                                                                                                                                                                                                                       \
                                                                                                                                                                                                                                                                                                               \
  if ((sample_interval < m_http_request_downsample_threshold || read_raw_data == true) && fc.no_aggregations() && fill_gaps != pbeast::FillGaps::All && fc.use_summarize() == false)                                                                                                                           \
    {                                                                                                                                                                                                                                                                                                          \
      std::map<std::string, std::set<uint64_t>> eov, upd;                                                                                                                                                                                                                                                      \
                                                                                                                                                                                                                                                                                                               \
      if (x.m_is_array == false)                                                                                                                                                                                                                                                                               \
        {                                                                                                                                                                                                                                                                                                      \
          std::map<std::string, std::set<pbeast::SeriesData<CPP_TYPE>>> data;                                                                                                                                                                                                                                  \
          verbose = get_##NAME##_attributes(partition_name.c_str(), class_name.c_str(), attribute_name_path.c_str(), x.m_since, x.m_until, prev, next, object_mask.c_str(), is_regexp, fc, show_all_updates, eov, upd, data, line_prefix_name);                                                                \
          t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                                                                               \
          dp_num = count_primitive_items(data, eov);                                                                                                                                                                                                                                                           \
          sr_nums = put_raw_lines_data(lines_data, max_num, ds_interval, x.m_since, x.m_until, data, eov, upd, is_string, is_array, fill_gaps);                                                                                                                                                                \
        }                                                                                                                                                                                                                                                                                                      \
      else                                                                                                                                                                                                                                                                                                     \
        {                                                                                                                                                                                                                                                                                                      \
          std::map<std::string,std::set<pbeast::SeriesVectorData<CPP_TYPE>>> data;                                                                                                                                                                                                                             \
          verbose = get_##NAME##_array_attributes(partition_name.c_str(), class_name.c_str(), attribute_name_path.c_str(), x.m_since, x.m_until, prev, next, object_mask.c_str(), is_regexp, fc, show_all_updates, eov, upd, data, line_prefix_name);                                                          \
          t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                                                                               \
          dp_num = count_container_items(data, eov);                                                                                                                                                                                                                                                           \
          sr_nums = put_raw_lines_data(lines_data, max_num, ds_interval, x.m_since, x.m_until, data, eov, upd, is_string, is_array, fill_gaps);                                                                                                                                                                \
        }                                                                                                                                                                                                                                                                                                      \
    }                                                                                                                                                                                                                                                                                                          \
  else                                                                                                                                                                                                                                                                                                         \
    {                                                                                                                                                                                                                                                                                                          \
      std::map<std::string, std::set<uint32_t>> eov, upd;                                                                                                                                                                                                                                                      \
                                                                                                                                                                                                                                                                                                               \
      if (x.m_is_array == false)                                                                                                                                                                                                                                                                               \
        {                                                                                                                                                                                                                                                                                                      \
          std::map<std::string, std::set<pbeast::DownsampledSeriesData<CPP_TYPE>>> data;                                                                                                                                                                                                                       \
          verbose = get_downsample_##NAME##_attributes(partition_name.c_str(), class_name.c_str(), attribute_name_path.c_str(), x.m_since, x.m_until, prev_interval, next_interval, sample_interval, fill_gaps, object_mask.c_str(), is_regexp, fc, show_all_updates, eov, upd, data, line_prefix_name);       \
          t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                                                                               \
          dp_num = count_primitive_items(data, eov);                                                                                                                                                                                                                                                           \
          sr_nums = put_ds_lines_data(lines_data, data, eov, upd, is_string, is_array, fc);                                                                                                                                                                                                                    \
        }                                                                                                                                                                                                                                                                                                      \
      else                                                                                                                                                                                                                                                                                                     \
        {                                                                                                                                                                                                                                                                                                      \
          std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<CPP_TYPE>>> data;                                                                                                                                                                                                                  \
          verbose = get_downsample_##NAME##_array_attributes(partition_name.c_str(), class_name.c_str(), attribute_name_path.c_str(), x.m_since, x.m_until, prev_interval, next_interval, sample_interval, fill_gaps, object_mask.c_str(), is_regexp, fc, show_all_updates, eov, upd, data, line_prefix_name); \
          t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                                                                               \
          dp_num = count_container_items(data, eov);                                                                                                                                                                                                                                                           \
          sr_nums = put_ds_lines_data(lines_data, data, eov, upd, is_string, is_array, fc);                                                                                                                                                                                                                    \
        }                                                                                                                                                                                                                                                                                                      \
    }                                                                                                                                                                                                                                                                                                          \
                                                                                                                                                                                                                                                                                                               \
  num_of_data_points += sr_nums;                                                                                                                                                                                                                                                                               \
  ers::log(ers::Message(ERS_HERE, make_log(t1, t2, std::chrono::steady_clock::now(), call_id, request_count, #NAME, x.m_is_array, dp_num, sr_nums, verbose)));                                                                                                                                                 \
}                                                                                                                                                                                                                                                                                                              \
break;


#define READ_HISTOGRAM_DATA(CPP_TYPE, NAME)                                                                                                                                                                                                   \
{                                                                                                                                                                                                                                             \
  std::chrono::time_point<std::chrono::steady_clock> t1 = std::chrono::steady_clock::now(), t2;                                                                                                                                               \
                                                                                                                                                                                                                                              \
  std::map<std::string, std::set<pbeast::SeriesData<CPP_TYPE>>> data;                                                                                                                                                                         \
                                                                                                                                                                                                                                              \
  if (x.m_is_array == false)                                                                                                                                                                                                                  \
    {                                                                                                                                                                                                                                         \
      verbose = get_##NAME##_attributes(partition_name.c_str(), class_name.c_str(), attribute_name_path.c_str(), x.m_since, x.m_until, prev, next, object_mask.c_str(), is_regexp, fc, false, eov, upd, data, line_prefix_name);              \
    }                                                                                                                                                                                                                                         \
  else                                                                                                                                                                                                                                        \
    {                                                                                                                                                                                                                                         \
      std::map<std::string,std::set<pbeast::SeriesVectorData<CPP_TYPE>>> array_data;                                                                                                                                                          \
      verbose = get_##NAME##_array_attributes(partition_name.c_str(), class_name.c_str(), attribute_name_path.c_str(), x.m_since, x.m_until, prev, next, object_mask.c_str(), is_regexp, fc, false, eov, upd, array_data, line_prefix_name);  \
      array2series(array_data, data);                                                                                                                                                                                                         \
    }                                                                                                                                                                                                                                         \
                                                                                                                                                                                                                                              \
  t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                      \
                                                                                                                                                                                                                                              \
  unsigned long dp_num = count_primitive_items(data, eov);                                                                                                                                                                                    \
  unsigned long sr_num = put_histogram_data(histogram_data, histogram_eov_size, x.m_since, x.m_until, data, eov);                                                                                                                             \
                                                                                                                                                                                                                                              \
  ers::log(ers::Message(ERS_HERE, make_log(t1, t2, std::chrono::steady_clock::now(), call_id, request_count, #NAME, x.m_is_array, dp_num, sr_num, verbose)));                                                                                 \
}                                                                                                                                                                                                                                             \
break;


bool
is_string_type(const std::string& s)
{
  static std::string str("string");
  return (str == s);
}

void
throw_incompartible_change_exception(uint64_t ts, const std::string& pname, const std::string& cname, const std::string& aname, const char * change)
{
  boost::posix_time::ptime t(boost::posix_time::from_time_t(pbeast::ts2secs(ts)));
  std::ostringstream text;
  text << "at " << t << " data type of " << aname << '@' << cname << " attribute in partition " << pname << " is changed from " << change << " (not supported by dashboard)";
  throw std::runtime_error(text.str().c_str());
}

void
pbeast::Server::http_read_series(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& requests, const std::string& plot_type, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, const std::string& show_all_updates_strs, uint64_t max_num, uint32_t ds_interval, const std::string& fill_gaps_str, const std::string& read_raw_data_strs, const std::string& is_regexp_strs, const std::string& use_fqn_strs, const std::string& fn_strs, std::ostringstream& out)
{
  if (requests.empty())
    {
      throw std::runtime_error("missing \"ids\" parameter for readSeries operation");
    }

  bool is_pie_chart = (plot_type == "pie");
  bool is_line_chart = (plot_type == "line");

  if (is_pie_chart == false && is_line_chart == false)
    {
      std::ostringstream text;
      text << "unsupported plot type: \"" << plot_type << '\"';
      throw std::runtime_error(text.str().c_str());
    }

  pbeast::FillGaps::Type fill_gaps = (fill_gaps_str.empty() ? pbeast::FillGaps::Near : pbeast::FillGaps::mk(fill_gaps_str));

  std::chrono::time_point<std::chrono::steady_clock> tg1 = std::chrono::steady_clock::now(), tg2, tg3;

  // common data specific for pie chart
  std::map<std::string, uint64_t> histogram_data;
  uint64_t histogram_eov_size(0);

  // common data specific for lines chart
  std::map<std::string, LinesData> lines_data; // [name: values]
  uint64_t num_of_data_points(0);

  std::stringstream requests_ss(requests);
  std::stringstream read_raw_data_ss(read_raw_data_strs);
  std::stringstream is_regexp_ss(is_regexp_strs);
  std::stringstream show_all_updates_ss(show_all_updates_strs);
  std::stringstream use_fqn_ss(use_fqn_strs);
  std::stringstream fn_ss(fn_strs);

  int request_count(0);

  while (requests_ss.good())
    {
      std::string request, read_raw_data_str, is_regexp_str, show_all_updates_str, use_fqn_str, fn_str;

      std::getline(requests_ss, request, ';');

      if (read_raw_data_ss.good())
        std::getline(read_raw_data_ss, read_raw_data_str, ';');

      if (is_regexp_ss.good())
        std::getline(is_regexp_ss, is_regexp_str, ';');

      if (show_all_updates_ss.good())
        std::getline(show_all_updates_ss, show_all_updates_str, ';');

      if (use_fqn_ss.good())
        std::getline(use_fqn_ss, use_fqn_str, ';');

      if (fn_ss.good())
        std::getline(fn_ss, fn_str, ';');

      request_count++;

      // skip empty requests
      if (request.empty())
        continue;

      bool read_raw_data = str2bool(read_raw_data_str, false, "readRawData", read_raw_data_strs, requests, request_count);
      bool is_regexp = str2bool(is_regexp_str, true, "isRegexp", is_regexp_strs, requests, request_count);
      bool show_all_updates = str2bool(show_all_updates_str, false, "showAllUpdates", show_all_updates_strs, requests, request_count);
      bool use_fqn = str2bool(use_fqn_str, true, "useFQN", use_fqn_strs, requests, request_count);

      pbeast::FunctionConfig fc;

      if (fn_str.empty() == false)
        {
          try
            {
              if (fn_str != "none")
                fc.construct(fn_str, 1);
            }
          catch (const std::exception& ex)
            {
              std::ostringstream text;
              text << "failed to parse function configuration = \"" << fn_str << "\" from \"functions\" parameter = \"" << fn_strs << "\": " << ex.what();
              throw pbeast::BadUserRequest(text.str());
            }
        }
      else
        {
          validate_mv_param("functions", fn_strs, requests, request_count);
        }

      std::string partition_name, class_name, attribute_name_path, object_mask;
      parse(request, partition_name, class_name, attribute_name_path, object_mask);

      validate_parameter(partition_name, "partition", "readSeries", "ids", request);
      validate_parameter(class_name, "class", "readSeries", "ids", request);
      validate_parameter(attribute_name_path, "attribute", "readSeries", "ids", request);

      std::vector<AttrInfoData> attr_type;
      get_attribute_type(partition_name.c_str(), class_name.c_str(), attribute_name_path.c_str(), attr_type);

      const unsigned long len(attr_type.size());

      if (len == 0)
        {
          throw std::runtime_error("cannot find data type attribute information");
        }

      // split search time interval on several intervals in accordance with attribute description

      struct AttrInfo
      {
        uint64_t m_since, m_until;
        const std::string m_type;
        bool m_is_array;
        AttrInfo(uint64_t s, uint64_t u, const std::string& t, bool a) :
            m_since(s), m_until(u), m_type(t), m_is_array(a)
        {
          ;
        }
      };

      std::vector<AttrInfo> info;

      if (len == 1)
        {
          info.emplace_back(since, until, attr_type[0].m_type, attr_type[0].m_is_array);
        }
      else
        {
          for (unsigned int idx = 0; idx < len; ++idx)
            {
              //
              // s, u - map current knowledges about attribute info validity on full time-line
              //
              //        [x1]...[y1]   [x2...y2]    [x3.......y3]
              //
              //  [s1]------------[u1][s2]-----[u2][s3]-------------------------[u3]
              //

              uint64_t s = (idx == 0 ? pbeast::min_ts_const : attr_type[idx].m_created);
              uint64_t u = (idx == (len - 1) ? pbeast::max_ts_const : attr_type[idx + 1].m_created - 1);

              //
              // since, until are shown as x, y
              //
              //             [s].............[u]
              //
              //  (A)    [x]--------[y]
              //  (B)    [x]------------------------[y]
              //  (C)            [x]---[y]
              //  (D)            [x]----------------[y]
              //

              if (until >= s)
                {
                  if (since <= s)
                    {
                      // case (A)
                      if (until <= u)
                        {
                          info.emplace_back(s, until, attr_type[idx].m_type, attr_type[idx].m_is_array);
                        }
                      // case (B)
                      else
                        {
                          info.emplace_back(s, u, attr_type[idx].m_type, attr_type[idx].m_is_array);
                        }
                    }
                  else if (since <= u)
                    {
                      // case (C)
                      if (until <= u)
                        {
                          info.emplace_back(since, until, attr_type[idx].m_type, attr_type[idx].m_is_array);
                        }
                      // case (D)
                      else
                        {
                          info.emplace_back(since, u, attr_type[idx].m_type, attr_type[idx].m_is_array);
                        }
                    }
                }
            }
        }

      std::string line_prefix_name_str;

      if (use_fqn)
        line_prefix_name_str = pbeast::Repository::create_fqn_prefix(partition_name, class_name, attribute_name_path);

      const char * line_prefix_name = line_prefix_name_str.c_str();

      if (is_pie_chart)
        {
          for (std::size_t idx = 0; idx < info.size(); ++idx)
            {
              const uint64_t prev = (idx == 0 ? prev_interval : 0);
              const uint64_t next = (idx != (info.size() - 1) ? 0 : next_interval);

              auto& x(info[idx]);

              std::map<std::string, std::set<uint64_t>> eov, upd;
              std::string verbose;

              switch (pbeast::str2type(x.m_type))
                {
                  case pbeast::BOOL:    READ_HISTOGRAM_DATA(bool, boolean)
                  case pbeast::S8:      READ_HISTOGRAM_DATA(int8_t, s8)
                  case pbeast::U8:      READ_HISTOGRAM_DATA(uint8_t, u8)
                  case pbeast::S16:     READ_HISTOGRAM_DATA(int16_t, s16)
                  case pbeast::U16:     READ_HISTOGRAM_DATA(uint16_t, u16)
                  case pbeast::S32:     READ_HISTOGRAM_DATA(int32_t, s32)
                  case pbeast::U32:     READ_HISTOGRAM_DATA(uint32_t, u32)
                  case pbeast::S64:     READ_HISTOGRAM_DATA(int64_t, s64)
                  case pbeast::U64:     READ_HISTOGRAM_DATA(uint64_t, u64)
                  case pbeast::FLOAT:   READ_HISTOGRAM_DATA(float, float)
                  case pbeast::DOUBLE:  READ_HISTOGRAM_DATA(double, double)
                  case pbeast::STRING:  READ_HISTOGRAM_DATA(std::string, string)

                  case pbeast::OBJECT:
                    {
                      std::ostringstream text;
                      text << "cannot read object of nested type " << x.m_type << "; provide path to an attribute of this type";
                      throw std::runtime_error(text.str().c_str());
                    }

                  default:
                    {
                      std::ostringstream text;
                      text << "data type " << x.m_type << " is not supported";
                      throw std::runtime_error(text.str().c_str());
                    }
                }
            }
        }
      else
        {
          // detect is-string and is-multi-value properties of the request
          bool is_string = info.empty() ? false : is_string_type(info[0].m_type);
          bool is_array = info.empty() ? false : info[0].m_is_array;

          for (std::size_t idx = 0; idx < info.size(); ++idx)
            {
              const uint64_t prev = (idx == 0 ? prev_interval : 0);
              const uint64_t next = (idx != (info.size() - 1) ? 0 : next_interval);

              auto& x(info[idx]);

              std::string verbose;

              if (is_string != is_string_type(x.m_type))
                {
                  throw_incompartible_change_exception(x.m_since, partition_name, class_name, attribute_name_path, (is_string ? "string to numeric" : "numeric to string"));
                }

              if (is_array != x.m_is_array)
                {
                  throw_incompartible_change_exception(x.m_since, partition_name, class_name, attribute_name_path, (is_array ? "array to single value" : "single to array value"));
                }

              uint32_t sample_interval = ds_interval ? ds_interval : pbeast::ts2secs(x.m_until - x.m_since) / max_num;

              if ((fc.no_aggregations() == false || !fill_gaps_str.empty()) && sample_interval == 0)
                sample_interval = 1;

              switch (pbeast::str2type(x.m_type))
                {
                  case pbeast::BOOL:    READ_LINES_DATA(bool, boolean)
                  case pbeast::S8:      READ_LINES_DATA(int8_t, s8)
                  case pbeast::U8:      READ_LINES_DATA(uint8_t, u8)
                  case pbeast::S16:     READ_LINES_DATA(int16_t, s16)
                  case pbeast::U16:     READ_LINES_DATA(uint16_t, u16)
                  case pbeast::S32:     READ_LINES_DATA(int32_t, s32)
                  case pbeast::U32:     READ_LINES_DATA(uint32_t, u32)
                  case pbeast::S64:     READ_LINES_DATA(int64_t, s64)
                  case pbeast::U64:     READ_LINES_DATA(uint64_t, u64)
                  case pbeast::FLOAT:   READ_LINES_DATA(float, float)
                  case pbeast::DOUBLE:  READ_LINES_DATA(double, double)
                  case pbeast::STRING:  READ_LINES_DATA(std::string, string)

                  case pbeast::OBJECT:
                    {
                      std::ostringstream text;
                      text << "cannot read object of nested type " << x.m_type << "; provide path to an attribute of this type";
                      throw std::runtime_error(text.str().c_str());
                    }

                  default:
                    {
                      std::ostringstream text;
                      text << "data type " << x.m_type << " is not supported";
                      throw std::runtime_error(text.str().c_str());
                    }
                }
            }
        }
    }

  tg2 = std::chrono::steady_clock::now();

  if (is_pie_chart)
    {
      if (!histogram_data.empty())
        {
          out << "{\"datapoints\":[";

          // sort by time duration and convert to milliseconds

          std::multimap<uint64_t, std::string> sorted;

          if (histogram_eov_size)
            {
              sorted.insert(std::pair<uint64_t, std::string>(histogram_eov_size / 1000, "[null]"));
            }

          for (auto& x : histogram_data)
            {
              uint64_t v(x.second / 1000);
              if (v == 0)
                v = 1;
              sorted.insert(std::pair<uint64_t, std::string>(v, x.first));
            }

          if (sorted.size() > max_num)
            {
              uint64_t others(0);

              while (sorted.size() > max_num)
                {
                  others += sorted.begin()->first;
                  sorted.erase(sorted.begin());
                }

              sorted.insert(std::pair<uint64_t, std::string>(others, "[others]"));
            }

          num_of_data_points = sorted.size();

          bool is_first(true);
          for (auto x = sorted.rbegin(); x != sorted.rend(); ++x)
            {
              if (is_first == false)
                out << ',';
              else
                is_first = false;

              out << "{\"label\": \"" << x->second << "\", \"data\": \"" << x->first << "\"}";
            }

          out << "],\"target\":\"" << requests << "\",\"allLoaded\": true}";
        }
      else
        {
          out << "{\"datapoints\": [], \"target\": \"" << requests << "\", \"allLoaded\": false}";
        }
    }
  else
    {
      out << "[";

      bool is_first(true);

      for (auto& t : lines_data)
        {
          if (is_first == true)
            is_first = false;
          else
            out << ',';

          out << "{\"label\":\"" << t.first << "\",";

          if (t.second.m_is_array)
            out << "\"isArray\":true,";

          if (t.second.m_is_string)
            out << "\"isString\":true,";

          out << "\"datapoints\":[" << t.second.m_json_value << "]}";
        }

      out << ']';
    }

  tg3 = std::chrono::steady_clock::now();

  auto ti = std::chrono::duration_cast<std::chrono::milliseconds>(tg3-tg1).count() / 1000.;
  uint64_t sz = out.tellp();

  add_rest(user_call_stat, static_cast<uint64_t>(ti), sz);
  ERS_LOG("generate json for request " << call_id << " in " << std::chrono::duration_cast<std::chrono::milliseconds>(tg3-tg2).count() / 1000. << " ms (" << num_of_data_points << " data points, " << sz << " bytes); total processing time " << ti << " ms");
}

static bool
match_val(const std::string& val, boost::regex * re)
{
  try
    {
      if (!re || boost::regex_match(val, *re))
        return true;
    }
  catch (std::exception& ex)
    {
      std::stringstream text;
      text << "boost::regex_match() failed for string " << val << ": " << ex.what();
      throw pbeast::BadUserRequest(text.str());
    }

  return false;
}

static void
add_val(std::map<uint64_t, std::string>& annotations_data, uint64_t ts, const std::string& val)
{
  annotations_data[ts2mks(ts)] = val;
}


template<class T>
  unsigned long
  put_annotations(std::map<std::string, std::map<uint64_t, std::string>>& annotations, const std::string& label, boost::regex * re, const std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<uint64_t>>& upds)
  {
    unsigned long num_of_formatted_data_points(0);

    if (data.empty() == false)
      {
        std::ostringstream ss;

        for (auto& s : data)
          {
            std::map<uint64_t, std::string>& annotations_data = annotations[label.empty() ? s.first : label];

            std::set<uint64_t>& upd = upds[pbeast::get_object_name(s.first)];
            auto upd_it = upd.begin();

            for (auto& v : s.second)
              {
                ss.str("");
                v.print_json_value(ss);

                const std::string val(ss.str());

                if(match_val(val, re))
                  {
                    if(upd.empty())
                      {
                        add_val(annotations_data, v.get_ts_created(), val);
                        num_of_formatted_data_points++;
                      }
                    else
                      {
                        while (upd_it != upd.end() && *upd_it < v.get_ts_created())
                          upd_it++;

                        while (upd_it != upd.end() && *upd_it <= v.get_ts_last_updated())
                          {
                            add_val(annotations_data, *upd_it, val);
                            num_of_formatted_data_points++;
                            upd_it++;
                          }
                      }
                  }
              }
          }
      }

    return num_of_formatted_data_points;
  }

#define READ_ANNOTATIONS(CPP_TYPE, NAME)                                                                                                                                                                                                       \
{                                                                                                                                                                                                                                              \
  std::chrono::time_point<std::chrono::steady_clock> t1 = std::chrono::steady_clock::now(), t2;                                                                                                                                                \
                                                                                                                                                                                                                                               \
  unsigned long dp_num;                                                                                                                                                                                                                        \
  unsigned long sr_nums;                                                                                                                                                                                                                       \
                                                                                                                                                                                                                                               \
  std::map<std::string, std::set<uint64_t>> eov, upd;                                                                                                                                                                                          \
                                                                                                                                                                                                                                               \
  if (x.m_is_array == false)                                                                                                                                                                                                                   \
    {                                                                                                                                                                                                                                          \
      std::map<std::string, std::set<pbeast::SeriesData<CPP_TYPE>>> data;                                                                                                                                                                      \
      verbose = get_##NAME##_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev, next, object_id.c_str(), is_regexp, fc, show_all_updates, eov, upd, data, line_prefix_name);           \
      t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                   \
      dp_num = count_primitive_items(data, eov);                                                                                                                                                                                               \
      sr_nums = put_annotations(annotations, merge_result ? object_id : "", value_filter_re.get(), data, upd);                                                                                                                                 \
    }                                                                                                                                                                                                                                          \
  else                                                                                                                                                                                                                                         \
    {                                                                                                                                                                                                                                          \
      std::map<std::string,std::set<pbeast::SeriesVectorData<CPP_TYPE>>> data;                                                                                                                                                                 \
      verbose = get_##NAME##_array_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev, next, object_id.c_str(), is_regexp, fc, show_all_updates, eov, upd, data, line_prefix_name);     \
      t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                   \
      dp_num = count_container_items(data, eov);                                                                                                                                                                                               \
      sr_nums = put_annotations(annotations, merge_result ? object_id : "", value_filter_re.get(), data, upd);                                                                                                                                 \
    }                                                                                                                                                                                                                                          \
                                                                                                                                                                                                                                               \
  num_of_data_points += sr_nums;                                                                                                                                                                                                               \
  ers::log(ers::Message(ERS_HERE, make_log(t1, t2, std::chrono::steady_clock::now(), call_id, 0, #NAME, x.m_is_array, dp_num, sr_nums, verbose)));                                                                                             \
}                                                                                                                                                                                                                                              \
break;

void
pbeast::Server::http_get_annotations(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, const std::string& object_id, bool is_regexp, bool merge_result, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, bool show_all_updates, uint64_t max_num, const std::string& value_filter, bool use_fqn, const std::string& fn_str, std::ostringstream& out)
{
  validate_parameter(partition_name, "partition", "getAnnotations");
  validate_parameter(class_name, "class", "getAnnotations");
  validate_parameter(attribute_name, "attribute", "getAnnotations");
  validate_parameter(object_id, "object", "getAnnotations");

  pbeast::FunctionConfig fc;

  if (fn_str.empty() == false)
    {
      try
        {
          if (fn_str != "none")
            fc.construct(fn_str, 1);
        }
      catch (const std::exception& ex)
        {
          std::ostringstream text;
          text << "failed to parse function configuration = \"" << fn_str << "\": " << ex.what();
          throw pbeast::BadUserRequest(text.str());
        }
    }

  std::unique_ptr<boost::regex> value_filter_re;

  try
    {
      if (!value_filter.empty())
        value_filter_re.reset(new boost::regex(value_filter));
    }
  catch (std::exception& ex)
    {
      std::stringstream text;
      text << "bad \'filter\' parameter, cannot parse regular expression " << value_filter << ": " << ex.what();
      throw pbeast::BadUserRequest(text.str());
    }

  std::vector<AttrInfoData> attr_type;
  get_attribute_type(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), attr_type);

  const unsigned long len(attr_type.size());

  if (len == 0)
    {
      throw std::runtime_error("cannot find data type attribute information");
    }

  // split search time interval on several intervals in accordance with attribute description

  struct AttrInfo
  {
    uint64_t m_since, m_until;
    const std::string m_type;
    bool m_is_array;
    AttrInfo(uint64_t s, uint64_t u, const std::string& t, bool a) :
        m_since(s), m_until(u), m_type(t), m_is_array(a)
    {
      ;
    }
  };

  std::vector<AttrInfo> info;

  if (len == 1)
    {
      info.emplace_back(since, until, attr_type[0].m_type, attr_type[0].m_is_array);
    }
  else
    {
      for (unsigned int idx = 0; idx < len; ++idx)
        {
          //
          // s, u - map current knowledges about attribute info validity on full time-line
          //
          //        [x1]...[y1]   [x2...y2]    [x3.......y3]
          //
          //  [s1]------------[u1][s2]-----[u2][s3]-------------------------[u3]
          //

          uint64_t s = (idx == 0 ? pbeast::min_ts_const : attr_type[idx].m_created);
          uint64_t u = (idx == (len - 1) ? pbeast::max_ts_const : attr_type[idx + 1].m_created - 1);

          //
          // since, until are shown as x, y
          //
          //             [s].............[u]
          //
          //  (A)    [x]--------[y]
          //  (B)    [x]------------------------[y]
          //  (C)            [x]---[y]
          //  (D)            [x]----------------[y]
          //

          if (until >= s)
            {
              if (since <= s)
                {
                  // case (A)
                  if (until <= u)
                    {
                      info.emplace_back(s, until, attr_type[idx].m_type, attr_type[idx].m_is_array);
                    }
                  // case (B)
                  else
                    {
                      info.emplace_back(s, u, attr_type[idx].m_type, attr_type[idx].m_is_array);
                    }
                }
              else if (since <= u)
                {
                  // case (C)
                  if (until <= u)
                    {
                      info.emplace_back(since, until, attr_type[idx].m_type, attr_type[idx].m_is_array);
                    }
                  // case (D)
                  else
                    {
                      info.emplace_back(since, u, attr_type[idx].m_type, attr_type[idx].m_is_array);
                    }
                }
            }
        }
    }

  std::string line_prefix_name_str;

  if (use_fqn)
    line_prefix_name_str = pbeast::Repository::create_fqn_prefix(partition_name, class_name, attribute_name);

  const char * line_prefix_name = line_prefix_name_str.c_str();

  std::map<std::string, std::map<uint64_t, std::string>> annotations;

  uint64_t num_of_data_points(0);

  // detect is-string and is-multi-value properties of the request
  const bool is_string = info.empty() ? false : is_string_type(info[0].m_type);
  const bool is_array = info.empty() ? false : info[0].m_is_array;

  for (std::size_t idx = 0; idx < info.size(); ++idx)
    {
      const uint64_t prev = (idx == 0 ? prev_interval : 0);
      const uint64_t next = (idx != (info.size() - 1) ? 0 : next_interval);

      auto& x(info[idx]);

      std::string verbose;

      if (is_string != is_string_type(x.m_type))
        throw_incompartible_change_exception(x.m_since, partition_name, class_name, attribute_name, (is_string ? "string to numeric" : "numeric to string"));

      if (is_array != x.m_is_array)
        throw_incompartible_change_exception(x.m_since, partition_name, class_name, attribute_name, (is_array ? "array to single value" : "single to array value"));

      switch (pbeast::str2type(x.m_type))
        {
          case pbeast::BOOL:    READ_ANNOTATIONS(bool, boolean)
          case pbeast::S8:      READ_ANNOTATIONS(int8_t, s8)
          case pbeast::U8:      READ_ANNOTATIONS(uint8_t, u8)
          case pbeast::S16:     READ_ANNOTATIONS(int16_t, s16)
          case pbeast::U16:     READ_ANNOTATIONS(uint16_t, u16)
          case pbeast::S32:     READ_ANNOTATIONS(int32_t, s32)
          case pbeast::U32:     READ_ANNOTATIONS(uint32_t, u32)
          case pbeast::S64:     READ_ANNOTATIONS(int64_t, s64)
          case pbeast::U64:     READ_ANNOTATIONS(uint64_t, u64)
          case pbeast::FLOAT:   READ_ANNOTATIONS(float, float)
          case pbeast::DOUBLE:  READ_ANNOTATIONS(double, double)
          case pbeast::STRING:  READ_ANNOTATIONS(std::string, string)

          case pbeast::OBJECT:
            {
              std::ostringstream text;
              text << "cannot read object of nested type " << x.m_type << "; provide path to an attribute of this type";
              throw std::runtime_error(text.str().c_str());
            }

          default:
            {
              std::ostringstream text;
              text << "data type " << x.m_type << " is not supported";
              throw std::runtime_error(text.str().c_str());
            }
        }
    }

  out << "[";

  bool is_first_label = true;
  for(const auto& v : annotations)
    {
      const auto& label = v.first;
      const auto& annotations_data = v.second;
      const std::size_t len = annotations_data.size();

      if (is_first_label == false)
        out << ',';
      else
        is_first_label = false;

      out << "{\"label\":\"" << label << "\",";

      if (is_array)
        out << "\"isArray\":true,";

      if (is_string)
        out << "\"isString\":true,";

      out << "\"datapoints\":[";

      // if max number of points below maximum required by use, ignore it
      if (max_num && len <= max_num)
        max_num = 0;

      const double reduction = static_cast<double>(max_num) / static_cast<double>(len);

      bool is_first(true);
      std::size_t total_idx = 0, current_idx = 0;

      const uint64_t since_mks(ts2mks(since));
      const uint64_t until_mks(ts2mks(until));

      for (const auto& x : annotations_data)
        {
          bool insert = (max_num == 0 || x.first <= since_mks || x.first >= until_mks);

          if (insert == false)
            {
              if(static_cast<std::size_t>(static_cast<double>(++current_idx) * reduction) == total_idx)
                {
                  insert = true;
                  total_idx++;
                }
            }

          if (insert)
            {
              if (is_first == false)
                out << ',';
              else
                is_first = false;

              out << '[' << x.first << ',' << x.second << ']';
            }
        }

      out << "]}";
    }

  out << ']';

}


template<typename T>
  void
  get_raw_data_time_info(const std::map<std::string, std::set<T>>& data, pbeast::DataFileResponse& dfr)
  {
    uint64_t min_value(pbeast::max_ts_const), max_value(pbeast::min_ts_const);

    for (const auto& x : data)
      {
        for (const auto& y : x.second)
          {
            if (y.get_ts_created() < min_value)
              min_value = y.get_ts_created();

            if (y.get_ts_last_updated() > max_value)
              max_value = y.get_ts_last_updated();
          }
      }

    dfr.m_started = min_value;
    dfr.m_age = max_value - min_value;
  }


template<typename T>
  void
  get_ds_data_time_info(const std::map<std::string, std::set<T>>& data, pbeast::DataFileResponse& dfr)
  {
    uint64_t min_value(pbeast::max_ts_const), max_value(pbeast::min_ts_const);

    for (const auto& x : data)
      {
        for (const auto& y : x.second)
          {
            if (y.get_ts() < min_value)
              min_value = y.get_ts();

            if (y.get_ts() > max_value)
              max_value = y.get_ts();
          }
      }

    dfr.m_started = pbeast::mk_ts(min_value);
    dfr.m_age = pbeast::mk_ts(max_value - min_value);
  }


template<typename T>
  int64_t
  save_data(pbeast::OutputDataFile& file, const std::map<std::string, std::set<T>>& data)
  {
    file.write_header();

    std::vector<T> v;

    for (auto & x : data)
      {
        v.clear();
        v.reserve(x.second.size());
        std::move(x.second.begin(), x.second.end(), std::back_inserter(v));
        file.write_data(x.first, v);
      }

    file.write_footer();
    return file.close();
  }

template<typename T>
  void
  clean_empty(std::map<std::string, std::set<T>>& data, const std::string& id)
  {
    if (data[id].empty())
      data.erase(id);
  }

#define GET_DATA(CPP_TYPE, NAME)                                                                                                                                                                                                                                                                         \
{                                                                                                                                                                                                                                                                                                        \
  std::chrono::time_point<std::chrono::steady_clock> t1 = std::chrono::steady_clock::now(), t2;                                                                                                                                                                                                          \
                                                                                                                                                                                                                                                                                                         \
  unsigned long dp_num;                                                                                                                                                                                                                                                                                  \
                                                                                                                                                                                                                                                                                                         \
  if (sample_interval == 0)                                                                                                                                                                                                                                                                              \
    {                                                                                                                                                                                                                                                                                                    \
      if (x.m_is_array == false)                                                                                                                                                                                                                                                                         \
        {                                                                                                                                                                                                                                                                                                \
          std::map<std::string, std::set<pbeast::SeriesData<CPP_TYPE>>> data;                                                                                                                                                                                                                            \
          verbose = get_##NAME##_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev, next, object.c_str(), is_regexp, fc, show_all_updates, eov64, upd64, data, line_prefix_name);                                                                \
          t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                                                                         \
          dp_num = count_primitive_items(data, eov64);                                                                                                                                                                                                                                                   \
          if (data.empty() == false)                                                                                                                                                                                                                                                                     \
            {                                                                                                                                                                                                                                                                                            \
              get_raw_data_time_info(data, *dfr);                                                                                                                                                                                                                                                        \
              pbeast::OutputDataFile file(&dfr->m_data, partition_name, class_name, class_name, attribute_name, pbeast::ts2secs(dfr->m_started), pbeast::str2type(x.m_type), x.m_is_array, data.size(), zip_catalog, zip_data, cstr, 0);                                                                 \
              data_size += save_data(file, data);                                                                                                                                                                                                                                                        \
              out->m_data.emplace_back(dfr);                                                                                                                                                                                                                                                             \
            }                                                                                                                                                                                                                                                                                            \
          else                                                                                                                                                                                                                                                                                           \
            {                                                                                                                                                                                                                                                                                            \
              delete dfr;                                                                                                                                                                                                                                                                                \
            }                                                                                                                                                                                                                                                                                            \
        }                                                                                                                                                                                                                                                                                                \
      else                                                                                                                                                                                                                                                                                               \
        {                                                                                                                                                                                                                                                                                                \
          std::map<std::string,std::set<pbeast::SeriesVectorData<CPP_TYPE>>> data;                                                                                                                                                                                                                       \
          verbose = get_##NAME##_array_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev, next, object.c_str(), is_regexp, fc, show_all_updates, eov64, upd64, data, line_prefix_name);                                                          \
          t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                                                                         \
          dp_num = count_container_items(data, eov64);                                                                                                                                                                                                                                                   \
          if (data.empty() == false)                                                                                                                                                                                                                                                                     \
            {                                                                                                                                                                                                                                                                                            \
              get_raw_data_time_info(data, *dfr);                                                                                                                                                                                                                                                        \
              pbeast::OutputDataFile file(&dfr->m_data, partition_name, class_name, class_name, attribute_name, pbeast::ts2secs(dfr->m_started), pbeast::str2type(x.m_type), x.m_is_array, data.size(), zip_catalog, zip_data, cstr, 0);                                                                 \
              data_size += save_data(file, data);                                                                                                                                                                                                                                                        \
              out->m_data.emplace_back(dfr);                                                                                                                                                                                                                                                             \
            }                                                                                                                                                                                                                                                                                            \
          else                                                                                                                                                                                                                                                                                           \
            {                                                                                                                                                                                                                                                                                            \
              delete dfr;                                                                                                                                                                                                                                                                                \
            }                                                                                                                                                                                                                                                                                            \
        }                                                                                                                                                                                                                                                                                                \
    }                                                                                                                                                                                                                                                                                                    \
  else                                                                                                                                                                                                                                                                                                   \
    {                                                                                                                                                                                                                                                                                                    \
      if (x.m_is_array == false)                                                                                                                                                                                                                                                                         \
        {                                                                                                                                                                                                                                                                                                \
          std::map<std::string, std::set<pbeast::DownsampledSeriesData<CPP_TYPE>>> data;                                                                                                                                                                                                                 \
          verbose = get_downsample_##NAME##_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev, next, sample_interval, fill_gaps, object.c_str(), is_regexp, fc, show_all_updates, eov32, upd32, data, line_prefix_name);                         \
          t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                                                                         \
          dp_num = count_primitive_items(data, eov32);                                                                                                                                                                                                                                                   \
          if (data.empty() == false)                                                                                                                                                                                                                                                                     \
            {                                                                                                                                                                                                                                                                                            \
              get_ds_data_time_info(data, *dfr);                                                                                                                                                                                                                                                         \
              pbeast::OutputDataFile file(&dfr->m_data, partition_name, class_name, class_name, attribute_name, pbeast::ts2secs(dfr->m_started), pbeast::str2type(x.m_type), x.m_is_array, data.size(), zip_catalog, zip_data, cstr, sample_interval);                                                   \
              data_size += save_data(file, data);                                                                                                                                                                                                                                                        \
              out->m_data.emplace_back(dfr);                                                                                                                                                                                                                                                             \
            }                                                                                                                                                                                                                                                                                            \
          else                                                                                                                                                                                                                                                                                           \
            {                                                                                                                                                                                                                                                                                            \
              delete dfr;                                                                                                                                                                                                                                                                                \
            }                                                                                                                                                                                                                                                                                            \
        }                                                                                                                                                                                                                                                                                                \
      else                                                                                                                                                                                                                                                                                               \
        {                                                                                                                                                                                                                                                                                                \
          std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<CPP_TYPE>>> data;                                                                                                                                                                                                            \
          verbose = get_downsample_##NAME##_array_attributes(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), x.m_since, x.m_until, prev, next, sample_interval, fill_gaps, object.c_str(), is_regexp, fc, show_all_updates, eov32, upd32, data, line_prefix_name);                   \
          t2 = std::chrono::steady_clock::now();                                                                                                                                                                                                                                                         \
          dp_num = count_container_items(data, eov32);                                                                                                                                                                                                                                                   \
          if (data.empty() == false)                                                                                                                                                                                                                                                                     \
            {                                                                                                                                                                                                                                                                                            \
              get_ds_data_time_info(data, *dfr);                                                                                                                                                                                                                                                         \
              pbeast::OutputDataFile file(&dfr->m_data, partition_name, class_name, class_name, attribute_name, pbeast::ts2secs(dfr->m_started), pbeast::str2type(x.m_type), x.m_is_array, data.size(), zip_catalog, zip_data, cstr, sample_interval);                                                   \
              data_size += save_data(file, data);                                                                                                                                                                                                                                                        \
              out->m_data.emplace_back(dfr);                                                                                                                                                                                                                                                             \
            }                                                                                                                                                                                                                                                                                            \
          else                                                                                                                                                                                                                                                                                           \
            {                                                                                                                                                                                                                                                                                            \
              delete dfr;                                                                                                                                                                                                                                                                                \
            }                                                                                                                                                                                                                                                                                            \
        }                                                                                                                                                                                                                                                                                                \
    }                                                                                                                                                                                                                                                                                                    \
    ers::log(ers::Message(ERS_HERE, make_log(t1, t2, std::chrono::steady_clock::now(), call_id, 0, #NAME, x.m_is_array, dp_num, 0, verbose)));                                                                                                                                                           \
}                                                                                                                                                                                                                                                                                                        \
break;


static int64_t
data2file(const std::map<std::string, std::set<uint64_t>>& data, pbeast::DataFileResponse& dfr, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, bool zip_catalog, bool zip_data, bool cstr)
{
  pbeast::OutputDataFile file(&dfr.m_data, partition_name, class_name, attribute_name, attribute_name, pbeast::ts2secs(dfr.m_started), pbeast::VOID, false, data.size(), zip_catalog, zip_data, cstr, 0);

  file.write_header();

  std::vector<pbeast::SeriesData<pbeast::Void>> v;

  for (auto & x : data)
    {
      v.clear();
      v.reserve(x.second.size());
      for (auto & y : x.second)
        v.emplace_back(y, y, pbeast::Void());
      file.write_data(x.first, v);
    }

  file.write_footer();
  return file.close();
}

static int64_t
data2file(const std::map<std::string, std::set<uint32_t>>& data, pbeast::DataFileResponse& dfr, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, uint32_t sample_interval, bool zip_catalog, bool zip_data, bool cstr)
{
  pbeast::OutputDataFile file(&dfr.m_data, partition_name, class_name, attribute_name, attribute_name, pbeast::ts2secs(dfr.m_started), pbeast::VOID, false, data.size(), zip_catalog, zip_data, cstr, sample_interval);

  file.write_header();

  std::vector<pbeast::DownsampledSeriesData<pbeast::Void>> v;

  for (auto & x : data)
    {
      v.clear();
      v.reserve(x.second.size());
      for (auto & y : x.second)
        v.emplace_back(y, pbeast::Void(), pbeast::Void(), pbeast::Void());
      file.write_data(x.first, v);
    }

  file.write_footer();
  return file.close();
}

void
pbeast::Server::http_get_data(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, const std::string& object, bool is_regexp, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, bool show_all_updates, uint32_t sample_interval, const std::string& fill_gaps_str, bool zip_catalog, bool zip_data, bool cstr, bool use_fqn, const std::string& fn_str, MultiDataFileResponseServer*& out)
{
  validate_parameter(partition_name, "partition", "getData");
  validate_parameter(class_name, "class", "getData");
  validate_parameter(attribute_name, "attribute", "getData");

  if (is_regexp == false)
    validate_parameter(attribute_name, "object", "getData");

  pbeast::FillGaps::Type fill_gaps = (fill_gaps_str.empty() ? pbeast::FillGaps::Near : pbeast::FillGaps::mk(fill_gaps_str));

  pbeast::FunctionConfig fc;

  if (fn_str.empty() == false)
    try
      {
        fc.construct(fn_str, sample_interval);
      }
    catch (const std::exception& ex)
      {
        std::ostringstream text;
        text << "failed to parse \"functions\" parameter = \"" << fn_str << "\": " << ex.what();
        throw pbeast::BadUserRequest(text.str());
      }

  std::vector<AttrInfoData> attr_type;
  get_attribute_type(partition_name.c_str(), class_name.c_str(), attribute_name.c_str(), attr_type);

  const unsigned long len(attr_type.size());

  if (len == 0)
    {
      throw std::runtime_error("cannot find data type attribute information");
    }

  out = new MultiDataFileResponseServer();

  // split search time interval on several intervals in accordance with attribute description

  struct AttrInfo
  {
    uint64_t m_since, m_until;
    const std::string m_type;
    bool m_is_array;
    AttrInfo(uint64_t s, uint64_t u, const std::string& t, bool a) :
        m_since(s), m_until(u), m_type(t), m_is_array(a)
    {
      ;
    }
  };

  std::vector<AttrInfo> info;

  if (len == 1)
    {
      info.emplace_back(since, until, attr_type[0].m_type, attr_type[0].m_is_array);
    }
  else
    {
      for (unsigned int idx = 0; idx < len; ++idx)
        {
          //
          // s, u - map current knowledges about attribute info validity on full time-line
          //
          //        [x1]...[y1]   [x2...y2]    [x3.......y3]
          //
          //  [s1]------------[u1][s2]-----[u2][s3]-------------------------[u3]
          //

          uint64_t s = (idx == 0 ? pbeast::min_ts_const : attr_type[idx].m_created);
          uint64_t u = (idx == (len - 1) ? pbeast::max_ts_const : attr_type[idx + 1].m_created - 1);

          //
          // since, until are shown as x, y
          //
          //             [s].............[u]
          //
          //  (A)    [x]--------[y]
          //  (B)    [x]------------------------[y]
          //  (C)            [x]---[y]
          //  (D)            [x]----------------[y]
          //

          if (until >= s)
            {
              if (since <= s)
                {
                  // case (A)
                  if (until <= u)
                    {
                      info.emplace_back(s, until, attr_type[idx].m_type, attr_type[idx].m_is_array);
                    }
                  // case (B)
                  else
                    {
                      info.emplace_back(s, u, attr_type[idx].m_type, attr_type[idx].m_is_array);
                    }
                }
              else if (since <= u)
                {
                  // case (C)
                  if (until <= u)
                    {
                      info.emplace_back(since, until, attr_type[idx].m_type, attr_type[idx].m_is_array);
                    }
                  // case (D)
                  else
                    {
                      info.emplace_back(since, u, attr_type[idx].m_type, attr_type[idx].m_is_array);
                    }
                }
            }
        }
    }

  std::string line_prefix_name_str;

  if (use_fqn)
    line_prefix_name_str = pbeast::Repository::create_fqn_prefix(partition_name, class_name, attribute_name);

  const char * line_prefix_name = line_prefix_name_str.c_str();

  int64_t data_size(0);

  auto t1 = std::chrono::steady_clock::now();

  std::map<std::string, std::set<uint64_t>> eov64, upd64;
  std::map<std::string, std::set<uint32_t>> eov32, upd32;

  for (std::size_t idx = 0; idx < info.size(); ++idx)
    {
      const uint64_t prev = (idx == 0 ? prev_interval : 0);
      const uint64_t next = (idx != (info.size() - 1) ? 0 : next_interval);

      auto& x(info[idx]);

      std::string verbose;

      pbeast::DataFileResponse * dfr = new pbeast::DataFileResponse(std::ios_base::out);

      switch (pbeast::str2type(x.m_type))
        {
          case pbeast::BOOL:    GET_DATA(bool, boolean)
          case pbeast::S8:      GET_DATA(int8_t, s8)
          case pbeast::U8:      GET_DATA(uint8_t, u8)
          case pbeast::S16:     GET_DATA(int16_t, s16)
          case pbeast::U16:     GET_DATA(uint16_t, u16)
          case pbeast::ENUM:
          case pbeast::S32:     GET_DATA(int32_t, s32)
          case pbeast::DATE:
          case pbeast::U32:     GET_DATA(uint32_t, u32)
          case pbeast::S64:     GET_DATA(int64_t, s64)
          case pbeast::TIME:
          case pbeast::U64:     GET_DATA(uint64_t, u64)
          case pbeast::FLOAT:   GET_DATA(float, float)
          case pbeast::DOUBLE:  GET_DATA(double, double)
          case pbeast::STRING:  GET_DATA(std::string, string)

          case pbeast::OBJECT:
            {
              std::ostringstream text;
              text << "cannot read object of nested type " << x.m_type << "; provide path to an attribute of this type";
              throw std::runtime_error(text.str().c_str());
            }

          default:
            {
              std::ostringstream text;
              text << "data type " << x.m_type << " is not supported";
              throw std::runtime_error(text.str().c_str());
            }
        }
    }


  std::ostringstream void_info;

    {
      int64_t eov_size(0), upd_size(0);

      if (!eov64.empty())
        eov_size = data2file(eov64, out->m_eov, partition_name, class_name, attribute_name, zip_catalog, zip_data, cstr);
      else if (!eov32.empty())
        eov_size = data2file(eov32, out->m_eov, partition_name, class_name, attribute_name, sample_interval, zip_catalog, zip_data, cstr);

      if (eov_size)
        void_info << ", EoV data (" << eov_size << " bytes)";

      if (!upd64.empty())
        upd_size = data2file(upd64, out->m_upd, partition_name, class_name, attribute_name, zip_catalog, zip_data, cstr);
      else if (!upd32.empty())
        upd_size = data2file(upd32, out->m_upd, partition_name, class_name, attribute_name, sample_interval, zip_catalog, zip_data, cstr);

      if (upd_size)
        void_info << ", update-info data (" << upd_size << " bytes)";
    }

  auto ti = std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-t1).count() / 1000.;

  add_api(user_call_stat, static_cast<uint64_t>(ti), data_size);
  ERS_LOG("request " << call_id << " returns " << out->m_data.size() << " data file(s) (" << data_size << " bytes)" << void_info.str() << " in " << ti << " ms");
}
