#ifndef PBEAST_RECEIVER_DATA_PROVIDER_H
#define PBEAST_RECEIVER_DATA_PROVIDER_H

#include <set>
#include <string>
#include <future>
#include <mutex>
#include <shared_mutex>
#include <filesystem>

#include <ipc/partition.h>
#include <ipc/namedobject.h>

#include "pbeast/writer_lock.h"
#include "pbeast/data-type.h"
#include "pbeast/thread_pool.h"
#include "pbeast/pbeast.hh"

#include "receiver_monitoring.h"

struct CheckProvidersThread;
struct SaveDataThread;
struct MonitoringThread;


namespace pbeast {
  namespace receiver {

    class Attribute;
    class Class;
    class Config;
    class Partition;

    class DataProvider : public IPCNamedObject<POA_pbeast::server>
    {

      friend struct ::CheckProvidersThread;
      friend struct ::SaveDataThread;
      friend struct ::MonitoringThread;
      friend class Object;

      public:

        DataProvider(const std::string& name, const std::filesystem::path& path, const IPCPartition& partition, int thread_pool_size);
        virtual ~DataProvider();

        void shutdown();

        virtual const Config& get_config () = 0;

        void write_ready(Partition& p, Class& c, Attribute& a, uint64_t ts_save_delay);

        void rebuild_all_eov(uint64_t ts_eov_delay);
        void compact_all_data(uint64_t ts_cb_delay);
        void commit_all_data(uint64_t ts_save_delay);

        std::pair<uint64_t, const Attribute *>
        compact_data(Attribute * a, uint64_t ts_cb_delay, const std::map<std::string, std::set<uint64_t>*> * eov);


        void write_ready_data();
        void start_write_thread(unsigned int max_save_delay);

        void start_monitoring_thread(unsigned int sleep_interval);

        void set_global_object();
        void unset_global_object();

        virtual DataProvider * get_this() = 0;

        Partition * get_partition(const std::string& name) const;
        Partition * define_partition(const std::string& name);
        Class * get_class(const std::string& name, const Partition * partition = m_default_partition) const;

        void check_directory(Partition& p, Class& c);

        static Monitoring& get_monitoring_object() { return *s_monitor; }

        static inline void
        add_data_size(uint64_t v)
        {
          m_total_data_size += v;
          m_data_size.insert(v);
        }


      protected:

        void ipc_withdraw();
        void save_all_data();

        void save_class(Partition& p, Class& c);

        void enqueue_for_compaction(Class& c, uint64_t ts_cb_delay, std::vector<std::future<std::pair<uint64_t, const pbeast::receiver::Attribute *>>>& results);
        void write_class(Partition& p, Class& c, uint64_t ts_save_delay);

        static void link_class_types(Partition * partition);


      private:

        // get partition by name; throw exception if no such partition
        pbeast::receiver::Partition * check_partition(const char* partition_name);

        // get class by name; throw exception if no such class
        pbeast::receiver::Class * check_and_get_class(const char* partition_name, const char* class_name);

        // get attribute by name; throw exception if no such attribute
        pbeast::receiver::Attribute * check_and_get_attribute(const char* partition_name, const char* class_name, const char* attribute_name);

        // check attribute type; throw exception if no such attribute or type mismatches
        void check_attribute(const pbeast::receiver::Attribute * a, const char* partition_name, const char* class_name, const char* attribute_name, pbeast::DataType type, bool is_array);


      public:

        // pbeast receiver IDL methods

        void send_command(const char* command_name, const ::pbeast::StringArray& command_args, ::CORBA::String_out command_output);

        void get_partitions(StringArray_out partitions);
        void get_classes(const char* partition_name, StringArray_out classes);
        void get_attributes(const char* partition_name, const char* class_name, StringDataPointArrayList_out attributes);
        void get_attribute_info(const char* partition_name, const char* class_name, const char* attribute_name, S32DataPointList_out type, BooleanDataPointList_out is_array, StringDataPointList_out description);
        void get_attribute_type(const char* partition_name, const char* class_name, const char* attribute_name, AttributeTypeList_out info);
        void get_schema(const char* partition_name, ClassInfoList_out schema);

        void get_updated_objects(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, const char* object_mask, StringArray_out objects);

        void get_eovs(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out data);

        void get_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanObjectList_out data);
        void get_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ObjectList_out data);
        void get_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ObjectList_out data);
        void get_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ObjectList_out data);
        void get_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ObjectList_out data);
        void get_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ObjectList_out data);
        void get_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ObjectList_out data);
        void get_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ObjectList_out data);
        void get_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ObjectList_out data);
        void get_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatObjectList_out data);
        void get_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleObjectList_out data);
        void get_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringObjectList_out data);
        void get_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanArrayObjectList_out data);
        void get_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ArrayObjectList_out data);
        void get_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ArrayObjectList_out data);
        void get_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ArrayObjectList_out data);
        void get_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ArrayObjectList_out data);
        void get_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ArrayObjectList_out data);
        void get_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ArrayObjectList_out data);
        void get_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ArrayObjectList_out data);
        void get_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ArrayObjectList_out data);
        void get_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatArrayObjectList_out data);
        void get_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleArrayObjectList_out data);
        void get_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringArrayObjectList_out data);

        void get_downsample_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleObjectList_out data);
        void get_downsample_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S8DownSampleObjectList_out data);
        void get_downsample_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U8DownSampleObjectList_out data);
        void get_downsample_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S16DownSampleObjectList_out data);
        void get_downsample_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U16DownSampleObjectList_out data);
        void get_downsample_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S32DownSampleObjectList_out data);
        void get_downsample_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U32DownSampleObjectList_out data);
        void get_downsample_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S64DownSampleObjectList_out data);
        void get_downsample_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U64DownSampleObjectList_out data);
        void get_downsample_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleObjectList_out data);
        void get_downsample_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleObjectList_out data);
        void get_downsample_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::StringDownSampleObjectList_out data);
        void get_downsample_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleArrayObjectList_out data);
        void get_downsample_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S8DownSampleArrayObjectList_out data);
        void get_downsample_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U8DownSampleArrayObjectList_out data);
        void get_downsample_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S16DownSampleArrayObjectList_out data);
        void get_downsample_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U16DownSampleArrayObjectList_out data);
        void get_downsample_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S32DownSampleArrayObjectList_out data);
        void get_downsample_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U32DownSampleArrayObjectList_out data);
        void get_downsample_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::S64DownSampleArrayObjectList_out data);
        void get_downsample_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::U64DownSampleArrayObjectList_out data);
        void get_downsample_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleArrayObjectList_out data);
        void get_downsample_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleArrayObjectList_out data);
        void get_downsample_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov, pbeast::V32ObjectList_out upd, pbeast::StringDownSampleArrayObjectList_out data);


      protected:

        const std::filesystem::path m_repository_path;
        pbeast::ThreadPool m_thread_pool;
        std::mutex m_thread_pool_mutex;

        const IPCPartition& m_partition;
        std::string m_default_dir_partition_name;

        mutable std::shared_mutex m_partitions_mutex;
        std::map<std::string, Partition *> m_partitions;
        static Partition * m_default_partition;

        std::mutex m_readers_mutex;

        static std::multiset<uint64_t> m_data_size;
        static uint64_t m_total_data_size;

        pbeast::WriterLock m_writer_lock;
        Monitoring m_monitor;

        uint64_t m_started_at;

        std::mutex m_mutex;

        static constexpr uint64_t s_iov_infinity = 0xffffffffffffffff;

        static DataProvider * s_data_provider;
        static boost::shared_mutex s_data_provider_mutex;

        static Monitoring * s_monitor;


      private:

        void get_v64(pbeast::receiver::Attribute * a, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out& data);
        void get_v32(pbeast::receiver::Attribute * a, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, const char* object_mask, bool is_regexp, pbeast::V32ObjectList_out& data);

        void get_upds(bool fill_upd, const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out data);

        void get_downsample_eovs(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, const char* object_mask, bool is_regexp, pbeast::V32ObjectList_out data);
        void get_downsample_upds(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, const char* object_mask, bool is_regexp, bool fill_upd, pbeast::V32ObjectList_out data);


      private:

        void commit_command(const std::string& partition_name, const std::string& class_name, const std::vector<std::string>& attribute_names, std::ostringstream& log);
        Attribute * find_attribute(Partition& p, Class& c, const std::string& name);

    };

  }
}

#endif
