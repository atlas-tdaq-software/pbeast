#ifndef PBEAST_REPOSITORY_SERVER_MERGE_ERRORS_H
#define PBEAST_REPOSITORY_SERVER_MERGE_ERRORS_H

#include <pbeast/exceptions.h>

namespace daq
{
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotGetFileSize,
    Exception,
    "cannot get size of file " << path << ": " << why,
    ,
    ((std::filesystem::path)path)
    ((std::string)why)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    UnexpectedFileSize,
    Exception,
    "size of file " << path << " was " << size_was << " bytes and now it has decreased to " << size_now << " bytes",
    ,
    ((std::filesystem::path)path)
    ((uint64_t)size_was)
    ((uint64_t)size_now)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    IncompatibleFiles,
    Exception,
    "file \"" << file1 << "\" is incompatible with file \"" << file2 << "\": " << what,
    ,
    ((std::string)file1)
    ((std::string)file2)
    ((const char *)what)
  )


  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileAlreadyExist,
    Exception,
    "file " << path << " already exist",
    ,
    ((std::filesystem::path)path)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    BadDataFile,
    Exception,
    "bad file \"" << file << "\": " << why,
    ,
    ((std::string)file)
    ((const char *)why)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    ProblematicDataFiles,
    Exception,
    "problematic files " << files << ": " << why,
    ,
    ((std::string)files)
    ((const char *)why)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    OutOfDateDiskInfo,
    Exception,
    "the disk information on node " << name << " was updated last time " << date << " (more than one day ago); check repository server running on this node",
    ,
    ((std::string)name)
    ((std::string)date)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    NoDiskInfo,
    Exception,
    "the disk information on merger node " << name << " is missing; check repository server running on this node",
    ,
    ((std::string)name)
  )

  ERS_DECLARE_ISSUE_BASE(pbeast, CancelRepositoryBalancing, Exception, "cancel repository balancing", ERS_EMPTY, ERS_EMPTY)

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileNotCopied,
    Exception,
    "file " << path << " does not exist (check Castor script)",
    ,
    ((std::filesystem::path)path)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    RemoteCopyFailed,
    Exception,
    "cannot copy file " << file << " on remote node " << node << ": " << why,
    ,
    ((std::filesystem::path)file)
    ((std::string)node)
    ((std::string)why)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    ClassRepositoryBalancingError,
    Exception,
    "class \"" << name << '@' << partition << "\" cannot be balanced for data split index " << idx,
    ,
    ((std::string)name)
    ((std::string)partition)
    ((uint64_t)idx)
  )
}

#endif
