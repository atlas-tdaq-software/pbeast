#include <errno.h>
#include <string.h>

#include <iostream>
#include <set>
#include <string>
#include <thread>

#include <filesystem>
#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <system/Host.h>

#include <pmg/pmg_initSync.h>
#include <ipc/alarm.h>
#include <ipc/signal.h>

#include <config/Configuration.h>
#include <config/Errors.h>
#include <dal/Partition.h>
#include <dal/util.h>

#include <pbeast/data-file.h>
#include <pbeast/service_lock.h>
#include <pbeast/repository.h>
#include <pbeast/meta.h>

#include "application_exceptions.h"
#include "repository_server.h"
#include "repository_server_monitoring.h"
#include "utils.h"

#include "pbeast/PBeastRepositoryApplication.h"
#include "pbeast/PBeastDownsampleCacheConfig.h"
#include "pbeast/PBeastMergerConfig.h"

pbeast::RepositoryServer * pbeast::RepositoryServer::s_instance(nullptr);

namespace po = boost::program_options;


static std::string
trim_right_slash(const std::string& path)
{
  std::string str(path);

  unsigned int i = str.size();
  while (i > 0 && str[i - 1] == '/')
    --i;
  str.erase(i, str.size());

  return str;
}

int
main(int argc, char ** argv)
{
  // initialize IPC and set default CORBA parameters

  try
    {
      auto opts = IPCCore::extractOptions( argc, argv );

      opts.emplace_front("threadPerConnectionPolicy", "0");
      opts.emplace_front("maxServerThreadPoolSize", "16");
      opts.emplace_front("threadPoolWatchConnection", "0");

      IPCCore::init(opts);
    }
  catch (const ers::Issue & ex)
    {
      ers::fatal(ex);
      return 1;
    }


  // command line parameters

  std::string application_id;
  std::string conf_db;
  bool create_sync_file(false);

  if (const char * env = getenv("TDAQ_APPLICATION_OBJECT_ID"))
    {
      application_id = env;
    }

  po::options_description desc("The P-BEAST Repository Service provides CORBA access to local and merged repositories and performs merging of data files.");

  try
    {
      desc.add_options()
        (
          "data,d",
          po::value<std::string>(&conf_db),
          "Name of the configuration database (also TDAQ_DB process environment variable)"
        )
        (
          "application-id,n",
          po::value<std::string>(&application_id)->default_value(application_id),
          "Name (id) of repository service application object (also TDAQ_APPLICATION_OBJECT_ID process environment variable)"
        )
        (
          "create-pmg-sync-file,s",
          "create pmg synchronization file at startup"
        )
        (
          "help,h",
          "Print help message"
        );

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      if (vm.count("create-pmg-sync-file"))
        {
          create_sync_file = true;
        }

      po::notify(vm);
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << ex.what() << "\nUsage: " << desc;
      ers::fatal(daq::pbeast::CommandLineError(ERS_HERE, text.str()));
      return 1;
    }


  // read configuration

  pbeast::RepositoryServerConfig config;

  try
    {
      Configuration db(conf_db);

      const std::string initial("initial");

      if (const daq::core::Partition * partition = daq::core::get_partition(db, initial))
        {
          db.register_converter(new daq::core::SubstituteVariables(*partition));
        }
      else
        {
          throw daq::pbeast::FailedReadConfiguration(ERS_HERE, initial, daq::core::Partition::s_class_name );
        }

      const pbeast::PBeastRepositoryApplication * app = db.get<pbeast::PBeastRepositoryApplication>(application_id);

      if (app == nullptr)
        {
          throw daq::pbeast::FailedReadConfiguration(ERS_HERE, application_id, pbeast::PBeastRepositoryApplication::s_class_name );
        }

      ERS_DEBUG( 1, "read configuration object " << app << ":\n" << *app);

      config.m_local_repository_path = trim_right_slash(app->get_LocalRepositoryPath());
      config.m_repository_path = trim_right_slash(app->get_RepositoryPath());
      config.m_publish_local_repository_info = app->get_PublishLocalRepositoryInfo();
      config.m_publish_repository_info = app->get_PublishRepositoryInfo();
      config.m_max_response_size = pbeast::utils::str2size(app->get_MaxResponseSize(), pbeast::PBeastRepositoryApplication::s_MaxResponseSize);
      config.m_max_local_ds_data_size = pbeast::utils::str2size(app->get_MaxResponseSize(), pbeast::PBeastRepositoryApplication::s_MaxLocalDsDataSize);
      config.m_series_info_max_size = pbeast::utils::str2size(app->get_SeriesInfoMaxSize(), pbeast::PBeastRepositoryApplication::s_SeriesInfoMaxSize);

      if(config.m_max_response_size > 0x7fffffff)
        {
          std::ostringstream text;
          text << "the value of \'" << pbeast::PBeastRepositoryApplication::s_MaxResponseSize << "\' of " << app << " exceeds 2G limit";
          throw std::runtime_error(text.str().c_str());
        }

      std::ostringstream config_report;

      config_report << std::boolalpha <<
          "repository server configuration:\n"
          "  local repository directory     : " << config.m_local_repository_path << "\n"
          "  repository directory           : " << config.m_repository_path << "\n"
          "  publish local repository info  : " << config.m_publish_local_repository_info << "\n"
          "  publish repository info        : " << config.m_publish_repository_info << "\n"
          "  max response size              : " << config.m_max_response_size << " bytes\n"
          "  max local ds data size         : " << config.m_max_local_ds_data_size << " bytes\n"
          "  series info max in-memory size : " << config.m_series_info_max_size << " bytes\n"
          "  downsample config              :";

      if(const pbeast::PBeastDownsampleCacheConfig * cache_config = app->get_DownsampleCacheConfig())
        {
          config.m_cache_ds_config.init(trim_right_slash(cache_config->get_CachePath()), cache_config->get_DownsampleThreadsPoolSize(), pbeast::utils::str2size(cache_config->get_CacheMaxSize(), pbeast::PBeastDownsampleCacheConfig::s_CacheMaxSize));
          config.m_cache_ds_config.init_zip(cache_config->get_ZipCatalog(), cache_config->get_ZipStringTypeData(), cache_config->get_ZipIntegerTypeData(), cache_config->get_ZipFloatingPointTypeData(), cache_config->get_ZipVoidData(), cache_config->get_CatalogizeStringsData());

          config_report << "\n"
              "    cache directory              : " << config.m_cache_ds_config.get_cache_path() << " (max size: " << config.m_cache_ds_config.get_max_cache_size() << " bytes)\n"
              "    zip catalog                  : " << config.m_cache_ds_config.get_zip_catalog() << "\n"
              "    zip integer type data        : " << config.m_cache_ds_config.get_zip_integer_data() << "\n"
              "    zip floating point type data : " << config.m_cache_ds_config.get_zip_float_data() << "\n"
              "    zip void data                : " << config.m_cache_ds_config.get_zip_void_data() << "\n"
              "    zip string type data         : " << config.m_cache_ds_config.get_zip_string_data() << "\n"
              "    catalogize string            : " << config.m_cache_ds_config.get_catalogize_strings();
        }
      else
        config_report << " (null)";

      config_report << "\n  merger config                  :";

      if(const pbeast::PBeastMergerConfig * merger = app->get_MergerConfiguration())
        {
          config.m_merger_zip_config.init_zip(merger->get_ZipCatalog(), merger->get_ZipStringTypeData(), merger->get_ZipIntegerTypeData(), merger->get_ZipFloatingPointTypeData(), merger->get_ZipVoidData(), merger->get_CatalogizeStringsData());

          config.m_eos_repository_directory = trim_right_slash(merger->get_EOSRepositoryDirectory());
          config.m_sleep_interval = merger->get_SleepInterval();
          config.m_data_split_duration = pbeast::utils::str2size(merger->get_DataSplitDuration(), pbeast::PBeastMergerConfig::s_DataSplitDuration);
          config.m_data_group_max_size = pbeast::utils::str2size(merger->get_DataGroupMaxSize(), pbeast::PBeastMergerConfig::s_DataGroupMaxSize);
          config.m_max_receiver_delay = merger->get_MaxReceiverDelay() * 3600 + 100;
          config.m_disk_balance_threshold = static_cast<double>(merger->get_DiskBalanceThreshold()) / 100.0;

          config.m_cache_downsample_threshold = (config.m_data_split_duration * 8) / 10; // automatically downsample data, if one had requested them recently (80% of interval)

          config_report << "\n"
              "    eos repository directory     : " << config.m_eos_repository_directory << "\n"
              "    sleep interval               : " << config.m_sleep_interval << "\n"
              "    data split_duration          : " << config.m_data_split_duration << "\n"
              "    data group max size          : " << config.m_data_group_max_size << " bytes\n"
              "    max receiver delay           : " << config.m_max_receiver_delay << "\n"
              "    disk balance threshold       : " << config.m_disk_balance_threshold << "\n"
              "    disk balancing               : " << (config.m_disk_balance_enabled ? "enabled" : "disabled") << "\n"
              "    cache downsample threshold   : " << config.m_cache_downsample_threshold << "\n"
              "    zip catalog                  : " << config.m_merger_zip_config.get_zip_catalog() << "\n"
              "    zip integer type data        : " << config.m_merger_zip_config.get_zip_integer_data() << "\n"
              "    zip floating point type data : " << config.m_merger_zip_config.get_zip_float_data() << "\n"
              "    zip string type data         : " << config.m_merger_zip_config.get_zip_string_data() << "\n"
              "    catalogize string            : " << config.m_merger_zip_config.get_catalogize_strings();
        }
      else
        config_report << " (null)\n";

      ERS_LOG(config_report.str());

      // FIXME: decide on writer_lock merger settings
//          m_writer_lock.set_read_timeout(1000);
//          m_writer_lock.set_read_retry_interval(500);
    }
  catch (const ers::Issue& ex)
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE, ex));
      return 1;
    }
  catch (const std::exception& ex)
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE, ex));
      return 1;
    }


  try
    {
      std::filesystem::path lock_path(config.m_local_repository_path);

      if (lock_path.empty())
        lock_path = config.m_cache_ds_config.get_cache_path();

      if (lock_path.empty())
        lock_path = std::filesystem::temp_directory_path();

      pbeast::ServiceLock repository_server_lock(lock_path, "repository-service.lock");

      IPCPartition initial;

        {
          pbeast::RepositoryServer * srv = new pbeast::RepositoryServer(config, initial, application_id);

          srv->init();

          if (create_sync_file)
            pmg_initSync();

          auto sig_received = daq::ipc::signal::wait_for();

          ERS_LOG("Caught signal " << sig_received << ", exiting...");

          srv->_destroy(true);
        }
    }
  catch (const ers::Issue& ex)
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE, ex));
      return 1;
    }

  return 0;
}

pbeast::RepositoryServerConfig::RepositoryServerConfig() :
    m_publish_local_repository_info(false), m_publish_repository_info(false), m_max_response_size(0), m_max_local_ds_data_size(0), m_sleep_interval(0), m_data_split_duration(0), m_data_group_max_size(0), m_series_info_max_size(0), m_max_receiver_delay(0), m_disk_balance_enabled(false), m_disk_balance_threshold(0.0), m_cache_downsample_threshold(0)
{
  if (char *s = getenv("TDAQ_PBEAST_ENABLE_REPO_BALANCING"))
    m_disk_balance_enabled = strcmp(s, "no");
}


pbeast::RepositoryServer::RepositoryServer(const RepositoryServerConfig& config, IPCPartition& partition, const std::string& id) :
    RepositoryServerConfig(config),
    IPCNamedObject<POA_pbeast::server>(partition, id),
    m_partition(partition),
    m_db(m_repository_path, m_local_repository_path, m_cache_ds_config),
    m_merger_alarm(nullptr),
    m_host_name(System::LocalHost::instance()->full_local_name()),
    m_count(0),
    m_max_idx(0),
    m_last_max_idx(0)
{
  m_monitor = new repository_server::Monitoring(name() + '@' + partition.name(), m_repository_path);
  s_instance = this;
}

pbeast::RepositoryServer::~RepositoryServer()
{
  try
    {
      delete m_merger_alarm;
      stop_monitoring();
      withdraw();
    }
  catch (ers::Issue & ex)
    {
      ers::error(daq::pbeast::WithdrawFailed(ERS_HERE, ex ) );
    }
}

void
pbeast::RepositoryServer::shutdown()
{
  ERS_DEBUG(0, "shutdown request received");
  daq::ipc::signal::raise();
}

void
pbeast::RepositoryServer::init()
{
  m_db.init_lock();

  try
    {
      publish();
    }
  catch (ers::Issue & ex)
    {
      throw daq::pbeast::PublishFailed(ERS_HERE, ex);
    }

  // publish calls statistics every 20" and disk info every 5' (if necessary)
  start_monitoring_thread(pbeast::repository_server::Monitoring::mon_sleep_inteval);

  if (m_cache_ds_config.get_max_cache_size())
    start_cache_cleaner_thread(24 * 3600, m_cache_ds_config.get_max_cache_size());

  start_series_info_cleaner_thread(300, m_series_info_max_size);

  if (is_merger_enabled())
    {
      m_merger_alarm = new IPCAlarm(m_sleep_interval, boost::bind(&pbeast::RepositoryServer::merger_action, this));
    }
}


struct MonitoringThread
{
  const unsigned int m_sleep_interval;
  pbeast::RepositoryServer * m_srv;


  MonitoringThread(unsigned int sleep_interval, pbeast::RepositoryServer *s) :
      m_sleep_interval(sleep_interval), m_srv(s)
  {
    ;
  }

  void
  operator()()
  {
    while (m_srv->needs_monitoring())
      {
        sleep(m_sleep_interval);

        if (m_srv->init_monitoring())
          m_srv->publish_monitoring_info();
      }
  }
};


void
pbeast::RepositoryServer::start_monitoring_thread(unsigned int sleep_interval)
{
  MonitoringThread obj(sleep_interval, this);
  std::thread thrd(obj);
  thrd.detach();
}

void
pbeast::RepositoryServer::stop_monitoring()
{
  std::lock_guard<std::mutex> lock(m_mon_thread_mutex);

  m_monitor->stop_monitoring();

  ERS_LOG("stop monitoring");
}


bool
pbeast::RepositoryServer::init_monitoring()
{
  std::lock_guard<std::mutex> lock(m_mon_thread_mutex);

  if (m_monitor && m_monitor->is_running() == true)
    return m_monitor->init_monitoring();
  else
    return false;
}

bool
pbeast::RepositoryServer::needs_monitoring()
{
  std::lock_guard<std::mutex> lock(m_mon_thread_mutex);

  return (m_monitor && m_monitor->is_running());
}


void
pbeast::RepositoryServer::publish_monitoring_info()
{
  std::lock_guard<std::mutex> lock(m_mon_thread_mutex);

  if (m_monitor && m_monitor->is_running() == true)
    {
      uint32_t now = time(0);
      const bool publish_disk_info((m_publish_repository_info || m_publish_local_repository_info) && (now - m_monitor->get_disk_info_object_pub_ts()) >= pbeast::repository_server::Monitoring::mon_disk_info_sleep_inteval);

      m_monitor->publish_calls_stats();

      if (publish_disk_info)
        m_monitor->publish_disk_info();

      if (m_monitor->commit("publish"))
        {
          if (publish_disk_info)
            m_monitor->set_disk_info_object_pub_ts(now);
        }
    }
}


static std::string
make_read_error_text(const char * what, const std::exception& ex)
{
  std::string text("failed to read ");
  text.append(what);
  text.append("\n\twas caused by: ");
  text.append(ex.what());
  return text;
}


struct CacheFileInfo
{
  CacheFileInfo(const std::filesystem::path& name, uint64_t size) :
      m_name(name), m_size(size)
  {
    ;
  }

  std::filesystem::path m_name;
  uint64_t m_size;
};


static void
get_file_stat(const std::filesystem::path& file, struct stat * buf)
{
  if(stat(file.c_str(), buf) != 0)
    {
      int error_code = errno;
      errno = 0;

      std::ostringstream text;
      text << "stat(" << file << ") failed with code " << error_code << ": " << pbeast::utils::err2str(error_code);
      throw std::runtime_error(text.str().c_str());
    }
}


static void
add_cache_files(const std::filesystem::path& path, std::multimap<time_t,CacheFileInfo>& files)
{
  std::filesystem::directory_iterator end_iter;

  try
    {
      for (std::filesystem::directory_iterator ip(path); ip != end_iter; ++ip)
        {
          if (std::filesystem::is_directory(ip->status()))
            {
              add_cache_files(ip->path(), files);
            }
          else if(std::filesystem::is_regular_file(ip->status()))
            {
              struct stat buf;
              get_file_stat(ip->path(), &buf);
              files.insert ( std::pair<uint32_t,CacheFileInfo>(buf.st_atime,CacheFileInfo(ip->path(),buf.st_size)) );
            }
        }
    }
  catch (const std::filesystem::filesystem_error & ex)
    {
      ers::error(daq::pbeast::FileSystemReadError(ERS_HERE, ex.code().message()));
    }
  catch (const std::exception & ex)
    {
      ers::error(daq::pbeast::FileSystemReadError(ERS_HERE, ex.what()));
    }
}


struct CacheCleanerThread
{
  unsigned int m_sleep_interval;
  uint64_t m_max_cache_dir_size;
  uint64_t m_cache_downsample_threshold;
  pbeast::Repository& m_db;


  CacheCleanerThread(unsigned int sleep_interval, uint64_t size, uint32_t cache_downsample_threshold, pbeast::Repository& db) :
      m_sleep_interval(sleep_interval), m_max_cache_dir_size(size), m_cache_downsample_threshold(cache_downsample_threshold), m_db(db)
  {
    ;
  }

  void
  operator()()
  {
    while (true)
      {
        std::multimap<time_t,CacheFileInfo> files;

        add_cache_files(m_db.get_downsample_config().get_cache_path(), files);

        // count total size
        uint64_t total_size(0);
        uint32_t total_removed(0);

        for (auto & x : files)
          total_size += x.second.m_size;

        ERS_LOG("Current size of cache directory: " << total_size);

        if (total_size > m_max_cache_dir_size)
          {
            std::ostringstream message;
            message << "Clean cache directory:\n";

            std::unique_lock lock(m_db.m_ds_cache_files);

            while (total_size > m_max_cache_dir_size)
              {
                std::multimap<time_t, CacheFileInfo>::iterator it = files.begin();

                try
                  {
                    std::filesystem::remove(it->second.m_name);
                    message << " * remove file " << it->second.m_name << ' ' << it->second.m_size << " bytes last accessed " << boost::posix_time::to_simple_string(boost::posix_time::from_time_t(it->first)) << std::endl;

                    if (pbeast::Directory * dir = m_db.get_directory(it->second.m_name.parent_path(), false))
                      dir->remove_file(it->second.m_name);
                  }
                catch (std::filesystem::filesystem_error& ex)
                  {
                    ers::error(daq::pbeast::FileSystemError(ERS_HERE, "remove file", it->second.m_name, ex.code().message()));
                  }

                total_size -= it->second.m_size;
                files.erase(it);
                total_removed++;
              }

            if (total_removed)
              {
                message << "Removed " << total_removed << " cache files\nNew size of cache directory: " << total_size;
                ERS_LOG(message.str());
              }
          }

        pbeast::RepositoryServer::instance().m_monitor->set_downsampled_data_cache_size(total_size);

        sleep(m_sleep_interval);
      }
  }
};


void
pbeast::RepositoryServer::start_cache_cleaner_thread(unsigned int sleep_interval, uint64_t max_size)
{
  CacheCleanerThread obj(sleep_interval, max_size, m_cache_downsample_threshold, m_db);
  std::thread thrd(obj);
  thrd.detach();
}


struct SeriesInfoCleanerThread
{
  unsigned int m_sleep_interval;
  uint64_t m_max_series_info_size;
  pbeast::Repository& m_db;


  SeriesInfoCleanerThread(unsigned int sleep_interval, uint64_t size, pbeast::Repository& db) :
      m_sleep_interval(sleep_interval), m_max_series_info_size(size), m_db(db)
  {
    ;
  }

  void
  operator()()
  {
    while (true)
      {
        // clean cached series info
        pbeast::RepositoryServer::instance().m_monitor->set_series_info_cache_size(m_db.clean_series_info(m_max_series_info_size));
        sleep(m_sleep_interval);
      }
  }
};

void
pbeast::RepositoryServer::start_series_info_cleaner_thread(unsigned int sleep_interval, uint64_t max_size)
{
  SeriesInfoCleanerThread obj(sleep_interval, max_size, m_db);
  std::thread thrd(obj);
  thrd.detach();
}


static void
dir2set(const std::filesystem::path& path, std::set<std::string>& data)
{
  std::filesystem::directory_iterator end_iter;

  try
    {
      for (std::filesystem::directory_iterator ip(path); ip != end_iter; ++ip)
        {
          if (std::filesystem::is_directory(ip->status()))
            {
              data.insert(pbeast::DataFile::decode(ip->path().filename().string()));
            }
        }
    }
  catch (std::exception & ex)
    {
      std::ostringstream text;
      text << "failed to read directory " << path << ": " << ex.what();
      throw pbeast::Failure(text.str().c_str());
    }
}

void
pbeast::RepositoryServer::send_command(const char* command_name, const ::pbeast::StringArray& command_args, ::CORBA::String_out command_output)
{
  command_output = pbeast::utils::string_dup("pbeast::RepositoryServer::send_command: not implemented yet");
}

void
pbeast::RepositoryServer::get_partitions(StringArray_out partitions)
{
  std::set<std::string> data;

  if(!m_local_repository_path.empty())
    dir2set(m_local_repository_path, data);

  dir2set(m_repository_path, data);

  partitions = new StringArray();
  partitions->length(data.size());

  unsigned int idx(0);
  for(auto & x : data)
    {
      (*partitions)[idx++] = CORBA::string_dup(x.c_str());
    }
}


void
pbeast::RepositoryServer::get_classes(const char* name, StringArray_out classes)
{
  const std::string p_name(name);

  try
    {
      pbeast::DataFile::validate_parameters(p_name, "", "");
    }
  catch (const daq::pbeast::BadParameter &ex)
    {
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());
    }

  const std::string partition_name(pbeast::DataFile::encode(p_name));

  const std::filesystem::path path(m_repository_path / partition_name);
  const std::filesystem::path local_path(m_local_repository_path / partition_name);

  const bool dir_exists(std::filesystem::exists(path));
  const bool local_dir_exists(!m_local_repository_path.empty() && std::filesystem::exists(local_path));

  if (!dir_exists && !local_dir_exists)
    {
      std::ostringstream text;
      text << "there is no partition " << partition_name;
      throw pbeast::NotFound(text.str().c_str());
    }

  std::set<std::string> data;

  if (local_dir_exists)
    dir2set(local_path, data);

  if (dir_exists)
    dir2set(path, data);

  classes = new StringArray();
  classes->length(data.size());

  unsigned int idx(0);
  for (auto & x : data)
    {
      (*classes)[idx++] = CORBA::string_dup(x.c_str());
    }
}

static void
throw_failed_read_meta(const char* partition_name, const std::string& class_name, const std::filesystem::path& dir, const std::string& why)
{
  std::ostringstream text;
  text << "failed to read meta information for " << class_name << '@' << partition_name << " in repository " << dir << why;
  throw pbeast::Failure(text.str().c_str());
}

static pbeast::Meta
read_meta_info(const char* partition_name, const std::string& class_name, const std::filesystem::path& dir, const std::filesystem::path& local_dir)
{
  try
    {
      pbeast::Meta info(dir, partition_name, class_name);

      if (!local_dir.empty())
        {
          try
            {
              pbeast::Meta local_info(local_dir, partition_name, class_name);
              info.merge(local_info);
              return info;
            }
          catch (const daq::pbeast::NoSuchClass& local_ex)
            {
              ; // OK, read data from '$dir'
            }
          catch (const daq::pbeast::Exception & ex)
            {
              throw_failed_read_meta(partition_name, class_name, local_dir, pbeast::utils::err2str(ex));
            }
        }

      return info;
    }
  catch (const pbeast::Failure&)
    {
      throw;
    }
  catch (const daq::pbeast::BadParameter &ex)
    {
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());
    }
  catch (daq::pbeast::NoSuchClass& ex)
    {
      if (!local_dir.empty())
        {
          try
            {
              pbeast::Meta local_info(local_dir, partition_name, class_name);
              return local_info;
            }
          catch (const daq::pbeast::NoSuchClass& local_ex)
            {
              ; // FAIL, throw pbeast::NotFound below
            }
          catch (const daq::pbeast::Exception & ex)
            {
              throw_failed_read_meta(partition_name, class_name, local_dir, pbeast::utils::err2str(ex));
            }
        }

      std::ostringstream text;
      text << "there is no class " << class_name << " in partition " << partition_name;
      throw pbeast::NotFound(text.str().c_str());
    }
  catch (const daq::pbeast::Exception & ex)
    {
      throw_failed_read_meta(partition_name, class_name, dir, pbeast::utils::err2str(ex));
    }

  // may not reach this line; make compiler happy
  throw pbeast::Failure("internal error in read_meta_info()");
}


void
pbeast::RepositoryServer::get_attributes(const char* partition_name, const char* class_name, StringDataPointArrayList_out attributes)
{
  attributes = new StringDataPointArrayList();

  pbeast::Meta info = read_meta_info(partition_name, class_name, m_repository_path, m_local_repository_path);

  std::set<uint64_t> changes;
  std::map<uint64_t, std::set<std::string>> attrs;

  for (auto & x : info.get_all_types())
    {
      for (auto & y : x.second)
        {
          changes.insert(y.get_ts_created());
          changes.insert(y.get_ts_last_updated());
        }
    }

  unsigned int count(0);
  for (auto & t : changes)
    {
      std::set<std::string>& at = attrs[t];

      for (auto & x : info.get_all_types())
        {
          for (auto & y : x.second)
            {
              if (y.get_ts_created() <= t && (y.get_ts_last_updated() > t || y.get_ts_created() == y.get_ts_last_updated()))
                {
                  at.insert(x.first);
                  break;
                }
            }
        }

      if (at.empty() == false)
        {
          count++;
        }
    }

  attributes->length(count);

  unsigned int idx(0);
  for (auto & x : attrs)
    {
      if (idx > 0 && (*attributes)[idx - 1].ts.updated == 0)
        {
          (*attributes)[idx - 1].ts.updated = x.first;
        }

      if (unsigned int size = x.second.size())
        {
          (*attributes)[idx].ts.created = x.first;
          (*attributes)[idx].ts.updated = 0;
          (*attributes)[idx].data.length(size);
          unsigned int j(0);
          for (auto & y : x.second)
            {
              (*attributes)[idx].data[j++] = CORBA::string_dup(y.c_str());
            }
          idx++;
        }
    }

  if (idx > 0 && (*attributes)[idx - 1].ts.updated == 0)
    {
      (*attributes)[idx - 1].ts.updated = pbeast::mk_ts(time(0), 0);
    }
}

void
pbeast::RepositoryServer::get_attribute_info(const char* partition_name, const char* class_name, const char* attribute_name_path, S32DataPointList_out type, BooleanDataPointList_out is_array, StringDataPointList_out description)
{
  type = new S32DataPointList();
  is_array = new BooleanDataPointList();
  description = new StringDataPointList();

  std::string attribute_class_name, attribute_name;

  pbeast::Meta::get_attribute_class_and_name(attribute_name_path, attribute_class_name, attribute_name);
  if(attribute_class_name.empty()) attribute_class_name = class_name;

  pbeast::Meta info = read_meta_info(partition_name, attribute_class_name, m_repository_path, m_local_repository_path);

  try
    {
      const std::vector<SeriesData<std::string>> & types = info.get_attribute_types(attribute_name);

      type->length(types.size());
      is_array->length(types.size());

      unsigned int idx(0);
      for(auto & x : types)
        {
          std::string type_str;
          bool is_array_var;

          pbeast::Meta::decode_type(x.get_value(), type_str, is_array_var);

          (*is_array)[idx].data = is_array_var;
          (*type)[idx].data = pbeast::str2type(type_str);

          (*is_array)[idx].ts.created = (*type)[idx].ts.created = x.get_ts_created();
          (*is_array)[idx].ts.updated = (*type)[idx].ts.updated = x.get_ts_last_updated();
          idx++;
        }


      const std::vector<SeriesData<std::string>> & descriptions = info.get_attribute_descriptions(attribute_name);

      description->length(descriptions.size());

      idx = 0;
      for(auto & x : descriptions)
        {
          (*description)[idx].data = CORBA::string_dup(x.get_value().c_str());
          (*description)[idx].ts.created = x.get_ts_created();
          (*description)[idx].ts.updated = x.get_ts_last_updated();
          idx++;
        }
    }
  catch (const daq::pbeast::NoSuchAttribute & ex)
    {
      std::ostringstream text;
      text << "there is no attribute " << attribute_name_path<< " in class " << class_name << " in partition " << partition_name;
      throw pbeast::NotFound(text.str().c_str());
    }

}

void
pbeast::RepositoryServer::get_attribute_type(const char* partition_name, const char* class_name, const char* attribute_name_path, AttributeTypeList_out data)
{
  data = new AttributeTypeList();

  std::string attribute_class_name, attribute_name;

  pbeast::Meta::get_attribute_class_and_name(attribute_name_path, attribute_class_name, attribute_name);
  if(attribute_class_name.empty()) attribute_class_name = class_name;

  pbeast::Meta info = read_meta_info(partition_name, attribute_class_name, m_repository_path, m_local_repository_path);

  try
    {
      const std::vector<SeriesData<std::string>> & types = info.get_attribute_types(attribute_name);

      data->length(types.size());

      unsigned int idx(0);
      for(auto & x : types)
        {
          std::string type_str;
          bool is_array_var;

          pbeast::Meta::decode_type(x.get_value(), type_str, is_array_var);

          (*data)[idx].is_array = is_array_var;
          (*data)[idx].type = CORBA::string_dup(type_str.c_str());

          (*data)[idx].ts.created = x.get_ts_created();
          (*data)[idx].ts.updated = x.get_ts_last_updated();
          idx++;
        }
    }
  catch (const daq::pbeast::NoSuchAttribute & ex)
    {
      std::ostringstream text;
      text << "there is no attribute " << attribute_name_path<< " in class " << class_name << " in partition " << partition_name;
      throw pbeast::NotFound(text.str().c_str());
    }
}

void
pbeast::RepositoryServer::get_schema(const char* name, ClassInfoList_out schema)
{
  const std::string p_name(name);

  try
    {
      pbeast::DataFile::validate_parameters(p_name, "", "");
    }
  catch (const daq::pbeast::BadParameter &ex)
    {
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());
    }

  // read classes

  const std::string partition_name(pbeast::DataFile::encode(p_name));

  const std::filesystem::path path(m_repository_path / partition_name);
  const std::filesystem::path local_path(m_local_repository_path / partition_name);

  const bool dir_exists(std::filesystem::exists(path));
  const bool local_dir_exists(!m_local_repository_path.empty() && std::filesystem::exists(local_path));

  if (!dir_exists && !local_dir_exists)
    {
      std::ostringstream text;
      text << "there is no partition " << partition_name;
      throw pbeast::NotFound(text.str().c_str());
    }

  std::set<std::string> data;

  if (local_dir_exists)
    dir2set(local_path, data);

  if (dir_exists)
    dir2set(path, data);

  schema = new ClassInfoList();
  schema->length(data.size());

  // read attributes per class

  unsigned int idx(0);
  for (auto & x : data)
    {
      (*schema)[idx].name = CORBA::string_dup(x.c_str());
      (*schema)[idx].ts = 0;

      pbeast::Meta info = read_meta_info(partition_name.c_str(), x, m_repository_path, m_local_repository_path);

      (*schema)[idx].attributes.length(info.get_all_types().size());

      unsigned int i(0);
      for (auto & y : info.get_all_types())
        {
          (*schema)[idx].attributes[i].name = CORBA::string_dup(y.first.c_str());

          auto last_type = *y.second.rbegin();
          std::string type_str;
          bool is_array_var;

          pbeast::Meta::decode_type(last_type.get_value(), type_str, is_array_var);

          (*schema)[idx].attributes[i].is_array = is_array_var;
          (*schema)[idx].attributes[i].type = CORBA::string_dup(type_str.c_str());

          auto it = info.get_all_descriptions().find(y.first);
          if(it != info.get_all_descriptions().end())
            (*schema)[idx].attributes[i].description = CORBA::string_dup((*it->second.rbegin()).get_value().c_str());
          else
            (*schema)[idx].attributes[i].description = CORBA::string_dup("unknown");

          if(last_type.get_ts_last_updated() > (*schema)[idx].ts)
            (*schema)[idx].ts = last_type.get_ts_last_updated();

          i++;
        }

      idx++;
    }
}


void
pbeast::RepositoryServer::get_updated_objects(const char* partition_name, const char* class_name, const char* attribute_name_path, ::CORBA::ULongLong since, ::CORBA::ULongLong until, const char* object_mask, pbeast::StringArray_out objects)
{
  std::set<std::string> data;

  try
    {
      MonCall mon_obj(m_max_response_size);
      m_db.list_updated_objects(data, partition_name, class_name, attribute_name_path, since, until, object_mask, mon_obj.stat());
    }
  catch(daq::pbeast::NoAttributeDirectory& ex)
    {
      throw pbeast::NotFound(ex.what());
    }
  catch(daq::pbeast::RegularExpressionError& ex)
    {
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());
    }
  catch(daq::pbeast::BadParameter& ex)
    {
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());
    }
  catch(std::filesystem::filesystem_error &ex)
    {
      std::ostringstream text;
      text << "there is no attribute " << attribute_name_path << ", class " << class_name << " or partition " << partition_name << "\n\twas caused by: " << ex.what();
      throw pbeast::NotFound(text.str().c_str());
    }
  catch(std::exception& ex)
    {
      std::ostringstream text;
      text << "failed to get list of updated object for attribute " << attribute_name_path << '@' << class_name << "\n\twas caused by: " << ex.what();
      throw pbeast::Failure(text.str().c_str());
    }

  objects = new pbeast::StringArray();

  objects->length(data.size());

  unsigned int idx(0);
  for(auto & x : data)
    {
      objects[idx++] = CORBA::string_dup(x.c_str());
    }
}


static void
fill_v64(const std::map<std::string, std::set<uint64_t>>& val, pbeast::V64ObjectList_out& to)
{
  to = new pbeast::V64ObjectList();
  to->length(val.size());

  unsigned int idx(0);
  for(auto & x : val)
    {
      to[idx].object_id = CORBA::string_dup(x.first.c_str());
      to[idx].data.length(x.second.size());
      unsigned int j(0);
      for(auto & y : x.second)
        {
          to[idx].data[j++] = y;
        }
      idx++;
    }
}

#define ASSIGN_PRIMITIVE_OBJ_TYPE    (*data)[count].data[idx].data = x.get_value();
#define ASSIGN_STRING_OBJ_TYPE       (*data)[count].data[idx].data = CORBA::string_dup(x.get_value().c_str());
#define ASSIGN_PRIMITIVE_OBJ_VECTOR  int idx2(0); (*data)[count].data[idx].data.length(x.get_value().size()); for(const auto & y : x.get_value()) { ((*data)[count].data[idx].data)[idx2++] = y; }
#define ASSIGN_STRING_OBJ_VECTOR     int idx2(0); (*data)[count].data[idx].data.length(x.get_value().size()); for(const auto & y : x.get_value()) { ((*data)[count].data[idx].data)[idx2++] = CORBA::string_dup(y.c_str()); }

static void
validate_is_empty(const char* select_functions)
{
  if (strlen(select_functions) != 0)
    throw pbeast::BadRequest("functions are not supported by the repository server application");
}

static void
validate_ds_interval(uint32_t interval)
{
  if (interval == 0)
    throw pbeast::BadRequest("downsampling interval cannot be 0");
}


#define GET_ALL( LIST_TYPE, PBEAST_TYPE, IS_ARRAY, CPP_TYPE, SERIE, ASSIGN_CODE )                                                                                                                  \
{                                                                                                                                                                                                  \
  validate_is_empty(select_functions);                                                                                                                                                             \
  try                                                                                                                                                                                              \
    {                                                                                                                                                                                              \
      MonRawCall mon_obj(m_max_response_size);                                                                                                                                                     \
                                                                                                                                                                                                   \
      std::map<std::string, std::set<uint64_t>> eov_val, upd_val;                                                                                                                                  \
      std::map<std::string, std::set<uint64_t>> * upd_ptr(fill_upd ? &upd_val : nullptr);                                                                                                          \
      std::map<std::string,std::vector<pbeast::SERIE< CPP_TYPE >>> val;                                                                                                                            \
                                                                                                                                                                                                   \
      m_db.read_attribute<pbeast::SERIE< CPP_TYPE >>(partition_name, class_name, attribute_name, since, until, prev_interval, next_interval, object_mask, is_regexp, "", false, val, eov_val, upd_ptr, mon_obj.stat()); \
      fill_v64(eov_val, eov);                                                                                                                                                                      \
      fill_v64(upd_val, upd);                                                                                                                                                                      \
                                                                                                                                                                                                   \
      data = new pbeast::LIST_TYPE();                                                                                                                                                              \
      data->length(val.size());                                                                                                                                                                    \
                                                                                                                                                                                                   \
      unsigned int count(0);                                                                                                                                                                       \
      for(auto & y : val)                                                                                                                                                                          \
        {                                                                                                                                                                                          \
          (*data)[count].object_id = CORBA::string_dup(y.first.c_str());                                                                                                                           \
          (*data)[count].data.length(y.second.size());                                                                                                                                             \
          for(unsigned int idx(0); idx < y.second.size(); ++idx)                                                                                                                                   \
            {                                                                                                                                                                                      \
              auto & x(y.second[idx]);                                                                                                                                                             \
              (*data)[count].data[idx].ts.created = x.get_ts_created();                                                                                                                            \
              (*data)[count].data[idx].ts.updated = x.get_ts_last_updated();                                                                                                                       \
              ASSIGN_CODE                                                                                                                                                                          \
            }                                                                                                                                                                                      \
          count++;                                                                                                                                                                                 \
        }                                                                                                                                                                                          \
    }                                                                                                                                                                                              \
  catch(daq::pbeast::NoAttributeDirectory& ex)                                                                                                                                                     \
    {                                                                                                                                                                                              \
      throw pbeast::NotFound(ex.what());                                                                                                                                                           \
    }                                                                                                                                                                                              \
  catch(daq::pbeast::RegularExpressionError& ex)                                                                                                                                                   \
    {                                                                                                                                                                                              \
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());                                                                                                                                \
    }                                                                                                                                                                                              \
  catch(daq::pbeast::BadParameter& ex)                                                                                                                                                             \
    {                                                                                                                                                                                              \
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());                                                                                                                                \
    }                                                                                                                                                                                              \
  catch(daq::pbeast::FileTypeError& ex)                                                                                                                                                            \
    {                                                                                                                                                                                              \
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());                                                                                                                                \
    }                                                                                                                                                                                              \
  catch(daq::pbeast::MaxMessageSizeError& ex)                                                                                                                                                      \
    {                                                                                                                                                                                              \
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());                                                                                                                                \
    }                                                                                                                                                                                              \
  catch(std::exception& ex)                                                                                                                                                                        \
    {                                                                                                                                                                                              \
      throw pbeast::Failure(make_read_error_text("attribute", ex).c_str());                                                                                                                        \
    }                                                                                                                                                                                              \
}


#define GET_SIMPLE_MANY(LIST_TYPE, PBEAST_TYPE, CPP_TYPE, ASSIGN_CODE)  \
  GET_ALL( LIST_TYPE, PBEAST_TYPE, false, CPP_TYPE, SeriesData, ASSIGN_CODE )

#define GET_VECTOR_MANY(LIST_TYPE, PBEAST_TYPE, CPP_TYPE, ASSIGN_CODE)  \
  GET_ALL( LIST_TYPE, PBEAST_TYPE, true, CPP_TYPE, SeriesVectorData, ASSIGN_CODE )


static void
fill_eov32(const std::map<std::string, std::set<uint32_t>>& val, pbeast::V32ObjectList_out& to)
{
  to = new pbeast::V32ObjectList();
  to->length(val.size());

  unsigned int idx(0);
  for(auto & x : val)
    {
      to[idx].object_id = CORBA::string_dup(x.first.c_str());
      to[idx].data.length(x.second.size());
      unsigned int j(0);
      for(auto & y : x.second)
        {
          to[idx].data[j++] = y;
        }
      idx++;
    }
}


void
pbeast::RepositoryServer::get_eovs(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out eov)
{
  std::unique_ptr<boost::regex> re;

  if (is_regexp)
    {
      try
        {
          if (pbeast::utils::is_regexp(object_mask))
            re.reset(new boost::regex(object_mask));
        }
      catch (std::exception& ex)
        {
          std::ostringstream text;
          text << "error parsing regular expression \'" << object_mask << "\': " << ex.what();
          throw pbeast::BadRequest(text.str().c_str());
        }
    }
  else if (*object_mask == 0)
    {
      throw pbeast::BadRequest("empty non-regex object name");
    }


  std::map<std::string, std::set<uint64_t>> eov_val;

  try
    {
      std::shared_lock lock(m_db.m_repository_files);

      pbeast::CallStats dummy;

      if(is_regexp)
        m_db.read_eov(partition_name, class_name, since, until, prev_interval, next_interval, re.get(), eov_val, dummy);
      else
        m_db.read_eov(partition_name, class_name, since, until, prev_interval, next_interval, object_mask, eov_val[object_mask], dummy);
    }
  catch(std::filesystem::filesystem_error &ex)
    {
      std::ostringstream text;
      text << "there is no class " << class_name << " or partition " << partition_name << ": " << ex.what();
      throw pbeast::NotFound(text.str().c_str());
    }
  catch(std::exception& ex)
    {
      throw pbeast::Failure(make_read_error_text("EoVs", ex).c_str());
    }

  fill_v64(eov_val, eov);
}


void
pbeast::RepositoryServer::get_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanObjectList_out data)
{
  GET_SIMPLE_MANY(BooleanObjectList, BOOL, bool, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ObjectList_out data)
{
  GET_SIMPLE_MANY(S8ObjectList, S8, int8_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ObjectList_out data)
{
  GET_SIMPLE_MANY(U8ObjectList, U8, uint8_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ObjectList_out data)
{
  GET_SIMPLE_MANY(S16ObjectList, S16, int16_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ObjectList_out data)
{
  GET_SIMPLE_MANY(U16ObjectList, U16, uint16_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ObjectList_out data)
{
  GET_SIMPLE_MANY(S32ObjectList, S32, int32_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ObjectList_out data)
{
  GET_SIMPLE_MANY(U32ObjectList, U32, uint32_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ObjectList_out data)
{
  GET_SIMPLE_MANY(S64ObjectList, S64, int64_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ObjectList_out data)
{
  GET_SIMPLE_MANY(U64ObjectList, U64, uint64_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatObjectList_out data)
{
  GET_SIMPLE_MANY(FloatObjectList, FLOAT, float, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleObjectList_out data)
{
  GET_SIMPLE_MANY(DoubleObjectList, DOUBLE, double, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringObjectList_out data)
{
  GET_SIMPLE_MANY(StringObjectList, STRING, std::string, ASSIGN_STRING_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanArrayObjectList_out data)
{
  GET_VECTOR_MANY(BooleanArrayObjectList, BOOL, bool, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S8ArrayObjectList, S8, int8_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U8ArrayObjectList, U8, uint8_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S16ArrayObjectList, S16, int16_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U16ArrayObjectList, U16, uint16_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S32ArrayObjectList, S32, int32_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U32ArrayObjectList, U32, uint32_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S64ArrayObjectList, S64, int64_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U64ArrayObjectList, U64, uint64_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatArrayObjectList_out data)
{
  GET_VECTOR_MANY(FloatArrayObjectList, FLOAT, float, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleArrayObjectList_out data)
{
  GET_VECTOR_MANY(DoubleArrayObjectList, DOUBLE, double, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringArrayObjectList_out data)
{
  GET_VECTOR_MANY(StringArrayObjectList, STRING, std::string, ASSIGN_STRING_OBJ_VECTOR)
}


#define GET_DS_ALL( LIST_TYPE, PBEAST_TYPE, IS_ARRAY, CPP_TYPE, SERIE, ASSIGN_CODE )                                                                                                                                            \
{                                                                                                                                                                                                                               \
  validate_is_empty(select_functions);                                                                                                                                                                                          \
  validate_ds_interval(interval);                                                                                                                                                                                               \
  try                                                                                                                                                                                                                           \
    {                                                                                                                                                                                                                           \
      MonDsCall mon_obj(m_max_response_size);                                                                                                                                                                                   \
      MonRawCall mon_local_obj(m_max_local_ds_data_size);                                                                                                                                                                       \
                                                                                                                                                                                                                                \
      std::map<std::string, std::set<uint32_t>> eov_val, upd_val;                                                                                                                                                               \
      std::map<std::string,std::vector<pbeast::SERIE< CPP_TYPE >>> val;                                                                                                                                                         \
                                                                                                                                                                                                                                \
      m_db.read_downsample_attribute<pbeast::SERIE< CPP_TYPE >>(partition_name, class_name, attribute_name, since, until, prev_interval, next_interval, interval, static_cast<pbeast::FillGaps::Type>(fill_gaps), object_mask, is_regexp, "", false, val, eov_val, fill_upd ? &upd_val : nullptr, mon_obj.stat(), mon_local_obj.stat()); \
      fill_eov32(eov_val, eov);                                                                                                                                                                                                 \
      fill_eov32(upd_val, upd);                                                                                                                                                                                                 \
                                                                                                                                                                                                                                \
      data = new pbeast::LIST_TYPE();                                                                                                                                                                                           \
      data->length(val.size());                                                                                                                                                                                                 \
                                                                                                                                                                                                                                \
      unsigned int count(0);                                                                                                                                                                                                    \
      for(auto & y : val)                                                                                                                                                                                                       \
        {                                                                                                                                                                                                                       \
          (*data)[count].object_id = CORBA::string_dup(y.first.c_str());                                                                                                                                                        \
          (*data)[count].data.length(y.second.size());                                                                                                                                                                          \
          for(unsigned int idx(0); idx < y.second.size(); ++idx)                                                                                                                                                                \
            {                                                                                                                                                                                                                   \
              auto & x(y.second[idx]);                                                                                                                                                                                          \
              (*data)[count].data[idx].ts = x.get_ts();                                                                                                                                                                         \
              ASSIGN_CODE                                                                                                                                                                                                       \
            }                                                                                                                                                                                                                   \
          count++;                                                                                                                                                                                                              \
        }                                                                                                                                                                                                                       \
    }                                                                                                                                                                                                                           \
  catch(daq::pbeast::NoAttributeDirectory& ex)                                                                                                                                                                                  \
    {                                                                                                                                                                                                                           \
      throw pbeast::NotFound(ex.what());                                                                                                                                                                                        \
    }                                                                                                                                                                                                                           \
  catch(daq::pbeast::RegularExpressionError& ex)                                                                                                                                                                                \
    {                                                                                                                                                                                                                           \
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());                                                                                                                                                             \
    }                                                                                                                                                                                                                           \
  catch(daq::pbeast::BadParameter& ex)                                                                                                                                                                                          \
    {                                                                                                                                                                                                                           \
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());                                                                                                                                                             \
    }                                                                                                                                                                                                                           \
  catch(daq::pbeast::FileTypeError& ex)                                                                                                                                                                                         \
    {                                                                                                                                                                                                                           \
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());                                                                                                                                                             \
    }                                                                                                                                                                                                                           \
  catch(daq::pbeast::MaxMessageSizeError& ex)                                                                                                                                                                                   \
    {                                                                                                                                                                                                                           \
      throw pbeast::BadRequest(pbeast::utils::err2str(ex).c_str());                                                                                                                                                             \
    }                                                                                                                                                                                                                           \
  catch(std::exception& ex)                                                                                                                                                                                                     \
    {                                                                                                                                                                                                                           \
      throw pbeast::Failure(make_read_error_text("downsampled attribute", ex).c_str());                                                                                                                                         \
    }                                                                                                                                                                                                                           \
}


#define GET_DS_SIMPLE_MANY(LIST_TYPE, PBEAST_TYPE, CPP_TYPE, ASSIGN_CODE)  \
  GET_DS_ALL( LIST_TYPE, PBEAST_TYPE, false, CPP_TYPE, DownsampledSeriesData, ASSIGN_CODE )

#define GET_DS_VECTOR_MANY(LIST_TYPE, PBEAST_TYPE, CPP_TYPE, ASSIGN_CODE)  \
  GET_DS_ALL( LIST_TYPE, PBEAST_TYPE, true, CPP_TYPE, DownsampledSeriesVectorData, ASSIGN_CODE )


#define ASSIGN_DS_PRIMITIVE_OBJ_TYPE                                                             \
    (*data)[count].data[idx].data.min = x.data().get_min_value();                                \
    (*data)[count].data[idx].data.val = x.data().get_value();                                    \
    (*data)[count].data[idx].data.max = x.data().get_max_value();

#define ASSIGN_DS_STRING_OBJ_TYPE                                                                \
    (*data)[count].data[idx].data.min = CORBA::string_dup(x.data().get_min_value().c_str());     \
    (*data)[count].data[idx].data.val = CORBA::string_dup(x.data().get_value().c_str());         \
    (*data)[count].data[idx].data.max = CORBA::string_dup(x.data().get_max_value().c_str());


void
pbeast::RepositoryServer::get_downsample_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(BooleanDownSampleObjectList, BOOL, bool, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S8DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S8DownSampleObjectList, S8, int8_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U8DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U8DownSampleObjectList, U8, uint8_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S16DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S16DownSampleObjectList, S16, int16_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U16DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U16DownSampleObjectList, U16, uint16_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S32DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S32DownSampleObjectList, S32, int32_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U32DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U32DownSampleObjectList, U32, uint32_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S64DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S64DownSampleObjectList, S64, int64_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U64DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U64DownSampleObjectList, U64, uint64_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(FloatDownSampleObjectList, FLOAT, float, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(DoubleDownSampleObjectList, DOUBLE, double, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::RepositoryServer::get_downsample_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::StringDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(StringDownSampleObjectList, STRING, std::string, ASSIGN_DS_STRING_OBJ_TYPE)
}


#define ASSIGN_DS_PRIMITIVE_OBJ_VECTOR                                                           \
    int idx2(0);                                                                                 \
    (*data)[count].data[idx].data.length(x.data().size());                                       \
    for(const auto & y : x.data()) {                                                             \
      ((*data)[count].data[idx].data)[idx2].min = y.get_min_value();                             \
      ((*data)[count].data[idx].data)[idx2].val = y.get_value();                                 \
      ((*data)[count].data[idx].data)[idx2].max = y.get_max_value();                             \
      idx2++;                                                                                    \
    }

#define ASSIGN_DS_STRING_OBJ_VECTOR                                                              \
    int idx2(0);                                                                                 \
    (*data)[count].data[idx].data.length(x.data().size());                                       \
    for(const auto & y : x.data()) {                                                             \
      ((*data)[count].data[idx].data)[idx2].min = CORBA::string_dup(y.get_min_value().c_str());  \
      ((*data)[count].data[idx].data)[idx2].val = CORBA::string_dup(y.get_value().c_str());      \
      ((*data)[count].data[idx].data)[idx2].max = CORBA::string_dup(y.get_max_value().c_str());  \
      idx2++;                                                                                    \
    }


void
pbeast::RepositoryServer::get_downsample_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(BooleanDownSampleArrayObjectList, BOOL, bool, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S8DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S8DownSampleArrayObjectList, S8, int8_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U8DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U8DownSampleArrayObjectList, U8, uint8_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S16DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S16DownSampleArrayObjectList, S16, int16_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U16DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U16DownSampleArrayObjectList, U16, uint16_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S32DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S32DownSampleArrayObjectList, S32, int32_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U32DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U32DownSampleArrayObjectList, U32, uint32_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S64DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S64DownSampleArrayObjectList, S64, int64_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U64DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U64DownSampleArrayObjectList, U64, uint64_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(FloatDownSampleArrayObjectList, FLOAT, float, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(DoubleDownSampleArrayObjectList, DOUBLE, double, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::RepositoryServer::get_downsample_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::StringDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(StringDownSampleArrayObjectList, STRING, std::string, ASSIGN_DS_STRING_OBJ_VECTOR)
}
