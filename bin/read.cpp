#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include "boost/date_time/local_time/local_time.hpp"

#include <ers/ers.h>

#include "pbeast/functions.h"
#include "pbeast/data-type.h"
#include "pbeast/def-intervals.h"
#include "pbeast/query.h"
#include "pbeast/series-data.h"
#include "pbeast/series-print.h"
#include "application_exceptions.h"

namespace po = boost::program_options;


// string to timestamp

static uint64_t
str2ts(std::string& in, boost::local_time::time_zone_ptr tz_ptr, const char * name)
{
  static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));

  std::string s(in);
  std::replace(s.begin(), s.end(), 'T', ' ');
  boost::posix_time::ptime t;

  try
    {
      t = boost::posix_time::time_from_string(s);
    }
  catch (std::exception& e)
    {
      std::ostringstream text;
      text << "cannot parse " << name << " = \'" << in << "\': \"" << e.what() << '\"';
      throw std::runtime_error(text.str().c_str());
    }

    // convert local time to UTC, if the time zone was provided

  if (tz_ptr)
    {
      try
        {
          boost::local_time::local_date_time lt(t.date(), t.time_of_day(),
              tz_ptr, boost::local_time::local_date_time::EXCEPTION_ON_ERROR);
          ERS_DEBUG(1,
              "Build zone\'s time \'" << in << "\' => \'" << lt.to_string() << "\' using \'" << tz_ptr->to_posix_string() << '\'');
          t = lt.utc_time();
        }
      catch (std::exception& e)
        {
          std::ostringstream text;
          text << "cannot parse " << name << " = \'" << in
              << "\' as zone\'s time: \"" << e.what() << '\"';
          throw std::runtime_error(text.str().c_str());
        }
    }
  return (t - epoch).total_microseconds();
}

static boost::local_time::time_zone_ptr
get_time_zone_ptr(const std::string& time_zone)
{
  const char * tz_spec_file = ::getenv("BOOST_DATE_TIME_TZ_SPEC");

  if (!tz_spec_file || !*tz_spec_file)
    {
      throw std::runtime_error("cannot read value of BOOST_DATE_TIME_TZ_SPEC environment variable to parse timezone parameter");
    }
  else
    {
      ERS_DEBUG(1, "Boost time-zone specification file is \'" << tz_spec_file << '\'');
    }

  boost::local_time::tz_database tz_db;

  try
    {
      tz_db.load_from_file(tz_spec_file);
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "cannot read Boost time-zone specification file " << tz_spec_file << ": " << ex.what();
      throw std::runtime_error(text.str().c_str());
    }

  if (time_zone == "list-regions")
    {
      for (auto & r : tz_db.region_list())
        {
          std::cout << r << std::endl;
        }

      exit(0);
    }

  if(boost::local_time::time_zone_ptr tz_ptr = tz_db.time_zone_from_region(time_zone))
    {
      return tz_ptr;
    }
  else
    {
      std::ostringstream text;
      text << "cannot find time-zone \'" << time_zone << '\'';
      throw std::runtime_error(text.str().c_str());
    }
}


template<class T>
  void
  print(pbeast::QueryDataBase * db, bool downsampled, bool print_every_update, const pbeast::FunctionConfig * fc_ptr, boost::local_time::time_zone_ptr tz_ptr, bool print_raw_ts)
  {
    if (downsampled == false)
      {
        if (db->is_array() == false)
          {
            pbeast::print(std::cout, static_cast<pbeast::QueryData<pbeast::DataPoint<T>>*>(db)->get_data(), tz_ptr, !print_every_update, !print_raw_ts, "    ");
          }
        else
          {
            pbeast::print(std::cout, static_cast<pbeast::QueryData<pbeast::DataPoint<std::vector<T>>>*>(db)->get_data(), tz_ptr, !print_every_update, !print_raw_ts, "    ");
          }
      }
    else
      {
        if (db->is_array() == false)
          {
            pbeast::print(std::cout, static_cast<pbeast::QueryData<pbeast::DownsampledDataPoint<T>>*>(db)->get_data(), tz_ptr, false, !print_raw_ts, "    ", fc_ptr);
          }
        else
          {
            pbeast::print(std::cout, static_cast<pbeast::QueryData<pbeast::DownsampledDataPoint<std::vector<T>>>*>(db)->get_data(), tz_ptr, false, !print_raw_ts, "    ", fc_ptr);
          }
      }
  }


int
main(int argc, char ** argv)
{
  po::options_description desc(
      "Data reader from pbeast server using c++ api.\n\n"
      "There are two ways to access P1 data on GPN using Kerberos authentication (see https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltPBeast#8_Access_P_BEAST_Data for more details):\n"
      "1) Set process environment variable PBEAST_SERVER_HTTPS_PROXY pointing to atlasgw and connect with Point-1 www server (recommended for CERN GPN).\n"
      "2) Set process environment variable PBEAST_SERVER_SSO_SETUP_TYPE=autoupdatekerberos and connect with Point-1 atlasop server (do not use for accounts with 2FA enabled).\n\n"
      "For timestamps use ISO 8601 date-time format, i.e. YYYY-MM-DD HH:MM:SS[,...]\n"
      "For available timezones run with -z list-regions\n\n"
      "Available options are"
  );

  std::string base_url;
  std::string partition_name;
  std::string class_name;
  std::string attribute_name;
  std::string object_name;
  bool object_name_is_regexp = false;
  boost::local_time::time_zone_ptr tz_ptr;
  uint64_t since(pbeast::get_def_since());
  uint64_t until(pbeast::get_def_until());
  uint64_t prev_interval(pbeast::def_lookup_prev_const);
  uint64_t next_interval(pbeast::def_lookup_next_const);
  uint32_t interval(0);
  pbeast::FillGaps::Type fill_gaps;
  bool print_every_update = false;
  bool print_raw_ts = false;
  bool print_raw_date_time = false;
  bool list_updated_objects = false;
  bool list_partitions = false;
  bool list_classes = false;
  bool list_attributes = false;
  pbeast::FunctionConfig function_config;
  pbeast::FunctionConfig * fc_ptr = nullptr;
  bool use_fqn = false;

  // parse command line
  try
    {
      std::string _data_type;
      std::string since_tmp;
      std::string until_tmp;
      std::string time_zone;
      std::string object_names_regexp;
      std::vector<std::string> functions;
      std::string fill_gaps_tmp("near");
      std::streamsize precision(std::cout.precision());

      std::string functions_desc = std::string("Space-separated ") + pbeast::FunctionConfig::make_description_of_functions();

      desc.add_options()
      (
          "base-url,n",
          po::value<std::string>(&base_url)->required(),
          "URL of pbeast server"
      )
      (
          "partition-name,p",
          po::value<std::string>(&partition_name),
          "Name of partition"
      )
      (
          "class-name,c",
          po::value<std::string>(&class_name),
          "Name of class"
      )
      (
          "attribute-name,a",
          po::value<std::string>(&attribute_name),
          "Name of attribute or path in case of nested type"
      )
      (
          "object-name,o",
          po::value<std::string>(&object_name),
          "Name of object"
      )
      (
          "object-name-regexp,O",
          po::value<std::string>(&object_names_regexp),
          "Regular expression for names of objects"
      )
      (
          "use-fully-qualified-object-names,F",
          "Use fully qualified names (put dot-separated partition, class and attribute names prefix for object names)"
      )
      (
          "list-updated-objects,l",
          "List names of updated objects"
      )
      (
          "list-partitions,L",
          "List partitions"
      )
      (
          "list-classes,C",
          "List classes"
      )
      (
          "list-attributes,A",
          "List attributes"
      )
      (
          "since,s",
          po::value<std::string>(&since_tmp)->default_value(since_tmp),
          "Show data since given timestamp"
      )
      (
          "until,t",
          po::value<std::string>(&until_tmp)->default_value(until_tmp),
          "Show data until given timestamp"
      )
      (
          "lookup-previous-value-time-lapse,P",
          po::value<uint64_t>(&prev_interval)->default_value(prev_interval),
          "Time lapse to lookup closest previous data point outside query time interval (in microseconds)"
      )
      (
          "lookup-next-value-time-lapse,N",
          po::value<uint64_t>(&next_interval)->default_value(next_interval),
          "Time lapse to lookup closest next data point outside query time interval (in microseconds)"
      )
      (
          "since-raw-timestamp,S",
          po::value<uint64_t>(&since)->default_value(since),
          "Show data since given raw timestamp (number of micro-seconds since Epoch)"
      )
      (
          "until-raw-timestamp,T",
          po::value<uint64_t>(&until)->default_value(until),
          "Show data until given raw timestamp (number of micro-seconds since Epoch)"
      )
      (
          "downsample-interval,i",
          po::value<uint32_t>(&interval),
          "Downsampling interval in seconds (if not set or zero, print raw data)"
      )
      (
          "fill-gaps,g",
          po::value<std::string>(&fill_gaps_tmp)->default_value(fill_gaps_tmp),
          "When downsample, approximate missing values: \"none\", \"near\" or \"all\""
      )
      (
          "functions,f",
          po::value<std::vector<std::string> >(&functions)->multitoken(),
          functions_desc.c_str()
      )
      (
          "keep-aggregated-data,K",
          "Keep original data after aggregation"
      )
      (
          "time-zone,z",
          po::value<std::string>(&time_zone)->default_value(time_zone),
          "Name of time zone (use \"-z list-regions\" to see known time zone names)"
      )
      (
          "print-raw-timestamp,w",
          "Print raw timestamp (number of micro-seconds since Epoch) instead of formatted one"
      )
      (
          "print-raw-date-time,W",
          "Print date and time attributes in raw format (number of micro-seconds since Epoch) instead of formatted one"
      )
      (
          "precision,V",
          po::value<std::streamsize>(&precision)->default_value(precision),
          "Set floating point data output precision"
      )
      (
          "print-value-per-update,u",
          "Print value per IS object update even if the value remained unchanged"
      )
      (
          "help,h",
          "Print help message"
      );

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);

      if (vm.count("object-name-regexp"))
        object_name_is_regexp = true;

      if (vm.count("use-fully-qualified-object-names"))
        use_fqn = true;

      if (vm.count("list-updated-objects"))
        list_updated_objects = true;

      if (vm.count("list-partitions"))
        list_partitions = true;
      else if (partition_name.empty())
        throw std::runtime_error("partition name is required parameter");

      if (vm.count("list-classes"))
        list_classes = true;
      else if (!list_partitions && class_name.empty())
        throw std::runtime_error("class name is required parameter");

      if (vm.count("list-attributes"))
        list_attributes = true;
      else if (!list_partitions && !list_classes && attribute_name.empty())
        throw std::runtime_error("attribute name is required parameter");

      if (vm.count("print-raw-timestamp"))
        print_raw_ts = true;

      if (vm.count("print-raw-date-time"))
        print_raw_date_time = true;

      if (vm.count("print-value-per-update"))
        print_every_update = true;

      if (vm["until-raw-timestamp"].defaulted() == false && vm["until"].defaulted() == false)
        throw std::runtime_error("both \"until\" and \"until-raw-timestamp\" parameters are defined");

      if (vm["since-raw-timestamp"].defaulted() == false && vm["since"].defaulted() == false)
        throw std::runtime_error("both \"since\" and \"since-raw-timestamp\" parameters are defined");

      if (!time_zone.empty())
        tz_ptr = get_time_zone_ptr(time_zone);

      if (!since_tmp.empty())
        since = str2ts(since_tmp, tz_ptr, "since");

      if (!until_tmp.empty())
        until = str2ts(until_tmp, tz_ptr, "until");

      if (precision != std::cout.precision())
        std::cout.precision(precision);

     if (!object_names_regexp.empty())
        if (object_names_regexp != ".*")
          object_name = object_names_regexp;

      if (functions.empty() == false)
        {
          function_config.construct(functions, interval);
          fc_ptr = &function_config;
        }

      if (vm.count("keep-aggregated-data"))
        {
          if(function_config.no_aggregations())
            throw std::runtime_error("cannot use \"keep-aggregated-data\" parameter without \"aggregate\" functions");

          function_config.m_keep_data = true;
        }

      fill_gaps = pbeast::FillGaps::mk(fill_gaps_tmp);
    }
  catch (std::exception& ex)
    {
      ers::fatal( daq::pbeast::CommandLineError( ERS_HERE, ex.what() ) );
      return 1;
    }

  try
    {
      pbeast::ServerProxy proxy(base_url);

      if (list_partitions)
        {
          for (const auto & x : proxy.get_partitions())
            std::cout << x << std::endl;

          return 0;
        }

      if (list_classes)
        {
          for (const auto & x : proxy.get_classes(partition_name))
            std::cout << x << std::endl;

          return 0;
        }

      if (list_attributes)
        {
          for (const auto & x : proxy.get_attributes(partition_name, class_name))
            std::cout << x << std::endl;

          return 0;
        }

      if (list_updated_objects)
        {
          for (const auto & x : proxy.get_objects(partition_name, class_name, attribute_name, object_name, since, until))
            std::cout << x << std::endl;

          return 0;
        }

      std::vector<std::shared_ptr<pbeast::QueryDataBase>> result = proxy.get_data(partition_name, class_name, attribute_name, object_name, object_name_is_regexp, since, until, prev_interval, next_interval, print_every_update, interval, fill_gaps, function_config, pbeast::ServerProxy::ZipCatalog | pbeast::ServerProxy::ZipData | pbeast::ServerProxy::CatalogizeStrings, use_fqn);

      if (result.empty())
        {
          std::cout << "no data found" << std::endl;
          return 0;
        }

          for (const auto& x : result)
            {
              pbeast::print_tss(std::cout, x->since(), x->until(), tz_ptr, !print_raw_ts, "(INFO) - ");
              std::cout << " attribute type: " << pbeast::as_string(x->type()) << ", is array: " << std::boolalpha << x->is_array() << std::endl;

              switch (x->type())
                {
                  case pbeast::BOOL:
                    print<bool>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::S8:
                    print<int8_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::U8:
                    print<uint8_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::S16:
                    print<int16_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::U16:
                    print<uint16_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::S32:
                    print<int32_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::U32:
                    print<uint32_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::S64:
                    print<int64_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::U64:
                    print<uint64_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::FLOAT:
                    print<float>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::DOUBLE:
                    print<double>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::ENUM:
                    print<int32_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::DATE:
                    if(print_raw_date_time) print<uint32_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    else print<pbeast::Date>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::TIME:
                    if(print_raw_date_time) print<uint64_t>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    else print<pbeast::Time>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts);
                    break;
                  case pbeast::STRING: print<std::string>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts); break;
                  case pbeast::VOID:   print<pbeast::Void>(x.get(), interval, print_every_update, fc_ptr, tz_ptr, print_raw_ts); break;
                  case pbeast::OBJECT:
                    {
                      throw std::runtime_error("cannot print object of nested type");
                    }
                  default:
                    {
                      std::ostringstream text;
                      text << "data type " << x->type() << " is not supported";
                      throw std::runtime_error(text.str().c_str());
                    }
                }
        }
    }
  catch (std::exception & ex)
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE, ex));
      return 1;
    }

  return 0;
}
