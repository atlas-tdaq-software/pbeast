#ifndef PBEAST_SERVER_MONITORING_H
#define PBEAST_SERVER_MONITORING_H

#include <stdint.h>
#include <sys/time.h>
#include <time.h>

#include <atomic>
#include <cmath>
#include <map>
#include <memory>
#include <shared_mutex>
#include <string>

#include <system/Host.h>

#include "pbeast/PBeastServerUserMon.h"

namespace pbeast
{
  namespace srv
  {
    struct UserStat
    {
      UserStat(pbeast::PBeastServerUserMon * obj = nullptr);

      std::atomic<uint_least64_t> m_num_of_requests;
      std::atomic<uint_least64_t> m_size_of_data;
      std::atomic<uint_least64_t> m_max_size_of_data;
      std::atomic<uint_least64_t> m_processing_time;
      std::atomic<uint_least64_t> m_max_processing_time;

      pbeast::PBeastServerUserMon * m_obj;

      time_t m_last_updated;
      time_t m_last_non_null;

      std::string
      to_string(const char * prefix = "  ") const;

      template<typename T>
        void
        update_maximum(std::atomic<T>& maximum_value, T const& value) noexcept
        {
          T prev_value = maximum_value;
          while (prev_value < value && !maximum_value.compare_exchange_weak(prev_value, value))
            ;
        }

      void
      add(uint64_t processing_time, uint64_t size_of_data)
      {
        m_num_of_requests++;
        m_size_of_data += size_of_data;
        m_processing_time += processing_time;
        update_maximum(m_max_size_of_data, size_of_data);
        update_maximum(m_max_processing_time, processing_time);
      }
    };


    class CorbaCall
    {
    public:
      timespec m_start;
      uint64_t m_size;

      CorbaCall() : m_size(0)
      {
        clock_gettime(CLOCK_MONOTONIC, &m_start);
      }

      ~CorbaCall();

      uint64_t
      get_interval()
      {
        timespec end;
        clock_gettime(CLOCK_MONOTONIC, &end);
        return ((end.tv_sec - m_start.tv_sec) * 1000 + (end.tv_nsec - m_start.tv_nsec) / 1000000);
      }
    };


    class Monitoring
    {

    public:

      Monitoring();
      ~Monitoring();

      // constants for monitoring thread
      enum { mon_sleep_interval = 10, user_sleep_inteval = 60, put_user_info_into_oblivion = 3600 * 24 * 7 };

      void
      publish_calls_stats(bool obvilion_flag = false);

      std::shared_ptr<UserStat>
      get_user_stat(const std::string& name);

      bool
      init_monitoring();


    public:

      UserStat m_api;
      UserStat m_rest;
      UserStat m_corba;


    private:

      std::map<std::string, std::shared_ptr<UserStat>> m_users;
      std::shared_mutex m_mutex;

      Configuration m_mon_db;

      inline static const std::string s_is_name_prefix = std::string("pbeast-server-") + System::LocalHost::instance()->local_name() + '.';
      inline static const std::string s_is_corba_name = "SUM_CORBA";
      inline static const std::string s_is_rest_name = "SUM_REST";
      inline static const std::string s_is_api_name = "SUM_API";


    private:

      bool
      publish_calls_stats(UserStat& stat, const std::string& name, uint64_t publication_interval, bool obvilion_flag);

      template<typename T>
        void
        update_maximum(std::atomic<T>& maximum_value, T const& value) noexcept
        {
          T prev_value = maximum_value;
          while (prev_value < value && !maximum_value.compare_exchange_weak(prev_value, value))
            ;
        }

      inline uint64_t
      average_size(uint64_t sum, uint64_t num)
      {
        return static_cast<uint64_t>(lrint(static_cast<float>(sum)/static_cast<float>(num)));
      }

      inline float
      average_rate(uint64_t sum, uint64_t interval)
      {
        return (static_cast<float>(sum) / static_cast<float>(interval));
      }
    };
  }
}


#endif
