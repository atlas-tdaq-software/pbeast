#ifndef PBEAST_RECEIVER_IS_INFO_READER_H
#define PBEAST_RECEIVER_IS_INFO_READER_H

#include <time.h>
#include <string>

#include <is/criteria.h>

namespace pbeast
{
  namespace receiver
  {

    class IS_DataProvider;

    class IS_InfoReader
    {

    public:

      IS_InfoReader(IS_DataProvider& dp, const std::string& server, bool read_at_subscribe, ISCriteria::Logic logic, const std::string& info_name, const std::string& type, bool subtypes, bool exclude);
      ~IS_InfoReader();

      void
      subscribe(bool read_all_when_subscribe);

      static void
      report_bad_class(const std::string& class_name, const std::string& object_id, const std::string& provider, const char * action, const char * op);

      bool
      is_subscribed() const
      {
        return (m_start_time != 0);
      }

    private:

      IS_DataProvider& m_data_provider;
      ISCriteria * m_criteria;
      std::string m_server_name;
      time_t m_start_time;bool m_read_at_subscribe;

    };

  }
}

#endif
