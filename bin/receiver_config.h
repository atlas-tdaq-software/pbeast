#ifndef PBEAST_RECEIVER_CONFIG_H
#define PBEAST_RECEIVER_CONFIG_H

#include <stdint.h>

#include <string>

namespace pbeast
{
  class PBeastReceiverApplication;

  namespace receiver
  {

    class Config
    {

      friend class DataProvider;

    public:

      void
      init(const pbeast::PBeastReceiverApplication& obj);

      virtual
      ~Config()
      {
        ;
      }

      uint32_t
      get_max_save_delay() const
      {
        return m_max_save_delay;
      }

      const std::string&
      get_repository_dir_name() const
      {
        return m_repository_dir_name;
      }

      uint32_t
      get_max_data_size() const
      {
        return m_max_data_size;
      }

      uint32_t
      get_max_file_size() const
      {
        return m_max_file_size;
      }

      uint32_t
      get_max_data_delay() const
      {
        return m_max_data_delay;
      }

      uint8_t
      get_flush_data_threshold() const
      {
        return m_flush_data_threshold;
      }

      uint32_t
      get_save_check_interval() const
      {
        return m_save_check_interval;
      }

      const std::string&
      get_name() const
      {
        return m_name;
      }

      uint32_t
      get_small_data_delay() const
      {
        return m_small_data_delay;
      }

      uint32_t
      get_big_data_delay() const
      {
        return m_big_data_delay;
      }


    private:

      uint32_t m_max_save_delay;
      std::string m_repository_dir_name;
      uint32_t m_max_data_size;
      uint32_t m_max_file_size;
      uint32_t m_max_data_delay;
      uint8_t m_flush_data_threshold;
      uint32_t m_save_check_interval;
      std::string m_name;
      uint32_t m_small_data_delay;
      uint32_t m_big_data_delay;

    };

  }
}

#endif
