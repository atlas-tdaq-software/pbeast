#ifndef PBEAST_RECEIVER_ATTRIBUTE_H
#define PBEAST_RECEIVER_ATTRIBUTE_H

#include <cstdint>
#include <filesystem>
#include <map>
#include <mutex>
#include <set>
#include <string>
#include <vector>

#include <pbeast/data-type.h>
#include <pbeast/data-file.h>

#include "receiver_data.h"

namespace pbeast {

  class WriterLock;

  namespace receiver {

    class Class;

    class BaseDataHolder
    {
      friend class Attribute;
      friend class Class;

    public:

      BaseDataHolder() :
          m_data_cache_id(0)
      {
        ;
      }

      virtual
      ~BaseDataHolder()
      {
        ;
      }

      virtual std::pair<uint64_t,uint64_t>
      compact(uint64_t till, Attribute& a, const std::map<std::string, std::set<uint64_t>*> * eov) = 0;

      virtual uint32_t
      get_size() const = 0;

    protected:

      uint64_t m_data_cache_id;
      std::mutex m_mutex;

    };


    class Attribute {

      friend class Object;
      friend class Class;
      friend class DataProvider;

      public:

        Attribute(Class * parent, const std::string& name, pbeast::DataType type, const std::string& type_name, bool is_array, const std::string& description)
          : m_type(type), m_type_name(type_name), m_type_class(nullptr), m_parent(parent), m_is_array(is_array), m_name(name), m_dir_name(pbeast::DataFile::encode(name)),
            m_description(description), m_compact_data(nullptr), m_raw_data(nullptr) { init(); }

        Attribute(const Attribute& a, Class * parent = nullptr);

        Attribute() = delete ;
        Attribute(const Attribute&) = delete;
        Attribute& operator=(const Attribute&) = delete;

        ~Attribute();


        inline bool
        operator==(const Attribute& a) const
        {
          return ((m_name == a.m_name) && (m_description == a.m_description) && (m_type_name == a.m_type_name) && (m_is_array == a.m_is_array));
        }

        void init(bool extend = false);

        void link_type(const std::map<std::string, Class *>& classes);

        void create_nested();

        const std::string& get_name() const { return m_name; }

        pbeast::DataType get_type() const { return m_type; }

        const std::string& get_type_name() const { return m_type_name; }

        Class * get_parent_class() const { return m_parent; }

        Class * get_type_class() const { return m_type_class; }

        BaseDataHolder * get_raw_data() { return m_raw_data; }

        bool get_is_array() const { return m_is_array; }

        const std::string& get_description() const { return m_description; }

        const std::string& get_dir_name() const { return m_dir_name; }

        void write_ready(pbeast::WriterLock& writer_lock, const std::filesystem::path& dir, const std::string& partition, const std::string& partition_dir_name, Class& c, uint64_t save_expire_ts);
        void compact_data(uint64_t cb_expire_ts, const std::map<std::string, std::set<uint64_t>*> * eov);

        bool
        has_compacted_data()
        {
          std::lock_guard<std::mutex> data_lock(m_compact_data_mutex);
          return (m_compact_data != nullptr);
        }

        void commit(const std::filesystem::path& dir, const std::string& partition, const std::string& partition_dir_name, Class& c, Attribute& a, const std::map<std::string, std::set<uint64_t>> * eov);

        template<typename T>
        Data<T>&
        data()
        {
          if (m_compact_data == nullptr)
            m_compact_data = static_cast<BaseData *>(new Data<T>(m_type));

          return *(static_cast<Data<T> *>(m_compact_data));
        }

        template<typename T>
        VectorData<T>&
        vector_data()
        {
          if (m_compact_data == nullptr)
            m_compact_data = static_cast<BaseData *>(new VectorData<T>(m_type));

          return *(static_cast<VectorData<T> *>(m_compact_data));
        }

        std::ostream& print(std::ostream& s, const std::string& prefix) const;

        struct QueryResult
        {
          QueryResult(const std::string * s, unsigned int begin_idx, unsigned int end_idx) :
            m_str(s), m_begin_idx(begin_idx), m_end_idx(end_idx)
          {
            ;
          }

          const std::string * m_str;
          unsigned int m_begin_idx;
          unsigned int m_end_idx;
        };

      private:

        pbeast::DataType m_type;
        std::string m_type_name;
        Class * m_type_class;
        Class * m_parent;
        bool m_is_array;
        std::string m_name;
        std::string m_dir_name;
        std::string m_description;
        BaseData * m_compact_data;
        std::mutex m_compact_data_mutex;
        BaseDataHolder * m_raw_data;
        std::vector<QueryResult> tmp_result;

        // constructor for class IoV and UpdatedInfo
        Attribute(const std::string& name, Class * p, bool extend = false)
          : m_type(pbeast::VOID), m_type_class(nullptr), m_parent(p), m_is_array(false), m_name(name), m_dir_name(name), m_compact_data(nullptr), m_raw_data(nullptr)  { init(extend); }

        void commit(const std::filesystem::path& dir, const std::string& partition, const std::string& partition_dir_name, Class& c, BaseData * data);

    };


    template<class T>
      class BaseHolderSerie
      {

      public:

        BaseHolderSerie() :
          m_last_eov(0xffffffffffffffff), m_serie(nullptr)
        {
          ;
        }

        inline void
        insert(uint64_t ts, const T& v)
        {
          m_data.insert(std::pair<uint64_t, T>(ts, v));
        }

        std::multimap<uint64_t, T> m_data;
        uint64_t m_last_eov;

        pbeast::Serie<T> * m_serie;

      };


    template<class T, bool P = false>
      class HolderSerie : public BaseHolderSerie<T>
    {
    };

    template<>
      class HolderSerie<pbeast::Void, true>: public BaseHolderSerie<pbeast::Void>
    {

      public:

        inline void
        insert(uint64_t ts, const pbeast::Void& v)
        {
          BaseHolderSerie::insert(ts, v);
          m_void_data.insert(ts);
        }

        inline std::set<uint64_t> *
        get_void_data()
        {
          return &m_void_data;
        }

        std::set<uint64_t> m_void_data;

    };


    template<class T, bool P = false> class DataHolder : public BaseDataHolder {

      public:

        DataHolder() { ; }

        ~DataHolder()
        {
          for (auto& x : m_data)
            delete x.second;

          m_data.clear();
        }

        void
        add_data(const std::string& name, uint64_t ts, const T& v)
        {
          std::lock_guard<std::mutex> lock(m_mutex);

          HolderSerie<T,P>*& serie = m_data[name];

          if (serie == nullptr)
            serie = new HolderSerie<T,P>();

          serie->insert(ts, v);
        }

        uint32_t
        get_size() const
        {
          return m_data.size();
        }

        virtual std::pair<uint64_t,uint64_t>
        compact(uint64_t till, Attribute& a, const std::map<std::string, std::set<uint64_t>*> * eov)
        {
          uint64_t updated_data_num(0);
          uint64_t merged_data_num(0);

          Data<T>& to(a.data<T>());

          const bool use_cache(m_data_cache_id == to.get_id());

          for (auto i = m_data.begin(); i != m_data.end(); ++i)
            {
              if (use_cache == false)
                {
                  i->second->m_serie = nullptr;
                }

              if (!i->second->m_data.empty())
                {
                  pbeast::Serie<T> * serie = nullptr;
                  const std::set<uint64_t> * serie_eov = nullptr;

                  typename std::multimap<uint64_t, T>::iterator d = i->second->m_data.begin();

                  for (; d != i->second->m_data.end(); ++d)
                    {
                      if (d->first > till)
                        {
                          break;
                        }

                      // initialize serie and serie EoV at first iteration
                      if (serie == nullptr)
                        {
                          if (use_cache && i->second->m_serie != nullptr)
                            {
                              serie = i->second->m_serie;
                            }
                          else
                            {
                              i->second->m_serie = serie = to.get_serie(i->first);
                            }

                          if (eov)
                            {
                              std::map<std::string, std::set<uint64_t>*>::const_iterator serie_eov_it = pbeast::find(*eov, i->first);
                              if (serie_eov_it != eov->end())
                                {
                                  serie_eov = serie_eov_it->second;
                                }
                            }
                        }

                      // if last and current values are equal, a merge may be needed
                      if (serie->m_data.empty() == false && serie->m_data.back().get_value() == d->second)
                        {
                          // rare case, when EoV comes from previous compact()
                          if (i->second->m_last_eov < d->first)
                            {
                              to.add_data(serie, d->first, d->second);
                              updated_data_num++;
                            }
                          else if (serie_eov != nullptr)
                            {
                              std::set<uint64_t>::const_iterator x = serie_eov->upper_bound(serie->m_data.back().get_ts_last_updated());

                              // next EoV after timestamp of last-inserted-data-item > timestamp of new data
                              if (x == serie_eov->end() || *x > d->first)
                                {
                                  to.adjust_last_updated(serie, d->first);
                                  merged_data_num++;
                                }
                              // EoV is between last inserted and new data item
                              else
                                {
                                  to.add_data(serie, d->first, d->second);
                                  updated_data_num++;
                                }
                            }
                          // there is no EoV breaking this IoV, so extend last-updated
                          else
                            {
                              to.adjust_last_updated(serie, d->first);
                              merged_data_num++;
                            }
                        }

                      // there are no inserted data or values are different
                      else
                        {
                          to.add_data(serie, d->first, d->second);
                          updated_data_num++;
                        }

                      i->second->m_last_eov = 0xffffffffffffffff;
                    }

                  // process rare case, when EoV is after last inserted value
                  if (serie_eov != nullptr && serie->m_data.empty() == false)
                    {
                      std::set<uint64_t>::const_iterator x = serie_eov->upper_bound(serie->m_data.back().get_ts_last_updated());
                      if (x != serie_eov->end() && *x < till)
                        {
                          i->second->m_last_eov = *x;
                        }
                      else
                        {
                          i->second->m_last_eov = 0xffffffffffffffff;
                        }
                    }


                  if (serie != nullptr)
                    {
                      // remove all data
                      if (d == i->second->m_data.end())
                        {
                          i->second->m_data.clear();
                        }
                      // remove some data
                      else if (d != i->second->m_data.begin())
                        {
                          i->second->m_data.erase(i->second->m_data.begin(), d);
                        }
                    }
                }
            }

          m_data_cache_id = to.get_id();

          return std::make_pair(updated_data_num,merged_data_num);
        }


        const std::map<std::string, HolderSerie<T,P>*>&
        get_data() const
        {
          return m_data;
        }

      private:

        std::map<std::string,HolderSerie<T,P>*> m_data;

    };


    template<class T>
      class VectorHolderSerie
      {

      public:

        VectorHolderSerie() :
          m_last_eov(0xffffffffffffffff), m_serie(nullptr)
        {
          ;
        }

        std::multimap<uint64_t, std::vector<T>> m_data;
        uint64_t m_last_eov;

        pbeast::receiver::VectorSerie<T> * m_serie;

      };


    template<class T>
      class VectorDataHolder : public BaseDataHolder
      {

      public:

        VectorDataHolder()
        {
          ;
        }

        ~VectorDataHolder()
        {
          for (auto& x : m_data)
            {
              delete x.second;
            }
          m_data.clear();
        }

        void
        add_data(const std::string& name, uint64_t ts, const std::vector<T>& v)
        {
          std::lock_guard<std::mutex> lock(m_mutex);
          VectorHolderSerie<T>*& serie = m_data[name];
          if (serie == nullptr)
            {
              serie = new VectorHolderSerie<T>();
            }
          serie->m_data.insert(std::pair<uint64_t, std::vector<T>>(ts, v));
        }

        uint32_t
        get_size() const
        {
          return m_data.size();
        }

        virtual std::pair<uint64_t,uint64_t>
        compact(uint64_t till, Attribute& a, const std::map<std::string, std::set<uint64_t>*> * eov)
        {
          uint64_t updated_data_num(0);
          uint64_t merged_data_num(0);

          VectorData<T>& to(a.vector_data<T>());

          const bool use_cache(m_data_cache_id == to.get_id());

          for (auto i = m_data.begin(); i != m_data.end(); ++i)
            {
              if (use_cache == false)
                {
                  i->second->m_serie = nullptr;
                }

              if (!i->second->m_data.empty())
                {
                  pbeast::receiver::VectorSerie<T> * serie = nullptr;
                  const std::set<uint64_t> * serie_eov = nullptr;

                  typename std::multimap<uint64_t, std::vector<T>>::iterator d = i->second->m_data.begin();

                  for (; d != i->second->m_data.end(); ++d)
                    {
                      if (d->first > till)
                        {
                          break;
                        }

                      // initialize serie and serie EoV at first iteration
                      if (serie == nullptr)
                        {
                          if (use_cache && i->second->m_serie != nullptr)
                            {
                              serie = i->second->m_serie;
                            }
                          else
                            {
                              i->second->m_serie = serie = to.get_serie(i->first);
                            }

                          if (eov)
                            {
                              std::map<std::string, std::set<uint64_t>*>::const_iterator serie_eov_it = pbeast::find(*eov, i->first);
                              if (serie_eov_it != eov->end())
                                {
                                  serie_eov = serie_eov_it->second;
                                }
                            }
                        }

                      // if last and current values are equal, a merge may be needed
                      if (serie->m_data.empty() == false && serie->m_data.back().get_value() == d->second)
                        {
                          // rare case, when EoV comes from previous compact()
                          if (i->second->m_last_eov < d->first)
                            {
                              to.add_data(serie, d->first, d->second);
                              updated_data_num++;
                            }
                          else if (serie_eov != nullptr)
                            {
                              std::set<uint64_t>::const_iterator x = serie_eov->upper_bound(serie->m_data.back().get_ts_last_updated());

                              // next EoV after timestamp of last-inserted-data-item > timestamp of new data
                              if (x == serie_eov->end() || *x > d->first)
                                {
                                  to.adjust_last_updated(serie, d->first);
                                  merged_data_num++;
                                }
                              // EoV is between last inserted and new data item
                              else
                                {
                                  to.add_data(serie, d->first, d->second);
                                  updated_data_num++;
                                }
                            }
                          // there is no EoV breaking this IoV, so extend last-updated
                          else
                            {
                              to.adjust_last_updated(serie, d->first);
                              merged_data_num++;
                            }
                        }

                      // there are no inserted data or values are different
                      else
                        {
                          to.add_data(serie, d->first, d->second);
                          updated_data_num++;
                        }

                      i->second->m_last_eov = 0xffffffffffffffff;
                    }

                  // process rare case, when EoV is after last inserted value
                  if (serie_eov != nullptr && serie->m_data.empty() == false)
                    {
                      std::set<uint64_t>::const_iterator x = serie_eov->upper_bound(serie->m_data.back().get_ts_last_updated());
                      if (x != serie_eov->end() && *x < till)
                        {
                          i->second->m_last_eov = *x;
                        }
                      else
                        {
                          i->second->m_last_eov = 0xffffffffffffffff;
                        }
                    }


                  if (serie != nullptr)
                    {
                      // remove all data
                      if (d == i->second->m_data.end())
                        {
                          i->second->m_data.clear();
                        }
                      // remove some data
                      else if (d != i->second->m_data.begin())
                        {
                          i->second->m_data.erase(i->second->m_data.begin(), d);
                        }
                    }
                }
            }

          m_data_cache_id = to.get_id();

          return std::make_pair(updated_data_num,merged_data_num);
        }


      private:

        std::map<std::string,VectorHolderSerie<T>*> m_data;

    };

  }
}


#endif
