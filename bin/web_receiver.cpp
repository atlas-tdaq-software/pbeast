#include <errno.h>
#include <string.h>

#include <string>
#include <iostream>

#include <boost/program_options.hpp>

#include <ipc/core.h>
#include <ipc/signal.h>

#include <pmg/pmg_initSync.h>

#include <dal/Partition.h>
#include <dal/util.h>

#include <pbeast/service_lock.h>

#include "pbeast/PBeastWebReceiverApplication.h"

#include "application_exceptions.h"
#include "receiver_data.h"
#include "receiver_errors.h"
#include "web_receiver_config.h"
#include "web_receiver_data_provider.h"

namespace po = boost::program_options;


int main(int argc, char ** argv)
{
  // initialize IPC and set default CORBA parameters

  try
    {
      IPCCore::init(argc, argv);
    }
  catch (ers::Issue & ex)
    {
      ers::fatal(ex);
      return 1;
    }


  // command line parameters

  std::string partition_name("initial");
  std::string application_id;
  std::string conf_db_name;
  long thread_pool_size;
  bool create_sync_file(false);


  // read number of cores
  {
    errno = 0;
    thread_pool_size = sysconf(_SC_NPROCESSORS_ONLN);
    if (thread_pool_size == -1 && !errno)
      {
        const char * error_text = strerror(errno);
        ers::error(daq::pbeast::SystemCallFailed( ERS_HERE, "sysconf(_SC_NPROCESSORS_ONLN)", errno, error_text ) );
        errno = 0;
      }

    ERS_LOG("number of cores: " << thread_pool_size);

    // be default use 3/4 of cores for thread pool
    thread_pool_size *=3;
    thread_pool_size /=4;

    if(thread_pool_size < 4)
      {
        thread_pool_size = 4;
      }
  }

  if (const char * env = getenv("TDAQ_PARTITION"))
    {
      partition_name = env;
    }

  if (const char * env = getenv("TDAQ_DB"))
    {
      conf_db_name = env;
    }

  if (const char * env = getenv("TDAQ_APPLICATION_OBJECT_ID"))
    {
      application_id = env;
    }

  po::options_description desc("P-BEAST Web data receiver");

  try
    {
      desc.add_options()
        (
          "partition,p",
          po::value<std::string>(&partition_name)->default_value(partition_name),
          "Name (id) of the partition object containing web receiver application object (also TDAQ_PARTITION process environment variable)"
        )
        (
          "data,d",
          po::value<std::string>(&conf_db_name)->default_value(conf_db_name),
          "Name of the configuration database (also TDAQ_DB process environment variable)"
        )
        (
          "application-id,n",
          po::value<std::string>(&application_id)->default_value(application_id),
          "Name (id) of application object (also TDAQ_APPLICATION_OBJECT_ID process environment variable)"
        )
        (
          "thread-pool-size,t",
          po::value<long>(&thread_pool_size)->default_value(thread_pool_size),
          "Size of threads pool used to compact attributes (set between half and total number of cores)"
        )
        (
          "create-pmg-sync-file,s","create pmg synchronization file at startup"
        )
        (
          "help,h","Print help message"
        );

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      if (vm.count("create-pmg-sync-file"))
        {
          create_sync_file = true;
        }

      po::notify(vm);

      if (partition_name.empty())
        {
          throw std::runtime_error("the option \'--partition\' is required but missing");
        }

      if (application_id.empty())
        {
          throw std::runtime_error("the option \'--application-id\' is required but missing");
        }

      if (conf_db_name.empty())
        {
          throw std::runtime_error("the option \'--data\' is required but missing");
        }
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << ex.what() << "\nUsage: " << desc;
      ers::fatal(daq::pbeast::CommandLineError( ERS_HERE, text.str() ) );
      return 1;
    }


  try
    {

      pbeast::receiver::Web_Config config;

        {
          // read configuration DB and search for application description

          Configuration db(conf_db_name);

          if (const daq::core::Partition * partition = daq::core::get_partition(db, partition_name))
            {
              db.register_converter(new daq::core::SubstituteVariables(*partition));
            }
          else
            {
              ers::fatal(daq::pbeast::FailedReadConfiguration(ERS_HERE, partition_name, daq::core::Partition::s_class_name));
              return 1;
            }

          const pbeast::PBeastWebReceiverApplication * app = db.get<pbeast::PBeastWebReceiverApplication>(application_id);

          if (app == 0)
            {
              ers::fatal(daq::pbeast::FailedReadConfiguration(ERS_HERE, application_id, pbeast::PBeastWebReceiverApplication::s_class_name));
              return 1;
            }

          config.init(*app);
        }

      pbeast::receiver::BaseData::set_max_data_bucket_size(config.get_max_data_size());

      // check and lock repository (single web receiver for all partitions)

      pbeast::ServiceLock partition_receiver_lock(config.get_repository_dir_name(), "web-receiver.lock");

      IPCPartition partition;

        {
          pbeast::receiver::Web_DataProvider * data_provider = new pbeast::receiver::Web_DataProvider(config, config.get_repository_dir_name(), partition, thread_pool_size);
          data_provider->set_global_object();

          data_provider->start_monitoring_thread(1);

          data_provider->start_write_thread(config.get_save_check_interval());

          if (create_sync_file)
            pmg_initSync();

          data_provider->publish();

          auto sig_received = daq::ipc::signal::wait_for();

          ERS_LOG("Caught signal " << sig_received << ", exiting...");

          data_provider->unset_global_object();

          data_provider->_destroy(true);
        }
    }
  catch (const ers::Issue& ex)
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE, ex));
      return 1;
    }

  return 0;
}
