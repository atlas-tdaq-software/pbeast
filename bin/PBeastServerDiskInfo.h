#ifndef PBEASTSERVERDISKINFO_H
#define PBEASTSERVERDISKINFO_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * P-BEAST node disk monitoring information; it is published by P-BEAST merged repository server applications
 * 
 * @author  produced by the IS generator
 */

class PBeastServerDiskInfo : public ISInfo {
public:

    /**
     * Name of the host, where server is running
     */
    std::string                   host;

    /**
     * Capacity of the disk (in bytes)
     */
    uint64_t                      capacity;

    /**
     * Space available on the disk (in bytes)
     */
    uint64_t                      available;

    /**
     * Used space on the disk (in bytes)
     */
    uint64_t                      used;

    /**
     * Shows percentage of disk usage
     */
    double                        usage;


    static const ISType & type() {
	static const ISType type_ = PBeastServerDiskInfo( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "host: " << host << "\t// Name of the host, where server is running" << std::endl;
	out << "capacity: " << capacity << "\t// Capacity of the disk (in bytes)" << std::endl;
	out << "available: " << available << "\t// Space available on the disk (in bytes)" << std::endl;
	out << "used: " << used << "\t// Used space on the disk (in bytes)" << std::endl;
	out << "usage: " << usage << "\t// Shows percentage of disk usage";
	return out;
    }

    PBeastServerDiskInfo( )
      : ISInfo( "PBeastServerDiskInfo" )
    {
	initialize();
    }

    ~PBeastServerDiskInfo(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    PBeastServerDiskInfo( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << host << capacity << available << used << usage;
    }

    void refreshGuts( ISistream & in ){
	in >> host >> capacity >> available >> used >> usage;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const PBeastServerDiskInfo & info ) {
    info.print( out );
    return out;
}

#endif // PBEASTSERVERDISKINFO_H
