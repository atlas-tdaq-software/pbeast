#include <map>
#include <mutex>
#include <set>
#include <string>
#include <vector>

#include <pbeast/repository.h>

#include "receiver_attribute.h"
#include "receiver_class.h"


pbeast::receiver::Class::Class(const std::string& name) :
    m_name(name),
    m_dir_path(pbeast::DataFile::encode(name)),
    m_end_of_validity_attribute(new Attribute(pbeast::Repository::get_eov_attribute_name(), this, true)),
    m_updated_info_attribute(new Attribute(pbeast::Repository::get_upd_attribute_name(), this)),
    m_directory_created(false),
    m_incomplete_schema(false),
    m_parent(nullptr)
{
  ;
}

pbeast::receiver::Class::Class(const Class& c, Attribute * parent) :
    m_name(c.m_name),
    m_dir_path(parent->m_parent->m_dir_path / parent->m_dir_name / c.m_dir_path),
    m_end_of_validity_attribute(nullptr),
    m_updated_info_attribute(nullptr),
    m_directory_created(c.m_directory_created),
    m_incomplete_schema(c.m_incomplete_schema),
    m_parent(parent)
{
  m_attributes.reserve(c.m_attributes.size());

  for (const auto &a : c.m_attributes)
    m_attributes.push_back(new Attribute(*a, this));
}


pbeast::receiver::Class::~Class()
{
  for (auto &a : m_attributes)
    delete a;

  delete m_end_of_validity_attribute;
  delete m_updated_info_attribute;
}


uint64_t
pbeast::receiver::Class::rebuid_eov(uint64_t low_limit)
{
  std::lock_guard<std::mutex> attribute_lock(m_end_of_validity_attribute->m_raw_data->m_mutex);

  // add newly updated eovs
  for (auto & x : (static_cast<pbeast::receiver::DataHolder<pbeast::Void,true>*>(m_end_of_validity_attribute->m_raw_data))->get_data())
    if (!x.second->m_data.empty())
      m_eov.try_emplace(x.first, x.second->get_void_data());

  // remove old data
  for (auto& x : m_eov)
    {
      std::set<uint64_t>::const_iterator j = x.second->lower_bound(low_limit);
      if (j != x.second->begin())
        x.second->erase(x.second->begin(), j);
    }

  return 0;
}


std::ostream&
pbeast::receiver::Class::print(std::ostream &s, const std::string &prefix) const
{
  s << prefix << "class: " << get_name() << (is_nested() ? " [nested]" : "") << ", dir-path: " << get_dir_path() << std::endl;

  std::string prefix2(prefix);
  prefix2.append("  ");

  for (auto &a : m_attributes)
    a->print(s, prefix2);

  return s;
}

pbeast::receiver::Attribute*
pbeast::receiver::Class::get_attribute(const std::string &name)
{
  for (auto &a : m_attributes)
    if (a->get_name() == name)
      return a;

  return nullptr;
}


namespace pbeast
{
  namespace receiver
  {

    std::ostream&
    operator<<(std::ostream& s, const Class & c)
    {
      return c.print(s, "");
    }

  }
}
