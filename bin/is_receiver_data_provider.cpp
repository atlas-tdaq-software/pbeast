#include <is/infodocument.h>
#include <is/inforeceiver.h>
#include <is/serveriterator.h>

#include "application_exceptions.h"

#include "receiver_partition.h"

#include "is_receiver_class.h"
#include "is_receiver_data_provider.h"

#include "is_receiver_is_info_reader.h"
#include "receiver_errors.h"

pbeast::receiver::IS_DataProvider::IS_DataProvider(const IS_Config& config, const std::filesystem::path& path, ISInfoReceiver& rec, int thread_pool_size) :
    pbeast::receiver::DataProvider(config.get_name(), path, rec.partition(), thread_pool_size),
    m_config(config),
    m_receiver(rec),
    m_callback(*this)
{
  // read info docs

  try
    {
      for (ISInfoDocument::Iterator i(m_receiver.partition()); i; i++)
        {
          const ISInfoDocument& isd(*i);
          const std::string& name = isd.name();
          m_default_partition->define_class(name, new IS_Class(name, isd));
        }

      link_class_types(m_default_partition);
    }
  catch (ers::Issue& ex)
    {
      throw daq::pbeast::FailedReadInfoDocument(ERS_HERE, "", ex);
    }

  if (m_default_partition->get_classes().empty())
    {
      throw daq::pbeast::FailedReadInfoDocument(ERS_HERE, "ISInfoDocument is empty");
    }
}

pbeast::receiver::IS_DataProvider::~IS_DataProvider()
{
  ipc_withdraw();

    {
      std::vector<std::future<void>> results;

      for (auto &s : m_config.get_subscriptions())
        {
          ERS_LOG("unsubscribe " << s->m_info_readers.size() << " IS info readers");
          for (auto &i : s->m_info_readers)
            {
              results.emplace_back(m_thread_pool.enqueue([&i]
                { delete i.second;}));
            }
        }

      for (auto && result : results)
        {
          result.get();
        }

      ERS_LOG("stop info receivers");

      for (auto &s : m_config.get_subscriptions())
        {
          s->m_info_readers.clear();
        }
    }

  save_all_data();

}

void
pbeast::receiver::IS_DataProvider::check_readers()
{
  ERS_DEBUG(1, "check IS servers running in partition \'" << m_receiver.partition().name() << "\'...");

  try
    {
      ISServerIterator ss(m_receiver.partition());
      std::vector<std::future<void>> results;

      std::lock_guard<std::mutex> readers_lock(m_readers_mutex);

      // avoid possible interference with rebuild eov and data compaction methods
      std::lock_guard<std::mutex> thread_pool_lock(m_thread_pool_mutex);

      while (ss())
        {
          const std::string server(ss.name());

          for (auto &s : m_config.get_subscriptions())
            {
              if (s->match_server(server))
                {
                  IS_InfoReader*& reader = s->get_info_reader(server);

                  if (!reader)
                    {
                      reader = new IS_InfoReader(*this, server, s->get_read_when_subscribe(), s->get_logic(), s->get_is_info_names(), s->get_is_type(), s->get_include_sub_types(), s->get_exclude_this_type());
                    }

                  if (reader->is_subscribed() == false)
                    {
                      results.emplace_back(m_thread_pool.enqueue(std::bind(&pbeast::receiver::IS_InfoReader::subscribe, reader, s->get_read_when_subscribe())));
                    }
                }
            }
        }

      for (auto && result : results)
        {
          result.get();
        }
    }
  catch (daq::ipc::InvalidPartition& ex)
    {
      ers::error(daq::pbeast::ListPublishedObjectFailure(ERS_HERE, "is/repository", m_receiver.partition().name(), ex));
      return;
    }
}

struct CheckProvidersThread
{
  unsigned int m_sleep_interval;

  CheckProvidersThread(unsigned int n) :
      m_sleep_interval(n)
  {
    ;
  }

  void
  operator()()
  {
    while (true)
      {
        sleep(m_sleep_interval);

          {
            boost::shared_lock<boost::shared_mutex> lock(pbeast::receiver::DataProvider::s_data_provider_mutex);
            if (pbeast::receiver::DataProvider::s_data_provider)
              {
                static_cast<pbeast::receiver::IS_DataProvider *>(pbeast::receiver::DataProvider::s_data_provider)->check_readers();
              }
          }
      }
  }
};

void
pbeast::receiver::IS_DataProvider::start_check_receivers_thread(unsigned int sleep_interval)
{
  CheckProvidersThread command(sleep_interval);
  std::thread thrd(command);
  thrd.detach();
}
