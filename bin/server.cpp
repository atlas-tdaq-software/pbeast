#include <string.h>

#include <iostream>
#include <thread>

#include <libmicrohttpd/microhttpd.h>

#include <boost/program_options.hpp>

#include <pmg/pmg_initSync.h>
#include <ipc/signal.h>

#include <config/Configuration.h>

#include <dal/BaseApplication.h>
#include <dal/Partition.h>
#include <dal/util.h>

#include "application_exceptions.h"
#include "server.h"
#include "server_monitoring.h"


pbeast::Server * pbeast::Server::s_srv(nullptr);

namespace po = boost::program_options;


int
main(int argc, char ** argv)
{
  // initialize IPC and set default CORBA parameters

  try
    {
      IPCCore::init(argc, argv);
    }
  catch (ers::Issue & ex)
    {
      ers::fatal(ex);
      return 1;
    }

  // command line parameters

  std::string conf_db_name;
  std::string name;
  long pbeast_thread_pool_size = 32;
  uint16_t port = 8080;
  unsigned int http_timeout = 120;
  unsigned int http_thread_pool_size = 0;
  uint32_t http_request_downsample_threshold = 30;
  bool create_sync_file(false);

  if (const char * env = getenv("TDAQ_DB"))
    {
      conf_db_name = env;
    }

  if (const char * env = getenv("TDAQ_APPLICATION_OBJECT_ID"))
    {
      name = env;
    }

  po::options_description desc("The P-BEAST Server provides remote access to P-BEAST data.");

  try
    {
      desc.add_options()
        (
          "name,n",
          po::value<std::string>(&name)->default_value(name),
          "Name of application object (also TDAQ_APPLICATION_OBJECT_ID process environment variable)"
        )
        (
          "data,d",
          po::value<std::string>(&conf_db_name)->default_value(conf_db_name),
          "Name of the configuration database (also TDAQ_DB process environment variable)"
        )
        (
          "http-port,p",
          po::value<uint16_t>(&port)->default_value(port),
          "Http port"
        )
        (
          "pbeast-thread-pool-size,t",
          po::value<long>(&pbeast_thread_pool_size)->default_value(pbeast_thread_pool_size),
          "Size of threads pool used to communicate with P-BEAST servers (minimum 4)"
        )
        (
          "http-thread-pool-size,q",
          po::value<unsigned int>(&http_thread_pool_size)->default_value(http_thread_pool_size),
          "If provided, defines number of threads processing http requests and turns threads-pool mode on; otherwise use thread-per-connection mode"
        )
        (
          "http-timeout,o",
          po::value<unsigned int>(&http_timeout)->default_value(http_timeout),
          "Timeout of http requests"
        )
        (
          "http-request-downsample-threshold,e",
          po::value<unsigned int>(&http_request_downsample_threshold)->default_value(http_request_downsample_threshold),
          "Use downsample data for any requests above this threshold (in seconds) per data point. Otherwise read raw data and downsample when needed."
        )
        (
          "create-pmg-sync-file,s",
          "create pmg synchronization file at startup"
        )
        (
          "help,h",
          "Print help message"
        );

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      if (vm.count("create-pmg-sync-file"))
        create_sync_file = true;

      po::notify(vm);
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << ex.what() << "\nUsage: " << desc;
      ers::fatal(daq::pbeast::CommandLineError(ERS_HERE, text.str()));
      return 1;
    }

  if (pbeast_thread_pool_size < 4)
    {
      pbeast_thread_pool_size = 4;
    }

  ERS_DEBUG(1, "pbeast_thread_pool_size = " << pbeast_thread_pool_size);

  try
    {
      std::set<std::string> initial_receivers;

      // read configuration DB and search for names of P-BEAST receiver applications in initial partition
        {
          Configuration db(conf_db_name);
          const std::string partition_name("initial");

          if (const daq::core::Partition * partition = daq::core::get_partition(db, partition_name))
            {
              db.register_converter(new daq::core::SubstituteVariables(*partition));

              std::set<std::string> types({ "PBeastApplication" });

              for (auto & x : partition->get_all_applications(&types))
                initial_receivers.insert(x->UID());
            }
          else
            {
              throw daq::pbeast::FailedReadConfiguration(ERS_HERE, partition_name, daq::core::Partition::s_class_name );
            }
        }

        {
          pbeast::Server * srv = new pbeast::Server(name, initial_receivers, pbeast_thread_pool_size, port, http_timeout, http_thread_pool_size, http_request_downsample_threshold);

          try
            {
              srv->publish();
            }
          catch (ers::Issue & ex)
            {
              throw daq::pbeast::PublishFailed(ERS_HERE, ex );
            }

          if (srv->is_http_running() == false)
            {
              throw daq::pbeast::StartHttpDaemonFailed(ERS_HERE, port);
            }

          // publish calls statistics every 10"
          srv->start_monitoring_thread(pbeast::srv::Monitoring::mon_sleep_interval);

          if (create_sync_file)
            pmg_initSync();

          auto sig_received = daq::ipc::signal::wait_for();

          ERS_LOG("Caught signal " << sig_received << ", exiting...");

          srv->stop_services();
          srv->_destroy(true);
        }
    }
  catch (const ers::Issue& ex)
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE, ex));
      return 1;
    }

  return 0;
}


void
pbeast::Server::stop_services()
{
  MHD_stop_daemon(m_mhd);
  stop_monitoring();
}


pbeast::Server::Server(const std::string& name, const std::set<std::string>& initial_receivers, long pbeast_thread_pool_size, uint16_t http_port, unsigned int http_timeout, unsigned int http_thread_pool_size, uint32_t http_request_downsample_threshold) :
    IPCNamedObject<POA_pbeast::server>(name), m_monitor(new pbeast::srv::Monitoring()), m_initial_receivers(initial_receivers), m_thread_pool(pbeast_thread_pool_size), m_http_request_downsample_threshold(http_request_downsample_threshold), m_mhd(nullptr)
{
  s_srv = this;

  const size_t connection_memory_limit(128 * 1024);
  const size_t connection_memory_increment(4 * 1024);

  if (http_thread_pool_size == 0)
    {
      m_mhd = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION, http_port, nullptr, nullptr, &answer_to_connection, nullptr,
          MHD_OPTION_CONNECTION_TIMEOUT, http_timeout,
          MHD_OPTION_CONNECTION_MEMORY_LIMIT, connection_memory_limit,
          MHD_OPTION_CONNECTION_MEMORY_INCREMENT, connection_memory_increment,
          MHD_OPTION_END);

      ERS_LOG("start http server in THREAD_PER_CONNECTION mode");
    }
  else
    {
      m_mhd = MHD_start_daemon(MHD_USE_SELECT_INTERNALLY, http_port, nullptr, nullptr, &answer_to_connection, nullptr,
          MHD_OPTION_CONNECTION_TIMEOUT, http_timeout,
          MHD_OPTION_THREAD_POOL_SIZE, http_thread_pool_size,
          MHD_OPTION_CONNECTION_MEMORY_LIMIT, connection_memory_limit,
          MHD_OPTION_CONNECTION_MEMORY_INCREMENT, connection_memory_increment,
          MHD_OPTION_END);

      ERS_LOG("start http server in USE_SELECT_INTERNALLY mode using pool of " << http_thread_pool_size << " threads");
    }

  ERS_LOG("http timeout: " << http_timeout << ", memory per connection limit: " << connection_memory_limit);
}

pbeast::Server::~Server()
{
  try
    {
      withdraw();
    }
  catch (ers::Issue & ex)
    {
      ers::error(daq::pbeast::WithdrawFailed(ERS_HERE, ex ) );
    }
}

void
pbeast::Server::shutdown()
{
  ERS_DEBUG(0, "shutdown request received");
  daq::ipc::signal::raise();
}

struct MonitoringThread
{
  const unsigned int m_sleep_interval;
  pbeast::Server * m_srv;


  MonitoringThread(unsigned int sleep_interval, pbeast::Server *s) :
      m_sleep_interval(sleep_interval), m_srv(s)
  {
    ;
  }

  void
  operator()()
  {
    while (m_srv->needs_monitoring())
      {
        if (m_srv->init_monitoring())
          m_srv->publish_monitoring_info();

        sleep(m_sleep_interval);
      }
  }
};

void
pbeast::Server::start_monitoring_thread(uint64_t sleep_interval)
{
  MonitoringThread obj(sleep_interval, this);
  std::thread thrd(obj);
  thrd.detach();
}

void
pbeast::Server::stop_monitoring()
{
  std::unique_lock lock(m_mon_thread_mutex);

  delete m_monitor;
  m_monitor = nullptr;

  ERS_LOG("stop monitoring");
}

bool
pbeast::Server::needs_monitoring()
{
  std::shared_lock lock(m_mon_thread_mutex);

  return (m_monitor != nullptr);
}

bool
pbeast::Server::init_monitoring()
{
  std::shared_lock lock(m_mon_thread_mutex);

  if (m_monitor)
    return m_monitor->init_monitoring();
  else
    return false;
}

void
pbeast::Server::publish_monitoring_info()
{
  std::shared_lock lock(m_mon_thread_mutex);

  if (m_monitor)
    m_monitor->publish_calls_stats();
}

void
pbeast::Server::add_rest(pbeast::srv::UserStat& user_call_stat, uint64_t processing_time, uint64_t size_of_data)
{
  std::shared_lock lock(m_mon_thread_mutex);

  if (m_monitor)
    {
      user_call_stat.add(processing_time, size_of_data);
      m_monitor->m_rest.add(processing_time, size_of_data);
    }
}

void
pbeast::Server::add_api(pbeast::srv::UserStat& user_call_stat, uint64_t processing_time, uint64_t size_of_data)
{
  std::shared_lock lock(m_mon_thread_mutex);

  if (m_monitor)
    {
      user_call_stat.add(processing_time, size_of_data);
      m_monitor->m_api.add(processing_time, size_of_data);
    }
}

void
pbeast::Server::add_corba(uint64_t processing_time, uint64_t size_of_data)
{
  std::shared_lock lock(m_mon_thread_mutex);

  if (m_monitor)
    {
      m_monitor->m_corba.add(processing_time, size_of_data);
    }

}

pbeast::srv::CorbaCall::~CorbaCall()
{
  pbeast::Server::s_srv->add_corba(get_interval(), m_size);
}
