#include <map>

#include <boost/spirit/include/karma.hpp>

#include <is/infodocument.h>
#include <is/inforeceiver.h>
#include <is/infoiterator.h>

#include "pbeast/data-type.h"

#include "receiver_attribute.h"
#include "is_receiver_class.h"
#include "is_receiver_data_provider.h"
#include "receiver_errors.h"


pbeast::receiver::IS_Class::IS_Class(const std::string& name, const ISInfoDocument& info_doc) :
    Class(name)
{
  try
    {
      m_type = info_doc;
      m_attributes.reserve(info_doc.attributeCount());

      std::ostringstream text;
      text << "class: " << name << '\n';

      for (unsigned int i = 0; i < info_doc.attributeCount(); ++i)
        {
          const ISInfoDocument::Attribute& a(info_doc.attribute(i));
          pbeast::DataType type(pbeast::str2type(a.typeName()));
          bool is_array(a.isArray());
          m_attributes.push_back(new Attribute(this, a.name(), type, a.typeName(), is_array, a.description()));
          text << a.name() << " of " << a.typeName() << (a.isArray() ? "[]" : "") << '\n';
        }

      ERS_DEBUG(1, text.str());
    }
  catch (daq::pbeast::BadInfoType& ex)
    {
      throw;
    }
  catch (ers::Issue &ex)
    {
      throw daq::pbeast::CannotReadClassInfoDocument(ERS_HERE, m_name, ex);
    }
  catch(CORBA::SystemException& ex)
    {
      throw daq::pbeast::CannotReadClassInfoDocument(ERS_HERE, m_name, daq::ipc::CorbaSystemException(ERS_HERE, &ex));
    }
  catch(...)
    {
      throw daq::pbeast::CannotReadClassInfoDocument(ERS_HERE, m_name);
    }
}


void
pbeast::receiver::IS_Class::update_object(const std::string& name, ISInfoAny& isa, ISInfo::Reason reason, const ISType * type)
{
  if (reason != ISInfo::Deleted)
    {
      const uint64_t update_ts(pbeast::receiver::info2time(isa));

      if (type == nullptr)
        add_updated(name, update_ts);

      update_object(name, isa, update_ts, (type ? *type : isa.type()), (type != nullptr));
    }
  else
    {
      ERS_DEBUG(2, "DELETE OBJECT " << name << '@' << m_name);

      if (type == nullptr)
        {
          timeval tv;
          gettimeofday(&tv, nullptr);
          add_eov(name, pbeast::mk_ts(tv.tv_sec, tv.tv_usec));
        }
    }
}

static void
report_bad_type(const pbeast::receiver::Attribute& a, unsigned int idx, pbeast::DataType type)
{
  std::ostringstream text;
  text << "expected type of attribute \"" << a.get_name() << "\" (index: " << idx << ") is " << pbeast::as_string(a.get_type()) << " instead of " << pbeast::as_string(type);
  throw std::runtime_error(text.str().c_str());
}

static void
report_bad_cardinality(const pbeast::receiver::Attribute& a, unsigned int idx, bool is_array)
{
  std::ostringstream text;
  text << "attribute \"" << a.get_name() << "\" (index: " << idx << ") is expected to be " << (is_array ? "a single-value" : "an array");
  throw std::runtime_error(text.str().c_str());
}

#define CHECK(type)                                                                                      \
  if ( __builtin_expect( ( a.get_type() != type ), 0) ) report_bad_type(a, i, type);                     \
  if ( __builtin_expect( ( a.get_is_array() != is_array ), 0) ) report_bad_cardinality(a, i, is_array);

#define CHECK2(type,type2)                                                                                     \
  if ( __builtin_expect( ( a.get_type() != type && a.get_type() != type2 ), 0) ) report_bad_type(a, i, type);  \
  if ( __builtin_expect( ( a.get_is_array() != is_array ), 0) ) report_bad_cardinality(a, i, is_array);


namespace pbeast
{
  namespace receiver
  {

    template<>
      uint32_t
      IS_Class::get<uint32_t, OWLDate>(ISInfoAny& s)
      {
        OWLDate date;
        s >> date;
        return static_cast<uint32_t>(date.c_time());
      }

    template<>
      void
      IS_Class::get_vector<uint32_t, OWLDate>(ISInfoAny& s, std::vector<uint32_t>& value)
      {
        std::vector<OWLDate> vec;
        s >> vec;
        for (OWLDate& v : vec)
          {
            value.push_back(static_cast<uint32_t>(v.c_time()));
          }
      }

    template<>
      uint64_t
      IS_Class::get<uint64_t, OWLTime>(ISInfoAny& s)
      {
        OWLTime time;
        s >> time;
        return pbeast::mk_ts(static_cast<uint32_t>(time.c_time()), time.mksec());
      }

    template<>
      void
      IS_Class::get_vector<uint64_t, OWLTime>(ISInfoAny& s, std::vector<uint64_t>& value)
      {
        std::vector<OWLTime> vec;
        s >> vec;
        for (OWLTime& v : vec)
          {
            value.push_back(pbeast::mk_ts(static_cast<uint32_t>(v.c_time()), v.mksec()));
          }
      }

  }
}


inline void make_nested_id(std::string& s, const pbeast::receiver::Attribute& a)
{
  s.push_back('/');
  s.append(a.get_name());
}

inline void make_nested_vector_id(std::string& s, unsigned int i)
{
  char buf[10];
  buf[0] = '[';
  char * ptr(buf+1);
  boost::spirit::karma::generate(ptr, boost::spirit::uint_, i);
  buf[ptr - buf] = ']';
  s.append(buf, ptr - buf + 1);
}


void
pbeast::receiver::IS_Class::update_object(Attribute& a, const std::string& name, unsigned int idx, ISInfoAny& s, const ISType & type)
{
  ISType entry_type = type.entryInfoType( idx );

  std::string id(name);
  make_nested_id(id, a);

  IS_Class * type_class = static_cast<IS_Class *>(a.get_type_class());

  if (a.get_is_array() == false)
    {
      type_class->update_object(id, s, ISInfo::Updated, &entry_type);
    }
  else
    {
      const int size(s.readArraySize());
      for (int i = 0; i < size; ++i)
        {
          std::string id2(id);
          make_nested_vector_id(id2,i);
          type_class->update_object(id2, s, ISInfo::Updated, &entry_type);
        }
    }

}

void
pbeast::receiver::IS_Class::update_object(const std::string& name, ISInfoAny& isa, uint64_t ts, const ISType & type, bool is_nested)
{
  const unsigned int attr_number(m_attributes.size());

  try
    {
      if (type.entries() != attr_number)
        {
          std::ostringstream text;
          text << "the object \"" << name << "\" has " << type.entries() << " attributes vs. " << attr_number << " attributes in the class description";
          throw std::runtime_error(text.str().c_str());
        }

      for (unsigned int i = 0; i < attr_number; i++)
        {
          Attribute& a = get_attribute(i);
          const bool is_array = type.entryArray(i);

          switch ( type.entryType(i) )
            {
              case ISType::Boolean:    CHECK(pbeast::BOOL);              update<bool>(a, name, isa, ts);             break;
              case ISType::S8:         CHECK(pbeast::S8);                update<char>(a, name, isa, ts);             break;
              case ISType::U8:         CHECK(pbeast::U8);                update<uint8_t>(a, name, isa, ts);          break;
              case ISType::S16:        CHECK(pbeast::S16);               update<int16_t>(a, name, isa, ts);          break;
              case ISType::U16:        CHECK(pbeast::U16);               update<uint16_t>(a, name, isa, ts);         break;
              case ISType::S32:        CHECK2(pbeast::S32,pbeast::ENUM); update<int32_t>(a, name, isa, ts);          break;
              case ISType::U32:        CHECK(pbeast::U32);               update<uint32_t>(a, name, isa, ts);         break;
              case ISType::S64:        CHECK(pbeast::S64);               update<int64_t>(a, name, isa, ts);          break;
              case ISType::U64:        CHECK(pbeast::U64);               update<uint64_t>(a, name, isa, ts);         break;
              case ISType::Float:      CHECK(pbeast::FLOAT);             update<float>(a, name, isa, ts);            break;
              case ISType::Double:     CHECK(pbeast::DOUBLE);            update<double>(a, name, isa, ts);           break;
              case ISType::String:     CHECK(pbeast::STRING);            update<std::string>(a, name, isa, ts);      break;
              case ISType::Date:       CHECK(pbeast::DATE);              update<uint32_t,OWLDate>(a, name, isa, ts); break;
              case ISType::Time:       CHECK(pbeast::TIME);              update<uint64_t,OWLTime>(a, name, isa, ts); break;
              case ISType::InfoObject: CHECK(pbeast::OBJECT);            update_object(a, name, i, isa, type);       break;
              default:                 throw std::runtime_error("unexpected IS type");
            }
        }
    }
  catch (std::exception& ex)
    {
      throw daq::pbeast::TypeMismatch(ERS_HERE, name, m_name, ex);
    }

  if (is_nested == false)
    {
      DataProvider::get_monitoring_object().add(attr_number);
    }
  else
    {
      DataProvider::get_monitoring_object().add_nested(attr_number);
    }
}
