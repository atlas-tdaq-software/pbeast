#include <system/Host.h>

#include <chrono>
#include <thread>

#include "application_exceptions.h"

#include "repository_server_monitoring.h"

using std::chrono_literals::operator""ms;

pbeast::repository_server::Monitoring::Monitoring(const std::string& server_id, const std::filesystem::path& path) :
    m_is_running                           (true),
    m_num_of_ds_calls                      (0),
    m_num_of_raw_calls                     (0),
    m_time_of_ds_calls                     (0),
    m_time_of_raw_calls                    (0),
    m_max_time_of_ds_call                  (0),
    m_max_time_of_raw_call                 (0),
    m_num_of_ds_objects                    (0),
    m_num_of_raw_objects                   (0),
    m_num_of_ds_data_points                (0),
    m_num_of_raw_data_points               (0),
    m_size_of_ds_data                      (0),
    m_size_of_raw_data                     (0),
    m_num_of_data_files                    (0),
    m_num_of_void_files                    (0),
    m_num_of_downsampled_files             (0),
    m_num_of_info_series_cache_hits        (0),
    m_size_of_downsampled_files            (0),
    m_size_of_read_headers                 (0),
    m_size_of_info_series_cache_hits       (0),
    m_num_of_primitive_data_reads          (0),
    m_num_of_void_timestamps_reads         (0),
    m_total_num_of_ds_calls                (0),
    m_total_num_of_raw_calls               (0),
    m_total_time_of_ds_calls               (0),
    m_total_time_of_raw_calls              (0),
    m_max_time_of_ds_calls                 (0),
    m_max_time_of_raw_calls                (0),
    m_total_num_of_ds_objects              (0),
    m_total_num_of_raw_objects             (0),
    m_total_num_of_ds_data_points          (0),
    m_total_num_of_raw_data_points         (0),
    m_total_size_of_ds_data                (0),
    m_total_size_of_raw_data               (0),
    m_total_num_of_data_files              (0),
    m_total_num_of_void_files              (0),
    m_total_num_of_downsampled_files       (0),
    m_total_num_of_info_series_cache_hits  (0),
    m_total_size_of_downsampled_files      (0),
    m_total_size_of_read_headers           (0),
    m_total_size_of_info_series_cache_hits (0),
    m_total_num_of_primitive_data_reads    (0),
    m_total_num_of_void_timestamps_reads   (0),
    m_series_info_cache_size               (0),
    m_downsampled_data_cache_size          (0),
    m_disk_info_object                     (nullptr),
    m_rep_server_mon_object                (nullptr),
    m_disk_info_object_pub_ts              (0),
    m_mon_db                               ("bstconfig"),
    m_calls_stats_info_name(std::string("pbeast-node-") + server_id),
    m_disk_info_name(std::string("pbeast-node-") + System::LocalHost::instance()->local_name()),
    m_repository_path(path)
{
}


void
pbeast::repository_server::Monitoring::stop_monitoring()
{
  publish_calls_stats();

  ERS_LOG(
    "TOTAL:\n"
    "  number of downsample data calls        : " << m_total_num_of_ds_calls << "\n"
    "  average time of downsample data call   : " << average_time(m_total_time_of_ds_calls, m_total_num_of_raw_calls) << " ms\n"
    "  maximum time of downsample data call   : " << to_ms(m_max_time_of_ds_calls) << " ms\n"
    "  number of raw data calls               : " << m_total_num_of_raw_calls << "\n"
    "  average time of raw data call          : " << average_time(m_total_time_of_raw_calls, m_total_time_of_raw_calls) << " ms\n"
    "  maximum time of raw data call          : " << to_ms(m_max_time_of_raw_calls) << " ms\n"
    "  number of read downsample objects      : " << m_total_num_of_ds_objects << "\n"
    "  number of read raw objects             : " << m_total_num_of_raw_objects << "\n"
    "  number of read downsample data points  : " << m_total_num_of_ds_data_points << "\n"
    "  number of read raw data points         : " << m_total_num_of_raw_data_points << "\n"
    "  size of read downsample data           : " << m_total_size_of_ds_data << " bytes\n"
    "  size of read raw data                  : " << m_total_size_of_raw_data << " bytes\n"
    "  number of read data files              : " << m_total_num_of_data_files << "\n"
    "  number of read void files              : " << m_total_num_of_void_files << "\n"
    "  number of downsampled files            : " << m_total_num_of_downsampled_files << "\n"
    "  number of info series cache hits       : " << m_total_num_of_info_series_cache_hits << "\n"
    "  size of downsampled files              : " << m_total_size_of_downsampled_files << "\n"
    "  size of read headers                   : " << m_total_size_of_read_headers << "\n"
    "  size of headers read from cache        : " << m_total_size_of_info_series_cache_hits << "\n"
    "  number of primitive data reads         : " << m_total_num_of_primitive_data_reads << "\n"
    "  number of void timestamps reads        : " << m_total_num_of_void_timestamps_reads << "\n"
  );

  try
    {
      // lock monitoring db access
      std::lock_guard<std::mutex> lock(m_mutex);

      if (m_disk_info_object)
        {
          m_mon_db.destroy(*m_disk_info_object);
          m_mon_db.commit("");
          m_disk_info_object = nullptr;
        }
    }
  catch (daq::config::Exception& ex)
    {
      ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "remove", ex));
    }

  // set most monitoring parameters to 0 before removal of the monitoring object

  publish_calls_stats();

  try
    {
      std::lock_guard<std::mutex> lock(m_mutex);

      m_mon_db.commit("");
    }
  catch (daq::config::Exception &ex)
    {
      ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "publish", ex));
    }

  std::this_thread::sleep_for(1ms);

  try
    {
      // lock monitoring db access
      std::lock_guard<std::mutex> lock(m_mutex);

      if (m_rep_server_mon_object)
        {
          m_mon_db.destroy(*m_rep_server_mon_object);
          m_mon_db.commit("");
          m_rep_server_mon_object = nullptr;
        }
    }
  catch (daq::config::Exception& ex)
    {
      ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "remove", ex));
    }

  m_is_running = false;
}

bool
pbeast::repository_server::Monitoring::init_monitoring()
{
  try
    {
      std::lock_guard<std::mutex> lock(m_mutex);

      if (m_mon_db.loaded() == false)
        m_mon_db.load("initial");
   }
  catch (daq::config::Exception &ex)
    {
      ers::warning(ex);
      return false;
    }

  return true;
}

void
pbeast::repository_server::Monitoring::publish_calls_stats()
{
  uint64_t num_of_ds_calls = m_num_of_ds_calls.exchange(0);
  uint64_t num_of_raw_calls = m_num_of_raw_calls.exchange(0);
  uint64_t time_of_ds_calls = m_time_of_ds_calls.exchange(0);
  uint64_t time_of_raw_calls = m_time_of_raw_calls.exchange(0);
  uint64_t num_of_ds_objects = m_num_of_ds_objects.exchange(0);
  uint64_t num_of_raw_objects = m_num_of_raw_objects.exchange(0);
  uint64_t num_of_ds_data_points = m_num_of_ds_data_points.exchange(0);
  uint64_t num_of_raw_data_points = m_num_of_raw_data_points.exchange(0);
  uint64_t size_of_ds_data = m_size_of_ds_data.exchange(0);
  uint64_t size_of_raw_data = m_size_of_raw_data.exchange(0);
  uint64_t num_of_data_files = m_num_of_data_files.exchange(0);
  uint64_t num_of_void_files = m_num_of_void_files.exchange(0);
  uint64_t num_of_downsampled_files = m_num_of_downsampled_files.exchange(0);
  uint64_t num_of_info_series_cache_hits = m_num_of_info_series_cache_hits.exchange(0);
  uint64_t size_of_downsampled_files = m_size_of_downsampled_files.exchange(0);
  uint64_t size_of_read_headers = m_size_of_read_headers.exchange(0);
  uint64_t size_of_info_series_cache_hits = m_size_of_info_series_cache_hits.exchange(0);
  uint64_t num_of_primitive_data_reads = m_num_of_primitive_data_reads.exchange(0);
  uint64_t num_of_void_timestamps_reads = m_num_of_void_timestamps_reads.exchange(0);

  const double max_time_of_ds_call = to_ms(m_max_time_of_ds_call.exchange(0));
  const double max_time_of_raw_call = to_ms(m_max_time_of_raw_call.exchange(0));

  // lock monitoring db access
  std::lock_guard<std::mutex> lock(m_mutex);

  if (!m_rep_server_mon_object)
    m_rep_server_mon_object = const_cast<pbeast::PBeastRepositoryServerMon*>(m_mon_db.create<pbeast::PBeastRepositoryServerMon>("", m_calls_stats_info_name, true));

  m_rep_server_mon_object->set_num_of_ds_calls(average_rate(num_of_ds_calls));
  m_rep_server_mon_object->set_num_of_raw_calls(average_rate(num_of_raw_calls));
  m_rep_server_mon_object->set_avearge_time_of_ds_call(average_time(time_of_ds_calls, num_of_ds_calls));
  m_rep_server_mon_object->set_avearge_time_of_raw_call(average_time(time_of_raw_calls, num_of_raw_calls));
  m_rep_server_mon_object->set_max_time_of_ds_call(to_ms(m_max_time_of_ds_call.exchange(0)));
  m_rep_server_mon_object->set_max_time_of_raw_call(max_time_of_raw_call);
  m_rep_server_mon_object->set_num_of_ds_objects(average_rate(num_of_ds_objects));
  m_rep_server_mon_object->set_num_of_raw_objects(average_rate(num_of_raw_objects));
  m_rep_server_mon_object->set_num_of_ds_data_points(average_rate(num_of_ds_data_points));
  m_rep_server_mon_object->set_num_of_raw_data_points(average_rate(num_of_raw_data_points));
  m_rep_server_mon_object->set_size_of_ds_data(average_rate(size_of_ds_data));
  m_rep_server_mon_object->set_size_of_raw_data(average_rate(size_of_raw_data));
  m_rep_server_mon_object->set_num_of_data_files(average_rate(num_of_data_files));
  m_rep_server_mon_object->set_num_of_void_files(average_rate(num_of_void_files));
  m_rep_server_mon_object->set_num_of_downsampled_files(average_rate(num_of_downsampled_files));
  m_rep_server_mon_object->set_num_of_info_series_cache_hits(average_rate(num_of_downsampled_files));
  m_rep_server_mon_object->set_size_of_downsampled_files(average_rate(size_of_downsampled_files));
  m_rep_server_mon_object->set_size_of_read_headers(average_rate(size_of_read_headers));
  m_rep_server_mon_object->set_size_of_info_series_cache_hits(average_rate(size_of_info_series_cache_hits));
  m_rep_server_mon_object->set_num_of_primitive_data_reads(average_rate(num_of_primitive_data_reads));
  m_rep_server_mon_object->set_num_of_void_timestamps_reads(average_rate(num_of_void_timestamps_reads));

  m_rep_server_mon_object->set_info_series_cache_size(m_series_info_cache_size);
  m_rep_server_mon_object->set_downsampled_data_cache_size(m_downsampled_data_cache_size);

  m_total_num_of_ds_calls += num_of_ds_calls;
  m_total_num_of_raw_calls += num_of_raw_calls;
  m_total_time_of_ds_calls += time_of_ds_calls;
  m_total_time_of_raw_calls += time_of_raw_calls;

  if (max_time_of_ds_call > m_max_time_of_ds_calls)
    m_max_time_of_ds_calls = max_time_of_ds_call;
  if (max_time_of_raw_call > m_max_time_of_raw_calls)
    m_max_time_of_raw_calls = max_time_of_raw_call;

  m_total_num_of_ds_objects += num_of_ds_objects;
  m_total_num_of_raw_objects += num_of_raw_objects;
  m_total_num_of_ds_data_points += num_of_ds_data_points;
  m_total_num_of_raw_data_points += num_of_raw_data_points;
  m_total_size_of_ds_data += size_of_ds_data;
  m_total_size_of_raw_data += size_of_raw_data;
  m_total_num_of_data_files += num_of_data_files;
  m_total_num_of_void_files += num_of_void_files;
  m_total_num_of_downsampled_files += num_of_downsampled_files;
  m_total_size_of_downsampled_files += size_of_downsampled_files;
  m_total_size_of_read_headers += size_of_read_headers;
  m_total_num_of_primitive_data_reads += num_of_primitive_data_reads;
  m_total_num_of_void_timestamps_reads += num_of_void_timestamps_reads;
  m_total_num_of_info_series_cache_hits += num_of_info_series_cache_hits;
  m_total_size_of_info_series_cache_hits += size_of_info_series_cache_hits;
}

void
pbeast::repository_server::Monitoring::publish_disk_info()
{
  try
    {
      std::filesystem::space_info info = std::filesystem::space(m_repository_path);

      // lock monitoring db access
      std::lock_guard<std::mutex> lock(m_mutex);

      if (!m_disk_info_object)
        {
          m_disk_info_object = const_cast<pbeast::PBeastServerDiskInfo*>(m_mon_db.create<pbeast::PBeastServerDiskInfo>("", m_disk_info_name, true));
          m_disk_info_object->set_host(System::LocalHost::instance()->full_local_name());
        }

      m_disk_info_object->set_available(info.available);
      m_disk_info_object->set_capacity(info.capacity);
      m_disk_info_object->set_used(info.capacity - info.available);
      m_disk_info_object->set_usage(static_cast<double>(m_disk_info_object->get_used()) / static_cast<double>(info.capacity));

      ERS_LOG("Disk monitoring:\n   available : " << info.available << "\n   capacity  : " << info.capacity << "\n   used      : " << m_disk_info_object->get_used() << "\n   usage     : " << m_disk_info_object->get_usage());
    }
  catch (const std::exception &ex)
    {
      ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "prepare description of ", ex));
    }
}

bool
pbeast::repository_server::Monitoring::commit(const char * action)
{
  try
    {
      m_mon_db.commit("");
      return true;
    }
  catch (const daq::config::Exception& ex)
    {
      ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, action, ex));
      return false;
    }
}
