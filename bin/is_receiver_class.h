#ifndef PBEAST_IS_RECEIVER_CLASS_H
#define PBEAST_IS_RECEIVER_CLASS_H

#include <is/type.h>
#include <is/info.h>
#include <is/infoiterator.h>

#include "receiver_attribute.h"
#include "receiver_class.h"

namespace pbeast
{
  namespace receiver
  {

    inline uint64_t
    info2time(ISInfoAny& s)
    {
      return pbeast::mk_ts(static_cast<uint32_t>(s.time().c_time()), s.time().mksec());
    }

    class IS_Class : public Class
    {

      friend class Attribute;
      friend class DataProvider;

    public:

      IS_Class(const std::string& name, const ISInfoDocument& i);

      IS_Class(const IS_Class& c, Attribute * parent) :
          Class(c, parent), m_type(c.m_type)
      {
        ;
      }

      ~IS_Class()
      {
        ;
      }

      IS_Class() = delete;

      IS_Class(const IS_Class&) = delete;

      IS_Class&
      operator=(const IS_Class&) = delete;

      Class *
      make_new(Attribute * a)
      {
        return new IS_Class(*this, a);
      }

      const ISType&
      get_type() const
      {
        return m_type;
      }

      // IS callback entry point
      void
      update_object(const std::string& name, ISInfoAny& isa, ISInfo::Reason reason, const ISType * type);

      // update values of attributes
      void
      update_object(const std::string& name, ISInfoAny& isa, uint64_t ts, const ISType & type, bool is_nested);

      template<typename T, typename F = T>
        static T
        get(ISInfoAny& s)
        {
          T value;
          s >> value;
          return value;
        }

      template<typename T, typename F = T>
        static void
        get_vector(ISInfoAny& s, std::vector<T>& value)
        {
          s >> value;
        }

      template<typename T, typename F = T>
        static void
        update(Attribute& a, const std::string& name, ISInfoAny& s, uint64_t current_ts)
        {
          if (a.get_is_array() == false)
            {
              T value = get<T, F>(s);
              static_cast<DataHolder<T> *>(a.get_raw_data())->add_data(name, current_ts, value);
            }
          else
            {
              std::vector<T> value;
              get_vector<T, F>(s, value);
              static_cast<VectorDataHolder<T> *>(a.get_raw_data())->add_data(name, current_ts, value);
            }
        }

      static void
      update_object(Attribute& a, const std::string& name, unsigned int idx, ISInfoAny& s, const ISType & type);

    private:

      ISType m_type;

    };

  }
}

#endif
