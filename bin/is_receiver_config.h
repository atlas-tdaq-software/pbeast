#ifndef PBEAST_IS_RECEIVER_CONFIG_H
#define PBEAST_IS_RECEIVER_CONFIG_H

#include <map>
#include <set>
#include <string>
#include <vector>

#include <boost/regex.hpp>

#include <is/criteria.h>

#include "receiver_config.h"

#include "pbeast/PBeastApplication.h"
#include "pbeast/PBeastSubscription.h"

namespace pbeast
{
  namespace receiver
  {

    class IS_InfoReader;

    class Subscription
    {

      friend class IS_DataProvider;

    public:

      Subscription(const pbeast::PBeastSubscription& obj);

      ~Subscription();

      bool
      get_read_when_subscribe() const
      {
        return m_read_when_subscribe;
      }

      ISCriteria::Logic
      get_logic() const
      {
        return m_logic;
      }

      const std::string&
      get_is_info_names() const
      {
        return m_is_info_names;
      }

      const std::string&
      get_is_type() const
      {
        return m_is_type;
      }

      bool
      get_include_sub_types() const
      {
        return m_include_sub_types;
      }

      bool
      get_exclude_this_type() const
      {
        return m_exclude_this_type;
      }

      bool
      match_server(const std::string& name) const;

      IS_InfoReader*&
      get_info_reader(const std::string name)
      {
        return m_info_readers[name];
      }

    private:

      bool m_read_when_subscribe;
      ISCriteria::Logic m_logic;

      std::string m_is_info_names;

      std::string m_is_type;
      bool m_include_sub_types;
      bool m_exclude_this_type;

      std::set<std::string> m_server_names;
      std::vector<boost::regex *> m_server_reg_exps;

      std::map<std::string, IS_InfoReader *> m_info_readers;

    };


    class IS_Config : public Config
    {

      friend class DataProvider;

    public:

      void
      init(const pbeast::PBeastApplication& obj);
      ~IS_Config();

      const std::vector<Subscription *>&
      get_subscriptions() const
      {
        return m_subscriptions;
      }

      uint32_t
      get_is_servers_check_interval() const
      {
        return m_is_servers_check_interval;
      }

    private:

      std::vector<Subscription *> m_subscriptions;
      uint32_t m_is_servers_check_interval;

    };

  }
}

#endif
