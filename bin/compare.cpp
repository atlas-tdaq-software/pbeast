#include <cstdint>

#include <string>
#include <filesystem>

#include <boost/program_options.hpp>

#include "pbeast/input-data-file.h"
#include "pbeast/output-data-file.h"
#include "pbeast/compare-files.h"

namespace po = boost::program_options;


int
main(int argc, char ** argv)
{
  po::options_description desc("P-BEAST utility to compare data files. It opens a file and saves it using latest data format. The file modification time is preserved.");

  std::string f1, f2;
  bool brief = false;

  try
    {
      desc.add_options()
          ("file1,f", po::value<std::string>(&f1)->required(), "Name of first file")
          ("file2,F", po::value<std::string>(&f2)->required(), "Name of second file")
          ("brief,q","Report only when files differ")
          ("help,h","Print help message");

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      if (vm.count("brief"))
        brief = true;

      po::notify(vm);

      pbeast::compare_files(f1, f2);

      if (brief == false)
        std::cout << "files \"" << f1 << "\" and \"" << f2 << "\" are identical" << std::endl;

      return 0;
    }
  catch (const daq::pbeast::FilesAreDifferent& ex)
    {
      std::cout << ex.what() << std::endl;
      return 1;
    }
  catch (const daq::pbeast::FileCompareFailed& ex)
    {
      ers::error(ex);
      return 2;
    }
  catch (const std::exception& ex)
    {
      std::cerr << "ERROR: " << ex.what() << std::endl;
      return 3;
    }
}
