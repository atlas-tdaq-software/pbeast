#ifndef PBEAST_REPOSITORY_SERVER_MONITORING_H
#define PBEAST_REPOSITORY_SERVER_MONITORING_H

#include <stdint.h>

#include <atomic>
#include <filesystem>
#include <string>

#include "pbeast/call-stats.h"

#include "PBeastServerDiskInfo.h"
#include "PBeastRepositoryServerMon.h"

#include "pbeast/PBeastServerDiskInfo.h"
#include "pbeast/PBeastRepositoryServerMon.h"

namespace pbeast
{
  class RepositoryServer;

  namespace repository_server
  {

    class Monitoring
    {

      friend class ::pbeast::RepositoryServer;

    public:

      Monitoring(const std::string& receiver_id, const std::filesystem::path& path);

      void
      stop_monitoring();

      // sleep constants for monitoring thread
      enum { mon_sleep_inteval = 20, mon_disk_info_sleep_inteval = 300 };

      void
      add_ds(uint_least64_t ns, uint_least64_t objs, uint_least64_t dps, uint_least64_t size)
      {
        m_num_of_ds_calls++;
        m_time_of_ds_calls += ns;
        update_maximum(m_max_time_of_ds_call, ns);
        m_num_of_ds_objects += objs;
        m_num_of_ds_data_points += dps;
        m_size_of_ds_data += size;
      }

      void
      add_raw(uint_least64_t ns, uint_least64_t objs, uint_least64_t dps, uint_least64_t size)
      {
        if (ns)
          {
            m_num_of_raw_calls++;
            m_time_of_raw_calls += ns;
            update_maximum(m_max_time_of_raw_call, ns);
          }
        m_num_of_raw_objects += objs;
        m_num_of_raw_data_points += dps;
        m_size_of_raw_data += size;
      }

      void
      add_any(const CallStats& stat)
      {
        m_num_of_data_files += stat.m_num_of_data_files;
        m_num_of_void_files += stat.m_num_of_void_files;
        m_num_of_downsampled_files += stat.m_num_of_downsampled_files;
        m_num_of_info_series_cache_hits += stat.m_num_of_info_series_cache_hits;
        m_size_of_downsampled_files += stat.m_size_of_downsampled_files;
        m_size_of_read_headers += stat.m_size_of_read_headers;
        m_size_of_info_series_cache_hits += stat.m_size_of_info_series_cache_hits;
        m_num_of_primitive_data_reads += stat.m_num_of_primitive_data_reads;
        m_num_of_void_timestamps_reads += stat.m_num_of_void_timestamps_reads;
      }

      void
      set_series_info_cache_size(uint64_t v)
      {
        m_series_info_cache_size = v;
      }

      void
      set_downsampled_data_cache_size(uint64_t v)
      {
        m_downsampled_data_cache_size = v;
      }

      bool
      init_monitoring();

      void
      publish_calls_stats();

      void
      publish_disk_info();

      bool
      commit(const char * action);

      bool
      is_running() const
      {
        return m_is_running;
      }

      uint32_t
      get_disk_info_object_pub_ts() const
      {
        return m_disk_info_object_pub_ts;
      }

      void
      set_disk_info_object_pub_ts(uint32_t value)
      {
        m_disk_info_object_pub_ts = value;
      }

    private:

      std::mutex m_mutex;
      bool m_is_running;

      std::atomic<uint_least64_t> m_num_of_ds_calls;
      std::atomic<uint_least64_t> m_num_of_raw_calls;
      std::atomic<uint_least64_t> m_time_of_ds_calls;
      std::atomic<uint_least64_t> m_time_of_raw_calls;
      std::atomic<uint_least64_t> m_max_time_of_ds_call;
      std::atomic<uint_least64_t> m_max_time_of_raw_call;
      std::atomic<uint_least64_t> m_num_of_ds_objects;
      std::atomic<uint_least64_t> m_num_of_raw_objects;
      std::atomic<uint_least64_t> m_num_of_ds_data_points;
      std::atomic<uint_least64_t> m_num_of_raw_data_points;
      std::atomic<uint_least64_t> m_size_of_ds_data;
      std::atomic<uint_least64_t> m_size_of_raw_data;
      std::atomic<uint_least64_t> m_num_of_data_files;
      std::atomic<uint_least64_t> m_num_of_void_files;
      std::atomic<uint_least64_t> m_num_of_downsampled_files;
      std::atomic<uint_least64_t> m_num_of_info_series_cache_hits;
      std::atomic<uint_least64_t> m_size_of_downsampled_files;
      std::atomic<uint_least64_t> m_size_of_read_headers;
      std::atomic<uint_least64_t> m_size_of_info_series_cache_hits;
      std::atomic<uint_least64_t> m_num_of_primitive_data_reads;
      std::atomic<uint_least64_t> m_num_of_void_timestamps_reads;

      uint64_t m_total_num_of_ds_calls;
      uint64_t m_total_num_of_raw_calls;
      uint64_t m_total_time_of_ds_calls;
      uint64_t m_total_time_of_raw_calls;
      uint64_t m_max_time_of_ds_calls;
      uint64_t m_max_time_of_raw_calls;
      uint64_t m_total_num_of_ds_objects;
      uint64_t m_total_num_of_raw_objects;
      uint64_t m_total_num_of_ds_data_points;
      uint64_t m_total_num_of_raw_data_points;
      uint64_t m_total_size_of_ds_data;
      uint64_t m_total_size_of_raw_data;
      uint64_t m_total_num_of_data_files;
      uint64_t m_total_num_of_void_files;
      uint64_t m_total_num_of_downsampled_files;
      uint64_t m_total_num_of_info_series_cache_hits;
      uint64_t m_total_size_of_downsampled_files;
      uint64_t m_total_size_of_read_headers;
      uint64_t m_total_size_of_info_series_cache_hits;
      uint64_t m_total_num_of_primitive_data_reads;
      uint64_t m_total_num_of_void_timestamps_reads;

      uint64_t m_series_info_cache_size;
      uint64_t m_downsampled_data_cache_size;

      pbeast::PBeastServerDiskInfo * m_disk_info_object;
      pbeast::PBeastRepositoryServerMon * m_rep_server_mon_object;
      uint32_t m_disk_info_object_pub_ts;
      Configuration m_mon_db;

      std::string m_calls_stats_info_name;
      std::string m_disk_info_name;

      const std::filesystem::path& m_repository_path;


    private:

      template<typename T>
        void
        update_maximum(std::atomic<T>& maximum_value, T const& value) noexcept
        {
          T prev_value = maximum_value;
          while (prev_value < value && !maximum_value.compare_exchange_weak(prev_value, value))
            ;
        }

      inline double
      to_ms(uint_least64_t v)
      {
        return (static_cast<double>(v) / 1000000.0);
      }

      inline double
      average_time(uint_least64_t sum, uint_least64_t num)
      {
        return (num ? to_ms(sum / num) : 0.0);
      }

      inline double
      average_rate(uint_least64_t sum)
      {
        return (static_cast<double>(sum) / static_cast<double>(mon_sleep_inteval));
      }
    };
  }
}


#endif
