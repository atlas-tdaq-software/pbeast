#include <pbeast/series-idl.h>

#include "application_exceptions.h"
#include "server.h"
#include "server_monitoring.h"
#include "utils.h"


#define ADD_PBEAST_SERVER(X) server_objects[i.first + '@' + X.name()] = i.second;

static void
add_pbeast_servers(IPCPartition& p, std::map<std::string, pbeast::server_var>& server_objects)
{
  std::map<std::string, pbeast::server_var> objects;

  try
    {
      p.getObjects<pbeast::server>(objects);

      for (auto &i : objects)
        {
          ADD_PBEAST_SERVER(p)
        }
    }
  catch (daq::ipc::Exception& ex)
    {
      ERS_DEBUG( 3 , "cannot get pbeast/server objects in partition " << p.name() );
    }
}

void
pbeast::Server::get_servers(const std::string& partition_name, std::map<std::string, pbeast::server_var>& server_objects, bool throw_empty)
{
  try
    {
      // read servers running in initial partition

      IPCPartition initial_partition;
      std::map<std::string, pbeast::server_var> objects;

      try
        {
          initial_partition.getObjects<pbeast::server>(objects);
        }
      catch (daq::ipc::Exception& ex)
        {
          throw daq::pbeast::ListPublishedObjectFailure(ERS_HERE, "pbeast/server", initial_partition.name(), ex);
        }

      for (auto &i : objects)
        {
          if (m_initial_receivers.find(i.first) == m_initial_receivers.end())
            {
              if (this->name() != i.first)
                {
                  ADD_PBEAST_SERVER(initial_partition)
                }
            }
          else if (partition_name.empty() || partition_name == initial_partition.name())
            {
              ADD_PBEAST_SERVER(initial_partition)
            }
        }

      if (partition_name == initial_partition.name())
        return;

      // read receivers running in user partition
      if (partition_name.empty() == false)
        {
          IPCPartition partition(partition_name);
          add_pbeast_servers(partition, server_objects);
        }

      // read receivers running in all partition
      else
        {
          std::list< IPCPartition > pl;
          initial_partition.getPartitions(pl);

          for(auto & i : pl )
            {
              add_pbeast_servers(i, server_objects);
            }
        }
    }
  catch (daq::pbeast::Exception& ex)
    {
      throw pbeast::Failure(ex.what());
    }

  if (server_objects.empty() && throw_empty)
    {
      throw pbeast::Failure("no p-beast repository servers and receivers running");
    }
}


void
pbeast::Server::send_command(const char* command_name, const ::pbeast::StringArray& command_args, ::CORBA::String_out command_output)
{
  command_output = pbeast::utils::string_dup("pbeast::Server::send_command: not implemented yet");
}

void
pbeast::Server::get_partitions(StringArray_out partitions)
{
  pbeast::srv::CorbaCall mon;

  std::set<std::string> data;

  get_partitions(data);

  partitions = new StringArray();
  partitions->length(data.size());

  mon.m_size = 4 * (data.size() + 1);

  unsigned int idx(0);
  for (auto & x : data)
    {
      (*partitions)[idx++] = CORBA::string_dup(x.c_str());
      mon.m_size += x.size();
    }
}


void
pbeast::Server::get_classes(const char* partition_name, StringArray_out classes)
{
  pbeast::srv::CorbaCall mon;

  std::set<std::string> data;

  get_classes(partition_name, data);

  classes = new StringArray();
  classes->length(data.size());

  mon.m_size = 4 * (data.size() + 1);

  unsigned int idx(0);
  for (auto & x : data)
    {
      (*classes)[idx++] = CORBA::string_dup(x.c_str());
      mon.m_size += x.size();
    }
}


void
pbeast::Server::get_attributes(const char* partition_name, const char* class_name, StringDataPointArrayList_out attributes)
{
  pbeast::srv::CorbaCall mon;

  std::map<uint64_t, std::set<std::string>> data;

  get_attributes(partition_name, class_name, data);

  struct AttrData
    {
      uint64_t m_created;
      uint64_t m_updated;
      std::set<std::string> * m_data;
      AttrData(uint64_t created, uint64_t updated, std::set<std::string> * data) :
          m_created(created), m_updated(updated), m_data(data)
      {
        ;
      }
    };

  std::vector<AttrData> v_data;

  for (auto &x : data)
    {
      if (v_data.empty())
        {
          v_data.emplace_back(x.first, x.first, &x.second);
        }
      else
        {
          AttrData &last(v_data.back());
          if (x.second == *last.m_data)
            last.m_updated = x.first;
          else
            v_data.emplace_back(x.first, x.first, &x.second);
        }
    }

  attributes = new StringDataPointArrayList();
  attributes->length(v_data.size());

  mon.m_size = 4 * (1 + v_data.size() * 6);

  unsigned int idx(0);
  for (auto & x : v_data)
    {
      auto& it((*attributes)[idx++]);
      it.ts.created = x.m_created;
      it.ts.updated = x.m_updated;
      it.data.length(x.m_data->size());
      unsigned int j(0);
      for (auto & y : *x.m_data)
        {
          it.data[j++] = CORBA::string_dup(y.c_str());
          mon.m_size += 4 + y.size();
        }
    }
}

void
pbeast::Server::get_attribute_info(const char* partition_name, const char* class_name, const char* attribute_name_path, S32DataPointList_out type, BooleanDataPointList_out is_array, StringDataPointList_out description)
{
  pbeast::srv::CorbaCall mon;

  std::map<std::string, pbeast::server_var> server_objects;
  get_servers(partition_name, server_objects);

  std::map<uint64_t, std::pair<int32_t, bool> > attr_data;
  std::map<uint64_t, std::string> description_data;


    {
      struct task
      {
        static void
        run(std::mutex& mutex, std::map<uint64_t, std::pair<int32_t, bool>>& attr_data, std::map<uint64_t, std::string>& description_data, const char * partition_name, const char * class_name, const char* attribute_name_path, Issues& errors, const std::string& server_name, pbeast::server_var server)
        {
          try
            {
              pbeast::S32DataPointList_var types;
              pbeast::BooleanDataPointList_var is_arrays;
              pbeast::StringDataPointList_var descriptions;
              server->get_attribute_info(partition_name, class_name, attribute_name_path, types, is_arrays, descriptions);

              if(types->length() != is_arrays->length())
                {
                  throw pbeast::Failure("\"is-array\" is not aligned with \"type\" attribute information (different length)");
                }

              std::lock_guard<std::mutex> lock(mutex);

              for (unsigned int idx = 0; idx < types->length(); idx++)
                {
                  if(types[idx].ts.created != is_arrays[idx].ts.created)
                    {
                      throw pbeast::Failure("\"is-array\" is not aligned with \"type\" attribute information (different creation time-stamp)");
                    }

                  if(types[idx].ts.updated != is_arrays[idx].ts.updated)
                    {
                      throw pbeast::Failure("\"is-array\" is not aligned with \"type\" attribute information (different update time-stamp)");
                    }

                  attr_data[types[idx].ts.created] = std::make_pair(types[idx].data,is_arrays[idx].data);
                  if(types[idx].ts.created != types[idx].ts.updated)
                    {
                      attr_data[types[idx].ts.updated] = std::make_pair(types[idx].data,is_arrays[idx].data);
                    }
                }

              for (unsigned int idx = 0; idx < descriptions->length(); idx++)
                {
                  description_data[descriptions[idx].ts.created] = descriptions[idx].data;
                  if(descriptions[idx].ts.created != descriptions[idx].ts.updated)
                    {
                      description_data[descriptions[idx].ts.updated] = descriptions[idx].data;
                    }
                }
            }

          CATCH_SERVER_EXCEPTIONS(make_method_params_name(server_name, "get_attributes", partition_name, class_name, attribute_name_path));

        }
      };

      std::mutex mutex;
      std::vector<std::future<void> > results;
      Issues issues;

      for (auto &i : server_objects)
        {
          results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(attr_data), std::ref(description_data), partition_name, class_name, attribute_name_path, std::ref(issues), std::ref(i.first), i.second)));
        }

      for (unsigned int i = 0; i < results.size(); ++i)
        {
          results[i].get();
        }

      issues.validate("get_attribute_info", attr_data.empty());
    }

    // fill type and is-array data
    {
      struct AttrInfoData
      {
        uint64_t m_created, m_updated;
        int32_t m_type;
        bool m_is_array;
        AttrInfoData(uint64_t created, uint64_t updated, int32_t type, bool is_array) :
            m_created(created), m_updated(updated), m_type(type), m_is_array(is_array)
        {
          ;
        }
      };

      std::vector<AttrInfoData> v_attr_info;

      for (auto & x : attr_data)
        {
          if (v_attr_info.empty())
            {
              v_attr_info.emplace_back(x.first, x.first, x.second.first, x.second.second);
            }
          else
            {
              AttrInfoData& last(v_attr_info.back());
              if (x.second.first == last.m_type && x.second.second == last.m_is_array)
                {
                  last.m_updated = x.first;
                }
              else
                {
                  v_attr_info.emplace_back(x.first, x.first, x.second.first, x.second.second);
                }
            }
        }

      type = new S32DataPointList();
      type->length(v_attr_info.size());

      is_array = new BooleanDataPointList();
      is_array->length(v_attr_info.size());

      unsigned int idx(0);
      for (auto & x : v_attr_info)
        {
          (*type)[idx].ts.created = (*is_array)[idx].ts.created = x.m_created;
          (*type)[idx].ts.updated = (*is_array)[idx].ts.updated = x.m_updated;
          (*type)[idx].data = x.m_type;
          (*is_array)[idx].data = x.m_is_array;
          idx++;
        }

      mon.m_size = 4 * (2 + v_attr_info.size() * 6);
    }

    // fill description data
    {
      struct DescriptionData
      {
        uint64_t m_created, m_updated;
        std::string m_data;
        DescriptionData(uint64_t created, uint64_t updated, const std::string& data) :
            m_created(created), m_updated(updated), m_data(data)
        {
          ;
        }
      };

      std::vector<DescriptionData> v_description_data;

      for (auto & x : description_data)
        {
          if (v_description_data.empty())
            {
              v_description_data.emplace_back(x.first, x.first, x.second);
            }
          else
            {
              DescriptionData& last(v_description_data.back());
              if (x.second == last.m_data)
                {
                  last.m_updated = x.first;
                }
              else
                {
                  v_description_data.emplace_back(x.first, x.first, x.second);
                }
            }
        }

      description = new StringDataPointList();
      description->length(v_description_data.size());

      mon.m_size += 4 * (1 + v_description_data.size() * 5);

      unsigned int idx(0);
      for (auto & x : v_description_data)
        {
          auto& it((*description)[idx++]);
          it.ts.created = x.m_created;
          it.ts.updated = x.m_updated;
          it.data = CORBA::string_dup(x.m_data.c_str());
          mon.m_size +=x.m_data.size();
        }
    }
}


void
pbeast::Server::get_attribute_type(const char* partition_name, const char* class_name, const char* attribute_name_path, AttributeTypeList_out data)
{
  pbeast::srv::CorbaCall mon;

  std::vector<AttrInfoData> v_attr_info;
  get_attribute_type(partition_name, class_name, attribute_name_path, v_attr_info);

  data = new AttributeTypeList();
  data->length(v_attr_info.size());

  mon.m_size = 4 * (1 + v_attr_info.size() * 6);

  unsigned int idx(0);
  for (auto & x : v_attr_info)
    {
      auto& it((*data)[idx++]);
      it.ts.created = x.m_created;
      it.ts.updated = x.m_updated;
      it.type = CORBA::string_dup(x.m_type.c_str());
      it.is_array = x.m_is_array;
      mon.m_size += x.m_type.size();
    }
}


void
pbeast::Server::get_schema(const char* partition_name, ClassInfoList_out schema)
{
  schema = new ClassInfoList();
  schema->length(0);
}

void
pbeast::Server::get_updated_objects(const char* partition_name, const char* class_name, const char* attribute_name_path, ::CORBA::ULongLong since, ::CORBA::ULongLong until, const char* object_mask, pbeast::StringArray_out objects)
{
  pbeast::srv::CorbaCall mon;

  std::set<std::string> data;

  get_updated_objects(partition_name, class_name, attribute_name_path, since, until, object_mask, data);

  objects = new pbeast::StringArray();

  objects->length(data.size());

  mon.m_size = 4 * (1 + data.size());

  unsigned int idx(0);
  for(auto & x : data)
    {
      objects[idx++] = CORBA::string_dup(x.c_str());
      mon.m_size += x.size();
    }
}


template<class X, class T, class V>
  void
  fill_map_void(const std::map<std::string, std::set<T>> &val, V &to, pbeast::srv::CorbaCall &mon)
  {
    to = new X();
    to->length(val.size());
    mon.m_size += 4 * (1 + val.size() * 2);

    unsigned int idx(0);
    for (auto &x : val)
      {
        auto &serie(to[idx++]);
        serie.object_id = CORBA::string_dup(x.first.c_str());
        serie.data.length(x.second.size());
        mon.m_size += 4 + x.first.size() + x.second.size() * sizeof(T);
        unsigned int j(0);
        for (auto &y : x.second)
          serie.data[j++] = y;
      }
  }

void
pbeast::Server::get_eovs(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out eov)
{
  pbeast::srv::CorbaCall mon;

  std::map<std::string, pbeast::server_var> server_objects;
  get_servers(partition_name, server_objects);

  std::map<std::string,std::set<uint64_t>> data;

  {
    struct task
    {
      static void
      run(std::mutex& mutex, std::map<std::string,std::set<uint64_t>>& data, const char * partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, Issues& errors, const std::string& server_name, pbeast::server_var server)
      {
        try
          {
            pbeast::V64ObjectList_var eovs;
            server->get_eovs(partition_name, class_name, since, until, prev_interval, next_interval, object_mask, is_regexp, eovs);

            std::lock_guard<std::mutex> lock(mutex);

            for (unsigned int idx = 0; idx < eovs->length(); idx++)
              {
                const char * serie_name = eovs[idx].object_id;
                std::set<uint64_t>& eov_ref(data[serie_name]);

                for (unsigned int i = 0; i < eovs[idx].data.length(); i++)
                  {
                    eov_ref.insert(eovs[idx].data[i]);
                  }
              }
          }

        CATCH_SERVER_EXCEPTIONS(make_method_params_name(server_name, "get_eovs", partition_name, class_name, nullptr, since, until, object_mask))
      }
    };

    std::mutex mutex;
    std::vector<std::future<void>> results;
    Issues issues;

    for (auto &i : server_objects)
      results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(data), partition_name, class_name, since, until, prev_interval, next_interval, object_mask, is_regexp, std::ref(issues), std::ref(i.first), i.second)));

    for (unsigned int i = 0; i < results.size(); ++i)
      results[i].get();

    issues.validate("get_eovs", data.empty(), results.size());
  }

  fill_map_void<V64ObjectList>(data, eov, mon);
}


#define GET_ALL( LIST_TYPE, FUNC, CPP_TYPE, SERIE, ASSIGN_CODE )                                                                                                                                              \
try                                                                                                                                                                                                           \
  {                                                                                                                                                                                                           \
    pbeast::srv::CorbaCall mon;                                                                                                                                                                               \
    std::map<std::string, std::set<uint64_t>> task_eov, task_upd;                                                                                                                                             \
    std::map<std::string,std::set<pbeast::SERIE<CPP_TYPE>>> out;                                                                                                                                              \
    FUNC(partition_name, class_name, attribute_name, since, until, prev_interval, next_interval, object_mask, is_regexp, pbeast::FunctionConfig(select_functions, 0), fill_upd, task_eov, task_upd, out, ""); \
    data = new pbeast::LIST_TYPE();                                                                                                                                                                           \
    data->length(out.size());                                                                                                                                                                                 \
    mon.m_size = 4 * (1 + 2 * out.size());                                                                                                                                                                    \
                                                                                                                                                                                                              \
    unsigned int idx(0);                                                                                                                                                                                      \
    for(auto & x : out)                                                                                                                                                                                       \
      {                                                                                                                                                                                                       \
        auto& serie((*data)[idx++]);                                                                                                                                                                          \
        serie.object_id = CORBA::string_dup(x.first.c_str());                                                                                                                                                 \
        serie.data.length(x.second.size());                                                                                                                                                                   \
        mon.m_size += x.first.size() + 4 * 5 * x.second.size();                                                                                                                                               \
                                                                                                                                                                                                              \
        unsigned int i(0);                                                                                                                                                                                    \
        for(auto & v : x.second)                                                                                                                                                                              \
          {                                                                                                                                                                                                   \
            auto& data_point(serie.data[i++]);                                                                                                                                                                \
            data_point.ts.created = v.get_ts_created();                                                                                                                                                       \
            data_point.ts.updated = v.get_ts_last_updated();                                                                                                                                                  \
            ASSIGN_CODE                                                                                                                                                                                       \
          }                                                                                                                                                                                                   \
      }                                                                                                                                                                                                       \
                                                                                                                                                                                                              \
    fill_map_void<V64ObjectList>(task_eov, eov, mon);                                                                                                                                                         \
    fill_map_void<V64ObjectList>(task_upd, upd, mon);                                                                                                                                                         \
  }                                                                                                                                                                                                           \
  catch(std::exception& ex)                                                                                                                                                                                   \
  {                                                                                                                                                                                                           \
    throw pbeast::BadRequest(ex.what());                                                                                                                                                                      \
  }


#define GET_SIMPLE_MANY(LIST_TYPE, FUNC, CPP_TYPE, ASSIGN_CODE)             \
  GET_ALL( LIST_TYPE, FUNC, CPP_TYPE, SeriesData, ASSIGN_CODE )

#define GET_VECTOR_MANY(LIST_TYPE, FUNC, CPP_TYPE, ASSIGN_CODE)             \
  GET_ALL( LIST_TYPE, FUNC, CPP_TYPE, SeriesVectorData, ASSIGN_CODE )


#define ASSIGN_PRIMITIVE_OBJ_TYPE                                           \
  data_point.data = v.get_value();                                          \
  mon.m_size += sizeof(v.get_value());                                      \

#define ASSIGN_STRING_OBJ_TYPE                                              \
  data_point.data = CORBA::string_dup(v.get_value().c_str());               \
  mon.m_size += 4 + v.get_value().size();


void
pbeast::Server::get_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanObjectList_out data)
{
  GET_SIMPLE_MANY(BooleanObjectList, get_boolean_attributes, bool, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ObjectList_out data)
{
  GET_SIMPLE_MANY(S8ObjectList, get_s8_attributes, int8_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ObjectList_out data)
{
  GET_SIMPLE_MANY(U8ObjectList, get_u8_attributes, uint8_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ObjectList_out data)
{
  GET_SIMPLE_MANY(S16ObjectList, get_s16_attributes, int16_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ObjectList_out data)
{
  GET_SIMPLE_MANY(U16ObjectList, get_u16_attributes, uint16_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ObjectList_out data)
{
  GET_SIMPLE_MANY(S32ObjectList, get_s32_attributes, int32_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ObjectList_out data)
{
  GET_SIMPLE_MANY(U32ObjectList, get_u32_attributes, uint32_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ObjectList_out data)
{
  GET_SIMPLE_MANY(S64ObjectList, get_s64_attributes, int64_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ObjectList_out data)
{
  GET_SIMPLE_MANY(U64ObjectList, get_u64_attributes, uint64_t, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatObjectList_out data)
{
  GET_SIMPLE_MANY(FloatObjectList, get_float_attributes, float, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleObjectList_out data)
{
  GET_SIMPLE_MANY(DoubleObjectList, get_double_attributes, double, ASSIGN_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringObjectList_out data)
{
  GET_SIMPLE_MANY(StringObjectList, get_string_attributes, std::string, ASSIGN_STRING_OBJ_TYPE)
}


#define ASSIGN_PRIMITIVE_OBJ_VECTOR                                         \
  int idx2(0);                                                              \
  data_point.data.length(v.get_value().size());                             \
  mon.m_size += 4;                                                          \
  for(const auto & y : v.get_value())                                       \
    {                                                                       \
      (data_point.data)[idx2++] = y;                                        \
      mon.m_size += sizeof(y);                                              \
    }

#define ASSIGN_STRING_OBJ_VECTOR                                            \
  int idx2(0);                                                              \
  data_point.data.length(v.get_value().size());                             \
  mon.m_size += 4 * (1 + v.get_value().size());                             \
  for(const auto & y : v.get_value())                                       \
    {                                                                       \
      (data_point.data)[idx2++] = CORBA::string_dup(y.c_str());             \
      mon.m_size += y.size();                                               \
    }


void
pbeast::Server::get_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanArrayObjectList_out data)
{
  GET_VECTOR_MANY(BooleanArrayObjectList, get_boolean_array_attributes, bool, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S8ArrayObjectList, get_s8_array_attributes, int8_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U8ArrayObjectList, get_u8_array_attributes, uint8_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S16ArrayObjectList, get_s16_array_attributes, int16_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U16ArrayObjectList, get_u16_array_attributes, uint16_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S32ArrayObjectList, get_s32_array_attributes, int32_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U32ArrayObjectList, get_u32_array_attributes, uint32_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ArrayObjectList_out data)
{
  GET_VECTOR_MANY(S64ArrayObjectList, get_s64_array_attributes, int64_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ArrayObjectList_out data)
{
  GET_VECTOR_MANY(U64ArrayObjectList, get_u64_array_attributes, uint64_t, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatArrayObjectList_out data)
{
  GET_VECTOR_MANY(FloatArrayObjectList, get_float_array_attributes, float, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleArrayObjectList_out data)
{
  GET_VECTOR_MANY(DoubleArrayObjectList, get_double_array_attributes, double, ASSIGN_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringArrayObjectList_out data)
{
  GET_VECTOR_MANY(StringArrayObjectList, get_string_array_attributes, std::string, ASSIGN_STRING_OBJ_VECTOR)
}



#define GET_DS_ALL( LIST_TYPE, FUNC, CPP_TYPE, SERIE, ASSIGN_CODE )                                                                                        \
try                                                                                                                                                        \
  {                                                                                                                                                        \
    pbeast::srv::CorbaCall mon;                                                                                                                            \
    std::map<std::string, std::set<uint32_t>> task_eov, task_upd;                                                                                          \
    std::map<std::string,std::set<pbeast::SERIE<CPP_TYPE>>> out;                                                                                           \
    FUNC(partition_name, class_name, attribute_name, since, until, prev_interval, next_interval, interval, static_cast<pbeast::FillGaps::Type>(fill_gaps), \
         object_mask, is_regexp, pbeast::FunctionConfig(select_functions, interval), fill_upd, task_eov, task_upd, out, "");                               \
    data = new pbeast::LIST_TYPE();                                                                                                                        \
    if (out.empty() == false)                                                                                                                              \
      {                                                                                                                                                    \
        data->length(out.size());                                                                                                                          \
        mon.m_size = 4 + out.size() * 8;                                                                                                                   \
                                                                                                                                                           \
        unsigned int idx(0);                                                                                                                               \
        for(auto & y : out)                                                                                                                                \
          {                                                                                                                                                \
            auto& serie((*data)[idx++]);                                                                                                                   \
            serie.object_id = CORBA::string_dup(y.first.c_str());                                                                                          \
            serie.data.length(y.second.size());                                                                                                            \
            mon.m_size += y.first.size() + 4 * y.second.size();                                                                                            \
                                                                                                                                                           \
            unsigned int i(0);                                                                                                                             \
            for (auto & x : y.second)                                                                                                                      \
              {                                                                                                                                            \
                auto& data_point(serie.data[i++]);                                                                                                         \
                data_point.ts = x.get_ts();                                                                                                                \
                ASSIGN_CODE                                                                                                                                \
              }                                                                                                                                            \
          }                                                                                                                                                \
      }                                                                                                                                                    \
                                                                                                                                                           \
    fill_map_void<V32ObjectList>(task_eov, eov, mon);                                                                                                      \
    fill_map_void<V32ObjectList>(task_upd, upd, mon);                                                                                                      \
  }                                                                                                                                                        \
catch(std::exception& ex)                                                                                                                                  \
  {                                                                                                                                                        \
    throw pbeast::BadRequest(ex.what());                                                                                                                   \
  }


#define GET_DS_SIMPLE_MANY(LIST_TYPE, FUNC, CPP_TYPE, ASSIGN_CODE)                                                       \
  GET_DS_ALL( LIST_TYPE, FUNC, CPP_TYPE, DownsampledSeriesData, ASSIGN_CODE )

#define ASSIGN_DS_PRIMITIVE_OBJ_TYPE                                                                                     \
    data_point.data.min = x.data().get_min_value();                                                                      \
    data_point.data.max = x.data().get_max_value();                                                                      \
    data_point.data.val = x.data().get_value();                                                                          \
    mon.m_size += sizeof(double) + 2 * sizeof(x.data().get_min_value());

#define ASSIGN_DS_STRING_OBJ_TYPE                                                                                        \
    data_point.data.min = CORBA::string_dup(x.data().get_min_value().c_str());                                           \
    data_point.data.max = CORBA::string_dup(x.data().get_max_value().c_str());                                           \
    data_point.data.val = CORBA::string_dup(x.data().get_value().c_str());                                               \
    mon.m_size += 12 + x.data().get_min_value().size() + x.data().get_value().size() + x.data().get_max_value().size();


void
pbeast::Server::get_downsample_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(BooleanDownSampleObjectList, get_downsample_boolean_attributes, bool, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S8DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S8DownSampleObjectList, get_downsample_s8_attributes, int8_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U8DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U8DownSampleObjectList, get_downsample_u8_attributes, uint8_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S16DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S16DownSampleObjectList, get_downsample_s16_attributes, int16_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U16DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U16DownSampleObjectList, get_downsample_u16_attributes, uint16_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S32DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S32DownSampleObjectList, get_downsample_s32_attributes, int32_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U32DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U32DownSampleObjectList, get_downsample_u32_attributes, uint32_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S64DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(S64DownSampleObjectList, get_downsample_s64_attributes, int64_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U64DownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(U64DownSampleObjectList, get_downsample_u64_attributes, uint64_t, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(FloatDownSampleObjectList, get_downsample_float_attributes, float, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(DoubleDownSampleObjectList, get_downsample_double_attributes, double, ASSIGN_DS_PRIMITIVE_OBJ_TYPE)
}

void
pbeast::Server::get_downsample_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::StringDownSampleObjectList_out data)
{
  GET_DS_SIMPLE_MANY(StringDownSampleObjectList, get_downsample_string_attributes, std::string, ASSIGN_DS_STRING_OBJ_TYPE)
}


#define GET_DS_VECTOR_MANY(LIST_TYPE, FUNC, CPP_TYPE, ASSIGN_CODE)                                     \
  GET_DS_ALL( LIST_TYPE, FUNC, CPP_TYPE, DownsampledSeriesVectorData, ASSIGN_CODE )


#define ASSIGN_DS_PRIMITIVE_OBJ_VECTOR                                                                 \
    int idx2(0);                                                                                       \
    data_point.data.length(x.data().size());                                                           \
    mon.m_size += 4;                                                                                   \
    for(const auto & y : x.data())                                                                     \
      {                                                                                                \
        auto& item(data_point.data[idx2++]);                                                           \
        item.min = y.get_min_value();                                                                  \
        item.max = y.get_max_value();                                                                  \
        item.val = y.get_value();                                                                      \
        mon.m_size += sizeof(double) + 2 * sizeof(y.get_min_value());                                  \
      }

#define ASSIGN_DS_STRING_OBJ_VECTOR                                                                    \
    int idx2(0);                                                                                       \
    data_point.data.length(x.data().size());                                                           \
    mon.m_size += 4;                                                                                   \
    for(const auto & y : x.data())                                                                     \
      {                                                                                                \
        auto& item(data_point.data[idx2++]);                                                           \
        item.min = CORBA::string_dup(y.get_min_value().c_str());                                       \
        item.max = CORBA::string_dup(y.get_max_value().c_str());                                       \
        item.val = CORBA::string_dup(y.get_value().c_str());                                           \
        mon.m_size += 12 + y.get_min_value().size() + y.get_value().size() + y.get_max_value().size(); \
      }

void
pbeast::Server::get_downsample_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(BooleanDownSampleArrayObjectList, get_downsample_boolean_array_attributes, bool, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S8DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S8DownSampleArrayObjectList, get_downsample_s8_array_attributes, int8_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U8DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U8DownSampleArrayObjectList, get_downsample_u8_array_attributes, uint8_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S16DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S16DownSampleArrayObjectList, get_downsample_s16_array_attributes, int16_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U16DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U16DownSampleArrayObjectList, get_downsample_u16_array_attributes, uint16_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S32DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S32DownSampleArrayObjectList, get_downsample_s32_array_attributes, int32_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U32DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U32DownSampleArrayObjectList, get_downsample_u32_array_attributes, uint32_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S64DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(S64DownSampleArrayObjectList, get_downsample_s64_array_attributes, int64_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U64DownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(U64DownSampleArrayObjectList, get_downsample_u64_array_attributes, uint64_t, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(FloatDownSampleArrayObjectList, get_downsample_float_array_attributes, float, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(DoubleDownSampleArrayObjectList, get_downsample_double_array_attributes, double, ASSIGN_DS_PRIMITIVE_OBJ_VECTOR)
}

void
pbeast::Server::get_downsample_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::StringDownSampleArrayObjectList_out data)
{
  GET_DS_VECTOR_MANY(StringDownSampleArrayObjectList, get_downsample_string_array_attributes, std::string, ASSIGN_DS_STRING_OBJ_VECTOR)
}
