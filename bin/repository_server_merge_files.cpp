#include <string>
#include <vector>
#include <filesystem>

#include "pbeast/exceptions.h"
#include "pbeast/data-type.h"
#include "pbeast/input-data-file.h"
#include "pbeast/output-data-file.h"

#include "repository_server_merge_errors.h"
#include "repository_server_merge_series.h"
#include "repository_server.h"


  // This class is used to destroy open input files

struct InputDataFiles {

  ~InputDataFiles() {
    for (auto &f : m_files) delete f;
  }

  pbeast::InputDataFile * first() { return m_files[0]; }

  std::vector<pbeast::InputDataFile *> m_files;
};


std::vector<std::string>
pbeast::merger::MergeDataBase::run(
    std::vector<std::string>& input_files,
    uint32_t idx,
    std::map<std::string, std::set<uint64_t> >& eov,
    FileType file_type,
    bool perform_downsample)
{
  std::vector<std::string> merged_files;

  // open files

  InputDataFiles input;
  input.m_files.reserve(input_files.size());

  for (auto &s : input_files)
    {
      try
        {
          pbeast::RepositoryServer::get_writer_lock().wait_read_permission();
          input.m_files.push_back(new pbeast::InputDataFile(s));
        }
      catch (std::exception& ex)
        {
          throw daq::pbeast::FileError(ERS_HERE, "read", s, ex);
        }
    }

  // keep files of the same type together (data-type and is-array)

  struct GroupOfFiles
  {
    uint32_t m_data_type;
    bool m_is_array;
    uint64_t m_usecs_min;
    uint64_t m_usecs_max;
    bool m_is_full;
    std::vector<pbeast::InputDataFile *> m_files;
    std::map<std::string,uint64_t> m_series_sizes;

    GroupOfFiles(pbeast::InputDataFile *f)
    {
      m_data_type = f->get_data_type();
      m_is_array = f->get_is_array();
      m_usecs_min = m_usecs_max = f->get_base_timestamp();
      m_is_full = false;
      m_files.push_back(f);

      std::shared_ptr<std::vector<uint64_t>> vec(f->estimate_in_memory_merge_size());
      std::size_t idx = 0;
      for(const auto& x : f->get_series_info())
        {
          if(vec->at(idx) > pbeast::RepositoryServer::instance().m_data_group_max_size)
            {
              ERS_DEBUG(0, "close group on the first file " << f->get_file_name() << " (serie " << x.first << " occupies in-memory " << vec->at(idx) << " bytes)");
              m_is_full = true;
              break;
            }

          m_series_sizes[x.first] = vec->at(idx++);
        }
    }
  };

  // common properties of a file
  std::string partition_name;
  std::string class_name;
  std::filesystem::path class_path;
  std::string attribute_name;

  // properties of files by groups
  std::vector<GroupOfFiles> groups;

  for (auto &f : input.m_files)
    {
      if (groups.empty())
        {
          partition_name = f->get_partition();
          class_name = f->get_class();
          class_path = f->get_class_path();
          attribute_name = f->get_attribute();
          groups.emplace_back(f);
        }
      else if (partition_name != f->get_partition())
        {
          throw daq::pbeast::IncompatibleFiles(ERS_HERE, f->get_file_name(),input.first()->get_file_name(), "different partitions");
        }
      else if(class_name != f->get_class())
        {
          throw daq::pbeast::IncompatibleFiles(ERS_HERE, f->get_file_name(), input.first()->get_file_name(), "different classes");
        }
      else if(class_path != f->get_class_path())
        {
          throw daq::pbeast::IncompatibleFiles(ERS_HERE, f->get_file_name(), input.first()->get_file_name(), "different class paths");
        }
      else if(attribute_name != f->get_attribute())
        {
          throw daq::pbeast::IncompatibleFiles(ERS_HERE, f->get_file_name(), input.first()->get_file_name(), "different attributes");
        }
      else
        {
          bool found_group(false);

          for (auto &x : groups)
            {
              if (x.m_is_full == false)
                {
                  if (x.m_data_type == f->get_data_type() && x.m_is_array == f->get_is_array())
                    {
                      std::shared_ptr<std::vector<uint64_t>> vec(f->estimate_in_memory_merge_size());
                      std::size_t idx = 0;
                      for (const auto &i : f->get_series_info())
                        {
                          auto ret = x.m_series_sizes.emplace(i.first, 0);
                          ret.first->second += vec->at(idx++);

                          if (ret.first->second > pbeast::RepositoryServer::instance().m_data_group_max_size)
                            {
                              x.m_is_full = true;
                              ERS_DEBUG(0, "close group on file \"" << f->get_file_name() << "\" (otherwise serie " << i.first << " will occupy in-memory " << ret.first->second << " bytes)");
                              break;
                            }
                        }

                      if (x.m_is_full)
                        continue;

                      found_group = true;

                      x.m_files.push_back(f);

                      if (f->get_base_timestamp() < x.m_usecs_min)
                        x.m_usecs_min = f->get_base_timestamp();

                      break;
                    }
                  else
                    {
                      x.m_is_full = true;
                      ERS_DEBUG(0, "close group on file \"" << f->get_file_name() << "\" data type change");
                    }
                }
            }

          if (found_group == false)
            groups.emplace_back(f);
        }
    }

  const std::string partition_dir_name(pbeast::DataFile::encode(partition_name));
  const std::string attribute_dir_name(file_type == DataFileType ? pbeast::DataFile::encode(attribute_name) : attribute_name);

  bool need_to_prepare_out_directory(true);

  unsigned int group_count(1);
  for (auto & x : groups)
    {
      // build set of all series for every group
      std::set<std::string> series;
      for (auto & f : x.m_files)
        {
          for (auto &s : f->get_series_info())
            {
              series.insert(s.first);
            }
        }

      // create output file name
      std::filesystem::path tmp_name = pbeast::OutputDataFile::make_file_name(
          m_db.get_repository_path(), partition_dir_name, class_path, attribute_dir_name,
          pbeast::ts2secs(x.m_usecs_min), 0, ".tmp");

      // prepare out directory, if needed
      if (need_to_prepare_out_directory)
        {
          pbeast::RepositoryServer::prepare_out_directory(tmp_name);
          need_to_prepare_out_directory = false;
        }

      // create out-file and merge group of files
      try
        {
          pbeast::OutputDataFile out_file(tmp_name.native(), partition_name, class_name, class_path, attribute_name, x.m_usecs_min, x.m_data_type, x.m_is_array, series.size(), pbeast::RepositoryServer::instance().m_merger_zip_config.get_zip_catalog(), pbeast::RepositoryServer::instance().m_merger_zip_config.zip_data(x.m_data_type), pbeast::RepositoryServer::instance().m_merger_zip_config.get_catalogize_strings(), 0);

          out_file.write_header();

          for (auto &s : series)
            {
              pbeast::RepositoryServer::get_writer_lock().wait_read_permission();
              merge(x.m_files, out_file, s, x.m_data_type, x.m_is_array, eov[s], x.m_usecs_max);
            }

          out_file.write_footer();

          int64_t bytes = out_file.close();

          std::filesystem::path final_name = pbeast::OutputDataFile::make_file_name(m_db.get_repository_path(), partition_dir_name, class_path, attribute_dir_name, pbeast::ts2secs(x.m_usecs_min), pbeast::ts2secs(x.m_usecs_max), ".pb", true);
          merged_files.push_back(final_name.native());

          if (std::filesystem::exists(final_name))
            {
              throw daq::pbeast::FileAlreadyExist(ERS_HERE, final_name);
            }

          try
            {
              std::filesystem::rename(tmp_name, final_name);
            }
          catch (const std::filesystem::filesystem_error& ex)
            {
              throw daq::pbeast::FileSystemOperationError(ERS_HERE, "rename file", tmp_name, final_name, ex.code().message());
            }

          std::ostringstream list_of_files, group_info;

          for (auto & f : x.m_files)
            list_of_files << " * \"" << f->get_file_name() << "\"\n";

          if(groups.size() > 1)
            group_info << " (group " << group_count++ << ')';

          ERS_LOG(
              "Merge " << x.m_files.size() << " files:\n" << list_of_files.str() <<
              " of " << pbeast::as_string(static_cast<pbeast::DataType>(x.m_data_type)) << (x.m_is_array ? "[]" : "") <<
              " items" << group_info.str() << " in " << series.size() << " series of " << attribute_name << '@' << class_name <<
              " to " << final_name << " (" << bytes << " bytes)"
          );

          pbeast::RepositoryServer::instance().create_eos_link(final_name.native());

          // downsample data files
          if (perform_downsample && file_type == DataFileType)
            {
              pbeast::RepositoryServer::instance().downsample_file(final_name);
            }
        }
      catch (daq::pbeast::FileAlreadyExist&)
        {
          throw;
        }
      catch (std::exception& ex)
        {
          std::cerr << "Caught " << ex.what() << std::endl;
          throw daq::pbeast::FileError(ERS_HERE, "merge", tmp_name, ex);
        }
    }

  return merged_files;
}

void
pbeast::merger::MergeVoid::merge(
    std::vector<pbeast::InputDataFile *>& input_files,
    pbeast::OutputDataFile& out_file,
    const std::string& name,
    uint32_t /*data_type*/,
    bool /*is_array*/,
    std::set<uint64_t>& /*eov*/,
    uint64_t& usecs_max)
{
  std::vector<pbeast::SeriesData<pbeast::Void> > data;
  std::set<uint64_t> eov;

  for (auto &f : input_files)
    {
      data.clear();
      f->read_serie(name, data);
      for (auto &i : data)
        {
          eov.insert(i.get_ts_created());
        }
    }

  std::vector<pbeast::SeriesData<pbeast::Void>> m_data;
  m_data.reserve(data.size());

  for (auto &i : eov)
    {
      m_data.emplace_back(i, i, pbeast::Void());
    }

  if (unsigned long size = m_data.size())
    {
      set_max(usecs_max, m_data[size - 1].get_ts_last_updated());
    }

  out_file.write_data<pbeast::Void>(name, m_data);
}


void
pbeast::merger::MergeData::merge(
    std::vector<pbeast::InputDataFile *>& input_files,
    pbeast::OutputDataFile& out_file,
    const std::string& name,
    uint32_t data_type,
    bool is_array,
    std::set<uint64_t>& eov,
    uint64_t& usecs_max)
{
  switch (data_type)
    {
  case pbeast::BOOL:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<bool>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<bool>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::S8:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<int8_t>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<int8_t>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::U8:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<uint8_t>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<uint8_t>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::S16:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<int16_t>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<int16_t>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::U16:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<uint16_t>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<uint16_t>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::S32:
  case pbeast::ENUM:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<int32_t>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<int32_t>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::U32:
  case pbeast::DATE:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<uint32_t>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<uint32_t>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::S64:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<int64_t>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<int64_t>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::U64:
  case pbeast::TIME:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<uint64_t>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<uint64_t>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::FLOAT:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<float>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<float>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::DOUBLE:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<double>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<double>(name, input_files, out_file, eov, usecs_max);
      }
    break;

  case pbeast::STRING:
    if (!is_array)
      {
        pbeast::merger::MergeSeriesData<std::string>(name, input_files, out_file, eov, usecs_max);
      }
    else
      {
        pbeast::merger::MergeSeriesVectorData<std::string>(name, input_files, out_file, eov, usecs_max);
      }
    break;

    }
}
