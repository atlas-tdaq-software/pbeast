#ifndef PBEAST_RECEIVER_MONITORING_H
#define PBEAST_RECEIVER_MONITORING_H

#include <stdint.h>
#include <sys/types.h>

#include <atomic>
#include <string>

#include <config/Configuration.h>

#include "pbeast/PBeastReceiverMon.h"

namespace pbeast
{
  namespace receiver
  {

    class Monitoring
    {

    public:

      Monitoring(const std::string& partition_name, const std::string& receiver_id);
      ~Monitoring();

      void
      add(unsigned int total)
      {
        m_num_of_tested_objects++;
        add_nested(total);
      }

      void
      add_nested(unsigned int total)
      {
        m_num_of_tested_attributes += total;
      }

      void
      save_file(unsigned int size)
      {
        m_num_of_saved_files++;
        m_size_of_saved_files += size;
      }

      void
      data_size(uint64_t size)
      {
        m_number_of_data_items = size;
      }

      void
      compact_time(unsigned int compact_time)
      {
        m_compact_time = compact_time;
      }

      void
      rebuild_eov_time(unsigned int rebuild_eov_time)
      {
        m_rebuild_eov_time = rebuild_eov_time;
      }

      void
      commit_time(unsigned int commit_time)
      {
        m_commit_time = commit_time;
      }

      void
      add_compact_data_points(const std::pair<uint64_t,uint64_t>& info)
      {
        m_num_of_updated_data_points += info.first;
        m_num_of_merged_data_points += info.second;
      }


      void
      publish();

    private:

      std::atomic<uint_least64_t> m_num_of_tested_objects;
      std::atomic<uint_least64_t> m_num_of_tested_attributes;
      std::atomic<uint_least64_t> m_num_of_updated_data_points;
      std::atomic<uint_least64_t> m_num_of_merged_data_points;
      std::atomic<uint_least64_t> m_num_of_saved_files;
      std::atomic<uint_least64_t> m_size_of_saved_files;

      uint64_t m_compact_time;
      uint64_t m_rebuild_eov_time;
      uint64_t m_commit_time;

      uint64_t m_total_num_of_tested_objects;
      uint64_t m_total_num_of_tested_attributes;
      uint64_t m_total_num_of_saved_files;
      uint64_t m_total_size_of_saved_files;

      uint64_t m_number_of_data_items;

      uint64_t m_total_num_of_updated_data_points;
      uint64_t m_total_num_of_merged_data_points;
      double m_data_compaction_efficiency;

      Configuration m_mon_db;
      const std::string m_info_name;
      pbeast::PBeastReceiverMon * m_mon_obj;
      std::mutex m_mutex;

      time_t m_started;

    };
  }
}


#endif
