#include <iostream>
#include <filesystem>
#include <string>

#include <boost/program_options.hpp>

#include <ers/ers.h>

#include <pbeast/meta.h>

#include "application_exceptions.h"


namespace po = boost::program_options;

int
main(int argc, char **argv)
{
  std::filesystem::path dir_from, dir_to;

  try
    {
      boost::program_options::options_description desc(
        "Merge P-BEAST meta files.\n"
        "\n"
        "The meta files from the source class directory are merged with destination ones.\n"
        "If the destination files do not exist, they are created from the source ones.\n"
        "The source files remain unchanged.\n"
        "\n"
        "Available options are");

     desc.add_options()
        ("source,s", boost::program_options::value<std::filesystem::path>(&dir_from)->required(), "Name of the source directory (will not be updated)")
        ("destination,o", boost::program_options::value<std::filesystem::path>(&dir_to)->required(), "Name of the destination directory")
        ("help,h", "Print help message");

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      boost::program_options::notify(vm);
    }
  catch (std::exception& ex)
    {
      ers::fatal( daq::pbeast::CommandLineError( ERS_HERE, ex.what() ) );
      return 1;
    }

  try
    {
      pbeast::Meta::check_and_merge_files(dir_from / pbeast::Meta::s_types_meta_filename, dir_to  / pbeast::Meta::s_types_meta_filename);
      pbeast::Meta::check_and_merge_files(dir_from / pbeast::Meta::s_descriptions_meta_filename, dir_to  / pbeast::Meta::s_descriptions_meta_filename);
    }
  catch(std::exception& ex)
    {
      ers::fatal ( daq::pbeast::ApplicationFailed(ERS_HERE, ex) );
      return 1;
    }

  return 0;
}
