#include <algorithm>
#include <cstdint>
#include <chrono>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include <pbeast/input-data-file.h>
#include <pbeast/series-print.h>
#include <pbeast/data-type.h>

namespace po = boost::program_options;

std::size_t dp_num(0), s_num(0);

template<class T>
void
print_data(bool print_series, const std::string& series_name, T data, bool print_raw_ts)
{
  s_num++;
  dp_num += data.size();

  if (print_series)
    {
      std::cout << "series " << series_name << " (" << data.size() << "):" << std::endl;
      pbeast::print(std::cout, data, nullptr, !print_raw_ts);
    }

  data.clear();
}


template<class T>
  void
  get_data(pbeast::InputDataFile& file, const std::string& series_name, bool print_series, bool print_raw_ts)
  {
    std::string name;

    if (file.get_is_array() == false)
      {
        if (file.is_downsampled() == false)
          {
            std::vector<pbeast::SeriesData<T> > data;

            if (series_name.empty())
              while (file.read_next(name, data))
                {
                  print_data(print_series, name, data, print_raw_ts);
                  data.clear();
                }
            else
              {
                file.read_serie(series_name, data);
                print_data(print_series, series_name, data, print_raw_ts);
              }
          }
        else
          {
            std::vector<pbeast::DownsampledSeriesData<T> > data;

            if (series_name.empty())
              while (file.read_next(name, data))
                {
                  print_data(print_series, name, data, print_raw_ts);
                  data.clear();
                }
            else
              {
                file.read_serie(series_name, data);
                print_data(print_series, series_name, data, print_raw_ts);
              }
          }
      }
    else
      {
        if (file.is_downsampled() == false)
          {
            std::vector<pbeast::SeriesVectorData<T> > data;

            if (series_name.empty())
              while (file.read_next(name, data))
                {
                  print_data(print_series, name, data, print_raw_ts);
                  data.clear();
                }
            else
              {
                file.read_serie(series_name, data);
                print_data(print_series, series_name, data, print_raw_ts);
              }
          }
        else
          {
            std::vector<pbeast::DownsampledSeriesVectorData<T> > data;

            if (series_name.empty())
              while (file.read_next(name, data))
                {
                  print_data(print_series, name, data, print_raw_ts);
                  data.clear();
                }
            else
              {
                file.read_serie(series_name, data);
                print_data(print_series, series_name, data, print_raw_ts);
              }
          }
      }
  }


int main(int argc, char* argv[]) {

  std::string data_file;
  std::string series_name;
  bool list_series(false);
  bool print_series(false);
  bool print_info(false);
  bool print_raw_ts(false);
  bool print_merge_memory_info(false);
  bool verbose(false);

  po::options_description desc("Print information and contents of P-BEAST data file.");

  try
    {
      std::streamsize precision(std::cout.precision());

      desc.add_options()
          ("data-file,f", po::value<std::string>(&data_file)->required(), "Name of data file")
          ("series-name,n", po::value<std::string>(&series_name), "Name of data series to print (print all, if not defined)")
          ("list-series,l", "List names of all data series")
          ("print-merge-in-memory-info,m", "Print prediction of RAM used by in-memory merge per data series")
          ("print-series,p", "Print data series")
          ("print-raw-timestamp,w", "Print raw timestamp (number of micro-seconds since Epoch)")
          ("precision,V", po::value<std::streamsize>(&precision)->default_value(precision), "Set floating point data output precision")
          ("print-info,i", "Print information about file")
          ("verbose,v", "Print verbose information")
          ("help,h", "Print help message");

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      if (vm.count("print-series"))
        print_series = true;

      if (vm.count("list-series"))
        list_series = true;

      if (vm.count("print-merge-in-memory-info"))
        print_merge_memory_info = true;

      if (vm.count("print-info"))
        print_info = true;

      if (vm.count("print-raw-timestamp"))
        print_raw_ts = true;

      if (vm.count("verbose"))
        verbose = true;

      po::notify(vm);

      if (precision != std::cout.precision())
        std::cout.precision(precision);
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      std::cerr << "ERROR: " << ex.what() << std::endl;
      return 1;
    }

  try
    {
      auto t1 = std::chrono::steady_clock::now();

      pbeast::InputDataFile file(data_file);

      if (verbose)
        std::cout << " * read file info with " << file.get_number_of_series() << " data series in " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-t1).count() / 1000. << " ms" << std::endl;

      if (print_info)
        {
          std::cout
              << std::boolalpha
              << "file version        : " << file.get_file_version() << std::endl
              << "data format         : " << (file.is_downsampled() ? "downsampled" : "raw") << std::endl
              << "partition           : " << file.get_partition() << std::endl
              << "class               : " << file.get_class() << std::endl
              << "attribute           : " << file.get_attribute() << std::endl
              << "data type           : " << pbeast::as_string(static_cast<pbeast::DataType>(file.get_data_type())) << std::endl
              << "is-array            : " << file.get_is_array() << std::endl
              << "number_of_series    : " << file.get_number_of_series() << std::endl
              << "zip catalog         : " << file.get_zip_catalog_flag() << std::endl
              << "zip data            : " << file.get_zip_data_flag() << std::endl
              << "catalogize strings  : " << file.get_catalogize_strings_flag() << std::endl;

          if (file.is_downsampled())
            std::cout << "downsample interval : " << file.get_downsample_interval() << std::endl;

          return 0;
        }


      if (list_series || print_merge_memory_info)
        {
          std::shared_ptr<std::vector<uint64_t>> merge_size;

          if (print_merge_memory_info)
            merge_size = file.estimate_in_memory_merge_size();

          unsigned int count(1);
          for (auto& i : file.get_series_info())
            {
              std::cout << count++ << "\t" << i.first;

              if(print_merge_memory_info)
                std::cout << ' ' << merge_size->at(count-2);

              std::cout << std::endl;
            }

          return 0;
        }

      t1 = std::chrono::steady_clock::now();

      if (file.get_data_type() == pbeast::BOOL)
        get_data<bool>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::S8)
        get_data<int8_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::U8)
        get_data<uint8_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::S16)
        get_data<int16_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::U16)
        get_data<uint16_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::S32)
        get_data<int32_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::U32)
        get_data<uint32_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::S64)
        get_data<int64_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::U64)
        get_data<uint64_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::FLOAT)
        get_data<float>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::DOUBLE)
        get_data<double>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::ENUM)
        get_data<int32_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::DATE)
        get_data<uint32_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::TIME)
        get_data<uint64_t>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::STRING)
        get_data<std::string>(file, series_name, print_series, print_raw_ts);
      else if (file.get_data_type() == pbeast::VOID)
        get_data<pbeast::Void>(file, series_name, print_series, print_raw_ts);

      if (!print_series || verbose)
        std::cout << " * read " << dp_num << " data points in " << s_num << " series" << std::endl;

      if (verbose)
        std::cout << " * read data in " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-t1).count() / 1000. << " ms" << std::endl;

      return 0;
    }
  catch (std::exception & ex)
    {
      std::cerr << "caught exception: " << ex.what() << std::endl;
      return -1;
    }

  return 0;
}
