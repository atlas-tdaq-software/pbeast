#ifndef PBEASTRECEIVERMON_H
#define PBEASTRECEIVERMON_H

#include <is/info.h>

#include <string>
#include <ostream>


// <<BeginUserCode>>

// <<EndUserCode>>
/**
 * Monitoring information published by P-BEAST receiver application
 * 
 * @author  produced by the IS generator
 */

class PBeastReceiverMon : public ISInfo {
public:

    /**
     * Number of tested objects per second
     */
    uint64_t                      num_of_tested_objects;

    /**
     * Number of tested attributes per second
     */
    uint64_t                      num_of_tested_attributes;

    /**
     * Number of saved files per second
     */
    uint64_t                      num_of_saved_files;

    /**
     * Size of saved files per second
     */
    uint64_t                      size_of_saved_files;

    /**
     * Time to compact (reorder IS data and remove duplicated values) data in milliseconds
     */
    uint64_t                      data_compaction_time;

    /**
     * Indicates fraction of data points merged during compaction (relation of number of merged data points to total).
     */
    double                        data_compaction_efficiency;

    /**
     * Time to rebuild EoV data before compaction in milliseconds
     */
    uint64_t                      rebuild_eov_time;

    /**
     * Time to commit data after compaction in milliseconds
     */
    uint64_t                      commit_time;

    /**
     * Number of data points in memory
     */
    uint64_t                      num_of_data_points;


    static const ISType & type() {
	static const ISType type_ = PBeastReceiverMon( ).ISInfo::type();
	return type_;
    }

    std::ostream & print( std::ostream & out ) const {
	ISInfo::print( out );
	out << std::endl;
	out << "num_of_tested_objects: " << num_of_tested_objects << "\t// Number of tested objects per second" << std::endl;
	out << "num_of_tested_attributes: " << num_of_tested_attributes << "\t// Number of tested attributes per second" << std::endl;
	out << "num_of_saved_files: " << num_of_saved_files << "\t// Number of saved files per second" << std::endl;
	out << "size_of_saved_files: " << size_of_saved_files << "\t// Size of saved files per second" << std::endl;
	out << "data_compaction_time: " << data_compaction_time << "\t// Time to compact (reorder IS data and remove duplicated values) data in milliseconds" << std::endl;
	out << "data_compaction_efficiency: " << data_compaction_efficiency << "\t// Indicates fraction of data points merged during compaction (relation of number of merged data points to total)." << std::endl;
	out << "rebuild_eov_time: " << rebuild_eov_time << "\t// Time to rebuild EoV data before compaction in milliseconds" << std::endl;
	out << "commit_time: " << commit_time << "\t// Time to commit data after compaction in milliseconds" << std::endl;
	out << "num_of_data_points: " << num_of_data_points << "\t// Number of data points in memory";
	return out;
    }

    PBeastReceiverMon( )
      : ISInfo( "PBeastReceiverMon" )
    {
	initialize();
    }

    ~PBeastReceiverMon(){

// <<BeginUserCode>>

// <<EndUserCode>>
    }

protected:
    PBeastReceiverMon( const std::string & type )
      : ISInfo( type )
    {
	initialize();
    }

    void publishGuts( ISostream & out ){
	out << num_of_tested_objects << num_of_tested_attributes << num_of_saved_files << size_of_saved_files;
	out << data_compaction_time << data_compaction_efficiency << rebuild_eov_time << commit_time;
	out << num_of_data_points;
    }

    void refreshGuts( ISistream & in ){
	in >> num_of_tested_objects >> num_of_tested_attributes >> num_of_saved_files >> size_of_saved_files;
	in >> data_compaction_time >> data_compaction_efficiency >> rebuild_eov_time >> commit_time;
	in >> num_of_data_points;
    }

private:
    void initialize()
    {

// <<BeginUserCode>>

// <<EndUserCode>>
    }


// <<BeginUserCode>>

// <<EndUserCode>>
};

// <<BeginUserCode>>

// <<EndUserCode>>
inline std::ostream & operator<<( std::ostream & out, const PBeastReceiverMon & info ) {
    info.print( out );
    return out;
}

#endif // PBEASTRECEIVERMON_H
