#ifndef PBEAST_UTILS_H
#define PBEAST_UTILS_H

#include <errno.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#include <string>
#include <sstream>

#include <ers/ers.h>

#include "pbeast/pbeast.hh"
#include "pbeast/downsample-fill-gaps.h"

namespace pbeast
{

  namespace utils
  {

    inline bool
    is_regexp(const char * object_mask)
    {
      return (object_mask && object_mask[0] != 0 && !(object_mask[0] == '.' && object_mask[1] == '*' && object_mask[2] == 0));
    }

    inline uint64_t
    str2size(const std::string& value, const std::string& parameter)
    {
      std::string::size_type value_len = value.size();

      if (value_len < 2)
        {
          std::ostringstream text;
          text << "the value of \'" << parameter << "\' parameter equal to \'" << value << "\' is too short (at least 2 characters are expected)";
          throw std::runtime_error(text.str().c_str());
        }

      const char last_char = value[value_len-1];
      char last_char_up(0);

      if(std::isdigit(last_char) == false)
        {
          value_len--;
          last_char_up = std::toupper(last_char);
        }

      int64_t result = ::atol(value.substr(0, value_len).c_str());

      if (result <= 0)
        {
          std::ostringstream text;
          text << "bad size number in \'" << parameter << "\' parameter equal to \'" << value << '\'';
          throw std::runtime_error(text.str().c_str());
        }

      switch(last_char_up)
        {
          case 'K' :  result *= 1000;            break;  // kilobytes
          case 'M' :  result *= 1000000;         break;  // megabytes
          case 'G' :  result *= 1000000000;      break;  // gigabytes

          case 0   :                             break;  // nothing
          case '\"':                             break;  // seconds
          case '\'':  result *= 60;              break;  // minutes
          case 'H' :  result *= 3600;            break;  // hours
          case 'D' :  result *= 3600 * 24;       break;  // days
          case 'W' :  result *= 3600 * 24 * 7;   break;  // weeks
          case 'Y' :  result *= 3600 * 24 * 365; break;  // years

          default:
            {
              std::ostringstream text;
              text << "bad size character in \'" << parameter << "\' parameter equal to \'" << value << '\'';
              throw std::runtime_error(text.str().c_str());

            }
        }

      return static_cast<uint64_t>(result);
    }

    constexpr char s_was_caused_separator[] = "\n\twas caused by: ";

    inline const std::string
    err2str(int error)
    {
      char buffer[1024];
      buffer[0] = 0;
      strerror_r(error, buffer, 1024);
      return std::string(buffer);
    }

    inline void
    err2str(const ers::Issue& ex, std::string& out)
    {
      out.append(s_was_caused_separator);
      out.append(ex.message());
      if(ex.cause())
        {
          err2str(*ex.cause(), out);
        }
    }

    inline std::string
    err2str(const ers::Issue& ex)
    {
      std::string out;
      err2str(ex, out);
      return out;
    }

    inline char*
    string_dup(const std::string& str)
    {
      return CORBA::string_dup(str.c_str());
    }

  }

}

#endif
