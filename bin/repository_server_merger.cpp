#include <errno.h>

#include <algorithm>
#include <chrono>
#include <cstdlib>
#include <filesystem>
#include <string>
#include <vector>

#include <boost/program_options.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>

#include <ers/ers.h>
#include <ipc/partition.h>

#include "pbeast/PBeastServerDiskInfo.h"

#include "pbeast/meta.h"
#include "pbeast/repository.h"

#include "repository_server.h"
#include "repository_server_merge_errors.h"
#include "repository_server_merge_series.h"
#include "application_exceptions.h"
#include "../src/system-command-output.h"
#include "utils.h"


bool
pbeast::RepositoryServer::merger_action()
{
  std::filesystem::directory_iterator end_iter;

  try
    {
      for (std::filesystem::directory_iterator ip(m_local_repository_path); ip != end_iter; ++ip)
        {
          if (std::filesystem::is_directory(ip->status()))
            {
              for (std::filesystem::directory_iterator ic(ip->path()); ic != end_iter; ++ic)
                {
                  if (std::filesystem::is_directory(ic->status()))
                    {
                      std::map<std::string, std::set<uint64_t> > eov;

                      // process eov and upd directories first

                      std::filesystem::path eov_path(ic->path() / pbeast::Repository::get_eov_attribute_name());
                      std::filesystem::path upd_path(ic->path() / pbeast::Repository::get_upd_attribute_name());

                      if (std::filesystem::is_directory(eov_path))
                        {
                          process_files(eov_path, eov, pbeast::merger::EovFileType);
                        }

                      if (std::filesystem::is_directory(upd_path))
                        {
                          process_files(upd_path, eov, pbeast::merger::UpdFileType);
                        }

                      if (m_repository_path != m_local_repository_path)
                        {
                          try
                            {
                              const std::filesystem::path descriptions_meta_file(ic->path() / "descriptions.meta");
                              const std::filesystem::path types_meta_file(ic->path() / "types.meta");
                              const std::filesystem::path out_dir(m_repository_path / ip->path().filename() / ic->path().filename());

                              prepare_out_directory(out_dir / "types.meta");

                              merge_meta_data(descriptions_meta_file, out_dir);
                              merge_meta_data(types_meta_file, out_dir);
                            }
                          catch (const ers::Issue& ex)
                            {
                              ers::error(ex);
                            }
                        }

                      process_directories(ic->path(), eov_path, upd_path, eov);
                    }
                }
            }
        }
    }
  catch (const ers::Issue& ex)
    {
      ers::error(ex);
    }
  catch (const std::filesystem::filesystem_error& ex)
    {
      ers::error(daq::pbeast::FileSystemReadError(ERS_HERE, ex.code().message()));
    }

  // move merged files across repositories once per day, if balancing is enabled
  if (m_disk_balance_enabled && ((m_sleep_interval * m_count) % (3600 * 24)) < m_sleep_interval)
    balance_distibuted_repositories();

  m_count++;

  return true;
}

void
pbeast::RepositoryServer::process_directories(const std::filesystem::path& path, const std::filesystem::path& ignore1, const std::filesystem::path& ignore2, std::map<std::string, std::set<uint64_t> >& eov)
{
  std::filesystem::directory_iterator end_iter;

  for (std::filesystem::directory_iterator i(path); i != end_iter; ++i)
    {
      if (std::filesystem::is_directory(i->status()) && i->path().native() != ignore1.native() && i->path().native() != ignore2.native())
        {
          process_directories(i->path(), ignore1, ignore2, eov);
          process_files(i->path(), eov, pbeast::merger::DataFileType);
        }
    }
}


std::filesystem::path
pbeast::RepositoryServer::get_eos_link(const std::string& file_name) const
{
  static std::string repository_path;

  if (repository_path.empty())
    {
      repository_path = m_local_repository_path.string();
      repository_path.push_back('/');
    }

  return (m_eos_repository_directory / file_name.substr((file_name.find(repository_path) == 0 ? m_local_repository_path.native().size() : m_repository_path.native().size())+1));
}

void
pbeast::RepositoryServer::create_eos_link(const std::string& file_name)
{
  if (m_eos_repository_directory.empty() == false)
    {
      std::filesystem::path eos_file_name(get_eos_link(file_name));
      prepare_out_directory(eos_file_name);

      try
        {
          if (std::filesystem::exists(eos_file_name))
            {
              ERS_DEBUG( 0 , "link " << eos_file_name << " exists already");
              return;
            }
        }
      catch (std::filesystem::filesystem_error& ex)
        {
          throw daq::pbeast::FileSystemError(ERS_HERE, "check existence of EOS file", eos_file_name, ex.code().message());
        }

      try
        {
          std::filesystem::create_symlink(file_name, eos_file_name);
        }
      catch (std::filesystem::filesystem_error& ex)
        {
          throw daq::pbeast::FileSystemOperationError(ERS_HERE, "create a symbolic link for file", file_name, eos_file_name, ex.code().message());
        }

      ERS_DEBUG( 0 , "link file \"" << file_name << "\" to " << eos_file_name);
    }
}


void
pbeast::RepositoryServer::process_files(const std::filesystem::path& path, std::map<std::string, std::set<uint64_t> >& eov, pbeast::merger::FileType file_type)
{
  const std::filesystem::directory_iterator end_iter;
  const uint32_t merge_max_duration(m_data_split_duration / 2);  // ignore all data files with duration > merge_max_duration
  const std::string valid_suffix(".pb");

  std::map<uint32_t, std::vector<std::string> > data;

  for (std::filesystem::directory_iterator f(path); f != end_iter; ++f)
    {
      if (std::filesystem::is_regular_file(f->status()) == false)
        continue;

      std::string full_file_name(f->path().string());
      std::string file_name(f->path().filename().string());

      uint32_t now = time(0);

      uint32_t max_idx = (now - m_max_receiver_delay) / m_data_split_duration;

      // notify about new data merge interval
      if (m_last_max_idx == 0)
        {
          m_last_max_idx = max_idx;
        }
      else if (m_last_max_idx != max_idx)
        {
          m_last_max_idx = max_idx;
          ERS_INFO("New data merge interval " << m_last_max_idx);
        }

      // ignore bad files
      if (m_bad.find(full_file_name) != m_bad.end())
        continue;

      // ignore processed files except EoV ones
      if (m_processed.find(full_file_name) != m_processed.end())
        continue;

      // process problematic files with low frequency (180 * 10" = 30')
      if (m_problematic.find(full_file_name) != m_problematic.end() && (m_count % 180) != 0)
        continue;

      std::string::size_type pos1 = file_name.find('-');

      if (pos1 == std::string::npos)
        {
          ers::error(daq::pbeast::BadDataFile(ERS_HERE, full_file_name, "cannot find dash symbol (-) in filename"));
          m_bad.insert(full_file_name);
          continue;
        }

      std::string::size_type pos2 = file_name.find('.', pos1);

      if (pos2 == std::string::npos)
        {
          ers::error(daq::pbeast::BadDataFile(ERS_HERE, full_file_name, "cannot find dot symbol (.) in filename"));
          m_bad.insert(full_file_name);
          continue;
        }

      std::string suffix = file_name.substr(file_name.find_last_of('.'));

      if (valid_suffix != suffix)
        {
          ERS_DEBUG( 0 , "ignore file " << f->path() << " with unexpected suffix " << suffix);
          continue;
        }

      // read EoV file in any case
      if (file_type == pbeast::merger::EovFileType)
        {
          read_eov(f->path(), eov);
        }

      std::string base_ts_str = file_name.substr(0, pos1);
      std::string duration_str = file_name.substr(pos1 + 1, pos2 - pos1 - 1);

      uint32_t duration = atol(duration_str.c_str());

      if (m_repository_path == m_local_repository_path && duration > merge_max_duration)
        {
          ERS_LOG("ignore file " << f->path() << " with duration " << duration << " greater " << merge_max_duration << " limit");
          m_processed.insert(full_file_name);
          continue;
        }

      uint32_t base_ts = atol(base_ts_str.c_str());

      if (base_ts > now)
        {
          ers::warning(daq::pbeast::BadDataFile(ERS_HERE, full_file_name, "ignore file with timestamp in future"));
          continue;
        }

      const uint32_t idx = base_ts / m_data_split_duration;

      if (idx >= max_idx)
        {
          ERS_DEBUG( 1 , "ignore fresh file " << f->path() << " (wait " << ((idx + 1) * m_data_split_duration + m_max_receiver_delay) - now << " s)");
          continue;
        }

      data[idx].push_back(full_file_name);

      ERS_DEBUG(2 , "file " << full_file_name << " => base-ts: \"" << base_ts << "\", duration: \"" << duration << '\"');
    }

  pbeast::Directory * dir = m_db.get_directory(path, false);

  // check necessity to downsample merged files
  bool perform_downsample = false;

  if (m_cache_ds_config.get_cache_path().empty() == false)
    {
      std::filesystem::path cache_path(m_cache_ds_config.get_cache_path() / path.native().substr(m_db.get_local_repository_path_len()));

      if (pbeast::Directory * cache_dir = m_db.get_directory(cache_path, false))
        {
          const std::time_t access_interval = time(nullptr) - cache_dir->get_last_accessed();

          if (access_interval < m_cache_downsample_threshold)
            {
              perform_downsample = true;
              ERS_DEBUG(1, "perform downsample of directory " << cache_path << " accessed " << access_interval << " seconds ago");
            }
        }
    }


  for (auto &j : data)
    {
      try
        {
          std::stable_sort(j.second.begin(), j.second.end()); // sort, since directory iteration is not ordered on some file systems

          std::vector<std::string> p;

          if (file_type == pbeast::merger::DataFileType)
            {
              pbeast::merger::MergeData obj(m_db);
              p = obj.run(j.second, j.first, eov, file_type, perform_downsample);
            }
          else
            {
              pbeast::merger::MergeVoid obj(m_db);
              p = obj.run(j.second, j.first, eov, file_type, perform_downsample);
            }

          m_processed.insert(p.begin(), p.end());
        }
      catch (std::exception& ex)
        {
          std::ostringstream files;
          std::copy(j.second.begin(), j.second.end(), std::ostream_iterator<std::string>(files, "\n"));
          ers::error(daq::pbeast::ProblematicDataFiles(ERS_HERE, files.str(), "merge failed", ex));

          for (auto &f : j.second)
            {
              m_problematic.insert(f);
            }

          continue;
        }

      if (dir)
        dir->remove_files(j.second);

      std::unique_lock lock(m_db.m_repository_files);

      for (auto &f : j.second)
        {
          try
            {
              std::filesystem::remove(f);
              ERS_DEBUG( 1 , "remove merged file \"" << f << '\"');
            }
          catch (const std::filesystem::filesystem_error& ex)
            {
              ers::fatal(daq::pbeast::FileSystemError(ERS_HERE, "remove merged file", f, ex.code().message()));
              m_bad.insert(f);
            }
        }
    }

}

void
pbeast::RepositoryServer::read_eov(const std::filesystem::path& path, std::map<std::string, std::set<uint64_t> >& eov)
{
  std::string file_name(path.string());

  try
    {
      ERS_DEBUG( 1 , "Reading EoV from file " << path);

      pbeast::InputDataFile f(file_name);
      std::vector<pbeast::SeriesData<pbeast::Void> > data;
      std::string serie_name;

      while (f.read_next(serie_name, data))
        {
          std::set<uint64_t>& serie_eov = eov[serie_name];

          for (auto &i : data)
            {
              serie_eov.insert(i.get_ts_created());
            }

          data.clear();
        }
    }
  catch (std::exception& ex)
    {
      ers::fatal(daq::pbeast::FileError(ERS_HERE, "read EoV file", file_name, ex));
    }
}

void
pbeast::RepositoryServer::prepare_out_directory(const std::filesystem::path& file)
{
  // create directory if it does not exist

  std::filesystem::path file_dir_name = file.parent_path();

  bool directory_exists;

  try
    {
      directory_exists = std::filesystem::exists(file_dir_name);
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "check existence of directory", file_dir_name, ex.code().message());
    }

  if (!directory_exists)
    {
      try
        {
          std::filesystem::create_directories (file_dir_name);
        }
      catch (const std::filesystem::filesystem_error & ex)
        {
          throw daq::pbeast::FileSystemError(ERS_HERE, "create directory", file_dir_name, ex.code().message());
        }
    }
  else if(std::filesystem::is_directory(file_dir_name) == false)
    {
      throw daq::pbeast::FileSystemPathIsNotDirectory(ERS_HERE, file_dir_name);
    }
}

void
pbeast::RepositoryServer::merge_meta_data(const std::filesystem::path& file_from, const std::filesystem::path& directory_to)
{
  boost::uintmax_t size;

  try
    {
      size = std::filesystem::file_size(file_from);
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::CannotGetFileSize(ERS_HERE, file_from, ex.code().message());
    }

  uintmax_t old_size = 0;
  const std::string& file_from_name(file_from.native());
  std::map<std::string, uintmax_t>::const_iterator i = m_meta_file_sizes.find(file_from_name);

  if (i != m_meta_file_sizes.end())
    {
      old_size = i->second;
    }

  if(old_size > size)
    {
      ers::warning(daq::pbeast::UnexpectedFileSize(ERS_HERE, file_from, old_size, size));
      return;
    }
  else if (old_size == size)
    {
      ERS_DEBUG( 3 , "size of file " << file_from << " remain unchanged (" << size << ") bytes" );
      return;
    }

  const std::filesystem::path file_to(directory_to / file_from.filename());

  try
    {
      pbeast::Meta::check_and_merge_files(file_from, file_to);
      m_meta_file_sizes[file_from_name] = size;
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemOperationError(ERS_HERE, "merge file", file_from, file_to, ex.code().message());
    }

  create_eos_link(file_to.native());
  ERS_LOG("mark file " << file_to << " to be copied to EOS");
}


void
pbeast::RepositoryServer::downsample_file(const std::filesystem::path& path)
{
  try
    {
      pbeast::CallStats dummu;
      pbeast::FileSet raw_files;
      raw_files.emplace(new InputDataFile(path.native()));
      m_db.downsample_files(raw_files, m_cache_ds_config.get_sample_intervals(), m_cache_ds_config, "", nullptr, false, dummu);
    }
  catch (const ers::Issue& ex)
    {
      ers::error(daq::pbeast::FileError(ERS_HERE, "downsample", path, ex));
    }
  catch (const std::exception& ex)
    {
      ers::error(daq::pbeast::FileError(ERS_HERE, "downsample", path, ex));
    }
}


pbeast::merger::File::File(const std::filesystem::path& file) :
    m_file(file), m_eos_file(pbeast::RepositoryServer::instance().get_eos_link(file.native())), m_size(std::filesystem::file_size(file))
{
}

void
pbeast::merger::File::check_eos_file() const
{
  try
    {
      if (std::filesystem::exists(m_eos_file) == true)
        {
          throw daq::pbeast::FileNotCopied(ERS_HERE, m_eos_file);
        }
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "check existence of EOS file", m_eos_file, ex.code().message());
    }
}

void
pbeast::RepositoryServer::search_class_directory(const std::filesystem::path& dir, std::map<uint64_t,std::set<pbeast::merger::File>>& info)
{
  const std::filesystem::directory_iterator end_iter;
  const std::string valid_suffix(".pb");

  const std::string descriptions_meta("descriptions.meta");
  const std::string types_meta("types.meta");

  for (std::filesystem::directory_iterator f(dir); f != end_iter; ++f)
    {
      if (std::filesystem::is_directory(f->status()) == true)
        {
          search_class_directory(f->path(), info);
        }
      else if (std::filesystem::is_regular_file(f->status()) == true)
        {
          std::string full_file_name(f->path().string());

          // ignore bad files
          if (m_bad_merged.find(full_file_name) != m_bad_merged.end())
            continue;

          std::string file_name(f->path().filename().string());

          if (file_name.find(descriptions_meta) != std::string::npos || file_name.find(types_meta) != std::string::npos)
            continue;

          std::string::size_type pos1 = file_name.find('-');

          if (pos1 == std::string::npos)
            {
              ers::error(daq::pbeast::BadDataFile(ERS_HERE, full_file_name, "cannot find dash symbol (-) in filename"));
              m_bad_merged.insert(full_file_name);
              continue;
            }

          std::string::size_type pos2 = file_name.find('.', pos1);

          if (pos2 == std::string::npos)
            {
              ers::error(daq::pbeast::BadDataFile(ERS_HERE, full_file_name, "cannot find dot symbol (.) in filename"));
              m_bad_merged.insert(full_file_name);
              continue;
            }

          std::string suffix = file_name.substr(file_name.find_last_of('.'));

          if (valid_suffix != suffix)
            {
              ERS_DEBUG( 0 , "ignore file " << f->path() << " with unexpected suffix " << suffix);
              continue;
            }

          const std::string base_ts_str = file_name.substr(0, pos1);

          uint32_t base_ts = atol(base_ts_str.c_str());

          const uint32_t idx = base_ts / m_data_split_duration;

          if (idx >= m_max_idx)
            {
              ERS_DEBUG( 1 , "ignore fresh file " << f->path());
              continue;
            }

          info[idx].insert(pbeast::merger::File(f->path()));
        }
    }
}


static void
remote_copy_file(const std::filesystem::path& path, const std::string& node)
{
  ERS_DEBUG(1, "copy file " << path << " on node \"" << node << '\"');

  std::string command("/usr/bin/rsync -Ra --no-implied-dirs \"");
  command.append(path.native());
  command.append("\" ");
  command.append(node);
  command.append(":/");

  pbeast::SystemCommandOutput cmd_out(command);

  pbeast::RepositoryServer::get_writer_lock().wait_read_permission();

  int status = system(command.c_str());

  if (status == -1)
    {
      std::ostringstream text;
      text << "cannot execute command \'" << command << "\': " << pbeast::utils::err2str(errno);
      errno = 0;
      throw daq::pbeast::RemoteCopyFailed(ERS_HERE, path, node, text.str());
    }
  else if (status != 0)
    {
      std::ostringstream text;
      text << "command \'" << command << "\' exit with error (code " << WEXITSTATUS(status) << "):\n" << cmd_out.get_output();
      throw daq::pbeast::RemoteCopyFailed(ERS_HERE, path, node, text.str());
    }
}

static void
remove_file(const std::filesystem::path& path)
{
  ERS_DEBUG(1, "remove file " << path);

  try
    {
      std::filesystem::remove(path);
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "remove file", path, ex.code().message());
    }
}


// The DataSize is used to print file size in human readable format (size of file, transfer rate)

struct DataSize
{
  double m_data;

  DataSize(double data) : m_data(data) { ; }

  void
  print(std::ostream& s) const
  {
    int i = 0;
    const char* units[] = { "B", "kB", "MB", "GB", "TB", "PB", "EB", "ZB", "YB" };

    double data = m_data;

    while (data > 1024.0)
      {
        data /= 1024.0;
        i++;
      }

    std::streamsize old_size = s.precision();

    s.precision(data < 1000 ? 3 : 4);
    s << data << ' ' << units[i];
    s.precision(old_size);
  }
};

std::ostream& operator<<(std::ostream& out, const DataSize& tr)
{
  tr.print(out);
  return out;
}

void
pbeast::RepositoryServer::balance_distibuted_repositories()
{
  if (init_monitoring())
  try
    {
      std::vector<const PBeastServerDiskInfo *> objs;
      m_monitor->m_mon_db.get(objs);

      struct DiskInfo
      {
        uint64_t m_capacity;
        uint64_t m_available;
        double m_usage;
        int64_t m_to_be_copied;

        DiskInfo() :
            m_capacity(0), m_available(0), m_usage(0), m_to_be_copied(0)
        {
          ;
        }

        DiskInfo(uint64_t capacity, uint64_t available, double usage) :
            m_capacity(capacity), m_available(available), m_usage(usage), m_to_be_copied(0)
        {
          ;
        }

        void
        add(uint64_t& total, uint64_t& used)
        {
          total += m_capacity;
          used += static_cast<uint64_t>(static_cast<double>(m_capacity)*m_usage);
        }

        void
        print(std::ostringstream& s, const char prefix, const std::string& name)
        {
          s << prefix << " node " << name << " available: " << DataSize(m_available) << " of " << DataSize(m_capacity) << " (use " << std::setprecision(4) << m_usage * 100 << "%)\n";
        }
      };

      std::map<std::string, DiskInfo> disk_info;
      DiskInfo this_node;

      time_t now = time(0);
      m_max_idx = now / m_data_split_duration - 2;

      for(auto& it : objs)
        {
          const PBeastServerDiskInfo& obj(*it);

          // if information was not updated more than one day, something is going wrong
          // cancel balancing in this case
          if ((now - obj.config_object().time().tv_sec) > 24 * 3600)
            {
              throw daq::pbeast::OutOfDateDiskInfo(ERS_HERE, obj.get_host(), boost::posix_time::to_simple_string(boost::posix_time::from_time_t(obj.config_object().time().tv_sec)));
            }

          if (obj.get_host() == m_host_name)
            this_node = DiskInfo(obj.get_capacity(), obj.get_available(), obj.get_usage());
          else
            disk_info[obj.get_host()] = DiskInfo(obj.get_capacity(), obj.get_available(), obj.get_usage());
        }

      if (this_node.m_capacity == 0)
        {
          throw daq::pbeast::NoDiskInfo(ERS_HERE, m_host_name);
        }

      if (disk_info.empty())
        {
          ERS_LOG("no other P-BEAST repositories found");
          return;
        }

      // print balancing info
        {
          std::ostringstream text;

          this_node.print(text, '*', m_host_name);

          for (auto& x : disk_info)
            {
              x.second.print(text, '-', x.first);
            }

          ERS_LOG("Nodes balancing info:\n" << text.str());
        }


      // Find final usage of each disk after balancing
      //
      // U[i]   => usage of disk i at given moment
      // C[i]   => capacity of disk i
      // i=0    => master disk
      // U      => final unknown usage (equal for every disk)
      //
      // (U-U[0])C[0] = sum[n=1..N]{(U[i]-U)C[i]}
      //
      // SOLUTION
      //
      // U = sum[n=0..N]{U[i]C[i]} / sum[n=0..N]{C[i]}

      uint64_t total(0);
      uint64_t used(0);

      for (auto& x : disk_info)
        {
          if ((this_node.m_usage - x.second.m_usage) >= m_disk_balance_threshold)
            {
              x.second.add(total, used);
            }
        }

      if (total > 0)
        {
          this_node.add(total, used);

          const double usage = static_cast<double>(used) / static_cast<double>(total);

          std::ostringstream text;

          for (auto& x : disk_info)
            {
              if ((this_node.m_usage - x.second.m_usage) >= m_disk_balance_threshold && x.second.m_usage < usage)
                {
                  x.second.m_to_be_copied = static_cast<int64_t>((usage - x.second.m_usage) * static_cast<double>(x.second.m_capacity));
                  text << "- move " << DataSize(x.second.m_to_be_copied) << " on node \"" << x.first << "\"\n";
                }
              else
                {
                  text << "- no need to balance node \"" << x.first << "\"\n";
                }
            }

          ERS_LOG("Balancing plan:\n* average usage on balanced nodes: " << std::setprecision(4) << usage * 100 << "%\n" << text.str());

          uint64_t min_idx(0xffffffffffffffff);


          // iterate partitions and top-level classes

          std::map<std::string,std::map<std::string,std::map<uint64_t,std::set<pbeast::merger::File>>>> files;

          std::filesystem::directory_iterator end_iter;
          std::filesystem::path path(m_repository_path == m_local_repository_path  ? m_local_repository_path : m_repository_path);

          for (std::filesystem::directory_iterator ip(path); ip != end_iter; ++ip)
            {
              if (std::filesystem::is_directory(ip->status()))
                {
                  const std::string partition_name(ip->path().filename().string());

                  for (std::filesystem::directory_iterator ic(ip->path()); ic != end_iter; ++ic)
                    {
                      if (std::filesystem::is_directory(ic->status()))
                        {
                          const std::string class_name(ic->path().filename().string());

                          std::map<uint64_t,std::set<pbeast::merger::File>>& ci(files[partition_name][class_name]);

                          search_class_directory(ic->path(), ci);

                          // check that all files have been copied by castor script
                          for (auto & y : ci)
                            {
                              try
                                {
                                  for (auto & x : y.second)
                                    {
                                      x.check_eos_file();
                                    }

                                  if (min_idx > y.first)
                                    min_idx = y.first;
                                }
                              catch (const ers::Issue& ex)
                                {
                                  ers::error(daq::pbeast::ClassRepositoryBalancingError(ERS_HERE,class_name,partition_name,y.first,ex));
                                  y.second.clear();
                                }
                            }
                        }
                    }
                }
            }

          // iterate remote disks and move / copy files

          std::set<std::filesystem::path> copied_meta_files;

          for (auto& x : disk_info)
            {
              while(x.second.m_to_be_copied > 0)
                {
                  for (auto & p : files)
                    {
                      for (auto & c : p.second)
                        {
                          std::map<uint64_t,std::set<pbeast::merger::File>>::iterator it = c.second.find(min_idx);

                          if (it != c.second.end() && it->second.empty() == false)
                            {
                              uint64_t size(0);

                              // find meta-information files and copy
                              // move (copy and remove) data files

                              const std::filesystem::path types_path = path / p.first / c.first / "types.meta";
                              const std::filesystem::path descriptions_path = path / p.first / c.first / "descriptions.meta";

                              try
                                {
                                  bool copied_types_meta_file(false);
                                  bool copied_descriptions_meta_file(false);

                                  if (copied_meta_files.find(types_path) == copied_meta_files.end())
                                    {
                                      if (std::filesystem::exists(types_path) == false)
                                        {
                                          throw daq::pbeast::FileSystemError(ERS_HERE, "find file", types_path, "file does not exist");
                                        }
                                      remote_copy_file(types_path, x.first);
                                      copied_meta_files.insert(types_path);
                                      copied_types_meta_file = true;
                                    }

                                  if (copied_meta_files.find(descriptions_path) == copied_meta_files.end())
                                    {
                                      if (std::filesystem::exists(descriptions_path) == false)
                                        {
                                          throw daq::pbeast::FileSystemError(ERS_HERE, "find file", descriptions_path, "file does not exist");
                                        }
                                      remote_copy_file(descriptions_path, x.first);
                                      copied_meta_files.insert(descriptions_path);
                                      copied_descriptions_meta_file = true;
                                    }

                                  auto t1 = std::chrono::steady_clock::now();

                                  std::ostringstream report;

                                  for (auto & f : it->second)
                                    {
                                      size += f.m_size;
                                      report << " - " << f.m_file << " (" << f.m_size << " bytes)\n";
                                      remote_copy_file(f.m_file, x.first);
                                    }

                                  auto t2 = std::chrono::steady_clock::now();

                                  if (copied_types_meta_file || copied_descriptions_meta_file)
                                    {
                                      report << "Copy on remote node \"" << x.first << "\":\n";

                                      if (copied_types_meta_file)
                                        report << " - " << types_path << "\n";

                                      if (copied_descriptions_meta_file)
                                        report << " - " << descriptions_path << "\n";
                                    }

                                  report << "Remove local files:\n";

                                    {
                                      std::unique_lock lock(m_db.m_repository_files);

                                      for (auto & f : it->second)
                                        {
                                          remove_file(f.m_file);
                                          remove_file(f.m_eos_file);

                                          report << " - " << f.m_eos_file << "\n";
                                        }
                                    }

                                  double ti = std::chrono::duration_cast<std::chrono::microseconds>(t2-t1).count() / 1000.;

                                  if (ti < 1.0)
                                    ti = 1.0;

                                  ERS_LOG("Move on node \"" << x.first << "\" " << DataSize(size) << " in " << std::setprecision(4) << ti/1000.0 << " s (" << DataSize((static_cast<double>(size)*1000.0)/ti) << "/s) for index " << min_idx << " of \"" << c.first << '@' << p.first << "\":\n" << report.str());

                                  x.second.m_to_be_copied -= size;
                                }
                              catch(ers::Issue& ex)
                                {
                                  ers::error(daq::pbeast::ClassRepositoryBalancingError(ERS_HERE,c.first,p.first,min_idx,ex));
                                }
                              catch(std::exception& ex)
                                {
                                  ers::error(daq::pbeast::ClassRepositoryBalancingError(ERS_HERE,c.first,p.first,min_idx,ex));
                                }

                              it->second.clear();
                            }

                          if (x.second.m_to_be_copied <= 0)
                            break;
                        }

                      if (x.second.m_to_be_copied <= 0)
                        break;
                    }

                  if (min_idx >= m_max_idx)
                    break;
                  else if (x.second.m_to_be_copied > 0)
                    min_idx++;
                }
            }
        }
      else
        {
          ERS_LOG("no balancing needed");
        }
    }
  catch(const ers::Issue& ex)
    {
      ers::error(daq::pbeast::CancelRepositoryBalancing(ERS_HERE, ex));
    }
  catch(const std::exception& ex)
    {
      ers::error(daq::pbeast::CancelRepositoryBalancing(ERS_HERE, ex));
    }
}
