#ifndef PBEAST_APPLICATION_EXCEPTIONS_H
#define PBEAST_APPLICATION_EXCEPTIONS_H

#include "pbeast/exceptions.h"

namespace daq
{

  /*! \class pbeast::FileSystemReadError
   *  This wrapper issue is reported when some operation reading file system failed
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileSystemReadError,
    Exception,
    "failed to read file system: " << why,
    ERS_EMPTY,
    ((std::string)why)
  )

  /*! \class pbeast::CannotCommitMetaInformation
   *  This issue is reported when metda information of a class cannot be committed
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotCommitMetaInformation,
    Exception,
    "cannot commit meta information for class " << class_name,
    ERS_EMPTY,
    ((std::string)class_name)
  )

  /*! \class pbeast::CommandLineError
   *  This issue is reported when there is a problem with application's command line
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CommandLineError,
    Exception,
    "command line problem: " << message,
    ERS_EMPTY,
    ((std::string)message)
  )

  /*! \class pbeast::FailedReadConfiguration
   *  This issue is reported when required configuration object is not found
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FailedReadConfiguration,
    Exception,
    "cannot find object \"" << id << '@' << class_name << '\"',
    ERS_EMPTY,
    ((std::string)id)
    ((std::string)class_name)
  )

  /*! \class pbeast::OperationalMonitoringError
   *  This issue is reported when there is a problem with publication of operational monitoring object
   */

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    OperationalMonitoringError,
    Exception,
    "cannot " << action << " monitoring data",
    ERS_EMPTY,
    ((std::string)action)
  )

  /*! \class pbeast::Error
   *  This issue is reported when there is a P-BEAST exception caught by application
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    Error,
    Exception,
    "P-BEAST " << exception_name << ": " << message,
    ERS_EMPTY,
    ((const char *)exception_name)
    ((const char *)message)
  )

  /*! \class pbeast::MethodFailure
   *  This issue is reported when P-BEAST method failed
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    MethodFailure,
    Exception,
    "method " << method << " caused P-BEAST error " << error_text,
    ,
    ((std::string)method)
    ((const char *)error_text)
  )

  /*! \class pbeast::FailedCreateHttpResponse
   *  This issue is reported when an application cannot create HTTP response
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FailedCreateHttpResponse,
    Exception,
    "Failed to create http response (" << size << " bytes)",
    ERS_EMPTY,
    ((uint32_t)size)
  )

  /*! \class pbeast::StartHttpDaemonFailed
   *  This issue is reported when an application cannot start HTTP daemon
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    StartHttpDaemonFailed,
    Exception,
    "Cannot start http daemon on port " << port,
    ERS_EMPTY,
    ((uint16_t)port)
  )


  /*! \class pbeast::ListPublishedObjectFailure
   *  This issue is reported when an application cannot check state of some servers
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    ListPublishedObjectFailure,
    Exception,
    "cannot get " << interface << " objects in partition " << partition,
    ERS_EMPTY,
    ((std::string)interface)
    ((std::string)partition)
  )

  /*! \class pbeast::ApplicationFailed
   *  This issue is reported when application exits because of fatal errors
   */
  ERS_DECLARE_ISSUE_BASE(pbeast, ApplicationFailed, Exception, "Application failed", ERS_EMPTY, ERS_EMPTY)

  /*! \class pbeast::PublishFailed
   *  This issue is reported when application cannot publish in IPC
   */
  ERS_DECLARE_ISSUE_BASE(pbeast, PublishFailed, Exception, "Cannot publish object in IPC", ERS_EMPTY, ERS_EMPTY)

  /*! \class pbeast::WithdrawFailed
   *  This issue is reported when application cannot withdraw publication in IPC
   */
  ERS_DECLARE_ISSUE_BASE(pbeast, WithdrawFailed, Exception, "Cannot remove publication of object in IPC", ERS_EMPTY, ERS_EMPTY)

}

#endif
