#ifndef PBEAST_REPOSITORY_SERVER_H
#define PBEAST_REPOSITORY_SERVER_H

#include <time.h>
#include <sys/time.h>

#include <cstdint>
#include <filesystem>
#include <map>
#include <mutex>
#include <set>
#include <string>
#include <vector>

#include <pbeast/repository.h>
#include <pbeast/downsample-config.h>
#include <pbeast/pbeast.hh>

#include "repository_server_monitoring.h"

class IPCAlarm;
class IPCPartition;

namespace pbeast
{

  namespace merger
  {
    struct File
    {
      const std::filesystem::path m_file;
      const std::filesystem::path m_eos_file;
      uint64_t m_size;

      File(const std::filesystem::path& file);

      void
      check_eos_file() const;

      inline bool
      operator<(const File & f) const
      {
        return (m_file < f.m_file);
      }
    };

    enum FileType
    {
      EovFileType, UpdFileType, DataFileType
    };

  }

  class RepositoryServerConfig
  {
  public:

    RepositoryServerConfig();

    bool
    is_merger_enabled() const
    {
      return (m_sleep_interval > 0);
    }

    std::filesystem::path m_local_repository_path;
    std::filesystem::path m_repository_path;
    bool m_publish_local_repository_info;
    bool m_publish_repository_info;
    uint64_t m_max_response_size;
    uint64_t m_max_local_ds_data_size;
    pbeast::DownsampleConfig m_cache_ds_config;
    pbeast::DataCompactionConfig m_merger_zip_config;
    std::filesystem::path m_eos_repository_directory;
    std::vector<std::string> m_downsample_files;
    uint32_t m_sleep_interval;
    uint64_t m_data_split_duration;
    uint64_t m_data_group_max_size;
    uint64_t m_series_info_max_size;
    uint32_t m_max_receiver_delay;
    bool m_disk_balance_enabled;
    double m_disk_balance_threshold;
    uint32_t m_cache_downsample_threshold;
  };


  class RepositoryServer : public RepositoryServerConfig, public IPCNamedObject<POA_pbeast::server>
  {

  public:

    RepositoryServer(const RepositoryServerConfig& config, IPCPartition& partition, const std::string& name);
    ~RepositoryServer();

    void
    shutdown();

    static RepositoryServer&
    instance()
    {
      return *s_instance;
    }

    repository_server::Monitoring * m_monitor;


    static pbeast::WriterLock&
    get_writer_lock()
    {
      return s_instance->m_db.get_writer_lock();
    }

    void
    init();


    // pbeast monitoring methods

    void
    start_monitoring_thread(unsigned int sleep_interval);

    void
    stop_monitoring();

    bool
    init_monitoring();

    bool
    needs_monitoring();

    void
    publish_monitoring_info();


    // pbeast cache directory cleaner

    void
    start_cache_cleaner_thread(unsigned int sleep_interval, uint64_t max_size);

    void
    start_series_info_cleaner_thread(unsigned int sleep_interval, uint64_t max_size);


    // pbeast server IDL methods

    void
    send_command(const char* command_name, const ::pbeast::StringArray& command_args, ::CORBA::String_out command_output);

    void
    get_partitions(StringArray_out partitions);
    void
    get_classes(const char* partition_name, StringArray_out classes);
    void
    get_attributes(const char* partition_name, const char* class_name, StringDataPointArrayList_out attributes);
    void
    get_attribute_info(const char* partition_name, const char* class_name, const char* attribute_name, S32DataPointList_out type, BooleanDataPointList_out is_array, StringDataPointList_out description);
    void
    get_attribute_type(const char* partition_name, const char* class_name, const char* attribute_name, AttributeTypeList_out info);
    void
    get_schema(const char* partition_name, ClassInfoList_out schema);

    void
    get_eovs(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out data);

    void
    get_updated_objects(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, const char* object_mask, pbeast::StringArray_out objects);

    void
    get_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanObjectList_out data);
    void
    get_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ObjectList_out data);
    void
    get_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ObjectList_out data);
    void
    get_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ObjectList_out data);
    void
    get_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ObjectList_out data);
    void
    get_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ObjectList_out data);
    void
    get_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ObjectList_out data);
    void
    get_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ObjectList_out data);
    void
    get_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ObjectList_out data);
    void
    get_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatObjectList_out data);
    void
    get_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleObjectList_out data);
    void
    get_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringObjectList_out data);
    void
    get_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanArrayObjectList_out data);
    void
    get_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ArrayObjectList_out data);
    void
    get_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ArrayObjectList_out data);
    void
    get_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ArrayObjectList_out data);
    void
    get_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ArrayObjectList_out data);
    void
    get_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ArrayObjectList_out data);
    void
    get_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ArrayObjectList_out data);
    void
    get_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ArrayObjectList_out data);
    void
    get_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ArrayObjectList_out data);
    void
    get_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatArrayObjectList_out data);
    void
    get_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleArrayObjectList_out data);
    void
    get_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringArrayObjectList_out data);

    void
    get_downsample_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleObjectList_out data);
    void
    get_downsample_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S8DownSampleObjectList_out data);
    void
    get_downsample_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U8DownSampleObjectList_out data);
    void
    get_downsample_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S16DownSampleObjectList_out data);
    void
    get_downsample_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U16DownSampleObjectList_out data);
    void
    get_downsample_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S32DownSampleObjectList_out data);
    void
    get_downsample_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U32DownSampleObjectList_out data);
    void
    get_downsample_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S64DownSampleObjectList_out data);
    void
    get_downsample_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U64DownSampleObjectList_out data);
    void
    get_downsample_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleObjectList_out data);
    void
    get_downsample_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleObjectList_out data);
    void
    get_downsample_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::StringDownSampleObjectList_out data);
    void
    get_downsample_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleArrayObjectList_out data);
    void
    get_downsample_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S8DownSampleArrayObjectList_out data);
    void
    get_downsample_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U8DownSampleArrayObjectList_out data);
    void
    get_downsample_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S16DownSampleArrayObjectList_out data);
    void
    get_downsample_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U16DownSampleArrayObjectList_out data);
    void
    get_downsample_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S32DownSampleArrayObjectList_out data);
    void
    get_downsample_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U32DownSampleArrayObjectList_out data);
    void
    get_downsample_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S64DownSampleArrayObjectList_out data);
    void
    get_downsample_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U64DownSampleArrayObjectList_out data);
    void
    get_downsample_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleArrayObjectList_out data);
    void
    get_downsample_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleArrayObjectList_out data);
    void
    get_downsample_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::StringDownSampleArrayObjectList_out data);


    static void
    prepare_out_directory(const std::filesystem::path& file);

    std::filesystem::path
    get_eos_link(const std::string& file_name) const;

    void
    create_eos_link(const std::string& file_name);

    void
    downsample_file(const std::filesystem::path& file);

  private:

    IPCPartition& m_partition;
    pbeast::Repository m_db;

    std::mutex m_mutex;

    static RepositoryServer * s_instance;


  private:

    std::mutex m_mon_thread_mutex;

    IPCAlarm * m_merger_alarm;
    bool merger_action();

    const std::string m_host_name;

    std::set<std::string> m_processed;
    std::set<std::string> m_bad;
    std::set<std::string> m_problematic;

    uint32_t m_count;

    std::set<std::string> m_bad_merged;
    uint32_t m_max_idx;

    std::map<std::string, boost::uintmax_t> m_meta_file_sizes;

    uint32_t m_last_max_idx;

    void
    process_directories(const std::filesystem::path& path, const std::filesystem::path& ignore1, const std::filesystem::path& ignore2, std::map<std::string, std::set<uint64_t> >& eov);

    void
    process_files(const std::filesystem::path& path, std::map<std::string, std::set<uint64_t> >& eov, pbeast::merger::FileType file_type);

    void
    read_eov(const std::filesystem::path& path, std::map<std::string, std::set<uint64_t> >& eov);

    void
    merge_meta_data(const std::filesystem::path& file_from, const std::filesystem::path& directory_to);

    void
    balance_distibuted_repositories();

    void
    search_class_directory(const std::filesystem::path& dir, std::map<uint64_t,std::set<pbeast::merger::File>>& info);


  private:

    class MonCall
    {
    protected:
      timespec m_start;
      CallStats m_call_stat;

    public:

      MonCall(uint64_t max_message_size/*, uint64_t max_local_ds_data_size = 0*/) : m_call_stat(max_message_size/*, max_local_ds_data_size*/)
      {
        clock_gettime(CLOCK_MONOTONIC, &m_start);
      }

      ~MonCall()
      {
        s_instance->m_monitor->add_any(m_call_stat);
      }

      CallStats&
      stat()
      {
        return m_call_stat;
      }

      uint64_t
      get_interval()
      {
        timespec end;
        clock_gettime(CLOCK_MONOTONIC, &end);
        return ((end.tv_sec - m_start.tv_sec) * 1000000000 + end.tv_nsec - m_start.tv_nsec);
      }
    };

    class MonRawCall : public MonCall
    {

    public:
      MonRawCall(uint64_t max_message_size, bool used_for_ds = false) :
          MonCall(max_message_size), m_used_for_ds(used_for_ds)
      {
        ;
      }

      ~MonRawCall()
      {
        if (m_call_stat.m_data_point_num)
          s_instance->m_monitor->add_raw((m_used_for_ds ? 0 : get_interval()), m_call_stat.m_objects_num, m_call_stat.m_data_point_num, m_call_stat.m_data_size);
      }

      bool m_used_for_ds;
    };

    class MonDsCall : public MonCall
    {

    public:
       MonDsCall(uint64_t max_message_size) :
          MonCall(max_message_size)
      {
        ;
      }

      ~MonDsCall()
      {
        s_instance->m_monitor->add_ds(get_interval(), m_call_stat.m_objects_num, m_call_stat.m_data_point_num, m_call_stat.m_data_size);
      }
    };

  };
}

#endif
