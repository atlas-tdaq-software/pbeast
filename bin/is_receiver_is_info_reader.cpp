#include <boost/date_time/posix_time/time_formatters.hpp>
#include "boost/date_time/c_local_time_adjustor.hpp"

#include <is/criteria.h>
#include <is/infoany.h>
#include <is/inforeceiver.h>

#include "is_receiver_is_info_reader.h"
#include "is_receiver_data_provider.h"
#include "is_receiver_class.h"
#include "receiver_errors.h"


pbeast::receiver::IS_InfoReader::IS_InfoReader(
    IS_DataProvider& dp,
    const std::string& server,
    bool read_at_subscribe,
    ISCriteria::Logic logic,
    const std::string& info_name,
    const std::string& type,
    bool subtypes,
    bool exclude
) :
    m_data_provider(dp), m_criteria(nullptr), m_server_name(server), m_start_time(0), m_read_at_subscribe(read_at_subscribe)
{
  if (!type.empty())
    {
      ISType is_type;

      if (IS_Class *c = static_cast<IS_Class*>(dp.get_class(type)))
        is_type = c->get_type();
      else
        throw daq::pbeast::NoInfoType(ERS_HERE, type);

      if (subtypes)
        is_type = ~is_type;

      if (exclude)
        is_type = !is_type;

      if (info_name.empty())
        m_criteria = new ISCriteria(".*", is_type);
      else
        m_criteria = new ISCriteria(info_name, is_type, logic);
    }
  else if (info_name.empty())
    {
      m_criteria = new ISCriteria(".*");
    }
  else
    {
      m_criteria = new ISCriteria(info_name);
    }
}

pbeast::receiver::IS_InfoReader::~IS_InfoReader()
{
  if (m_start_time != 0)
    try
      {
        ERS_LOG("unsubscribe " << m_server_name);
        m_data_provider.get_receiver().unsubscribe(m_server_name, *m_criteria, true);
      }
    catch (daq::is::SubscriptionNotFound &ex)
      {
        ers::error(ex);
      }
    catch (daq::is::RepositoryNotFound &ex)
      {
        ers::warning(ex);
      }

  if (m_criteria)
    delete m_criteria;
}


void
pbeast::receiver::IS_InfoReader::report_bad_class(const std::string& class_name, const std::string& object_id, const std::string& provider, const char * action, const char * op)
{
  static std::set<std::string> s_reported;
  static std::mutex s_mutex;

  std::lock_guard<std::mutex> lock(s_mutex);

  if (s_reported.insert(class_name).second == true)
    ers::error(daq::pbeast::CannotFindDescriptionOfClass(ERS_HERE, action, class_name, provider, op, object_id));
}

static std::string
to_local_time(const OWLTime & owl_tm)
{
  typedef boost::date_time::c_local_adjustor<boost::posix_time::ptime> local_adj;

  boost::posix_time::ptime tm = local_adj::utc_to_local(boost::posix_time::from_time_t(owl_tm.c_time())) + boost::posix_time::microseconds(owl_tm.mksec());
  return boost::posix_time::to_simple_string(tm);
}

pbeast::receiver::Callback::Callback(pbeast::receiver::IS_DataProvider &dp) :
    m_data_provider(dp), m_small_delay(m_data_provider.get_config().get_small_data_delay()), m_big_delay(m_data_provider.get_config().get_big_data_delay())
{
  ;
}

void
pbeast::receiver::Callback::callback(ISCallbackInfo * isc)
{
  try
    {
      ISInfoAny isa;
      isc->value(isa);

      // sanitisation of IS data

      time_t now = time(0);
      time_t cb_time = isa.time().c_time();

      if (__builtin_expect((cb_time > now), 0))
        {
          if (cb_time > (now + 1))
            ers::warning(daq::pbeast::DataTimestampInFutureError( ERS_HERE, to_local_time(isa.time()), isc->name(), isa.type().name(), isa.provider()));
        }
      else if (__builtin_expect((isc->reason() != ISInfo::Deleted), 1))
        {
          const uint32_t delta(now - cb_time);

          if (__builtin_expect((delta > m_small_delay), 0))
            {
              daq::pbeast::SkipDataDelayedTimestampError issue(ERS_HERE, to_local_time(isa.time()), isc->name(), isa.type().name(), isa.provider(), (delta > m_big_delay ? m_big_delay : m_small_delay));

              if (delta > m_big_delay)
                {
                  if (isc->reason() != ISInfo::Subscribed)
                    ers::error(issue);
                  else
                    ers::debug(issue, 1);

                  return;
                }
              else
                {
                  if (isc->reason() != ISInfo::Subscribed)
                    ers::warning(issue);
                  else
                    ers::debug(issue, 1);
                }
            }
        }

      if (pbeast::receiver::IS_Class * c = static_cast<IS_Class *>(m_data_provider.get_class(isa.type().name())))
        {
          if (c->is_incomplete_schema() == false)
            c->update_object(isc->name(), isa, isc->reason(), nullptr);
          else
            pbeast::receiver::IS_InfoReader::report_bad_class(isa.type().name(), isc->name(), isa.provider(), "use incomplete", "process callback of");
        }
      else
        {
          pbeast::receiver::IS_InfoReader::report_bad_class(isa.type().name(), isc->name(), isa.provider(), "find", "process callback of");
        }
    }
  catch (ers::Issue& ex)
    {
      ers::error(ex);
    }
}


void
pbeast::receiver::IS_InfoReader::subscribe(bool read_all_when_subscribe)
{
  // try to subscribe

  try
    {
      if (read_all_when_subscribe == false)
        {
          ERS_LOG("subscribe " << m_server_name << " with \'" << *m_criteria << '\'');
          m_data_provider.get_receiver().subscribe(m_server_name, *m_criteria, &Callback::callback, m_data_provider.get_callback());
        }
      else
        {
          ERS_LOG("subscribe " << m_server_name << " with \'" << *m_criteria << "\' and read existing data");
          m_data_provider.get_receiver().subscribe(m_server_name, *m_criteria, &Callback::callback, m_data_provider.get_callback(), {ISInfo::Created, ISInfo::Updated, ISInfo::Deleted, ISInfo::Subscribed});
        }
    }
  catch (daq::is::RepositoryNotFound& ex)
    {
      ers::error(ex);
      return;
    }
  catch (ers::Issue& ex)
    {
      ers::error(ex);
      return;
    }

  m_start_time = time(0);
}
