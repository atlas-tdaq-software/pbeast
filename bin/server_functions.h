#ifndef PBEAST_SERVER_FUNCTIONS_H
#define PBEAST_SERVER_FUNCTIONS_H

#include <algorithm>
#include <chrono>
#include <iterator>

#include <stdlib.h>

#include <pbeast/downsample-data.h>
#include "pbeast/pbeast.hh"

namespace pbeast
{
  constexpr float verbose_report_duration_threshold_const = 5000.; // add to report any action taking more than 5 seconds

  float
  get_interval(const std::chrono::time_point<std::chrono::steady_clock>& t1);
}

template<typename T>
  pbeast::DownsampledSeriesData<T>
  calculate_average(const std::set<pbeast::SeriesData<T>>& data, uint64_t since, uint64_t until)
  {
    pbeast::DownsampleData<T> val(1);
    val.set_ts(since, until - since);

    val.simple_reset((*data.cbegin()).get_value());

    for (auto& x : data)
      val.add(x.get_value(), x.get_ts_created());

    return val.get_average();
  }

template<typename T>
  pbeast::DownsampledSeriesVectorData<T>
  calculate_average(const std::set<pbeast::SeriesVectorData<T>>& data, uint64_t since, uint64_t until)
  {
    pbeast::DownsampleVectorData<T> val(1);
    val.set_ts(since, until - since);

    val.simple_reset((*data.cbegin()).get_value());

    for (auto& x : data)
      val.add(x.get_value(), x.get_ts_created());

    return val.get_average();
  }

template<typename T>
  pbeast::DownsampledSeriesData<T>
  calculate_average(const std::set<pbeast::DownsampledSeriesData<T>>& data, uint64_t since, uint64_t until)
  {
    pbeast::DownsampleData<T> val(1);
    val.set_ts(since, until - since);

    val.simple_reset((*data.cbegin()).data());

    for (auto& x : data)
      val.add(x.data().get_value(), pbeast::mk_ts(x.get_ts()));
    return val.get_average();
  }

template<typename T>
  pbeast::DownsampledSeriesVectorData<T>
  calculate_average(const std::set<pbeast::DownsampledSeriesVectorData<T>>& data, uint64_t since, uint64_t until)
  {
    pbeast::DownsampleVectorData<T> val(1);
    val.set_ts(since, until - since);

    val.simple_reset((*data.cbegin()).data());

    for (auto& x : data)
      val.add(x.data(), pbeast::mk_ts(x.get_ts()));

    return val.get_average();
  }

template<typename T>
  pbeast::SeriesData<T>
  get_current(const std::set<pbeast::SeriesData<T>>& data, uint64_t until, uint64_t /*interval*/)
  {
    for (auto it = data.rbegin(); it != data.rend(); ++it)
      {
        if ((*it).get_ts_created() <= until)
          return *it;
      }

    return *data.rbegin();
  }

template<typename T>
  pbeast::SeriesVectorData<T>
  get_current(const std::set<pbeast::SeriesVectorData<T>>& data, uint64_t until, uint32_t /*interval*/)
  {
    for (auto it = data.rbegin(); it != data.rend(); ++it)
      {
        if ((*it).get_ts_created() <= until)
          return *it;
      }

    return *data.rbegin();
  }

template<typename T>
  pbeast::DownsampledSeriesData<T>
  get_current(const std::set<pbeast::DownsampledSeriesData<T>>& data, uint64_t until64, uint32_t interval)
  {
    uint32_t until = pbeast::mk_downsample_ts(pbeast::ts2secs(until64), interval) + interval;

    for (auto it = data.rbegin(); it != data.rend(); ++it)
      {
        if ((*it).get_ts() <= until)
          return *it;
      }

    return *data.rbegin();
  }

template<typename T>
  pbeast::DownsampledSeriesVectorData<T>
  get_current(const std::set<pbeast::DownsampledSeriesVectorData<T>>& data, uint64_t until64, uint32_t interval)
  {
    uint32_t until = pbeast::mk_downsample_ts(pbeast::ts2secs(until64), interval) + interval;

    for (auto it = data.rbegin(); it != data.rend(); ++it)
      {
        if ((*it).get_ts() <= until)
          return *it;
      }

    return *data.rbegin();
  }

template<typename T>
  pbeast::SeriesData<T>
  get_max(const std::set<pbeast::SeriesData<T>>& data, uint64_t until, uint32_t /*interval*/)
  {
    const pbeast::SeriesData<T> * p = nullptr;

    for (const auto& x : data)
      {
        if (!p || (x.get_ts_created() <= until && x.get_value() > p->get_value()))
          p = &x;
      }

    return *p;
  }

template<typename T>
  pbeast::SeriesVectorData<T>
  get_max(const std::set<pbeast::SeriesVectorData<T>>& data, uint64_t until, uint32_t /*interval*/)
  {
    const pbeast::SeriesVectorData<T> * p = nullptr;
    T last;

    for (const auto& x : data)
      {
        if(!p)
          {
            p = &x;
            last = *std::max_element(std::begin(x.get_value()), std::end(x.get_value()));
          }
        else if(x.get_ts_created() <= until)
          {
            T cur = *std::max_element(std::begin(x.get_value()), std::end(x.get_value()));
            if(cur > last)
              {
                last = cur;
                p = &x;
              }
          }
      }

    return *p;
  }

template<typename T>
  pbeast::DownsampledSeriesData<T>
  get_max(const std::set<pbeast::DownsampledSeriesData<T>>& data, uint64_t until64, uint32_t interval)
  {
    uint32_t until = pbeast::mk_downsample_ts(pbeast::ts2secs(until64), interval) + interval;

    const pbeast::DownsampledSeriesData<T> * p = nullptr;

    for (const auto& x : data)
      {
        if (!p || (x.get_ts() <= until && x.data().get_max_value() > p->data().get_max_value()))
          p = &x;
      }

    return *p;
  }


template<typename T>
  const T
  get_max_value(const pbeast::DownsampledSeriesVectorData<T>& v)
  {
    const T * p = nullptr;

    for (const auto& x : v.data())
      {
        if (!p)
          p = &x.get_max_value();
        else if (x.get_max_value() > *p)
          p = &x.get_max_value();
      }

    return *p;
  }

template<typename T>
  pbeast::DownsampledSeriesVectorData<T>
  get_max(const std::set<pbeast::DownsampledSeriesVectorData<T>>& data, uint64_t until64, uint32_t interval)
  {
    uint32_t until = pbeast::mk_downsample_ts(pbeast::ts2secs(until64), interval) + interval;
    T last;

    const pbeast::DownsampledSeriesVectorData<T> * p = nullptr;

    for (const auto& x : data)
      {
        if(!p)
          {
            p = &x;
            last = get_max_value<T>(x);
          }
        else if(x.get_ts() <= until)
          {
            T cur = get_max_value<T>(x);
            if(cur > last)
              {
                last = cur;
                p = &x;
              }
          }
      }

    return *p;
  }


template<typename T>
  struct ComparableValueParameter
  {
    ComparableValueParameter(const std::string& value)
    {
      m_value = pbeast::utils::str2double(value);
    }

    template<typename K>
      bool
      compare(const pbeast::SeriesData<K>& value)
      {
        return (value.get_value() > m_value);
      }

    template<typename K>
      bool
      compare(const pbeast::SeriesVectorData<K>& value)
      {
        for (const auto& x : value.get_value())
          if (x > m_value)
            return true;

        return false;
      }

    template<typename K>
      bool
      compare(const pbeast::DownsampledSeriesData<K>& value)
      {
        return (value.data().m_value > m_value);
      }

    template<typename K>
      bool
      compare(const pbeast::DownsampledSeriesVectorData<K>& value)
      {
        for (const auto& x : value.data())
          if (x.m_value > m_value)
            return true;

        return false;
      }

    typename pbeast::AverageType<T>::average_t m_value;
  };

template<>
  ComparableValueParameter<std::string>::ComparableValueParameter(const std::string& value)
  {
    m_value = value;
  }


template<typename T, typename K>
  void
  select_average_above(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& eov_data, uint64_t since, uint64_t until)
  {
    ComparableValueParameter<typename T::basic_type> v(value);

    for (auto i = data.begin(); i != data.end();)
      {
        if (v.compare(calculate_average(i->second, since, until)))
          {
            ++i;
          }
        else
          {
            eov_data.erase(i->first);
            i = data.erase(i);
          }
      }
  }

template<typename T, typename K>
  void
  select_average_below(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& eov_data, uint64_t since, uint64_t until)
  {
    ComparableValueParameter<typename T::basic_type> v(value);

    for (auto i = data.begin(); i != data.end();)
      {
        if (v.compare(calculate_average(i->second, since, until)))
          {
            eov_data.erase(i->first);
            i = data.erase(i);
          }
        else
          {
            ++i;
          }
      }
  }

template<typename T, typename K>
  void
  select_current_above(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& eov_data, uint64_t until, uint32_t interval)
  {
    ComparableValueParameter<typename T::basic_type> v(value);

    for (auto i = data.begin(); i != data.end();)
      {
        if (v.compare(get_current(i->second, until, interval)))
          {
            ++i;
          }
        else
          {
            eov_data.erase(i->first);
            i = data.erase(i);
          }
      }
  }

template<typename T, typename K>
  void
  select_current_below(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& eov_data, uint64_t until, uint32_t interval)
  {
    ComparableValueParameter<typename T::basic_type> v(value);

    for (auto i = data.begin(); i != data.end();)
      {
        if (v.compare(get_current(i->second, until, interval)))
          {
            eov_data.erase(i->first);
            i = data.erase(i);
          }
        else
          {
            ++i;
          }
      }
  }


template<typename T>
  struct SortableValueParameter
  {

    template<typename K>
      SortableValueParameter(const pbeast::SeriesData<K>& value)
      {
        m_value = value.get_value();
      }

    template<typename K>
      SortableValueParameter(const pbeast::SeriesVectorData<K>& value)
      {
        bool is_first(true);

        for (const auto& x : value.get_value())
          {
            if (is_first)
              {
                m_value = x;
                is_first = false;
              }
            else
              {
                if (x > m_value)
                  m_value = x;
              }
          }
      }

    template<typename K>
      SortableValueParameter(const pbeast::DownsampledSeriesData<K>& v)
      {
        m_value = v.data().get_value();
      }

    template<typename K>
      SortableValueParameter(const pbeast::DownsampledSeriesVectorData<K>& v)
      {
        bool is_first(true);

        for (const auto& x : v.data())
          {
            if (is_first)
              {
                m_value = x.get_value();
                is_first = false;
              }
            else
              {
                if (x.get_value() > m_value)
                  m_value = x.get_value();
              }
          }
      }

    bool
    operator<(const SortableValueParameter& d) const
    {
      return (m_value < d.m_value);
    }

    typename pbeast::AverageType<typename T::basic_type>::average_t m_value;
  };

template<typename T>
void
put_lowest_elements(unsigned long num, std::map<std::string, std::set<T>>& data, std::map<SortableValueParameter<T>, std::pair<std::string, std::set<T>>>& tmp)
{
  data.clear();

  for (const auto& x : tmp)
    {
      if (num-- > 0)
        data.emplace(x.second);
      else
        break;
    }
}

template<typename T>
void
put_highest_elements(unsigned long num, std::map<std::string, std::set<T>>& data, std::map<SortableValueParameter<T>, std::pair<std::string, std::set<T>>>& tmp)
{
  data.clear();

  for (auto x = tmp.rbegin(); x != tmp.rend(); ++x)
    {
      if (num-- > 0)
        data.emplace(x->second);
      else
        break;
    }
}


template<typename T, typename K>
  void
  select_highest_average(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& eov_data, uint64_t since, uint64_t until)
  {
    unsigned long param = pbeast::utils::str2uint(value);

    if (data.size() < param)
      return;

    std::map<SortableValueParameter<T>, std::pair<std::string, std::set<T>>> tmp;

    for (auto& x : data)
      tmp.emplace(SortableValueParameter<T>(calculate_average(x.second, since, until)), x);

    put_highest_elements(param, data, tmp);
  }

template<typename T, typename K>
  void
  select_lowest_average(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& eov_data, uint64_t since, uint64_t until)
  {
    unsigned long param = pbeast::utils::str2uint(value);

    if (data.size() < param)
      return;

    std::map<SortableValueParameter<T>, std::pair<std::string, std::set<T>>> tmp;

    for (auto& x : data)
      tmp.emplace(SortableValueParameter<T>(calculate_average(x.second, since, until)), x);

    put_lowest_elements(param, data, tmp);
  }

template<typename T, typename K>
  void
  select_highest_current(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& eov_data, uint64_t until, uint32_t interval)
  {
    unsigned long param = pbeast::utils::str2uint(value);

    if (data.size() < param)
      return;

    std::map<SortableValueParameter<T>, std::pair<std::string, std::set<T>>>tmp;

    for (auto& x : data)
      tmp.emplace(SortableValueParameter<T>(get_current(x.second, until, interval)), x);

    put_highest_elements(param, data, tmp);
  }

template<typename T, typename K>
  void
  select_lowest_current(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& eov_data, uint64_t until, uint32_t interval)
  {
    unsigned long param = pbeast::utils::str2uint(value);

    if (data.size() < param)
      return;

    std::map<SortableValueParameter<T>, std::pair<std::string, std::set<T>>>tmp;

    for (auto& x : data)
      tmp.emplace(SortableValueParameter<T>(get_current(x.second, until, interval)), x);

    put_lowest_elements(param, data, tmp);
  }


template<typename T, typename K>
  void
  select_highest_max(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& eov_data, uint64_t until, uint32_t interval)
  {
    unsigned long param = pbeast::utils::str2uint(value);

    if (data.size() < param)
      return;

    std::map<SortableValueParameter<T>, std::pair<std::string, std::set<T>>>tmp;

    for (auto& x : data)
      if (x.second.empty() == false)
        tmp.emplace(SortableValueParameter<T>(get_max(x.second, until, interval)), x);

    put_highest_elements(param, data, tmp);
  }


template<typename T>
  void
  sum_array(std::map<std::string, std::set<T>>& data)
  {
    for (auto& x : data)
      for (auto& y : x.second)
        const_cast<T&>(y).sum_array_elements();
  }

inline uint64_t
get_next(uint64_t cur, uint64_t dt)
{
  uint64_t num = cur / dt + 1;
  return (dt * num);
}


template<typename T>
  void
  summarize(uint64_t dt, std::set<pbeast::SeriesData<T>>& data, std::set<uint64_t>&upd, uint64_t since, uint64_t until)
  {
    auto data_it = data.begin();

    // skip all data before "since"
    while (data_it != data.end() && (*data_it).get_ts_last_updated() < since)
      data_it++;

    // empty result
    if (data_it == data.end())
      {
        data.clear();
        return;
      }

    std::set<pbeast::SeriesData<T>> result;
    std::set<uint64_t> new_upd;

    auto upd_it = upd.begin();
    uint64_t udt_next = (upd_it != upd.end()) ? *upd_it : 0;

    uint64_t cur_ts = since;

    uint64_t first_ts = (*data.begin()).get_ts_created();
    uint64_t last_ts = (*data.rbegin()).get_ts_last_updated();

    if (cur_ts < first_ts)
      cur_ts = first_ts;

    if(last_ts > until)
      last_ts = until;

    uint64_t next_ts = get_next(cur_ts, dt);

    while (next_ts <= last_ts && data_it != data.end())
      {
        T v{};

        while (true)
          {
            const uint64_t created_ts((*data_it).get_ts_created());
            const uint64_t last_updated_ts((*data_it).get_ts_last_updated());

            while (upd_it != upd.end() && udt_next < created_ts)
              {
                upd_it++;
                udt_next = (upd_it != upd.end()) ? *upd_it : 0;
              }

            uint64_t stop_ts = std::min(last_updated_ts, next_ts);

            while (upd_it != upd.end() && udt_next <= stop_ts)
              {
                v += (*data_it).get_value();
                upd_it++;
                udt_next = (upd_it != upd.end()) ? *upd_it : 0;
              }

            if(last_updated_ts >= next_ts)
              {
                break;
              }
            else
              {
                data_it++;
                if (data_it == data.end())
                  break;
              }
          }

        result.emplace(cur_ts, next_ts-1, v);
        new_upd.insert(cur_ts);
        new_upd.insert(next_ts-1);
        cur_ts = next_ts;
        next_ts = get_next(cur_ts, dt);
      }

    data.swap(result);
    upd.swap(new_upd);
}

template<typename T>
  void
  summarize(uint64_t dt, std::set<pbeast::SeriesVectorData<T>>& data, std::set<uint64_t>&upd, uint64_t since, uint64_t until)
{
  auto data_it = data.begin();

  // skip all data before "since"
  while (data_it != data.end() && (*data_it).get_ts_last_updated() < since)
    data_it++;

  // empty result
  if (data_it == data.end())
    {
      data.clear();
      return;
    }

  std::set<pbeast::SeriesVectorData<T>> result;
  std::set<uint64_t> new_upd;

  auto upd_it = upd.begin();
  uint64_t udt_next = (upd_it != upd.end()) ? *upd_it : 0;

  uint64_t cur_ts = since;

  uint64_t first_ts = (*data.begin()).get_ts_created();
  uint64_t last_ts = (*data.rbegin()).get_ts_last_updated();

  if (cur_ts < first_ts)
    cur_ts = first_ts;

  if(last_ts > until)
    last_ts = until;

  uint64_t next_ts = get_next(cur_ts, dt);

  while (next_ts <= last_ts && data_it != data.end())
    {
      std::vector<T> v;

      while (true)
        {
          const uint64_t created_ts((*data_it).get_ts_created());
          const uint64_t last_updated_ts((*data_it).get_ts_last_updated());

          while (upd_it != upd.end() && udt_next < created_ts)
            {
              upd_it++;
              udt_next = (upd_it != upd.end()) ? *upd_it : 0;
            }

          uint64_t stop_ts = std::min(last_updated_ts, next_ts);

          while (upd_it != upd.end() && udt_next <= stop_ts)
            {
              const unsigned int vec_len(v.size());
              const unsigned int val_len((*data_it).get_value().size());
              const unsigned int len = std::min(vec_len, val_len);

              for(unsigned int i = 0; i < len; ++i)
                {
                  v[i] = v[i] + (*data_it).get_value()[i];
                }

              for(unsigned int i = len; i < val_len; ++i)
                {
                  v.emplace_back((*data_it).get_value()[i]);
                }

              upd_it++;
              udt_next = (upd_it != upd.end()) ? *upd_it : 0;
            }

          if(last_updated_ts >= next_ts)
            {
              break;
            }
          else
            {
              data_it++;
              if (data_it == data.end())
                break;
            }
        }

      result.emplace(cur_ts, next_ts-1, v);
      new_upd.insert(cur_ts);
      new_upd.insert(next_ts-1);
      cur_ts = next_ts;
      next_ts = get_next(cur_ts, dt);
    }

  data.swap(result);
  upd.swap(new_upd);
}

template<typename T>
  void
  summarize(uint64_t dt, std::set<pbeast::DownsampledSeriesData<T>>& data, std::set<uint32_t>&upd, uint64_t since, uint64_t until)
{
  throw std::runtime_error("downsampled data cannot be summarized; apply summarize() function on raw data");
}

template<typename T>
  void
  summarize(uint64_t dt, std::set<pbeast::DownsampledSeriesVectorData<T>>& data, std::set<uint32_t>&upd, uint64_t since, uint64_t until)
{
  throw std::runtime_error("downsampled data cannot be summarized; apply summarize() function on raw data");
}


template<typename T, typename K>
  void
  summarize(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& upd_data, uint64_t since, uint64_t until, uint32_t interval)
  {
    uint32_t param32 = value.empty() ? 0 : pbeast::utils::str2uint(value);

    if(param32 == 0)
      param32 = interval;

    const uint64_t param = pbeast::mk_ts(param32);

    if (upd_data.empty() == true)
      throw std::runtime_error("data without update info cannot be summarized; enable all-updates option");

    for (auto x = data.begin(); x != data.end();)
      {
        if (x->second.empty() == false)
          {
            std::set<K> dummy;
            auto upd_it = pbeast::find(upd_data,x->first);
            std::set<K>& upd = (upd_it == upd_data.end() ? dummy : upd_it->second);
            summarize(param, x->second, upd, since, until);

            if (upd.empty() && upd_it != upd_data.end())
              upd_data.erase(upd_it);
          }

        if (x->second.empty())
          x = data.erase(x);
        else
          x++;
      }
  }


static void
remove_void(std::map<std::string, std::set<uint64_t>>& data, const std::string& name, const pbeast::SeriesDataBase * value)
{
  auto upd_it = pbeast::find(data,name);

  if (upd_it != data.end())
    {
      auto first = upd_it->second.find(value->get_ts_created());

      if (first != upd_it->second.end())
        {
          auto last = upd_it->second.find(value->get_ts_last_updated());
          upd_it->second.erase(first, last);

          if (upd_it->second.empty())
            data.erase(upd_it);
        }
    }
}

static void
remove_void(std::map<std::string, std::set<uint32_t>>& data, const std::string& name, const pbeast::DownsampledSeriesDataBase * value)
{
  auto upd_it = pbeast::find(data,name);

  if (upd_it != data.end())
    {
      auto first = upd_it->second.find(value->get_ts());

      if (first != upd_it->second.end())
        {
          upd_it->second.erase(first);

          if (upd_it->second.empty())
            data.erase(upd_it);
        }
    }
}


template<typename T, typename K>
  void
  remove_above_data(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& upd_data)
  {
    std::istringstream s(value);
    typename T::basic_type val;
    s >> val;

    for (auto i = data.begin(); i != data.end();)
      {
        for (auto x = i->second.begin(); x != i->second.end();)
          {
            if ((*x).is_greater(val))
              {
                remove_void(upd_data, i->first, &*x);
                x = i->second.erase(x);
              }
            else
              {
                x++;
              }
          }

        if (i->second.empty())
          i = data.erase(i);
        else
          i++;
      }
  }

template<typename T, typename K>
  void
  remove_below_data(const std::string& value, std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& upd_data)
  {
    std::istringstream s(value);
    typename T::basic_type val;
    s >> val;

    for (auto i = data.begin(); i != data.end();)
      {
        for (auto x = i->second.begin(); x != i->second.end();)
          {
            if ((*x).is_less(val))
              {
                remove_void(upd_data, i->first, &*x);
                x = i->second.erase(x);
              }
            else
              {
                x++;
              }
          }

        if (i->second.empty())
          i = data.erase(i);
        else
          i++;
      }
  }

template<typename T, typename K>
  void
  remove_corrupted(std::map<std::string, std::set<T>>& data, std::map<std::string, std::set<K>>& upd_data)
  {
    for (auto i = data.begin(); i != data.end();)
      {
        for (auto x = i->second.begin(); x != i->second.end();)
          {
            if ((*x).is_corrupted())
              {
                remove_void(upd_data, i->first, &*x);
                x = i->second.erase(x);
              }
            else
              {
                x++;
              }
          }

        if (i->second.empty())
          i = data.erase(i);
        else
          i++;
      }
  }

static std::string
mk_verbose_report(const pbeast::FunctionConfig& fc, float duration, uint64_t size)
{
  std::ostringstream text;

  text << "apply " << fc.m_data_names.size() + fc.m_aliases.size() + fc.m_data.size() << " functions on " << size << " data series in " << duration << " ms";

  return text.str();
}

namespace pbeast
{
  template<typename T, typename K>
    void
    apply_functions(std::map<std::string,std::set<T>>& data, std::map<std::string,std::set<K>>& eov_data, std::map<std::string,std::set<K>>& upd_data, const pbeast::FunctionConfig& fc, uint64_t since, uint64_t until, uint32_t interval, std::string& verbose_out)
    {
      if (fc.is_null())
        return;

      auto tm = std::chrono::steady_clock::now();

      // apply alias functions
      if (fc.m_aliases.empty() == false)
        {
          std::set<std::string> names;

          for(const auto& x : data)
            names.insert(x.first);

          for(const auto& x : eov_data)
            names.insert(x.first);

          // consider special case with complex path attribute names: duplicate UPD to allow alias functions work
          for (const auto &x : data)
            {
              const std::string &obj_name = pbeast::get_object_name(x.first);
              auto it_object_name = upd_data.find(obj_name);
              auto it_full_name = upd_data.find(x.first);

              if (it_object_name != upd_data.end() && it_full_name == upd_data.end())
                upd_data[x.first] = it_object_name->second;
            }

          std::map<std::string, std::string> cvt_map;

          pbeast::utils::create_alias_map(fc, names, cvt_map);

          std::map<std::string,std::set<T>> new_data;

          for (auto& x : data)
            new_data[cvt_map[x.first]].swap(x.second);

          data.swap(new_data);

          pbeast::utils::cvt_names(eov_data, cvt_map);
          pbeast::utils::cvt_names(upd_data, cvt_map);
        }


      if (!fc.m_data.empty())
        {
          // remember known series before applying functions; clean eov of removed series
          std::set<std::string> known_series;
          for (const auto& x : data)
            known_series.insert(x.first);

          for (const auto& f : fc.m_data)
            {
              try
                {
                  switch (f.first)
                    {
                      case pbeast::SelectAverageAbove:
                        select_average_above(f.second, data, eov_data, since, until);
                        break;

                      case pbeast::SelectAverageBelow:
                        select_average_below(f.second, data, eov_data, since, until);
                        break;

                      case pbeast::SelectHighestAverage:
                        select_highest_average(f.second, data, eov_data, since, until);
                        break;

                      case pbeast::SelectLowestAverage:
                        select_lowest_average(f.second, data, eov_data, since, until);
                        break;

                      case pbeast::SelectCurrentAbove:
                        select_current_above(f.second, data, eov_data, until, interval);
                        break;

                      case pbeast::SelectCurrentBelow:
                        select_current_below(f.second, data, eov_data, until, interval);
                        break;

                      case pbeast::SelectHighestCurrent:
                        select_highest_current(f.second, data, eov_data, until, interval);
                        break;

                      case pbeast::SelectLowestCurrent:
                        select_lowest_current(f.second, data, eov_data, until, interval);
                        break;

                      case pbeast::SelectHighestMax:
                        select_highest_max(f.second, data, eov_data, until, interval);
                        break;

                      case pbeast::SumArray:
                        sum_array(data);
                        break;

                      case pbeast::Summarize:
                        summarize(f.second, data, upd_data, since, until, fc.m_interval);
                        break;

                      case pbeast::RemoveAboveValue:
                        remove_above_data(f.second, data, upd_data);
                        break;

                      case pbeast::RemoveBelowValue:
                        remove_below_data(f.second, data, upd_data);
                        break;

                      case pbeast::RemoveCorrupted:
                        remove_corrupted(data, upd_data);
                        break;
                    }
                }
              catch(const std::exception& ex)
                {
                  std::ostringstream text;
                  text << "function " << pbeast::FunctionConfig::function_name(f.first) << "(" << f.second << ") failed\n\twas caused by: " << ex.what();
                  throw pbeast::BadRequest(text.str().c_str());
                }
            }

          for (const auto& x : data)
            known_series.erase(x.first);

          if (known_series.empty() == false)
            {
              for (const auto& x : known_series)
                {
                  auto eov_it = pbeast::find(eov_data,x);
                  if (eov_it != eov_data.end())
                    eov_data.erase(eov_it);
                }
            }
        }

      auto duration = get_interval(tm);

      if (ers::debug_level() > 0 || duration > verbose_report_duration_threshold_const)
        pbeast::utils::add_verbose(verbose_out, mk_verbose_report(fc, duration, data.size()));
    }
}

#endif
