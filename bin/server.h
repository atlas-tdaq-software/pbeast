#ifndef PBEAST_SERVER_H
#define PBEAST_SERVER_H

#include <mutex>
#include <set>
#include <shared_mutex>
#include <string>
#include <string_view>

#include <libmicrohttpd/microhttpd.h>

#include <ipc/exceptions.h>
#include <ipc/namedobject.h>

#include <pbeast/downsample-fill-gaps.h>
#include <pbeast/exceptions.h>
#include <pbeast/pbeast.hh>
#include <pbeast/series-data.h>
#include <pbeast/thread_pool.h>

namespace daq
{
  /*! \class pbeast::CorbaError
   *  This issue is reported when there is CORBA communication problem
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CorbaError,
    Exception,
    "CORBA system exception \"" << exception << "\" has been raised calling method " << method,
    ERS_EMPTY,
    ((CORBA::SystemException *)exception )
    ((std::string)method)
  )
}

class Issues
{
public:

  void
  add(const std::string& method, const pbeast::Failure& ex)
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_failure.insert(make_text(method, "pbeast::Failure", ex.text));
  }

  void
  add(const std::string& method, const CORBA::SystemException& ex)
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_failure.insert(make_text(method, ex));
  }

  void
  add(const std::string& method, const pbeast::NotFound& ex)
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_not_found.insert(make_text(method, "pbeast::NotFound", ex.text));
  }

  void
  add(const std::string& method, const pbeast::BadRequest& ex)
  {
    std::lock_guard<std::mutex> lock(m_mutex);
    m_bad_request.insert(make_text(method, "pbeast::BadRequest", ex.text));
  }

  void
  validate(const char * name, bool empty_data, uint64_t size = 0) const;

private:

  static std::string
  make_text(const std::string& method_name, const char * what, const char * issue);

  static std::string
  make_text(const std::string& method_name, const CORBA::SystemException& issue);

  static std::string
  make_report(const char * method, const char * action, const std::set<std::string>& data);

  std::set<std::string> m_failure;
  std::set<std::string> m_bad_request;
  std::set<std::string> m_not_found;
  std::mutex m_mutex;
};


struct MultiDataFileResponseServer;


namespace pbeast
{
  namespace srv
  {
    class Monitoring;
    struct UserStat;

    struct AttributeInfo
    {
      AttributeInfo(const char * type, const char * description, bool is_array) :
        m_type(type),
        m_description(description),
        m_is_array(is_array)
      {
        ;
      }

      std::string m_type;
      std::string m_description;
      bool m_is_array;
    };

    struct ClassInfo
    {
      std::map<std::string, AttributeInfo> m_attributes;
      uint64_t m_ts;

      ClassInfo() :
          m_ts(0)
      {
        ;
      }
    };
  }

  struct AttrInfoData
  {
    uint64_t m_created, m_updated;
    std::string m_type;
    bool m_is_array;
    AttrInfoData(uint64_t created, uint64_t updated, const std::string& type, bool is_array) :
        m_created(created), m_updated(updated), m_type(type), m_is_array(is_array)
    {
      ;
    }
  };


  class Server : public IPCNamedObject<POA_pbeast::server>
  {

  public:

    Server(const std::string& name, const std::set<std::string>& initial_receivers, long pbeast_thread_pool_size, uint16_t http_port, unsigned int http_timeout, unsigned int http_thread_pool_size, uint32_t http_request_downsample_threshold);
    ~Server();

    void
    shutdown();

    void
    stop_services();

    bool
    is_http_running() const
    {
      return m_mhd != nullptr;
    }


    // pbeast server IDL methods

    void
    send_command(const char* command_name, const ::pbeast::StringArray& command_args, ::CORBA::String_out command_output);

    void
    get_partitions(StringArray_out partitions);
    void
    get_classes(const char* partition_name, StringArray_out classes);
    void
    get_attributes(const char* partition_name, const char* class_name, StringDataPointArrayList_out attributes);
    void
    get_attribute_info(const char* partition_name, const char* class_name, const char* attribute_name, S32DataPointList_out type, BooleanDataPointList_out is_array, StringDataPointList_out description);
    void
    get_attribute_type(const char* partition_name, const char* class_name, const char* attribute_name, AttributeTypeList_out info);

    void
    get_schema(const char* partition_name, ClassInfoList_out schema);

    void
    get_eovs(const char* partition_name, const char* class_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, pbeast::V64ObjectList_out data);

    void
    get_updated_objects(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, const char* object_mask, pbeast::StringArray_out objects);

    void
    get_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanObjectList_out data);
    void
    get_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ObjectList_out data);
    void
    get_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ObjectList_out data);
    void
    get_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ObjectList_out data);
    void
    get_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ObjectList_out data);
    void
    get_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ObjectList_out data);
    void
    get_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ObjectList_out data);
    void
    get_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ObjectList_out data);
    void
    get_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ObjectList_out data);
    void
    get_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatObjectList_out data);
    void
    get_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleObjectList_out data);
    void
    get_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringObjectList_out data);
    void
    get_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::BooleanArrayObjectList_out data);
    void
    get_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S8ArrayObjectList_out data);
    void
    get_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U8ArrayObjectList_out data);
    void
    get_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S16ArrayObjectList_out data);
    void
    get_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U16ArrayObjectList_out data);
    void
    get_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S32ArrayObjectList_out data);
    void
    get_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U32ArrayObjectList_out data);
    void
    get_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::S64ArrayObjectList_out data);
    void
    get_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::U64ArrayObjectList_out data);
    void
    get_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::FloatArrayObjectList_out data);
    void
    get_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::DoubleArrayObjectList_out data);
    void
    get_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, V64ObjectList_out eov, V64ObjectList_out upd, pbeast::StringArrayObjectList_out data);

    void
    get_downsample_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleObjectList_out data);
    void
    get_downsample_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S8DownSampleObjectList_out data);
    void
    get_downsample_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U8DownSampleObjectList_out data);
    void
    get_downsample_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S16DownSampleObjectList_out data);
    void
    get_downsample_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U16DownSampleObjectList_out data);
    void
    get_downsample_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S32DownSampleObjectList_out data);
    void
    get_downsample_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U32DownSampleObjectList_out data);
    void
    get_downsample_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S64DownSampleObjectList_out data);
    void
    get_downsample_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U64DownSampleObjectList_out data);
    void
    get_downsample_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleObjectList_out data);
    void
    get_downsample_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleObjectList_out data);
    void
    get_downsample_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::StringDownSampleObjectList_out data);
    void
    get_downsample_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::BooleanDownSampleArrayObjectList_out data);
    void
    get_downsample_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S8DownSampleArrayObjectList_out data);
    void
    get_downsample_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U8DownSampleArrayObjectList_out data);
    void
    get_downsample_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S16DownSampleArrayObjectList_out data);
    void
    get_downsample_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U16DownSampleArrayObjectList_out data);
    void
    get_downsample_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S32DownSampleArrayObjectList_out data);
    void
    get_downsample_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U32DownSampleArrayObjectList_out data);
    void
    get_downsample_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::S64DownSampleArrayObjectList_out data);
    void
    get_downsample_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::U64DownSampleArrayObjectList_out data);
    void
    get_downsample_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::FloatDownSampleArrayObjectList_out data);
    void
    get_downsample_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::DoubleDownSampleArrayObjectList_out data);
    void
    get_downsample_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, ::CORBA::Long fill_gaps, const char* object_mask, bool is_regexp, const char* select_functions, bool fill_upd, pbeast::V32ObjectList_out eov,  pbeast::V32ObjectList_out upd, pbeast::StringDownSampleArrayObjectList_out data);


    void
    http_get_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, const std::string& regex, uint64_t since, uint64_t until, uint64_t max_num, const std::string& function, const std::string& request, std::ostringstream& out);

    void
    http_autocomplete_series(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& request, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out);

    void
    http_get_partition_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& query, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out);

    void
    http_get_class_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& query, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out);

    void
    http_get_attribute_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& query, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out);

    void
    http_get_object_names(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, const std::string& query, uint64_t since, uint64_t until, uint64_t max_num, std::ostringstream& out);

    void
    http_get_schema(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, uint64_t prev_interval, std::ostringstream& out);

    void
    http_read_series(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& request, const std::string& plot_type, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, const std::string& show_all_updates_strs, uint64_t max_num, uint32_t sample_interval, const std::string& fill_gaps, const std::string& read_raw_data_strs, const std::string& is_regexp_strs, const std::string& use_fqn_strs, const std::string& fn_strs, std::ostringstream& out);

    void
    http_get_annotations(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, const std::string& object_id, bool is_regexp, bool merge, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, bool show_all_updates, uint64_t max_num, const std::string& value_filter, bool use_fqn, const std::string& fn_str, std::ostringstream& out);

    void
    http_get_data(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, const std::string& partition_name, const std::string& class_name, const std::string& attribute_name, const std::string& object, bool is_regexp, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, bool show_all_updates, uint32_t sample_interval, const std::string& fill_gaps, bool zip_catalog, bool zip_data, bool catalogize_strings, bool use_fqn, const std::string& fn_str, MultiDataFileResponseServer*& out);

    unsigned int
    http_health(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, std::ostringstream& out);


  private:

    void
    get_servers(const std::string& partition_name, std::map< std::string, pbeast::server_var >& objects, bool throw_empty = true);

    static MHD_Result
    answer_to_connection (void *cls, struct MHD_Connection *connection, const char *url, const char *method, const char *version, const char *upload_data, size_t *upload_data_size, void **ptr);

    unsigned int
    process_http_request(uint64_t call_id, pbeast::srv::UserStat& user_call_stat, std::string_view name, const std::map<std::string,std::string>& parameters, std::ostringstream& out, MultiDataFileResponseServer*& mdfr);

    static std::string
    make_method_params_name(const std::string& server_name, const char * method, const char * partition_name = nullptr, const char * class_name = nullptr, const char* attribute_name = nullptr, uint64_t since = 0, uint64_t until = 0, const char* object = nullptr);


  public:

    // pbeast monitoring methods

    void
    start_monitoring_thread(uint64_t sleep_interval);

    void
    stop_monitoring();

    bool
    needs_monitoring();

    bool
    init_monitoring();

    void
    publish_monitoring_info();

    void
    add_rest(pbeast::srv::UserStat& user_call_stat, uint64_t processing_time, uint64_t size_of_data);

    void
    add_api(pbeast::srv::UserStat& user_call_stat, uint64_t processing_time, uint64_t size_of_data);

    void
    add_corba(uint64_t processing_time, uint64_t size_of_data);


  private:

    std::shared_mutex m_mon_thread_mutex;

    pbeast::srv::Monitoring * m_monitor;

    std::set<std::string> m_initial_receivers;
    pbeast::ThreadPool m_thread_pool;
    uint32_t m_http_request_downsample_threshold;

    struct MHD_Daemon * m_mhd;

    std::mutex m_mutex;


  public:

    static Server * s_srv;
    static bool s_profiling;


  // implementation of CORBA methods reading data from P-BEAST servers
  private:

    void
    get_partitions(std::set<std::string>& data);

    void
    get_classes(const char* partition_name, std::set<std::string>& data);

    void
    get_attributes(const char* partition_name, const char* class_name, std::map<uint64_t, std::set<std::string>>& data);

    void
    get_attributes_with_nested(const char* partition_name, const char* class_name, std::set<std::string>& data);

    void
    get_updated_objects(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, const char* object_mask, std::set<std::string>& data);

    void
    get_attribute_type(const char* partition_name, const char* class_name, const char* attribute_name_path, std::vector<AttrInfoData>& data);

    std::map<std::string, srv::ClassInfo>
    get_schema(const char* partition_name);


    std::string
    get_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<bool>>>& data, const char * fqn_prefix);
    std::string
    get_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<int8_t>>>& data, const char * fqn_prefix);
    std::string
    get_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<uint8_t>>>& data, const char * fqn_prefix);
    std::string
    get_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<int16_t>>>& data, const char * fqn_prefix);
    std::string
    get_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<uint16_t>>>& data, const char * fqn_prefix);
    std::string
    get_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<int32_t>>>& data, const char * fqn_prefix);
    std::string
    get_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<uint32_t>>>& data, const char * fqn_prefix);
    std::string
    get_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<int64_t>>>& data, const char * fqn_prefix);
    std::string
    get_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<uint64_t>>>& data, const char * fqn_prefix);
    std::string
    get_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<float>>>& data, const char * fqn_prefix);
    std::string
    get_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<double>>>& data, const char * fqn_prefix);
    std::string
    get_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<std::string>>>& data, const char * fqn_prefix);
    std::string
    get_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<bool>>>& data, const char * fqn_prefix);
    std::string
    get_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<int8_t>>>& data, const char * fqn_prefix);
    std::string
    get_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<uint8_t>>>& data, const char * fqn_prefix);
    std::string
    get_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<int16_t>>>& data, const char * fqn_prefix);
    std::string
    get_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<uint16_t>>>& data, const char * fqn_prefix);
    std::string
    get_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<int32_t>>>& data, const char * fqn_prefix);
    std::string
    get_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<uint32_t>>>& data, const char * fqn_prefix);
    std::string
    get_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<int64_t>>>& data, const char * fqn_prefix);
    std::string
    get_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<uint64_t>>>& data, const char * fqn_prefix);
    std::string
    get_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<float>>>& data, const char * fqn_prefix);
    std::string
    get_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<double>>>& data, const char * fqn_prefix);
    std::string
    get_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<std::string>>>& data, const char * fqn_prefix);


    std::string
    get_downsample_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<bool>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<int8_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<uint8_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<int16_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<uint16_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<int32_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<uint32_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<int64_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<uint64_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<float>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<double>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesData<std::string>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<bool>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<int8_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<uint8_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<int16_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<uint16_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<int32_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<uint32_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<int64_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<uint64_t>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<float>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<double>>>& data, const char * fqn_prefix);
    std::string
    get_downsample_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov, std::map<std::string, std::set<uint32_t>>& upd, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<std::string>>>& data, const char * fqn_prefix);

  };

  namespace utils
  {
    void
    throw_on_error(const std::string& value, char * sanity, const char * ftype);

    double
    str2double(const std::string& value);

    unsigned long
    str2uint(const std::string& value);

    void
    create_alias_map(const pbeast::FunctionConfig& fc, const std::set<std::string>& names, std::map<std::string, std::string>& out);

    void
    cvt_names(std::map<std::string,std::set<uint32_t>>& data, const std::map<std::string, std::string>& cvt);

    void
    cvt_names(std::map<std::string,std::set<uint64_t>>& data, const std::map<std::string, std::string>& cvt);

    void
    add_verbose(std::string &to, const std::string &what);
  }

}


#define CATCH_SERVER_EXCEPTIONS(METHOD_NAME)                                                     \
catch (const pbeast::NotFound& ex)                                                               \
  {                                                                                              \
    const std::string method(METHOD_NAME);                                                       \
    errors.add(method, ex);                                                                      \
    ERS_DEBUG( 2, "method " << method << " raised NotFound exception \"" << ex.text << '\"');    \
  }                                                                                              \
catch (const pbeast::Failure& ex)                                                                \
  {                                                                                              \
    const std::string method(METHOD_NAME);                                                       \
    errors.add(method, ex);                                                                      \
    ers::error(daq::pbeast::MethodFailure( ERS_HERE, method, ex.text ));                         \
  }                                                                                              \
catch (const pbeast::BadRequest& ex)                                                             \
  {                                                                                              \
    const std::string method(METHOD_NAME);                                                       \
    errors.add(method, ex);                                                                      \
    ERS_DEBUG( 2, "method " << method << " raised BadRequest exception \"" << ex.text << '\"');  \
  }                                                                                              \
catch (CORBA::SystemException& ex)                                                               \
  {                                                                                              \
    const std::string method(METHOD_NAME);                                                       \
    errors.add(method, ex);                                                                      \
    ers::error(daq::pbeast::CorbaError( ERS_HERE, &ex, method ));                                \
  }

#endif
