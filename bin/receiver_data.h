#ifndef PBEAST_RECEIVER_DATA_H
#define PBEAST_RECEIVER_DATA_H

#include <stdint.h>
#include <limits.h>

#include <atomic>
#include <iostream>
#include <filesystem>
#include <map>
#include <stdexcept>
#include <string>
#include <vector>

#include <boost/regex.hpp>

#include <pbeast/data-type.h>
#include <pbeast/series-data.h>
#include <pbeast/output-data-file.h>

namespace pbeast
{
  namespace receiver
  {

    class Attribute;
    class Class;

    template<class T>
      class VectorSerie
      {

      public:

        std::vector<pbeast::SeriesVectorData<T>> m_data;

      };


    class BaseData
    {

    public:

      BaseData(pbeast::DataType type) :
          m_type(type), m_min_ts(ULLONG_MAX), m_max_ts(0), m_number_of_items(0), m_id(s_id_count++)
      {
        ;
      }

      static void
      set_max_data_bucket_size(uint64_t val)
      {
        s_max_data_bucket_size = val;
      }

      virtual
      ~BaseData()
      {
        ;
      }

      bool
      commit(const std::filesystem::path& dir, const std::string& partition,
          const std::string& partition_dir_name, Class& c, Attribute& a);

      bool
      is_ready(uint64_t expire_ts) const
      {
        if (!m_number_of_items)
          return false;

        if(m_number_of_items >= s_max_data_bucket_size)
          return true;

        return (m_min_ts < expire_ts);
      }

      uint32_t
      get_number_of_items() const
      {
        return m_number_of_items;
      }

      uint64_t
      get_min_ts() const
      {
        return m_min_ts;
      }

      uint64_t
      get_max_ts() const
      {
        return m_max_ts;
      }

      virtual uint32_t
      get_num_of_series() const = 0;

      pbeast::DataType
      get_type() const
      {
        return m_type;
      }

      void
      process_stat(uint64_t ts)
      {
        m_number_of_items++;
        if (ts < m_min_ts)
          m_min_ts = ts;
        if (ts > m_max_ts)
          m_max_ts = ts;
      }

      void
      process_last_updated_stat(uint64_t ts_last_updated)
      {
        if (ts_last_updated > m_max_ts)
          m_max_ts = ts_last_updated;
      }

      uint64_t
      get_id() const
      {
        return m_id;
      }

      virtual void
      write(pbeast::OutputDataFile& file) = 0;

      virtual void
      get_objects(std::vector<std::string>& out, boost::regex * re = nullptr) = 0;

    protected:

      pbeast::DataType m_type;
      uint64_t m_min_ts;
      uint64_t m_max_ts;
      uint32_t m_number_of_items;
      uint64_t m_id;

    private:

      static std::atomic<uint_least64_t> s_max_data_bucket_size;
      static std::atomic<uint_least64_t> s_id_count;

    };


    template<class T>
      class Data : public BaseData
      {

      public:

        Data(pbeast::DataType type) :
            BaseData(type)
        {
          ;
        }

        ~Data()
        {
          for(auto& x : m_data)
            {
              delete x.second;
            }
          m_data.clear();
        }

        const std::map<std::string,pbeast::Serie<T>*>&
        get_data()
        {
          return m_data;
        }

        pbeast::Serie<T> *
        get_serie(const std::string& name)
        {
          pbeast::Serie<T> *& ptr(m_data[name]);
          if(ptr == nullptr)
            {
              ptr = new pbeast::Serie<T>();
            }
          return ptr;
        }

        pbeast::Serie<T> *
        find_serie(const std::string& name)
        {
          typename std::map<std::string,pbeast::Serie<T>*>::iterator i = m_data.find(name);
          if(i != m_data.end())
            return i->second;
          else
            return nullptr;
        }

        void add_data(pbeast::Serie<T> * serie, uint64_t ts, const T& v)
        {
          serie->m_data.emplace_back(ts, ts, v);
          process_stat(ts);
        }

        void adjust_last_updated(pbeast::Serie<T> * serie, uint64_t ts)
        {
          serie->m_data.back().set_ts_last_updated(ts);
          process_last_updated_stat(ts);
        }

        uint32_t get_num_of_series() const { return m_data.size(); }

        void
        write(pbeast::OutputDataFile& file)
        {
          for(auto & i : m_data)
            {
              file.write_data(i.first, i.second->m_data);
            }
        }

        void
        get_objects(std::vector<std::string>& out, boost::regex * re)
        {
          for(auto & x : m_data)
            {
              if(re)
                {
                  try
                    {
                      if(boost::regex_match(x.first, *re) == false) continue;
                    }
                  catch (std::exception& ex)
                    {
                      std::ostringstream text;
                      text << "boost::regex_match() failed for string \'" << x.first << "\': " << ex.what() << std::endl;
                      throw std::runtime_error(text.str().c_str());
                    }
                }

              out.push_back(x.first);
            }
        }


      private:

        std::map<std::string,pbeast::Serie<T>*> m_data;

    };


    template<class T> class VectorData : public BaseData {

      public:

        VectorData(pbeast::DataType type) : BaseData(type) { ; }

        ~VectorData()
        {
          for(auto& x : m_data)
            {
              delete x.second;
            }
          m_data.clear();
        }

        const std::map<std::string,VectorSerie<T>*>&
        get_data()
        {
          return m_data;
        }

        VectorSerie<T> *
        get_serie(const std::string& name)
        {
          VectorSerie<T> *& ptr(m_data[name]);
          if(ptr == nullptr)
            {
              ptr = new VectorSerie<T>();
            }
          return ptr;
        }

        VectorSerie<T> *
        find_serie(const std::string& name)
        {
          typename std::map<std::string,VectorSerie<T>*>::iterator i = m_data.find(name);
          if(i != m_data.end())
            return i->second;
          else
            return nullptr;
        }

        void add_data(VectorSerie<T> * serie, uint64_t ts, const std::vector<T>& v)
        {
          serie->m_data.emplace_back(ts, ts, v);
          process_stat(ts);
        }

        void adjust_last_updated(VectorSerie<T> * serie, uint64_t ts)
        {
          serie->m_data.back().set_ts_last_updated(ts);
          process_last_updated_stat(ts);
        }

        uint32_t
        get_num_of_series() const
        {
          return m_data.size();
        }

        void
        write(pbeast::OutputDataFile& file)
        {
          for(auto & i : m_data)
            {
              file.write_data(i.first, i.second->m_data);
            }
        }

        void
        get_objects(std::vector<std::string>& out, boost::regex * re)
        {
          for(auto & x : m_data)
            {
              if(re)
                {
                  try
                    {
                      if(boost::regex_match(x.first, *re) == false) continue;
                    }
                  catch (std::exception& ex)
                    {
                      std::ostringstream text;
                      text << "boost::regex_match() failed for string \'" << x.first << "\': " << ex.what() << std::endl;
                      throw std::runtime_error(text.str().c_str());
                    }
                }

              out.push_back(x.first);
            }
        }

      private:

        std::map<std::string,VectorSerie<T>*> m_data;

    };
  }
}


#endif
