#ifndef PBEAST_WEB_RECEIVER_CONFIG_H
#define PBEAST_WEB_RECEIVER_CONFIG_H

#include <string>

#include "receiver_config.h"

#include "pbeast/PBeastWebReceiverApplication.h"

namespace pbeast
{
  namespace receiver
  {

    class Web_Config : public Config
    {

      friend class DataProvider;

    public:

      void
      init(const pbeast::PBeastWebReceiverApplication& obj)
      {
        Config::init(obj);

        m_http_port = obj.get_HttpPort();
        m_http_timeout = obj.get_HttpTimeout();
        m_http_thread_pool_size = obj.get_HttpThreadPoolSize();
      }

      ~Web_Config()
      {
        ;
      }

      uint32_t
      get_http_port() const
      {
        return m_http_port;
      }

      uint32_t
      get_http_timeout() const
      {
        return m_http_timeout;
      }

      uint32_t
      get_http_thread_pool_size() const
      {
        return m_http_thread_pool_size;
      }


    private:

      uint32_t m_http_port;
      uint32_t m_http_timeout;
      uint32_t m_http_thread_pool_size;

    };

  }
}

#endif
