#ifndef PBEAST_RECEIVER_ERRORS_H
#define PBEAST_RECEIVER_ERRORS_H

#include <pbeast/exceptions.h>

namespace daq {

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    SystemCallFailed,
    Exception,
    "function \"" << function << "\" failed with error code " << code << ": " << message,
    ,
    ((std::string)function)
    ((int)code)
    ((std::string)message)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotReadClassInfoDocument,
    Exception,
    "cannot read info document for class " << name,
    ,
    ((std::string)name)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    UnexpectedAttributeClassType,
    Exception,
    "unexpected class type for attribute " << attribute_name << " of class " << class_name << " in partition " << partition_name,
    ,
    ((std::string)attribute_name)
    ((std::string)class_name)
    ((std::string)partition_name)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FailedReadInfoDocument,
    Exception,
    "failed to read info document" << message,
    ,
    ((std::string)message)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    BadInfoType,
    Exception,
    "unexpected info type \"" << type << '\"',
    ,
    ((std::string)type)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    NoInfoType,
    Exception,
    "cannot find info type \"" << type << "\" required for subscription criteria",
    ,
    ((std::string)type)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    TypeMismatch,
    Exception,
    "mismatch between IS info description and object \"" << object_name << "\" of class \"" << class_name << '\"',
    ,
    ((std::string)object_name)
    ((std::string)class_name)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    SkipDataDelayedTimestampError,
    Exception,
    "skip IS callback with timestamp \"" << ts << "\" of IS object \"" << object_name << "@" << class_name << "\" published by \"" << provider << "\", that is outside tolerable data delay interval set to " << limit << " seconds",
    ,
    ((std::string)ts)
    ((std::string)object_name)
    ((std::string)class_name)
    ((std::string)provider)
    ((uint32_t)limit)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    DataDelayedTimestampError,
    Exception,
    "the timestamp \"" << ts << "\" of IS callback for object \"" << object_name << "@" << class_name << "\" published by \"" << provider << "\" is outside expected data delay interval set to " << limit << " seconds",
    ,
    ((std::string)ts)
    ((std::string)object_name)
    ((std::string)class_name)
    ((std::string)provider)
    ((uint32_t)limit)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    DataTimestampInFutureError,
    Exception,
    "the timestamp \"" << ts << "\" of IS callback for object \"" << object_name << "@" << class_name << "\" published by \"" << provider << "\" is in the future",
    ,
    ((std::string)ts)
    ((std::string)object_name)
    ((std::string)class_name)
    ((std::string)provider)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    NoPartitionDirectory,
    Exception,
    "cannot write P-BEAST data to directory " << path << " (" << why << ")",
    ,
    ((std::filesystem::path)path)
    ((std::string)why)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotFindDescriptionOfClass,
    Exception,
    "cannot " << action << " description of class " << class_name << " to " << op << " object " << id << " published by \"" << provider << '\"',
    ,
    ((const char *)action)
    ((std::string)class_name)
    ((std::string)provider)
    ((const char *)op)
    ((std::string)id)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotCommitDataFile,
    Exception,
    "cannot commit file " << path,
    ,
    ((std::filesystem::path)path)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    LostData,
    Exception,
    "cannot commit data file after " << num_of_attempts << " attempts and lost " << num << " data items of " << attribute_name << '@' << class_name,
    ,
    ((unsigned int)num_of_attempts)
    ((unsigned int)num)
    ((std::string)attribute_name)
    ((std::string)class_name)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotReadMetaInformation,
    Exception,
    "cannot read meta information",
    ,
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    BadConfigDelays,
    Exception,
    "MaxSaveDelay configuration parameter (" << max_save_delay << ") is smaller than MaxCallbackDelay (" << max_callback_delay << ')',
    ,
    ((uint32_t)max_save_delay)
    ((uint32_t)max_callback_delay)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    BadConfigDataSizeLimits,
    Exception,
    "MaxDataSize configuration parameter (" << max_data_size << ") is smaller than MaxFileSize (" << max_file_size << ')',
    ,
    ((uint32_t)max_data_size)
    ((uint32_t)max_file_size)
  )

}

#endif
