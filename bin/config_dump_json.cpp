#include <stdlib.h>

#include <iostream>
#include <string>

#include <boost/program_options.hpp>

#include <config/Configuration.h>
#include <config/ConfigObject.h>
#include <config/Schema.h>

ERS_DECLARE_ISSUE(
  config_dump,
  BadCommandLine,
  "bad command line: " << reason,
  ((const char*)reason)
)

ERS_DECLARE_ISSUE(
  config_dump,
  ConfigException,
  "caught daq::config::Exception exception",
)

struct SortByName
{
  bool
  operator()(const ConfigObject *o1, const ConfigObject *o2) const
  {
    return o1->UID() < o2->UID();
  }
};

inline bool
is_null_obj(const ConfigObject * o)
{
  return (o == nullptr || o->is_null());
}

template<class T>
  void
  print_val(const T &val)
  {
    std::cout << val;
  }

template<>
  void
  print_val<uint8_t>(const uint8_t &val)
  {
  std::cout << static_cast<uint16_t>(val);
  }

template<>
  void
  print_val<int8_t>(const int8_t &val)
  {
    std::cout << static_cast<int16_t>(val);
  }

template<>
  void
  print_val<std::string>(const std::string &val)
  {
    std::cout << '\"' << val << '\"';
  }

template<class T>
  void
  print_value(const ConfigObject& const_obj, const daq::config::attribute_t& a, const std::string& prefix, const std::string& tab, bool extended)
  {
    ConfigObject& obj = const_cast<ConfigObject&>(const_obj);

    const std::string prefix2(prefix + tab);
    const std::string prefix3(prefix2 + tab);

    if (extended)
      std::cout
        << prefix2 << "{\n"
        << prefix3 << "\"type\": \"" << daq::config::attribute_t::type(a.p_type) << "\",\n"
        << prefix3;
    else
      std::cout << prefix2;

    std::cout << "\"" << a.p_name << "\": ";

    if (a.p_is_multi_value)
      {
        std::vector<T> value;
        obj.get(a.p_name, value);

        std::cout << '[';
        for (unsigned int i = 0; i < value.size(); ++i)
          {
            if (i != 0)
              std::cout << ", ";

            print_val<T>(value[i]);
          }

        std::cout << ']';
      }
    else
      {
        T value;
        obj.get(a.p_name, value);
        print_val<T>(value);
      }

    if (extended)
      std::cout << std::endl
        << prefix2 << '}';
  }


static  void
dump(::Configuration& db, const ::ConfigObject& obj, const std::string& prefix, const std::string& tab, bool extended, bool is_nested, std::unordered_set<const ConfigObjectImpl *> * fuse)
{
  const std::string prefix2(prefix + tab);
  const std::string prefix3(prefix2 + tab);

  // check if it is not a reference to 0
  if (is_null_obj(&obj))
    {
      std::cout << " null";
      return;
    }

  if (!is_nested)
    std::cout
      << prefix2 << "{\n"
      << prefix3 << "\"id\": \"" << obj.UID() << "\",\n"
      << prefix3 << "\"ts\": " << obj.time().tv_sec * 1000000 + obj.time().tv_nsec / 1000 << ",\n"
      << prefix3 << "\"data\": ";
  else
    std::cout << std::endl << prefix3;

  const std::string prefix4(prefix3 + tab);
  const std::string prefix5(prefix4 + tab);

  std::cout << (extended ? '[' : '{') << std::endl;

  const daq::config::class_t& cd(db.get_class_info(obj.class_name()));

  bool is_first=true;

  // print attributes
  for (const auto& x : cd.p_attributes)
    {
      if (is_first)
        is_first = false;
      else
        std::cout << ",\n";

      switch (x.p_type)
        {
          case daq::config::string_type :
          case daq::config::enum_type :
          case daq::config::date_type :
          case daq::config::time_type :
          case daq::config::class_type :
                                         print_value<std::string>(obj, x, prefix3, tab, extended); break;
          case daq::config::bool_type:   print_value<bool>(obj, x, prefix3, tab, extended);        break;
          case daq::config::u8_type:     print_value<uint8_t>(obj, x, prefix3, tab, extended);     break;
          case daq::config::s8_type:     print_value<int8_t>(obj, x, prefix3, tab, extended);      break;
          case daq::config::u16_type:    print_value<uint16_t>(obj, x, prefix3, tab, extended);    break;
          case daq::config::s16_type:    print_value<int16_t>(obj, x, prefix3, tab, extended);     break;
          case daq::config::u32_type:    print_value<uint32_t>(obj, x, prefix3, tab, extended);    break;
          case daq::config::s32_type:    print_value<int32_t>(obj, x, prefix3, tab, extended);     break;
          case daq::config::u64_type:    print_value<uint64_t>(obj, x, prefix3, tab, extended);    break;
          case daq::config::s64_type:    print_value<int64_t>(obj, x, prefix3, tab, extended);     break;
          case daq::config::float_type:  print_value<float>(obj, x, prefix3, tab, extended);       break;
          case daq::config::double_type: print_value<double>(obj, x, prefix3, tab, extended);      break;
          default:                       std::cout << "*** bad type ***";
        }
    }

  // print relationships
  for (const auto& x : cd.p_relationships)
    {
      if (is_first)
        is_first = false;
      else
        std::cout << ",\n";

      if (extended)
        std::cout
          << prefix4 << "{\n"
          << prefix5 << "\"type\": \"" << x.p_type << "\",\n"
          << prefix5;
      else
        std::cout << prefix4;

      std::cout << "\"" << x.p_name << "\":";

      const bool mv((x.p_cardinality == daq::config::zero_or_many) || (x.p_cardinality == daq::config::one_or_many));

      if (mv)
        std::cout << " [";

      if (x.p_is_aggregation == true)
        {
          if (mv)
            {
              std::vector<ConfigObject> value;
              const_cast<ConfigObject&>(obj).get(x.p_name, value);
              if (!value.empty())
                {
                  bool is_first = true;

                  for (const auto& x : value)
                    {
                      if (is_first)
                        is_first = false;
                      else
                        std::cout << ',';

                      dump(db, x, prefix4, tab, extended, true, fuse);
                    }
                }
            }
          else
            {
              ConfigObject value;
              const_cast<ConfigObject&>(obj).get(x.p_name, value);
              dump(db, value, prefix4, tab, extended, true, fuse);
            }
        }

      if (mv)
        std::cout << std::endl << (extended ? prefix5 : prefix4) << "]";

      if (extended)
              std::cout
                << std::endl << prefix4 << "}";
    }

  std::cout << std::endl
    << prefix3 << (extended ? ']' : '}');

  if (!is_nested)
    std::cout << "\n" << prefix2 << "}";
}


int main(int argc, char *argv[])
{
  const std::string any("*");
  std::string db_name, class_name, object_id;
  bool extended = false;
  unsigned int tab_size = 2;

  std::unordered_set<const ConfigObjectImpl *> fuse;

  boost::program_options::options_description desc(
      "Dump objects in json format adapted for P-BEAST case. The aggregation relationships are expanded.\n"
      "\n"
      "Options/Arguments");

  std::unordered_set<const ConfigObjectImpl *> printed_aggregation_objects;

  try
    {
      desc.add_options()
        ( "database,d", boost::program_options::value<std::string>(&db_name)->required(), "database specification in format plugin-name:parameters")
        ( "class,c", boost::program_options::value<std::string>(&class_name)->required(), "print objects of given class" )
        ( "objects,o", boost::program_options::value<std::string>(&object_id)->default_value(any), "dump this object only" )
        ( "extended,x", "print extended format" )
        ( "tab,t", boost::program_options::value<unsigned int>(&tab_size)->default_value(tab_size), "tab size" )
        ( "help,h", "print help message" );

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
        }

      boost::program_options::notify(vm);

      if (vm.count("extended"))
        extended = true;
    }
  catch (std::exception &ex)
    {
      ers::fatal(config_dump::BadCommandLine(ERS_HERE, ex.what()));
      return EXIT_FAILURE;
    }

  const std::string tab (tab_size, ' ');

  try
    {
      Configuration db(db_name);

      // validate class existence
      const daq::config::class_t& info = db.get_class_info(class_name);

      std::cout
          << "{\n"
          << tab << "\"" << info.p_name << "\": [\n";

      std::vector<ConfigObject> objects;

      if (object_id == any)
        {
          std::vector<ConfigObject> objects;
          db.get(class_name, objects);

          // sort by ID for consistent output
          std::set<const ConfigObject*, SortByName> sorted_by_id;

          for (const auto &x : objects)
            sorted_by_id.insert(&x);

          bool is_first = true;

          for (const auto &x : sorted_by_id)
            {
              if (is_first)
                is_first = false;
              else
                std::cout << ",\n";

              dump(db, *x, tab, tab, extended, false, &fuse);
            }
        }
      else
        {
          ConfigObject obj;
          db.get(class_name, object_id, obj);
          dump(db, obj, tab, tab, extended, false, &fuse);
        }

      std::cout << std::endl
          << tab << "]\n"
          << "}\n";

    }
  catch (daq::config::Exception &ex)
    {
      ers::fatal(config_dump::ConfigException(ERS_HERE, ex));
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
