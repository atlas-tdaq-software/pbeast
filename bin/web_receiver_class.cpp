#include <algorithm>

#include "pbeast/data-type.h"
#include "pbeast/meta.h"
#include "pbeast/pbeast.hh"
#include "pbeast/series-data.h"

#include "receiver_attribute.h"
#include "receiver_data_provider.h"
#include "web_receiver_class.h"

/*
 *  JSON attribute object format:
 *  {
 *    "name":"attribute-A",
 *    "description":"description of attribute-A",
 *    "type":"pbeast-data-type",
 *    "isArray":boolean
 *  }
 */

pbeast::receiver::Web_Class::Web_Class(const std::string& name, JsonValue obj) :
    Class(name)
{
  // count attribute objects and reserve space for them

  unsigned int count(1);

  for (auto i : obj)
    {
      if (i->value.getTag() != JSON_OBJECT)
        {
          std::ostringstream text;
          text << "attribute #" << count << " of class \"" << name << "\" is not an object";
          throw std::runtime_error(text.str().c_str());
        }

      count++;
    }

  m_attributes.reserve(count-1);


  // parse json and create attributes

  count = 1;

  for (auto i : obj)
    {
      std::string attribute_name, attribute_description, type_name;
      pbeast::DataType type(pbeast::VOID);
      bool is_array(false);

      for (auto j : i->value)
        {
          if (!strcmp(j->key, "name"))
            {
              if (j->value.getTag() != JSON_STRING)
                {
                  std::ostringstream text;
                  text << "the value of \"name\" key of attribute #" << count << " of class \"" << name << "\" is not a string";
                  throw std::runtime_error(text.str().c_str());
                }
              attribute_name = j->value.toString();
            }
          else if (!strcmp(j->key, "description"))
            {
              if (j->value.getTag() != JSON_STRING)
                {
                  std::ostringstream text;
                  text << "the value of \"description\" key of attribute #" << count << " of class \"" << name << "\" is not a string";
                  throw std::runtime_error(text.str().c_str());
                }
              attribute_description = j->value.toString();
            }
          else if (!strcmp(j->key, "type"))
            {
              if (j->value.getTag() != JSON_STRING)
                {
                  std::ostringstream text;
                  text << "the value of \"type\" key of attribute #" << count << " of class \"" << name << "\" is not a string";
                  throw std::runtime_error(text.str().c_str());
                }
              type_name = j->value.toString();
              type = pbeast::str2type(type_name);

              if (type == pbeast::OBJECT)
                {
                  std::ostringstream text;
                  text << "the type of attribute #" << count << " of class \"" << name << "\" is not P-BEAST primitive type; object types are not yet supported";
                  throw std::runtime_error(text.str().c_str());
                }
            }
          else if (!strcmp(j->key, "isArray"))
            {
              if (j->value.getTag() == JSON_TRUE)
                {
                  is_array = true;
                }
              else if (j->value.getTag() == JSON_FALSE)
                {
                  is_array = false;
                }
              else
                {
                  std::ostringstream text;
                  text << "the value of \"isArray\" key of attribute #" << count << " of class \"" << name << "\" is not a boolean";
                  throw std::runtime_error(text.str().c_str());
                }
            }
          else
            {
              std::ostringstream text;
              text << "the attribute #" << count << " of class \"" << name << "\" has unexpected key \'" << j->key << "\'";
              throw std::runtime_error(text.str().c_str());
            }
        }

      if(attribute_name.empty())
        {
          std::ostringstream text;
          text << "the \"name\" of attribute #" << count << " of class \"" << name << "\" object is not set";
          throw std::runtime_error(text.str().c_str());
        }

      if(attribute_description.empty())
        {
          std::ostringstream text;
          text << "the \"description\" of attribute \"" << attribute_name << "\" of class \"" << name << "\" object is not set";
          throw std::runtime_error(text.str().c_str());
        }

      if(type_name.empty())
        {
          std::ostringstream text;
          text << "the \"type\" of attribute \"" << attribute_name << "\" of class \"" << name << "\" object is not set";
          throw std::runtime_error(text.str().c_str());
        }

      if(get_attribute(attribute_name))
        {
          std::ostringstream text;
          text << "the attribute \"" << attribute_name << "\" is already defined in class \"" << name << '\"';
          throw std::runtime_error(text.str().c_str());
        }

      m_attributes.push_back(new Attribute(this, attribute_name, type, type_name, is_array, attribute_description));

      count++;
    }

  std::sort(m_attributes.begin(), m_attributes.end(), [](Attribute * a1, Attribute * a2) -> bool
    { return (a1->get_name() < a2->get_name());});

  std::ostringstream debug_text;
  debug_text << "class: " << name << '\n';

  for(const auto& a : m_attributes)
    debug_text << a->get_name() << " of " << a->get_type_name() << (a->get_is_array() ? "[]" : "") << '\n';

  ERS_DEBUG(0, debug_text.str());
}

pbeast::receiver::Web_Class::Web_Class(const std::string& class_name, const std::string& partition_name, const std::filesystem::path& repository_path) :
    Class(class_name)
{
  pbeast::Meta info(repository_path, partition_name, class_name);

  const auto& attribute_types = info.get_all_types();

  m_attributes.reserve(attribute_types.size());

  std::ostringstream debug_text;
  debug_text << "class: " << class_name << '\n';

  for(auto & x : attribute_types)
    {
      std::string type_name;
      bool is_array;
      pbeast::Meta::decode_type((*x.second.rbegin()).get_value(), type_name, is_array);

      const std::string& description = (*info.get_attribute_descriptions(x.first).rbegin()).get_value();

      debug_text << x.first << " of " << type_name << (is_array ? "[]" : "") << '\n';

      m_attributes.push_back(new Attribute(this, x.first, pbeast::str2type(type_name), type_name, is_array, description));
    }

  m_directory_created = true;

  ERS_DEBUG(1, debug_text.str());
}

static void
update_attribute(pbeast::receiver::Attribute& a, const std::string& id, uint64_t ts, JsonValue value)
{
  try
    {
      switch (a.get_type())
        {
          case pbeast::BOOL:       pbeast::receiver::Web_Class::update<bool>(a, id, value, ts);             break;
          case pbeast::S8:         pbeast::receiver::Web_Class::update<int8_t>(a, id, value, ts);           break;
          case pbeast::U8:         pbeast::receiver::Web_Class::update<uint8_t>(a, id, value, ts);          break;
          case pbeast::S16:        pbeast::receiver::Web_Class::update<int16_t>(a, id, value, ts);          break;
          case pbeast::U16:        pbeast::receiver::Web_Class::update<uint16_t>(a, id, value, ts);         break;
          case pbeast::ENUM:
          case pbeast::S32:        pbeast::receiver::Web_Class::update<int32_t>(a, id, value, ts);          break;
          case pbeast::DATE:
          case pbeast::U32:        pbeast::receiver::Web_Class::update<uint32_t>(a, id, value, ts);         break;
          case pbeast::S64:        pbeast::receiver::Web_Class::update<int64_t>(a, id, value, ts);          break;
          case pbeast::TIME:
          case pbeast::U64:        pbeast::receiver::Web_Class::update<uint64_t>(a, id, value, ts);         break;
          case pbeast::FLOAT:      pbeast::receiver::Web_Class::update<float>(a, id, value, ts);            break;
          case pbeast::DOUBLE:     pbeast::receiver::Web_Class::update<double>(a, id, value, ts);           break;
          case pbeast::STRING:     pbeast::receiver::Web_Class::update<std::string>(a, id, value, ts);      break;
          case pbeast::OBJECT:     throw std::runtime_error("object type is not supported");
          case pbeast::VOID:       throw std::runtime_error("void type is not supported");
        }
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "failed to update attribute \"" << a.get_name() << "\" of object \"" << id << '@' << a.get_parent_class()->get_name() << "\" because " << ex.what();
      throw std::runtime_error(text.str().c_str());
    }

}

static JsonValue dummu;
static JsonIterator s_end(end(dummu));

void
pbeast::receiver::Web_Class::update_any_object(const std::string& id, uint64_t ts, JsonIterator it)
{
  int count = 0;

  add_updated(id, ts);

  while(it != s_end)
    {
      if(Attribute * a = get_attribute(it->key))
        {
          count++;
          update_attribute(*a, id, ts, it->value);
        }
      else
        {
          std::ostringstream text;
          text << "object \"" << id << "\" of class " << get_name() << " contains attribute \"" << it->key << "\" that is not defined in the class";
          throw pbeast::NotFound(text.str().c_str());
        }

      ++it;
    }

  DataProvider::get_monitoring_object().add(count);
}

void
pbeast::receiver::Web_Class::update_full_object(const std::string& id, uint64_t ts, JsonIterator it)
{
  add_updated(id, ts);

  for(unsigned long idx = 0; it != s_end; ++it, ++idx)
    {
      update_attribute(get_attribute(idx), id, ts, it->value);
    }

  DataProvider::get_monitoring_object().add(get_number_of_attributes());
}

void
pbeast::receiver::Web_Class::close_objects(uint64_t ts, JsonValue value)
{
  int count = 1;

  for (auto i : value)
    {
      if (i->value.getTag() != JSON_STRING)
        {
          std::ostringstream text;
          text << "the item #" << count << " of class \"" << get_name() << "\" is not a string";
          throw std::runtime_error(text.str().c_str());
        }

      if (char * name = i->value.toString())
        {
          add_eov(name, ts);
        }
      else
        {
          std::ostringstream text;
          text << "the item #" << count << " of class \"" << get_name() << "\" is not a string";
          throw std::runtime_error(text.str().c_str());
        }

      count++;
    }
}
