#ifndef PBEAST_WEB_RECEIVER_CLASS_H
#define PBEAST_WEB_RECEIVER_CLASS_H

#include <stdint.h>
#include <filesystem>
#include <string>
#include <vector>

#include "../src/gason.h"
#include "receiver_attribute.h"
#include "receiver_class.h"

namespace pbeast
{
  namespace receiver
  {

    class Web_Class : public Class
    {

      friend class Attribute;
      friend class DataProvider;

    public:

      Web_Class(const std::string& name, JsonValue obj);

      Web_Class(const std::string& class_name, const std::string& partition_name, const std::filesystem::path& repository_path);

      Web_Class(const Web_Class& c, Attribute * parent) :
          Class(c, parent)
      {
        ;
      }

      ~Web_Class()
      {
        ;
      }

      Web_Class() = delete;

      Web_Class(const Web_Class&) = delete;

      Web_Class&
      operator=(const Web_Class&) = delete;

      Class *
      make_new(Attribute * a)
      {
        return new Web_Class(*this, a);
      }

      void
      update_any_object(const std::string& id, uint64_t ts, JsonIterator it);

      void
      update_full_object(const std::string& id, uint64_t ts, JsonIterator it);

      template<typename T>
        static void
        update(Attribute& a, const std::string& name, JsonValue s, uint64_t current_ts)
        {
          if (a.get_is_array() == false)
            {
              T value;
              s.get(value);
              static_cast<DataHolder<T> *>(a.get_raw_data())->add_data(name, current_ts, value);
            }
          else
            {
              std::vector<T> value;
              s.get<T>(value);
              static_cast<VectorDataHolder<T> *>(a.get_raw_data())->add_data(name, current_ts, value);
            }
        }

      void
      close_objects(uint64_t ts, JsonValue value);

   };

  }
}


#endif
