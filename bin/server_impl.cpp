#include <chrono>

#include <boost/regex.hpp>

#include <ipc/exceptions.h>

#include <pbeast/data-file.h>
#include <pbeast/series-idl.h>
#include <pbeast/object-name.h>

#include "application_exceptions.h"
#include "server.h"
#include "server_aggregation.h"
#include "server_functions.h"


std::string
pbeast::Server::make_method_params_name(const std::string& server_name, const char * method, const char * partition_name, const char * class_name, const char* attribute_name, uint64_t since, uint64_t until, const char* object)
{
  std::ostringstream text;
  text << method << '(';

  if (partition_name)
    text << "partition: \'" << partition_name << '\'';

  if (class_name)
    text << ", class: \'" << class_name << '\'';

  if (attribute_name)
    text << ", attribute: \'" << attribute_name << '\'';

  if (since || until)
    text << ", since:  " << since << ", until:  " << until;

  if (object && *object)
    text << ", object(s): \'" << object << '\'';

  text << ") on server \'" << server_name << '\'';

  return text.str();
}

#define METHOD( F , OBJ )  \
make_method_params_name(server_name, F, partition_name, class_name, attribute_name, since, until, OBJ)


std::string
Issues::make_text(const std::string& method, const char * what, const char * issue)
{
  return ("remote method " + method + " raised " + what + " exception: " + issue);
}

std::string
Issues::make_text(const std::string& method, const CORBA::SystemException& issue)
{
  std::ostringstream text;
  text << "remote method " + method << " raised CORBA system exception " << &issue;
  return text.str();
}

std::string
Issues::make_report(const char * method, const char * action, const std::set<std::string>& data)
{
  std::ostringstream text;

  text << "method " << method << ' ' << action << "\n\twas caused by";

  if (data.size() > 1)
    {
      text << ' ' << data.size() << " issues: ";

      unsigned int count(1);
      for (auto& x : data)
        {
          text << "\n\t(" << count++ << ") " << x;
        }
    }
  else
    {
      text << ": " << *data.begin();
    }

  return text.str();
}

void
Issues::validate(const char * name, bool empty_data, uint64_t size_of_result) const
{
  // throw if failed
  if (!m_bad_request.empty())
    {
      const std::string text(make_report(name, "failed", m_bad_request));
      ERS_LOG("throw pbeast::BadRequest: " << text);
      throw pbeast::BadRequest(text.c_str());
    }

  if (!m_failure.empty())
    {
      const std::string text(make_report(name, "failed", m_failure));
      ERS_LOG("throw pbeast::Failure: " << text);
      throw pbeast::Failure(text.c_str());
    }

  // throw if not found
  if (empty_data)
    {
      if (!m_not_found.empty())
        {
          if (size_of_result == 0 || size_of_result == m_not_found.size())
            {
              const std::string text(make_report(name, "returns no data", m_not_found));
              ERS_LOG("throw pbeast::NotFound: " << text);
              throw pbeast::NotFound(text.c_str());
            }
        }
    }
}

void
pbeast::Server::get_updated_objects(const char* partition_name, const char* class_name, const char* attribute_name_path, ::CORBA::ULongLong since, ::CORBA::ULongLong until, const char* object_mask, std::set<std::string>& data)
{
  std::map<std::string, pbeast::server_var> server_objects;
  get_servers(partition_name, server_objects);

    {
      struct task
      {
        static void
        run(std::mutex& mutex, std::set<std::string>& data, const char * partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, const char* object_mask, Issues& errors, const std::string& server_name, pbeast::server_var server)
        {
          try
            {
              pbeast::StringArray_var objects;
              server->get_updated_objects(partition_name, class_name, attribute_name, since, until, object_mask, objects);

              std::lock_guard<std::mutex> lock(mutex);

              for (unsigned int idx = 0; idx < objects->length(); idx++)
                {
                  data.insert(static_cast<const char *>(objects[idx]));
                }
            }

          CATCH_SERVER_EXCEPTIONS(METHOD("get_updated_objects", object_mask))

        }
      };

      std::mutex mutex;
      std::vector<std::future<void> > results;
      Issues issues;

      for (auto &i : server_objects)
        results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(data), partition_name, class_name, attribute_name_path, since, until, object_mask, std::ref(issues), std::ref(i.first), i.second)));

      for (unsigned int i = 0; i < results.size(); ++i)
        results[i].get();

      issues.validate("get_updated_objects", data.empty(), results.size());
    }
}

void
pbeast::Server::get_attributes(const char* partition_name, const char* class_name, std::map<uint64_t, std::set<std::string>>& data)
{
  std::map<std::string, pbeast::server_var> server_objects;
  get_servers(partition_name, server_objects);

    {
      struct task
      {
        static void
        run(std::mutex& mutex, std::map<uint64_t, std::set<std::string>>& data, const char * partition_name, const char * class_name, Issues& errors, const std::string& server_name, pbeast::server_var server)
        {
          try
            {
              pbeast::StringDataPointArrayList_var attributes;
              server->get_attributes(partition_name, class_name, attributes);
              std::lock_guard<std::mutex> lock(mutex);

              for (unsigned int idx = 0; idx < attributes->length(); idx++)
                {
                  std::set<std::string>& created_data = data[attributes[idx].ts.created];
                  for (unsigned int j = 0; j < attributes[idx].data.length(); j++)
                    {
                      created_data.insert(static_cast<const char *>(attributes[idx].data[j]));
                    }

                  if (attributes[idx].ts.updated != attributes[idx].ts.created)
                    {
                      std::set<std::string>& updated_data = data[attributes[idx].ts.updated];
                      for (unsigned int j = 0; j < attributes[idx].data.length(); j++)
                        {
                          updated_data.insert(static_cast<const char *>(attributes[idx].data[j]));
                        }
                    }
                }
            }

          CATCH_SERVER_EXCEPTIONS(make_method_params_name(server_name, "get_attributes", partition_name, class_name))

        }
      };

      std::mutex mutex;
      std::vector<std::future<void> > results;
      Issues issues;

      for (auto &i : server_objects)
        results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(data), partition_name, class_name, std::ref(issues), std::ref(i.first), i.second)));

      for (unsigned int i = 0; i < results.size(); ++i)
        results[i].get();

      issues.validate("get_attributes", data.empty());
    }
}

#define GET_ATTRIBUTES_WITH_NESTED_PARAMS \
make_method_params_name(server_name, "get_attributes/get_attribute_type", partition_name, class_name)

// is used by REST auto-complete series call
void
pbeast::Server::get_attributes_with_nested(const char* partition_name, const char* class_name, std::set<std::string>& data)
{
  std::map<std::string, pbeast::server_var> server_objects;
  get_servers(partition_name, server_objects);

    {
      struct task
      {
        static void
        process_class(std::set<std::string>& data, const char * partition_name, const char * class_name, const std::string& prefix, std::mutex& mutex, const std::string& server_name, pbeast::server_var server)
        {
          pbeast::StringDataPointArrayList_var attributes;

          try
            {
              server->get_attributes(partition_name, class_name, attributes);
            }
          catch (CORBA::SystemException& ex)
            {
              ers::error(daq::pbeast::CorbaError( ERS_HERE, &ex, make_method_params_name(server_name, "get_attributes", partition_name, class_name) ));
              throw;
            }

          std::set<std::string> attr_names;

          for (unsigned int idx = 0; idx < attributes->length(); idx++)
            {
              for (unsigned int j = 0; j < attributes[idx].data.length(); j++)
                {
                  attr_names.insert(static_cast<const char *>(attributes[idx].data[j]));
                }
            }

          for (const auto& a : attr_names)
            {
              pbeast::AttributeTypeList_var types;

              try
                {
                  server->get_attribute_type(partition_name, class_name, a.c_str(), types);
                }
              catch (CORBA::SystemException& ex)
                {
                  ers::error(daq::pbeast::CorbaError( ERS_HERE, &ex, make_method_params_name(server_name, "get_attribute_type", partition_name, class_name) ));
                  throw;
                }

              for (unsigned int idx = 0; idx < types->length(); idx++)
                {
                  for (unsigned int idx = 0; idx < types->length(); idx++)
                    {
                      if(pbeast::str2type(static_cast<const char *>(types[idx].type)) == pbeast::OBJECT)
                        {
                          process_class(data, partition_name, static_cast<const char *>(types[idx].type), prefix + a + '/' + static_cast<const char *>(types[idx].type) + '/', mutex, server_name, server);
                        }
                      else
                        {
                          std::lock_guard<std::mutex> lock(mutex);
                          data.insert(prefix + a);
                        }
                    }
                }
            }
        }

        static void
        run(std::mutex& mutex, std::set<std::string>& data, const char * partition_name, const char * class_name, Issues& errors, const std::string& server_name, pbeast::server_var server)
        {
          try
            {
              process_class(data, partition_name, class_name, "", mutex, server_name, server);
            }
          catch (const pbeast::NotFound& ex)
            {
              const std::string method(GET_ATTRIBUTES_WITH_NESTED_PARAMS);
              errors.add(method, ex);
              ERS_DEBUG( 1, "method " << method << " caused pbeast::NotFound exception " << ex.text);
            }
          catch (const pbeast::Failure& ex)
            {
              const std::string method(GET_ATTRIBUTES_WITH_NESTED_PARAMS);
              errors.add(method, ex);
              ers::error(daq::pbeast::MethodFailure( ERS_HERE, method, ex.text ));
            }
          catch (const pbeast::BadRequest& ex)
            {
              const std::string method(GET_ATTRIBUTES_WITH_NESTED_PARAMS);
              errors.add(method, ex);
              ERS_DEBUG( 1, "method " << method << " caused pbeast::BadRequest exception " << ex.text);
            }
          catch (const CORBA::SystemException& ex)
            {
              errors.add(GET_ATTRIBUTES_WITH_NESTED_PARAMS, ex);
            }
        }
      };

      std::mutex mutex;
      std::vector<std::future<void> > results;
      Issues issues;

      for (auto &i : server_objects)
        results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(data), partition_name, class_name, std::ref(issues), std::ref(i.first), i.second)));

      for (unsigned int i = 0; i < results.size(); ++i)
        results[i].get();

      issues.validate("get_attributes", data.empty());
    }

}


void
pbeast::Server::get_classes(const char* partition_name, std::set<std::string>& data)
{
  std::map<std::string, pbeast::server_var> server_objects;
  get_servers(partition_name, server_objects);

    {
      struct task
      {
        static void
        run(std::mutex& mutex, std::set<std::string>& data, const char * partition_name, Issues& errors, const std::string& server_name, pbeast::server_var server)
        {
          try
            {
              pbeast::StringArray_var classes;
              server->get_classes(partition_name, classes);

              std::lock_guard<std::mutex> lock(mutex);

              for (unsigned int idx = 0; idx < classes->length(); idx++)
                {
                  data.insert(static_cast<const char *>(classes[idx]));
                }
            }

          CATCH_SERVER_EXCEPTIONS(make_method_params_name(server_name, "get_classes", partition_name))

        }
      };

      std::mutex mutex;
      std::vector<std::future<void> > results;
      Issues issues;

      for (auto &i : server_objects)
        results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(data), partition_name, std::ref(issues), std::ref(i.first), i.second)));

      for (unsigned int i = 0; i < results.size(); ++i)
        results[i].get();

      issues.validate("get_classes", data.empty());
    }
}


void
pbeast::Server::get_partitions(std::set<std::string>& data)
{
  static std::string empty;

  std::map<std::string, pbeast::server_var> server_objects;
  get_servers(empty, server_objects);

  {
    struct task
    {
      static void
      run(std::mutex& mutex, std::set<std::string>& data, Issues& errors, const std::string& server_name, pbeast::server_var server)
      {
        try
          {
            pbeast::StringArray_var partitions;
            server->get_partitions(partitions);

            std::lock_guard<std::mutex> lock(mutex);

            for (unsigned int idx = 0; idx < partitions->length(); idx++)
              {
                data.insert(static_cast<const char *>(partitions[idx]));
              }
          }

        CATCH_SERVER_EXCEPTIONS(make_method_params_name(server_name, "get_partitions"))

      }
    };

    std::mutex mutex;
    std::vector<std::future<void> > results;
    Issues issues;

    for (auto &i : server_objects)
      results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(data), std::ref(issues), std::ref(i.first), i.second)));

    for (unsigned int i = 0; i < results.size(); ++i)
      results[i].get();

    issues.validate("get_partitions", data.empty());
  }
}


void
pbeast::Server::get_attribute_type(const char* partition_name, const char* class_name, const char* attribute_name_path, std::vector<AttrInfoData>& data)
{
  std::map<std::string, pbeast::server_var> server_objects;
  get_servers(partition_name, server_objects);

  std::map<uint64_t, std::pair<std::string, bool> > attr_data;

    {
      struct task
      {
        static void
        run(std::mutex& mutex, std::map<uint64_t, std::pair<std::string, bool>>& attr_data, const char * partition_name, const char * class_name, const char* attribute_name_path, Issues& errors, const std::string& server_name, pbeast::server_var server)
        {
          try
            {
              pbeast::AttributeTypeList_var types;
              server->get_attribute_type(partition_name, class_name, attribute_name_path, types);

              std::lock_guard<std::mutex> lock(mutex);

              for (unsigned int idx = 0; idx < types->length(); idx++)
                {
                  attr_data[types[idx].ts.created] = std::make_pair(types[idx].type,types[idx].is_array);
                  if(types[idx].ts.created != types[idx].ts.updated)
                    {
                      attr_data[types[idx].ts.updated] = std::make_pair(types[idx].type,types[idx].is_array);
                    }
                }
            }

          CATCH_SERVER_EXCEPTIONS(make_method_params_name(server_name, "get_attribute_type", partition_name, class_name, attribute_name_path))

        }
      };

      std::mutex mutex;
      std::vector<std::future<void> > results;
      Issues issues;

      for (auto &i : server_objects)
        results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(attr_data), partition_name, class_name, attribute_name_path, std::ref(issues), std::ref(i.first), i.second)));

      for (unsigned int i = 0; i < results.size(); ++i)
        results[i].get();

      issues.validate("get_attribute_type", attr_data.empty());
    }

  // fill type and is-array data
  for (auto & x : attr_data)
    {
      if (data.empty())
        {
          data.emplace_back(x.first, x.first, x.second.first, x.second.second);
        }
      else
        {
          AttrInfoData& last(data.back());
          if (x.second.first == last.m_type && x.second.second == last.m_is_array)
            {
              last.m_updated = x.first;
            }
          else
            {
              data.emplace_back(x.first, x.first, x.second.first, x.second.second);
            }
        }
    }
}

std::map<std::string, pbeast::srv::ClassInfo>
pbeast::Server::get_schema(const char* partition_name)
{
  std::map<std::string, pbeast::server_var> server_objects;
  get_servers(partition_name, server_objects);

  std::map<std::string, pbeast::srv::ClassInfo> data;

  {
    struct task
    {
      static void
      run(std::mutex& mutex, std::map<std::string, pbeast::srv::ClassInfo>& data, const char * partition_name, Issues& errors, const std::string& server_name, pbeast::server_var server)
      {
        try
          {
            pbeast::ClassInfoList_var class_info;
            server->get_schema(partition_name, class_info);

            std::lock_guard<std::mutex> lock(mutex);

            for (unsigned int idx = 0; idx < class_info->length(); idx++)
              {
                pbeast::srv::ClassInfo& info = data[static_cast<const char *>(class_info[idx].name)];

                if (info.m_ts < class_info[idx].ts)
                  {
                    info.m_ts = class_info[idx].ts;

                    info.m_attributes.clear();
                    for (unsigned int j = 0; j < class_info[idx].attributes.length(); j++)
                      info.m_attributes.try_emplace(static_cast<const char *>(class_info[idx].attributes[j].name), static_cast<const char *>(class_info[idx].attributes[j].type), static_cast<const char *>(class_info[idx].attributes[j].description), class_info[idx].attributes[j].is_array);
                  }
              }
          }

        CATCH_SERVER_EXCEPTIONS(make_method_params_name(server_name, "get_schema", partition_name))

      }
    };

    std::mutex mutex;
    std::vector<std::future<void> > results;
    Issues issues;

    for (auto &i : server_objects)
      results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(data), partition_name, std::ref(issues), std::ref(i.first), i.second)));

    for (unsigned int i = 0; i < results.size(); ++i)
      results[i].get();

    issues.validate("get_schema", data.empty());
  }

  return data;
}


float
pbeast::get_interval(const std::chrono::time_point<std::chrono::steady_clock>& t1)
{
  return std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - t1).count() / 1000.;
}

static std::string
mk_verbose_report(const std::string &server, float interval, uint64_t size)
{
  std::ostringstream text;

  text << "read " << size << " data series from " << server << " in " << interval << " ms";

  return text.str();
}

template<class T, class V>
  void
  read_all_common(std::mutex &mutex, const std::chrono::time_point<std::chrono::steady_clock> &tm, const std::string &server_name, uint64_t data_size, T &eov_values, T &upd_values,
                  std::map<std::string, std::set<V>> &eov_data, std::map<std::string, std::set<V>> &upd_data, const char *fqn_prefix, std::string &verbose_out)
  {
    if (eov_values->length())
      {
        std::lock_guard<std::mutex> lock(mutex);
        pbeast::cvt_void(eov_values, eov_data, fqn_prefix);
      }

    if (upd_values->length())
      {
        std::lock_guard<std::mutex> lock(mutex);
        pbeast::cvt_void(upd_values, upd_data, fqn_prefix);
      }

    auto duration = pbeast::get_interval(tm);

    if (ers::debug_level() > 0 || duration > pbeast::verbose_report_duration_threshold_const)
      {
        std::lock_guard<std::mutex> lock(mutex);
        pbeast::utils::add_verbose(verbose_out, mk_verbose_report(server_name, duration, data_size));
      }
  }

/// merge equal data, which may come from different sources

template<typename T>
  static void
  merge_equal(std::map<std::string, std::set<T>> &data, const std::map<std::string, std::set<uint64_t>> &eov_data)
  {
    for (auto& x : data)
      {
        std::set<T>& to = x.second;

        if (to.size() < 2)
          continue;

        typename std::set<T>::iterator i = to.begin();

        typename T::data_type val = (*i).get_value();
        ++i;

        while (i != to.end())
          {
            if ((*i).get_value() != val)
              {
                val = (*i).get_value();
                ++i;
              }
            else
              {
                // p points to previous value
                auto p = std::prev(i);

                std::map<std::string, std::set<uint64_t>>::const_iterator eov_ref = eov_data.find(x.first);

                if (eov_ref != eov_data.end())
                  {
                    std::set<uint64_t>::const_iterator eov_it = (*eov_ref).second.lower_bound((*p).get_ts_last_updated());

                    // EoV splits two intervals; they are justified
                    if (eov_it != (*eov_ref).second.end() && (*eov_it) < (*i).get_ts_created())
                      {
                        ++i;
                        continue;
                      }
                  }

                // Merge intervals
                if ((*p).get_ts_last_updated() < (*i).get_ts_last_updated())
                  {
                    const_cast<T*>(&(*p))->set_ts_last_updated((*i).get_ts_last_updated());
                  }
                i = to.erase(i);
              }
          }
      }
  }



/// trim extra data but one outside query interval, which may come from different sources

template<typename K>
  static void
  trim_void_edges(std::map<std::string, std::set<K>> &data, K since, K until)
  {
    for (auto& x : data)
      {
        while (x.second.size() > 1)
          {
            const auto first = x.second.begin();
            const auto next = std::next(first);
            if (*next < since)
              x.second.erase(first);
            else
              break;
          }

        while (x.second.size() > 1)
          {
            const auto last = x.second.rbegin();
            const auto previous = std::next(last);
            if (*previous > until)
              x.second.erase(previous.base());
            else
              break;
          }
      }
  }

template<typename T>
  static void
  trim_edges(std::map<std::string, std::set<T>> &data, std::map<std::string, std::set<uint64_t>> &eov_data, std::map<std::string, std::set<uint64_t>> &upd_data, uint64_t since, uint64_t until)
  {
    for (auto& x : data)
      {
        while (x.second.size() > 1)
          {
            const auto first = x.second.begin();
            const auto next = std::next(first);
            if (next->get_ts_last_updated() < since)
              x.second.erase(first);
            else
              break;
          }

        while (x.second.size() > 1)
          {
            const auto last = x.second.rbegin();
            const auto previous = std::next(last);
            if (previous->get_ts_created() > until)
              x.second.erase(previous.base());
            else
              break;
          }
      }

    trim_void_edges(eov_data, since, until);
    trim_void_edges(upd_data, since, until);
  }

template<typename T>
  static void
  trim_edges(std::map<std::string, std::set<T>> &data, std::map<std::string, std::set<uint32_t>> &eov_data, std::map<std::string, std::set<uint32_t>> &upd_data, uint32_t since, uint32_t until)
  {
    for (auto& x : data)
      {
        while (x.second.size() > 1)
          {
            const auto first = x.second.begin();
            const auto next = std::next(first);
            if (next->get_ts() < since)
              x.second.erase(first);
            else
              break;
          }

        while (x.second.size() > 1)
          {
            const auto last = x.second.rbegin();
            const auto previous = std::next(last);
            if (previous->get_ts() > until)
              x.second.erase(previous.base());
            else
              break;
          }
      }

    trim_void_edges(eov_data, since, until);
    trim_void_edges(upd_data, since, until);
  }

#define READ_ALL( LIST_TYPE, FUNC, CPP_TYPE, SERIE )                                                                                                                                    \
std::map<std::string, pbeast::server_var> server_objects;                                                                                                                               \
get_servers(partition_name, server_objects);                                                                                                                                            \
std::string verbose;                                                                                                                                                                    \
                                                                                                                                                                                        \
  {                                                                                                                                                                                     \
    struct task                                                                                                                                                                         \
    {                                                                                                                                                                                   \
      static void                                                                                                                                                                       \
      run(std::mutex& mutex, std::map<std::string,std::set<pbeast::SERIE<CPP_TYPE>>>& data, std::map<std::string, std::set<uint64_t>>& eov_data,                                        \
          std::map<std::string,std::set<uint64_t>>& upd_data, const char * partition_name, const char * class_name, const char* attribute_name, uint64_t since, uint64_t until,         \
          uint64_t prev_interval, uint64_t next_interval, const char* object_mask, bool is_regexp, bool fill_upd, Issues& errors, const std::string& server_name,                       \
          pbeast::server_var server, const char * fqn_prefix, std::string& verbose_out)                                                                                                 \
      {                                                                                                                                                                                 \
        try                                                                                                                                                                             \
          {                                                                                                                                                                             \
            auto tm = std::chrono::steady_clock::now();                                                                                                                                 \
                                                                                                                                                                                        \
            pbeast::LIST_TYPE##_var values;                                                                                                                                             \
            pbeast::V64ObjectList_var eov_values, upd_values;                                                                                                                           \
            server->FUNC(partition_name, class_name, attribute_name, since, until, prev_interval, next_interval, object_mask, is_regexp, "", fill_upd, eov_values, upd_values, values); \
                                                                                                                                                                                        \
            if (values->length())                                                                                                                                                       \
              {                                                                                                                                                                         \
                std::lock_guard<std::mutex> lock(mutex);                                                                                                                                \
                pbeast::convert(values.in(), data, fqn_prefix);                                                                                                                         \
              }                                                                                                                                                                         \
                                                                                                                                                                                        \
            read_all_common<pbeast::V64ObjectList_var, uint64_t>(mutex, tm, server_name, values->length(), eov_values, upd_values, eov_data, upd_data, fqn_prefix, verbose_out);        \
          }                                                                                                                                                                             \
                                                                                                                                                                                        \
        CATCH_SERVER_EXCEPTIONS(METHOD(#FUNC, object_mask))                                                                                                                             \
      }                                                                                                                                                                                 \
    };                                                                                                                                                                                  \
                                                                                                                                                                                        \
    std::mutex mutex;                                                                                                                                                                   \
    std::vector<std::future<void>> results;                                                                                                                                             \
    Issues issues;                                                                                                                                                                      \
                                                                                                                                                                                        \
    bool get_upd = (fill_upd || fc.m_use_summarize);                                                                                                                                    \
                                                                                                                                                                                        \
    for (auto &i : server_objects)                                                                                                                                                      \
      results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(data), std::ref(eov), std::ref(upd), partition_name, class_name, attribute_name,          \
          since, until, prev_interval, next_interval, object_mask, is_regexp, get_upd, std::ref(issues), std::ref(i.first), i.second, fqn_prefix, std::ref(verbose))));                 \
                                                                                                                                                                                        \
    for (unsigned int i = 0; i < results.size(); ++i)                                                                                                                                   \
      results[i].get();                                                                                                                                                                 \
                                                                                                                                                                                        \
    issues.validate(#FUNC, data.empty(), results.size());                                                                                                                               \
                                                                                                                                                                                        \
    merge_equal(data, eov);                                                                                                                                                             \
    trim_edges(data, eov, upd, since, until);                                                                                                                                           \
    pbeast::apply_functions(data, eov, upd, fc, since, until, 0, verbose);                                                                                                              \
  }                                                                                                                                                                                     \
                                                                                                                                                                                        \
  return verbose;


#define READ_SIMPLE_MANY(LIST_TYPE, FUNC, CPP_TYPE)  \
  READ_ALL( LIST_TYPE, FUNC, CPP_TYPE, SeriesData )

#define READ_VECTOR_MANY(LIST_TYPE, FUNC, CPP_TYPE)  \
  READ_ALL( LIST_TYPE, FUNC, CPP_TYPE, SeriesVectorData )


std::string
pbeast::Server::get_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<bool>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(BooleanObjectList, get_boolean_attributes, bool)
}

std::string
pbeast::Server::get_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<int8_t>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(S8ObjectList, get_s8_attributes, int8_t)
}

std::string
pbeast::Server::get_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<uint8_t>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(U8ObjectList, get_u8_attributes, uint8_t)
}

std::string
pbeast::Server::get_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<int16_t>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(S16ObjectList, get_s16_attributes, int16_t)
}

std::string
pbeast::Server::get_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<uint16_t>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(U16ObjectList, get_u16_attributes, uint16_t)
}

std::string
pbeast::Server::get_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<int32_t>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(S32ObjectList, get_s32_attributes, int32_t)
}

std::string
pbeast::Server::get_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<uint32_t>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(U32ObjectList, get_u32_attributes, uint32_t)
}

std::string
pbeast::Server::get_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<int64_t>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(S64ObjectList, get_s64_attributes, int64_t)
}

std::string
pbeast::Server::get_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<uint64_t>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(U64ObjectList, get_u64_attributes, uint64_t)
}

std::string
pbeast::Server::get_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<float>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(FloatObjectList, get_float_attributes, float)
}

std::string
pbeast::Server::get_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<double>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(DoubleObjectList, get_double_attributes, double)
}

std::string
pbeast::Server::get_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesData<std::string>>>& data, const char * fqn_prefix)
{
  READ_SIMPLE_MANY(StringObjectList, get_string_attributes, std::string)
}

std::string
pbeast::Server::get_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<bool>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(BooleanArrayObjectList, get_boolean_array_attributes, bool)
}

std::string
pbeast::Server::get_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<int8_t>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(S8ArrayObjectList, get_s8_array_attributes, int8_t)
}

std::string
pbeast::Server::get_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<uint8_t>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(U8ArrayObjectList, get_u8_array_attributes, uint8_t)
}

std::string
pbeast::Server::get_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<int16_t>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(S16ArrayObjectList, get_s16_array_attributes, int16_t)
}

std::string
pbeast::Server::get_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<uint16_t>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(U16ArrayObjectList, get_u16_array_attributes, uint16_t)
}

std::string
pbeast::Server::get_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<int32_t>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(S32ArrayObjectList, get_s32_array_attributes, int32_t)
}

std::string
pbeast::Server::get_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<uint32_t>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(U32ArrayObjectList, get_u32_array_attributes, uint32_t)
}

std::string
pbeast::Server::get_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<int64_t>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(S64ArrayObjectList, get_s64_array_attributes, int64_t)
}

std::string
pbeast::Server::get_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<uint64_t>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(U64ArrayObjectList, get_u64_array_attributes, uint64_t)
}

std::string
pbeast::Server::get_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<float>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(FloatArrayObjectList, get_float_array_attributes, float)
}

std::string
pbeast::Server::get_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<double>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(DoubleArrayObjectList, get_double_array_attributes, double)
}

std::string
pbeast::Server::get_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint64_t>>& eov, std::map<std::string, std::set<uint64_t>>& upd, std::map<std::string,std::set<pbeast::SeriesVectorData<std::string>>>& data, const char * fqn_prefix)
{
  READ_VECTOR_MANY(StringArrayObjectList, get_string_array_attributes, std::string)
}

static void
get_min_ts(std::map<std::string, std::set<uint32_t>> &vdata, uint32_t &ts)
{
  for (auto &x : vdata)
    if (!x.second.empty())
      {
        const uint32_t v = *x.second.begin();
        if (v < ts)
          ts = v;
      }
}

static void
get_max_ts(std::map<std::string, std::set<uint32_t>> &vdata, uint32_t &ts)
{
  for (auto &x : vdata)
    if (!x.second.empty())
      {
        const uint32_t v = *x.second.rbegin();
        if (v > ts)
          ts = v;
      }
}

template<class T>
static uint32_t
get_min_ds_ts(std::map<std::string, std::set<T>> &data, std::map<std::string, std::set<uint32_t>> &eovs, std::map<std::string, std::set<uint32_t>> &upds, uint32_t since, uint32_t sample_interval)
{
  uint32_t min_ts(since);

  if (min_ts != 0)
    {
      min_ts = pbeast::mk_downsample_ts(since, sample_interval) + sample_interval / 2;
      if (min_ts > since && min_ts > sample_interval)
        min_ts -= sample_interval;
    }
  else
    {
      min_ts = std::numeric_limits<uint32_t>::max();

      get_min_ts(eovs, min_ts);

      for (const auto& x : data)
        if (!x.second.empty())
          {
            const uint32_t v = (*x.second.begin()).get_ts();
            if (v < min_ts)
              min_ts = v;
          }
    }

  return min_ts;
}


template<class T>
static uint32_t
get_max_ds_ts(std::map<std::string, std::set<T>> &data, std::map<std::string, std::set<uint32_t>> &eovs, std::map<std::string, std::set<uint32_t>> &upds, uint32_t until, uint32_t sample_interval)
{
  uint32_t max_ts(until);

  if (max_ts != std::numeric_limits<uint32_t>::max())
    {
      max_ts = pbeast::mk_downsample_ts(until, sample_interval) + sample_interval / 2;
      if (max_ts < until)
        max_ts += sample_interval;
    }
  else
    {
      max_ts = 0;

      get_max_ts(eovs, max_ts);

      for (const auto& x : data)
        {
          if (!x.second.empty())
            {
              const uint32_t v = (*x.second.rbegin()).get_ts();
              if (v > max_ts)
                max_ts = v;
            }
        }
    }

  return max_ts;
}

// remove all but one before since and after until

template<class T>
static void
trim_boundaries(std::map<std::string, std::set<T>> &data, std::map<std::string, std::set<uint32_t>> &eovs, std::map<std::string, std::set<uint32_t>> &upds, uint32_t since, uint32_t until)
{
// TO DO
}

template<class T>
  static void
  fill_all_gaps(std::map<std::string, std::set<T>> &data, std::map<std::string, std::set<uint32_t>> &eovs, std::map<std::string, std::set<uint32_t>> &upds, uint32_t since, uint32_t until, uint32_t sample_interval)
  {
    const uint32_t min_ts = get_min_ds_ts<T>(data, eovs, upds, since, sample_interval);
    const uint32_t max_ts = get_max_ds_ts<T>(data, eovs, upds, until, sample_interval);

    // calculate expected number of points
    const uint64_t num = (max_ts - min_ts) / sample_interval + 1;

    for (auto &x : data)
      {
        const std::set<uint32_t> *eov = nullptr;

          {
            auto it = pbeast::find(eovs, x.first);
            if (it != eovs.end())
              eov = &it->second;
          }

        uint32_t size = x.second.size();

        if (eov)
          size += eov->size();

        if (size != num || (*x.second.begin()).get_ts() != min_ts)
          {
            std::set<T> v;

            auto data_it = x.second.begin();

            std::set<uint32_t>::const_iterator eov_it;

            if (eov)
              eov_it = eov->begin();

            std::set<uint32_t> &upd = upds[pbeast::get_object_name(x.first)];
            upd.clear();

            for (uint32_t idx = min_ts; idx <= max_ts; idx += sample_interval)
              {
                while ((*data_it).get_ts() < idx && data_it != x.second.end())
                  data_it++;

                if (data_it != x.second.end())
                  {
                    if ((*data_it).get_ts() == idx)
                      {
                        v.emplace(*data_it);
                        upd.insert(idx);
                        data_it++;
                      }
                    else if (eov && eov_it != eov->end() && *eov_it == idx)
                      {
                        data_it++;
                      }
                    else
                      {
                        auto d = *data_it;
                        d.set_ts(idx);
                        v.emplace(d);
                        upd.insert(idx);
                      }
                  }
                else if (eov && eov_it != eov->end() && *eov_it == idx)
                  {
                    ;
                  }
                else
                  {
                    auto d = *x.second.rbegin();
                    d.set_ts(idx);
                    v.emplace(d);
                    upd.insert(idx);
                  }
              }

            x.second = v;
          }
      }
  }

static void
expand_eov(uint32_t sample_interval, uint32_t inserted, const std::set<uint32_t> &upd, std::vector<uint32_t>& to, uint32_t last)
{
  const uint32_t half_interval(sample_interval / 2);

  auto it = upd.upper_bound(inserted);
  if (it != upd.end() && *it < last)
    last = *it;


  inserted += sample_interval;
  while (inserted < last)
    {
      inserted = pbeast::mk_downsample_ts(inserted, sample_interval) + half_interval;
      to.push_back(inserted);
      inserted += sample_interval;
    }
}

static void
expand_eov(uint32_t sample_interval, std::map<std::string, std::set<uint32_t>> &eovs, const std::map<std::string, std::set<uint32_t>> &upds, uint32_t until)
{
  const uint32_t half_interval(sample_interval / 2);

  for (auto &x : eovs)
    {
      auto upd_it = pbeast::find(upds, x.first);

      if (upd_it != upds.end())
        {
          std::vector<uint32_t> expansion;

          auto it = x.second.begin();

          while (it != x.second.end())
            {
              uint32_t eov_cur = *it;
              uint32_t eov_next;

              if (++it != x.second.end())
                {
                  eov_next = *it;

                  if((eov_next - eov_cur) == sample_interval)
                    continue;
                }
              else
                {
                  eov_next = pbeast::mk_downsample_ts(until, sample_interval) + half_interval + 1; // +1 is needed to add last EoV at the edge
                }

              expand_eov(sample_interval, eov_cur, (*upd_it).second, expansion, eov_next);
            }

          for(auto& eov : expansion)
            x.second.insert(eov);
        }
    }
}



#define READ_DS_ALL( LIST_TYPE, FUNC, CPP_TYPE, SERIE )                                                                                                                                                      \
std::map<std::string, pbeast::server_var> server_objects;                                                                                                                                                    \
get_servers(partition_name, server_objects);                                                                                                                                                                 \
                                                                                                                                                                                                             \
  {                                                                                                                                                                                                          \
    struct task                                                                                                                                                                                              \
    {                                                                                                                                                                                                        \
      static void                                                                                                                                                                                            \
      run(std::mutex& mutex, std::map<std::string,std::set<pbeast::SERIE<CPP_TYPE>>>& data, std::map<std::string, std::set<uint32_t>>& eov_data,                                                             \
          std::map<std::string, std::set<uint32_t>>& upd_data, const char * partition_name, const char * class_name, const char* attribute_name,                                                             \
          uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, uint64_t interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp,                      \
          bool fill_upd, Issues& errors, const std::string& server_name, pbeast::server_var server, const char * fqn_prefix, std::string& verbose_out)                                                       \
      {                                                                                                                                                                                                      \
        try                                                                                                                                                                                                  \
          {                                                                                                                                                                                                  \
            auto tm = std::chrono::steady_clock::now();                                                                                                                                                      \
                                                                                                                                                                                                             \
            pbeast::LIST_TYPE##_var values;                                                                                                                                                                  \
            pbeast::V32ObjectList_var eov_values, upd_values;                                                                                                                                                \
            server->FUNC(partition_name, class_name, attribute_name, since, until, prev_interval, next_interval, interval, fill_gaps, object_mask, is_regexp, "", fill_upd, eov_values, upd_values, values); \
                                                                                                                                                                                                             \
            if (values->length())                                                                                                                                                                            \
              {                                                                                                                                                                                              \
                std::lock_guard<std::mutex> lock(mutex);                                                                                                                                                     \
                pbeast::convert(values.in(), data, fqn_prefix);                                                                                                                                              \
              }                                                                                                                                                                                              \
                                                                                                                                                                                                             \
            read_all_common<pbeast::V32ObjectList_var, uint32_t>(mutex, tm, server_name, values->length(), eov_values, upd_values, eov_data, upd_data, fqn_prefix, verbose_out);                             \
          }                                                                                                                                                                                                  \
                                                                                                                                                                                                             \
        CATCH_SERVER_EXCEPTIONS(METHOD(#FUNC, object_mask))                                                                                                                                                  \
                                                                                                                                                                                                             \
      }                                                                                                                                                                                                      \
    };                                                                                                                                                                                                       \
                                                                                                                                                                                                             \
    if (fill_gaps == pbeast::FillGaps::All)                                                                                                                                                                  \
      fill_upd = true;                                                                                                                                                                                       \
                                                                                                                                                                                                             \
    std::mutex mutex;                                                                                                                                                                                        \
    std::vector<std::future<void>> results;                                                                                                                                                                  \
    Issues issues;                                                                                                                                                                                           \
                                                                                                                                                                                                             \
    for (auto &i : server_objects)                                                                                                                                                                           \
      results.push_back(m_thread_pool.enqueue(std::bind(&task::run, std::ref(mutex), std::ref(data32), std::ref(eov32), std::ref(upd32), partition_name, class_name, attribute_name,                         \
          since, until, prev_interval, next_interval, interval, fill_gaps, object_mask, is_regexp, fill_upd, std::ref(issues), std::ref(i.first), i.second, fqn_prefix, std::ref(verbose))));                \
                                                                                                                                                                                                             \
    for (unsigned int i = 0; i < results.size(); ++i)                                                                                                                                                        \
      results[i].get();                                                                                                                                                                                      \
                                                                                                                                                                                                             \
    issues.validate(#FUNC, data32.empty(), results.size());                                                                                                                                                  \
                                                                                                                                                                                                             \
    expand_eov(interval, eov32, upd32, pbeast::ts2secs(until+next_interval));                                                                                                                                \
                                                                                                                                                                                                             \
    if (fill_gaps == pbeast::FillGaps::All)                                                                                                                                                                  \
      fill_all_gaps(data32, eov32, upd32, pbeast::ts2secs(since), pbeast::ts2secs(until), interval);                                                                                                         \
                                                                                                                                                                                                             \
    trim_edges(data32, eov32, upd32, pbeast::ts2secs(since), pbeast::ts2secs(until));                                                                                                                        \
    pbeast::apply_functions(data32, eov32, upd32, fc, since, until, interval, verbose);                                                                                                                      \
  }                                                                                                                                                                                                          \


template<class T>
static void
refresh_data_point(std::set<T> &data, typename std::set<T>::iterator& it, uint32_t ts)
{
  T v = *it;
  data.erase(it);
  v.set_ts(ts);
  data.insert(v);
}

static void
refresh_void(std::map<std::string, std::set<uint32_t>> &data, uint32_t min_ts, uint32_t max_ts)
{
  for (auto& x : data)
    if (!x.second.empty())
      {
        auto data_it = x.second.begin();

        if (*data_it < min_ts)
          {
            x.second.erase(data_it);
            x.second.insert(min_ts);
          }

        data_it = --x.second.end();

        if (*data_it > max_ts)
          {
            x.second.erase(data_it);
            x.second.insert(max_ts);
          }
      }
}

template<class T>
static void
adjust_ds_edges(std::map<std::string, std::set<T>> &data, std::map<std::string, std::set<uint32_t>> &eovs, std::map<std::string, std::set<uint32_t>> &upds, uint32_t since, uint32_t until, uint32_t sample_interval)
{
  const uint32_t min_ts = get_min_ds_ts<T>(data, eovs, upds, since, sample_interval);
  const uint32_t max_ts = get_max_ds_ts<T>(data, eovs, upds, until, sample_interval);

  for (auto& x : data)
    {
      if (!x.second.empty())
        {
          auto data_it = x.second.begin();

          if (data_it->get_ts() < min_ts)
            refresh_data_point<T>(x.second, data_it, min_ts);

          data_it = --x.second.end();

          if (data_it->get_ts() > max_ts)
            refresh_data_point<T>(x.second, data_it, max_ts);
        }
    }

  refresh_void(upds, min_ts, max_ts);
}


#define READ_DS_SIMPLE_MANY(LIST_TYPE, FUNC, CPP_TYPE)                                                                                                                            \
  std::string verbose;                                                                                                                                                            \
                                                                                                                                                                                  \
  if (fc.m_use_summarize)                                                                                                                                                         \
    {                                                                                                                                                                             \
      fc.set_interval(interval);                                                                                                                                                  \
      std::map<std::string, std::set<uint64_t>> upd, eov;                                                                                                                         \
      std::map<std::string,std::set<pbeast::SeriesData<CPP_TYPE>>> data;                                                                                                          \
      verbose = get_##FUNC(partition_name, class_name, attribute_name, since, until, prev_interval, next_interval, object_mask, is_regexp, fc, true, eov, upd, data, fqn_prefix); \
      pbeast::downsample_void(interval, upd, upd32);                                                                                                                              \
      pbeast::downsample_void(interval, eov, eov32);                                                                                                                              \
      pbeast::downsample(interval, data, data32, fill_gaps);                                                                                                                      \
    }                                                                                                                                                                             \
  else                                                                                                                                                                            \
    {                                                                                                                                                                             \
      READ_DS_ALL( LIST_TYPE, get_downsample_##FUNC, CPP_TYPE, DownsampledSeriesData )                                                                                            \
    }                                                                                                                                                                             \
                                                                                                                                                                                  \
  if (fc.no_aggregations() == false)                                                                                                                                              \
    {                                                                                                                                                                             \
      adjust_ds_edges(data32, eov32, upd32, pbeast::ts2secs(since), pbeast::ts2secs(until), interval);                                                                            \
      uint32_t first, last;                                                                                                                                                       \
      get_query_interval(data32, first, last);                                                                                                                                    \
      uint32_t len = (last - first) / interval + 1;                                                                                                                               \
      AggregatedPrimitiveData<CPP_TYPE> values(interval, first, len);                                                                                                             \
      values.process(data32, eov32);                                                                                                                                              \
      if (fc.m_keep_data == false)                                                                                                                                                \
        {                                                                                                                                                                         \
          data32.clear();                                                                                                                                                         \
          eov32.clear();                                                                                                                                                          \
          upd32.clear();                                                                                                                                                          \
        }                                                                                                                                                                         \
      values.store(fc, data32, since, until);                                                                                                                                     \
    }                                                                                                                                                                             \
                                                                                                                                                                                  \
  return verbose;


std::string
pbeast::Server::get_downsample_boolean_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<bool>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(BooleanDownSampleObjectList, boolean_attributes, bool)
}

std::string
pbeast::Server::get_downsample_s8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<int8_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(S8DownSampleObjectList, s8_attributes, int8_t)
}

std::string
pbeast::Server::get_downsample_u8_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<uint8_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(U8DownSampleObjectList, u8_attributes, uint8_t)
}

std::string
pbeast::Server::get_downsample_s16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<int16_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(S16DownSampleObjectList, s16_attributes, int16_t)
}

std::string
pbeast::Server::get_downsample_u16_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<uint16_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(U16DownSampleObjectList, u16_attributes, uint16_t)
}

std::string
pbeast::Server::get_downsample_s32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<int32_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(S32DownSampleObjectList, s32_attributes, int32_t)
}

std::string
pbeast::Server::get_downsample_u32_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<uint32_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(U32DownSampleObjectList, u32_attributes, uint32_t)
}

std::string
pbeast::Server::get_downsample_s64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<int64_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(S64DownSampleObjectList, s64_attributes, int64_t)
}

std::string
pbeast::Server::get_downsample_u64_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<uint64_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(U64DownSampleObjectList, u64_attributes, uint64_t)
}

std::string
pbeast::Server::get_downsample_float_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<float>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(FloatDownSampleObjectList, float_attributes, float)
}

std::string
pbeast::Server::get_downsample_double_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<double>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(DoubleDownSampleObjectList, double_attributes, double)
}

std::string
pbeast::Server::get_downsample_string_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesData<std::string>>>& data32, const char * fqn_prefix)
{
  READ_DS_SIMPLE_MANY(StringDownSampleObjectList, string_attributes, std::string)
}

#define READ_DS_VECTOR_MANY(LIST_TYPE, FUNC, CPP_TYPE)                                                                                                                            \
  std::string verbose;                                                                                                                                                            \
                                                                                                                                                                                  \
  if (fc.m_use_summarize)                                                                                                                                                         \
    {                                                                                                                                                                             \
      fc.set_interval(interval);                                                                                                                                                  \
      std::map<std::string, std::set<uint64_t>> upd, eov;                                                                                                                         \
      std::map<std::string,std::set<pbeast::SeriesVectorData<CPP_TYPE>>> data;                                                                                                    \
      verbose = get_##FUNC(partition_name, class_name, attribute_name, since, until, prev_interval, next_interval, object_mask, is_regexp, fc, true, eov, upd, data, fqn_prefix); \
      pbeast::downsample_void(interval, upd, upd32);                                                                                                                              \
      pbeast::downsample_void(interval, eov, eov32);                                                                                                                              \
      pbeast::downsample(interval, data, data32, fill_gaps);                                                                                                                      \
    }                                                                                                                                                                             \
  else                                                                                                                                                                            \
    {                                                                                                                                                                             \
      READ_DS_ALL( LIST_TYPE, get_downsample_##FUNC, CPP_TYPE, DownsampledSeriesVectorData )                                                                                      \
    }                                                                                                                                                                             \
                                                                                                                                                                                  \
  if (fc.no_aggregations() == false)                                                                                                                                              \
    {                                                                                                                                                                             \
      adjust_ds_edges(data32, eov32, upd32, pbeast::ts2secs(since), pbeast::ts2secs(until), interval);                                                                            \
      uint32_t first, last;                                                                                                                                                       \
      get_query_interval(data32, first, last);                                                                                                                                    \
      uint32_t len = (last - first) / interval + 1;                                                                                                                               \
      AggregatedVectorData<CPP_TYPE> values(interval, first, len);                                                                                                                \
      values.process(data32, eov32);                                                                                                                                              \
      if (fc.m_keep_data == false)                                                                                                                                                \
        {                                                                                                                                                                         \
          data32.clear();                                                                                                                                                         \
          eov32.clear();                                                                                                                                                          \
          upd32.clear();                                                                                                                                                          \
        }                                                                                                                                                                         \
      values.store(fc, data32, since, until);                                                                                                                                     \
    }                                                                                                                                                                             \
                                                                                                                                                                                  \
  return verbose;


std::string
pbeast::Server::get_downsample_boolean_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<bool>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(BooleanDownSampleArrayObjectList, boolean_array_attributes, bool)
}

std::string
pbeast::Server::get_downsample_s8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<int8_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(S8DownSampleArrayObjectList, s8_array_attributes, int8_t)
}

std::string
pbeast::Server::get_downsample_u8_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<uint8_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(U8DownSampleArrayObjectList, u8_array_attributes, uint8_t)
}

std::string
pbeast::Server::get_downsample_s16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<int16_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(S16DownSampleArrayObjectList, s16_array_attributes, int16_t)
}

std::string
pbeast::Server::get_downsample_u16_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<uint16_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(U16DownSampleArrayObjectList, u16_array_attributes, uint16_t)
}

std::string
pbeast::Server::get_downsample_s32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<int32_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(S32DownSampleArrayObjectList, s32_array_attributes, int32_t)
}

std::string
pbeast::Server::get_downsample_u32_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<uint32_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(U32DownSampleArrayObjectList, u32_array_attributes, uint32_t)
}

std::string
pbeast::Server::get_downsample_s64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<int64_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(S64DownSampleArrayObjectList, s64_array_attributes, int64_t)
}

std::string
pbeast::Server::get_downsample_u64_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<uint64_t>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(U64DownSampleArrayObjectList, u64_array_attributes, uint64_t)
}

std::string
pbeast::Server::get_downsample_float_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<float>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(FloatDownSampleArrayObjectList, float_array_attributes, float)
}

std::string
pbeast::Server::get_downsample_double_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<double>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(DoubleDownSampleArrayObjectList, double_array_attributes, double)
}

std::string
pbeast::Server::get_downsample_string_array_attributes(const char* partition_name, const char* class_name, const char* attribute_name, ::CORBA::ULongLong since, ::CORBA::ULongLong until, ::CORBA::ULongLong prev_interval, ::CORBA::ULongLong next_interval, ::CORBA::ULong interval, pbeast::FillGaps::Type fill_gaps, const char* object_mask, bool is_regexp, const pbeast::FunctionConfig& fc, bool fill_upd, std::map<std::string, std::set<uint32_t>>& eov32, std::map<std::string, std::set<uint32_t>>& upd32, std::map<std::string,std::set<pbeast::DownsampledSeriesVectorData<std::string>>>& data32, const char * fqn_prefix)
{
  READ_DS_VECTOR_MANY(StringDownSampleArrayObjectList, string_array_attributes, std::string)
}


void
pbeast::utils::throw_on_error(const std::string& value, char * sanity, const char * ftype)
{
  if (*sanity != 0 || errno == ERANGE)
    {
      std::ostringstream text;
      text << "failed to parse " << ftype << " value \'" << value << "\':";
      if (*sanity)
        text << " on unrecognized characters \'" << sanity << "\'";
      if (errno)
        {
          char buffer[1024];
          buffer[0] = 0;
          strerror_r(errno, buffer, 1024);
          errno = 0;
          text << " with code " << errno << ", reason = \'" << buffer << '\'';
        }
      errno = 0;
      throw std::runtime_error(text.str().c_str());
    }
}

double
pbeast::utils::str2double(const std::string& value)
{
  char * sanity;
  double v = strtod(value.c_str(), &sanity);

  throw_on_error(value, sanity, "double-precision floating-point");

  return v;
}

unsigned long
pbeast::utils::str2uint(const std::string& value)
{
  char * sanity;
  unsigned long v = strtoul(value.c_str(), &sanity, 0);

  throw_on_error(value, sanity, "unsigned integer");

  return v;
}


static constexpr char s_node_separator = '.';

void
pbeast::utils::create_alias_map(const pbeast::FunctionConfig& fc, const std::set<std::string>& names, std::map<std::string, std::string>& out)
{
  const char value_separator = pbeast::FunctionConfig::get_value_separator();

  for(const auto& x : names)
    out[x] = x;

  for (const auto& f : fc.m_aliases)
    {
      try
        {
          switch (f.first)
            {
              case pbeast::Alias:
                if (out.size() > 1)
                  {
                    std::ostringstream text;
                    text << "there is more than one object in query result: ";
                    bool is_first = true;
                    for(const auto& x : out)
                      {
                        if (is_first)
                          is_first = false;
                        else
                          text << ", ";
                        text << '\"' << x.first << '\"';
                      }
                    throw std::runtime_error(text.str().c_str());
                  }

                for (auto& x : out)
                  x.second = f.second;

                break;


              case pbeast::AliasByMetric:
                for (auto& x : out)
                  {
                    std::string::size_type pos = x.second.rfind(s_node_separator);
                    if (pos != std::string::npos)
                      x.second.erase(0, pos+1);
                  }

                break;

              case pbeast::AliasByNode: {
                std::vector<unsigned long> idxs;

                std::size_t start = 0, end = 0;
                while ((end = f.second.find(value_separator, start)) != std::string::npos)
                  {
                    idxs.push_back(str2uint(f.second.substr(start, end - start)));
                    start = end + 1;
                  }
                idxs.push_back(str2uint(f.second.substr(start)));

                for (auto& x : out)
                  {
                    std::string str;

                    for (auto& idx : idxs)
                      {
                        start = end = 0;
                        std::size_t i = 0;
                        while ((end = x.second.find(s_node_separator, start)) != std::string::npos)
                          {
                            if (i == idx)
                              break;
                            start = end + 1;
                            i++;
                          }

                        if (i == idx)
                          {
                            if (str.empty() == false)
                              str.push_back(s_node_separator);
                            str.append(x.second.substr(start, end - start));
                          }
                        else
                          {
                            std::ostringstream text;
                            text << "failed to find node " << idx << " in value \"" << x.second << "\" of \"" << x.first << "\"";
                            throw std::runtime_error(text.str().c_str());
                          }
                      }

                    x.second = str;
                  }

                break; }


              case pbeast::AliasSub: {
                std::string::size_type pos = f.second.find(value_separator);
                if(pos == std::string::npos)
                  {
                    std::ostringstream text;
                    text << "bad format of function parameter expecting \"regex-search" << value_separator << "replace-value\" format";
                    throw std::runtime_error(text.str().c_str());
                  }

                const std::string re_str(pbeast::DataFile::decode(f.second.substr(0, pos), '%'));
                const std::string sub_value(pbeast::DataFile::decode(f.second.substr(pos+1), '%'));

                std::unique_ptr<boost::regex> re;

                try
                  {
                    re.reset(new boost::regex(re_str));
                  }
                catch (const std::exception& ex)
                  {
                    throw daq::pbeast::RegularExpressionError(ERS_HERE, "cannot parse regular expression", re_str, ex);
                  }

                for (auto& x : out)
                  {
                    try
                      {
                        x.second = boost::regex_replace(x.second, *re, sub_value);
                      }
                    catch (const std::exception& ex)
                      {
                        std::ostringstream text;
                        text << "boost::regex_replace(" << re_str << "," << sub_value << ") failed for string ";
                        throw daq::pbeast::RegularExpressionError(ERS_HERE, text.str().c_str(), x.second, ex);
                      }
                  }

                break; }
            }
        }
      catch (const std::exception& ex)
        {
          std::ostringstream text;
          text << "function " << pbeast::FunctionConfig::function_name(f.first) << "(" << f.second << ") failed\n\twas caused by: " << ex.what();
          throw pbeast::BadRequest(text.str().c_str());
        }

      std::map<std::string,std::string> reverse_map;

      for (const auto& x : out)
        {
          auto res = reverse_map.emplace(x.second, x.first);
          if(!res.second)
            {
              std::ostringstream text;
              text << "the configuration of alias functions results conversion of \"" << x.first << "\" and \"" << (res.first)->second << "\" object names into " << x.second << " alias name";
              throw pbeast::BadRequest(text.str().c_str());
            }
        }
    }
}

template<typename T>
  void
  __cvt_names(std::map<std::string, std::set<T>>& data, const std::map<std::string, std::string>& cvt_map)
  {
    std::map<std::string, std::set<T>> new_data;

    for (auto& x : data)
      {
        auto it = cvt_map.find(x.first);
        if (it != cvt_map.end())
          new_data[it->second].swap(x.second);
      }

    data.swap(new_data);
  }

void
pbeast::utils::cvt_names(std::map<std::string,std::set<uint32_t>>& data, const std::map<std::string, std::string>& cvt_map)
{
  __cvt_names(data, cvt_map);
}

void
pbeast::utils::cvt_names(std::map<std::string,std::set<uint64_t>>& data, const std::map<std::string, std::string>& cvt_map)
{
  __cvt_names(data, cvt_map);
}

void
pbeast::utils::add_verbose(std::string &to, const std::string &what)
{
  if (!what.empty())
    {
      to.append("\n  ");
      to.append(what);
    }
}
