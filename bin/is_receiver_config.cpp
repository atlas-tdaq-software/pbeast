#include <pbeast/exceptions.h>

#include "is_receiver_config.h"

#include "pbeast/PBeastSubscriptionTypes.h"


static bool is_regexp(const std::string& s)
{
  for(const char& c : s) {
    if(!isalnum(c) && c != '_' && c != ' ' && c != ':' && c != '-') return true;
  }
  return false;
}


pbeast::receiver::Subscription::Subscription(const pbeast::PBeastSubscription& obj) :
    m_read_when_subscribe (obj.get_ReadWhenSubscribe()),
    m_logic               (ISCriteria::Logic::AND),
    m_include_sub_types   (false),
    m_exclude_this_type   (false)
{
  if (const pbeast::PBeastSubscriptionTypes * types_obj = obj.get_Types())
    {
      m_is_type = types_obj->get_Name();
      m_include_sub_types = types_obj->get_IncludeSubTypes();
      m_exclude_this_type = types_obj->get_ExcludeThisType();

      if (types_obj->get_CriteriaLogic() == "OR")
        {
          m_logic = ISCriteria::Logic::OR;
        }
    }

  m_is_info_names = obj.get_InfoNames();

  const std::vector<std::string>& servers = obj.get_Servers();

  for(auto &s : servers)
    {
      if (is_regexp(s))
        {
          try
            {
              boost::regex * re = new boost::regex(s);
              ERS_DEBUG(0, "create regular expression for \"" << s << '\"');
              m_server_reg_exps.push_back(re);
            }
          catch (std::exception& ex)
            {
              ers::error(daq::pbeast::RegularExpressionError(ERS_HERE, "bad regular expression", s, ex));
            }
        }
      else
        {
          m_server_names.insert(s);
        }
    }

}

pbeast::receiver::Subscription::~Subscription()
{
  for(auto &re : m_server_reg_exps) delete re;
}

bool pbeast::receiver::Subscription::match_server(const std::string& name) const
{
  // no reg-ex, no names = accept any name
  if(m_server_reg_exps.empty() && m_server_names.empty()) return true;

  // check by name
  if(m_server_names.find(name) != m_server_names.end()) return true;

  // check by reg-exp
  for (auto &re : m_server_reg_exps)
    {
      try
        {
          if (boost::regex_match(name, *re))
            return true;
        }
      catch (std::exception& ex)
        {
          ers::error(daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", name, ex));
        }
    }

  return false;
}


void pbeast::receiver::IS_Config::init(const pbeast::PBeastApplication& obj)
{
  Config::init(obj);

  m_is_servers_check_interval = obj.get_IS_ServersCheckInterval();

  const std::vector<const pbeast::PBeastSubscription*>& subscriptions = obj.get_Subscriptions();

  for(auto &s : subscriptions)
    {
      m_subscriptions.push_back(new Subscription(*s));
    }
}

pbeast::receiver::IS_Config::~IS_Config()
{
  for(auto &s : m_subscriptions) delete s;
}
