#include <pbeast/meta.h>

#include "receiver_partition.h"

#include "application_exceptions.h"
#include "web_receiver_class.h"
#include "web_receiver_config.h"
#include "web_receiver_data_provider.h"

#include "../src/gason.h"
#include "receiver_errors.h"

pbeast::receiver::Web_DataProvider::Web_DataProvider(const Web_Config& config, const std::filesystem::path& path, IPCPartition& partition, int thread_pool_size) :
    pbeast::receiver::DataProvider(config.get_name(), path, partition, thread_pool_size),
    m_config(config)
{
  const size_t connection_memory_limit(128 * 1024);
  const size_t connection_memory_increment(4 * 1024);

  if (m_config.get_http_thread_pool_size() == 0)
    {
      m_mhd = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION, m_config.get_http_port(), nullptr, nullptr, &answer_to_connection, nullptr,
          MHD_OPTION_CONNECTION_TIMEOUT, m_config.get_http_timeout(),
          MHD_OPTION_CONNECTION_MEMORY_LIMIT, connection_memory_limit,
          MHD_OPTION_CONNECTION_MEMORY_INCREMENT, connection_memory_increment,
          MHD_OPTION_NOTIFY_COMPLETED, &request_completed, NULL,
          MHD_OPTION_END);

      ERS_LOG("start http server in THREAD_PER_CONNECTION mode");
    }
  else
    {
      m_mhd = MHD_start_daemon(MHD_USE_SELECT_INTERNALLY, m_config.get_http_port(), nullptr, nullptr, &answer_to_connection, nullptr,
          MHD_OPTION_CONNECTION_TIMEOUT, m_config.get_http_timeout(),
          MHD_OPTION_THREAD_POOL_SIZE, m_config.get_http_thread_pool_size(),
          MHD_OPTION_CONNECTION_MEMORY_LIMIT, connection_memory_limit,
          MHD_OPTION_CONNECTION_MEMORY_INCREMENT, connection_memory_increment,
          MHD_OPTION_NOTIFY_COMPLETED, &request_completed, nullptr,
          MHD_OPTION_END);

      ERS_LOG("start http server in USE_SELECT_INTERNALLY mode using pool of " << m_config.get_http_thread_pool_size() << " threads");
    }

  ERS_LOG("http timeout: " << m_config.get_http_timeout() << ", memory per connection limit: " << connection_memory_limit);

  read_classes_from_meta(m_partition.name());
}

pbeast::receiver::Partition *
pbeast::receiver::Web_DataProvider::read_classes_from_meta(const std::string& partition_name)
{
  pbeast::DataFile::validate_parameters(partition_name, "", "");

  const std::filesystem::path path(m_repository_path / pbeast::DataFile::encode(partition_name));

  try
    {
      if (std::filesystem::exists(path) == false)
        return nullptr;

      if (std::filesystem::is_directory(path) == false)
        throw daq::pbeast::FileSystemPathIsNotDirectory(ERS_HERE, path);
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "access file system path", path, ex.code().message());
    }

  ERS_DEBUG (0, "process partition folder " << path);

  try
    {
      auto ts = std::chrono::steady_clock::now();

      std::ostringstream details;

      Partition *p = get_partition(partition_name);

      if (p == nullptr)
        p = define_partition(partition_name);

      try
        {
          const std::filesystem::directory_iterator end_iter;

          for (std::filesystem::directory_iterator ic(path); ic != end_iter; ++ic)
            {
              if (std::filesystem::is_directory(ic->status()))
                {
                  auto ts2 = std::chrono::steady_clock::now();
                  const std::string class_name = pbeast::DataFile::decode(ic->path().filename().string());
                  p->define_class(class_name, new Web_Class(class_name, partition_name, m_repository_path));
                  details << "  * read meta info for class \"" << class_name << "\" in "
                      << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now() - ts2).count() << " μs\n";
                }
            }
        }
      catch (ers::Issue &ex)
        {
          throw daq::pbeast::CannotReadMetaInformation(ERS_HERE, ex);
        }
      catch (std::exception &ex)
        {
          throw daq::pbeast::CannotReadMetaInformation(ERS_HERE, ex);
        }

      ERS_LOG(
          "read description of " << p->get_classes().size() << " classes in partition \"" << p->get_name() << "\" in " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-ts).count() << " μs:\n" << details.str());

      link_class_types(p);

      return p;
    }
  catch (std::filesystem::filesystem_error &ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "iterate path", m_repository_path, ex.code().message());
    }
  catch (...)
    {
      throw;
    }
}


pbeast::receiver::Web_DataProvider::~Web_DataProvider()
{
  ipc_withdraw();

  MHD_stop_daemon (m_mhd);

  save_all_data();
}

struct ContextInfo
{
  struct MHD_PostProcessor * m_postprocessor;
  std::string m_data;
  std::string m_error;
};


static void
put_encoded(std::string& out, const std::string& s)
{
  for (auto & c : s)
    {
      if (c == '&')
        out.append("&amp;");
      else if (c == '<')
        out.append("&lt;");
      else if (c == '>')
        out.append("&gt;");
      else
        out.push_back(c);
    }
}


void
pbeast::receiver::Web_DataProvider::request_completed(void * /*cls*/, struct MHD_Connection * /*connection*/, void **con_cls, enum MHD_RequestTerminationCode /*toe*/)
{
  if (nullptr == con_cls || nullptr == *con_cls)
    return;

  ContextInfo *con_info = reinterpret_cast<ContextInfo *>(*con_cls);

  if (nullptr != con_info->m_postprocessor)
    {
      MHD_destroy_post_processor(con_info->m_postprocessor);
      con_info->m_postprocessor = nullptr;
      delete con_info;
    }

  *con_cls = nullptr;
}

static MHD_Result
upload_get_data(void *cls, enum MHD_ValueKind /*kind*/, const char *key, const char *value)
{
  ContextInfo * con_info = reinterpret_cast<ContextInfo *>(cls);

  if (key == nullptr || *key == 0)
    {
      con_info->m_error = "empty key is not allowed";
      return MHD_NO;
    }

  if (value == nullptr || *value == 0)
    {
      con_info->m_error = "empty value is not allowed";
      return MHD_NO;
    }

  if (0 == strcmp(key, "data"))
    {
      con_info->m_data = value;
      return MHD_YES;
    }

  con_info->m_error = std::string("unexpected key \"") + key + "\"";

  return MHD_NO;
}

static MHD_Result
upload_post_data(void *coninfo_cls, enum MHD_ValueKind /*kind*/, const char *key, const char */*filename*/, const char */*content_type*/, const char */*transfer_encoding*/, const char *data, uint64_t /*off*/, size_t size)
{
  struct ContextInfo *con_info = reinterpret_cast<ContextInfo *>(coninfo_cls);

  if (0 != strcmp (key, "data"))
    {
      con_info->m_error = std::string("Wrong method key \"") + key + "\"";
      return MHD_NO;
    }

  if (size > 0)
    {
      con_info->m_data.append(data, size);
    }

  return MHD_YES;
}

static void
make_response(std::string& out, const std::string& title, const std::string& body, const char * pre_text = nullptr)
{
  out = std::string("<html><title>") + title + "</title><body>";

  if (pre_text)
    {
      out.append(pre_text);
      out.append("</pre></p>");
    }

  put_encoded(out, body);

  if (pre_text)
    out.append("</pre></p>");

  out += "</body></html>";
}

MHD_Result
pbeast::receiver::Web_DataProvider::answer_to_connection(void * /*cls*/, struct MHD_Connection *connection, const char *url, const char *method, const char * /*version*/, const char * upload_data, size_t * upload_data_size, void **con_cls)
{
  enum ConnectionType {
    GET,
    POST,
    PUT,
    OTHER
  } connection_type = (
      (0 == strcmp(method, MHD_HTTP_METHOD_GET))  ? GET  :
      (0 == strcmp(method, MHD_HTTP_METHOD_POST)) ? POST :
      (0 == strcmp(method, MHD_HTTP_METHOD_PUT))  ? PUT  :
      OTHER
  );

  if (connection_type == OTHER)
    {
      ERS_DEBUG( 1, "Reject request method \"" << method << "\" url: \"" << url << "\"" );
      *con_cls = nullptr;
      return MHD_NO;
    }

  if (nullptr == *con_cls)
    {
      ERS_DEBUG(2, "First call for method \"" << method << "\" url: \"" << url << "\"");

      ContextInfo * con_info = new ContextInfo();
      *con_cls = reinterpret_cast<void *>(con_info);

      if (connection_type == GET)
        {
          con_info->m_postprocessor = nullptr;
        }
      else
        {
          con_info->m_postprocessor = MHD_create_post_processor(connection, 64 * 1024, &upload_post_data, *con_cls);

          if (nullptr == con_info->m_postprocessor)
            {
              delete con_info;
              return MHD_NO;
            }
        }

      return MHD_YES;
    }

  std::string response_text;
  unsigned int status_code = 0;
  std::string method_name(url ? url : "");
  ContextInfo * con_info = reinterpret_cast<ContextInfo *>(*con_cls);

  if (connection_type == GET)
    {
      MHD_get_connection_values(connection, MHD_GET_ARGUMENT_KIND, upload_get_data, reinterpret_cast<void *>(*con_cls));
    }
  else if (0 != *upload_data_size)
    {
      MHD_post_process(con_info->m_postprocessor, upload_data, *upload_data_size);
      *upload_data_size = 0;
      return MHD_YES;
    }

  ERS_DEBUG( 1 , "call " << method << " \"" << method_name << "\" with parameter: " << con_info->m_data );

  if (con_info->m_error.empty() == false)
    {
      make_response(response_text, "Bad Request", con_info->m_error, "Bad User Request");
      status_code = MHD_HTTP_BAD_REQUEST;
    }
  else
    {
      try
        {
          boost::shared_lock<boost::shared_mutex> lock(pbeast::receiver::DataProvider::s_data_provider_mutex);

          std::string title = "Data Uploaded";
          std::string body;

          if (pbeast::receiver::DataProvider::s_data_provider)
            {
              static_cast<pbeast::receiver::Web_DataProvider *>(pbeast::receiver::DataProvider::s_data_provider)->process_http_request(method_name, con_info->m_data, title, body);
            }

          if (body.empty())
            {
              std::ostringstream text;
              text << "Uploaded " << con_info->m_data.size() << " bytes";
              body = text.str();
            }

          make_response(response_text, title, body);
          status_code = MHD_HTTP_OK;
        }
      catch (pbeast::Failure & ex)
        {
          make_response(response_text, "P-BEAST Failure", (const char *) ex.text, "A failure has occurred");
          status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
        }
      catch (pbeast::NotFound & ex)
        {
          make_response(response_text, "P-BEAST NotFound", (const char *) ex.text, "No data found");
          status_code = MHD_HTTP_NOT_FOUND;
        }
      catch (pbeast::BadRequest & ex)
        {
          make_response(response_text, "P-BEAST BadRequest", (const char *) ex.text, "Bad User Request");
          status_code = MHD_HTTP_BAD_REQUEST;
        }
      catch (const std::exception& ex)
        {
          make_response(response_text, "P-BEAST Error", (const char *) ex.what(), "An error has occurred");
          status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
        }
    }

  if (MHD_Response * response = MHD_create_response_from_buffer(response_text.length(), (void *) (response_text.c_str()), MHD_RESPMEM_MUST_COPY))
    {
      MHD_Result ret = MHD_queue_response(connection, status_code, response);
      MHD_destroy_response(response);
      return ret;
    }
  else
    {
      ers::error(daq::pbeast::FailedCreateHttpResponse(ERS_HERE, response_text.length()));
      return MHD_NO;
    }
}


void
pbeast::receiver::Web_DataProvider::process_http_request(std::string& name, const std::string& json, std::string& title, std::string& out)
{
  if(name == "/tdaq/pbeast/define")
    {
      define_classes(json);
    }
  else if(name == "/tdaq/pbeast/add")
    {
      update_objects(json);
    }
  else if(name == "/tdaq/pbeast/close")
    {
      close_objects(json);
    }
  else if(name == "/health")
    {
      title = "Test Result";
      out = "healthy";
    }
  else
    {
      std::ostringstream out;
      out << "the method \"" << name << "\" does not exist\n";
      throw pbeast::BadRequest(out.str().c_str());
    }
}



//static void
//print_json(JsonValue o, std::string prefix)
//{
//  switch (o.getTag())
//    {
//      case JSON_NUMBER:
//        std::cout << prefix << "number " << o.toDouble() << std::endl;
//        break;
//
//      case JSON_STRING:
//        std::cout << prefix << "string " << o.toString() << std::endl;
//        break;
//
//      case JSON_ARRAY:
//        std::cout << prefix << "array " << std::endl;
//        for (auto i : o)
//          {
//            print_json(i->value, prefix + "  ");
//          }
//        break;
//
//      case JSON_OBJECT:
//        std::cout << prefix << "object " << std::endl;
//        for (auto i : o)
//          {
//            std::cout << prefix << "key: \"" << i->key << '\"' << std::endl
//                      << prefix << "value:\n";
//            print_json(i->value, prefix + "  ");
//          }
//        break;
//
//      case JSON_TRUE:
//        std::cout << prefix << "true" << std::endl;
//        break;
//
//      case JSON_FALSE:
//        std::cout << prefix << "false" << std::endl;
//        break;
//
//      case JSON_NULL:
//        std::cout << prefix << "(null)" << std::endl;
//        break;
//    }
//}

/*
 * Structure of schema description
 * "partition" value is optional; if not defined, use default partition = "initial"
 *   [
 *     {
 *       "partition":"partition-X",
 *       "name":"class-A",
 *       "attributes":[
 *         {
 *           "name":"attribute-A1",
 *           "description":"description of attribute-A1",
 *           "type":"pbeast-data-type",
 *           "isArray":boolean
 *         },
 *         ...
 *         {
 *           ...
 *         }
 *       ]
 *     },
 *     ...
 *     {
 *       ...
 *     }
 *   ]
 */

void
pbeast::receiver::Web_DataProvider::define_classes(const std::string& json)
{
  char *endptr;
  JsonValue value;
  JsonAllocator allocator;
  char * source = const_cast<char *>(json.c_str());
  int status = jsonParse(source, &endptr, &value, allocator);
  if (status != JSON_OK)
    {
      std::ostringstream text;
      text << "failed to parse json: " << jsonStrError(status) << " at " << (endptr - source);
      throw pbeast::BadRequest(text.str().c_str());
    }

  pbeast::Meta::set_info_ts(pbeast::mk_ts(time(0), 0));

  try
    {
      if (value.getTag() != JSON_ARRAY)
        {
          throw std::runtime_error("top-level item is not an array");
        }

      unsigned int count(1);

      for (auto i : value)
        {
          std::string partition_name;

          if (i->value.getTag() != JSON_OBJECT)
            {
              std::ostringstream text;
              text << "item #" << count << " of top-level array is not an object";
              throw std::runtime_error(text.str().c_str());
            }

          std::string name;
          JsonValue attributes;

          for (auto j : i->value)
            {
              if (!strcmp(j->key, "name"))
                {
                  if (j->value.getTag() != JSON_STRING)
                    {
                      std::ostringstream text;
                      text << "the \"name\" of a class #" << count << " is not a string";
                      throw std::runtime_error(text.str().c_str());
                    }
                  name = j->value.toString();
                }
              else if (!strcmp(j->key, "partition"))
                {
                  if (j->value.getTag() != JSON_STRING)
                    {
                      std::ostringstream text;
                      text << "the \"partition\" of a class #" << count << " is not a string";
                      throw std::runtime_error(text.str().c_str());
                    }
                  partition_name = j->value.toString();
                }
              else if (!strcmp(j->key, "attributes"))
                {
                  if (j->value.getTag() != JSON_ARRAY)
                    {
                      std::ostringstream text;
                      text << "the \"attributes\" of a class #" << count << " is not an array";
                      throw std::runtime_error(text.str().c_str());
                    }
                  attributes = j->value;
                }
              else
                {
                  std::ostringstream text;
                  text << "class object has unexpected key \'" << j->key << "\'";
                  throw std::runtime_error(text.str().c_str());
                }
            }

          if (name.empty())
            {
              std::ostringstream text;
              text << "the \"name\" of a class #" << count << " is not set";
              throw std::runtime_error(text.str().c_str());
            }

          if (attributes.getTag() != JSON_ARRAY)
            {
              std::ostringstream text;
              text << "the \"attributes\" of a class \"" << name << "\" is not set";
              throw std::runtime_error(text.str().c_str());
            }

          Partition * partition = (partition_name.empty() ? m_default_partition : define_partition(partition_name));

          pbeast::receiver::Web_Class * old = static_cast<Web_Class *>(get_class(name, partition));
          Web_Class * c = new Web_Class(name, attributes);

          if (old)
            {
              if (*old == *c)
                {
                  ERS_LOG("definition of class " << name << " was not changed");
                  delete c;
                  c = nullptr;
                }
              else
                {
                  partition->define_class(name, c);
                  save_class(*partition, *old);
                  delete old;
                  ERS_LOG("re-set class " << name << " because it's definition was changed");
                }
            }
          else
            {
              partition->define_class(name, c);
            }

          if (c)
            try
              {
                check_directory(*partition, *c);
              }
            catch (const ers::Issue& ex)
              {
                std::ostringstream text;
                text << "failed to update meta information for class \"" << name << "\":\n" << ex;
                throw pbeast::Failure(text.str().c_str());
              }

          count++;
        }
    }
  catch (pbeast::Failure&)
    {
      throw;
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "failed to read json: " << ex.what();
      throw pbeast::BadRequest(text.str().c_str());
    }

}

/*
 * Structure of schema description
 * "partition" value is optional; if not defined, use default partition = "initial"
 *
 * Two possible formats:
 *
 * 1. Any attributes, access by name:
 * "id" and "ts" tags are obligatory; their order is fixed
 * attributes can be any defined in class
 * also including above "id" and "ts" names!
 *
 * 2. "id", "ts" and all attributes as array
 *   [
 *     {
 *       "partition":"partition-Y",
 *       "name":"class-A",
 *       "objects":[
 *         {
 *           "id":"object-X",
 *           "ts":number-of-microseconds,
 *           "attribute-N":"string-value",
 *           "attribute-M":number,
 *           "attribute-array-L":["string-value-L1","string-value-L2",...,"string-value-Ln"],
 *           "attribute-array-K":[number-K1,number-K2,...,number-Km],
 *         },
 *         ...
 *         {
 *           ...
 *         }
 *       ]
 *     },
 *     {
 *       "partition":"partition-Z",
 *       "name":"class-B",
 *       "objects":[
 *         ["object-X",number-of-microseconds,"attribute1-string-value",attribute2-number,...,attribute-array-K":[number-K1,number-K2,...,number-Km]],
 *         ...
 *         [...]
 *       ]
 *     },
 *     ...
 *     {
 *       ...
 *     }
 *   ]
 */

void
pbeast::receiver::Web_DataProvider::update_objects(const std::string& json)
{
  char *endptr;
  JsonValue classes;
  JsonAllocator allocator;
  char * source = const_cast<char *>(json.c_str());
  int status = jsonParse(source, &endptr, &classes, allocator);
  if (status != JSON_OK)
    {
      std::ostringstream text;
      text << "failed to parse json: " << jsonStrError(status) << " at " << (endptr - source);
      throw pbeast::BadRequest(text.str().c_str());
    }

  try
    {
      if (classes.getTag() != JSON_ARRAY)
        {
          throw std::runtime_error("top-level item is not an array");
        }

      unsigned int count(1);

      for (auto i : classes)
        {
          if (i->value.getTag() != JSON_OBJECT)
            {
              std::ostringstream text;
              text << "item #" << count << " of top-level object is not an object";
              throw std::runtime_error(text.str().c_str());
            }

          std::string partition_name, name;
          JsonValue objects;

          for (auto j : i->value)
            {
              if (!strcmp(j->key, "name"))
                {
                  if (j->value.getTag() != JSON_STRING)
                    {
                      std::ostringstream text;
                      text << "the \"name\" of a class #" << count << " is not a string";
                      throw std::runtime_error(text.str().c_str());
                    }
                  name = j->value.toString();
                }
              else if (!strcmp(j->key, "partition"))
                {
                  if (j->value.getTag() != JSON_STRING)
                    {
                      std::ostringstream text;
                      text << "the \"partition\" of a class #" << count << " is not a string";
                      throw std::runtime_error(text.str().c_str());
                    }
                  partition_name = j->value.toString();
                }
              else if (!strcmp(j->key, "objects"))
                {
                  if (j->value.getTag() != JSON_ARRAY)
                    {
                      std::ostringstream text;
                      text << "the \"objects\" of a class #" << count << " is not an array";
                      throw std::runtime_error(text.str().c_str());
                    }
                  objects = j->value;
                }
              else
                {
                  std::ostringstream text;
                  text << "class object has unexpected key \'" << j->key << "\'";
                  throw std::runtime_error(text.str().c_str());
                }
            }

          if (name.empty())
            {
              std::ostringstream text;
              text << "the \"name\" of a class #" << count << " is not set";
              throw std::runtime_error(text.str().c_str());
            }

          if (objects.getTag() != JSON_ARRAY)
            {
              std::ostringstream text;
              text << "the \"objects\" of a class \"" << name << "\" is not set";
              throw std::runtime_error(text.str().c_str());
            }

          Partition * partition = (partition_name.empty() ? m_default_partition : get_partition(partition_name));

          if (partition == nullptr)
            try
              {
                partition = read_classes_from_meta(partition_name);
              }
            catch (const daq::pbeast::Exception& ex)
              {
                std::ostringstream text;
                text << "failed to read meta information for partition \"" << partition_name << "\":\n" << ex;
                throw pbeast::Failure(text.str().c_str());
              }

          if (partition == nullptr)
            {
              std::ostringstream text;
              text << "the partition \"" << partition_name << "\" is not defined";
              throw pbeast::NotFound(text.str().c_str());
            }

          pbeast::receiver::Web_Class * c = static_cast<Web_Class *>(get_class(name, partition));

          if (c == nullptr)
            {
              std::ostringstream text;
              text << "the class \"" << name << "\" is not defined in partition \"" << partition->get_name() << "\"";
              throw pbeast::NotFound(text.str().c_str());
            }

          unsigned int obj_count(1);

          for (auto i : objects)
            {
              if (i->value.getTag() == JSON_OBJECT)
                {
                  JsonIterator it = begin(i->value);

                  if (!(it != end(i->value)))
                    {
                      std::ostringstream text;
                      text << "the \"id\" attribute of item #" << obj_count << " of class \"" << name << "\" is not set";
                      throw std::runtime_error(text.str().c_str());
                    }

                  if (strcmp(it->key, "id"))
                    {
                      std::ostringstream text;
                      text << "the first attribute of item #" << obj_count << " of class \"" << name << "\" is not \"id\"";
                      throw std::runtime_error(text.str().c_str());
                    }

                  if (it->value.getTag() != JSON_STRING)
                    {
                      std::ostringstream text;
                      text << "the value of \"id\" attribute of item #" << obj_count << " of class \"" << name << "\" is not a string";
                      throw std::runtime_error(text.str().c_str());
                    }

                  std::string id = it->value.toString();

                  ++it;

                  if (!(it != end(i->value)))
                    {
                      std::ostringstream text;
                      text << "the \"ts\" attribute of object \"" << id << "\" of class \"" << name << "\" is not set";
                      throw std::runtime_error(text.str().c_str());
                    }

                  if (strcmp(it->key, "ts"))
                    {
                      std::ostringstream text;
                      text << "the second attribute of object \"" << id << "\" of class \"" << name << "\" is not \"ts\"";
                      throw std::runtime_error(text.str().c_str());
                    }

                  if (it->value.getTag() != JSON_NUMBER)
                    {
                      std::ostringstream text;
                      text << "the value of \"ts\" attribute of item #" << obj_count << " of class \"" << name << "\" is not a number";
                      throw std::runtime_error(text.str().c_str());
                    }

                  uint64_t timestamp = it->value.toULong();

                  ++it;

                  c->update_any_object(id, timestamp, it);
                }
              else if (i->value.getTag() == JSON_ARRAY)
                {
                  if (i->value.get_array_len() != (c->get_number_of_attributes() + 2))
                    {
                      std::ostringstream text;
                      text << "size of array of item #" << obj_count << " of class \"" << name << "\" is not equal to expected " << c->get_number_of_attributes() + 2;
                      throw std::runtime_error(text.str().c_str());
                    }

                  JsonIterator it = begin(i->value);

                  if (it->value.getTag() != JSON_STRING)
                    {
                      std::ostringstream text;
                      text << "the value of first element of array of item #" << obj_count << " of class \"" << name << "\" is not string (expected object id)";
                      throw std::runtime_error(text.str().c_str());
                    }

                  std::string id = it->value.toString();

                  ++it;

                  if (it->value.getTag() != JSON_NUMBER)
                    {
                      std::ostringstream text;
                      text << "the value of second element of array of object \"" << id << "\" of class \"" << name << "\" is not a number (expected timestamp)";
                      throw std::runtime_error(text.str().c_str());
                    }

                  uint64_t timestamp = it->value.toULong();

                  ++it;

                  c->update_full_object(id, timestamp, it);
                }
              else
                {
                  std::ostringstream text;
                  text << "item #" << obj_count << " of class \"" << name << "\" is not an object or an array";
                  throw std::runtime_error(text.str().c_str());
                }

              obj_count++;
            }

          count++;
        }
    }
  catch (pbeast::NotFound&)
    {
      throw;
    }
  catch (pbeast::Failure&)
    {
      throw;
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "failed to read json: " << ex.what();
      throw pbeast::BadRequest(text.str().c_str());
    }
}


/*
 * Structure of schema description
 *
 * "partition" value is optional; if not defined, use default partition = "initial"
 *
 *   {
 *     "ts":number-of-microseconds,
 *     "partition":"partition-X",
 *     "classes":[
 *       ["class-A",["object-A1",...,"object-AN"]],
 *       ...,
 *       [...]
 *     ]
 *   }
 */

void
pbeast::receiver::Web_DataProvider::close_objects(const std::string& json)
{
  char *endptr;
  JsonValue value;
  JsonAllocator allocator;
  char * source = const_cast<char *>(json.c_str());
  int status = jsonParse(source, &endptr, &value, allocator);
  if (status != JSON_OK)
    {
      std::ostringstream text;
      text << "failed to parse json: " << jsonStrError(status) << " at " << (endptr - source);
      throw pbeast::BadRequest(text.str().c_str());
    }

  try
    {
      if (value.getTag() != JSON_OBJECT)
        {
          throw std::runtime_error("top-level item is not an object");
        }

      JsonIterator it = begin(value);

      if (!(it != end(value)))
        {
          std::ostringstream text;
          text << "the \"ts\" attribute of top-level objects is not set";
          throw std::runtime_error(text.str().c_str());
        }

      if (strcmp(it->key, "ts"))
        {
          std::ostringstream text;
          text << "the first attribute of top-level objects is not \"ts\"";
          throw std::runtime_error(text.str().c_str());
        }

      if (it->value.getTag() != JSON_NUMBER)
        {
          std::ostringstream text;
          text << "the value of \"ts\" attribute of top-level objects is not an integer";
          throw std::runtime_error(text.str().c_str());
        }

      uint64_t timestamp = it->value.toULong();

      ++it;

      if (!(it != end(value)))
        {
          std::ostringstream text;
          text << "the \"classes\" or \"partition\" attribute of top-level objects is not set";
          throw std::runtime_error(text.str().c_str());
        }

      std::string partition_name;

      if (!strcmp(it->key, "partition"))
        {
          if (it->value.getTag() != JSON_STRING)
            {
              std::ostringstream text;
              text << "the value of \"partition\" attribute of top-level objects is not a string";
              throw std::runtime_error(text.str().c_str());
            }

          partition_name = it->value.toString();

          ++it;

          if (!(it != end(value)))
            {
              std::ostringstream text;
              text << "the \"classes\" attribute of top-level objects is not set";
              throw std::runtime_error(text.str().c_str());
            }
        }

      if (strcmp(it->key, "classes"))
        {
          std::ostringstream text;
          text << "the second attribute of top-level objects is not \"classes\"";
          throw std::runtime_error(text.str().c_str());
        }

      if (it->value.getTag() != JSON_ARRAY)
        {
          std::ostringstream text;
          text << "the value of \"classes\" attribute of top-level objects is not an array";
          throw std::runtime_error(text.str().c_str());
        }

      Partition * partition = (partition_name.empty() ? m_default_partition : get_partition(partition_name));

      if (partition == nullptr)
        {
          std::ostringstream text;
          text << "the partition \"" << partition_name << "\" is not defined";
          throw pbeast::NotFound(text.str().c_str());
        }

      unsigned int count(1);

      for (auto i : it->value)
        {
          if (i->value.getTag() == JSON_ARRAY)
            {
              JsonIterator it = begin(i->value);

              if (!(it != end(i->value)))
                {
                  std::ostringstream text;
                  text << "the first element of item #" << count << " of \"classes\" array of top-level objects is not set";
                  throw std::runtime_error(text.str().c_str());
                }

              if(it->value.getTag() != JSON_STRING)
                {
                  std::ostringstream text;
                  text << "the first element of item #" << count << " of \"classes\" array of top-level objects is not a string";
                  throw std::runtime_error(text.str().c_str());
                }

              pbeast::receiver::Web_Class * c = static_cast<Web_Class *>(get_class(it->value.toString(), partition));

              if (c == nullptr)
                {
                  std::ostringstream text;
                  text << "the class \"" << it->value.toString() << '@' << partition->get_name() << "\" is not found (item #" << count << " of \"classes\" array of top-level objects)";
                  throw pbeast::NotFound(text.str().c_str());
                }

              ++it;

              if (!(it != end(i->value)))
                {
                  std::ostringstream text;
                  text << "the second element of item #" << count << " of \"classes\" array of top-level objects is not set";
                  throw std::runtime_error(text.str().c_str());
                }

              if(it->value.getTag() != JSON_ARRAY)
                {
                  std::ostringstream text;
                  text << "the second element of item #" << count << " of \"classes\" array of top-level objects is not an array";
                  throw std::runtime_error(text.str().c_str());
                }

              c->close_objects(timestamp,it->value);

              count++;
            }
          else
            {
              std::ostringstream text;
              text << "the element #" << count << " of \"classes\" array of top-level objects is not an array";
              throw std::runtime_error(text.str().c_str());

            }
        }

    }
  catch (pbeast::NotFound&)
    {
      throw;
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "failed to read json: " << ex.what();
      throw pbeast::BadRequest(text.str().c_str());
    }


}
