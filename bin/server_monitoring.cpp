#include <config/Errors.h>

#include "application_exceptions.h"
#include "server_monitoring.h"


pbeast::srv::UserStat::UserStat(pbeast::PBeastServerUserMon * obj) :
  m_num_of_requests(0),
  m_size_of_data(0),
  m_max_size_of_data(0),
  m_processing_time(0),
  m_max_processing_time(0),
  m_obj(obj),
  m_last_updated(time(0)),
  m_last_non_null(m_last_updated)
{
}

pbeast::srv::Monitoring::Monitoring() :
  m_api(),
  m_rest(),
  m_corba(),
  m_mon_db("bstconfig")
{
}

bool
pbeast::srv::Monitoring::init_monitoring()
{
  try
    {
      if (m_mon_db.loaded() == false)
        {
          m_mon_db.load("initial");

          try
            {
              // read backup
              std::vector<const pbeast::PBeastServerUserMon*> objs;
              m_mon_db.get(objs);

              for (auto it : objs)
                {
                  std::string name = it->UID();

                  if (name.find(s_is_name_prefix) == 0)
                    {
                      name.erase(0, s_is_name_prefix.length());
                      if (name == s_is_api_name)
                        m_api.m_obj = const_cast<pbeast::PBeastServerUserMon*>(it);
                      else if (name == s_is_rest_name)
                        m_rest.m_obj = const_cast<pbeast::PBeastServerUserMon*>(it);
                      else if (name == s_is_corba_name)
                        m_corba.m_obj = const_cast<pbeast::PBeastServerUserMon*>(it);
                      else
                        {
                          std::unique_lock lock(m_mutex);
                          m_users.emplace(name, new UserStat(const_cast<pbeast::PBeastServerUserMon*>(it)));
                        }
                    }
                }
            }
          catch (daq::config::Exception &ex)
            {
              ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "access backup of", ex));
            }
        }
   }
  catch (daq::config::Exception &ex)
    {
      ers::warning(ex);
      return false;
    }

  return true;
}

std::string
pbeast::srv::UserStat::to_string(const char * prefix) const
{
  std::ostringstream s;

  if (m_obj && m_obj->get_total_num_of_requests())
    s << '\n'
      << prefix << "number of requests          : " << m_obj->get_total_num_of_requests() << '\n'
      << prefix << "total size of data          : " << m_obj->get_total_size_of_data() << '\n'
      << prefix << "max size of data            : " << m_obj->get_max_size_of_data() << '\n'
      << prefix << "total processing time       : " << m_obj->get_total_processing_time() << '\n'
      << prefix << "max request processing time : " << m_obj->get_max_processing_time();
  else
    s << " (null)";

  return s.str();
}


bool
pbeast::srv::Monitoring::publish_calls_stats(UserStat& stat, const std::string& name, uint64_t publication_interval, bool obvilion_flag)
{
  uint64_t num_of_requests = stat.m_num_of_requests.exchange(0);
  uint64_t size_of_data = stat.m_size_of_data.exchange(0);
  uint64_t max_size_of_data = stat.m_max_size_of_data.exchange(0);
  uint64_t processing_time = stat.m_processing_time.exchange(0);
  uint64_t max_processing_time = stat.m_max_processing_time.exchange(0);

  const auto now = time(0);

  if (!stat.m_obj)
    try
      {
        stat.m_obj = const_cast<pbeast::PBeastServerUserMon*>(m_mon_db.create<pbeast::PBeastServerUserMon>("", s_is_name_prefix + name, true));
      }
    catch (const daq::config::Exception &ex)
      {
        ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "create", ex));
        return true;
      }

  bool need_to_publish = false;

  if (num_of_requests)
    {
      stat.m_obj->set_num_of_requests(average_rate(num_of_requests, publication_interval));
      stat.m_obj->set_average_size_of_data_per_inteval(average_size(size_of_data, num_of_requests));
      stat.m_obj->set_max_size_of_data_per_inteval(max_size_of_data);
      stat.m_obj->set_average_processing_time_per_inteval(average_size(processing_time, num_of_requests));
      stat.m_obj->set_max_processing_time_per_inteval(max_processing_time);

      const auto prev_total_num_of_requests(stat.m_obj->get_total_num_of_requests());
      const auto prev_total_size_of_data(stat.m_obj->get_total_size_of_data());
      const auto prev_max_size_of_data(stat.m_obj->get_max_size_of_data());
      const auto prev_total_processing_time(stat.m_obj->get_total_processing_time());
      const auto prev_max_processing_time(stat.m_obj->get_max_processing_time());

      stat.m_obj->set_total_num_of_requests(prev_total_num_of_requests + num_of_requests);
      stat.m_obj->set_total_size_of_data(prev_total_size_of_data + size_of_data);
      stat.m_obj->set_max_size_of_data(std::max(prev_max_size_of_data, max_size_of_data));
      stat.m_obj->set_total_processing_time(prev_total_processing_time + processing_time);
      stat.m_obj->set_max_processing_time(std::max(prev_max_processing_time, max_processing_time));

      need_to_publish = true;
      stat.m_last_non_null = now;
    }
  else if (stat.m_obj->get_num_of_requests() != 0)
    {
      stat.m_obj->set_num_of_requests(0.0);
      stat.m_obj->set_average_size_of_data_per_inteval(0);
      stat.m_obj->set_max_size_of_data_per_inteval(0);
      stat.m_obj->set_average_processing_time_per_inteval(0);
      stat.m_obj->set_max_processing_time_per_inteval(0);
   }

  const std::string full_name(s_is_name_prefix + name);

  if (need_to_publish == false && obvilion_flag)
    {
      ERS_DEBUG(1, full_name << " was published last time " << (now - stat.m_last_updated) << " seconds ago");

      if((now - stat.m_last_non_null) > put_user_info_into_oblivion)
        {
          try
            {
              ERS_DEBUG(0, "put into oblivion monitoring object " << full_name);
              m_mon_db.destroy(*stat.m_obj);
              stat.m_obj = nullptr;
            }
          catch (const ers::Issue& ex)
            {
              ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "remove", ex));
            }

          return false;
        }
    }

  stat.m_last_updated = now;

  return true;
}


pbeast::srv::Monitoring::~Monitoring()
{
  publish_calls_stats(true);

  ERS_LOG("API requests:" << m_api.to_string());
  ERS_LOG("REST requests:" << m_rest.to_string());
  ERS_LOG("CORBA requests:" << m_corba.to_string());

  std::unique_lock lock(m_mutex);

  for (const auto& x : m_users)
    ERS_LOG(x.first << " user requests:" << x.second->to_string());
}

void
pbeast::srv::Monitoring::publish_calls_stats(bool obvilion_flag)
{
  static uint64_t counter(0);
  counter++;

  publish_calls_stats(m_api, s_is_api_name, mon_sleep_interval, false);
  publish_calls_stats(m_rest, s_is_rest_name, mon_sleep_interval, false);
  publish_calls_stats(m_corba, s_is_corba_name, mon_sleep_interval, false);

  if (!(counter % (user_sleep_inteval / mon_sleep_interval)) || obvilion_flag)
    {
      std::unique_lock lock(m_mutex);

      for (auto x = m_users.cbegin(); x != m_users.cend(); )
        x = publish_calls_stats(*x->second, x->first, user_sleep_inteval, obvilion_flag) ? std::next(x) : m_users.erase(x);
    }

  try
    {
      m_mon_db.commit("");
    }
  catch (daq::config::Exception &ex)
    {
      ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "publish", ex));
    }
}

std::shared_ptr<pbeast::srv::UserStat>
pbeast::srv::Monitoring::get_user_stat(const std::string &name)
{
  std::unique_lock lock(m_mutex);

  auto it = m_users.find(name);

  if (it != m_users.end())
    return it->second;
  else
    return m_users.emplace(name, new UserStat()).first->second;
}
