#include <stdlib.h>

#include <iostream>
#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include <config/Configuration.h>
#include <config/ConfigObject.h>
#include <config/Schema.h>

#include "pbeast/query.h"
#include "pbeast/curl-request.h"
#include "pbeast/json-serialize.h"

ERS_DECLARE_ISSUE(
  pbeast_define_schema,
  BadCommandLine,
  "bad command line: " << reason,
  ((const char*)reason)
)

ERS_DECLARE_ISSUE(
  pbeast_define_schema,
  ConfigException,
  "caught config exception",
)

ERS_DECLARE_ISSUE(
  pbeast_define_schema,
  PBeastException,
  "caught pbeast exception",
)

int main(int argc, char *argv[])
{
  std::string db_name, web_url, partition;
  std::vector<std::string> classes;

  if (const char * val = getenv(pbeast::url_var::s_receiver))
    web_url = val;

  boost::program_options::options_description desc(
      "Define schema from OKS database on pbeast using web receiver.\n"
      "\n"
      "Options/Arguments");

  try
    {
      desc.add_options()
        (
          "database,d",
          boost::program_options::value<std::string>(&db_name)->required(),
          "database specification in format plugin-name:parameters"
        )
        (
          "classes,c",
          boost::program_options::value<std::vector<std::string>>(&classes)->multitoken(),
          "define only these classes (all classes in this option is not used)"
        )
        (
          "url,u",
          boost::program_options::value<std::string>(&web_url)->default_value(web_url)->required(),
          "pbeast web receiver URL (also defined by the TDAQ_PBEAST_WEB_RECEIVER_URL process environment variable)"
        )
        (
          "partition,p",
          boost::program_options::value<std::string>(&partition)->required(),
          "pbeast partition name"
        )
        (
          "help,h",
          "print help message"
        );

      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::parse_command_line(argc, argv, desc), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return EXIT_SUCCESS;
        }

      boost::program_options::notify(vm);
    }
  catch (std::exception &ex)
    {
      ers::fatal(pbeast_define_schema::BadCommandLine(ERS_HERE, ex.what()));
      return EXIT_FAILURE;
    }


  std::ostringstream buf;

  try
    {
      Configuration db(db_name);

      if (classes.empty())
        {
          for (const auto &x : db.superclasses())
            classes.push_back(*x.first);
        }

      if (classes.empty())
        {
          std::cout << "no classes" << std::endl;
          return EXIT_SUCCESS;
        }

      buf << std::boolalpha << "[\n";

      std::cout << "process classes:" << std::endl;

      for (const auto &x : classes)
        {
          const daq::config::class_t& cl = db.get_class_info(x);

          std::cout << " - define class \"" << cl.p_name << '\"' << std::endl;

          buf <<
              " {\n"
              "  \"partition\":" << pbeast::json::serisalize(partition) << ",\n"
              "  \"name\":" << pbeast::json::serisalize(cl.p_name) << ",\n"
              "  \"attributes\": [\n";

          for (const auto& a : cl.p_attributes)
            buf <<
                "    {\n"
                "     \"name\":" << pbeast::json::serisalize(a.p_name) << ",\n"
                "     \"description\":" << pbeast::json::serisalize(a.p_description) << ",\n"
                "     \"type\":\"" << daq::config::attribute_t::type(a.p_type) << "\",\n"
                "     \"isArray\":" << a.p_is_multi_value << "\n"
                "    },\n";

          if (!cl.p_relationships.empty())
            std::cerr << "warning: the class \"" << cl.p_name << "\" contains relationships not yet supported by pbeast-web-receiver" << std::endl;

          buf.seekp(cl.p_attributes.empty() ? -1 : -2, std::ios_base::end);

          buf <<
              "\n"
              "  ]\n"
              " },\n";
        }

      buf.seekp(-2, std::ios_base::end);
      buf << "\n]";
    }
  catch (daq::config::Exception &ex)
    {
      ers::fatal(pbeast_define_schema::ConfigException(ERS_HERE, ex));
      return EXIT_FAILURE;
    }

  try
  {
    pbeast::ServerProxy web_receiver(web_url);
    pbeast::SimpleRequest obj(web_receiver);

    const std::string json = buf.str();

    obj.request() << "/tdaq/pbeast/define";
    obj.add_post_multipart("data", json.c_str(), json.size());

    std::cout << "execute request \"" << obj.request().str() << "\" with data=\"" << json << "\"..." << std::endl;

    obj.execute(pbeast::CurlSimpleBuffer::get);
    std::cout << "done" << std::endl;
  }
  catch (std::exception &ex)
    {
      ers::fatal(pbeast_define_schema::PBeastException(ERS_HERE, ex));
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
