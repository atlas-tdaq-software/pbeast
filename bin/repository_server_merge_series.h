#ifndef PBEAST_REPOSITORY_SERVER_MERGE_SERIES_H
#define PBEAST_REPOSITORY_SERVER_MERGE_SERIES_H

#include <stdint.h>
#include <set>
#include <map>
#include <vector>

#include <ers/ers.h>

#include <pbeast/series-data.h>
#include <pbeast/input-data-file.h>
#include <pbeast/output-data-file.h>
#include <pbeast/repository.h>

#include "repository_server.h"

namespace pbeast
{
  namespace merger
  {

    inline void
    set_max(uint64_t& max, const uint64_t& v)
    {
      if (v > max)
        max = v;
    }

    class MergeDataBase
    {
    public:

      MergeDataBase(pbeast::Repository& db) :
          m_db(db)
      {
        ;
      }

      virtual
      ~MergeDataBase()
      {
        ;
      }

      std::vector<std::string>
      run(std::vector<std::string>& input_files,
          uint32_t idx,
          std::map<std::string, std::set<uint64_t> >& eov,
          FileType file_type,
          bool perform_downsample
      );

      virtual void
      merge(std::vector<pbeast::InputDataFile *>& m_files,
          pbeast::OutputDataFile& out_file, const std::string& name,
          uint32_t data_type, bool is_array, std::set<uint64_t>& eov,
          uint64_t& usecs_max) = 0;

      pbeast::Repository& m_db;
    };

    class MergeVoid : public MergeDataBase
    {
    public:

      MergeVoid(pbeast::Repository& db) : MergeDataBase(db) { ; }

      virtual void
      merge(std::vector<pbeast::InputDataFile *>& m_files,
          pbeast::OutputDataFile& out_file, const std::string& name,
          uint32_t data_type, bool is_array, std::set<uint64_t>& eov,
          uint64_t& usecs_max);
    };

    class MergeData : public MergeDataBase
    {
    public:

      MergeData(pbeast::Repository& db) : MergeDataBase(db) { ; }

      virtual void
      merge(std::vector<pbeast::InputDataFile *>& m_files,
          pbeast::OutputDataFile& out_file, const std::string& name,
          uint32_t data_type, bool is_array, std::set<uint64_t>& eov,
          uint64_t& usecs_max);
    };

    template<class T>
      class MergeSeriesData
      {

      public:

        MergeSeriesData(const std::string& name,
            std::vector<pbeast::InputDataFile *>& input_files,
            pbeast::OutputDataFile& out_file, const std::set<uint64_t>& eov,
            uint64_t& max)
        {
          std::set<pbeast::SeriesData<T>> m_data_set;

          std::vector<pbeast::SeriesData<T> > data;

          for (auto &f : input_files)
            {
              data.clear();
              f->read_serie(name, data);
              for (auto &i : data)
                {
                  m_data_set.insert(i);
                }
            }

          std::vector<pbeast::SeriesData<T>> m_data;
          pbeast::Repository::merge(m_data_set, &eov, m_data);

          if (unsigned long size = m_data.size())
            {
              set_max(max, m_data[size - 1].get_ts_last_updated());
            }

          out_file.write_data<T>(name, m_data);
        }
      };

    template<class T>
      class MergeSeriesVectorData
      {
      public:

        MergeSeriesVectorData(const std::string& name,
            std::vector<pbeast::InputDataFile *>& input_files,
            pbeast::OutputDataFile& out_file, const std::set<uint64_t>& eov,
            uint64_t& max)
        {
          std::set<pbeast::SeriesVectorData<T>> m_data_set;

          std::vector<pbeast::SeriesVectorData<T> > data;

          for (auto &f : input_files)
            {
              data.clear();
              f->read_serie(name, data);
              for (auto &i : data)
                {
                  m_data_set.insert(i);
                }
            }

          std::vector<pbeast::SeriesVectorData<T>> m_data;
          pbeast::Repository::merge(m_data_set, &eov, m_data);

          if (unsigned long size = m_data.size())
            {
              set_max(max, m_data[size - 1].get_ts_last_updated());
            }

          out_file.write_data(name, m_data);
        }

      };

  }
}

#endif
