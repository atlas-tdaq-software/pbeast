#ifndef PBEAST_WEB_RECEIVER_DATA_PROVIDER_H
#define PBEAST_WEB_RECEIVER_DATA_PROVIDER_H

#include <stddef.h>
#include <filesystem>
#include <string>

#include <libmicrohttpd/microhttpd.h>

#include "receiver_data_provider.h"
//#include "web_receiver_config.h"

namespace pbeast
{
  namespace receiver
  {
    class Config;

    class Web_DataProvider : public DataProvider
    {

    public:

      Web_DataProvider(const Web_Config& config, const std::filesystem::path& path, IPCPartition& partition, int thread_pool_size);
      ~Web_DataProvider();

      DataProvider *
      get_this()
      {
        return this;
      }

      const Config&
      get_config()
      {
        return m_config;
      }

    private:

      static MHD_Result
      answer_to_connection(void * /*cls*/, struct MHD_Connection *connection, const char *url, const char *method, const char * /*version*/, const char * /*upload_data*/, size_t * /*upload_data_size*/, void **con_cls);

      static void
      request_completed(void *cls, struct MHD_Connection *connection, void **con_cls, enum MHD_RequestTerminationCode toe);

      void
      process_http_request(std::string& name, const std::string& json, std::string& title, std::string& out);

      Partition *
      read_classes_from_meta(const std::string& partition);

      void
      define_classes(const std::string& json);

      void
      update_objects(const std::string& json);

      void
      close_objects(const std::string& json);

      struct MHD_Daemon * m_mhd;

      const
      Web_Config& m_config;
    };

  }
}

#endif
