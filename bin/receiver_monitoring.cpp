#include <chrono>
#include <ctime>
#include <thread>

#include <is/exceptions.h>

#include "application_exceptions.h"
#include "receiver_monitoring.h"

#include "pbeast/PBeastReceiverMon.h"

using std::chrono_literals::operator""ms;

pbeast::receiver::Monitoring::Monitoring(const std::string& partition_name, const std::string& receiver_id) :
  m_num_of_tested_objects           (0),
  m_num_of_tested_attributes        (0),
  m_num_of_updated_data_points      (0),
  m_num_of_merged_data_points       (0),
  m_num_of_saved_files              (0),
  m_size_of_saved_files             (0),
  m_compact_time                    (0),
  m_total_num_of_tested_objects     (0),
  m_total_num_of_tested_attributes  (0),
  m_total_num_of_saved_files        (0),
  m_total_size_of_saved_files       (0),
  m_number_of_data_items            (0),
  m_total_num_of_updated_data_points(0),
  m_total_num_of_merged_data_points (0),
  m_data_compaction_efficiency      (0.0),
  m_mon_db                          ("bstconfig"),
  m_info_name                       (receiver_id + '@' + partition_name),
  m_mon_obj                         (nullptr),
  m_started                         (std::time(nullptr))
{
  publish();
}

pbeast::receiver::Monitoring::~Monitoring()
{
  publish();

  ERS_LOG(
    "TOTAL:\n"
    "  number of tested objects      : " << m_total_num_of_tested_objects << "\n"
    "  number of tested attributes   : " << m_total_num_of_tested_attributes << "\n"
    "  number of updated data points : " << m_total_num_of_updated_data_points << "\n"
    "  number of merged data points  : " << m_total_num_of_merged_data_points << "\n"
    "  number of saved files         : " << m_total_num_of_saved_files << "\n"
    "  size of saved files           : " << m_total_size_of_saved_files << "\n"
  );

  time_t now = time(0);

  double interval = now - m_started;
  if(interval < 1.0) interval = 1.0;

  ERS_LOG(
    "AVERAGES:\n"
    "  testing objects       : " << static_cast<double>(m_total_num_of_tested_objects) / interval / 1000.0 << " kHz\n"
    "  testing attributes    : " << static_cast<double>(m_total_num_of_tested_attributes) / interval / 1000.0 << " kHz\n"
    "  updating data points  : " << static_cast<double>(m_total_num_of_updated_data_points) / interval / 1000.0 << " kHz\n"
    "  merging data points   : " << static_cast<double>(m_total_num_of_merged_data_points) / interval / 1000.0 << " kHz\n"
    "  compaction efficiency : " << static_cast<double>(m_total_num_of_merged_data_points) / static_cast<double>(m_total_num_of_updated_data_points+m_total_num_of_merged_data_points) << "\n"
    "  saving files          : " << static_cast<double>(m_total_num_of_saved_files) / interval << " Hz\n"
    "  saving rate           : " << static_cast<double>(m_total_size_of_saved_files) / interval / 1000.0 << " KBytes/s"
  );


  // set most monitoring parameters to 0 before removal of the monitoring object
  std::this_thread::sleep_for(1ms);
  m_compact_time = 0;
  m_rebuild_eov_time = 0;
  m_commit_time = 0;
  publish();
  std::this_thread::sleep_for(10ms);

  try
    {
      std::unique_lock lock(m_mutex);

      if (m_mon_db.loaded() && m_mon_obj)
        {
          m_mon_db.destroy(*m_mon_obj);
          m_mon_db.commit("");
          m_mon_obj = nullptr;
        }
    }
  catch (daq::config::Exception& ex)
    {
      ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "remove", ex));
    }
}


void pbeast::receiver::Monitoring::publish()
{
  std::unique_lock lock(m_mutex);

  try
    {
      if (m_mon_db.loaded() == false)
        try
          {
            m_mon_db.load("initial");
          }
        catch (daq::config::Exception &ex)
          {
            ers::warning(ex);
            return;
          }

      if (!m_mon_obj)
        try
          {
            m_mon_obj = const_cast<pbeast::PBeastReceiverMon*>(m_mon_db.create<pbeast::PBeastReceiverMon>("", m_info_name, true));
          }
        catch (const daq::config::Exception &ex)
          {
            ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "create", ex));
            return;
          }

      if (uint64_t total = (m_num_of_merged_data_points + m_num_of_updated_data_points))
        {
          m_data_compaction_efficiency = static_cast<double>(m_num_of_merged_data_points) / static_cast<double>(total);
          m_total_num_of_updated_data_points += m_num_of_updated_data_points.exchange(0);
          m_total_num_of_merged_data_points += m_num_of_merged_data_points.exchange(0);
        }

      const auto num_of_tested_objects = m_num_of_tested_objects.exchange(0);
      const auto num_of_tested_attributes = m_num_of_tested_attributes.exchange(0);
      const auto num_of_saved_files = m_num_of_saved_files.exchange(0);
      const auto size_of_saved_files = m_size_of_saved_files.exchange(0);

      m_mon_obj->set_num_of_tested_objects(num_of_tested_objects);
      m_mon_obj->set_num_of_tested_attributes(num_of_tested_attributes);
      m_mon_obj->set_num_of_saved_files(num_of_saved_files);
      m_mon_obj->set_size_of_saved_files(size_of_saved_files);
      m_mon_obj->set_data_compaction_time(m_compact_time);
      m_mon_obj->set_rebuild_eov_time(m_rebuild_eov_time);
      m_mon_obj->set_commit_time(m_commit_time);
      m_mon_obj->set_data_compaction_efficiency(m_data_compaction_efficiency);
      m_mon_obj->set_num_of_data_points(m_number_of_data_items);

      m_total_num_of_tested_objects += num_of_tested_objects;
      m_total_num_of_tested_attributes += num_of_tested_attributes;
      m_total_num_of_saved_files += num_of_saved_files;
      m_total_size_of_saved_files += size_of_saved_files;

      m_mon_db.commit("");
    }
  catch (daq::config::Exception& ex)
    {
      ers::error(daq::pbeast::OperationalMonitoringError(ERS_HERE, "publish", ex));
    }
}
