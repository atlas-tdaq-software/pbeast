#include <mutex>

#include <pbeast/writer_lock.h>

#include "receiver_attribute.h"
#include "receiver_class.h"
#include "receiver_data_provider.h"
#include "receiver_monitoring.h"
#include "receiver_errors.h"

pbeast::receiver::Attribute::Attribute(const Attribute& a, Class * parent) :
    m_type(a.m_type),
    m_type_name(a.m_type_name),
    m_type_class(nullptr),
    m_parent(parent ? parent : a.m_parent),
    m_is_array(a.m_is_array),
    m_name(a.m_name),
    m_dir_name(a.m_dir_name),
    m_description(a.m_description),
    m_compact_data(nullptr),
    m_raw_data(nullptr)
{
  m_type_class = (parent && a.m_type_class ? a.m_type_class->make_new(this) : a.m_type_class);
  init();
}

pbeast::receiver::Attribute::~Attribute()
{
  delete m_type_class;
  delete m_raw_data;
}

void
pbeast::receiver::Attribute::init(bool extend)
{
  switch (m_type)
    {

  case pbeast::BOOL:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<bool>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<bool>();
    break;

  case pbeast::S8:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<int8_t>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<int8_t>();
    break;

  case pbeast::U8:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<uint8_t>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<uint8_t>();
    break;

  case pbeast::S16:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<int16_t>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<int16_t>();
    break;

  case pbeast::U16:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<uint16_t>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<uint16_t>();
    break;

  case pbeast::S32:
  case pbeast::ENUM:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<int32_t>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<int32_t>();
    break;

  case pbeast::U32:
  case pbeast::DATE:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<uint32_t>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<uint32_t>();
    break;

  case pbeast::S64:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<int64_t>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<int64_t>();
    break;

  case pbeast::U64:
  case pbeast::TIME:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<uint64_t>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<uint64_t>();
    break;

  case pbeast::FLOAT:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<float>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<float>();
    break;

  case pbeast::DOUBLE:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<double>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<double>();
    break;

  case pbeast::STRING:
    if (m_is_array == false)
      m_raw_data = new pbeast::receiver::DataHolder<std::string>();
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<std::string>();
    break;

  case pbeast::VOID:
    if (m_is_array == false)
      {
        if(extend == false)
          m_raw_data = new pbeast::receiver::DataHolder<Void>();
        else
          m_raw_data = new pbeast::receiver::DataHolder<Void,true>();
      }
    else
      m_raw_data = new pbeast::receiver::VectorDataHolder<Void>();
    break;

  case pbeast::OBJECT:
    ;
    break;

    }
}


void pbeast::receiver::Attribute::link_type(const std::map<std::string, Class *>& classes)
{
  std::map<std::string, Class *>::const_iterator j = classes.find(m_type_name);

  if (j != classes.end())
    {
      m_type_class = j->second;
    }
  else
    {
      throw daq::pbeast::BadInfoType(ERS_HERE, m_type_name);
    }
}

void pbeast::receiver::Attribute::create_nested()
{
  m_type_class = m_type_class->make_new(this);
}

void
pbeast::receiver::Attribute::commit(const std::filesystem::path& dir, const std::string& partition, const std::string& partition_dir_name, Class& c, BaseData * data)
{
  const unsigned int max_number_of_attempts(3);
  unsigned int count = 0;

  while (count++ < max_number_of_attempts)
    {
      if (data->commit(dir, partition, partition_dir_name, c, *this))
        break;
      usleep(250000);
    }

  if (count > max_number_of_attempts)
    {
      ers::fatal(daq::pbeast::LostData(ERS_HERE, max_number_of_attempts, data->get_number_of_items(), m_name, c.get_name()));
    }
}

void
pbeast::receiver::Attribute::compact_data(uint64_t cb_expire_ts, const std::map<std::string, std::set<uint64_t>*> * eov)
{
  std::lock_guard<std::mutex> raw_data_lock(m_raw_data->m_mutex);

  if (m_raw_data->get_size() > 0)
    {
      std::lock_guard<std::mutex> compact_data_lock(m_compact_data_mutex);
      DataProvider::get_monitoring_object().add_compact_data_points(m_raw_data->compact(cb_expire_ts, *this, eov));
    }
}

void
pbeast::receiver::Attribute::write_ready(pbeast::WriterLock& writer_lock, const std::filesystem::path& dir, const std::string& partition, const std::string& partition_dir_name, Class& c, uint64_t expire_ts)
{
  std::lock_guard<std::mutex> data_lock(m_compact_data_mutex);

  if(m_compact_data != nullptr)
    {
      if(m_compact_data->is_ready(expire_ts))
        {
          writer_lock.lock();
          commit(dir, partition, partition_dir_name, c, m_compact_data);
          delete m_compact_data;
          m_compact_data = nullptr;
        }
      else if(uint32_t size = m_compact_data->get_number_of_items())
        {
          pbeast::receiver::DataProvider::add_data_size(size);
        }
    }
}

std::ostream& pbeast::receiver::Attribute::print(std::ostream& s, const std::string& prefix) const
{
  s << prefix << "attribute: " << get_name() << " type: " << pbeast::as_string(m_type) << std::endl;

  if(m_type_class)
    {
      m_type_class->print(s, prefix + "  ");
    }

  return s;
}
