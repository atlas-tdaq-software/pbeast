#include "receiver_config.h"
#include "receiver_errors.h"

#include "pbeast/PBeastReceiverApplication.h"

void
pbeast::receiver::Config::init(const pbeast::PBeastReceiverApplication &obj)
{
  m_name = obj.UID();

  m_max_save_delay = obj.get_MaxSaveDelay();
  m_repository_dir_name = obj.get_RepositoryDirName();
  m_max_data_size = obj.get_MaxDataSize();
  m_max_file_size = obj.get_MaxFileSize();
  m_max_data_delay = obj.get_MaxDataDelay();
  m_flush_data_threshold = obj.get_FlushDataThreshold();
  m_save_check_interval = obj.get_SaveCheckInterval();

  if (m_max_data_delay >= m_max_save_delay)
    throw daq::pbeast::BadConfigDelays(ERS_HERE, m_max_save_delay, m_max_data_delay);

  if (m_max_file_size >= m_max_data_size)
    throw daq::pbeast::BadConfigDataSizeLimits(ERS_HERE, m_max_data_size, m_max_file_size);

  m_small_data_delay = m_max_data_delay;
  m_big_data_delay = m_max_save_delay / 2;
}
