#ifndef PBEAST_IS_RECEIVER_DATA_PROVIDER_H
#define PBEAST_IS_RECEIVER_DATA_PROVIDER_H

#include <filesystem>

#include "receiver_data_provider.h"
#include "is_receiver_config.h"

class ISInfoReceiver;

namespace pbeast
{
  namespace receiver
  {

    class IS_DataProvider;

    struct Callback
    {
      pbeast::receiver::IS_DataProvider &m_data_provider;
      const uint32_t m_small_delay;
      const uint32_t m_big_delay;

      Callback(pbeast::receiver::IS_DataProvider &dp);

      void
      callback(ISCallbackInfo *ise);
    };

    class IS_DataProvider : public DataProvider
    {

    public:

      IS_DataProvider(const IS_Config& config, const std::filesystem::path& path, ISInfoReceiver& receiver, int thread_pool_size);
      ~IS_DataProvider();

      DataProvider *
      get_this()
      {
        return this;
      }

      const Config&
      get_config()
      {
        return m_config;
      }

      ISInfoReceiver&
      get_receiver()
      {
        return m_receiver;
      }

      Callback *
      get_callback()
      {
        return &m_callback;
      }

      void
      check_readers();

      void
      start_check_receivers_thread(unsigned int sleep_interval);

    private:

      const IS_Config& m_config;
      ISInfoReceiver& m_receiver;
      Callback m_callback;

    };

  }
}

#endif
