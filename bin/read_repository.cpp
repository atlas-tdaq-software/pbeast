#include <cstdint>
#include <cstdlib>
#include <filesystem>
#include <iostream>
#include <map>
#include <set>
#include <sstream>
#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include "boost/date_time/local_time/local_time.hpp"

#include <ers/ers.h>

#include <pbeast/def-intervals.h>
#include <pbeast/meta.h>
#include <pbeast/repository.h>
#include <pbeast/series-data.h>
#include <pbeast/series-print.h>

#include "utils.h"
#include "application_exceptions.h"

namespace po = boost::program_options;


// string to timestamp

static uint64_t
str2ts(std::string& in, boost::local_time::time_zone_ptr tz_ptr, const char * name)
{
  static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));

  std::string s(in);
  std::replace(s.begin(), s.end(), 'T', ' ');
  boost::posix_time::ptime t;

  try
    {
      t = boost::posix_time::time_from_string(s);
    }
  catch (std::exception& e)
    {
      std::ostringstream text;
      text << "cannot parse " << name << " = \'" << in << "\': \"" << e.what() << '\"';
      throw std::runtime_error(text.str().c_str());
    }

    // convert local time to UTC, if the time zone was provided

  if (tz_ptr)
    {
      try
        {
          boost::local_time::local_date_time lt(t.date(), t.time_of_day(),
              tz_ptr, boost::local_time::local_date_time::EXCEPTION_ON_ERROR);
          ERS_DEBUG(1,
              "Build zone\'s time \'" << in << "\' => \'" << lt.to_string() << "\' using \'" << tz_ptr->to_posix_string() << '\'');
          t = lt.utc_time();
        }
      catch (std::exception& e)
        {
          std::ostringstream text;
          text << "cannot parse " << name << " = \'" << in
              << "\' as zone\'s time: \"" << e.what() << '\"';
          throw std::runtime_error(text.str().c_str());
        }
    }
  return (t - epoch).total_microseconds();
}

static boost::local_time::time_zone_ptr
get_time_zone_ptr(const std::string& time_zone)
{
  const char * tz_spec_file = ::getenv("BOOST_DATE_TIME_TZ_SPEC");

  if (!tz_spec_file || !*tz_spec_file)
    {
      throw std::runtime_error("cannot read value of BOOST_DATE_TIME_TZ_SPEC environment variable to parse timezone parameter");
    }
  else
    {
      ERS_DEBUG(1, "Boost time-zone specification file is \'" << tz_spec_file << '\'');
    }

  boost::local_time::tz_database tz_db;

  try
    {
      tz_db.load_from_file(tz_spec_file);
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "cannot read Boost time-zone specification file " << tz_spec_file << ": " << ex.what();
      throw std::runtime_error(text.str().c_str());
    }

  if (time_zone == "list-regions")
    {
      for (auto & r : tz_db.region_list())
        {
          std::cout << r << std::endl;
        }

      exit(0);
    }

  if(boost::local_time::time_zone_ptr tz_ptr = tz_db.time_zone_from_region(time_zone))
    {
      return tz_ptr;
    }
  else
    {
      std::ostringstream text;
      text << "cannot find time-zone \'" << time_zone << '\'';
      throw std::runtime_error(text.str().c_str());
    }
}

void
print_eov_serie(const std::pair<const std::string, std::set<uint64_t>> & x, boost::local_time::time_zone_ptr tz_ptr, bool ts2str)
{
  std::cout << x.first << ":\n";

  for (const auto& e : x.second)
    {
      pbeast::print_eov(std::cout, e, tz_ptr, ts2str, "    ");
    }
}

void
print_eov_serie(const std::pair<const std::string, std::set<uint32_t>> & x, boost::local_time::time_zone_ptr tz_ptr, bool ts2str)
{
  std::cout << x.first << ":\n";

  for (const auto& e : x.second)
    {
      pbeast::print_eov(std::cout, e, tz_ptr, ts2str, "    ");
    }
}

template<class T>
  void
  print(const std::map<std::string, std::vector<T>>& data, const std::map<std::string, std::set<uint64_t>>& map_eov, const std::map<std::string, std::set<uint64_t>>* map_upd, const std::string& suffix_separator, boost::local_time::time_zone_ptr tz_ptr, bool print_raw_ts)
  {
    std::set<uint64_t> empty;

    // print time series data
    for (const auto & x : data)
      {
        std::map<std::string, std::set<uint64_t>>::const_iterator e(suffix_separator.empty() ? map_eov.find(x.first) : map_eov.find(pbeast::get_object_name(x.first, suffix_separator)));

        const std::set<uint64_t>& obj_eov(e != map_eov.end() ? e->second : empty);
        const std::set<uint64_t>* obj_upd(nullptr);

        if (map_upd)
          {
            std::map<std::string, std::set<uint64_t>>::const_iterator u(suffix_separator.empty() ? map_upd->find(x.first) : map_upd->find(pbeast::get_object_name(x.first, suffix_separator)));
            if (u != map_upd->end())
              obj_upd = &u->second;
            else
              obj_upd = &empty;
          }

        std::cout << x.first << ":\n";
        pbeast::print(std::cout, x.second, obj_eov, obj_upd, tz_ptr, !print_raw_ts, "    ");
      }

    // print deleted objects with no data
    for (const auto& x : map_eov)
      if (data.count(x.first) == 0)
        print_eov_serie(x, tz_ptr, !print_raw_ts);
  }

template<class T>
  void
  print(const std::map<std::string, std::vector<T>>& data, const std::map<std::string, std::set<uint32_t>>& map_eov, const std::map<std::string, std::set<uint32_t>>& map_upd, const std::string& suffix_separator, boost::local_time::time_zone_ptr tz_ptr, bool print_raw_ts)
  {
    std::set<uint32_t> empty32;

    // print time series data
    for (const auto & x : data)
      {
        std::map<std::string, std::set<uint32_t>>::const_iterator e(suffix_separator.empty() ? map_eov.find(x.first) : map_eov.find(pbeast::get_object_name(x.first, suffix_separator)));
        std::map<std::string, std::set<uint32_t>>::const_iterator u(suffix_separator.empty() ? map_upd.find(x.first) : map_upd.find(pbeast::get_object_name(x.first, suffix_separator)));
        const std::set<uint32_t>& obj_eov(e != map_eov.end() ? e->second : empty32);
        const std::set<uint32_t>& obj_upd(u != map_upd.end() ? u->second : empty32);
        std::cout << x.first << ":\n";
        pbeast::print(std::cout, x.second, obj_eov, obj_upd, tz_ptr, !print_raw_ts, "    ");
      }

    // print deleted objects with no data
    for (const auto& x : map_eov)
      {
        if (data.count(x.first) == 0)
          {
            print_eov_serie(x, tz_ptr, !print_raw_ts);
          }
      }
  }

template<typename T>
  void
  read_and_print(pbeast::Repository& db, const std::string& partition_name, const std::string& class_name, const std::filesystem::path& attribute_path,
      uint64_t since, uint64_t until, uint64_t prev_delta, uint64_t next_delta, uint32_t interval, pbeast::FillGaps::Type fill_gaps, bool is_array, const std::string& serie_name, bool serie_is_regexp, const std::string& serie_name_suffix,
      bool suffix_is_regexp, boost::local_time::time_zone_ptr tz_ptr, bool print_every_update, bool print_raw_ts, pbeast::CallStats& stat, pbeast::CallStats& local_stat)
  {
    ERS_DEBUG( 1 , "read_and_print serie_name: \'" << serie_name << "\' is regexp: " << std::boolalpha << serie_is_regexp << ", serie_name_suffix: \'" << serie_name_suffix << "\' is regexp: " << std::boolalpha << suffix_is_regexp );


    std::string suffix_separator;

    if (suffix_is_regexp || !serie_name_suffix.empty() || attribute_path.native().find(std::filesystem::path::preferred_separator) != std::string::npos)
      {
        suffix_separator = pbeast::Repository::get_suffix_separator(attribute_path);
      }

    if (interval == 0)
      {
        std::map<std::string,std::set<uint64_t>> eov;
        std::map<std::string,std::set<uint64_t>> upd_holder, *upd(nullptr);

        if (print_every_update)
          upd = &upd_holder;

        if (is_array == false)
          {
            std::map<std::string,std::vector<pbeast::SeriesData<T>>> data;
            db.read_attribute<pbeast::SeriesData<T>>(partition_name, class_name, attribute_path, since, until, prev_delta, next_delta, serie_name, serie_is_regexp, serie_name_suffix, suffix_is_regexp, data, eov, upd, stat);
            print(data, eov, upd, suffix_separator, tz_ptr, print_raw_ts);
          }
        else
          {
            std::map<std::string,std::vector<pbeast::SeriesVectorData<T>>> data;
            db.read_attribute<pbeast::SeriesVectorData<T>>(partition_name, class_name, attribute_path, since, until, prev_delta, next_delta, serie_name, serie_is_regexp, serie_name_suffix, suffix_is_regexp, data, eov, upd, stat);
            print(data, eov, upd, suffix_separator, tz_ptr, print_raw_ts);
          }
      }
    else
      {
        std::map<std::string,std::set<uint32_t>> eov;

        std::map<std::string,std::set<uint32_t>> upd_holder, *upd(nullptr);

        if (print_every_update)
          upd = &upd_holder;

        if (is_array == false)
          {
            std::map<std::string,std::vector<pbeast::DownsampledSeriesData<T>>> data;
            db.read_downsample_attribute<pbeast::DownsampledSeriesData<T>>(partition_name, class_name, attribute_path, since, until, prev_delta, next_delta, interval, fill_gaps, serie_name, serie_is_regexp, serie_name_suffix, suffix_is_regexp, data, eov, upd, stat, local_stat);
            print(data, eov, upd_holder, suffix_separator, tz_ptr, print_raw_ts);
          }
        else
          {
            std::map<std::string,std::vector<pbeast::DownsampledSeriesVectorData<T>>> data;
            db.read_downsample_attribute<pbeast::DownsampledSeriesVectorData<T>>(partition_name, class_name, attribute_path, since, until, prev_delta, next_delta, interval, fill_gaps, serie_name, serie_is_regexp, serie_name_suffix, suffix_is_regexp, data, eov, upd, stat, local_stat);
            print(data, eov, upd_holder, suffix_separator, tz_ptr, print_raw_ts);
          }

      }
  }

int
main(int argc, char ** argv)
{
  po::options_description desc(
      "pBeast data reader.\n"
      "For timestamps use ISO 8601 date-time format, i.e. YYYY-MM-DD HH:MM:SS[,...]\n"
      "For available timezones run with -z list-regions\n\n"
      "Available options are"
 );

  std::filesystem::path repository_path;
  std::filesystem::path local_repository_path;
  std::filesystem::path cache_path;
  std::string partition_name;
  std::string class_name;
  std::string _attribute_path; // workaround Boost bug http://boost.2283326.n4.nabble.com/program-options-Problem-with-paths-that-have-spaces-td2576490.html
  std::string object_name;
  bool object_name_is_regexp = false;
  std::string object_suffix_name;
  bool object_suffix_is_regexp = false;
  boost::local_time::time_zone_ptr tz_ptr;
  uint64_t since(pbeast::get_def_since());
  uint64_t until(pbeast::get_def_until());
  uint64_t prev_interval(pbeast::def_lookup_prev_const);
  uint64_t next_interval(pbeast::def_lookup_next_const);
  pbeast::FillGaps::Type fill_gaps;
  uint32_t interval(0);
  bool list_only = false;
  bool print_every_update = false;
  bool print_raw_ts = false;
  uint64_t max_message_size;
  uint64_t max_local_ds_data_size;
  bool verbose = false;
  uint32_t downsample_thread_pool_size = 4;


  // parse command line
  try
    {
      std::string since_tmp;
      std::string until_tmp;
      std::string time_zone;
      std::string object_names_regexp;
      std::string object_suffix_name_regexp;
      std::string max_response_size_str("2g");
      std::string max_local_ds_data_size_str("5g");
      std::string fill_gaps_tmp("near");
      std::streamsize precision(std::cout.precision());

      desc.add_options()
      (
          "repository-dir,r",
          po::value<std::filesystem::path>(&repository_path)->required(),
          "Name of the repository directory containing P-BEAST files"
      )
      (
          "local-repository-dir,y",
          po::value<std::filesystem::path>(&local_repository_path),
          "Name of the local repository directory containing P-BEAST files"
      )
      (
          "cache-dir,R",
          po::value<std::filesystem::path>(&cache_path),
          "Name of the cache directory to store downsampled files (in not defined, downsample data in-memory)"
      )
      (
          "downsample-thread-pool-size,q",
          po::value<uint32_t>(&downsample_thread_pool_size)->default_value(downsample_thread_pool_size),
          "Number of threads processing downsampling requests"
      )
      (
          "partition-name,p",
          po::value<std::string>(&partition_name)->required(),
          "Name of partition"
      )
      (
          "class-name,c",
          po::value<std::string>(&class_name)->required(),
          "Name of class"
      )
      (
          "attribute-name,a",
          po::value<std::string>(&_attribute_path)->required(),
          "Name of attribute or path in case of nested type"
      )
      (
          "object-name,o",
          po::value<std::string>(&object_name),
          "Name of object"
      )
      (
          "object-name-regexp,O",
          po::value<std::string>(&object_names_regexp),
          "Regular expression for names of objects"
      )
      (
          "object-suffix-name,x",
          po::value<std::string>(&object_suffix_name),
          "Optional suffix for object name (for nested types)"
      )
      (
          "object-suffix-name-regexp,X",
          po::value<std::string>(&object_suffix_name_regexp),
          "Optional regular expression for object name suffixes (for nested types)"
      )
      (
          "since,s",
          po::value<std::string>(&since_tmp)->default_value(since_tmp),
          "Show data since given timestamp"
      )
      (
          "until,t",
          po::value<std::string>(&until_tmp)->default_value(until_tmp),
          "Show data until given timestamp"
      )
      (
          "since-raw-timestamp,S",
          po::value<uint64_t>(&since)->default_value(since),
          "Show data since given raw timestamp (number of micro-seconds since Epoch)"
      )
      (
          "until-raw-timestamp,T",
          po::value<uint64_t>(&until)->default_value(until),
          "Show data until given raw timestamp (number of micro-seconds since Epoch)"
      )
      (
          "lookup-previous-value-time-lapse,P",
          po::value<uint64_t>(&prev_interval)->default_value(prev_interval),
          "Time lapse to lookup closest previous data point outside query time interval (in microseconds)"
      )
      (
          "lookup-next-value-time-lapse,N",
          po::value<uint64_t>(&next_interval)->default_value(next_interval),
          "Time lapse to lookup closest next data point outside query time interval (in microseconds)"
      )
      (
          "downsample-interval,i",
          po::value<uint32_t>(&interval),
          "Downsampling interval (in seconds; if not set or zero, print raw data)"
      )
      (
          "fill-gaps,g",
          po::value<std::string>(&fill_gaps_tmp)->default_value(fill_gaps_tmp),
          "When downsample, approximate missing values: \"none\", \"near\" or \"all\""
      )
      (
          "time-zone,z",
          po::value<std::string>(&time_zone)->default_value(time_zone),
          "Name of time zone (use \"-z list-regions\" to see known time zone names)"
      )
      (
          "list-updated-objects,l",
          "List names of updated objects only"
      )
      (
          "print-value-per-update,u",
          "Print value per IS object update even if the value remained unchanged"
      )
      (
          "print-raw-timestamp,w",
          "Print raw timestamp (number of micro-seconds since Epoch) instead of formatted one"
      )
      (
          "precision,V",
          po::value<std::streamsize>(&precision)->default_value(precision),
          "Set floating point data output precision"
      )
      (
          "max-response-size,m",
          po::value<std::string>(&max_response_size_str)->default_value(max_response_size_str),
          "Limit amount of read data using xC format, where x is an integer and optional C is one of k,m,g (kilo, mega, giga)"
      )
      (
          "max-local-ds-data-size,M",
          po::value<std::string>(&max_local_ds_data_size_str)->default_value(max_local_ds_data_size_str),
          "Limit amount of in-memory downsampled data read from local repository using xC format as in previous option"
      )
      (
          "verbose,v",
          "Report verbose statistics"
      )
      (
          "help,h",
          "Print help message"
      );

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("object-name-regexp"))
        object_name_is_regexp = true;

      if (vm.count("object-suffix-name-regexp"))
        object_suffix_is_regexp = true;

      if (vm.count("list-updated-objects"))
        list_only = true;

      if (vm.count("print-value-per-update"))
        print_every_update = true;

      if (vm.count("print-raw-timestamp"))
        print_raw_ts = true;

      if (vm.count("verbose"))
        verbose = true;

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);

      if (vm["until-raw-timestamp"].defaulted() == false && vm["until"].defaulted() == false)
        throw std::runtime_error("both \"until\" and \"until-raw-timestamp\" parameters are defined");

      if (vm["since-raw-timestamp"].defaulted() == false && vm["since"].defaulted() == false)
        throw std::runtime_error("both \"since\" and \"since-raw-timestamp\" parameters are defined");

      if (!time_zone.empty())
        tz_ptr = get_time_zone_ptr(time_zone);

      if (!since_tmp.empty())
        since = str2ts(since_tmp, tz_ptr, "since");

      if (!until_tmp.empty())
        until = str2ts(until_tmp, tz_ptr, "until");

      if (precision != std::cout.precision())
        std::cout.precision(precision);

      if (!object_names_regexp.empty() && object_names_regexp != ".*")
        object_name = object_names_regexp;

      if (!object_suffix_name_regexp.empty() && object_suffix_name_regexp != ".*")
        object_suffix_name = object_suffix_name_regexp;

      max_message_size = pbeast::utils::str2size(max_response_size_str, "max-response-size");

      max_local_ds_data_size = pbeast::utils::str2size(max_local_ds_data_size_str, "max-local-ds-data-size");

      fill_gaps = pbeast::FillGaps::mk(fill_gaps_tmp);
    }
  catch (std::exception& ex)
    {
      ers::fatal( daq::pbeast::CommandLineError( ERS_HERE, ex.what() ) );
      return 1;
    }

  std::filesystem::path attribute_path(_attribute_path);

  std::string attribute_name;

  try
    {
      pbeast::DownsampleConfig ds_config;

      if(interval > 0)
        {
          ds_config.init(cache_path, downsample_thread_pool_size);
        }

      pbeast::Repository db(repository_path, local_repository_path, ds_config);

      db.init_lock();

      if(list_only)
        {
          pbeast::CallStats dummy(max_message_size);
          std::set<std::string> objects;
          db.list_updated_objects(objects, partition_name, class_name, attribute_path, since, until, object_name, dummy);

          for(auto & x : objects)
            std::cout << x << '\n';

          return 0;
        }

     std::string attribute_class_name;

     pbeast::Meta::get_attribute_class_and_name(attribute_path, attribute_class_name, attribute_name);

     if(attribute_class_name.empty()) attribute_class_name = class_name;

     pbeast::Meta info(repository_path, partition_name, attribute_class_name);

     const std::vector<pbeast::SeriesData<std::string>>& types = info.get_attribute_types(attribute_name);

     unsigned int len = types.size();

     if(len == 0)
       {
         throw std::runtime_error("cannot find data type attribute information");
       }

     // split search time interval on several intervals in accordance with attribute description

     struct AttrInfo
     {
       uint64_t m_since, m_until;
       pbeast::DataType m_type;
       bool m_is_array;
       AttrInfo(uint64_t s, uint64_t u, pbeast::DataType t, bool a) : m_since(s), m_until(u), m_type(t), m_is_array(a) {;}
     };

     std::vector<AttrInfo> attr_info;

     for(unsigned int idx = 0; idx < len; ++idx)
       {
         pbeast::DataType type;
         bool is_array;

         std::string type_str;

         pbeast::Meta::decode_type(types[idx].get_value(), type_str, is_array);
         type = pbeast::str2type(type_str);
         ERS_DEBUG( 0 , "read type: " << types[idx].get_value() << " => " << (int)type << ", is array: " << std::boolalpha << is_array );

         if(type == pbeast::OBJECT)
           {
             std::ostringstream text;
             text << "attribute " << attribute_path << " points to nested object type; provide full path to leave attribute";
             throw std::runtime_error(text.str().c_str());
           }

         if(len == 1)
           {
             attr_info.emplace_back(since, until, type, is_array);
           }
         else
           {

             //
             // s, u - map current knowledges about attribute info validity on full time-line
             //
             //        [x1]...[y1]   [x2...y2]    [x3.......y3]
             //
             //  [s1]------------[u1][s2]-----[u2][s3]-------------------------[u3]
             //

             uint64_t s = (idx == 0 ? pbeast::min_ts_const : types[idx].get_ts_created());
             uint64_t u = (idx == (len-1) ? pbeast::max_ts_const : types[idx+1].get_ts_created()-1);

             //
             // since, until are shown as x, y
             //
             //             [s].............[u]
             //
             //  (A)    [x]--------[y]
             //  (B)    [x]------------------------[y]
             //  (C)            [x]---[y]
             //  (D)            [x]----------------[y]
             //

             if(until >= s)
               {
                 if(since <= s)
                   {
                     // case (A)
                     if(until <= u)
                       {
                         attr_info.emplace_back(s, until, type, is_array);
                       }
                     // case (B)
                     else
                       {
                         attr_info.emplace_back(s, u, type, is_array);
                       }
                   }
                 else if(since <= u)
                   {
                     // case (C)
                     if(until <= u)
                       {
                         attr_info.emplace_back(since, until, type, is_array);
                       }
                     // case (D)
                     else
                       {
                         attr_info.emplace_back(since, u, type, is_array);
                       }
                   }
               }
           }
       }

       pbeast::CallStats stat(max_message_size);
       pbeast::CallStats local_stat(max_local_ds_data_size);

       for(std::size_t idx = 0; idx < attr_info.size(); ++idx)
         {
           const uint64_t prev = (idx == 0 ? prev_interval : 0);
           const uint64_t next = (idx != (attr_info.size() - 1) ? 0 : next_interval);

           auto& x(attr_info[idx]);

           pbeast::print_tss(std::cout, x.m_since, x.m_until, tz_ptr, !print_raw_ts, "(INFO) - ");
           std::cout << " attribute type: " << pbeast::as_string(x.m_type) << ", is array: " << std::boolalpha << x.m_is_array << std::endl;

           switch(x.m_type)
           {
           case pbeast::BOOL:
             read_and_print<bool>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::S8:
             read_and_print<int8_t>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::U8:
             read_and_print<uint8_t>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::S16:
             read_and_print<int16_t>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::U16:
             read_and_print<uint16_t>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::ENUM:
           case pbeast::S32:
             read_and_print<int32_t>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::DATE:
           case pbeast::U32:
             read_and_print<uint32_t>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::S64:
             read_and_print<int64_t>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::TIME:
           case pbeast::U64:
             read_and_print<uint64_t>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::FLOAT:
             read_and_print<float>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::DOUBLE:
             read_and_print<double>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::STRING:
             read_and_print<std::string>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           case pbeast::VOID:
             read_and_print<pbeast::Void>(db, partition_name, class_name, attribute_path, x.m_since, x.m_until, prev, next, interval, fill_gaps, x.m_is_array, object_name, object_name_is_regexp, object_suffix_name, object_suffix_is_regexp, tz_ptr, print_every_update, print_raw_ts, stat, local_stat);
             break;

           default:
              {
                std::ostringstream text;
                text << "unsupported type " << x.m_type;
                throw std::runtime_error(text.str().c_str());
              }
           }
         }

      if (verbose)
        {
          stat.print(std::cout);

          if (local_stat.m_data_point_num)
            {
              std::cout << "In-memory downsampling ";
              local_stat.print(std::cout);
            }
        }
    }
  catch(std::exception& ex)
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE, ex));
      return 1;
    }

  return 0;
}
