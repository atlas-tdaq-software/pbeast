#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include <ers/ers.h>
#include <ipc/core.h>
#include <ipc/partition.h>

#include <pbeast/pbeast.hh>

#include "application_exceptions.h"
#include "utils.h"

namespace po = boost::program_options;

int
main(int argc, char ** argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (ers::Issue & ex)
    {
      ers::fatal(ers::Message(ERS_HERE, ex));
      return 1;
    }

  po::options_description desc(
      "P-BEAST server commander.\n\n"
      "Available options are"
  );

  std::string server_name;
  std::string ipc_partition_name("initial");
  std::string command_name;
  std::vector<std::string> command_args;

  // parse command line
  try
    {
      desc.add_options()
        ( "server,n",  po::value<std::string>(&server_name)->required(), "Name of pbeast server" )
        ( "partition,p", po::value<std::string>(&ipc_partition_name)->default_value(ipc_partition_name), "Name of partition" )
        ( "command,c", po::value<std::string>(&command_name)->required(), "Command name" )
        ( "arguments,a", po::value<std::vector<std::string>>(&command_args)->multitoken(), "Command arguments" )
        ( "help,h", "Print help message");

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);
    }
  catch (std::exception& ex)
    {
      ers::fatal( daq::pbeast::CommandLineError( ERS_HERE, ex.what() ) );
      return 1;
    }

  try
    {
      IPCPartition p(ipc_partition_name);

      pbeast::server_var db = p.lookup<pbeast::server>(server_name);

      pbeast::StringArray cmd_args;
      cmd_args.length(command_args.size());

      for (size_t i = 0; i < command_args.size(); ++i)
        cmd_args[i] = pbeast::utils::string_dup(command_args[i]);

      CORBA::String_var log;

      db->send_command(command_name.c_str(), cmd_args, log);

      std::cout << "[SERVER]: " << static_cast<const char *>(log);
    }
  catch ( pbeast::Failure& ex )
    {
      ers::fatal(daq::pbeast::Error(ERS_HERE, "failure", ex.text));
      return 1;
    }
  catch ( pbeast::BadRequest& ex )
    {
      ers::fatal(daq::pbeast::Error(ERS_HERE, "error", ex.text));
      return 1;
    }
  catch ( daq::ipc::Exception & ex )
    {
      ers::fatal(ex);
      return 1;
    }
  catch ( CORBA::SystemException & ex )
    {
      ers::fatal(daq::ipc::CorbaSystemException(ERS_HERE, &ex));
      return 1;
    }
  catch ( std::exception & ex )
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE));
      return 1;
    }

  return 0;
}
