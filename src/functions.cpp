#include <sstream>
#include <pbeast/functions.h>

static constexpr char s_next_separator = ',';
static constexpr char s_value_separator = ':';

template<typename T, uint32_t N>
  std::string
  __make_list_of_names(T const (&data)[N])
  {
    std::string s;

    for (uint32_t i = 0; i < N; ++i)
      {
        if (!s.empty())
          s.append(", ");

        s.push_back('\"');
        s.append(data[i].function_name);
        s.push_back('\"');
      }

    return s;
  }


template<typename C, typename T, uint32_t N>
  void
  __vec_to_string(std::string& s, const C& conf, T const (&data)[N])
  {
    for (const auto& x : conf)
      {
        if (!s.empty())
          s.push_back(s_next_separator);

        for (uint32_t i = 0; i < N; ++i)
          if (x.first == data[i].type)
            {
              s.append(data[i].function_name);
              if (x.second.empty() == false)
                {
                  s.push_back(s_value_separator);
                  s.append(x.second);
                }
              break;
            }
      }
  }


template<typename X, typename T, uint32_t N>
const std::string&
__function_name(X type, T const (&data)[N])
{
  static std::string unknown("unknown");

  for (uint32_t i = 0; i < N; ++i)
    if (type == data[i].type)
      return data[i].function_name;

  return unknown;
}


struct function_t
{
  std::string function_name;
  pbeast::FunctionType type;
} function_names[] =
  {
    { pbeast::FunctionConfig::s_select_average_above, pbeast::SelectAverageAbove },
    { pbeast::FunctionConfig::s_select_average_below, pbeast::SelectAverageBelow },
    { pbeast::FunctionConfig::s_select_current_above, pbeast::SelectCurrentAbove },
    { pbeast::FunctionConfig::s_select_current_below, pbeast::SelectCurrentBelow },
    { pbeast::FunctionConfig::s_select_highest_average, pbeast::SelectHighestAverage },
    { pbeast::FunctionConfig::s_select_highest_current, pbeast::SelectHighestCurrent },
    { pbeast::FunctionConfig::s_select_highest_max, pbeast::SelectHighestMax },
    { pbeast::FunctionConfig::s_select_lowest_average, pbeast::SelectLowestAverage },
    { pbeast::FunctionConfig::s_select_lowest_current, pbeast::SelectLowestCurrent },
    { pbeast::FunctionConfig::s_sum_array, pbeast::SumArray },
    { pbeast::FunctionConfig::s_summarize, pbeast::Summarize },
    { pbeast::FunctionConfig::s_remove_above_value, pbeast::RemoveAboveValue },
    { pbeast::FunctionConfig::s_remove_below_value, pbeast::RemoveBelowValue },
    { pbeast::FunctionConfig::s_remove_corrupted, pbeast::RemoveCorrupted }
  };

struct aggregations_t
{
  std::string function_name;
  pbeast::AggregationType type;
} aggregation_names[] =
  {
    { pbeast::FunctionConfig::s_aggegation_min, pbeast::AggregationMin },
    { pbeast::FunctionConfig::s_aggegation_max, pbeast::AggregationMax },
    { pbeast::FunctionConfig::s_aggegation_sum, pbeast::AggregationSum },
    { pbeast::FunctionConfig::s_aggegation_avg, pbeast::AggregationAverage }
  };


struct alias_t
{
  std::string function_name;
  pbeast::AliasType type;
} alias_names[] =
  {
    { pbeast::FunctionConfig::s_alias, pbeast::Alias },
    { pbeast::FunctionConfig::s_alias_by_metric, pbeast::AliasByMetric },
    { pbeast::FunctionConfig::s_alias_by_node, pbeast::AliasByNode },
    { pbeast::FunctionConfig::s_alias_sub, pbeast::AliasSub }
  };

void
pbeast::FunctionConfig::validate(uint32_t interval) const
{
  if (no_aggregations() == false && interval == 0)
    throw std::runtime_error("cannot use aggregation functions with 0 downsample interval");
}

void
pbeast::FunctionConfig::construct(const std::string& stream, uint32_t interval)
{
  if (!stream.empty())
    {
      std::istringstream f(stream);

      std::string buf;
      while (std::getline(f, buf, s_next_separator))
        validate(buf);
    }

  validate(interval);
}

void
pbeast::FunctionConfig::construct(const std::vector<std::string>& command_line, uint32_t interval)
{
  for (const auto& x : command_line)
    validate(x);

  validate(interval);
}

void
pbeast::FunctionConfig::validate(FunctionType type, const std::string& value)
{
  if((type != pbeast::FunctionType::SumArray && type != pbeast::FunctionType::Summarize&& type != pbeast::FunctionType::RemoveCorrupted) && value.empty())
    {
      std::ostringstream text;
      text << "function: \"" << function_name(type) << "\" has no value parameter";
      throw std::runtime_error(text.str().c_str());
    }

  m_data.emplace_back(type, value);

  if (type == pbeast::FunctionType::Summarize)
    m_use_summarize = true;
}

void
pbeast::FunctionConfig::validate(AliasType type, const std::string& value)
{
  if(type != pbeast::AliasType::AliasByMetric && value.empty())
    {
      std::ostringstream text;
      text << "function: \"" << function_name(type) << "\" has no value parameter";
      throw std::runtime_error(text.str().c_str());
    }

  m_aliases.emplace_back(type, value);
}

void
pbeast::FunctionConfig::validate(AggregationType type, const std::string& as)
{
  if((m_types & type) || m_data_names.find(as) != m_data_names.end())
    {
      std::ostringstream text;
      text << "aggregation \"" << as << "\" is requested more than once";
      throw std::runtime_error(text.str().c_str());
    }

  m_types |= type;
  m_data_names[as] = type;
}

void
pbeast::FunctionConfig::validate(const std::string& command_line)
{
  std::string::size_type pos = command_line.find(s_value_separator);

  std::string f = command_line.substr(0, pos);
  std::string value;

  if (pos != std::string::npos)
    value = command_line.substr(pos + 1);

  for (uint32_t i = 0; i < sizeof(function_names) / sizeof(function_t); ++i)
    {
      if (f == function_names[i].function_name)
        {
          validate(function_names[i].type, value);
          return;
        }
    }

  for (uint32_t i = 0; i < sizeof(alias_names) / sizeof(alias_t); ++i)
    {
      if (f == alias_names[i].function_name)
        {
          validate(alias_names[i].type, value);
          return;
        }
    }

  for (uint32_t i = 0; i < sizeof(aggregation_names) / sizeof(aggregations_t); ++i)
    {
      if (f == aggregation_names[i].function_name)
        {
          validate(aggregation_names[i].type, value.empty() ? f : value);
          return;
        }
    }

  if (f == "keep-data")
    {
      m_keep_data = (value == "true");
      return;
    }

  std::ostringstream text;
  text << "unexpected name of function: \"" << f << '\"';
  throw std::runtime_error(text.str().c_str());
}

pbeast::AggregationType
pbeast::FunctionConfig::get_type(const std::string& s) const
{
  std::map<std::string,AggregationType>::const_iterator it = m_data_names.find(s);

  if(it != m_data_names.end())
    return it->second;

  return AggregationNone;
}

std::string
pbeast::FunctionConfig::to_string() const
{
  std::string s;

  __vec_to_string(s, m_aliases, alias_names);

  __vec_to_string(s, m_data, function_names);

  if (no_aggregations() == false)
    {
      for (const auto& x : m_data_names)
        {
          if (!s.empty())
            s.push_back(s_next_separator);

          for (uint32_t i = 0; i < sizeof(aggregation_names) / sizeof(aggregations_t); ++i)
            if (x.second == aggregation_names[i].type)
              {
                s.append(aggregation_names[i].function_name);
                if (x.first != aggregation_names[i].function_name)
                  {
                    s.push_back(s_value_separator);
                    s.append(x.first);
                  }
              }
        }

      s.push_back(s_next_separator);
      s.append("keep-data");
      s.push_back(s_value_separator);
      s.append(m_keep_data ? "true" : "false");
    }

  return s;
}

const std::string&
pbeast::FunctionConfig::function_name(AliasType type)
{
  return __function_name(type, alias_names);
}

const std::string&
pbeast::FunctionConfig::function_name(FunctionType type)
{
  return __function_name(type, function_names);
}

std::string
pbeast::FunctionConfig::make_list_of_alias_names()
{
  return __make_list_of_names(alias_names);
}

std::string
pbeast::FunctionConfig::make_list_of_function_names()
{
  return __make_list_of_names(function_names);
}

std::string
pbeast::FunctionConfig::make_list_of_aggregation_names()
{
  return __make_list_of_names(aggregation_names);
}

std::string
pbeast::FunctionConfig::make_description_of_functions()
{
  return std::string("list of functions in format function:value, where the \"function\" is one of ") + make_list_of_function_names() +
      " and \"value\" is a function parameter, and/or list of aliases in format alias[:how], where the \"alias\" is one of " + make_list_of_alias_names() +
      " and optional \"how\" is an alias parameter, and/or list of aggregations in format aggregation[:as], where the \"aggregation\" is one of " +
      make_list_of_aggregation_names() + " and optional \"as\" is a name of aggregated data serie";
}

char
pbeast::FunctionConfig::get_value_separator()
{
  return s_value_separator;
}
