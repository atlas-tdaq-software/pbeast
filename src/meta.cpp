#include "pbeast/exceptions.h"
#include "pbeast/meta.h"
#include "pbeast/input-data-file.h"
#include "pbeast/output-data-file.h"

#include <algorithm>

uint64_t pbeast::Meta::s_info_ts(0);

static void
init(const std::filesystem::path& path, std::map<std::string, std::vector<pbeast::SeriesData<std::string>>>& series, std::string& read_partition, std::string& read_class, const std::string& class_name)
{
  bool read_file(false);

  try
    {
      if (class_name.size() <= NAME_MAX && std::filesystem::exists(path) == true)
        {
          pbeast::InputDataFile f(path.native());

          std::vector<pbeast::SeriesData<std::string>> data;
          std::string name;

          while (f.read_next(name, data))
            {
              series[name] = data;
              data.clear();
            }

          read_partition = f.get_partition();
          read_class = f.get_class();

          read_file = true;
        }
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "access file", path, ex.code().message());
    }
  catch(std::exception & ex)
    {
      throw daq::pbeast::FileError(ERS_HERE, "read", path, ex);
    }

  if(read_file == false && class_name.empty() == false)
    {
      throw daq::pbeast::NoSuchClass(ERS_HERE, class_name);
    }
}

void
pbeast::Meta::initialise(const std::filesystem::path& path, const std::string& class_name)
{
  m_types_path = path / s_types_meta_filename;
  m_descriptions_path = path / s_descriptions_meta_filename;

  std::string dummu1, dummu2;

  init(m_types_path, m_types, dummu1, dummu2, class_name);
  init(m_descriptions_path, m_descriptions, dummu1, dummu2, class_name);
}

pbeast::Meta::Meta(const std::filesystem::path& path)
{
  static std::string empty;
  initialise(path, empty);
}

pbeast::Meta::Meta(const std::filesystem::path& repository_path, const std::string& partition_name, const std::string& class_name)
{
  pbeast::DataFile::validate_parameters(partition_name, class_name, "");

  initialise(repository_path / pbeast::DataFile::encode(partition_name) / pbeast::DataFile::encode(class_name), class_name);
}

void
pbeast::Meta::update_attribute(std::vector<pbeast::SeriesData<std::string>> & series, const std::string& value)
{
  const unsigned int len = series.size();

  if (len > 0 && series[0].get_value() == value && s_info_ts <= series[0].get_ts_last_updated())
    {
      if (s_info_ts < series[0].get_ts_created())
        series[0].set_ts_created(s_info_ts);

      return;
    }

  if (len > 0 && series[len - 1].get_value() == value)
    {
      if (s_info_ts > series[len - 1].get_ts_last_updated())
        series[len - 1].set_ts_last_updated(s_info_ts);
    }
  else
    {
      series.emplace_back(s_info_ts, s_info_ts, value);
    }
}

void
pbeast::Meta::update_attribute(const std::string& name, const std::string& type, const std::string& description)
{
  update_attribute(m_types[name], type);
  update_attribute(m_descriptions[name], description);
}

static void
commit_data(const std::string &partition, const std::string &class_name, const std::filesystem::path &path,
            const std::map<std::string, std::vector<pbeast::SeriesData<std::string>>> &data)
{
  std::filesystem::path tmp_path(path);
  tmp_path += ".tmp";

  uint64_t base_ts(std::numeric_limits<uint64_t>::max());

  for (const auto& x : data)
    {
      if (!x.second.empty())
        {
          const uint64_t first(x.second.begin()->get_ts_created());

          if (first < base_ts)
            base_ts = first;
        }
    }

  try
    {
      pbeast::OutputDataFile f(tmp_path.native(), partition, class_name, "", "", base_ts, pbeast::STRING, false, data.size(), false, false, false, 0);

      f.write_header();

      for (auto &x : data)
        f.write_data(x.first, x.second);

      f.write_footer();

      f.close();
    }
  catch (std::exception &ex)
    {
      throw daq::pbeast::FileError(ERS_HERE, "write", tmp_path, ex);
    }

  try
    {
      std::filesystem::rename(tmp_path, path);
    }
  catch (std::filesystem::filesystem_error &ex)
    {
      throw daq::pbeast::FileSystemOperationError(ERS_HERE, "rename file", tmp_path, path, ex.code().message());
    }
}

void
pbeast::Meta::commit(const std::string& partition, const std::string& class_name)
{
  commit_data(partition, class_name, m_types_path, m_types);
  commit_data(partition, class_name, m_descriptions_path, m_descriptions);
}

const char _mv_char_('*');
const char _sv_char_('1');
static const std::string _mv_str_(1,_mv_char_);
static const std::string _sv_str_(1,_sv_char_);

std::string
pbeast::Meta::encode_type(const std::string& type, bool is_mv)
{
  std::string value(is_mv ? _mv_str_ : _sv_str_);
  value.append(type);
  return value;
}

void
pbeast::Meta::decode_type(const std::string& value, std::string& type, bool& is_mv) noexcept
{
  is_mv = (value[0] == _mv_char_);
  type = value.substr(1);
}

void
pbeast::Meta::get_attribute_class_and_name(const std::filesystem::path& attribute_path, std::string& class_name, std::string& attribute_name)
{
  attribute_name.clear();

  for (std::filesystem::path::iterator it(attribute_path.begin()), it_end(attribute_path.end()); it != it_end; ++it)
    {
      class_name = attribute_name;
      attribute_name = (*it).native();
    }
}

const std::vector<pbeast::SeriesData<std::string>>&
pbeast::Meta::get_attribute_data(const std::string& name, const std::map<std::string, std::vector<SeriesData<std::string>>>& src) const
{
  std::map<std::string, std::vector<SeriesData<std::string>>>::const_iterator i = src.find(name);

  if(i != src.end())
    {
      return i->second;
    }
  else
    {
      throw daq::pbeast::NoSuchAttribute(ERS_HERE, name);
    }
}


static std::vector<pbeast::SeriesData<std::string>>
merge_vec(const std::vector<pbeast::SeriesData<std::string>>& v1, const std::vector<pbeast::SeriesData<std::string>>& v2)
{
  std::map<uint64_t, std::string> updates;

  for (const auto& x : v1)
    {
      updates[x.get_ts_created()] = x.get_value();
      updates[x.get_ts_last_updated()] = x.get_value();
    }

  for (const auto& x : v2)
    {
      updates[x.get_ts_created()] = x.get_value();
      updates[x.get_ts_last_updated()] = x.get_value();
    }

  std::vector<pbeast::SeriesData<std::string>> out;

  out.reserve(updates.size());

  std::string last;

  for (const auto& x : updates)
    {
      if(out.empty())
        {
          out.emplace_back(x.first, x.first, x.second);
        }
      else
        {
          if(out.back().get_value() == x.second)
            {
              out.back().set_ts_last_updated(x.first);
            }
          else
            {
              out.emplace_back(x.first, x.first, x.second);
            }
        }
    }

  return out;
}


static std::map<std::string, std::vector<pbeast::SeriesData<std::string>>>
merge_values(const std::map<std::string, std::vector<pbeast::SeriesData<std::string>>>& v1, const std::map<std::string, std::vector<pbeast::SeriesData<std::string>>>& v2)
{
  std::map<std::string, std::vector<pbeast::SeriesData<std::string>>> out;

  for(auto& x : v1)
    {
      auto i = v2.find(x.first);

      if(i != v2.end())
        out[x.first] = merge_vec(x.second, i->second);
      else
        out[x.first] = x.second;
    }

  for(auto& x : v2)
    {
      auto i = v1.find(x.first);

      if(i == v1.end())
        out[x.first] = x.second;
    }

  return out;
}

void
pbeast::Meta::merge(const Meta& info)
{
  std::map<std::string, std::vector<pbeast::SeriesData<std::string>>> new_types = merge_values(get_all_types(), info.get_all_types());
  std::map<std::string, std::vector<pbeast::SeriesData<std::string>>> new_descs = merge_values(get_all_descriptions(), info.get_all_descriptions());

  m_types = new_types;
  m_descriptions = new_descs;
}

void
pbeast::Meta::check_and_merge_files(const std::filesystem::path& file_from, const std::filesystem::path& file_to)
{
  // copy file, if it does not exist

  try
    {
      if (std::filesystem::exists(file_to) == false)
        {
          std::filesystem::copy_file(file_from, file_to);
          ERS_LOG("copy file " << file_from << " to " << file_to);
          return;
        }
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemOperationError(ERS_HERE, "copy file", file_from, file_to, ex.code().message());
    }


  // read both files, compare and merge

  std::map<std::string, std::vector<pbeast::SeriesData<std::string>>> series_from, series_to;
  std::string partition_name, class_name;

  init(file_from, series_from, partition_name, class_name, "");
  init(file_to, series_to, partition_name, class_name, "");

  auto result = merge_values(series_from, series_to);


  // write new file if different

  if (result != series_to)
    {
      commit_data(partition_name, class_name, file_to, result);
      ERS_LOG("merge file " << file_from << " with " << file_to);
    }
  else
    {
      ERS_DEBUG(0, "no need to merge " << file_from << " with " << file_to);
    }
}

