#include <string.h>
#include <curl/curl.h>

#include <filesystem>
#include <limits>

#include <boost/algorithm/string.hpp>

#include <ers/ers.h>

#include <system/User.h>
#include <system/Host.h>

#include <sso-helper/sso-helper.h>

#include <pbeast/downsample-algo.h>
#include <pbeast/input-data-file.h>
#include <pbeast/file-response.h>
#include <pbeast/query.h>
#include <pbeast/curl-request.h>
#include <pbeast/object-name.h>

#include "gason.h"
#include "query-internal.h"
#include "system-command-output.h"

static System::User s_user;

std::string pbeast::HttpHeaderFields::user_login = "PBEAST-login";
std::string pbeast::HttpHeaderFields::full_username = "PBEAST-fullname";
std::string pbeast::HttpHeaderFields::user_host = "PBEAST-host";
std::string pbeast::HttpHeaderFields::x_forwarded_for = "X-Forwarded-For";
std::string pbeast::HttpHeaderFields::x_forwarded_fullname = "X-Forwarded-fullname";
std::string pbeast::HttpHeaderFields::x_forwarded_login = "X-Forwarded-login";
std::string pbeast::HttpHeaderFields::x_grafana_username = "Grafana-username";


namespace pbeast
{
  CurlRequest::CurlRequest(void *result_handler, const pbeast::ServerProxy &proxy) :
      m_proxy(proxy), m_result_handler(result_handler), m_multipart(nullptr)
  {
    m_request << m_proxy.get_base_url();
  }

  void
  CurlRequest::add_post_multipart(const char *name, const char *ptr, long size)
  {
    if (!m_multipart)
      m_multipart = curl_mime_init(pbeast::curl::get_handle());

    if (curl_mimepart *part = curl_mime_addpart(m_multipart))
      {
        curl_mime_name(part, name);
        curl_mime_data(part, ptr, size);
      }
  }


  void
  CurlRequest::execute(WriterFunction f)
  {
    CURL * curl = pbeast::curl::get_handle();

    const std::string request(m_request.str());

    ERS_DEBUG(2, "call " << request);

    curl_easy_setopt(curl, CURLOPT_URL, request.c_str());
    curl_easy_setopt(curl, CURLOPT_FOLLOWLOCATION, 1L);

    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, m_proxy.m_http_header);

    if (m_proxy.get_cookie_file_name().empty() == false)
      {
        m_proxy.refresh_cookie();

        ERS_DEBUG(1, "set cookie file " << m_proxy.get_cookie_file_name());
        curl_easy_setopt(curl, CURLOPT_COOKIEFILE, m_proxy.get_cookie_file_name().c_str());
      }

    if (!m_proxy.get_https_proxy().empty())
      {
        ERS_DEBUG(1, "use http proxy \"" << m_proxy.get_https_proxy() << "\" authentication");
        curl_easy_setopt(curl, CURLOPT_PROXY, m_proxy.get_https_proxy().c_str());
        curl_easy_setopt(curl, CURLOPT_PROXYUSERPWD, ":");
        curl_easy_setopt(curl, CURLOPT_PROXYAUTH, CURLAUTH_GSSNEGOTIATE);
      }

    if (ers::debug_level() > 1)
      curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);

    curl_easy_setopt(curl, CURLOPT_NOPROGRESS, 1L);

    if (m_multipart)
      curl_easy_setopt(curl, CURLOPT_MIMEPOST, m_multipart);

    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, f);

    curl_easy_setopt(curl, CURLOPT_WRITEDATA, m_result_handler);

    // curl_easy_setopt(curl, CURLOPT_BUFFERSIZE, 128 * 1024L);

    CURLcode res = curl_easy_perform(curl);
    if (res != CURLE_OK)
      throw daq::pbeast::HttpRequestFailure(ERS_HERE, request, curl_easy_strerror(res), res);

    long http_code = 0;
    curl_easy_getinfo(curl, CURLINFO_RESPONSE_CODE, &http_code);

    if (is_failed() || http_code != 200)
      throw daq::pbeast::HttpRequestFailure(ERS_HERE, request, get_error_text(), http_code);

    process_result();
  }

  CurlRequest::~CurlRequest()
  {
    curl_easy_reset(pbeast::curl::get_handle());

    if (m_multipart)
      curl_mime_free(m_multipart);
  }

  size_t
  CurlSimpleBuffer::get(void *v_ptr, size_t size, size_t nmemb, void *stream)
  {
    reinterpret_cast<CurlSimpleBuffer *>(stream)->m_buffer.append((char *)v_ptr, size * nmemb);
    return size * nmemb;
  }
}


struct MultiDataFileResponseClient : pbeast::MultiDataFileResponse
{
  MultiDataFileResponseClient() :
    pbeast::MultiDataFileResponse(std::ios_base::in), m_num_files(-1), m_num_eov(0), m_num_upd(0), m_header_len(0), m_read_bytes(0)
  {
    ;
  }

  // goal         - expected size of stream
  // available    - available bytes in buffer
  // return true when buffer is ready and false in case of underflow
  bool
  fill_stream(char *& ptr, size_t& available, std::stringstream& s, size_t goal)
  {
    if ((s.str().size() + available) >= goal)
      {
        size_t read_now = goal - s.str().size();
        s.write(ptr, read_now);
        ptr += read_now;
        m_read_bytes += read_now;
        available -= read_now;
        return true;
      }
    else
      {
        s.write(ptr, available);
        m_read_bytes += available;
        return false;
      }
  }

  bool
  fill_stream(char *& ptr, size_t& available, std::string& buf, size_t goal)
  {
    if ((buf.size() + available) >= goal)
      {
        size_t read_now = goal - buf.size();
        buf.append(ptr, read_now);
        ptr += read_now;
        m_read_bytes += read_now;
        available -= read_now;
        return true;
      }
    else
      {
        buf.append(ptr, available);
        m_read_bytes += available;
        return false;
      }
  }

  bool
  is_failed() const
  {
    return (m_num_eov == -1);
  }

  void
  set_failed()
  {
    m_num_eov = -1;
  }

  void
  read_error_text(char *& ptr, size_t& available)
  {
    fill_stream(ptr, available, m_first, std::numeric_limits<int>::max());
  }

  static size_t
  get(void *v_ptr, size_t size, size_t nmemb, void *stream)
  {
    MultiDataFileResponseClient * th = reinterpret_cast<MultiDataFileResponseClient *>(stream);
    char * ptr = (char *) v_ptr;
    size_t available = size * nmemb;

    if (th->is_failed())
      {
        th->read_error_text(ptr, available);
        return size * nmemb;
      }

    // read first (detect size of header)
    if (!th->m_header_len)
      {
        if (th->fill_stream(ptr, available, th->m_first, pbeast::MultiDataFileResponse::num_width))
          {
            const std::string first_str(th->m_first.str());
            if (first_str.empty() || first_str[0] == '<' || !isdigit(first_str.back()))
              {
                th->set_failed();
                th->read_error_text(ptr, available);
                return size * nmemb;
              }

            th->m_first >> th->m_header_len;
            ERS_DEBUG(3, "header len: " << th->m_header_len);
          }
        else
          {
            return size * nmemb;
          }
      }

    if (th->m_num_files == -1)
      {
        // read header
        if (th->fill_stream(ptr, available, th->m_header, th->m_header_len))
          {
            th->m_header >> th->m_num_files >> th->m_num_eov >> th->m_num_upd;

            for (int i = 0; i < th->m_num_files; ++i)
              th->m_data.push_back(new pbeast::DataFileResponse(std::ios_base::in));

            for (int i = 0; i < (th->m_num_files + th->m_num_eov + th->m_num_upd); ++i)
              {
                uint64_t len;
                th->m_header >> len;
                th->m_file_lengths.push_back(len);
                th->get_file_by_idx(i)->reserve(len + 1);
              }
          }
        else
          {
            return size * nmemb;
          }
      }

    for (int i = 0; i < (int) th->m_file_lengths.size(); ++i)
      {
        pbeast::DataFileResponse * f = th->get_file_by_idx(i);

        if (f->m_buffer.size() < th->m_file_lengths[i])
          {
            if (!th->fill_stream(ptr, available, f->m_buffer, th->m_file_lengths[i]))
              {
                return size * nmemb;
              }
            else
              {
                f->m_data.str(f->m_buffer);
                ERS_DEBUG(1, "read file #" << i << " (" << f->m_buffer.size() << " bytes)");
              }
          }
      }

    return size * nmemb;
  }

  pbeast::DataFileResponse *
  get_file_by_idx(int i)
  {
    return (i < m_num_files ? m_data[i] : (i == m_num_files && m_num_eov ? &m_eov : &m_upd));
  }

  std::stringstream m_first;
  std::stringstream m_header;
  std::vector<uint64_t> m_file_lengths;
  int m_num_files;
  int m_num_eov;
  int m_num_upd;
  int m_header_len;
  uint64_t m_read_bytes;
};

static std::string
get_parameter(const char * var)
{
  if (char * v = getenv(var))
    {
      return v;
    }
  else
    {
      return "";
    }
}

static curl_slist *
add_http_header_field(curl_slist * http_header, const std::string& name, const std::string& value)
{
  std::string str(name);
  str.append(": ");
  str.append(value);
  return curl_slist_append(http_header, str.c_str());
}

pbeast::ServerProxy::ServerProxy(const std::string& base_url, CookieSetup cookie_setup_type, const std::filesystem::path& user_cert_dir) :
    m_base_url(base_url), m_cookie_setup_type(cookie_setup_type), m_https_proxy(get_parameter("PBEAST_SERVER_HTTPS_PROXY")), m_user_cert_dir(user_cert_dir), m_cookie_ts(0), m_cookie_refresh_interval(5 * 3600), m_http_header(curl_slist_append(nullptr, "Accept:"))
{
  if (m_base_url.empty())
    {
      m_base_url = get_parameter(pbeast::url_var::s_server);

      if (m_base_url.empty()) // FIXME: remove in tdaq release for 2024 (left for compatibility)
        m_base_url = get_parameter("PBEAST_SERVER_BASE_URL");

      if (m_base_url.empty())
        throw daq::pbeast::BadUserConfigurationParameter(ERS_HERE, "base url parameter is not set");
    }

  m_http_header = add_http_header_field(m_http_header, pbeast::HttpHeaderFields::user_login, s_user.name());
  m_http_header = add_http_header_field(m_http_header, pbeast::HttpHeaderFields::full_username, s_user.real_name());
  m_http_header = add_http_header_field(m_http_header, pbeast::HttpHeaderFields::user_host, System::LocalHost::full_local_name());

  if (m_cookie_setup_type == NotInitited)
    {
      std::string s = get_parameter("PBEAST_SERVER_SSO_SETUP_TYPE");

      if (s.empty())
        {
          m_cookie_setup_type = None;
        }
      else
        {
          boost::algorithm::to_lower(s);

          if (s == "none")
            m_cookie_setup_type = None;
          else if (s == "autoupdatekerberos" || s == "auto_update_kerberos")
            m_cookie_setup_type = AutoUpdateKerberos;
//          else if (s == "autoupdateusercertificates" || s == "auto_update_user_certificates")
//            m_cookie_setup_type = AutoUpdateUserCertificates;
          else
            {
              std::ostringstream text;
              text << "unexpected value of process environment variable PBEAST_SERVER_SSO_SETUP_TYPE=\"" << get_parameter("PBEAST_SERVER_SSO_SETUP_TYPE") << '\"';
              throw daq::pbeast::BadUserConfigurationParameter(ERS_HERE, text.str());
            }
        }
    }

  if (m_cookie_setup_type == None)
    {
      return;
    }

//  if (m_user_cert_dir.empty())
//    {
//      m_user_cert_dir = get_parameter("PBEAST_SERVER_SSO_USER_CERT_DIR");
//    }
//
//  if (m_cookie_setup_type == AutoUpdateUserCertificates)
//    {
//      if (m_user_cert_dir.empty())
//        {
//          m_user_cert_dir = static_cast<std::filesystem::path>(s_user.home()) / "private";
//          ERS_DEBUG(1, "user certificates directory is " << m_user_cert_dir);
//        }
//    }

  std::ostringstream cookie_dir_name;
  cookie_dir_name << "pbeast-sso-cookie." << s_user.name() << '.' << getpid();
  m_cookie_file_name = static_cast<std::filesystem::path>("/tmp") / cookie_dir_name.str() / "ssocookie.txt";
  ERS_DEBUG(1, "cookie file is " << m_cookie_file_name);

  const std::filesystem::path dir(m_cookie_file_name.parent_path());

  try
    {
      std::filesystem::create_directory(dir);
    }
  catch(std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "create directory", dir, ex.code().message());
    }

  try
    {
      std::filesystem::permissions(dir, std::filesystem::perms::owner_all);
    }
  catch(std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "set permission 0700 of", dir, ex.code().message());
    }
}


void
pbeast::ServerProxy::refresh_cookie() const
{
  time_t now = time(0);

  if (now - m_cookie_ts > m_cookie_refresh_interval)
    {
      try
        {
          daq::sso::get(m_base_url, m_cookie_file_name);
        }
      catch (const daq::sso::SSOException& ex)
        {
          throw daq::pbeast::CannotCreateCookieFile(ERS_HERE, ex);
        }

      m_cookie_ts = now;
    }
}


pbeast::ServerProxy::~ServerProxy() noexcept
{
  if (!m_cookie_file_name.empty())
    {
      ERS_DEBUG(1, "delete directory " << m_cookie_file_name.parent_path());
      std::filesystem::remove_all(m_cookie_file_name.parent_path());
    }

  curl_slist_free_all(m_http_header);
}

template<class T>
  std::set<T>&
  get_by_name(std::map<std::string, std::set<T>>& data, const std::string& name)
  {
    static std::set<T> empty;
    typename std::map<std::string, std::set<T>>::iterator i = pbeast::find(data, name);
    return (i != data.end() ? i->second : empty);
  }

template<class T>
  T
  next_value(std::set<T>& eov, T next_data)
  {
    typename std::set<T>::iterator eov_it = eov.begin();
    T eov_next = (eov_it != eov.end()) ? *eov_it : 0;

    if (eov_next && eov_next < next_data)
      {
        eov.erase(eov_it);
        return eov_next;
      }
    else
      {
        return 0;
      }
  }


template<class T> void
insert(uint64_t ts, std::vector<pbeast::DownsampledDataPoint<T>>& to, const pbeast::DownsampleValue<T>& data)
{
  to.emplace_back(ts, data.get_min_value(), data.get_value(), data.get_max_value());
}


template<class T>
  void
  insert(uint64_t ts, std::vector<pbeast::DownsampledDataPoint<std::vector<T>>>& to, const std::vector<pbeast::DownsampleValue<T>>& data)
    {
      std::vector<T> min_value, max_value;
      std::vector<typename pbeast::AverageType<T>::average_t> value;

      min_value.reserve(data.size());
      value.reserve(data.size());
      max_value.reserve(data.size());

      for (const auto& x : data)
        {
          min_value.push_back(x.get_min_value());
          value.push_back(x.get_value());
          max_value.push_back(x.get_max_value());
        }

      to.emplace_back(ts, min_value, value, max_value);
    }


std::string
pbeast::url_encode(CURL * curl, const std::string& value)
{
  std::string out;

  if (char *s = curl_easy_escape(curl, value.c_str(), value.length()))
    {
      out = s;
      curl_free(s);
    }

  return out;
}

template<class T>
  bool
  check_need_for_insertion(std::vector<T>& vec, uint64_t ts, uint64_t since, uint64_t until)
  {
    if (vec.size() == 1 && ts <= since)
      {
        ERS_DEBUG(2, "remove value with ts " << vec.front().ts() << " to be overwritten by value with ts " << ts << " (smaller \'since\')");
        vec.clear();
      }

    if (ts > until && vec.empty() == false && vec.back().ts() >= until)
      {
        ERS_DEBUG(2, "skip value with ts " << ts << " because a value with ts " << vec.back().ts() << " was already inserted (greater \'until\')");
        return false;
      }
    else
      {
        return true;
      }
  }


struct GetData : pbeast::CurlRequest
{
  std::vector<std::shared_ptr<pbeast::QueryDataBase>> m_out;
  std::map<std::string,std::set<uint32_t>> m_eov32, m_upd32;
  std::map<std::string,std::set<uint64_t>> m_eov64, m_upd64;
  MultiDataFileResponseClient m_resp;
  bool m_all_updates;
  bool m_is_sampled;
  uint64_t m_since;
  uint64_t m_until;

  GetData(const pbeast::ServerProxy& proxy, bool all_updates, bool is_sampled, uint64_t since, uint64_t until) :
    pbeast::CurlRequest(reinterpret_cast<void *>(&m_resp), proxy),
    m_all_updates(all_updates),
    m_is_sampled(is_sampled),
    m_since(since),
    m_until(until)
  {
  }

  bool
  is_failed() const
  {
    return m_resp.is_failed();
  }

  const std::string
  get_error_text() const
  {
    return m_resp.m_first.str();
  }

  template<class F, class T>
    void
    convert(const std::vector<F>& from, std::set<uint64_t>& eov, const std::set<uint64_t>& upd, bool all_updates, uint64_t next_data, std::vector<pbeast::DataPoint<T>>& to)
    {
      to.reserve(from.size() + eov.size());

      uint64_t eov_next = next_value(eov, next_data);

      std::set<uint64_t>::const_iterator upd_it;
      uint64_t upd_next = 0;
      if (all_updates)
        {
          upd_it = upd.begin();
          upd_next = *upd_it;
        }

      const auto len = from.size();

      for (uint32_t i = 0; i < len; ++i)
        {
          const auto& value(from[i].get_value());
          const uint64_t created_ts(from[i].get_ts_created());

          while (eov_next != 0 && eov_next < created_ts)
            {
              if (check_need_for_insertion(to, eov_next, m_since, m_until))
                to.emplace_back(eov_next);

              eov_next = next_value(eov, next_data);
            }

          if (all_updates)
            {
              while (upd_next != 0 && upd_next <= created_ts)
                {
                  upd_it++;
                  upd_next = (upd_it != upd.end()) ? *upd_it : 0;
                }

              if (check_need_for_insertion(to, created_ts, m_since, m_until))
                to.emplace_back(created_ts, value);

              uint64_t until(len - i > 1 ? from[i+1].get_ts_created() : m_until);

              if (eov_next && eov_next < until)
                until = eov_next;

              while (upd_next != 0 && upd_next < until)
                {
                  if (check_need_for_insertion(to, upd_next, m_since, m_until))
                    to.emplace_back(upd_next, value);

                  upd_it++;
                  upd_next = (upd_it != upd.end()) ? *upd_it : 0;
                }
            }
          else
            {
              const uint64_t last_updated_ts(from[i].get_ts_last_updated());

              if (check_need_for_insertion(to, created_ts, m_since, m_until))
                to.emplace_back(created_ts, value);

              if (created_ts != last_updated_ts)
                if (check_need_for_insertion(to, last_updated_ts, m_since, m_until))
                  to.emplace_back(last_updated_ts, value);
            }
        }

      if (eov_next != 0 && check_need_for_insertion(to, eov_next, m_since, m_until))
        to.emplace_back(eov_next);
    }

  template<class F, class T>
    void
    convert(const std::vector<F> &data, std::set<uint32_t> &eov, const std::set<uint32_t> &upd, bool all_updates, uint32_t next_data, uint32_t sample_interval, std::vector<pbeast::DownsampledDataPoint<T>> &to)
    {
      #define EOV_ADD_CODE       to.emplace_back(pbeast::mk_ts(eov_next));
      #define DATA_ADD_CODE(TS)  insert(pbeast::mk_ts(TS), to, (*it).data());

      APPLY_DS_ALGO(data, eov, upd, EOV_ADD_CODE, DATA_ADD_CODE)
    }

  template<class T>
    pbeast::QueryDataBase *
    read_data(pbeast::InputDataFile * file, uint64_t next_data)
    {
      std::string name;

      if (file->get_is_array() == false)
        {
          if (file->is_downsampled() == false)
            {
              pbeast::QueryData<pbeast::DataPoint<T>> * result = new pbeast::QueryData<pbeast::DataPoint<T>>(static_cast<pbeast::DataType>(file->get_data_type()), file->get_is_array());
              std::vector<pbeast::SeriesData<T>> data;

              while (file->read_next(name, data))
                {
                  auto& out_data(result->m_data[name]);
                  convert(data, get_by_name(m_eov64, name), get_by_name(m_upd64, name), m_all_updates, next_data, out_data);
                  if (!out_data.empty())
                    {
                      result->align_since(out_data.front().ts());
                      result->align_until(out_data.back().ts());
                    }
                  data.clear();
                }

              return result;
            }
          else
            {
              pbeast::QueryData<pbeast::DownsampledDataPoint<T>> * result = new pbeast::QueryData<pbeast::DownsampledDataPoint<T>>(static_cast<pbeast::DataType>(file->get_data_type()), file->get_is_array());
              std::vector<pbeast::DownsampledSeriesData<T> > data;

              while (file->read_next(name, data))
                {
                  auto& out_data(result->m_data[name]);
                  convert(data, get_by_name(m_eov32, name), get_by_name(m_upd32, name), m_all_updates, next_data, file->get_downsample_interval(), out_data);
                  if (!out_data.empty())
                    {
                      result->align_since(out_data.front().ts());
                      result->align_until(out_data.back().ts());
                    }
                  data.clear();
               }

              return result;
            }
        }
      else
        {
          if (file->is_downsampled() == false)
            {
              pbeast::QueryData<pbeast::DataPoint<std::vector<T>>> * result = new pbeast::QueryData<pbeast::DataPoint<std::vector<T>>>(static_cast<pbeast::DataType>(file->get_data_type()), file->get_is_array());
              std::vector<pbeast::SeriesVectorData<T>> data;

              while (file->read_next(name, data))
                {
                  auto& out_data(result->m_data[name]);
                  convert(data, get_by_name(m_eov64, name), get_by_name(m_upd64, name), m_all_updates, next_data, out_data);
                  if (!out_data.empty())
                    {
                      result->align_since(out_data.front().ts());
                      result->align_until(out_data.back().ts());
                    }
                  data.clear();
                }

              return result;
            }
          else
            {
              pbeast::QueryData<pbeast::DownsampledDataPoint<std::vector<T>>> * result = new pbeast::QueryData<pbeast::DownsampledDataPoint<std::vector<T>>>(static_cast<pbeast::DataType>(file->get_data_type()), file->get_is_array());
              std::vector<pbeast::DownsampledSeriesVectorData<T> > data;

              while (file->read_next(name, data))
                {
                  auto& out_data(result->m_data[name]);
                  convert(data, get_by_name(m_eov32, name), get_by_name(m_upd32, name), m_all_updates, next_data, file->get_downsample_interval(), out_data);
                  if (!out_data.empty())
                     {
                       result->align_since(out_data.front().ts());
                       result->align_until(out_data.back().ts());
                     }
                  data.clear();
                }

              return result;
            }
        }
    }


  virtual void
  process_result()
  {
    if (!m_resp.m_eov.m_buffer.empty())
      {
        pbeast::InputDataFile file(&m_resp.m_eov.m_data);

        std::string name;

        if (!m_is_sampled)
          {
            std::vector<pbeast::SeriesData<pbeast::Void> > data;

            while (file.read_next(name, data))
              {
                std::set<uint64_t>& v = m_eov64[name];
                std::set<uint64_t>::const_iterator hint = v.end();

                for (const auto& x : data)
                  {
                    hint = v.insert(hint, x.get_ts_created());
                  }

                data.clear();
              }
          }
        else
          {
            std::vector<pbeast::DownsampledSeriesData<pbeast::Void> > data;

            while (file.read_next(name, data))
              {
                std::set<uint32_t>& v = m_eov32[name];
                std::set<uint32_t>::const_iterator hint = v.end();

                for (const auto& x : data)
                  {
                    hint = v.insert(hint, x.get_ts());
                  }

                data.clear();
              }
          }
      }

    if (!m_resp.m_upd.m_buffer.empty())
      {
        pbeast::InputDataFile file(&m_resp.m_upd.m_data);

        std::string name;

          if (!m_is_sampled)
            {
              std::vector<pbeast::SeriesData<pbeast::Void> > data;

              while (file.read_next(name, data))
                {
                  std::set<uint64_t>& v = m_upd64[name];
                  std::set<uint64_t>::const_iterator hint = v.end();

                  for (const auto& x : data)
                    {
                      hint = v.insert(hint, x.get_ts_created());
                    }

                  data.clear();
                }
            }
          else
            {
              std::vector<pbeast::DownsampledSeriesData<pbeast::Void> > data;

              while (file.read_next(name, data))
                {
                  std::set<uint32_t>& v = m_upd32[name];
                  std::set<uint32_t>::const_iterator hint = v.end();

                  for (const auto& x : data)
                    {
                      hint = v.insert(hint, x.get_ts());
                    }

                  data.clear();
                }

            }
      }

    // read data (need to know information about all files, i.e. base timestamp of next data file)

    std::vector<std::unique_ptr<pbeast::InputDataFile>> files;
    files.reserve(m_resp.m_data.size());

    for (auto & x : m_resp.m_data)
      {
        files.emplace_back(new pbeast::InputDataFile(&x->m_data));
      }

    const unsigned int len = files.size();
    for (unsigned int i = 0; i < len; ++i)
      {
        pbeast::InputDataFile * file = files[i].get();
        uint64_t next_data = ((i + 1) < len ? files[i].get()->get_base_timestamp() : pbeast::max_ts_const);

        pbeast::QueryDataBase * query_data = (
          (file->get_data_type() == pbeast::BOOL) ? read_data<bool>(file, next_data) :
          (file->get_data_type() == pbeast::S8) ? read_data<int8_t>(file, next_data) :
          (file->get_data_type() == pbeast::U8) ? read_data<uint8_t>(file, next_data) :
          (file->get_data_type() == pbeast::S16) ? read_data<int16_t>(file, next_data) :
          (file->get_data_type() == pbeast::U16) ? read_data<uint16_t>(file, next_data) :
          (file->get_data_type() == pbeast::S32) ? read_data<int32_t>(file, next_data) :
          (file->get_data_type() == pbeast::U32) ? read_data<uint32_t>(file, next_data) :
          (file->get_data_type() == pbeast::S64) ? read_data<int64_t>(file, next_data) :
          (file->get_data_type() == pbeast::U64) ? read_data<uint64_t>(file, next_data) :
          (file->get_data_type() == pbeast::FLOAT) ? read_data<float>(file, next_data) :
          (file->get_data_type() == pbeast::DOUBLE) ? read_data<double>(file, next_data) :
          (file->get_data_type() == pbeast::ENUM) ? read_data<int32_t>(file, next_data) :
          (file->get_data_type() == pbeast::DATE) ? read_data<uint32_t>(file, next_data) :
          (file->get_data_type() == pbeast::TIME) ? read_data<uint64_t>(file, next_data) :
          (file->get_data_type() == pbeast::STRING) ? read_data<std::string>(file, next_data) :
          (file->get_data_type() == pbeast::VOID) ? read_data<pbeast::Void>(file, next_data) :
          nullptr
        );

        if (query_data)
          {
            m_out.push_back(std::shared_ptr<pbeast::QueryDataBase>(query_data));
          }
      }
  }

};

std::vector<std::shared_ptr<pbeast::QueryDataBase>>
pbeast::ServerProxy::get_data(const std::string& partition_name, const std::string& class_name, const std::string& attribute_path, const std::string& object_mask, bool is_regexp, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, bool get_all_updates, uint32_t sample_interval, pbeast::FillGaps::Type fill_gaps, const pbeast::FunctionConfig& fc, int compress_data, bool use_fqn) const
{
  GetData obj(*this, get_all_updates, (sample_interval != 0), pbeast::mk_wide_since(since, prev_interval), pbeast::mk_wide_until(until, next_interval));

  CURL * curl = pbeast::curl::get_handle();

  obj.request() << std::boolalpha << "/tdaq/pbeast/getData?"
       << "partition=" << url_encode(curl, partition_name)
       << "&class=" << url_encode(curl, class_name)
       << "&attribute=" << url_encode(curl, attribute_path) << "&object=" << url_encode(curl, object_mask)
       << "&showAllUpdates=" << get_all_updates
       << "&isRegexp=" << is_regexp << "&since=" << since << "&until=" << until << "&sampleInterval=" << sample_interval << "&fillGaps=" << pbeast::FillGaps::str(fill_gaps)
       << "&prevLookupLapse=" << prev_interval << "&nextLookupLapse=" << next_interval
       << "&zipCatalog=" << (bool)(compress_data & ZipCatalog)
       << "&zipData=" << (bool)(compress_data & ZipData)
       << "&catalogizeStrings=" << (bool)(compress_data & CatalogizeStrings)
       << "&useFQN=" << use_fqn;

  if (fc.is_null() == false)
    obj.request() << "&functions=" << url_encode(curl, fc.to_string());

  obj.execute(MultiDataFileResponseClient::get);

  return obj.m_out;
}

struct GetStringVector : pbeast::CurlRequest
{
  std::vector<std::string> m_out;
  pbeast::CurlSimpleBuffer m_resp;

  GetStringVector(const pbeast::ServerProxy& proxy) :
    pbeast::CurlRequest(reinterpret_cast<void *>(&m_resp), proxy)
  {
  }

  bool
  is_failed() const
  {
    return false;
  }

  const std::string
  get_error_text() const
  {
    return m_resp.m_buffer;
  }

  virtual void
  process_result()
  {
    ERS_DEBUG(1 , m_resp.m_buffer);

    char *endptr;
    JsonValue value;
    JsonAllocator allocator;

    char * source = const_cast<char *>(m_resp.m_buffer.c_str());

    int status = jsonParse(source, &endptr, &value, allocator);

    if (status != JSON_OK)
      {
        std::ostringstream text;
        text << "failed to parse json: " << jsonStrError(status) << " at " << (endptr - source);
        throw daq::pbeast::BadHttpResponse(ERS_HERE, text.str());
      }

   if (value.getTag() != JSON_OBJECT)
     {
       throw daq::pbeast::BadHttpResponse(ERS_HERE, "top-level json item is not an object");
     }

   std::string name;

   for (auto x : value)
     {
       if (!strcmp(x->key, "list"))
         {
           if (x->value.getTag() != JSON_ARRAY)
             {
               throw daq::pbeast::BadHttpResponse(ERS_HERE, "the \"list\" of top-level object is not an array");
             }

           unsigned int count(0);

           for (auto i : x->value)
             {
               count++;
               if (i->value.getTag() != JSON_STRING)
                 {
                   std::ostringstream text;
                   text << "the item #" << count << " of the top-level object \"list\" is not a string";
                   throw daq::pbeast::BadHttpResponse(ERS_HERE, text.str());
                 }

               m_out.push_back(i->value.toString());
             }
         }

     }
  }

};

const std::string autocomplete_series_name = "/tdaq/pbeast/autocompleteSeries?maxDataPoints=-1";

std::vector<std::string>
pbeast::ServerProxy::get_partitions() const
{
  GetStringVector obj(*this);

  obj.request() << autocomplete_series_name;

  obj.execute(pbeast::CurlSimpleBuffer::get);

  return obj.m_out;
}

std::vector<std::string>
pbeast::ServerProxy::get_classes(const std::string& partition_name) const
{
  GetStringVector obj(*this);

  obj.request() << autocomplete_series_name << "&stub=" << url_encode(pbeast::curl::get_handle(), partition_name);

  obj.execute(pbeast::CurlSimpleBuffer::get);

  return obj.m_out;
}

std::vector<std::string>
pbeast::ServerProxy::get_attributes(const std::string& partition_name, const std::string& class_name) const
{
  GetStringVector obj(*this);

  CURL * curl = pbeast::curl::get_handle();

  obj.request() << autocomplete_series_name << "&stub=" << url_encode(curl, partition_name) << '.' << url_encode(curl, class_name);

  obj.execute(pbeast::CurlSimpleBuffer::get);

  return obj.m_out;
}


std::vector<std::string>
pbeast::ServerProxy::get_objects(const std::string& partition_name, const std::string& class_name, const std::string& attribute_path, const std::string& object_mask, uint64_t since, uint64_t until) const
{
  GetStringVector obj(*this);

  CURL * curl = pbeast::curl::get_handle();

  obj.request() << autocomplete_series_name << "&stub=" << url_encode(curl, partition_name) << '.' << url_encode(curl, class_name) << '.' << url_encode(curl, attribute_path);

  if (!object_mask.empty())
    obj.request() << '.' << url_encode(curl, object_mask);

  if (since != pbeast::min_ts_const)
    obj.request() << '&' << "since=" << since;

  if (until != pbeast::max_ts_const)
    obj.request() << '&' << "until=" << until;

  obj.execute(pbeast::CurlSimpleBuffer::get);

  return obj.m_out;
}

