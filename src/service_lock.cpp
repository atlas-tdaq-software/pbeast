#include <unistd.h>

#include <fstream>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <filesystem>

#include <system/Host.h>
#include <system/User.h>

#include "pbeast/exceptions.h"
#include "pbeast/service_lock.h"

std::filesystem::path
pbeast::ServiceLock::check_repository_lock(const std::filesystem::path& repository_path, const std::string& lock_name)
{
  try
    {
      if (std::filesystem::exists(repository_path) == false)
        {
          throw daq::pbeast::FileSystemPathDoesNotExist(ERS_HERE, repository_path);
        }

      if(std::filesystem::is_directory(repository_path) == false)
        {
          throw daq::pbeast::FileSystemPathIsNotDirectory(ERS_HERE, repository_path);
        }
    }
  catch(std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "check repository directory", repository_path, ex.code().message());
    }

  std::filesystem::path file_path(repository_path / lock_name);

  try
    {
      if(std::filesystem::exists(file_path) == true)
        {
          std::ifstream f(file_path.native());
          std::string lock_file_contents;

          if(f.good())
            {
              std::getline(f, lock_file_contents);
            }
          else
            {
              lock_file_contents = "unknown [cannot read lock file \"";
              lock_file_contents += file_path.native();
              lock_file_contents += "\"]";
            }

          boost::interprocess::file_lock lock(file_path.c_str());

          if (lock.try_lock() == false)
            {
              throw daq::pbeast::RepositoryAlreadyLocked(ERS_HERE, repository_path, lock_file_contents);
            }
          else
            {
              try
                {
                  lock.unlock();
                }
              catch(std::exception& ex)
                {
                  throw daq::pbeast::CannotUnlockRepositoryLockFile(ERS_HERE, file_path, ex);
                }

              ers::warning(daq::pbeast::RemoveObsoleteRepositoryLock(ERS_HERE, repository_path, lock_file_contents));

              if(unlink(file_path.c_str()) != 0)
                {
                  throw daq::pbeast::CannotRemoveRepositoryLockFile(ERS_HERE, file_path, errno, strerror(errno));
                }
            }
        }
    }
  catch(daq::pbeast::Exception&)
    {
      throw;
    }
  catch(std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "access repository lock file", file_path, ex.code().message());
    }
  catch(const std::exception& ex)
    {
      throw daq::pbeast::CannotLockRepository(ERS_HERE, "lock", file_path, ex);
    }

  return file_path;
}

const char *
pbeast::ServiceLock::create_repository_lock_file(const std::filesystem::path& file_path)
{
  std::ofstream file(file_path.native());

  if(!file.good())
    {
      throw daq::pbeast::CannotLockRepository(ERS_HERE, "create", file_path);
    }

  try
    {
      boost::posix_time::ptime now(boost::posix_time::second_clock::universal_time());
      System::User myself;
      file << "process " << getpid() << " on " << System::LocalHost::instance()->full_local_name() << " from release " << TDAQ_RELEASE << " started by " << myself.name_safe() << " at " << boost::posix_time::to_simple_string(now) << " (UTC)" << std::endl;
      file.flush();
      file.close();
    }
  catch(std::exception& ex)
    {
      throw daq::pbeast::CannotLockRepository(ERS_HERE, "write to", file_path, ex);
    }

  return file_path.c_str();
}

pbeast::ServiceLock::ServiceLock(const std::filesystem::path& path, const std::string& lock_name) :
    m_lock_created          (false),
    m_repository_lock_file  (check_repository_lock(path, lock_name)),
    m_lock                  (create_repository_lock_file(m_repository_lock_file))
{
  m_lock_created = true;

  if (m_lock.try_lock() == false)
    {
      throw daq::pbeast::CannotLockRepository(ERS_HERE, "lock", m_repository_lock_file);
    }
}

pbeast::ServiceLock::~ServiceLock()
{
  if (m_lock_created)
    {
      try
        {
          try
            {
              if (std::filesystem::exists(m_repository_lock_file) == false)
                {
                  throw daq::pbeast::NoRepositoryLockFile(ERS_HERE,m_repository_lock_file);
                }
            }
          catch(std::filesystem::filesystem_error& ex)
            {
              throw daq::pbeast::FileSystemError(ERS_HERE, "check existence of repository lock file", m_repository_lock_file, ex.code().message());
            }

          try
            {
              m_lock.unlock();
            }
          catch(const std::exception& ex)
            {
              throw daq::pbeast::CannotUnlockRepositoryLockFile(ERS_HERE, m_repository_lock_file, ex);
            }

          if(unlink(m_repository_lock_file.c_str()) != 0)
            {
              throw daq::pbeast::CannotRemoveRepositoryLockFile(ERS_HERE, m_repository_lock_file, errno, strerror(errno));
            }
          }
      catch(ers::Issue &ex)
        {
          ers::error(daq::pbeast::RepositoryUnlockFailed(ERS_HERE, ex));
        }
    }
}
