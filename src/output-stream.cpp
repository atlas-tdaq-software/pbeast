#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include <sstream>
#include <stdexcept>

#include <ers/ers.h>

#include <google/protobuf/io/zero_copy_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>

#include "pbeast/output-stream.h"


void pbeast::OutputStream::create_streams(bool use_zip)
{
  if(m_fd)
    m_raw_output = new google::protobuf::io::FileOutputStream(m_fd);
  else
    m_raw_output = new google::protobuf::io::OstreamOutputStream(m_string_stream);

  reset_streams(use_zip);
}

void pbeast::OutputStream::reset_streams(bool use_zip)
{
  if (m_coded_output)
    delete m_coded_output;

  if (m_zip_output)
    delete m_zip_output;

  if(use_zip)
    {
      google::protobuf::io::GzipOutputStream::Options ops;
      ops.format = google::protobuf::io::GzipOutputStream::ZLIB;
      m_zip_output = new google::protobuf::io::GzipOutputStream(m_raw_output, ops);
      m_coded_output = new google::protobuf::io::CodedOutputStream(m_zip_output);
    }
  else
    {
      m_coded_output = new google::protobuf::io::CodedOutputStream(m_raw_output);
      m_zip_output = nullptr;
    }
}


uint64_t
pbeast::OutputStream::delete_streams()
{
  delete m_coded_output;
  m_coded_output = nullptr;

  if (m_zip_output)
    {
      delete m_zip_output;
      m_zip_output = nullptr;
    }

  uint64_t pos = m_position;

  if (m_raw_output)
    {
      pos += (uint64_t) m_raw_output->ByteCount();

      delete m_raw_output;
      m_raw_output = nullptr;
    }

  return pos;
}

pbeast::OutputStream::OutputStream(const std::string& file) :
    m_file(file), m_string_stream(nullptr), m_raw_output(nullptr), m_coded_output(nullptr), m_zip_output(nullptr), m_position(0)
{
  m_fd = open(m_file.c_str(), O_CREAT | O_WRONLY | O_TRUNC, 0664);

  if (m_fd > 0)
    {
      create_streams(false);
    }
  else
    {
      char buf[2048];
      std::ostringstream text;
      text << "open(\"" << m_file << "\") failed with code " << errno << ": "
          << strerror_r(errno, buf, sizeof(buf));
      throw std::runtime_error(text.str().c_str());
    }
}

pbeast::OutputStream::OutputStream(std::stringstream * stream) :
    m_fd(0), m_string_stream(stream), m_raw_output(nullptr), m_coded_output(nullptr), m_zip_output(nullptr), m_position(0)
{
  create_streams(false);
}


pbeast::OutputStream::~OutputStream()
{
  close_file(true);
}

uint64_t pbeast::OutputStream::close_file(bool no_throw)
{
  uint64_t pos(0);

  if (m_fd)
    {
      if (!no_throw)
        {
          check_state();
        }

      pos = delete_streams();

      if (close(m_fd) && !no_throw)
        {
          m_fd = 0;
          char buf[2048];
          std::ostringstream text;
          text << "close(\"" << m_file << "\") failed with code " << errno << ": " << strerror_r(errno, buf, sizeof(buf));
          throw std::runtime_error(text.str().c_str());
        }

      m_fd = 0;
    }
  else
    {
      pos = delete_streams();
    }

  return pos;
}

int64_t
pbeast::OutputStream::tell_pos() const
{
  if (m_fd)
    return lseek64(m_fd, 0, SEEK_CUR);
  else
    return (m_position + m_coded_output->ByteCount());
}

void pbeast::OutputStream::write(const std::string& v)
{
  const uint32_t len(v.size());
  write(len);
  m_coded_output->WriteRaw(v.data(), len);
}

void
pbeast::OutputStream::check_state()
{
  if (!m_string_stream)
    {
      if (int error = static_cast<google::protobuf::io::FileOutputStream*>(m_raw_output)->GetErrno())
        {
          std::ostringstream text;
          char buf[2048];
          text << "write to file \"" << m_file << "\" failed at position " << (m_position + m_coded_output->ByteCount()) << ", error " << error << ": " << strerror_r(errno, buf, sizeof(buf));
          throw std::runtime_error(text.str().c_str());
        }
    }
}

uint64_t
pbeast::OutputStream::check_and_reset(bool use_zip, int32_t limit)
{
  check_state();

  if(use_zip)
    {
      if(m_raw_output->ByteCount() > limit)
        {
#ifndef ERS_NO_DEBUG
          uint64_t old_pos = m_position;
#endif
          m_position = delete_streams();
          ERS_DEBUG(1, "reset zip stream on " << m_position << " (wrote " << (m_position - old_pos) << " bytes) at " << tell_pos());
          create_streams(true);
          return m_position;
        }
      else
        {
          reset_streams(true);
          return (m_position + m_raw_output->ByteCount());
        }
    }

  if (m_raw_output->ByteCount() > limit || m_zip_output)
    {
#ifndef ERS_NO_DEBUG
      uint64_t old_pos = m_position;
#endif
      m_position = delete_streams();

      if (!m_zip_output)
        ERS_DEBUG(1, "reset stream on " << m_position << " (wrote " << (m_position - old_pos) << " bytes) at " << tell_pos());

      create_streams(false);
      return m_position;
    }
  else
    {
      return (m_position + m_coded_output->ByteCount());
    }
}
