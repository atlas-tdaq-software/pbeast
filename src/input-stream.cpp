#include "pbeast/exceptions.h"
#include "pbeast/input-stream.h"

#include <errno.h>
#include <fcntl.h>
#include <string.h>

#include <sstream>
#include <stdexcept>
#include <iostream>

#include <ers/ers.h>

namespace daq {

  /*! \class pbeast::CannotCloseFile
   *  This issue is reported as ERS error when the file cannot be closed
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotCloseFile,
    Exception,
    "close(\"" << file << "\") failed with code " << code << ": " << message,
    ERS_EMPTY,
    ((std::string)file)
    ((int)code)
    ((const char *)message)
  )

}

void pbeast::InputStream::create_streams(bool use_zip)
{
  if (!m_string_stream)
    m_raw_input = new google::protobuf::io::FileInputStream(m_fd);
  else
    m_raw_input = new google::protobuf::io::IstreamInputStream(m_string_stream);


  if(use_zip)
    {
      m_zip_input = new google::protobuf::io::GzipInputStream(m_raw_input,  google::protobuf::io::GzipInputStream::Format::ZLIB);
      m_coded_input = new google::protobuf::io::CodedInputStream(m_zip_input);
    }
  else
    {
      m_coded_input = new google::protobuf::io::CodedInputStream(m_raw_input);
      m_zip_input = nullptr;
    }

  m_coded_input->SetTotalBytesLimit(INT_MAX);
}

void pbeast::InputStream::delete_streams()
{
  if(m_coded_input)
    {
      delete m_coded_input;
      m_coded_input = nullptr;
    }

  if(m_zip_input)
    {
      delete m_zip_input;
      m_zip_input = nullptr;
    }

  if(m_raw_input)
    {
      delete m_raw_input;
      m_raw_input = nullptr;
    }
}

void pbeast::InputStream::check_and_reset_zip_stream()
{
  int64_t cur_pos = m_coded_input->CurrentPosition();

  if (cur_pos > c_stream_size_limit)
    {
      ERS_DEBUG(1,"reset zip stream at " << cur_pos << " [file pos: " << tell_pos() << ']');
      delete m_coded_input;
      m_coded_input = new google::protobuf::io::CodedInputStream(m_zip_input);
#if GOOGLE_PROTOBUF_VERSION < 3007000
      m_coded_input->SetTotalBytesLimit(INT_MAX, -1);
#else
      m_coded_input->SetTotalBytesLimit(INT_MAX);
#endif
    }
}


pbeast::InputStream::InputStream(const std::string& file) : m_file(file), m_string_stream(nullptr), m_raw_input(nullptr), m_coded_input(nullptr), m_zip_input(nullptr)
{
  m_fd = open(file.c_str(), O_RDONLY);

  if(m_fd > 0) {
    create_streams(false);
  }
  else {
    char buf[2048];
    std::ostringstream text;
    text << "open(\"" << file << "\") failed with code " << errno << ": " << strerror_r(errno, buf, sizeof(buf));
    throw std::runtime_error(text.str().c_str());
  }
}

pbeast::InputStream::InputStream(std::stringstream * stream) : m_fd(0), m_string_stream(stream), m_raw_input(nullptr), m_coded_input(nullptr), m_zip_input(nullptr)
{
  create_streams(false);
}

pbeast::InputStream::~InputStream()
{
  delete_streams();

  if (!m_string_stream && close(m_fd))
    {
      char buf[2048];
      char * error_msg = strerror_r(errno, buf, sizeof(buf));
      ers::error(daq::pbeast::CannotCloseFile(ERS_HERE, m_file, errno, error_msg));
    }
}

void
pbeast::InputStream::clear_ss_eof()
{
  if (m_string_stream->rdstate() & std::ios_base::eofbit)
    {
      ERS_DEBUG(3, "clear eofbit");
      m_string_stream->clear();
    }
}

void
pbeast::InputStream::reset(bool uze_zip, int64_t position)
{
  delete_streams();

  if (position > 0)
    {
      if (!m_string_stream)
        {
          m_seek_pos = lseek64(m_fd, position, SEEK_SET);
        }
      else
        {
          clear_ss_eof();
          m_string_stream->seekg(m_seek_pos = position);
        }
    }

  create_streams(uze_zip);
}

void
pbeast::InputStream::seek_pos(int64_t offset)
{
  if (!m_string_stream)
    {
      m_seek_pos = lseek64(m_fd, offset, SEEK_SET);
    }
  else
    {
      clear_ss_eof();
      m_string_stream->seekg(m_seek_pos = offset);
    }
}

void
pbeast::InputStream::seek_end(int64_t offset)
{
  if (!m_string_stream)
    {
      m_seek_pos = lseek64(m_fd, -offset, SEEK_END);
    }
  else
    {
      clear_ss_eof();
      m_string_stream->seekg(m_seek_pos = (m_string_stream->str().size() - offset));
    }
}

int64_t
pbeast::InputStream::tell_pos()
{
  return m_seek_pos + m_coded_input->CurrentPosition();
}

void
pbeast::InputStream::check_and_reset(bool uze_zip, int64_t limit)
{
  if (uze_zip)
    {
      reset(true, limit);
    }
  else
    {
      int64_t cur_pos = m_coded_input->CurrentPosition();

      if (cur_pos > limit)
        {
          ERS_DEBUG(1,"reset stream at " << cur_pos + m_seek_pos << " [file pos: " << tell_pos() << ", read " << cur_pos << " bytes]");
          reset(false, cur_pos + m_seek_pos);
        }
    }
}
