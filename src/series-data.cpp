#include <locale>

#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/conversion.hpp>
#include <boost/date_time/posix_time/posix_time_io.hpp>

#include "pbeast/data-type.h"
#include "pbeast/series-print.h"
#include "pbeast/series-idl.h"

void
pbeast::print_date(uint32_t value, std::ostream &s)
{
  boost::posix_time::ptime t = boost::posix_time::from_time_t(value);
  s << t.date();
}

void
pbeast::print_time(uint32_t value, boost::local_time::time_zone_ptr tz_ptr, std::ostream &s)
{
  if (value == 0xffffffff)
    {
      s << "...";
      return;
    }

  boost::posix_time::ptime t = boost::posix_time::from_time_t(value);

  if (!tz_ptr)
    s << t;
  else
    s << boost::local_time::local_date_time(t, tz_ptr).to_string();

}

void inline
print_mk_secs(uint64_t value, std::ostream &s)
{
  static const char decimal_mark = std::use_facet<std::numpunct<char> >(std::cout.getloc()).decimal_point();
  s << decimal_mark << std::setw(6) << std::setfill('0') << pbeast::ts2mksecs(value);
}

void
pbeast::print_time(uint64_t value, boost::local_time::time_zone_ptr tz_ptr, std::ostream &s)
{
  if (value >= 0xffffffff * 1000000LL)
    {
      s << "...";
      return;
    }

  boost::posix_time::ptime t = boost::posix_time::from_time_t(ts2secs(value));

  if (!tz_ptr)
    {
      s << t;
      print_mk_secs(value, s);
    }
  else
    {
      boost::local_time::local_date_time lt(t, tz_ptr);

      s << lt.local_time();
      print_mk_secs(value, s);

      if (lt.is_dst())
        s << ' ' << lt.zone()->dst_zone_abbrev();
      else
        s << ' ' << lt.zone()->std_zone_abbrev();
    }
}

void
pbeast::print_tss(std::ostream& s, uint64_t created_ts, uint64_t last_updated_ts, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix)
{
  s << prefix << '[';

  if (ts2str)
    {
      print_time(created_ts, tz_ptr, s);

      if (created_ts != last_updated_ts)
        {
          s << " - ";
          print_time(last_updated_ts, tz_ptr, s);
        }
    }
  else
    {
      s << created_ts;

      if (created_ts != last_updated_ts)
        s << " - " << last_updated_ts;

    }

  s << ']';
}


void
pbeast::print_ts(std::ostream &s, uint32_t ts, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char *prefix)
{
  s << prefix << '[';

  if (ts2str)
    print_time(ts, tz_ptr, s);
  else
    s << ts;

  s << ']';
}

namespace pbeast
{
  std::ostream&
  operator<<(std::ostream& s, const Void&)
  {
    s << "(void)";
    return s;
  }
}

void
pbeast::print(std::ostream &s, const std::vector<uint64_t> &data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char *prefix)
{
  for (uint32_t i = 0; i < data.size(); ++i)
    {
      s << prefix;
      if (ts2str)
        print_time(data[i], tz_ptr, s);
      else
        s << data[i];
      s << '\n';
    }
  s.flush();
}

void
pbeast::print(std::ostream &s, const std::set<uint64_t> &data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char *prefix)
{
  for (auto &x : data)
    {
      s << prefix;
      if (ts2str)
        print_time(x, tz_ptr, s);
      else
        s << x;
      s << '\n';
    }
  s.flush();
}

void
pbeast::print_eov(std::ostream &s, uint64_t data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char *prefix)
{
  s << prefix << '[';

  if (ts2str)
    print_time(data, tz_ptr, s);
  else
    s << data;

  s << "] (EOV)\n";
}

void
pbeast::print_eov(std::ostream &s, uint32_t data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char *prefix)
{
  s << prefix << '[';

  if (ts2str)
    print_time(data, tz_ptr, s);
  else
    s << data;

  s << "] (EOV)\n";
}

void
pbeast::print_eov_serie(std::ostream &s, const std::pair<const std::string, std::set<uint64_t>> &x, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char *prefix)
{
  s << x.first << ":\n";

  for (const auto &e : x.second)
    pbeast::print_eov(s, e, tz_ptr, ts2str, prefix);
}

void
pbeast::print_eov_serie(std::ostream &s, const std::pair<const std::string, std::set<uint32_t>> &x, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char *prefix)
{
  s << x.first << ":\n";

  for (const auto &e : x.second)
    pbeast::print_eov(s, e, tz_ptr, ts2str, prefix);
}



const char * pbeast::s_names_of_types[] = {
 "bool",
 "s8",
 "u8",
 "s16",
 "u16",
 "s32",
 "u32",
 "s64",
 "u64",
 "float",
 "double",
 "enum",
 "date",
 "time",
 "string",
 "object",
 "void"
};


pbeast::DataType
pbeast::str2type(const std::string & s)
{
  static const std::map<std::string,pbeast::DataType> s_data ({
    { pbeast::s_names_of_types[pbeast::BOOL],   pbeast::BOOL   },
    { pbeast::s_names_of_types[pbeast::S8],     pbeast::S8     },
    { pbeast::s_names_of_types[pbeast::U8],     pbeast::U8     },
    { pbeast::s_names_of_types[pbeast::S16],    pbeast::S16    },
    { pbeast::s_names_of_types[pbeast::U16],    pbeast::U16    },
    { pbeast::s_names_of_types[pbeast::S32],    pbeast::S32    },
    { pbeast::s_names_of_types[pbeast::U32],    pbeast::U32    },
    { pbeast::s_names_of_types[pbeast::S64],    pbeast::S64    },
    { pbeast::s_names_of_types[pbeast::U64],    pbeast::U64    },
    { pbeast::s_names_of_types[pbeast::FLOAT],  pbeast::FLOAT  },
    { pbeast::s_names_of_types[pbeast::DOUBLE], pbeast::DOUBLE },
    { pbeast::s_names_of_types[pbeast::ENUM],   pbeast::ENUM   },
    { pbeast::s_names_of_types[pbeast::DATE],   pbeast::DATE   },
    { pbeast::s_names_of_types[pbeast::TIME],   pbeast::TIME   },
    { pbeast::s_names_of_types[pbeast::STRING], pbeast::STRING },
    { pbeast::s_names_of_types[pbeast::VOID],   pbeast::VOID }
  });

  std::map<std::string,pbeast::DataType>::const_iterator i = s_data.find(s);

  if(i != s_data.end()) {
    return i->second;
  }
  else {
    return pbeast::OBJECT;
  }
}


#define CVT_MAP_VOID(T)                                                  \
for (unsigned int idx = 0 ; idx < from.length(); ++idx)                  \
  {                                                                      \
    std::string obj_id(prefix);                                          \
    obj_id.append(static_cast<const char *>(from[idx].object_id));       \
                                                                         \
    std::set<T>& x = to[obj_id];                                         \
    std::set<T>::const_iterator hint = x.end();                          \
                                                                         \
    for (unsigned int i = 0 ; i < from[idx].data.length(); ++i)          \
      {                                                                  \
        hint = x.insert(hint, from[idx].data[i]);                        \
      }                                                                  \
  }

void
pbeast::cvt_void(const pbeast::V64ObjectList& from, std::map<std::string, std::set<uint64_t>>& to, const char * prefix)
{
  CVT_MAP_VOID(uint64_t)
}

void
pbeast::cvt_void(const pbeast::V32ObjectList& from, std::map<std::string, std::set<uint32_t>>& to, const char * prefix)
{
  CVT_MAP_VOID(uint32_t)
}
