#include <sstream>
#include <stdexcept>

#include <pbeast/exceptions.h>
#include <pbeast/directory.h>
#include "pbeast/input-stream.h"
#include "pbeast/input-data-file.h"


pbeast::InputDataFile::InputDataFile(const std::filesystem::path& path, const std::string& name, pbeast::File& file, CallStats& stat) :
    InputDataFileBase((path / name).native()), m_string_stream(nullptr), m_input_stream(nullptr), m_it_inited(false), m_total_data_size(0)
{
  std::lock_guard<std::mutex> lock(file.m_mutex);

  if (file.m_file_info)
    {
      m_file_version = file.m_file_info->get_file_version();
      m_partition = file.m_file_info->get_partition();
      m_class = file.m_file_info->get_class();
      m_class_path = file.m_file_info->get_class_path();
      m_attribute = file.m_file_info->get_attribute();
      m_base_timestamp = file.m_file_info->get_base_timestamp();
      m_data_type = file.m_file_info->get_data_type();
      m_is_array = file.m_file_info->get_is_array();
      m_number_of_series = file.m_file_info->get_number_of_series();
      m_start_of_names = file.m_file_info->get_start_of_names();
      m_zip_catalog = file.m_file_info->get_zip_catalog_flag();
      m_zip_data = file.m_file_info->get_zip_data_flag();
      m_catalogize_strings = file.m_file_info->get_catalogize_strings_flag();
      m_downsample_interval = file.m_file_info->get_downsample_interval();

      m_series_info = file.m_file_info->get_series_info_ptr();
      m_file_length = file.m_file_info->get_file_length();
      m_info_size = file.m_file_info->get_info_size();

      stat.m_num_of_info_series_cache_hits++;
      stat.m_size_of_info_series_cache_hits += m_info_size;
    }
  else
    {
      read_info();
      close();

      file.m_file_info.reset(new pbeast::InputDataFileBase(*this, m_series_info));
      file.calculate_file_info_size(get_file_name());

      stat.m_size_of_read_headers += m_info_size;
    }
}


std::string
pbeast::DataFile::encode(const std::string& s) noexcept
{
  static bool s_dont_need_encoding[256] = { false };
  static bool initialized(false);

  if (!initialized)
    {
      for (int i = 'a'; i <= 'z'; i++)
        {
          s_dont_need_encoding[i] = true;
        }
      for (int i = 'A'; i <= 'Z'; i++)
        {
          s_dont_need_encoding[i] = true;
        }
      for (int i = '0'; i <= '9'; i++)
        {
          s_dont_need_encoding[i] = true;
        }
      // add symbols allowed by EOS: ".-_:^";
      // the symbol "tilde" "~" is allowed by EOS and reserved for special pBeast files
      s_dont_need_encoding['.'] = true;
      s_dont_need_encoding['-'] = true;
      s_dont_need_encoding['_'] = true;
      s_dont_need_encoding[':'] = true;
      s_dont_need_encoding['^'] = true;
      initialized = true;
    }

  static const char hex[] = "0123456789ABCDEF";

  std::string out;

  for (const char& c : s)
    {
      if (s_dont_need_encoding[static_cast<unsigned char>(c)])
        {
          out.push_back(c);
        }
      else
        {
          out.push_back('#');
          out.push_back(hex[c / 16]);
          out.push_back(hex[c % 16]);
        }
    }

  return out;
}

std::string
pbeast::DataFile::encode(const std::filesystem::path& s) noexcept
{
  std::filesystem::path p;
  for (std::filesystem::path::iterator it(s.begin()), it_end(s.end()); it != it_end; ++it)
    {
      std::string s(encode((*it).native()));
      if(p.empty()) p = s;
      else p /= s;
    }

  return p.string();
}

static char hex2int(char c)
{
  if(c >= '0' && c <= '9') return (c - '0');
  else return (c - 'A' + 10);
}

std::string pbeast::DataFile::decode(const std::string& s, const unsigned char symbol)
{
  std::string out;

  const unsigned int len(s.size());

  for(unsigned int i = 0; i < len; ++i) {
    if(s[i] != symbol) {
      out.push_back(s[i]);
    }
    else {
      if((i + 2) > len) {
        std::ostringstream text;
        text << "bad format of string \"" << s << "\" at position " << i;
        throw std::runtime_error(text.str().c_str());
      }

      char s1 = s[++i];
      char s2 = s[++i];
      out.push_back(hex2int(s1)*16+hex2int(s2));
    }
  }

  return out;
}

static void
validate_parameter(const std::string &value, const char *name)
{
  if (!value.empty())
    try
      {
        if (pbeast::DataFile::encode(value).size() > NAME_MAX)
          throw std::runtime_error("value is too long");
      }
    catch (const std::exception &ex)
      {
        throw daq::pbeast::BadParameter(ERS_HERE, name, value, ex);
      }
}

void
pbeast::DataFile::validate_parameters(const std::string& partition_name, const std::string& class_name, const std::filesystem::path& attribute_path)
{
  validate_parameter(partition_name, "partition name");
  validate_parameter(class_name, "class name");

  for (std::filesystem::path::iterator it(attribute_path.begin()), it_end(attribute_path.end()); it != it_end; ++it)
    validate_parameter((*it).native(), "attribute path");
}


#define READ_STRING_SERIES_RAW_DATA( CONDITION )                                          \
try                                                                                       \
  {                                                                                       \
    uint64_t prev_ts = m_base_timestamp;                                                  \
    uint64_t prev_delta = 0;                                                              \
    uint64_t catalog_size;                                                                \
                                                                                          \
    m_input_stream->read(catalog_size);                                                   \
                                                                                          \
    std::vector<std::string> catalog(catalog_size);                                       \
    std::vector<uint64_t> catalog_len(catalog_size);                                      \
                                                                                          \
    for (uint32_t i = 0; i < catalog_size; ++i)                                           \
      {                                                                                   \
        m_input_stream->read(catalog_len[i]);                                             \
      }                                                                                   \
                                                                                          \
    for (uint32_t i = 0; i < catalog_size; ++i)                                           \
      {                                                                                   \
        m_input_stream->read(catalog[i], catalog_len[i]);                                 \
        if (i%10000 == 9999)                                                              \
          m_input_stream->check_and_reset_object(m_zip_data);                             \
      }                                                                                   \
                                                                                          \
    uint32_t data_len;                                                                    \
    uint64_t ts_created, ts_last_updated;                                                 \
    uint64_t idx;                                                                         \
                                                                                          \
    m_input_stream->read(data_len);                                                       \
    data.reserve(data_len);                                                               \
                                                                                          \
    for (uint32_t i = 0; i < data_len; ++i)                                               \
      {                                                                                   \
        if (m_file_version > 4)                                                           \
          read_interval(ts_created, ts_last_updated, prev_ts, prev_delta);                \
        else                                                                              \
          read_timestamps<std::string>(ts_created, ts_last_updated, prev_ts, prev_delta); \
                                                                                          \
        m_input_stream->read(idx);                                                        \
                                                                                          \
        CONDITION                                                                         \
          data.emplace_back(ts_created, ts_last_updated, catalog[idx]);                   \
                                                                                          \
        m_input_stream->check_and_reset_object(m_zip_data);                               \
      }                                                                                   \
                                                                                          \
    m_total_data_size += data_len;                                                        \
                                                                                          \
    return (2 + catalog_size * 2 + data_len * 3);                                         \
  }                                                                                       \
catch (const std::exception& ex)                                                          \
  {                                                                                       \
    throw std::runtime_error(make_exception_report("file", ex).c_str());                  \
  }


uint64_t
pbeast::InputDataFile::read_data(std::vector<SeriesData<std::string> >& data)
{
  if (m_catalogize_strings)
    {
      READ_STRING_SERIES_RAW_DATA(EMPTY_CONDITION)
    }
  else
    {
      return read_data<std::string>(data);
    }
}

uint64_t
pbeast::InputDataFile::read_data(uint64_t since, uint64_t until, std::vector<SeriesData<std::string> >& data)
{
  if (m_catalogize_strings)
    {
      READ_STRING_SERIES_RAW_DATA(RAW_DATA_POINT_CONDITION)
    }
  else
    {
      return read_data<std::string>(since, until, data);
    }
}


uint64_t
pbeast::InputDataFile::read_data(std::vector<DownsampledSeriesData<std::string> >& data)
{
  if (m_catalogize_strings)
    {
      try
        {
          uint64_t catalog_size;

          m_input_stream->read(catalog_size);

          std::vector<std::string> catalog(catalog_size);
          std::vector<uint64_t> catalog_len(catalog_size);

          for (uint32_t i = 0; i < catalog_size; ++i)
            {
              m_input_stream->read(catalog_len[i]);
            }

          for (uint32_t i = 0; i < catalog_size; ++i)
            {
              m_input_stream->read(catalog[i], catalog_len[i]);
            }

          uint32_t data_len;
          uint32_t ts;
          uint64_t min_idx, idx, max_idx;

          uint32_t prev = 0;

          m_input_stream->read(data_len);
          data.reserve(data_len);

          for (uint32_t i = 0; i < data_len; ++i)
            {
              read_timestamp(ts, prev);
              m_input_stream->read(min_idx);
              m_input_stream->read(idx);
              m_input_stream->read(max_idx);
              data.emplace_back(ts, catalog[min_idx], catalog[idx], catalog[max_idx]);
              m_input_stream->check_and_reset_object(m_zip_data);
            }

          m_total_data_size += data_len;

          return (2 + catalog_size * 2 + data_len * 4);
        }
      catch (const std::exception& ex)
        {
          throw std::runtime_error(make_exception_report("file", ex).c_str());
        }
    }
  else
    {
      return read_data<std::string>(data);
    }
}


template<>
void
pbeast::InputDataFile::read_timestamps<pbeast::Void>(uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t& prev_delta)
{
  if (m_file_version > 4)
    {
      const uint64_t delta = prev_delta;
      int64_t created;
      m_input_stream->read(created);
      prev_delta = static_cast<uint64_t>(delta + created);
      ts_created = prev_ts + prev_delta;
    }
    else
      {
        m_input_stream->read(ts_created);
        ts_created += prev_ts;

        if (m_file_version < 4)
          {
            m_input_stream->read(ts_last_updated);

            ERS_ASSERT_MSG((ts_last_updated == 0), "last-updated timestamps of \'void\' data is not zero (created: " << ts_created << ", last-updated: " << ts_last_updated << ')');
          }
      }

  ts_last_updated = ts_created;

  if (m_file_version > 3)
    prev_ts = ts_last_updated;
}


void
pbeast::InputDataFile::read_info()
{
  open();

  try
    {
      std::string class_path;

      m_input_stream->read(m_file_version);
      m_input_stream->read(m_partition);
      m_input_stream->read(m_class);
      m_input_stream->read(class_path);
      m_input_stream->read(m_attribute);
      m_input_stream->read(m_base_timestamp);

      if(m_file_version < 4)
        {
          uint32_t precision; // obsolete
          m_input_stream->read(precision);
        }

      m_input_stream->read(m_data_type);
      m_input_stream->read(m_is_array);
      m_input_stream->read(m_number_of_series);

      if (m_file_version > 1)
        {
          m_input_stream->read(m_zip_catalog);
          m_input_stream->read(m_zip_data);

          if (m_data_type == pbeast::STRING)
            {
              m_input_stream->read(m_catalogize_strings);
            }
          else
            {
              m_catalogize_strings = false;
            }
        }
      else
        {
          m_zip_catalog = false;
          m_zip_data = false;
          m_catalogize_strings = false;
        }

      if (m_file_version > 2)
        m_input_stream->read(m_downsample_interval);
      else
        m_downsample_interval = 0;

      m_class_path = (class_path.empty() ? m_class : class_path);

      m_info_size = m_input_stream->tell_pos();
    }
  catch (const std::exception& ex)
    {
      throw std::runtime_error(make_exception_report("header of file", ex).c_str());
    }

  try
    {
      m_input_stream->seek_end(8);
      m_file_length = m_input_stream->tell_pos() + 8;
      m_input_stream->reset(false);

      m_input_stream->read64(m_start_of_names);

      m_info_size += (m_file_length - m_start_of_names);

      m_input_stream->reset(m_zip_catalog, m_start_of_names);

      init_series_info();

      std::string buf;
      uint64_t val;
      std::map<std::string, uint64_t>::iterator hint = m_series_info->end();
      for (uint32_t i = 0; i < m_number_of_series; ++i)
        {
          m_input_stream->read(buf);
          m_input_stream->read(val);
          hint = m_series_info->emplace_hint(hint, buf, val);
       }
    }
  catch (const std::exception& ex)
    {
      throw std::runtime_error(make_exception_report("catalog of file", ex).c_str());
    }
}


std::string
pbeast::InputDataFile::make_exception_report(const char * what, const std::exception& ex) const
{
  std::ostringstream text;

  text << "failed to read " << what << " \"" << m_input_stream->get_file_name() << "\" at " << m_input_stream->tell_pos() << "\n\twas caused by: " << ex.what();

  return text.str();
}

uint64_t
pbeast::InputDataFile::get_num_of_series(const std::string& ids, boost::regex *re) const
{
  if (ids.empty())
    return m_series_info->size();
  else if (re)
    {
      uint64_t count(0);
      for (const auto& x : *m_series_info)
        try
          {
            if (boost::regex_match(x.first, *re))
              count++;
          }
        catch (std::exception& ex)
          {
            throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
          }
      return count;
    }
  else
    {
      return m_series_info->find(ids) != m_series_info->end();
    }
}

std::shared_ptr<std::vector<uint64_t>>
pbeast::InputDataFile::estimate_in_memory_merge_size()
{
  if(m_in_memory_merge_size.get() == 0)
    m_in_memory_merge_size.reset(new std::vector<uint64_t>(m_series_info->size()));

  if (m_series_info->size())
    {
      uint64_t size_per_data_point(m_data_type != pbeast::VOID ? 88 : 72);
      uint64_t array_multiplier = 0;

      // average overhead to store array information (72 to 104 bytes depending on exact array size)
      if (m_is_array || m_data_type == pbeast::STRING)
        {
          size_per_data_point += 88;

          // strings, floats and doubles are stored without compaction; numeric types are compacted
          // arrays are replicated twice in vector and set containers
          array_multiplier = ((m_data_type == pbeast::STRING || m_data_type == pbeast::FLOAT || m_data_type == pbeast::DOUBLE) ? 2 : 4);
        }

      std::size_t idx = 0;
      uint64_t prev_pos = 0;
      for (const auto& x : *m_series_info)
        {
          reset(m_zip_data, x.second);
          uint32_t data_len;
          m_input_stream->read(data_len);
          (*m_in_memory_merge_size)[idx] = data_len * size_per_data_point;

          if (prev_pos)
            (*m_in_memory_merge_size)[idx - 1] += (x.second - prev_pos) * array_multiplier;

          prev_pos = x.second;
          idx++;
        }

      (*m_in_memory_merge_size)[idx - 1] += (m_start_of_names - prev_pos) * array_multiplier;
    }

  return m_in_memory_merge_size;
}

void
pbeast::InputDataFile::read_bools(std::vector<bool> &vec, uint64_t len)
{
  vec.clear();
  vec.reserve(len);

  uint64_t val;

  for (uint64_t i = 0; i < len; ++i)
    {
      if (i % 64 == 0)
        m_input_stream->read(val);

      vec.push_back(val >> (i % 64) & 1U);
    }
}

void
pbeast::InputDataFile::read_interval_and_bools(uint64_t& ts_created, uint64_t& ts_last_updated, uint64_t &ts_prev, uint64_t &ts_prev_delta, bool fixed_len_flag, std::vector<bool>& vec, std::vector<bool>& prev_vec)
{
  bool flag1, flag2;
  uint64_t len;

  uint64_t diff;
  m_input_stream->read(diff);

  flag1 = (diff & 1UL);
  flag2 = false;

  if (fixed_len_flag || !flag1)
    read_interval_impl(diff, ts_created, ts_last_updated, ts_prev, ts_prev_delta, flag1);
  else
    read_block_impl(diff, ts_created, ts_last_updated, ts_prev, ts_prev_delta, flag1, flag2);

  if (flag2)
      m_input_stream->read(len);
  else
      len = prev_vec.size();

  if (flag1)
    {
      read_bools(vec, len);
      prev_vec = vec;
    }
  else
    {
      vec = prev_vec;
    }
}
