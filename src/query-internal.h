#ifndef PBEAST_QUERY_INTERNAL_H
#define PBEAST_QUERY_INTERNAL_H

#include <string>

namespace pbeast
{
  struct HttpHeaderFields
  {
    // added by PBEAST Query API
    static std::string user_login;              // "PBEAST-login"
    static std::string full_username;           // "PBEAST-fullname"
    static std::string user_host;               // "PBEAST-host"

    // added by proxy
    static std::string x_forwarded_for;         // "X-Forwarded-For"
    static std::string x_forwarded_fullname;    // "X-Forwarded-fullname"
    static std::string x_forwarded_login;       // "X-Forwarded-login"

    // added by Grafana
    static std::string x_grafana_username;       // "Grafana-username"
  };
}

#endif
