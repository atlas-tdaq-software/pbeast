#include <stdint.h>
#include <unistd.h>

#include <ostream>

#include <boost/interprocess/sync/scoped_lock.hpp>
#include <boost/interprocess/sync/named_mutex.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include <ers/ers.h>

#include <pbeast/writer_lock.h>

const char * pbeast::WriterLock::mutex_name = "pbeast_writer_lock_mutex";
const char * pbeast::WriterLock::memory_name = "pbeast_writer_lock_memory";

pbeast::WriterLock::WriterLock() : m_data(nullptr), m_locked(false), m_timeout(10), m_retry(100)
{
  bool memory_was_open(false);

  // initialize shared memory
  try
    {
      boost::interprocess::permissions permissions;
      permissions.set_unrestricted();

        {
          boost::interprocess::named_mutex mutex(boost::interprocess::open_or_create, mutex_name, permissions);
          boost::interprocess::scoped_lock<boost::interprocess::named_mutex> lock(mutex);

          try
            {
              m_shm = boost::interprocess::shared_memory_object(boost::interprocess::open_only, memory_name, boost::interprocess::read_write);
              ERS_DEBUG(1 , "open shared memory file " << m_shm.get_name());
              m_region = boost::interprocess::mapped_region(m_shm, boost::interprocess::read_write);
              void * addr = m_region.get_address();
              m_data = static_cast<Data*>(addr);
              memory_was_open = true;
            }
          catch (const boost::interprocess::interprocess_exception &create_ex)
            {
              ERS_DEBUG(1, "cannot create shared_memory_object: " << create_ex.what());
            }

          if (memory_was_open == false)
            try
              {
                m_shm = boost::interprocess::shared_memory_object(boost::interprocess::create_only, memory_name, boost::interprocess::read_write, permissions);
                ERS_DEBUG(1, "create shared memory file " << m_shm.get_name() << " of size " << sizeof(Data));
                m_shm.truncate(sizeof(Data));
                m_region = boost::interprocess::mapped_region(m_shm, boost::interprocess::read_write);
                void * addr = m_region.get_address();
                m_data = new (addr) Data;
              }
            catch (const boost::interprocess::interprocess_exception &create_ex)
              {
                boost::interprocess::named_mutex::remove(mutex_name);
                throw daq::pbeast::CannotOpenSharedMemoryObject(ERS_HERE, memory_name, create_ex);
              }
        }

      if (boost::interprocess::named_mutex::remove(mutex_name) == false)
        {
          ers::warning(daq::pbeast::CannotRemoveMutexObject(ERS_HERE, mutex_name));
        }
    }
  catch(boost::interprocess::interprocess_exception &ex)
    {
      boost::interprocess::named_mutex::remove(mutex_name);
      throw daq::pbeast::CannotOpenSharedMemoryObject(ERS_HERE, mutex_name, ex);
    }
}

pbeast::WriterLock::~WriterLock()
{
  unlock();
//  boost::interprocess::named_mutex::remove(mutex_name);
//  boost::interprocess::shared_memory_object::remove(memory_name);
}

void
pbeast::WriterLock::lock()
{
  if (m_locked == false)
    {
      boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(m_data->m_mutex);
      m_data->m_count++;
      m_locked = true;
    }
}

void
pbeast::WriterLock::unlock()
{
  if (m_locked == true)
    {
      boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(m_data->m_mutex);
      if (m_data->m_count > 0)
        {
          m_data->m_count--;
        }
      m_locked = false;
    }
}

void
pbeast::WriterLock::wait_read_permission()
{
  const uint32_t timeout_m_sec(m_timeout * 1000);
  const uint32_t sleep_interval_mk_sec(m_retry * 1000);

  for (uint32_t x = 0; x < timeout_m_sec; x += m_retry)
    {
        {
          boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(m_data->m_mutex);
          if (m_data->m_count == 0)
            {
              return;
            }
        }

      usleep(sleep_interval_mk_sec);
    }

  uint64_t old_count;

    {
      boost::interprocess::scoped_lock<boost::interprocess::interprocess_mutex> lock(m_data->m_mutex);
      if (m_data->m_count == 0)
        {
          return;
        }

      old_count = m_data->m_count;
      m_data->m_count = 0;
    }

  ers::error(daq::pbeast::ResetWritersCount(ERS_HERE, m_timeout, old_count));
}
