#include "pbeast/copy-file.h"

uint64_t
pbeast::copy_file(pbeast::InputDataFile& in, pbeast::OutputDataFile& out, const std::string& ids, boost::regex *re)
{
  //out.write_header();

  uint64_t ret = (
      in.get_data_type() == pbeast::BOOL   ? pbeast::copy<bool>(in, out, ids, re)         :
      in.get_data_type() == pbeast::S8     ? pbeast::copy<int8_t>(in, out, ids, re)       :
      in.get_data_type() == pbeast::U8     ? pbeast::copy<uint8_t>(in, out, ids, re)      :
      in.get_data_type() == pbeast::S16    ? pbeast::copy<int16_t>(in, out, ids, re)      :
      in.get_data_type() == pbeast::U16    ? pbeast::copy<uint16_t>(in, out, ids, re)     :
      in.get_data_type() == pbeast::S32    ? pbeast::copy<int32_t>(in, out, ids, re)      :
      in.get_data_type() == pbeast::U32    ? pbeast::copy<uint32_t>(in, out, ids, re)     :
      in.get_data_type() == pbeast::S64    ? pbeast::copy<int64_t>(in, out, ids, re)      :
      in.get_data_type() == pbeast::U64    ? pbeast::copy<uint64_t>(in, out, ids, re)     :
      in.get_data_type() == pbeast::FLOAT  ? pbeast::copy<float>(in, out, ids, re)        :
      in.get_data_type() == pbeast::DOUBLE ? pbeast::copy<double>(in, out, ids, re)       :
      in.get_data_type() == pbeast::ENUM   ? pbeast::copy<int32_t>(in, out, ids, re)      :
      in.get_data_type() == pbeast::DATE   ? pbeast::copy<uint32_t>(in, out, ids, re)     :
      in.get_data_type() == pbeast::TIME   ? pbeast::copy<uint64_t>(in, out, ids, re)     :
      in.get_data_type() == pbeast::STRING ? pbeast::copy<std::string>(in, out, ids, re)  :
      in.get_data_type() == pbeast::VOID   ? pbeast::copy<pbeast::Void>(in, out, ids, re) :
      0
  );

  out.write_footer();

  return ret;
}
