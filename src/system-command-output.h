#ifndef PBEAST_SYSTEM_COMMAND_OUTPUT_H
#define PBEAST_SYSTEM_COMMAND_OUTPUT_H

#include <string>
#include <fstream>
#include <sstream>

namespace pbeast
{
  struct SystemCommandOutput
  {
    SystemCommandOutput(std::string& command)
    {
      std::ostringstream text;
      text << "/tmp/pbeast-" << getpid() << ".log";
      p_name = text.str();

      command.append(" &>");
      command.append(p_name);
    }

    ~SystemCommandOutput()
    {
      unlink(p_name.c_str());
    }

    std::string
    get_output() const
    {
      std::ifstream f(p_name.c_str());
      std::string out;

      if (f)
        {
          std::filebuf * buf = f.rdbuf();
          while (true)
            {
              auto c = buf->sbumpc();
              if (c == EOF)
                break;
              else
                out += static_cast<char>(c);
            }
        }

      return out;
    }

    std::string p_name;
  };
}

#endif
