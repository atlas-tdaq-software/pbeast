#define _LARGEFILE64_SOURCE

#include <unistd.h>
#include <errno.h>
#include <fcntl.h>

#include <iostream>
#include <sstream>
#include <stdexcept>

#include <ers/Assertion.h>

#include "pbeast/exceptions.h"
#include "pbeast/output-stream.h"
#include "pbeast/series-info.h"
#include "pbeast/output-data-file.h"


/** Functor to sort map of OksObject pointers by object Id */

namespace pbeast
{
  struct SortByPointer
  {
    inline bool
    operator()(const std::string * s1, const std::string * s2) const
    {
      return *s1 < *s2;
    }
  };
}


template<>
void
pbeast::OutputDataFile::write_data<std::string>(const std::string& name, const std::vector<SeriesData<std::string> >& data)
{
  if (m_catalogize_strings)
    {
      assert_raw_type();

      if (data.size())
        {
          std::map<const std::string *, uint64_t, pbeast::SortByPointer> catalog;

          uint64_t pos = m_output_stream->check_and_reset(m_zip_data);
          m_series_info.push_back(new SeriesInfo(name, pos));

          for (unsigned int i = 0; i < data.size(); ++i)
            {
              catalog[&data[i].get_value()] = 0;
            }

          m_output_stream->write((uint64_t) catalog.size());

          uint64_t count(0);

          for (auto & x : catalog)
            {
              x.second = count++;
              m_output_stream->write((uint64_t) x.first->size());
            }

          for (auto & x : catalog)
            {
              m_output_stream->write_no_len(*x.first);
            }

          m_output_stream->write((uint64_t) data.size());
          uint64_t prev_ts = m_base_timestamp;
          uint64_t delta_ts = 0;

          for (unsigned int i = 0; i < data.size(); ++i)
            {
              const SeriesData<std::string>& d(data[i]);
              const std::string& val(d.get_value());
              write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts);
              m_output_stream->write(catalog[&val]);
            }
        }
    }
  else
    {
      write_data_imp<std::string>(name, data);
    }
}

template<>
void
pbeast::OutputDataFile::write_data<std::string>(const std::string& name, const std::vector<DownsampledSeriesData<std::string> >& data)
{
  if (m_catalogize_strings)
    {
      assert_downsample_type();

      if (data.size())
        {
          std::map<const std::string *, uint64_t, pbeast::SortByPointer> catalog;

          uint32_t prev = 0;
          uint64_t pos = m_output_stream->check_and_reset(m_zip_data);
          m_series_info.push_back(new SeriesInfo(name, pos));

          for (unsigned int i = 0; i < data.size(); ++i)
            {
              const DownsampledSeriesData<std::string>& d(data[i]);
              catalog[&d.data().get_min_value()] = 0;
              catalog[&d.data().get_value()] = 0;
              catalog[&d.data().get_max_value()] = 0;
            }

          m_output_stream->write((uint64_t) catalog.size());

          uint64_t count(0);

          for (auto & x : catalog)
            {
              x.second = count++;
              m_output_stream->write((uint64_t) x.first->size());
            }

          for (auto & x : catalog)
            {
              m_output_stream->write_no_len(*x.first);
            }

          m_output_stream->write((uint64_t) data.size());

          for (unsigned int i = 0; i < data.size(); ++i)
            {
              const DownsampledSeriesData<std::string>& d(data[i]);
              write_timestamp(d.get_ts(), prev);
              m_output_stream->write(catalog[&d.data().get_min_value()]);
              m_output_stream->write(catalog[&d.data().get_value()]);
              m_output_stream->write(catalog[&d.data().get_max_value()]);
            }
        }
    }
  else
    {
      write_data_imp<std::string>(name, data);
    }
}


std::filesystem::path pbeast::OutputDataFile::make_file_name(
  const std::filesystem::path& repository_path,
  const std::string& partition_name,
  const std::filesystem::path& class_path,
  const std::string& attribute_name,
  uint32_t secs_min,
  uint32_t secs_max,
  const char * extension,
  bool unique,
  uint32_t downsample_interval
)
{
  unsigned int count(0);
  uint32_t duration = secs_max - secs_min + 1;

  while (true)
    {
      std::ostringstream file_name;
      file_name << secs_min << '-' << duration;

      if (count != 0)
        {
          file_name << '.' << count;
        }

      count++;

      file_name << extension;

      if (downsample_interval)
        {
          file_name << '.' << downsample_interval;
        }

      std::filesystem::path out(repository_path / partition_name / class_path / attribute_name / file_name.str());

      try
        {
          if (unique == false || std::filesystem::exists(out) == false)
            {
              return out;
            }
        }
      catch (std::filesystem::filesystem_error &ex)
        {
          throw daq::pbeast::FileSystemError(ERS_HERE, "access file", out, ex.code().message());
        }
    }
}

void
pbeast::OutputDataFile::write_header()
{
  if (!m_output_stream)
    {
      if (!m_string_stream)
        m_output_stream = new pbeast::OutputStream(m_file_name);
      else
        m_output_stream = new pbeast::OutputStream(m_string_stream);
    }

  std::string class_path(m_class_path.string());
  if (class_path == m_class)
    class_path.clear();

  m_output_stream->write(m_file_version);
  m_output_stream->write(m_partition);
  m_output_stream->write(m_class);
  m_output_stream->write(class_path);
  m_output_stream->write(m_attribute);
  m_output_stream->write(m_base_timestamp);
  m_output_stream->write(m_data_type);
  m_output_stream->write(m_is_array);
  m_output_stream->write(m_number_of_series);
  m_output_stream->write(m_zip_catalog);
  m_output_stream->write(m_zip_data);

  if (m_data_type == pbeast::STRING)
    m_output_stream->write(m_catalogize_strings);

  m_output_stream->write(m_downsample_interval);

  m_output_stream->check_state();
}

void
pbeast::OutputDataFile::write_footer()
{
  m_start_of_names = m_output_stream->check_and_reset(m_zip_catalog);

  std::string last_name;

  for (uint32_t i = 0; i < m_number_of_series; ++i)
    {
      const std::string& serie_name(m_series_info[i]->get_name());

      if (last_name.compare(serie_name) >= 0)
        {
          std::ostringstream text;
          text << "data were not sorted, series name [" << i << "]: \"" << serie_name << "\", previous name: \"" << last_name << '\"';
          throw std::runtime_error(text.str().c_str());
        }
      else
        {
          last_name = serie_name;
        }

      m_output_stream->write(serie_name);
      m_output_stream->write(m_series_info[i]->get_data_position());
    }

  m_output_stream->reset_streams(false);

  m_output_stream->write64(m_start_of_names);

  m_output_stream->check_state();
}

int64_t
pbeast::OutputDataFile::close()
{
  if (m_number_of_series != m_series_info.size())
    {
      std::ostringstream text;

      text << "mismatch number of series in \"";

      if (!m_string_stream)
        text << m_file_name;
      else
        text << "(stringstream)";

      text << "\" - given: " << m_number_of_series << ", actual: " << m_series_info.size();

      throw std::runtime_error(text.str().c_str());
    }

  uint64_t number_of_written_bytes = m_output_stream->close_file();

  if (!m_string_stream)
    {
      int fd = ::open(m_file_name.c_str(), O_RDONLY);

      if (fd <= 0)
        {
          char buf[2048];
          std::ostringstream text;
          text << "open(\"" << m_file_name << "\") failed with code " << errno << ": " << strerror_r(errno, buf, sizeof(buf));
          throw std::runtime_error(text.str().c_str());
        }

      m_file_length = lseek64(fd, 0, SEEK_END);

      if (::close(fd))
        {
          char buf[2048];
          std::ostringstream text;
          text << "close(\"" << m_file_name << "\") failed with code " << errno << ": " << strerror_r(errno, buf, sizeof(buf));
          throw std::runtime_error(text.str().c_str());
        }

      if (number_of_written_bytes != m_file_length)
        {
          std::ostringstream text;
          text << "failed to write file \"" << m_file_name << "\": " << m_file_length << " bytes have been written instead of " << number_of_written_bytes << " expected";
          throw std::runtime_error(text.str().c_str());
        }
    }
  else
    {
      m_file_length = m_string_stream->str().size();
      ERS_ASSERT_MSG((number_of_written_bytes == m_file_length), "number of written bytes is not equal to size of string");
    }

  m_info_size = m_file_length - m_start_of_names;

  if (!m_series_info.empty())
    {
      m_info_size += m_series_info[0]->get_data_position();
    }

  return m_file_length;
}

pbeast::OutputDataFile::~OutputDataFile()
{
  if (m_output_stream)
    {
      delete m_output_stream;
      m_output_stream = nullptr;
    }

  // set m_number_of_series to actual value to avoid possible crash in delete,
  // e.g. if write to file was not completed

  m_number_of_series = m_series_info.size();

  for (uint32_t i = 0; i < m_number_of_series; ++i)
    {
      delete m_series_info[i];
    }
}

void
pbeast::OutputDataFile::assert_raw_type()
{
  ERS_ASSERT_MSG(
    (m_downsample_interval == 0),
    "failed to write raw data to file \"" << m_file_name << "\" containing downsampled data"
  );
}

void
pbeast::OutputDataFile::assert_downsample_type()
{
  ERS_ASSERT_MSG(
    (m_downsample_interval != 0),
    "failed to write downsampled data to file \"" << m_file_name << "\" containing raw data"
  );
}

void
pbeast::OutputDataFile::write_bools(const std::vector<bool>& vec)
{
  uint64_t val(0); // initialization to 0 is not needed, but compiler thinks "it may be used uninitialized"
  const auto len = vec.size();

  for (uint64_t i = 0; true; ++i)
    {
      if (i % 64 == 0)
        {
          if (i != 0)
            m_output_stream->write(val);
          val = 0;
        }

      if (i == len)
        {
          if (i % 64 != 0)
            m_output_stream->write(val);
          break;
        }

      if (vec[i])
        val |= 1UL << i % 64;
    }
}

void
pbeast::OutputDataFile::write_interval_and_bools(const uint64_t ts_created, const uint64_t ts_last_updated, uint64_t &ts_prev, uint64_t &ts_prev_delta, bool fixed_len_flag, const std::vector<bool>& vec, std::vector<bool>& prev_vec)
{
  const uint64_t len = vec.size();

  if (prev_vec == vec)
    {
      write_interval(ts_created, ts_last_updated, ts_prev, ts_prev_delta, 0);
    }
  else
    {
      if (fixed_len_flag)
        {
          write_interval(ts_created, ts_last_updated, ts_prev, ts_prev_delta, 1);
        }
      else
        {
          if (prev_vec.size() == len)
            {
              write_interval(ts_created, ts_last_updated, ts_prev, ts_prev_delta, 0, 1);
            }
          else
            {
              write_interval(ts_created, ts_last_updated, ts_prev, ts_prev_delta, 1, 1);
              m_output_stream->write(len);
            }
        }
      write_bools(vec);
      prev_vec = vec;
    }
}


// boolean type is encoded as flag
void
pbeast::OutputDataFile::write_boolean_type(const std::vector<SeriesData<bool> > &data, uint64_t &prev_ts, uint64_t &delta_ts, uint64_t intervals, const uint64_t start, const uint64_t end)
{
  if (intervals)
    write_interval(data[start].get_ts_created(), data[start].get_ts_last_updated(), prev_ts, delta_ts, data[start].get_value(), 1);
  else
    write_timestamp(data[start].get_ts_created(), prev_ts, delta_ts, data[start].get_value(), 0);

  for (uint64_t i = start + 1; i < end; ++i)
    if (intervals)
      write_interval(data[i].get_ts_created(), data[i].get_ts_last_updated(), prev_ts, delta_ts, data[i].get_value());
    else
      write_timestamp(data[i].get_ts_created(), prev_ts, delta_ts, data[i].get_value());
}
