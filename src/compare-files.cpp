#include <pbeast/compare-files.h>

static const char *
array2str(const pbeast::InputDataFile& f)
{
  return f.get_is_array() ? "[]" : "";
}

static const char *
ds2str(const pbeast::InputDataFile& f)
{
  return f.is_downsampled() ? "donwsampled" : "raw";
}

static void
compare_names(const std::string& name1, const std::string& name2)
{
  if (name1 != name2)
    {
      std::ostringstream text;
      text << "data series names are \"" <<  name1 << "\" and \"" << name2 << '\"';
      throw std::range_error(text.str().c_str());
    }
}

static void
compare_sizes(const std::string& name, std::size_t len1, std::size_t len2)
{
  if (len1 != len2)
    {
      std::ostringstream text;
      text << "numbers of data points in data serie \"" <<  name << "\" are: " << len1 << " and " << len2;
      throw std::range_error(text.str().c_str());
    }
}

static void
report_series_diff(const std::string& name, std::size_t idx)
{
  std::ostringstream text;
  text << "different data points in data serie \"" <<  name << "\" at index " << idx;
  throw std::range_error(text.str().c_str());
}

template<class T>
void
compare_data(pbeast::InputDataFile &f1, pbeast::InputDataFile &f2)
{
  std::string name1, name2;
  std::vector<T> data1, data2;

  while (f1.read_next(name1, data1) && f2.read_next(name2, data2))
    {
      compare_names(name1, name2);
      compare_sizes(name1, data1.size(), data2.size());

      for (uint64_t idx = 0; idx < data1.size(); ++idx)
        if (!(data1[idx] == data2[idx]))
          report_series_diff(name1, idx);

      data1.clear();
      data2.clear();
    }
}

template<class T>
void
compare_data_files(pbeast::InputDataFile &f1, pbeast::InputDataFile &f2)
{
  std::string name1, name2;

  if (f1.get_is_array() == false)
    {
      if (f1.is_downsampled() == false)
        compare_data<pbeast::SeriesData<T> >(f1, f2);
      else
        compare_data<pbeast::DownsampledSeriesData<T> >(f1, f2);
    }
  else
    {
      if (f1.is_downsampled() == false)
        compare_data<pbeast::SeriesVectorData<T> >(f1, f2);
      else
        compare_data<pbeast::DownsampledSeriesVectorData<T> >(f1, f2);
    }
}

void
pbeast::compare_files(const std::string &file1, const std::string &file2)
{
  try
    {
      pbeast::InputDataFile f1(file1);
      pbeast::InputDataFile f2(file2);

      auto f1_type = static_cast<pbeast::DataType>(f1.get_data_type());
      auto f2_type = static_cast<pbeast::DataType>(f2.get_data_type());

      if (f1_type != f2_type || f1.get_is_array() != f2.get_is_array())
        {
          std::ostringstream text;
          text << "different data types \"" << pbeast::as_string(f1_type) << array2str(f1) << "\" and \"" << pbeast::as_string(f2_type) << array2str(f2) << '\"';
          throw std::range_error(text.str().c_str());
        }

      if (f1.is_downsampled() != f2.is_downsampled())
        {
          std::ostringstream text;
          text << "compare " << ds2str(f1) << " and " << ds2str(f2) << " data";
          throw std::range_error(text.str().c_str());
        }

      if (f1.is_downsampled() && (f1.get_downsample_interval() != f2.get_downsample_interval()))
        {
          std::ostringstream text;
          text << "compare downsampled data with different intervals " << f1.get_downsample_interval() << " and " << f2.get_downsample_interval();
          throw std::range_error(text.str().c_str());
        }

      if (f1.get_number_of_series() != f2.get_number_of_series())
        {
          std::ostringstream text;
          text << "numbers of data series are " << f1.get_number_of_series() << " and " << f2.get_number_of_series();
          throw std::range_error(text.str().c_str());
        }

      switch (f1_type)
      {
        case pbeast::BOOL:   compare_data_files<bool>(f1, f2); break;
        case pbeast::S8:     compare_data_files<int8_t>(f1, f2); break;
        case pbeast::U8:     compare_data_files<uint8_t>(f1, f2); break;
        case pbeast::S16:    compare_data_files<int16_t>(f1, f2); break;
        case pbeast::U16:    compare_data_files<uint16_t>(f1, f2); break;
        case pbeast::S32:    compare_data_files<int32_t>(f1, f2); break;
        case pbeast::U32:    compare_data_files<uint32_t>(f1, f2); break;
        case pbeast::S64:    compare_data_files<int64_t>(f1, f2); break;
        case pbeast::U64:    compare_data_files<uint64_t>(f1, f2); break;
        case pbeast::FLOAT:  compare_data_files<float>(f1, f2); break;
        case pbeast::DOUBLE: compare_data_files<double>(f1, f2); break;
        case pbeast::ENUM:   compare_data_files<int32_t>(f1, f2); break;
        case pbeast::DATE:   compare_data_files<uint32_t>(f1, f2); break;
        case pbeast::TIME:   compare_data_files<uint64_t>(f1, f2); break;
        case pbeast::STRING: compare_data_files<std::string>(f1, f2); break;
        case pbeast::VOID:   compare_data_files<pbeast::Void>(f1, f2); break;
        case pbeast::OBJECT: compare_data_files<pbeast::Void>(f1, f2); break;
      }
    }
  catch (const std::range_error &ex)
    {
      throw daq::pbeast::FilesAreDifferent(ERS_HERE, file1, file2, ex.what());
    }
  catch (const std::exception &ex)
    {
      throw daq::pbeast::FileCompareFailed(ERS_HERE, file1, file2, ex);
    }
}
