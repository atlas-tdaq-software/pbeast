#include <openssl/evp.h>
#include <chrono>
#include <mutex>
#include <filesystem>

#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>

#include "pbeast/copy-file.h"
#include "pbeast/def-intervals.h"
#include "pbeast/repository.h"
#include "pbeast/meta.h"


namespace pbeast
{
  uint64_t
  get_def_since()
  {
    timeval tv;
    gettimeofday(&tv, nullptr);
    return mk_ts(tv.tv_sec - 24 * 3600);
  }

  uint64_t
  get_def_until()
  {
    timeval tv;
    gettimeofday(&tv, nullptr);
    return mk_ts(tv.tv_sec);
  }
}


static void
parse_filename(const std::string& file, uint32_t& base, uint32_t& duration, bool skip_suffix_check)
{
  static const std::string valid_suffix(".pb");

  std::string::size_type pos1 = file.find('-');

  if (pos1 == std::string::npos)
    {
      throw std::runtime_error("cannot find dash symbol (-) in filename");
    }

  std::string::size_type pos2 = file.find('.', pos1);

  if (pos2 == std::string::npos)
    {
      throw std::runtime_error("cannot find dot symbol (.) in filename");
    }

  if (skip_suffix_check == false && valid_suffix != file.substr(file.find_last_of('.')))
    {
      throw std::runtime_error("unexpected filename suffix");
    }

  base = atol(file.substr(0, pos1).c_str());
  duration = atol(file.substr(pos1 + 1, pos2 - pos1 - 1).c_str());
}

uint32_t
pbeast::DataFile::get_duration() const
{
  constexpr uint32_t merge_interval_const = 3600 * 24 * 7;

  if (m_duration == std::numeric_limits<uint32_t>::max())
    {
      try
        {
          uint32_t dummy;
          parse_filename(m_file_name, dummy, m_duration, true);

          uint32_t sum = dummy + m_duration;
          const uint32_t num = sum / merge_interval_const;

          m_duration = (num + 1) * merge_interval_const - dummy;
        }
      catch (const std::exception &ex)
        {
          ERS_DEBUG(0, "ignore unexpected format of \'" << m_file_name << "\' file: " << ex.what());
          m_duration = merge_interval_const;
        }
    }

  return m_duration;
}


void
pbeast::File::calculate_file_info_size(const std::filesystem::path& name)
{
  for (const auto& x : m_file_info->get_series_info())
    m_file_info_size += x.first.size();

  ERS_DEBUG(2, "file info size of " << name << " is " << m_file_info_size);
}


pbeast::Directory::Directory(const std::filesystem::path& path) :
    m_path(path)
{
  refresh();
}

bool
pbeast::Directory::exist_no_lock()
{
  try
    {
      if (std::filesystem::exists(m_path) == false)
        return false;

      if (std::filesystem::is_directory(m_path) == false)
        throw daq::pbeast::FileSystemPathIsNotDirectory(ERS_HERE, m_path);
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "check existence of directory", m_path, ex.code().message());
    }

  return true;
}

bool
pbeast::Directory::exist()
{
  std::shared_lock reader_lock(m_mutex);

  return exist_no_lock();
}

void
pbeast::Directory::create()
{
  std::unique_lock writer_lock(m_mutex);

  if (exist_no_lock() == false)
    {
      try
        {
          std::filesystem::create_directories(m_path);
        }
      catch (const std::filesystem::filesystem_error & ex)
        {
          throw daq::pbeast::FileSystemError(ERS_HERE, "create directory", m_path, ex.code().message());
        }
    }
}


void
pbeast::Directory::refresh()
{
  std::unique_lock writer_lock(m_mutex);

  try
    {
      if (std::filesystem::exists(m_path) == false)
        {
          m_files.clear();
          m_last_updated = 0;
          return;
        }

      if (std::filesystem::is_directory(m_path) == false)
        {
          m_files.clear();
          m_last_updated = 0;
          throw daq::pbeast::FileSystemPathIsNotDirectory(ERS_HERE, m_path);
        }
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "check existence of directory", m_path, ex.code().message());
    }


  // unset files confirmation

  for(auto& x : m_files)
    x.second->m_confirmed = false;


  // add files not yet in the cache

  const std::filesystem::directory_iterator end_iter;

  for (std::filesystem::directory_iterator f(m_path); f != end_iter; ++f)
    {
      if (std::filesystem::is_regular_file(f->status()) == false)
        continue;

      const std::string filename = f->path().filename().native();

      auto ret = m_files.find(filename);

      if (ret == m_files.end())
        {
          uint32_t base, duration;

          try
            {
              parse_filename(filename, base, duration, false);
              m_files.emplace(filename, new File(base, duration));
            }
          catch (std::exception& ex)
            {
              ERS_DEBUG(1, "skip processing \'" << f->path() << "\': " << ex.what());
              continue;
            }
        }
      else
        {
          ret->second->m_confirmed = true;
        }
    }


  // remove deleted files

  for (auto i = m_files.begin(); i != m_files.end();)
    {
      if (i->second->m_confirmed == false)
        i = m_files.erase(i);
      else
        ++i;
    }


  m_last_updated = time(nullptr);
}


void
pbeast::Directory::get_files(uint32_t since, uint32_t until, pbeast::FileSet& files, CallStats& stat)
{
  if (std::chrono::system_clock::to_time_t(std::chrono::clock_cast<std::chrono::system_clock>(std::filesystem::last_write_time(m_path))) > m_last_updated)
    refresh();

  std::shared_lock reader_lock(m_mutex);

  for (const auto& x : m_files)
    {
      if (x.second->m_base <= since)
        {
          if (x.second->m_base + x.second->m_duration >= since)
            {
              files.emplace(new pbeast::InputDataFile(m_path, x.first, *x.second, stat));
            }
        }
      else
        {
          if (x.second->m_base <= until)
            {
              files.emplace(new pbeast::InputDataFile(m_path, x.first, *x.second, stat));
            }
        }
    }

  m_last_accessed = time(nullptr);
}

bool
pbeast::Directory::get_file(const std::string& filename, FileSet& files, bool access, CallStats& stat)
{
  if (std::chrono::system_clock::to_time_t(std::chrono::clock_cast<std::chrono::system_clock>(std::filesystem::last_write_time(m_path))) > m_last_updated)
    refresh();

  std::shared_lock reader_lock(m_mutex);

  auto it = m_files.find(filename);
  if (it != m_files.end())
    {
      if (access)
        m_last_accessed = time(nullptr);

      files.emplace(new pbeast::InputDataFile(m_path, filename, *it->second, stat));

      return true;
    }

  return false;
}

void
pbeast::Directory::add_file(const std::string& filename, const pbeast::OutputDataFile& out_file, FileSet& files, CallStats& stat)
{
    {
      std::shared_lock reader_lock(m_mutex);

      auto it = m_files.find(filename);
      if (it != m_files.end())
        {
          files.emplace(new pbeast::InputDataFile(m_path, filename, *it->second, stat));
          m_last_accessed = time(nullptr);
          return;
        }
    }

  std::unique_lock writer_lock(m_mutex);

  uint32_t base, duration;
  parse_filename(filename, base, duration, false);

  File* file = new File(base, duration);
  file->m_file_info.reset(new pbeast::InputDataFileBase(out_file, out_file.get_series_info()));
  file->calculate_file_info_size(out_file.get_file_name());
  m_files.emplace(filename, file);
  files.emplace(new pbeast::InputDataFile(m_path, filename, *file, stat));

  m_last_accessed = time(nullptr);
}

void
pbeast::Directory::remove_files(const std::vector<std::string>& paths)
{
  std::unique_lock writer_lock(m_mutex);

  for (const auto& x : paths)
    {
      auto it = m_files.find(pbeast::extract_file_name(x));
      if (it != m_files.end())
        m_files.erase(it);
    }
}

void
pbeast::Directory::remove_all_files()
{
  std::unique_lock writer_lock(m_mutex);

  m_files.clear();

  m_last_accessed = m_last_updated = 0;
}



void
pbeast::Directory::remove_file(const std::filesystem::path& path)
{
  std::unique_lock writer_lock(m_mutex);

  auto it = m_files.find(path.filename().native());
  if (it != m_files.end())
    m_files.erase(it);
}


uint64_t
pbeast::Directory::get_size_of_file_infos()
{
  std::shared_lock reader_lock(m_mutex);

  uint64_t size = 0;

  for (const auto& x : m_files)
    size += x.second->m_file_info_size;

  ERS_DEBUG(2, "size of series info directory " << m_path << " is " << size);

  return size;
}

uint64_t
pbeast::Directory::get_number_of_files()
{
  std::shared_lock reader_lock(m_mutex);

  return m_files.size();
}


pbeast::Directory*
pbeast::Repository::get_directory(const std::filesystem::path& path, bool add)
{
  std::lock_guard<std::mutex> lock(m_directories_mutex);

  auto it = m_directories.find(path.native());
  if (it != m_directories.end())
    {
      return it->second.get();
    }
  else
    {
      if (add)
        {
          pbeast::Directory * dir = new Directory(path);
          m_directories[path.native()] = std::unique_ptr<Directory>(dir);
          return dir;
        }
      else
        {
          return nullptr;
        }
    }
}


uint64_t
pbeast::Repository::clean_series_info(uint64_t max_size)
{
  std::lock_guard<std::mutex> lock(m_directories_mutex);

  uint64_t size = 0;

  for (const auto& x : m_directories)
    size += x.second->get_size_of_file_infos();

  if (size > max_size)
    {
      std::ostringstream message;
      message << "Current cached series info size: " << size << " bytes\nRemove information about least used series:";

      std::multimap<uint64_t, Directory*> sorted;

      for (const auto& x : m_directories)
        if (x.second->get_number_of_files())
          sorted.insert(std::pair<uint64_t, Directory*>(x.second->get_last_accessed(), x.second.get()));

      for (const auto& x : sorted)
        {
          if(const uint64_t dir_size = x.second->get_size_of_file_infos())
            {
              message << " * discard " << dir_size << " bytes of " << x.second->get_number_of_files();
              size -= dir_size;
            }
          else
            {
              message << " * skip " << x.second->get_number_of_files() << " uninitialized";
            }

          message << " files in " << x.second->get_path() << " accessed last time " << boost::posix_time::to_simple_string(boost::posix_time::from_time_t(x.second->get_last_accessed())) << " (GMT)" << std::endl;

          x.second->remove_all_files();

          if (size <= max_size)
            break;
        }

      ERS_LOG(message.str() << "New size: " << size);
    }

  return size;
}

std::size_t
pbeast::Repository::get_path_len(const std::filesystem::path& path)
{
  std::size_t len = path.native().length();

  while (len > 1 && path.native()[len - 1] == std::filesystem::path::preferred_separator)
    len--;

  return len;
}

void
pbeast::Repository::find_files(const std::string& partition_name, const std::string& class_name, const std::filesystem::path& attribute_path, uint64_t since, uint64_t until, bool encode_attribute_name, FileSet& repository_files, FileSet& local_repository_files, CallStats& stat, CallStats& local_stat)
{
  try
    {
      const std::filesystem::path a_path(
          static_cast<std::filesystem::path>(pbeast::DataFile::encode(partition_name)) / pbeast::DataFile::encode(class_name)
              / (encode_attribute_name ? static_cast<std::filesystem::path>(pbeast::DataFile::encode(attribute_path)) : attribute_path));

      const std::filesystem::path dir(m_repository_path / a_path);
      const std::filesystem::path local_dir(m_local_repository_path / a_path);

      const bool dir_exists(std::filesystem::exists(dir));
      const bool local_dir_exists(!m_local_repository_path.empty() && std::filesystem::exists(local_dir));

      if (!dir_exists && !local_dir_exists)
        {
          // in case of EoV directory may not exist
          if (encode_attribute_name == false)
            {
              return;
            }
          // attribute directory may not exist yet, e.g. when merger is already running, but there are no data to be merged yet
          else
            {
              throw daq::pbeast::NoAttributeDirectory(ERS_HERE, attribute_path.native(), class_name, partition_name, (m_repository_path.empty() ? local_dir : dir));
            }
        }

      uint32_t min_since(pbeast::ts2secs(since));
      uint32_t max_until(pbeast::ts2secs(until));

      if (local_dir_exists)
        get_files(local_dir, min_since, max_until, local_repository_files, local_stat);

      if (dir_exists)
        get_files(dir, min_since, max_until, repository_files, stat);
    }
  catch (std::filesystem::filesystem_error &ex)
    {
      throw daq::pbeast::FileSystemAccessError(ERS_HERE, ex.code().message());
    }
}


pbeast::FileSet
pbeast::Repository::downsample_files(
    const pbeast::FileSet& raw_files,
    const std::vector<uint64_t> sample_intervals,
    pbeast::DownsampleConfig downsample_config,
    const std::string& ids,
    boost::regex *re,
    bool access,
    CallStats& stat)
{
  pbeast::Directory * dir = nullptr;
  FileSet files;

  for (auto & x : raw_files)
    {
      for (auto & dt : sample_intervals)
        {
          // create name for downsample file

          std::ostringstream buf;

            {
              std::string input_file = x->get_file_name().substr(m_repository_path_len);
              input_file.erase(input_file.size() - 3);
              buf << downsample_config.m_cache_path.native() << input_file << '.' << dt;
            }

          std::filesystem::path final_name(buf.str() + ".pb");

          if (dir == nullptr)
            {
              dir = get_directory(final_name.parent_path(), true);

              if (dir->exist() == false)
                dir->create();
            }

          if (dir->get_file(final_name.filename().native(), files, access, stat))
            continue;

          // if there is a regular expression or concrete name, change downsample file name

          if (!ids.empty() && ids != ".*")
            {
              final_name = buf.str();
              final_name += (re ? ".~" : ".=");

              std::string encoded_ids = pbeast::DataFile::encode(ids);

              if ((final_name.native().size() + encoded_ids.size()) >= (NAME_MAX - 1))
                if (EVP_MD_CTX *ctx = EVP_MD_CTX_new())
                  {
                    if (const EVP_MD *md = EVP_sha256())
                      {
                        uint8_t hash_buf[EVP_MAX_MD_SIZE];
                        unsigned int hash_len = 0;

                        if (
                          EVP_DigestInit_ex(ctx, md, nullptr) == 1 &&
                          EVP_DigestUpdate(ctx, ids.c_str(), ids.size()) == 1 &&
                          EVP_DigestFinal_ex(ctx, hash_buf, &hash_len) == 1
                        )
                          {
                            encoded_ids.clear();
                            encoded_ids.push_back('~');

                            for (std::size_t i = 0; i != hash_len; ++i)
                              {
                                encoded_ids.push_back("0123456789abcdef"[hash_buf[i] / 16]);
                                encoded_ids.push_back("0123456789abcdef"[hash_buf[i] % 16]);
                              }

                            ERS_DEBUG(0, "encode long request \'" << ids << "\' as sha256 \'" << encoded_ids.substr(1) << '\'');
                          }
                      }

                    EVP_MD_CTX_free(ctx);
                  }

              encoded_ids.append(".pb");
              final_name += encoded_ids;
            }

          struct task
          {
            static void
            run(pbeast::Directory * dir, FileSet& files, pbeast::InputDataFile * in, std::filesystem::path& final_name, const uint64_t dt, pbeast::DownsampleConfig downsample_config, const std::string& ids, boost::regex *re, const uint64_t queue_size, bool access, CallStats& stat)
            {
              if (dir->get_file(final_name.filename().native(), files, access, stat))
                return;

              // downsample file

              std::filesystem::path tmp_name(final_name);
              tmp_name += ".tmp";

              auto tv = std::chrono::steady_clock::now();

              pbeast::FileOpenGuard open(in);

              stat.m_num_of_data_files++;
              stat.m_size_of_read_headers += in->get_info_size();

              uint64_t number_of_series = in->get_num_of_series(ids, re);

              if (number_of_series == 0)
                {
                  ERS_DEBUG(1, "skip cache request for file \"" << in << "\" with no match for " << (re ? "regex" : "object") << " \"" << ids << '\"');
                  return;
                }

              pbeast::OutputDataFile out(tmp_name.native(), in->get_partition(), in->get_class(), in->get_class_path(), in->get_attribute(), in->get_base_timestamp(), in->get_data_type(), in->get_is_array(), number_of_series,
                  downsample_config.m_zip_catalog, downsample_config.zip_data(in->get_data_type()), downsample_config.m_catalogize_strings, dt);

              // writer_lock.wait_read_permission();

              stat.m_num_of_primitive_data_reads += pbeast::copy_file(*in, out, ids, re);

              int64_t out_size = out.close();

              stat.m_num_of_downsampled_files++;
              stat.m_size_of_downsampled_files += out_size;

              try
                {
                  std::filesystem::rename(tmp_name, final_name);
                }
              catch (const std::filesystem::filesystem_error& ex)
                {
                  throw daq::pbeast::FileSystemOperationError(ERS_HERE, "rename file", tmp_name, final_name, ex.code().message());
                }

              dir->add_file(final_name.filename().native(), out, files, stat);

              ERS_LOG("downsample file " << in->get_file_name() << " to " << final_name << " (time: " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-tv).count() / 1000. << "\", bytes: " << out_size << ", queue position: " << queue_size << ')');
            }
          };

          struct clean_queue_guard
          {
            clean_queue_guard(const std::filesystem::path& path, std::mutex& lock, std::map<std::filesystem::path, std::shared_future<void>>& queue) :
                m_path(path), m_lock(lock), m_queue(queue), m_clean_queue(false)
            {
              ;
            }

            ~clean_queue_guard()
            {
              if (m_clean_queue)
                {
                  std::lock_guard<std::mutex> scoped_lock(m_lock);
                  m_queue.erase(m_path);
                }
            }

            const std::filesystem::path& m_path;
            std::mutex& m_lock;
            std::map<std::filesystem::path, std::shared_future<void>>& m_queue;
            bool m_clean_queue;
          };

          static std::mutex s_downsample_lock;
          clean_queue_guard clean_quard(final_name, s_downsample_lock, m_downsample_queue);
          std::shared_future<void> future;

            {
              std::lock_guard<std::mutex> scoped_lock(s_downsample_lock);

              auto it = m_downsample_queue.find(final_name);

              if (it == m_downsample_queue.end())
                {
                  clean_quard.m_clean_queue = true;
                  future = m_downsample_thread_pool.enqueue(std::bind(&task::run, dir, std::ref(files), x.get(), std::ref(final_name), dt, std::ref(downsample_config), std::ref(ids), re, m_downsample_queue.size(), access, std::ref(stat)));
                  m_downsample_queue[final_name] = future;
                  ERS_DEBUG( 1, "downsample file " << final_name << ", thread " << std::this_thread::get_id() << ", queue size: " << m_downsample_queue.size());
                }
              else
                {
                  future = it->second;
                  ERS_DEBUG( 2, "wait file " << final_name << ", thread " << std::this_thread::get_id() << ", queue size: " << m_downsample_queue.size());
                }
            }

          future.get();

          ERS_DEBUG( 2, "finish request for file " << final_name << ", thread " << std::this_thread::get_id() << ", queue size: " << m_downsample_queue.size());
        }
    }

  return files;
}

pbeast::FileSet
pbeast::Repository::downsample_files(const pbeast::FileSet& raw_files, uint64_t sample_interval, const std::string& ids, boost::regex *re, CallStats& stat)
{
  if (raw_files.size() == 0)
    return FileSet();

  // detect "standard" sample interval closest to given interval
  uint64_t dt = 0;
  for (std::vector<uint64_t>::reverse_iterator x = m_downsample_config.m_sample_intervals.rbegin(); x != m_downsample_config.m_sample_intervals.rend(); ++x)
    {
      dt = *x;
      if (dt <= sample_interval)
        break;
    }

  return downsample_files(raw_files, std::vector<uint64_t>(1, dt), m_downsample_config, ids, re, true, stat);
}

///////////

template<class T>
  void
  _add_stat(const std::map<std::string,std::set<T>>& data, pbeast::CallStats& stat)
{
  for (const auto& x : data)
    stat.m_data_size += sizeof(uint64_t) + x.first.size() + x.second.size() * sizeof(T);
}


void
pbeast::Repository::add_stat(std::map<std::string,std::set<uint64_t>>& data, CallStats& stat)
{
  _add_stat(data, stat);
}

void
pbeast::Repository::add_stat(std::map<std::string,std::set<uint32_t>>& data, CallStats& stat)
{
  _add_stat(data, stat);
}

void
pbeast::Repository::read_void(const FileSet& files, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, const std::string& object_id, std::set<uint64_t>& result, CallStats& stat)
{
  if (files.size() == 0)
    return;

  stat.m_num_of_void_files += files.size();

  std::vector<pbeast::SeriesData<pbeast::Void> > v;

  uint64_t low_bound(pbeast::min_ts_const), high_bound(pbeast::max_ts_const);

  for (auto &f : files)
    {
      m_writer_lock->wait_read_permission();
      pbeast::FileOpenGuard open(f.get());

      check_not_compatible(*f, pbeast::VOID, false, true);

      v.clear();

      stat.m_num_of_void_timestamps_reads += f->read_serie(object_id, w_since, w_until, v) * 2 / 3;
      std::set<uint64_t>::const_iterator hint = result.end();

      for (auto &i : v)
        {
          const uint64_t val = i.get_ts_created();
          if (val > since && val < until)
            {
              hint = result.insert(hint, val);
            }
          else
            {
              if (val <= since && val > low_bound)
                low_bound = val;
              if (val >= until && val < high_bound)
                high_bound = val;
            }
        }
    }

  if (low_bound != pbeast::min_ts_const)
    result.insert(low_bound);

  if (high_bound != pbeast::max_ts_const)
    result.insert(high_bound);
}

void
pbeast::Repository::read_void(const FileSet& files, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, boost::regex * re, const std::set<std::string> * names, std::map<std::string, std::set<uint64_t>>& result, CallStats& stat)
{
  if (files.size() == 0)
    return;

  stat.m_num_of_void_files += files.size();

  std::vector<pbeast::SeriesData<pbeast::Void> > v;

  for (auto &f : files)
    {
      m_writer_lock->wait_read_permission();
      pbeast::FileOpenGuard open(f.get());

      check_not_compatible(*f, pbeast::VOID, false, true);

      const auto& info = f->get_series_info();

      for (auto & x : info)
        {
          if (re)
            try
              {
                if (boost::regex_match(x.first, *re) == false)
                  continue;
              }
            catch (std::exception& ex)
              {
                throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
              }

          if (names && (names->find(x.first) == names->end() || result.find(x.first) != result.end()))
            continue;

          v.clear();

          uint64_t low_bound(pbeast::min_ts_const), high_bound(pbeast::max_ts_const);

          stat.m_num_of_void_timestamps_reads += f->read_serie(x.second, w_since, w_until, v) * 2 / 3;

          std::set<uint64_t> * data_set(nullptr);
          std::set<uint64_t>::const_iterator hint;

          for (auto &i : v)
            {
              const uint64_t val = i.get_ts_created();

              if (val > since && val < until)
                {
                  if (data_set == nullptr)
                    {
                      data_set = &result[x.first];
                      hint = data_set->end();
                    }

                  hint = data_set->insert(hint, val);
                }
              else
                {
                  if (val <= since && val > low_bound)
                    low_bound = val;
                  if (val >= until && val < high_bound)
                    high_bound = val;
                }
            }


          // insert low and high bounds
          // extra check with [*]begin() is needed, because data may come from different files and the bounds can be inserted already

          if (low_bound != pbeast::min_ts_const)
            {
              if (data_set == nullptr)
                data_set = &result[x.first];

              while (!data_set->empty() && *data_set->begin() <= low_bound)
                data_set->erase(data_set->begin());

              data_set->insert(low_bound);
            }

          if (high_bound != pbeast::max_ts_const)
            {
              if (data_set == nullptr)
                data_set = &result[x.first];

              if (data_set->empty() || *data_set->rbegin() < until)
                data_set->insert(high_bound);
            }
        }
    }
}

void
pbeast::Repository::read_void(const std::string& attr_name, const std::string& partition_name, const std::string& class_name, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, const std::string& object_id, std::set<uint64_t>& result, CallStats& stat)
{
  ERS_ASSERT_MSG( (m_writer_lock != nullptr), "writer lock was not initialized" );

  m_writer_lock->wait_read_permission();
  FileSet files;
  find_files(partition_name, class_name, attr_name, w_since, w_until, false, files, stat);

  read_void(files, since, until, w_since, w_until, object_id, result, stat);
}


void
pbeast::Repository::read_void(const std::string& attr_name, const std::string& partition_name, const std::string& class_name, uint64_t since, uint64_t until,
    uint64_t w_since, uint64_t w_until, boost::regex * re, std::map<std::string,std::set<uint64_t>>& result, CallStats& stat)
{
  ERS_ASSERT_MSG( (m_writer_lock != nullptr), "writer lock was not initialized" );

  m_writer_lock->wait_read_permission();
  FileSet files;
  find_files(partition_name, class_name, attr_name, w_since, w_until, false, files, stat);

  read_void(files, since, until, w_since, w_until, re, nullptr, result, stat);
}


void
pbeast::Repository::read_downsample_void(const FileSet& merged_files, const FileSet& local_files, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, uint32_t sample_interval, const std::string& object_id, std::set<uint32_t>& data, std::set<uint64_t>& data64, CallStats& stat, CallStats& local_stat)
{
  // read merged files first; downsample them if necessary
  read_persistent_downsample_void(merged_files, pbeast::ts2secs(w_since), pbeast::ts2secs(w_until), sample_interval, object_id, data, stat);

  // read raw files from local repository and merge with above result
  read_void(local_files, since, until, w_since, w_until, object_id, data64, local_stat);
  pbeast::downsample_void(sample_interval, data64, data);
}


void
pbeast::Repository::read_persistent_downsample_void(const FileSet& merged_files, uint32_t since, uint32_t until, uint32_t sample_interval, const std::string& object_id, std::set<uint32_t>& data, CallStats& stat)
{
  std::set<uint32_t> tmp;
  std::set<uint32_t>::const_iterator hint = tmp.end();

    {
      std::shared_lock lock(m_ds_cache_files);

      FileSet files = downsample_files(merged_files, sample_interval, object_id, nullptr, stat);

      if (files.size() == 0)
        return;

      stat.m_num_of_void_files += files.size();

      std::vector<pbeast::DownsampledSeriesData<pbeast::Void> > v;

      for (auto &f : files)
        {
          m_writer_lock->wait_read_permission();
          pbeast::FileOpenGuard open(f.get());

          v.clear();

          stat.m_num_of_void_timestamps_reads += f->read_serie(object_id, v) / 4;

          for (auto &i : v)
            {
              if (i.get_ts() >= since && i.get_ts() <= until)
                {
                  hint = tmp.insert(hint, i.get_ts());
                }
            }
        }
    }

  if (tmp.empty() == false)
    {
      const uint32_t half_interval(sample_interval / 2);

      uint32_t last(pbeast::mk_downsample_ts(*tmp.begin(), sample_interval));

      hint = data.end();

      for (auto& x : tmp)
        {
          if ((x - last) > sample_interval)
            {
              hint = data.insert(hint, last + half_interval);
              last = pbeast::mk_downsample_ts(x, sample_interval);
            }
        }

      data.insert(hint, last + half_interval);
    }
}


void
pbeast::Repository::read_downsample_void(const FileSet& merged_files, const FileSet& local_files, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, uint32_t sample_interval, const std::string& ids, boost::regex * re, const std::set<std::string> * names, std::map<std::string,std::set<uint32_t>>& data, std::map<std::string,std::set<uint64_t>>& data64, CallStats& stat, CallStats& local_stat)
{
  // read merged files first; downsample them if necessary
  read_persistent_downsample_void(merged_files, pbeast::ts2secs(w_since), pbeast::ts2secs(w_until), sample_interval, ids, re, names, data, stat);

  // read raw files from local repository and merge with above result
  read_void(local_files, since, until, w_since, w_until, re, names, data64, local_stat);

  pbeast::downsample_void(sample_interval, data64, data);
}


void
pbeast::Repository::read_persistent_downsample_void(const FileSet& merged_files, uint32_t since, uint32_t until, uint32_t sample_interval, const std::string& ids, boost::regex * re, const std::set<std::string> * names, std::map<std::string,std::set<uint32_t>>& data, CallStats& stat)
{
  std::map<std::string, std::set<uint32_t>> tmp;

    {
      std::shared_lock lock(m_ds_cache_files);

      FileSet files = downsample_files(merged_files, sample_interval, ids, re, stat);

      if (files.size() == 0)
        return;

      stat.m_num_of_void_files += files.size();

      std::vector<pbeast::DownsampledSeriesData<pbeast::Void> > v;

      for (auto &f : files)
        {
          m_writer_lock->wait_read_permission();
          pbeast::FileOpenGuard open(f.get());

          const std::map<std::string, uint64_t>& info = f->get_series_info();

          for (auto & x : info)
            {
              if (re)
                try
                  {
                    if (boost::regex_match(x.first, *re) == false)
                      continue;
                  }
                catch (std::exception& ex)
                  {
                    throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
                  }

              if (names && (names->find(x.first) == names->end()))
                continue;

              v.clear();
              stat.m_num_of_void_timestamps_reads += f->read_serie(x.second, v) / 4;

              std::set<uint32_t> * data_set(nullptr);
              std::set<uint32_t>::const_iterator hint;

              for (auto &i : v)
                {
                  if (i.get_ts() >= since && i.get_ts() <= until)
                    {
                      if (data_set == nullptr)
                        {
                          data_set = &tmp[x.first];
                          hint = data_set->end();
                        }

                      hint = data_set->insert(hint, i.get_ts());
                    }
                }
            }
        }
    }


  if (tmp.empty() == false)
    {
      const uint32_t half_interval(sample_interval / 2);

      for (auto& y : tmp)
        {
          std::set<uint32_t>& e(data[y.first]);
          std::set<uint32_t>::const_iterator hint = e.end();
          uint32_t last(pbeast::mk_downsample_ts(*y.second.begin(), sample_interval));

          for (auto& x : y.second)
            {
              if ((x - last) > sample_interval)
                {
                  hint = e.insert(hint, last + half_interval);
                  last = pbeast::mk_downsample_ts(x, sample_interval);
                }
            }

          e.insert(hint, last + half_interval);
        }
    }
}


std::string
pbeast::Repository::get_suffix_separator(const std::filesystem::path& path)
{
  std::string separator("/");
  separator += (*path.begin()).native();
  return separator;
}


std::string
pbeast::Repository::create_fqn_prefix(const std::string& partition_name, const std::string& class_name, const std::string& attribute_name)
{
  return (partition_name + '.' + class_name + '.' + attribute_name + '.');
}


void
pbeast::Repository::list_updated_objects(std::set<std::string>& data, const std::string& partition_name,
    const std::string& class_name, const std::filesystem::path& attribute_path,
    uint64_t since, uint64_t until, const std::string& object_id, CallStats& stat)
{
  pbeast::DataFile::validate_parameters(partition_name, class_name, attribute_path);

  std::shared_lock lock(m_repository_files);

  FileSet files;
  find_files(partition_name, class_name, attribute_path, since, until, true, files, stat);

  if (files.size() == 0)
    return;

  std::unique_ptr<boost::regex> object_id_re;

  try
    {
      if (!object_id.empty())
        object_id_re.reset(new boost::regex(object_id));
    }
  catch (std::exception& ex)
    {
      throw daq::pbeast::RegularExpressionError(ERS_HERE, "cannot parse regular expression", object_id, ex);
    }

  for (auto &f : files)
    {
      const std::map<std::string, uint64_t>& info = f->get_series_info();

      for (auto & x : info)
        {
          try
            {
              if (object_id_re && boost::regex_match(x.first, *object_id_re) == false)
                continue;
            }
          catch (std::exception& ex)
            {
              throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
            }

          data.insert(x.first);
        }
    }
}


void
pbeast::downsample_void(uint32_t sample_interval, const std::set<uint64_t>& from, std::set<uint32_t> & to)
{
  if (from.empty() == false)
    {
      const uint32_t half_interval(sample_interval / 2);

      uint32_t last(pbeast::mk_downsample_ts(pbeast::ts2secs(*from.begin()), sample_interval));

      for (auto& x : from)
        {
          if ((x - last) > sample_interval)
            {
              uint32_t inserted = last + half_interval;
              to.insert(inserted);
              last = pbeast::mk_downsample_ts(pbeast::ts2secs(x), sample_interval);
            }
        }

      uint32_t inserted = last + half_interval;
      to.insert(inserted);
    }
}

void
pbeast::downsample_void(uint32_t sample_interval, const std::map<std::string, std::set<uint64_t>> &from, std::map<std::string, std::set<uint32_t>> &to)
{
  for (const auto &x : from)
    if (x.second.size())
      downsample_void(sample_interval, x.second, to[x.first]);
}

void
pbeast::Repository::trim(std::map<std::string, std::set<uint32_t>>& data, uint32_t w_since, uint32_t w_until)
{
  for (auto x = data.begin(); x != data.end(); )
    {
      for (std::set<uint32_t>::iterator i = x->second.begin(); i != x->second.end();)
        {
          if (*i < w_since)
            i = x->second.erase(i);
          else
            break;
        }

      while (x->second.empty() == false)
        {
          std::set<uint32_t>::iterator i = x->second.end();
          --i;

          if (*i > w_until)
            x->second.erase(i);
          else
            break;
        }

      if (x->second.empty() == false)
        x++;
      else
        x = data.erase(x);
    }
}

void
pbeast::Repository::trim_but_one(std::map<std::string,std::set<uint32_t>>& data, uint32_t since, uint32_t until)
{
  for (auto x = data.begin(); x != data.end(); )
    {
      while (x->second.size() > 1)
        {
          auto it = x->second.begin();

          if (*std::next(it) < since)
            x->second.erase(it);
          else
            break;
        }

      while (x->second.size() > 1)
        {
          auto it = x->second.rbegin();

          if (*(++it) > until)
            x->second.erase(it.base());
          else
            break;
        }

      if (x->second.empty() == false)
        x++;
      else
        x = data.erase(x);
    }
}

bool
pbeast::Repository::check_not_compatible(DataFile& file, DataType data_type, bool is_array, bool throw_on_error)
{
  const pbeast::DataType file_data_type(static_cast<pbeast::DataType>(file.get_data_type()));
  const bool not_compatible(are_compartible(file_data_type, data_type) == false || file.get_is_array() != is_array);

  if (not_compatible)
    {
      daq::pbeast::FileTypeError ex(ERS_HERE, file.get_file_name(), as_string(file_data_type), file.get_is_array(), as_string(data_type), is_array);

      if(throw_on_error)
        throw ex;
      else
        ers::debug(ex, 0);
    }

  return not_compatible;
}

void
pbeast::CallStats::check_size() const
{
  if (m_max_data_size && m_data_size > m_max_data_size)
    throw daq::pbeast::MaxMessageSizeError(ERS_HERE, m_max_data_size);
}


void
pbeast::CallStats::print(std::ostream& s)
{
  s << "Read Statistics:\n"
      "  number of data files             : " << m_num_of_data_files << "\n"
      "  number of void files             : " << m_num_of_void_files << "\n"
      "  number of downsampled files      : " << m_num_of_downsampled_files << "\n"
      "  number of info series cache hits : " << m_num_of_info_series_cache_hits << "\n"
      "  size of downsampled files        : " << m_size_of_downsampled_files << "\n"
      "  size of read headers             : " << m_size_of_read_headers << "\n"
      "  size of headers read from cache  : " << m_size_of_info_series_cache_hits << "\n"
      "  number of primitive data reads   : " << m_num_of_primitive_data_reads << "\n"
      "  number of void timestamps reads  : " << m_num_of_void_timestamps_reads << "\n"
      "  number of objects                : " << m_objects_num << "\n"
      "  number of data points            : " << m_data_point_num << "\n"
      "  in-memory / network message size : " << m_data_size << "\n";
}



static const std::string s_fg_none = "none";
static const std::string s_fg_near = "near";
static const std::string s_fg_all = "all";
static const std::string s_fg_bad = "(unexpected)";

pbeast::FillGaps::Type
pbeast::FillGaps::mk(const std::string &value)
{
  if (value == s_fg_none)
    return pbeast::FillGaps::None;
  else if (value == s_fg_near)
    return pbeast::FillGaps::Near;
  else if (value == s_fg_all)
    return pbeast::FillGaps::All;
  else
    {
      std::ostringstream s;
      s << "Unexpected value of the fill gaps parameter: \"" << value << '\"';
      throw std::runtime_error(s.str().c_str());
    }
}

const std::string&
pbeast::FillGaps::str(pbeast::FillGaps::Type type)
{
  switch(type)
  {
    case pbeast::FillGaps::None:
      return s_fg_none;
    case pbeast::FillGaps::Near:
      return s_fg_near;
    case pbeast::FillGaps::All:
      return s_fg_all;
  }

  return s_fg_bad;
}
