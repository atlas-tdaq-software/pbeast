#!/bin/sh

###############################################################################

# Usage: pbeast_refresh_repo.sh directory-path [eov-path]

version='5'

###############################################################################

error_exit()
{
  echo "ERROR [pbeast_refresh_repo.sh]: $1" 1>&2
  exit 1
}

if [ -z "$1" ]
then
  error_exit "directory parameter is not set"
else
  dir=`readlink -e "$1"`
  if [ ! -d "${dir}" ]
  then
    error_exit "repo directory ${dir} does not exist"
  fi
fi

if [ ! -z "$2" ]
then
  eov=`readlink -e "$2"`
  if [ ! -d "${eov}" ]
  then
    error_exit "eov directory ${eov} does not exist"
  fi
fi

###############################################################################

echo "du -sh ${dir}"
du -sh ${dir}

for f in `find "${dir}" -type f '(' -name '*.pb' -o -name '*.meta' ')'`
do
  v=`pbeast_read_file -f $f -i | grep 'file version' | sed 's/.*: //g'`
  
  if [ -z "${v}" ]
  then
    error_exit "failed to read version of file ${f}"
  fi

  if [ "${version}" == "${v}" ]
  then
    echo "skip file ${f} already stored in version ${version}"
    continue
  fi

  if [ "${f##*.}" == "pb" ]
  then
    compression='-c -d'
  else
    compression='-C -D'
  fi

  f2="${f}.tmp"

  pbeast_copy -f "${f}" -o "${f2}" ${compression} || error_exit "pbeast_copy -f ${f} -o ${f2} ${compression} failed"
  pbeast_compare -q --file1 ${f} --file2 ${f2} || error_exit "pbeast_compare -q --file1 ${f} --file2 ${f2} failed"
  mv ${f2} ${f} || error_exit "mv ${f2} ${f} failed"

  if [ ! -z "${eov}" ]
  then
    fd=${eov}${f#${dir}}
    eovdir=`dirname "${fd}"`

    if [ ! -d "${eovdir}" ]
    then
      mkdir -p "${eovdir}" || error_exit "mkdir -p ${eovdir} failed"
    fi

    ln -s "${f}" "${fd}" || error_exit "ln -s $f $fd failed"
  fi
done

echo "du -sh ${dir}"
du -sh ${dir}

exit 0

###############################################################################
