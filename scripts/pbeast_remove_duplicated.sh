#!/bin/sh

###############################################################################

# Usage: pbeast_remove_duplicated directory-path

# see https://its.cern.ch/jira/browse/ADAMATLAS-430

###############################################################################

error_exit()
{
  echo "ERROR [pbeast_remove_duplicated.sh]: $1" 1>&2
  exit 1
}

if [ -z "$1" ]
then
  error_exit "directory parameter is not set"
else
  dir=`readlink -e "$1"`
  if [ ! -d "${dir}" ]
  then
    error_exit "repo directory ${dir} does not exist"
  fi
fi

dry=false

if [ "s $2" == "s dry" ]
then
  dry=true
fi

###############################################################################

echo "[`date +"%Y-%m-%d %H:%M:%S"`] du -sh ${dir}"
du -sh ${dir}

count=0

for f in `find "${dir}" -type f -name '*.1.pb'`
do
  base="${f%%.*}"
  d="$(dirname ${base})"
  file="$(basename ${base})"
  original="${base}.pb"
  echo "[`date +"%Y-%m-%d %H:%M:%S"`] check duplications of ${base}.pb"
  c=0
  for dup in `find "${d}" -type f -name "${file}.*.pb"`
  do
    skip=false

    if [ "${f}" != "${dup}" ]
    then
      ff="${f}"
      if [ ! -f ${ff} ]
      then
        ff=${original}
      fi

      if ! diff -q ${ff} ${dup} &>/dev/null
      then
        skip=true
      fi
    else
      if ! diff -q ${original} ${dup} &>/dev/null
      then
        skip=true
      fi
    fi

    if [ "$skip" = true ]
    then
      echo "  skip $dup"
    else
      if [ "$dry" = false ]
      then
        echo "  remove $dup"
        rm -f "${dup}" || error_exit "rm -f ${dup} failed"
      else
        echo "  [dry]: remove $dup"
      fi
      (( c++ ))
      (( count++ ))
    fi
  done
  echo "removed $c duplications of $original"
done

echo "removed $count duplicated files"

if [ $count -ne 0 ]
then
  echo "[`date +"%Y-%m-%d %H:%M:%S"`] du -sh ${dir}"
  du -sh ${dir}
fi

exit 0

###############################################################################
