#!/bin/sh

###############################################################################

# Usage: pbeast_merge_repo.sh source-directory-path destination-directory-path

src="$1"
dest="$2"

###############################################################################

error_exit()
{
  echo "ERROR [pbeast_merge_repo.sh]: $1" 1>&2
  exit 1
}

if [ -z "${src}" ]; then error_exit "source directory parameter is not set"; fi
if [ -z "${dest}" ]; then error_exit "destination directory parameter is not set"; fi


if [ ! -d "${src}" ]; then error_exit "source directory ${src} does not exist"; fi
if [ ! -d "${dest}" ]; then error_exit "desitination directory ${dest} does not exist"; fi

###############################################################################

echo "copy data files"

#echo "rsync -av --include='*.pb' --include='*/' --exclude='*' ${src} ${dest}/.."
#rsync -av --include='*.pb' --include='*/' --exclude='*' "${src}" "${dest}/.." || error_exit "rsync failed"

echo "cd ${src}"
cd "${src}" || error_exit "cd failed"

for f in `find . -type f -name '*.pb'`
do
  d=`dirname $f`
  if [ ! -d "${dest}/${d}" ]
  then
    echo "mkdir -p ${dest}/${d}"
    mkdir -p "${dest}/${d}" || error_exit "cannot create ${dest}/${d}"
  fi 

  if [ ! -f "${dest}/${f}" ]
  then
    echo "mv ${src}/${f} ${dest}/${f}"
    mv "${src}/${f}" "${dest}/${f}" || error_exit "cannot mv ${src}/${f} to ${dest}/${f}"
  else
    if((`stat -c%s "${src}/${f}"`==`stat -c%s "${dest}/${f}"`))
    then
      echo "file ${dest}/${f} already exists"
    else
      error_exit "error: files ${src}/${f} ${dest}/${f} are different"
    fi
  fi
done

echo "merge meta files"

for x in ${src}/*
do
  p="${x##*/}"

  if [ ! -d "${dest}/${p}" ]
  then
    echo "mkdir ${dest}/${p}"
    mkdir "${dest}/${p}" || error_exit "cannot create ${dest}/${p}"
  fi
  
  for y in ${x}/*
  do
    c="${y##*/}"
    
    if [ ! -d "${dest}/${p}/${c}" ]
    then
      echo "mkdir ${dest}/${p}/${c}"
      mkdir "${dest}/${p}/${c}" || error_exit "cannot create ${dest}/${p}/${c}"
    fi
    
    echo pbeast_merge_meta -s "${y}" -o "${dest}/${p}/${c}"
    pbeast_merge_meta -s "${y}" -o "${dest}/${p}/${c}" || error_exit "merge failed"
  done
done

exit 0

###############################################################################
