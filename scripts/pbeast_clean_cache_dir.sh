#!/bin/sh

###############################################################################

# Usage: pbeast_clean_cache_dir.sh cache-directory-path maximum-size-in-bytes

dir="$1"
max_size="$2"

###############################################################################

if [ -z "${dir}" ]
then
  echo "Directory parameter is not set"
  exit 1
fi

if [ -z "${max_size}" ]
then
  echo "Max size parameter is not set"
  exit 1
fi

###############################################################################

if [ ! -d "${dir}" ]
then
  echo "Directory ${dir} does not exist"
  exit 1
fi

echo "Cache directory: ${dir}"
echo "Max cache directory size: ${max_size}"

###############################################################################

PATH="/bin:/usr/bin:/usr/local/bin"
export PATH

###############################################################################

# Directory size function
dir_size () {
  echo `du -b -s "${dir}" | awk '{print $1;}'`
}

size=$(dir_size)
echo "Actual directory size: ${size}"

age=60

while [ ${size} -gt ${max_size} -a ${age} -gt 5 ]
do
  echo "Remove files not accessed more than $age days ..."
  find "${dir}" -atime +${age} -type f -exec rm -f {} \; -print
  size=$(dir_size)
  age=`expr ${age} - 1`
done

echo "Size of files accessed less than $age days ago: ${size}"

exit 0

###############################################################################
