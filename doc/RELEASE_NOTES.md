# P-BEAST

## tdaq-11-02-00

No need to use obsolete `auth-get-sso-cookie` package by CERN IT to get Point-1 data on GPN. The cookie is created by DAQ sso-helper library.
To read data on GPN, the proxy kerberos authentication method is recommended. A valid kerberos ticket is needed.

All changes in details:
* limit number of processing threads in repository server (16 by default) to avoid OOM issues in case of heavy load
* fix json syntax issue for special symbols in user data (like quotes in string data), see [ADAMATLAS-437](https://its.cern.ch/jira/browse/ADAMATLAS-437) for details
* validate partition, class and attribute parameters in server calls, see [ADAMATLAS-438](https://its.cern.ch/jira/browse/ADAMATLAS-438)
* add option `-V` to specify floating point data precision in output of pbeast command line tools, see [ADAMATLAS-442](https://its.cern.ch/jira/browse/ADAMATLAS-442) for details
* if defined, the `sampleInterval` parameter in `readSeries` REST call has a priority, see [ADAMATLAS-450](https://its.cern.ch/jira/browse/ADAMATLAS-450) for details
* support regular expressions for `getAnnotations` REST call, see [ADAMATLAS-451](https://its.cern.ch/jira/browse/ADAMATLAS-451) for details
* replace obsolete CERN IT's `auth-get-sso-cookie` package by DAQ sso-helper library, see [ADTCC-326](https://its.cern.ch/jira/browse/ADTCC-326) for details
* support proxy kerberos alternative authentication method (recommended for GPN), see [ADHI-4951](https://its.cern.ch/jira/browse/ADHI-4951) for details

## tdaq-10-00-00

* introduce v5 data format with more efficient data compaction (use delta compaction for integer data types)
* add **bstconfig** (new config plugin) and use it for pbeast service operational monitoring (simplifies integration with k8s services), see [ADAMATLAS-419](https://its.cern.ch/jira/browse/ADAMATLAS-419)
* the TDAQ_PBEAST_SERVER_URL and TDAQ_PBEAST_RECEIVER_URL process environment variables can be used to specify the server and web receiver URLs; the PBEAST_SERVER_BASE_URL is deprecated


## tdaq-09-04-00

* introduce v4 data format with more efficient compaction (next data series timestamps stores difference from previous one instead of base file timestamp)
* rename **pbeast_zip** to **pbeast_copy** and add more compress/uncompress options
* add **pbeast_compare** to compare two pbeast files
* add **pbeast_refresh_repo.sh** utility to refresh repository using latest file format (v4)

## tdaq-09-03-00

* add **FillGaps** parameter to various queries allowing to define filling empty intervals, when downsample data:
    * **none** - no any extra data points are inserted, if there are no source data points
    * **near** - add approximated data points before and after interval of existing source data points to avoid undesired linear approximation on long empty intervals (previous behavior)
    * **all** - add approximated data points into all empty intervals

## tdaq-09-01-00

* replace boost::filesystem by std::filesystem
* receiver application:
    * performance improvements in pre-compaction algorithm, see [ADAMATLAS-383](https://its.cern.ch/jira/browse/ADAMATLAS-383)
    * monitoring the _pre-compaction_ and the _commit_ functions times
* repository server monitoring:
    * publish information about disk used space
