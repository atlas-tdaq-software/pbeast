#include <chrono>
#include <iostream>
#include <set>

#include <pbeast/exceptions.h>
#include <pbeast/series-data.h>
#include <pbeast/series-print.h>
#include <pbeast/input-data-file.h>
#include <pbeast/output-data-file.h>

constexpr uint64_t base_timestamp = 1000000000000000;

uint64_t
created(const uint64_t i)
{
  return base_timestamp + i * 5000000 + i % 13 - 6 * (i % 2);
}

uint64_t
updated(const uint64_t i)
{
  uint64_t c = created(i);

  if (i % 1024 < 256)
    c += (i % 3) * 1000000;

  return c;
}

template<typename T>
  void
  report_simple_write(std::chrono::time_point<std::chrono::steady_clock> &tm, uint64_t size, uint64_t num)
  {
    std::cout << "write " << size << " bytes of " << pbeast::as_string(pbeast::CType<T>::type()) << " data (" << static_cast<float>(size) / static_cast<float>(num)
        << " bytes per data point) in " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - tm).count() / 1000. << " seconds, ";
  }

template<typename T>
  void
  report_array_write(std::chrono::time_point<std::chrono::steady_clock> &tm, uint64_t total, uint64_t size)
  {
    std::cout << "write " << size << " bytes of " << pbeast::as_string(pbeast::CType<T>::type()) << "[] data (" << static_cast<float>(size) / static_cast<float>(total)
        << " bytes per array item) in " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - tm).count() / 1000. << " seconds, ";
  }

void
report_read(std::chrono::time_point<std::chrono::steady_clock> &tm)
{
  std::cout << "read data in " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - tm).count() / 1000. << " seconds";
}

bool
test_void_data_type()
{
  const unsigned int num = 1000000;

  std::vector<pbeast::SeriesData<pbeast::Void>> payload_out, payload_in;
  payload_out.reserve(num);

  for (unsigned int i = 0; i < num; ++i)
    {
      const uint64_t created_ts = created(i);
      payload_out.emplace_back(created_ts, created_ts, pbeast::Void());
    }

  std::stringstream stream;

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::OutputDataFile out(&stream, "p", "c", "c", "a", base_timestamp, pbeast::CType<pbeast::Void>::type(), false, 1, false, true, false, 0);

      out.write_header();
      out.write_data(typeid(pbeast::Void).name(), payload_out);
      out.write_footer();

      out.close();

      report_simple_write<pbeast::Void>(tm, out.get_file_length() - out.get_info_size(), num);
    }
  catch(const daq::pbeast::Exception& ex)
    {
      ers::error(ex);
      return false;
    }

  stream.seekp(0);

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::InputDataFile in(&stream);
      std::string name;

      in.read_next(name, payload_in);

      report_read(tm);
    }
  catch (const daq::pbeast::Exception &ex)
    {
      ers::error(ex);
      return false;
    }
  catch (const std::exception &ex)
    {
      std::cerr << ex.what() << std::endl;
      return false;
    }

  if (payload_in.size() != num)
    {
      std::cout << "read " << payload_in.size() << " data points instead of " << payload_out.size() << std::endl;
      return false;
    }

  for (unsigned int i = 0; i < num; ++i)
    {
      if (payload_in[i].get_ts_created()  != payload_out[i].get_ts_created())
        {
          std::cout << "in.data_point[" << i << "].created " << payload_in[i].get_ts_created() << " != out.data_point[" << i << "].created "<< payload_out[i].get_ts_created() << std::endl;
          return false;
        }

      if (payload_in[i].get_ts_last_updated()  != payload_out[i].get_ts_last_updated())
        {
          std::cout << "in.data_point[" << i << "].last-updated " << payload_in[i].get_ts_last_updated() << " != out.data_point[" << i << "].last-updated "<< payload_out[i].get_ts_last_updated() << std::endl;
          return false;
        }
    }

  return true;
}


template<typename T>
bool
test_simple_data_type()
{
  const unsigned int num = 250000;
  //const unsigned int num = 50000;

  std::vector<pbeast::SeriesData<T>> payload_out, payload_in;
  payload_out.reserve(num);

  unsigned int i = 0;

  payload_out.emplace_back(created(i), updated(i), std::numeric_limits<T>::min()); ++i;

  // check possible delta compaction overflow
  payload_out.emplace_back(created(i), updated(i), std::numeric_limits<T>::max()); ++i;
  payload_out.emplace_back(created(i), updated(i), std::numeric_limits<T>::min()); ++i;
  payload_out.emplace_back(created(i), updated(i), std::numeric_limits<T>::max()); ++i;
  payload_out.emplace_back(created(i), updated(i), std::numeric_limits<T>::min()); ++i;
  payload_out.emplace_back(created(i), updated(i), std::numeric_limits<T>::max() - 1); ++i;
  payload_out.emplace_back(created(i), updated(i), std::numeric_limits<T>::min() + 1); ++i;
  payload_out.emplace_back(created(i), updated(i), 0); ++i;

  constexpr double avg = (static_cast<double>(std::numeric_limits<T>::max()) - static_cast<double>(std::numeric_limits<T>::min())) / 2.;
  const double dt = (static_cast<double>(std::numeric_limits<T>::max()) - static_cast<double>(std::numeric_limits<T>::min())) / (num / 3 - i);
  T dtt = std::max(static_cast<T>(dt),static_cast<T>(1));
  const uint64_t dtt64((static_cast<double>(dtt) > static_cast<double>(std::numeric_limits<uint64_t>::max())) ? std::numeric_limits<uint64_t>::max() : static_cast<uint64_t>(dtt));

  const unsigned int increasing_interval = num / 3;
  const unsigned int oscillating_interval = (num * 2) / 3;


  for (; i < num; ++i)
    {
      double v;

      if (i <= increasing_interval)
        v = std::numeric_limits<T>::min() + dt * i - (i % 119) % dtt64 + (i % 121) % dtt64;
      else if (i <= oscillating_interval)
        if constexpr (std::is_integral_v<T>)
          v = avg - dt * ((i % 7) % dtt64) + dt * ((i % 5) % dtt64) + static_cast<double>((i + 1 - num / 3) % static_cast<T>(std::numeric_limits<T>::max())) / 5.;
        else
          v = avg - dt * ((i % 7) % dtt64) + dt * ((i % 5) % dtt64) + static_cast<double>((i + 1 - num / 3) / 5.) ;
      else
        v = std::numeric_limits<T>::min() + dt * (num - i) - (i % 121) % dtt64 + (i % 119) % dtt64;

      if (v <= std::numeric_limits<T>::min())
        v = std::numeric_limits<T>::min();
      if (v >= std::numeric_limits<T>::max())
        v = std::numeric_limits<T>::max();

      payload_out.emplace_back(created(i), updated(i), static_cast<T>(v));
    }

  std::stringstream stream;

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::OutputDataFile out(&stream, "p", "c", "c", "a", base_timestamp, pbeast::CType<T>::type(), false, 1, false, true, false, 0);

      out.write_header();
      out.write_data(typeid(T).name(), payload_out);
      out.write_footer();

      out.close();

      report_simple_write<T>(tm, out.get_file_length() - out.get_info_size(), num);
    }
  catch(const daq::pbeast::Exception& ex)
    {
      ers::error(ex);
      return false;
    }

  stream.seekp(0);

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::InputDataFile in(&stream);
      std::string name;

      in.read_next(name, payload_in);

      report_read(tm);
    }
  catch (const daq::pbeast::Exception &ex)
    {
      ers::error(ex);
      return false;
    }
  catch (const std::exception &ex)
    {
      std::cerr << ex.what() << std::endl;
      return false;
    }

  if (payload_in.size() != num)
    {
      std::cout << "read " << payload_in.size() << " data points instead of " << payload_out.size() << std::endl;
      return false;
    }

  for (unsigned int i = 0; i < num; ++i)
    {
      if (payload_in[i].get_ts_created()  != payload_out[i].get_ts_created())
        {
          std::cout << "in.data_point[" << i << "].created " << payload_in[i].get_ts_created() << " != out.data_point[" << i << "].created "<< payload_out[i].get_ts_created() << std::endl;
          return false;
        }

      if (payload_in[i].get_ts_last_updated()  != payload_out[i].get_ts_last_updated())
        {
          std::cout << "in.data_point[" << i << "].last-updated " << payload_in[i].get_ts_last_updated() << " != out.data_point[" << i << "].last-updated "<< payload_out[i].get_ts_last_updated() << std::endl;
          return false;
        }

      if (payload_in[i].get_value() != payload_out[i].get_value())
        {
          std::cout << "in.data_point[" << i << "].value ";
          pbeast::print(std::cout, payload_in[i].get_value());
          std::cout << " != out.data_point[" << i << "].value ";
          pbeast::print(std::cout, payload_out[i].get_value());
          std::cout << std::endl;
          return false;
        }
    }

  return true;
}


template<typename T>
bool
test_vector_data_type(bool test_fixed)
{
  const unsigned int num = 5000, base_len = 1024;
  constexpr double dt = (static_cast<double>(std::numeric_limits<T>::max()) - static_cast<double>(std::numeric_limits<T>::min())) / (num + base_len * 10);

  std::vector<pbeast::SeriesVectorData<T>> payload_out, payload_in;
  payload_out.reserve(num);

  uint64_t total = 0;

  unsigned int len = base_len;

  for (unsigned int i = 0; i < num; ++i)
    {
      if (!test_fixed && i % 19 == 17)
        len = base_len + (i % 5) - (i % 7);

      if (!test_fixed && i % 19 == 18 && i % 5 == 1)
        len = base_len;

      total += len;

      std::vector<T> vec;
      vec.reserve(len);

      for (unsigned int j = 0; j < len; ++j)
        {
          double v = std::numeric_limits<T>::min() + dt * static_cast<double>((i + j * 10)) - i % 7 + i % 5 - j % 3 + j % 5;

          if (test_fixed && (j % 7) == 0)
            v = static_cast<double>(j % 17);

          if (v <= std::numeric_limits<T>::min())
            v = std::numeric_limits<T>::min();
          if (v >= std::numeric_limits<T>::max())
            v = std::numeric_limits<T>::max();

          vec.push_back(static_cast<T>(v));
        }

      if (i == 0)
        {
          vec.push_back(0);
          vec.push_back(1);
          vec.push_back(2);
          vec.push_back(std::numeric_limits<T>::min());
          vec.push_back(std::numeric_limits<T>::max());
          vec.push_back(std::numeric_limits<T>::min());
          vec.push_back(std::numeric_limits<T>::max());
        }

      payload_out.emplace_back(created(i), updated(i), vec);
    }

  std::stringstream stream;

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::OutputDataFile out(&stream, "p", "c", "c", "a", base_timestamp, pbeast::CType<T>::type(), true, 1, false, true, false, 0);

      out.write_header();
      out.write_data(typeid(T).name(), payload_out);
      out.write_footer();

      out.close();

      report_array_write<T>(tm, total, out.get_file_length() - out.get_info_size());
    }
  catch(const daq::pbeast::Exception& ex)
    {
      ers::error(ex);
      return false;
    }

  stream.seekp(0);

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::InputDataFile in(&stream);
      std::string name;

      in.read_next(name, payload_in);

      report_read(tm);
    }
  catch (const daq::pbeast::Exception &ex)
    {
      ers::error(ex);
      return false;
    }
  catch (const std::exception &ex)
    {
      std::cerr << ex.what() << std::endl;
      return false;
    }

  if (payload_in.size() != num)
    {
      std::cout << "read " << payload_in.size() << " data points instead of " << payload_out.size() << std::endl;
      return false;
    }

  for (unsigned int i = 0; i < num; ++i)
    {
      if (payload_in[i].get_ts_created()  != payload_out[i].get_ts_created())
        {
          std::cout << "in.data_point[" << i << "].created " << payload_in[i].get_ts_created() << " != out.data_point[" << i << "].created "<< payload_out[i].get_ts_created() << std::endl;
          return false;
        }

      if (payload_in[i].get_ts_last_updated()  != payload_out[i].get_ts_last_updated())
        {
          std::cout << "in.data_point[" << i << "].last-updated " << payload_in[i].get_ts_last_updated() << " != out.data_point[" << i << "].last-updated "<< payload_out[i].get_ts_last_updated() << std::endl;
          return false;
        }

      if (payload_in[i].get_value() != payload_out[i].get_value())
        {
          std::cout << "in.data_point[" << i << "].value ";
          pbeast::print(std::cout, payload_in[i].get_value());
          std::cout << " != out.data_point[" << i << "].value ";
          pbeast::print(std::cout, payload_out[i].get_value());
          std::cout << std::endl;
          return false;
        }
    }

  return true;
}

bool
test_simple_string_data_type(bool cat_str)
{
  const unsigned int num = 100000;

  std::vector<pbeast::SeriesData<std::string>> payload_out, payload_in;
  payload_out.reserve(num);

  std::stringstream s;
  for (unsigned int i = 0; i < num; ++i)
    {
      s.str("");
      s << std::hex << (i % 131) << (i % 3) << (i % 131) << (i % 3) << (i % 131) << (i % 3);
      payload_out.emplace_back(created(i), updated(i), s.str());
    }

  std::stringstream stream;

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::OutputDataFile out(&stream, "p", "c", "c", "a", base_timestamp, pbeast::CType<std::string>::type(), false, 1, false, true, cat_str, 0);

      out.write_header();
      out.write_data(typeid(std::string).name(), payload_out);
      out.write_footer();

      out.close();

      report_simple_write<std::string>(tm, out.get_file_length() - out.get_info_size(), num);
    }
  catch(const daq::pbeast::Exception& ex)
    {
      ers::error(ex);
      return false;
    }

  stream.seekp(0);

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::InputDataFile in(&stream);
      std::string name;

      in.read_next(name, payload_in);

      report_read(tm);
    }
  catch (const daq::pbeast::Exception &ex)
    {
      ers::error(ex);
      return false;
    }
  catch (const std::exception &ex)
    {
      std::cerr << ex.what() << std::endl;
      return false;
    }

  if (payload_in.size() != num)
    {
      std::cout << "read " << payload_in.size() << " data points instead of " << payload_out.size() << std::endl;
      return false;
    }

  for (unsigned int i = 0; i < num; ++i)
    {
      if (payload_in[i].get_ts_created()  != payload_out[i].get_ts_created())
        {
          std::cout << "in.data_point[" << i << "].created " << payload_in[i].get_ts_created() << " != out.data_point[" << i << "].created "<< payload_out[i].get_ts_created() << std::endl;
          return false;
        }

      if (payload_in[i].get_ts_last_updated()  != payload_out[i].get_ts_last_updated())
        {
          std::cout << "in.data_point[" << i << "].last-updated " << payload_in[i].get_ts_last_updated() << " != out.data_point[" << i << "].last-updated "<< payload_out[i].get_ts_last_updated() << std::endl;
          return false;
        }

      if (payload_in[i].get_value() != payload_out[i].get_value())
        {
          std::cout << "in.data_point[" << i << "].value ";
          pbeast::print(std::cout, payload_in[i].get_value());
          std::cout << " != out.data_point[" << i << "].value ";
          pbeast::print(std::cout, payload_out[i].get_value());
          std::cout << std::endl;
          return false;
        }
    }

  return true;
}

bool
test_vector_string_data_type(bool test_fixed)
{
  const unsigned int num = 5000, base_len = 1000;

  std::vector<pbeast::SeriesVectorData<std::string>> payload_out, payload_in;
  payload_out.reserve(num);

  uint64_t total = 0;

  unsigned int len = base_len;

  std::stringstream s;
  for (unsigned int i = 0; i < num; ++i)
    {
      if (!test_fixed && i % 19 == 17)
        len = base_len + (i % 5) - (i % 7);

      if (!test_fixed && i % 19 == 18 && i % 5 == 1)
        len = base_len;

      total += len;

      std::vector<std::string> vec;
      vec.reserve(len);

      for (unsigned int j = 0; j < len; ++j)
        {
          s.str("");
          if (test_fixed && (j % 7) == 0)
            s << std::hex << "fixed-str-0x1234";
         else
            s << std::hex << (i % 131) << (i % 3) << (j % 131) << (i % 3) << (i % 131) << (j % 3);
          vec.emplace_back(s.str());
        }

      payload_out.emplace_back(created(i), updated(i), vec);
    }

  std::stringstream stream;

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::OutputDataFile out(&stream, "p", "c", "c", "a", base_timestamp, pbeast::CType<std::string>::type(), true, 1, false, true, false, 0);

      out.write_header();
      out.write_data(typeid(std::string).name(), payload_out);
      out.write_footer();

      out.close();

      report_array_write<std::string>(tm, total, out.get_file_length() - out.get_info_size());
    }
  catch(const daq::pbeast::Exception& ex)
    {
      ers::error(ex);
      return false;
    }

  stream.seekp(0);

  try
    {
      auto tm = std::chrono::steady_clock::now();

      pbeast::InputDataFile in(&stream);
      std::string name;

      in.read_next(name, payload_in);

      report_read(tm);
    }
  catch (const daq::pbeast::Exception &ex)
    {
      ers::error(ex);
      return false;
    }
  catch (const std::exception &ex)
    {
      std::cerr << ex.what() << std::endl;
      return false;
    }

  if (payload_in.size() != num)
    {
      std::cout << "read " << payload_in.size() << " data points instead of " << payload_out.size() << std::endl;
      return false;
    }

  for (unsigned int i = 0; i < num; ++i)
    {
      if (payload_in[i].get_ts_created()  != payload_out[i].get_ts_created())
        {
          std::cout << "in.data_point[" << i << "].created " << payload_in[i].get_ts_created() << " != out.data_point[" << i << "].created "<< payload_out[i].get_ts_created() << std::endl;
          return false;
        }

      if (payload_in[i].get_ts_last_updated()  != payload_out[i].get_ts_last_updated())
        {
          std::cout << "in.data_point[" << i << "].last-updated " << payload_in[i].get_ts_last_updated() << " != out.data_point[" << i << "].last-updated "<< payload_out[i].get_ts_last_updated() << std::endl;
          return false;
        }

      if (payload_in[i].get_value() != payload_out[i].get_value())
        {
          std::cout << "in.data_point[" << i << "].value ";
          pbeast::print(std::cout, payload_in[i].get_value());
          std::cout << " != out.data_point[" << i << "].value ";
          pbeast::print(std::cout, payload_out[i].get_value());
          std::cout << std::endl;
          return false;
        }
    }

  return true;
}



int status = 0;
#define REPORT(...) std::cout << #__VA_ARGS__ << " ==> "; if (__VA_ARGS__) std::cout << " ==> PASSED\n"; else {std::cout << " ==> FAILED\n" << '\n'; status = 1; }

int
main(int argc, char *argv[])
{
  std::cout << "check pbeast streams" << std::endl;

  REPORT(test_void_data_type());

  REPORT(test_simple_data_type<bool>())
  REPORT(test_simple_data_type<uint8_t>())
  REPORT(test_simple_data_type<int8_t>());
  REPORT(test_simple_data_type<uint16_t>());
  REPORT(test_simple_data_type<int16_t>());
  REPORT(test_simple_data_type<uint32_t>());
  REPORT(test_simple_data_type<int32_t>());
  REPORT(test_simple_data_type<uint64_t>());
  REPORT(test_simple_data_type<int64_t>());
  REPORT(test_simple_data_type<float>());
  REPORT(test_simple_data_type<double>());

  REPORT(test_simple_string_data_type(false));
  REPORT(test_simple_string_data_type(true));

  REPORT(test_vector_data_type<bool>(false));
  REPORT(test_vector_data_type<uint8_t>(false));
  REPORT(test_vector_data_type<int8_t>(false));
  REPORT(test_vector_data_type<uint16_t>(false));
  REPORT(test_vector_data_type<int16_t>(false));
  REPORT(test_vector_data_type<uint32_t>(false));
  REPORT(test_vector_data_type<int32_t>(false));
  REPORT(test_vector_data_type<uint64_t>(false));
  REPORT(test_vector_data_type<int64_t>(false));
  REPORT(test_vector_data_type<float>(false));
  REPORT(test_vector_data_type<double>(false));
  REPORT(test_vector_string_data_type(false));

  REPORT(test_vector_data_type<bool>(true));
  REPORT(test_vector_data_type<uint8_t>(true));
  REPORT(test_vector_data_type<int8_t>(true));
  REPORT(test_vector_data_type<uint16_t>(true));
  REPORT(test_vector_data_type<int16_t>(true));
  REPORT(test_vector_data_type<uint32_t>(true));
  REPORT(test_vector_data_type<int32_t>(true));
  REPORT(test_vector_data_type<uint64_t>(true));
  REPORT(test_vector_data_type<int64_t>(true));
  REPORT(test_vector_data_type<float>(true));
  REPORT(test_vector_data_type<double>(true));
  REPORT(test_vector_string_data_type(true));

  return status;
}
