#!/bin/sh
# usage: test-web-api.sh host-name port

#######################################################################################################################
# CLASS DEFINITION
#######################################################################################################################

read -r -d '' class << EOM
{
  "name":"AllTypes",
  "partition":"web-test",
  "attributes":
    [
      {"name":"Boolean","description":"boolean attribute","type":"bool","isArray":false},
      {"name":"SignedInt8","description":"signed integer 8-bits","type":"s8","isArray":false},
      {"name":"UnsignedInt8","description":"unsigned integer 8-bits","type":"u8","isArray":false},
      {"name":"SignedInt16","description":"signed integer 16-bits","type":"s16","isArray":false},
      {"name":"UnsignedInt16","description":"unsigned integer 16-bits","type":"u16","isArray":false},
      {"name":"SignedInt32","description":"signed integer 32-bits","type":"s32","isArray":false},
      {"name":"UnsignedInt32","description":"unsigned integer 32-bits","type":"u32","isArray":false},
      {"name":"SignedInt64","description":"signed integer 64-bits","type":"s64","isArray":false},
      {"name":"UnsignedInt64","description":"unsigned integer 64-bits","type":"u64","isArray":false},
      {"name":"Float","description":"float number","type":"float","isArray":false},
      {"name":"Double","description":"double number","type":"double","isArray":false},
      {"name":"String","description":"a string","type":"string","isArray":false},
      {"name":"Enumeration","description":"some enumeration","type":"enum","isArray":false},
      {"name":"Date","description":"date attribute","type":"date","isArray":false},
      {"name":"Time","description":"time attribute","type":"time","isArray":false},
      {"name":"Array<Boolean>","description":"boolean array attribute","type":"bool","isArray":true},
      {"name":"Array<SignedInt8>","description":"signed integer 8-bits array","type":"s8","isArray":true},
      {"name":"Array<UnsignedInt8>","description":"unsigned integer 8-bits array","type":"u8","isArray":true},
      {"name":"Array<SignedInt16>","description":"signed integer 16-bits array","type":"s16","isArray":true},
      {"name":"Array<UnsignedInt16>","description":"unsigned integer 16-bits array","type":"u16","isArray":true},
      {"name":"Array<SignedInt32>","description":"signed integer 32-bits array","type":"s32","isArray":true},
      {"name":"Array<UnsignedInt32>","description":"unsigned integer 32-bits array","type":"u32","isArray":true},
      {"name":"Array<SignedInt64>","description":"signed integer 64-bits array","type":"s64","isArray":true},
      {"name":"Array<UnsignedInt64>","description":"unsigned integer 64-bits array","type":"u64","isArray":true},
      {"name":"Array<Float>","description":"array of float numbers","type":"float","isArray":true},
      {"name":"Array<Double>","description":"array of double numbers","type":"double","isArray":true},
      {"name":"Array<String>","description":"array of strings","type":"string","isArray":true},
      {"name":"Array<Enumeration>","description":"some enumeration","type":"enum","isArray":true},
      {"name":"Array<Date>","description":"date attribute","type":"date","isArray":true},
      {"name":"Array<Time>","description":"time attribute","type":"time","isArray":true}
    ]
}
EOM

#######################################################################################################################

ts=`date +%s%N`
cmd="http://${1}:${2}/tdaq/pbeast/define?data=[${class}]"
cmd=`echo ${cmd} | sed -s 's/ /%20/g;s/</%3C/g;s/>/%3E/g;s/{/%7B/g;s/}/%7D/g;s/\[/%5B/g;s/\]/%5D/g'`

echo "curl ${cmd}"
curl "${cmd}"
echo ''

#######################################################################################################################
# the full object can contain any attributes in any sequence
#######################################################################################################################

read -r -d '' full_object << EOM
{
  "id":"ID",
  "ts":TS,
  "Boolean":S_BOOL,
  "Array<Boolean>":M_BOOL,
  "SignedInt8":S_INT8,
  "Array<SignedInt8>":M_INT8,
  "UnsignedInt8":S_UINT8,
  "Array<UnsignedInt8>":M_UINT8,
  "SignedInt16":S_INT16,
  "Array<SignedInt16>":M_INT16,
  "UnsignedInt16":S_UINT16,
  "Array<UnsignedInt16>":M_UINT16,
  "SignedInt32":S_INT32,
  "Array<SignedInt32>":M_INT32,
  "UnsignedInt32":S_UINT32,
  "Array<UnsignedInt32>":M_UINT32,
  "SignedInt64":S_INT64,
  "Array<SignedInt64>":M_INT64,
  "UnsignedInt64":S_UINT64,
  "Array<UnsignedInt64>":M_UINT64,
  "Float":S_FLOAT,
  "Array<Float>":M_FLOAT,
  "Double":S_DOUBLE,
  "Array<Double>":M_DOUBLE,
  "String":S_STRING,
  "Array<String>":M_STRING,
  "Enumeration":S_ENUM,
  "Array<Enumeration>":M_ENUM,
  "Date":S_DATE,
  "Array<Date>":M_DATE,
  "Time":S_TIME,
  "Array<Time>":M_TIME
}
EOM

#######################################################################################################################
# the compact object must contain all attributes in the sequence as they were defined in the class
#######################################################################################################################

read -r -d '' compact_object << EOM
[
"ID",
TS,
M_BOOL,
M_DATE,
M_DOUBLE,
M_ENUM,
M_FLOAT,
M_INT16,
M_INT32,
M_INT64,
M_INT8,
M_STRING,
M_TIME,
M_UINT16,
M_UINT32,
M_UINT64,
M_UINT8,
S_BOOL,
S_DATE,
S_DOUBLE,
S_ENUM,
S_FLOAT,
S_INT16,
S_INT32,
S_INT64,
S_INT8,
S_STRING,
S_TIME,
S_UINT16,
S_UINT32,
S_UINT64,
S_UINT8
]
EOM

ID='obj1'

TS=`date +%s%N`
TS=${TS::16}

ts2=$((ts+1000))
ts3=$((ts+1000000000))

S_BOOL='true'
S_INT8='-5'
S_UINT8='200'
S_INT16='-30000'
S_UINT16='40000'
S_INT32='1234567'
S_UINT32="${ts:8:7}"
S_INT64='-1000000000000'
S_UINT64='1000000000000'
S_FLOAT='3.1415926'
S_DOUBLE='3.123456789'
S_STRING='"this is a test string"'
S_ENUM='1'
S_DATE="${ts::10}"
S_TIME="${ts::16}"
M_BOOL='[true,false]'
M_INT8='[-10,10]'
M_UINT8='[10,10]'
M_INT16='[127,255]'
M_UINT16='[10,100,10000]'
M_INT32='[1,-2,3,-4,5]'
M_UINT32="[0,${ts:14:2},30000]"
M_INT64='[10,100,1000,10000]'
M_UINT64="[10,100,${ts:13:3},10000]"
M_FLOAT='[3.14,3.141,3.1415]'
M_DOUBLE='[3.123456781,3.123456782]'
M_STRING='["test 1","test 2"]'
M_ENUM='[1,2,3]'
M_DATE="[${ts::10}]"
M_TIME="[${ts::16},${ts2::16},${ts3::16}]"

full_object_1=`echo "$full_object" | sed "
s/ID/$ID/g;
s/TS/$TS/g;
s/S_BOOL/$S_BOOL/g;
s/S_INT8/$S_INT8/g;
s/S_UINT8/$S_UINT8/g;
s/S_INT16/$S_INT16/g;
s/S_UINT16/$S_UINT16/g;
s/S_INT32/$S_INT32/g;
s/S_UINT32/$S_UINT32/g;
s/S_INT64/$S_INT64/g;
s/S_UINT64/$S_UINT64/g;
s/S_FLOAT/$S_FLOAT/g;
s/S_DOUBLE/$S_DOUBLE/g;
s/S_STRING/$S_STRING/g;
s/S_ENUM/$S_ENUM/g;
s/S_DATE/$S_DATE/g;
s/S_TIME/$S_TIME/g;
s/M_BOOL/$M_BOOL/g;
s/M_INT8/$M_INT8/g;
s/M_UINT8/$M_UINT8/g;
s/M_INT16/$M_INT16/g;
s/M_UINT16/$M_UINT16/g;
s/M_INT32/$M_INT32/g;
s/M_UINT32/$M_UINT32/g;
s/M_INT64/$M_INT64/g;
s/M_UINT64/$M_UINT64/g;
s/M_FLOAT/$M_FLOAT/g;
s/M_DOUBLE/$M_DOUBLE/g;
s/M_STRING/$M_STRING/g;
s/M_ENUM/$M_ENUM/g;
s/M_DATE/$M_DATE/g;
s/M_TIME/$M_TIME/g;
"`

# Use GET method
cmd="http://${1}:${2}/tdaq/pbeast/add?data=[{\"name\":\"AllTypes\",\"partition\":\"web-test\",\"objects\":[$full_object_1]}]"
cmd=`echo ${cmd} | sed -s 's/ /%20/g;s/</%3C/g;s/>/%3E/g;s/{/%7B/g;s/}/%7D/g;s/\[/%5B/g;s/\]/%5D/g'`

echo "curl ${cmd}"
curl "${cmd}"
echo ''

TS=`date +%s%N`
TS=${TS::16}

S_UINT32="${ts:7:7}"
M_UINT32="[0,${ts:15:2},30000]"
M_UINT64="[10,100,${ts:16:3},10000]"

compact_object_1=`echo "$compact_object" | sed "
s/ID/$ID/g;
s/TS/$TS/g;
s/S_BOOL/$S_BOOL/g;
s/S_INT8/$S_INT8/g;
s/S_UINT8/$S_UINT8/g;
s/S_INT16/$S_INT16/g;
s/S_UINT16/$S_UINT16/g;
s/S_INT32/$S_INT32/g;
s/S_UINT32/$S_UINT32/g;
s/S_INT64/$S_INT64/g;
s/S_UINT64/$S_UINT64/g;
s/S_FLOAT/$S_FLOAT/g;
s/S_DOUBLE/$S_DOUBLE/g;
s/S_STRING/$S_STRING/g;
s/S_ENUM/$S_ENUM/g;
s/S_DATE/$S_DATE/g;
s/S_TIME/$S_TIME/g;
s/M_BOOL/$M_BOOL/g;
s/M_INT8/$M_INT8/g;
s/M_UINT8/$M_UINT8/g;
s/M_INT16/$M_INT16/g;
s/M_UINT16/$M_UINT16/g;
s/M_INT32/$M_INT32/g;
s/M_UINT32/$M_UINT32/g;
s/M_INT64/$M_INT64/g;
s/M_UINT64/$M_UINT64/g;
s/M_FLOAT/$M_FLOAT/g;
s/M_DOUBLE/$M_DOUBLE/g;
s/M_STRING/$M_STRING/g;
s/M_ENUM/$M_ENUM/g;
s/M_DATE/$M_DATE/g;
s/M_TIME/$M_TIME/g;
"`

# Use POST method
data_file="/tmp/test.$$.json"
echo "[{\"name\":\"AllTypes\",\"partition\":\"web-test\",\"objects\":[$compact_object_1]}]" > $data_file
cmd="-X POST -F \"data=@${data_file}\" http://${1}:${2}/tdaq/pbeast/add"

echo "curl -X POST -F \"data=@${data_file}\" http://${1}:${2}/tdaq/pbeast/add"
curl -X POST -F "data=@${data_file}" http://${1}:${2}/tdaq/pbeast/add
echo ''
rm -f ${data_file}

TS=`date +%s%N`
TS=${TS::16}


cmd="http://${1}:${2}/tdaq/pbeast/close?data={\"ts\":$TS,\"partition\":\"web-test\",\"classes\":[[\"AllTypes\",[\"$ID\"]]]}"
cmd=`echo ${cmd} | sed -s 's/ /%20/g;s/</%3C/g;s/>/%3E/g;s/{/%7B/g;s/}/%7D/g;s/\[/%5B/g;s/\]/%5D/g'`

echo "Execute ${cmd}"
curl "${cmd}"
