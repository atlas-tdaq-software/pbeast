#include <pbeast/input-data-file.h>
#include <pbeast/data-type.h>

#include <sys/time.h>
#include <stdint.h>

#include <iostream>
#include <string>
#include <vector>
#include <map>

static double
get_time_interval(const timeval * t2, const timeval * t1)
{
  return (t2->tv_sec - t1->tv_sec) + (t2->tv_usec - t1->tv_usec) / 1000000.;
}



#define PRINT_SERIE(N)                                                                      \
  if(print_series) {                                                                        \
    if(print_splunk_format == false) {                                                      \
      std::cout << "series: " << N << std::endl;                                            \
      if(!data.empty()) {                                                                   \
        std::cout << " data[" << data.size() << "]:" << std::endl;                          \
        pbeast::print(std::cout, ts_decimal_width, data);                                   \
      }                                                                                     \
    }                                                                                       \
    else {                                                                                  \
      print_splunk_header(file.get_partition(), N, file.get_class(), file.get_attribute()); \
      pbeast::print_splunk(std::cout, data);                                                \
    }                                                                                       \
  }                                                                                         \


#define GET_DATA(C, T)                                             \
  std::vector< C <T> > data;                                       \
  if(series_name) {                                                \
    file.read_serie(series_name, data);                            \
    PRINT_SERIE(series_name)                                       \
  }                                                                \
  else {                                                           \
     while(file.read_next(name, data)) {                           \
       PRINT_SERIE(name)                                           \
       data.clear();                                               \
     }                                                             \
  }

int main(int argc, char* argv[]) {
  const char * data_file(0);


  for(int i = 1; i < argc; i++) {
    const char * cp = argv[i];

    if(!strcmp(cp, "-f") || !strcmp(cp, "--data-file")) {
      if(++i == argc) {
        std::cerr << "ERROR: data-file is missing" << std::endl;
        return -1;
      }
      else {
        data_file = argv[i];
      }
    }
  }

  if(!data_file) {
    std::cerr << "Usage: " << argv[0] << " -f strings-data-file" << std::endl;
    return -1;
  }

  struct timeval t1, t2;

  try {
    gettimeofday( &t1, 0 );
    pbeast::InputDataFile file(data_file);
    gettimeofday( &t2, 0 );

    std::cout << " * read " << file.get_number_of_series() << " headers: " << get_time_interval(&t2, &t1) << " seconds" << std::endl;

    if(file.get_data_type() != pbeast::STRING) {
      std::cerr << "ERROR: wrong file data type: " << pbeast::as_string(static_cast<pbeast::DataType>(file.get_data_type())) << std::endl;
      return -1;
    }

    gettimeofday( &t1, 0 );

    std::map<std::string,uint64_t> all_strings;
    uint64_t all_strings_cont(0);
    std::map<std::string,uint64_t> local_strings;

    std::vector< pbeast::SeriesData <std::string> > data;
    std::string name;
    uint64_t global_size(0), full_size(0);

    while(file.read_next(name, data))
      {
        uint64_t all_size(0), local_size(0);
        local_strings.clear();

        for (uint32_t i = 0; i < data.size(); ++i)
          {
            const std::string& v = data[i].get_value();

            uint64_t& cl = local_strings[v];
            if(cl == 0)
              {
                local_size += v.size();
              }
            cl++;
            all_size += v.size();

            uint64_t& cg = all_strings[v];
            if(cg == 0)
              {
                global_size += v.size();
              }
            else
              {
                if(cl == 1)
                  {
                    std::cout << "*** found \"" << v << "\" in global dictionary!\n";
                  }
              }
            cg++;
            full_size += v.size();
          }

        all_strings_cont += data.size();

        std::cout << "obj \"" << name << "\": total " << data.size() << " items of " << all_size << " bytes; local: " << local_strings.size() << " items of " << local_size << " bytes; rate " << (double)all_size / (double)local_size << std::endl;

        data.clear();
      }

    std::cout << "FINAL total strings " << all_strings_cont << " items of " << full_size << " bytes; dictionary: " << all_strings.size() << " items of " << global_size << " bytes; rate " << (double)full_size / (double)global_size << std::endl;


    return 0;
  }
  catch (std::exception & ex) {
    std::cerr << "caught exception: " << ex.what() << std::endl;
    return -1;
  }

  gettimeofday( &t2, 0 );

  std::cout << " * process data: " << get_time_interval(&t2, &t1) << " seconds" << std::endl;

  return 0;
}
