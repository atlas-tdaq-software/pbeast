#include <stdio.h>

#include <iostream>
#include <set>

#include "pbeast/series-data.h"

#define PAGE_SIZE (double)4096

typedef struct
{
  unsigned long size, resident, share, text, lib, data, dt;
} statm_t;

static void
read_off_memory_status(statm_t& result)
{
  const char* statm_path = "/proc/self/statm";

  FILE *f = fopen(statm_path, "r");

  if (!f)
    {
      perror(statm_path);
      abort();
    }

  if (7 != fscanf(f, "%lu %lu %lu %lu %lu %lu %lu", &result.size, &result.resident, &result.share, &result.text, &result.lib, &result.data, &result.dt))
    {
      perror(statm_path);
      abort();
    }

  fclose(f);
}

static void
report(std::size_t len, const statm_t& r2, const statm_t& r1, const char * what)
{
  std::cout << ' ' << what << "\n"
      << "  VmSize (program size)     : " << r2.size << " - " << r1.size << " \t= " << (r2.size-r1.size) << " \t=> " << (double)(r2.size-r1.size) / (double)len * PAGE_SIZE << "\n"
      << "  VmRSS (resident set size) : " << r2.resident << " - " << r1.resident << " \t= " << (r2.resident-r1.resident) << " \t=> " << (double)(r2.resident-r1.resident) / (double)len * PAGE_SIZE << "\n"
      << "  DATA (data + stack)       : " << r2.data << " - " << r1.data << " \t= " << (r2.data-r1.data) << " \t=> " << (double)(r2.data-r1.data) / (double)len * PAGE_SIZE << "\n";
}

template<class T>
void
check(const T& val, std::size_t len)
{
  statm_t r1, r2, r3;

  read_off_memory_status(r1);

  std::set<pbeast::SeriesData<T>> data_set;

  for(std::size_t i = 0; i < len; ++i)
    data_set.emplace(i, i, val);

  read_off_memory_status(r2);

  report(len, r2, r1, "set");

  std::vector<pbeast::SeriesData<T> > data_vec;
  data_vec.reserve(len);

  for(const auto& x : data_set)
    data_vec.emplace_back(x);

  read_off_memory_status(r3);

  report(len, r3, r2, "vector");
  report(len, r3, r1, "set+vector");

  std::cout << std::endl;
}

void
check_void(std::size_t len)
{
  statm_t r1, r2, r3;

  read_off_memory_status(r1);

  std::set<uint64_t> data_set;

  for(std::size_t i = 0; i < len; ++i)
    data_set.emplace(i);

  read_off_memory_status(r2);

  report(len, r2, r1, "set");

  std::vector<pbeast::SeriesData<pbeast::Void> > data_vec;
  data_vec.reserve(len);

  for(const auto& x : data_set)
    data_vec.emplace_back(x,x,pbeast::Void());

  read_off_memory_status(r3);

  report(len, r3, r2, "vector");
  report(len, r3, r1, "set+vector");

  std::cout << std::endl;
}


int main(int argc, char* argv[]) {

  if(argc != 3)
    {
      std::cerr << "ERROR: usage " << argv[0] << " size-of-container name-of-container\n";
      return 1;
    }

  size_t len = atol(argv[1]);
  const std::string name = argv[2];

  if(name == "pbeast::Void")
    check_void(len);
  else if(name == "void")
    check<pbeast::Void>(pbeast::Void(), len);
  else if(name == "int8_t")
    check<int8_t>(1, len);
  else if(name == "int16_t")
    check<int16_t>(1, len);
  else if(name == "int32_t")
    check<int32_t>(1, len);
  else if(name == "int64_t")
    check<int64_t>(1, len);
  else if(name == "float")
    check<float>(1, len);
  else if(name == "double")
    check<double>(1, len);
  else if(name.find("string") == 0)
    {
      std::size_t size = atol(name.substr(6).c_str());
      check<std::string>(std::string(size, '1'), len);
    }
  else
    std::cerr << "unknown type " << name << std::endl;

  return 0;
}
