#include <unistd.h>
#include <time.h>

#include <string>
#include <iostream>
#include <sstream>

#include <config/Configuration.h>
#include <config/Errors.h>

#include "pbeast/PBeastServerUserMon.h"
#include "pbeast/PBeastServerDiskInfo.h"
#include "pbeast/PBeastRepositoryServerMon.h"
#include "pbeast/PBeastReceiverMon.h"


int main(int argc, char ** argv)
{
  try
    {
      Configuration db("bstconfig:initial");

      pbeast::PBeastServerUserMon * o1 = const_cast<pbeast::PBeastServerUserMon *>(db.get<pbeast::PBeastServerUserMon>("first"));

      if (o1)
        std::cout << "read object " << *o1 << std::endl;
      else
        o1 = const_cast<pbeast::PBeastServerUserMon *>(db.create<pbeast::PBeastServerUserMon>("initial", "first", true));

      o1->set_num_of_requests(3.14156);
      o1->set_average_size_of_data_per_inteval(314);
      o1->set_max_size_of_data_per_inteval(500);
      o1->set_average_processing_time_per_inteval(44);
      o1->set_max_processing_time_per_inteval(144);
      o1->set_total_num_of_requests(12345);
      o1->set_total_size_of_data(9999);
      o1->set_max_size_of_data(777);
      o1->set_total_processing_time(512);
      o1->set_max_processing_time(99);

      db.commit("test commit 1");

      sleep(2);

      auto v = o1->get_num_of_requests();
      std::cout << "num_of_requests@first = " << v << " (expect 3.14156)\n";

      o1->set_num_of_requests(2.718);
      o1->set_average_size_of_data_per_inteval(31415);
      o1->set_max_size_of_data_per_inteval(501);
      o1->set_average_processing_time_per_inteval(45);
      o1->set_max_processing_time_per_inteval(146);
      o1->set_total_num_of_requests(12346);
      o1->set_total_size_of_data(10000);
      o1->set_max_size_of_data(888);
      o1->set_total_processing_time(1024);
      o1->set_max_processing_time(199);

      db.commit("test commit 2");

      sleep(5);

      db.destroy(*o1);

      db.commit("test commit 3");

      sleep(2);


    }
  catch (daq::config::Exception& ex)
    {
      ers::error(ex);
    }
  catch (std::exception &ex)
    {
      std::cerr << "ERROR: " << ex.what() << std::endl;
    }


  return 0;
}
