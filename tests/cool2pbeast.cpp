#include <boost/date_time/posix_time/posix_time_types.hpp>
#include <boost/date_time/posix_time/time_formatters.hpp>
#include <boost/date_time/posix_time/time_parsers.hpp>
#include "boost/date_time/local_time/local_time.hpp"

#include <boost/program_options.hpp>
namespace po = boost::program_options;

#include <filesystem>

#include <boost/algorithm/string/replace.hpp>

#include <CoralBase/Attribute.h>
#include <CoolKernel/FolderSpecification.h>
#include <CoolKernel/DatabaseId.h>
#include <CoolKernel/IDatabaseSvc.h>
#include <CoolKernel/IDatabase.h>
#include <CoolKernel/IFolder.h>
#include <CoolKernel/IFolderSet.h>
#include <CoolKernel/IObject.h>
#include <CoolKernel/IObjectIterator.h>
#include <CoolKernel/Record.h>
#include <CoolApplication/Application.h>

#include <ers/ers.h>

#include <pbeast/data-file.h>
#include <pbeast/data-type.h>
#include <pbeast/meta.h>
#include <pbeast/output-data-file.h>

#include "../bin/application_exceptions.h"


inline uint64_t
s2ns(uint64_t s)
{
  return s * 1000000000L;
}

inline uint64_t
ns2mks(uint64_t s)
{
  return s / 1000L;
}

const uint64_t m_data_split_duration = 7 * 24 * 3600;
const uint64_t m_data_split_duration_ns = s2ns(m_data_split_duration);

// string to timestamp

static uint64_t
str2ts(std::string& in, boost::local_time::time_zone_ptr tz_ptr, const char * name)
{
  static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));

  std::string s(in);
  std::replace(s.begin(), s.end(), 'T', ' ');
  boost::posix_time::ptime t;

  try
    {
      t = boost::posix_time::time_from_string(s);
    }
  catch (std::exception& e)
    {
      std::ostringstream text;
      text << "cannot parse " << name << " = \'" << in << "\': \"" << e.what() << '\"';
      throw std::runtime_error(text.str().c_str());
    }

    // convert local time to UTC, if the time zone was provided

  if (tz_ptr)
    {
      try
        {
          boost::local_time::local_date_time lt(t.date(), t.time_of_day(), tz_ptr, boost::local_time::local_date_time::EXCEPTION_ON_ERROR);
          t = lt.utc_time();
        }
      catch (std::exception& e)
        {
          std::ostringstream text;
          text << "cannot parse " << name << " = \'" << in
              << "\' as zone\'s time: \"" << e.what() << '\"';
          throw std::runtime_error(text.str().c_str());
        }
    }
  return (t - epoch).total_nanoseconds();
}

static boost::local_time::time_zone_ptr
get_time_zone_ptr(const std::string& time_zone)
{
  const char * tz_spec_file = ::getenv("BOOST_DATE_TIME_TZ_SPEC");

  if (!tz_spec_file || !*tz_spec_file)
    {
      throw std::runtime_error("cannot read value of BOOST_DATE_TIME_TZ_SPEC environment variable to parse timezone parameter");
    }
  else
    {
      ERS_DEBUG(1, "Boost time-zone specification file is \'" << tz_spec_file << '\'');
    }

  boost::local_time::tz_database tz_db;

  try
    {
      tz_db.load_from_file(tz_spec_file);
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "cannot read Boost time-zone specification file " << tz_spec_file << ": " << ex.what();
      throw std::runtime_error(text.str().c_str());
    }

  if (time_zone == "list-regions")
    {
      for (auto & r : tz_db.region_list())
        {
          std::cout << r << std::endl;
        }

      exit(0);
    }

  if(boost::local_time::time_zone_ptr tz_ptr = tz_db.time_zone_from_region(time_zone))
    {
      return tz_ptr;
    }
  else
    {
      std::ostringstream text;
      text << "cannot find time-zone \'" << time_zone << '\'';
      throw std::runtime_error(text.str().c_str());
    }
}

static void
create_directory(const std::filesystem::path& path)
{
  bool directory_exists;

  try
    {
      directory_exists = std::filesystem::exists(path);
    }
  catch (std::filesystem::filesystem_error& ex)
    {
      throw daq::pbeast::FileSystemError(ERS_HERE, "check existence of directory", path, ex.code().message());
    }

  if (!directory_exists)
    {
      try
        {
          std::filesystem::create_directories(path);
        }
      catch (const std::filesystem::filesystem_error & ex)
        {
          throw daq::pbeast::FileSystemError(ERS_HERE, "create directory", path, ex.code().message());
        }
    }
  else if(std::filesystem::is_directory(path) == false)
    {
      throw daq::pbeast::FileSystemPathIsNotDirectory(ERS_HERE, path);
    }
}


static pbeast::DataType
ctype2ptype(cool::StorageType::TypeId cool_type)
{
  switch (cool_type)
    {
      case cool::StorageType::Bool:
        return pbeast::BOOL;

      case cool::StorageType::UChar:
        return pbeast::U8;

      case cool::StorageType::Int16:
        return pbeast::S16;

      case cool::StorageType::UInt16:
        return pbeast::U16;

      case cool::StorageType::Int32:
        return pbeast::S32;

      case cool::StorageType::UInt32:
        return pbeast::U32;

      case cool::StorageType::UInt63:
        return pbeast::U64;

      case cool::StorageType::Int64:
        return pbeast::S64;

      case cool::StorageType::Float:
        return pbeast::FLOAT;

      case cool::StorageType::Double:
        return pbeast::DOUBLE;

      case cool::StorageType::String255:
      case cool::StorageType::String4k:
      case cool::StorageType::String64k:
      case cool::StorageType::String16M:
        return pbeast::STRING;

      case cool::StorageType::Blob64k:
        throw std::runtime_error("[ctype2ptype]: \'Blob64k\' is not supported by P-BEAST");

      case cool::StorageType::Blob16M:
        throw std::runtime_error("[ctype2ptype]: \'Blob16M\' is not supported by P-BEAST");

      default:
        throw std::runtime_error("[ctype2ptype]: unexpected COOL data type");
    }
}

class AttributeBase
{
public:

  AttributeBase(pbeast::DataType type, const std::string& name) :
      m_type(type), m_name(name)
  {
    ;
  }

  static AttributeBase* create(pbeast::DataType type, const std::string& name);

  virtual ~AttributeBase() { ; }

  virtual void clear() = 0;

  virtual void write(cool::ChannelId id, const std::string& name, pbeast::OutputDataFile& file) = 0;

  virtual unsigned int size() = 0;

  const std::string&
  get_name() const
  {
    return m_name;
  }

  pbeast::DataType
  get_type()
  {
    return m_type;
  }

protected:

  pbeast::DataType m_type;
  std::string m_name;

};

template<class T> class Attribute : public AttributeBase
{
public:
    Attribute(pbeast::DataType type, const std::string& name) : AttributeBase(type, name) { ; }

    virtual ~Attribute() { clear(); }

    void
    add(cool::ChannelId id, const T& v, uint64_t since, uint64_t until)
    {
      std::vector<pbeast::SeriesData<T>>& vec = m_data[id];

      if(vec.empty() == false)
        {
          pbeast::SeriesData<T>& last = vec[vec.size()-1];
          if(last.get_value() == v)
            {
              ERS_DEBUG( 1 , "merge [" << last.get_ts_created() << ',' << last.get_ts_last_updated() << "] with [" << since << ',' << until << "] for value \'" << v << "\' of channel " << id);
              last.set_ts_last_updated(until);
              return;
            }
          else if(last.get_ts_last_updated() == since)
            {
              ERS_DEBUG( 1 , "change IoV [" << last.get_ts_created() << ',' << last.get_ts_last_updated() << "] to [" << last.get_ts_created() << ',' << (since-1) << "] for channel " << id);
              last.set_ts_last_updated(since-1);
            }
        }

      vec.emplace_back(since, until, v);
    }

    const std::vector<pbeast::SeriesData<T>>&
    get(cool::ChannelId id)
    {
      return m_data[id];
    }

    virtual void
    clear()
    {
      m_data.clear();
    }

    virtual unsigned int
    size()
    {
      return m_data.size();
    }

    virtual void
    write(cool::ChannelId id, const std::string& name, pbeast::OutputDataFile& file)
    {
      const std::vector<pbeast::SeriesData<T>>& vec(m_data[id]);

      if (vec.empty() == false)
        {
          file.write_data(name, vec);
        }
    }

private:

  std::map<cool::ChannelId, std::vector<pbeast::SeriesData<T>>> m_data;
};



AttributeBase*
AttributeBase::create(pbeast::DataType type, const std::string& name)
{
  switch (type)
    {
      case pbeast::BOOL:
        return new Attribute<bool>(type, name);

      case pbeast::U8:
        return new Attribute<uint8_t>(type, name);

      case pbeast::S8:
        return new Attribute<int8_t>(type, name);

      case pbeast::U16:
        return new Attribute<uint16_t>(type, name);

      case pbeast::S16:
        return new Attribute<int16_t>(type, name);

      case pbeast::U32:
        return new Attribute<uint32_t>(type, name);

      case pbeast::S32:
        return new Attribute<int32_t>(type, name);

      case pbeast::U64:
        return new Attribute<uint64_t>(type, name);

      case pbeast::S64:
        return new Attribute<int64_t>(type, name);

      case pbeast::FLOAT:
        return new Attribute<float>(type, name);

      case pbeast::DOUBLE:
        return new Attribute<double>(type, name);

      case pbeast::STRING:
        return new Attribute<std::string>(type, name);

      default:
        throw std::runtime_error("unexpected type");
    }
}



int main(int argc, char* argv[])
{

  // parse command line

  std::filesystem::path repository;
  std::string cool_db;
  std::string folder_name;
  boost::local_time::time_zone_ptr tz_ptr;
  uint64_t min_idx, max_idx;
  bool update_meta_only(false);

  po::options_description desc("pBeast IS data receiver");

  try
    {
      uint64_t since(cool::ValidityKeyMin);
      uint64_t until(cool::ValidityKeyMax);
      std::string since_tmp;
      std::string until_tmp;
      std::string time_zone;

      desc.add_options()
        (
          "cooldb,c",
          po::value<std::string>(&cool_db)->required(),
          "name of COOL database"
        )
        (
          "folder,f",
          po::value<std::string>(&folder_name)->required(),
          "name of folder"
        )
        (
          "repository,r",
          po::value<std::filesystem::path>(&repository)->required(),
          "Name of output pbeast repository"
        )
        (
          "since,s",
          po::value<std::string>(&since_tmp)->default_value(since_tmp),
          "Show data since given timestamp"
        )
        (
          "until,t",
          po::value<std::string>(&until_tmp)->default_value(until_tmp),
          "Show data until given timestamp"
        )
        (
          "since-raw-timestamp,S",
          po::value<uint64_t>(&since)->default_value(since),
          "Show data since given raw timestamp (number of micro-seconds since Epoch)"
        )
        (
          "until-raw-timestamp,T",
          po::value<uint64_t>(&until)->default_value(until),
          "Show data until given raw timestamp (number of micro-seconds since Epoch)"
        )
        (
          "time-zone,z",
          po::value<std::string>(&time_zone)->default_value(time_zone),
          "Name of time zone (use \"-z list-regions\" to see known time zone names)"
        )
        (
          "update-meta,m","Update meta information and exit"
        )
        (
          "help,h","Print help message"
        );

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if(vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      if (vm.count("update-meta"))
        {
          update_meta_only = true;
        }

      po::notify(vm);

      if(vm["until-raw-timestamp"].defaulted() == false && vm["until"].defaulted() == false)
        {
          throw std::runtime_error("both \"until\" and \"until-raw-timestamp\" parameters are defined");
        }

      if(vm["since-raw-timestamp"].defaulted() == false && vm["since"].defaulted() == false)
        {
          throw std::runtime_error("both \"since\" and \"since-raw-timestamp\" parameters are defined");
        }

      if (!time_zone.empty())
        {
          tz_ptr = get_time_zone_ptr(time_zone);
        }

      if(!since_tmp.empty())
        {
          since = str2ts(since_tmp, tz_ptr, "since");
        }

      if(!until_tmp.empty())
        {
          until = str2ts(until_tmp, tz_ptr, "until");
        }

      if(since >= until)
        {
          throw std::runtime_error("\"since\" is after \"until\"");
        }

      ERS_DEBUG( 1 , "since: " << since << ", until: " << until);

      min_idx = since / m_data_split_duration_ns;

      if(min_idx * m_data_split_duration_ns != since)
        {
          min_idx++;
        }

      uint64_t max_iov = s2ns(time(0));

      if(until > max_iov)
        {
          until = max_iov;
        }

      max_idx = until / m_data_split_duration_ns;

      ERS_DEBUG( 1 , "min idx: " << min_idx << ", max idx: " << max_idx);

      if(min_idx > max_idx)
        {
          throw std::runtime_error("time interval is too short");
        }
    }
  catch(std::exception& ex)
    {
      std::cerr << "Command line error: " << ex.what() << std::endl;
      return 1;
    }

  cool::IDatabasePtr p_db;
  cool::Application p_cool_app;

  try
    {
      p_db = p_cool_app.databaseService().openDatabase(cool_db, false);
    }
  catch (std::exception& ex)
    {
      std::cerr << "ERROR: failed to open COOL database \'" << cool_db << "\' : " << ex.what() << std::endl;
      return 1;
    }

  cool::IFolderPtr p_cool_folder;

  try
    {
      p_cool_folder = p_db->getFolder(folder_name);
    }
  catch (std::exception& ex)
    {
      std::cerr << "ERROR: failed to get COOL folder \'" << folder_name << "\' : " << ex.what() << std::endl;
      return 1;
    }

  const cool::IRecordSpecification& attributes = p_cool_folder->payloadSpecification();

    {
      std::ostringstream text;
      text << "The folder " << folder_name << " attributes are:\n";

      for (unsigned int i = 0; i < attributes.size(); ++i)
        {
          text << "- attribute: " << attributes[i].name() << ", type: " << attributes[i].storageType().name() << std::endl;
        }

      ERS_LOG(text.str());
    }


    // check named channels

    std::map<cool::ChannelId, std::string> channels_id2name;
    std::map<std::string, cool::ChannelId> name2channels_id;

    try
      {
        channels_id2name = p_cool_folder->listChannelsWithNames();
      }
    catch (std::exception& ex)
      {
        std::cerr << "ERROR: failed to list objects of COOL folder \'" << folder_name << "\': " << ex.what() << std::endl;
        return 1;
      }

    ERS_LOG("number of named channels: " << channels_id2name.size());

    for(auto& x : channels_id2name)
      {
        std::map<std::string, cool::ChannelId>::iterator i = name2channels_id.find(x.second);

        if(i != name2channels_id.end())
          {
            std::cerr << "ERROR: named channels " << i->second << " and " << x.first << " have the same channel name: \'" << x.second << "\'\n";
            return 1;
          }

        name2channels_id[x.second] = x.first;
      }

  // create / update meta information and directories

  std::string pbeast_class_name(folder_name,1);
  boost::replace_first(pbeast_class_name, "/DCS/", "_");
  boost::replace_all(pbeast_class_name, "/", "_");

  std::filesystem::path class_path = repository / "DCS" / pbeast::DataFile::encode(pbeast_class_name);

  ::create_directory(class_path);

  pbeast::Meta::set_info_ts(pbeast::mk_ts(min_idx * m_data_split_duration, 0));

  pbeast::Meta info(class_path);

  std::map<std::string,AttributeBase*> data;

  for (unsigned int i = 0; i < attributes.size(); ++i)
    {
      const std::string attribute_name(attributes[i].name());

      std::filesystem::path attribute_path = class_path / pbeast::DataFile::encode(attribute_name);

      ::create_directory(attribute_path);

      pbeast::DataType pbeast_type;

      try
        {
          pbeast_type = ctype2ptype(attributes[i].storageType().id());
        }
      catch (std::exception& ex)
        {
          std::cerr << "ERROR: " << ex.what() << std::endl;
          return 1;
        }

      info.update_attribute(attribute_name, pbeast::Meta::encode_type(pbeast::as_string(pbeast_type), false), "");

      data[attribute_name] = AttributeBase::create(pbeast_type, attribute_name);
   }

  try
    {
      info.commit("DCS", pbeast_class_name);
    }
  catch (daq::pbeast::Exception& ex)
    {
      ers::error(daq::pbeast::CannotCommitMetaInformation(ERS_HERE, pbeast_class_name, ex));
      return 1;
    }

  if(update_meta_only)
    return 0;


  cool::ChannelSelection all;

  std::map<cool::ChannelId, uint64_t> last_processed;

  uint64_t max_until = (max_idx + 1) * m_data_split_duration_ns;

  for (uint64_t idx = min_idx; idx <= max_idx; idx++)
    {
      cool::IObjectIteratorPtr p;

      std::map<std::string,uint64_t> cool_errors;

      uint64_t since = idx * m_data_split_duration_ns;
      uint64_t until = (idx + 1) * m_data_split_duration_ns;

      uint64_t min_ts = ns2mks(since);
      uint64_t max_ts = ns2mks(until);

      ERS_LOG("Process data for index " << idx << " in [" << since << ',' << until << "] interval");

      // clear previous data
      for (unsigned int i = 0; i < attributes.size(); ++i)
        {
          data[attributes[i].name()]->clear();
        }

      try
        {
          unsigned int count = p_cool_folder->countObjects(since, until, all);

          if(count > 500000)
            {
              ERS_LOG("the query will return " << count << " objects, set PrefetchAll = false");
              p_cool_folder->setPrefetchAll(false);
            }

          p = p_cool_folder->browseObjects(since, until, all);
        }
      catch (std::exception& ex)
        {
          std::cerr << "ERROR: failed to browse objects of COOL folder \'" << folder_name << "\' in [" << since << "," << until << "] interval: " << ex.what() << std::endl;
          return 1;
        }

      unsigned int channels_count(0), objects_count(0);

      cool::ChannelId last_id(0xffffffff);

      while (p->goToNext())
        {
          const cool::IObject& obj(p->currentRef());
          objects_count++;

          if (last_id != obj.channelId())
            {
              channels_count++;
              last_id = obj.channelId();
            }

          const uint64_t obj_since(ns2mks(obj.since()));
          const uint64_t obj_until(ns2mks(obj.until() < max_until ? obj.until() : max_until));

          std::map<cool::ChannelId, uint64_t>::iterator li = last_processed.find(last_id);

          if(li != last_processed.end())
            {
              if(li->second >= obj_until)
                {
                  ERS_DEBUG( 1 , "ignore already processed interval [" << obj_since << ',' << obj_until << "] for channel " << last_id );
                  continue;
                }
              else
                {
                  li->second = obj_until;
                }
            }
          else
            {
              last_processed[last_id] = obj_until;
            }

          if(obj_since < min_ts) min_ts = obj_since;
          if(obj_until > max_ts) max_ts = obj_until;

          for (unsigned int i = 0; i < attributes.size(); ++i)
            {
              const std::string attribute_name = attributes[i].name();

              try
                {
                  switch (attributes[i].storageType().id())
                    {
                      case cool::StorageType::Bool:
                        static_cast<Attribute<bool>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::Bool>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::UChar:
                        static_cast<Attribute<uint8_t>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::UChar>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::Int16:
                        static_cast<Attribute<int16_t>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::Int16>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::UInt16:
                        static_cast<Attribute<uint16_t>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::UInt16>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::Int32:
                        static_cast<Attribute<int32_t>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::Int32>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::UInt32:
                        static_cast<Attribute<uint32_t>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::UInt32>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::UInt63:
                        static_cast<Attribute<uint64_t>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::UInt63>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::Int64:
                        static_cast<Attribute<int64_t>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::Int64>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::Float:
                        static_cast<Attribute<float>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::Float>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::Double:
                        static_cast<Attribute<double>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::Double>(), obj_since, obj_until);
                        break;

                      case cool::StorageType::String255:
                      case cool::StorageType::String4k:
                      case cool::StorageType::String64k:
                      case cool::StorageType::String16M:
                        static_cast<Attribute<std::string>*>(data[attribute_name])->add(obj.channelId(), obj.payload()[attribute_name].data<cool::String16M>(), obj_since, obj_until);
                        break;

                      default:
                        throw std::runtime_error("never reach this");
                    }
                }
              catch (std::exception& ex)
                {
                  std::string error = ex.what();

                  std::map<std::string,uint64_t>::iterator ei = cool_errors.find(error);

                  if(ei == cool_errors.end())
                    {
                      std::cout << "ERROR: COOL db read of attribute " << attribute_name << " of object " << obj.channelId() << "[" << obj.since() << ',' << obj.until() << "] failed: " << error << std::endl;
                      cool_errors[error] = 1;
                    }
                  else
                    {
                      ei->second++;
                    }
                }
            }
        }

      ERS_LOG("for index " << idx << " there are " << channels_count << " channels (" << objects_count << " IoV objects)");

      for (const auto&x : data)
        {
          try
            {
              if (x.second->size() == 0)
                {
                  ERS_LOG("skip empty attribute \'" << x.first << "\'for index " << idx);
                  continue;
                }

              std::filesystem::path file_path = pbeast::OutputDataFile::make_file_name(
                repository,
                "DCS",
                pbeast::DataFile::encode(pbeast_class_name),
                x.first,
                pbeast::ts2secs(min_ts),
                pbeast::ts2secs(max_ts),
                ".pb",
                true
              );

              std::cout.flush();

              pbeast::OutputDataFile file(file_path.native(), "DCS", pbeast_class_name, pbeast::DataFile::encode(pbeast_class_name), x.first, pbeast::mk_ts(min_ts, 0), 1, x.second->get_type(), false, x.second->size(), true, true, true);

              file.write_header();

              for(auto& v : name2channels_id)
                {
                  x.second->write(v.second, v.first, file);
                }

              file.write_footer();

              int64_t written = file.close();

              ERS_LOG("store " << x.first << '@' << pbeast_class_name << "[" << idx << "] to file " << file_path << '(' << written << " bytes)");
            }
          catch (const std::exception& ex)
            {
              std::cerr << "ERROR: write to file failed: " << ex.what() << std::endl;
              return 1;
            }
        }

      for(auto&x : cool_errors)
        {
          if(x.second != 1)
            {
              std::cerr << "COOL error \'" << x.first << "\' appeared " << x.second << " times for index " << idx << std::endl;
            }
        }
    }



  return 0;
}
