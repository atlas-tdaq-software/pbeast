#include "pbeast/output-data-file.h"
#include "pbeast/series-data-cvt.h"
#include "pbeast/data-type.h"

#include <sys/time.h>
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <stdint.h>
#include <stdlib.h>

#include <boost/program_options.hpp>
namespace po = boost::program_options;

const uint64_t base_ts = 1234567890L;


static double
get_time_interval(const timeval * t2, const timeval * t1)
{
  return (t2->tv_sec - t1->tv_sec) + (t2->tv_usec - t1->tv_usec) / 1000000.;
}

void gen_random(char *s, const int len) {
    static const char alphanum[] =
        "0123456789"
        "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        "abcdefghijklmnopqrstuvwxyz";

    for (int i = 0; i < len; ++i) {
        s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
    }

    s[len] = 0;
}


struct Data {
  std::string name;
  std::vector<pbeast::SeriesData<int32_t> > data;

  Data(bool small);

  void dump();
};

Data::Data(bool small)
{
  char buf[128];
  gen_random(buf, 10 + (rand()%118));
  name = buf;

  int len = rand()%10000 + 1;

  data.reserve(len);

  for(int i = 0; i < len; ++i) {
    int32_t val = (int32_t)rand();
//    if(small && (rand() % 17) != 0) val /= 16348;
    if(small && (rand() % 17) != 0) val /= 1068576;

    uint64_t ts_created((base_ts + i) * 1000000 + (rand()%1000000));
    uint64_t ts_last_updated(ts_created);
    if(rand()%100 > 20) {
        ts_last_updated += 1 + rand()%1000000;
    }
    data.push_back(pbeast::SeriesData<int32_t>(ts_created, ts_last_updated, val));
  }
}

int main(int argc, char* argv[]) {

  // parse command line

  std::string data_file;
  int32_t num;
  pbeast::DataType type = pbeast::VOID;
  bool zip_catalog = false;
  bool zip_data = false;
  bool catalogize_strings = false;
  bool small_data = false;


  po::options_description desc("pBeast IS data receiver");

  try
    {
      std::string data_type_str;

      desc.add_options()
        (
          "output-file,o",
          po::value<std::string>(&data_file),
          "Name of output data file"
        )
        (
          "num-of-objects,n",
          po::value<int>(&num),
          "Number of objects"
        )
        (
          "data-type,t",
          po::value<std::string>(&data_type_str),
          "Data type"
        )
        (
          "small-data,S", "Create small data"
        )
        (
          "zip-catalog,z", "Zip catalog"
        )
        (
          "zip-data,Z", "Zip data"
        )
        (
          "catalogize-strings,C", "catalogize string data"
        )
        (
          "help,h","Print help message"
        );

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if(vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);

      if(data_file.empty())
        {
          throw std::runtime_error("the option \'--output-file\' is required but missing");
        }

      if(data_type_str.empty())
        {
          throw std::runtime_error("the option \'--data-type\' is required but missing");
        }

      if      (!strcasecmp("BOOL", data_type_str.c_str()))   type = pbeast::BOOL;
      else if (!strcasecmp("S8", data_type_str.c_str()))     type = pbeast::S8;
      else if (!strcasecmp("U8", data_type_str.c_str()))     type = pbeast::U8;
      else if (!strcasecmp("S16", data_type_str.c_str()))    type = pbeast::S16;
      else if (!strcasecmp("U16", data_type_str.c_str()))    type = pbeast::U16;
      else if (!strcasecmp("S32", data_type_str.c_str()))    type = pbeast::S32;
      else if (!strcasecmp("U32", data_type_str.c_str()))    type = pbeast::U32;
      else if (!strcasecmp("S64", data_type_str.c_str()))    type = pbeast::S64;
      else if (!strcasecmp("U64", data_type_str.c_str()))    type = pbeast::U64;
      else if (!strcasecmp("FLOAT", data_type_str.c_str()))  type = pbeast::FLOAT;
      else if (!strcasecmp("DOUBLE", data_type_str.c_str())) type = pbeast::DOUBLE;
      else if (!strcasecmp("DATE", data_type_str.c_str()))   type = pbeast::DATE;
      else if (!strcasecmp("TIME", data_type_str.c_str()))   type = pbeast::TIME;
      else if (!strcasecmp("STRING", data_type_str.c_str())) type = pbeast::STRING;
      else {
        std::cerr << "bad TYPE" << data_type_str << std::endl;
        return -1;
      }

      if(vm.count("zip-catalog"))
        {
          zip_catalog = true;
        }

      if(vm.count("zip-data"))
        {
          zip_data = true;
        }

      if(vm.count("catalogize-strings"))
        {
          catalogize_strings = true;
        }

      if(vm.count("small-data"))
        {
          small_data = true;
        }
    }
  catch(std::exception& ex)
    {
      std::cerr << "Command line error: " << ex.what() << std::endl;
    }

  

  uint64_t total_data_size(0);

  struct timeval t1, t2;

  gettimeofday( &t1, 0 );

  std::map<std::string, Data *> series;

  for(int i = 0; i < num; ++ i) {
    Data * d = 0;
    while(d == 0) {
      d = new Data(small_data);
      if(series.find(d->name) != series.end()) {
        std::cerr << "Oops, string " << d->name << " already exists, trying next ..." << std::endl;
        d = 0;
      }
    }
    series[d->name] = d;
    total_data_size += d->data.size();
  }

  gettimeofday( &t2, 0 );
  std::cout << " * generate " << num << " (data items: " << total_data_size << ") data series: " << get_time_interval(&t2, &t1) << " seconds" << std::endl;

  gettimeofday( &t1, 0 );

  try {
    pbeast::OutputDataFile file(data_file, "ATLAS", "TestClass", "TestClass", "TestAttribute", base_ts, 1000, type, false, num, zip_catalog, zip_data, catalogize_strings);

    file.write_header();

    for(std::map<std::string, Data *>::const_iterator it = series.begin(); it != series.end(); ++it) {
      if(type == pbeast::S32) {
        file.write_data(it->first,it->second->data);
      }
      else if(type == pbeast::U32) {
        std::vector<pbeast::SeriesData<uint32_t> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type == pbeast::U64) {
        std::vector<pbeast::SeriesData<uint64_t> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type == pbeast::S64) {
        std::vector<pbeast::SeriesData<int64_t> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type == pbeast::U16) {
        std::vector<pbeast::SeriesData<uint16_t> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type == pbeast::S16) {
        std::vector<pbeast::SeriesData<int16_t> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type == pbeast::U8) {
        std::vector<pbeast::SeriesData<uint8_t> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type == pbeast::S8) {
        std::vector<pbeast::SeriesData<int8_t> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type == pbeast::FLOAT) {
        std::vector<pbeast::SeriesData<float> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type == pbeast::DOUBLE) {
        std::vector<pbeast::SeriesData<double> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type == pbeast::BOOL) {
        std::vector<pbeast::SeriesData<bool> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
      else if(type >= pbeast::DATE) {
        std::vector<pbeast::SeriesData<std::string> > data;
        pbeast::convert(it->second->data,data);
        file.write_data(it->first,data);
      }
    }

    file.write_footer();

    file.close();
  }
  catch(std::exception& ex) {
    std::cerr << "ERROR: " << ex.what() << std::endl;
    return -1;
  }

  gettimeofday( &t2, 0 );
  std::cout << " * save data: " << get_time_interval(&t2, &t1) << " seconds" << std::endl;

  return 0;
}
