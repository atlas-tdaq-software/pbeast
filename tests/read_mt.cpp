
#include <stdlib.h>
#include <chrono>
#include <thread>
#include <fstream>
#include <sstream>

#include <boost/program_options.hpp>

#include "pbeast/functions.h"
#include "pbeast/data-type.h"
#include "pbeast/def-intervals.h"
#include "pbeast/query.h"
#include "pbeast/series-data.h"
#include "pbeast/series-print.h"
#include "../bin/application_exceptions.h"


  /*! \class pbeast::ApplicationFailed
   *  This issue is reported when application exits because of fatal errors
   */
ERS_DECLARE_ISSUE(pbeast, ThreadFailed, "Thread " << id << " failed", ((unsigned int)id));


struct QueryParamaters {
  std::string partition_name;
  std::string class_name;
  std::string attribute_name;
  std::string object_name;
  bool object_name_is_regexp = false;
  boost::local_time::time_zone_ptr tz_ptr;
  uint64_t since = pbeast::get_def_since();
  uint64_t until = pbeast::get_def_until();
  uint64_t prev_interval = pbeast::def_lookup_prev_const;
  uint64_t next_interval = pbeast::def_lookup_next_const;
  uint32_t interval = 0;
  pbeast::FillGaps::Type fill_gaps;
  bool list_updated_objects = false;
  bool list_partitions = false;
  bool list_classes = false;
  bool list_attributes = false;
  bool print_every_update = false;
  pbeast::FunctionConfig function_config;
  pbeast::FunctionConfig * fc_ptr = nullptr;
  bool use_fqn = false;
} params;


static void
print(pbeast::ServerProxy& proxy, unsigned int count, unsigned int sleep_ms, int thread_num)
{
  for (unsigned int x = 0; x < count; ++x)
    {
      try
        {
          auto t1 = std::chrono::steady_clock::now();

          std::string msg;

          if (params.list_partitions)
            {
              msg = std::to_string(proxy.get_partitions().size()) + " partitions";
            }
          else if (params.list_classes)
            {
              msg = std::to_string(proxy.get_classes(params.partition_name).size()) + " classes in partition \"" + params.partition_name + '\"';
            }
          else if (params.list_attributes)
            {
              msg = std::to_string(proxy.get_attributes(params.partition_name, params.class_name).size()) + " attributes in class \"" + params.class_name + "\" of partition \""
                  + params.partition_name + '\"';
            }
          else if (params.list_updated_objects)
            {
              msg = std::to_string(proxy.get_objects(params.partition_name, params.class_name, params.attribute_name, params.object_name, params.since, params.until).size())
                  + " objects in  \"" + params.attribute_name + '@' + params.class_name + "\" of partition \"" + params.partition_name + '\"';
            }
          else
            {
              std::vector<std::shared_ptr<pbeast::QueryDataBase>> result = proxy.get_data(
                  params.partition_name, params.class_name, params.attribute_name, params.object_name, params.object_name_is_regexp, params.since, params.until,
                  params.prev_interval, params.next_interval, params.print_every_update, params.interval, params.fill_gaps, params.function_config,
                  pbeast::ServerProxy::ZipCatalog | pbeast::ServerProxy::ZipData | pbeast::ServerProxy::CatalogizeStrings, params.use_fqn);
              msg = std::to_string(result.size()) + " results returnd by query";
            }

          ERS_LOG('[' << thread_num << "] [" << x << "] get " << msg << " in " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count() << " ms");
        }
      catch (std::exception & ex)
        {
          ers::error(pbeast::ThreadFailed(ERS_HERE, thread_num, ex));
        }

      if (sleep_ms)
        std::this_thread::sleep_for (std::chrono::milliseconds(sleep_ms));
    }
}


// string to timestamp

static uint64_t
str2ts(std::string& in, boost::local_time::time_zone_ptr tz_ptr, const char * name)
{
  static boost::posix_time::ptime epoch(boost::gregorian::date(1970,1,1));

  std::string s(in);
  std::replace(s.begin(), s.end(), 'T', ' ');
  boost::posix_time::ptime t;

  try
    {
      t = boost::posix_time::time_from_string(s);
    }
  catch (std::exception& e)
    {
      std::ostringstream text;
      text << "cannot parse " << name << " = \'" << in << "\': \"" << e.what() << '\"';
      throw std::runtime_error(text.str().c_str());
    }

    // convert local time to UTC, if the time zone was provided

  if (tz_ptr)
    {
      try
        {
          boost::local_time::local_date_time lt(t.date(), t.time_of_day(),
              tz_ptr, boost::local_time::local_date_time::EXCEPTION_ON_ERROR);
          ERS_DEBUG(1,
              "Build zone\'s time \'" << in << "\' => \'" << lt.to_string() << "\' using \'" << tz_ptr->to_posix_string() << '\'');
          t = lt.utc_time();
        }
      catch (std::exception& e)
        {
          std::ostringstream text;
          text << "cannot parse " << name << " = \'" << in
              << "\' as zone\'s time: \"" << e.what() << '\"';
          throw std::runtime_error(text.str().c_str());
        }
    }
  return (t - epoch).total_microseconds();
}

static boost::local_time::time_zone_ptr
get_time_zone_ptr(const std::string& time_zone)
{
  const char * tz_spec_file = ::getenv("BOOST_DATE_TIME_TZ_SPEC");

  if (!tz_spec_file || !*tz_spec_file)
    {
      throw std::runtime_error("cannot read value of BOOST_DATE_TIME_TZ_SPEC environment variable to parse timezone parameter");
    }
  else
    {
      ERS_DEBUG(1, "Boost time-zone specification file is \'" << tz_spec_file << '\'');
    }

  boost::local_time::tz_database tz_db;

  try
    {
      tz_db.load_from_file(tz_spec_file);
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "cannot read Boost time-zone specification file " << tz_spec_file << ": " << ex.what();
      throw std::runtime_error(text.str().c_str());
    }

  if (time_zone == "list-regions")
    {
      for (auto & r : tz_db.region_list())
        {
          std::cout << r << std::endl;
        }

      exit(0);
    }

  if(boost::local_time::time_zone_ptr tz_ptr = tz_db.time_zone_from_region(time_zone))
    {
      return tz_ptr;
    }
  else
    {
      std::ostringstream text;
      text << "cannot find time-zone \'" << time_zone << '\'';
      throw std::runtime_error(text.str().c_str());
    }
}


int
main(int argc, char *argv[])
{
  boost::program_options::options_description desc("Run queries using P-BEAST ");

  unsigned int thread_num = 4;
  unsigned int sleep_ms = 10;
  unsigned int count = 10;
  std::string base_url;

  try
    {
      std::string since_tmp;
      std::string until_tmp;
      std::string time_zone;
      std::string object_names_regexp;
      std::vector<std::string> functions;
      std::string fill_gaps_tmp("near");
      std::streamsize precision(std::cout.precision());

      std::string functions_desc = std::string("Space-separated ") + pbeast::FunctionConfig::make_description_of_functions();

      desc.add_options()
        ( "thread-num,e", boost::program_options::value<unsigned int>(&thread_num)->default_value(thread_num), "Number of threads" )
        ( "sleep-interval,E", boost::program_options::value<unsigned int>(&sleep_ms)->default_value(sleep_ms), "Sleep interval" )
        ( "count,X", boost::program_options::value<unsigned int>(&count)->default_value(count), "Number of queries per thread" )
        ( "base-url,n", boost::program_options::value<std::string>(&base_url)->required(), "URL of pbeast server" )
        ( "partition-name,p", boost::program_options::value<std::string>(&params.partition_name), "Name of partition" )
        ( "class-name,c", boost::program_options::value<std::string>(&params.class_name), "Name of class" )
        ( "attribute-name,a", boost::program_options::value<std::string>(&params.attribute_name), "Name of attribute or path in case of nested type" )
        ( "object-name,o", boost::program_options::value<std::string>(&params.object_name), "Name of object" )
        ( "object-name-regexp,O", boost::program_options::value<std::string>(&object_names_regexp), "Regular expression for names of objects" )
        ("use-fully-qualified-object-names,F", "Use fully qualified names (put dot-separated partition, class and attribute names prefix for object names)" )
        ( "list-updated-objects,l", "List names of updated objects" )
        ( "list-partitions,L", "List partitions" )
        ( "list-classes,C", "List classes" )
        ( "list-attributes,A", "List attributes" )
        ( "since,s", boost::program_options::value<std::string>(&since_tmp)->default_value(since_tmp), "Show data since given timestamp" )
        ( "until,t", boost::program_options::value<std::string>(&until_tmp)->default_value(until_tmp), "Show data until given timestamp" )
        ( "lookup-previous-value-time-lapse,P", boost::program_options::value<uint64_t>(&params.prev_interval)->default_value(params.prev_interval), "Time lapse to lookup closest previous data point outside query time interval (in microseconds)" )
        ( "lookup-next-value-time-lapse,N", boost::program_options::value<uint64_t>(&params.next_interval)->default_value(params.next_interval), "Time lapse to lookup closest next data point outside query time interval (in microseconds)" )
        ( "since-raw-timestamp,S", boost::program_options::value<uint64_t>(&params.since)->default_value(params.since), "Show data since given raw timestamp (number of micro-seconds since Epoch)" )
        ( "until-raw-timestamp,T", boost::program_options::value<uint64_t>(&params.until)->default_value(params.until), "Show data until given raw timestamp (number of micro-seconds since Epoch)" )
        ( "downsample-interval,i", boost::program_options::value<uint32_t>(&params.interval), "Downsampling interval in seconds (if not set or zero, print raw data)" )
        ( "fill-gaps,g", boost::program_options::value<std::string>(&fill_gaps_tmp)->default_value(fill_gaps_tmp), "When downsample, approximate missing values: \"none\", \"near\" or \"all\"" )
        ( "functions,f", boost::program_options::value<std::vector<std::string> >(&functions)->multitoken(), functions_desc.c_str() )
        ( "keep-aggregated-data,K", "Keep original data after aggregation" )
        ( "time-zone,z", boost::program_options::value<std::string>(&time_zone)->default_value(time_zone), "Name of time zone (use \"-z list-regions\" to see known time zone names)" )
        ( "print-raw-date-time,W", "Print date and time attributes in raw format (number of micro-seconds since Epoch) instead of formatted one" )
        ( "precision,V", boost::program_options::value<std::streamsize>(&precision)->default_value(precision), "Set floating point data output precision" )
        ( "print-value-per-update,u", "Print value per IS object update even if the value remained unchanged" )
        ( "help,h", "Print help message" );

      const boost::program_options::positional_options_description p;
      boost::program_options::variables_map vm;
      boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      boost::program_options::notify(vm);

      if (vm.count("object-name-regexp"))
        params.object_name_is_regexp = true;

      if (vm.count("use-fully-qualified-object-names"))
        params.use_fqn = true;

      if (vm.count("list-updated-objects"))
        params.list_updated_objects = true;

      if (vm.count("list-partitions"))
        params.list_partitions = true;
      else if (params.partition_name.empty())
        throw std::runtime_error("partition name is required parameter");

      if (vm.count("list-classes"))
        params.list_classes = true;
      else if (!params.list_partitions && params.class_name.empty())
        throw std::runtime_error("class name is required parameter");

      if (vm.count("list-attributes"))
        params.list_attributes = true;
      else if (!params.list_partitions && !params.list_classes && params.attribute_name.empty())
        throw std::runtime_error("attribute name is required parameter");

      if (vm.count("print-value-per-update"))
        params.print_every_update = true;

      if (vm["until-raw-timestamp"].defaulted() == false && vm["until"].defaulted() == false)
        throw std::runtime_error("both \"until\" and \"until-raw-timestamp\" parameters are defined");

      if (vm["since-raw-timestamp"].defaulted() == false && vm["since"].defaulted() == false)
        throw std::runtime_error("both \"since\" and \"since-raw-timestamp\" parameters are defined");

      if (!time_zone.empty())
        params.tz_ptr = get_time_zone_ptr(time_zone);

      if (!since_tmp.empty())
        params.since = str2ts(since_tmp, params.tz_ptr, "since");

      if (!until_tmp.empty())
        params.until = str2ts(until_tmp, params.tz_ptr, "until");

      if (precision != std::cout.precision())
        std::cout.precision(precision);

     if (!object_names_regexp.empty())
        if (object_names_regexp != ".*")
          params.object_name = object_names_regexp;

      if (functions.empty() == false)
        {
          params.function_config.construct(functions, params.interval);
          params.fc_ptr = &params.function_config;
        }

      if (vm.count("keep-aggregated-data"))
        {
          if(params.function_config.no_aggregations())
            throw std::runtime_error("cannot use \"keep-aggregated-data\" parameter without \"aggregate\" functions");

          params.function_config.m_keep_data = true;
        }

      params.fill_gaps = pbeast::FillGaps::mk(fill_gaps_tmp);
    }
  catch (std::exception& ex)
    {
      std::cerr << "Command line parsing errors occurred:\n" << ex.what() << std::endl;
      return EXIT_FAILURE;
    }

  std::cout << "run " << thread_num << " threads" << std::endl;

  if (thread_num < 1)
    {
      std::cerr << "ERROR: invalid number of threads" << std::endl;
      return 1;
    }

  try
    {
      pbeast::ServerProxy proxy(base_url);

      std::vector<std::thread> v;
      v.reserve(thread_num);

      for (unsigned int i = 1; i <= thread_num; ++i)
        v.push_back(std::thread(print, std::ref(proxy), count, sleep_ms, i));

      for (unsigned int i = 0; i < thread_num; ++i)
        v[i].join();
    }
  catch (std::exception & ex)
    {
      ers::fatal(daq::pbeast::ApplicationFailed(ERS_HERE, ex));
      return EXIT_FAILURE;
    }

  return EXIT_SUCCESS;
}
