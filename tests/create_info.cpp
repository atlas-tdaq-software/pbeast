#include <unistd.h>
#include <time.h>

#include <string>
#include <iostream>
#include <sstream>

#include <boost/program_options.hpp>

#include <ipc/core.h>

#include "pbeast/SmallNamed.h"
#include "pbeast/LargeNamed.h"

#include "pbeast/LineNamed.h"
#include "pbeast/PolyLineNamed.h"
#include "pbeast/SimpleGraphNamed.h"

namespace po = boost::program_options;

std::string generate_str(const std::string& prefix, char sep, const std::string& body, unsigned long object_id, unsigned long mult, unsigned long idx)
{
  std::ostringstream s;
  s << prefix << sep << body << (object_id * mult + idx);
  return s.str();
}

pbeast::SmallNamed::enumeration small_enums[] = {
  pbeast::SmallNamed::A,
  pbeast::SmallNamed::B,
  pbeast::SmallNamed::C,
  pbeast::SmallNamed::D,
  pbeast::SmallNamed::E,
  pbeast::SmallNamed::F,
  pbeast::SmallNamed::G,
  pbeast::SmallNamed::H,
  pbeast::SmallNamed::K,
  pbeast::SmallNamed::L
};

pbeast::LargeNamed::array_Enumeration_ large_enums[] = {
  pbeast::LargeNamed::AA,
  pbeast::LargeNamed::BB,
  pbeast::LargeNamed::CC,
  pbeast::LargeNamed::DD,
  pbeast::LargeNamed::EE,
  pbeast::LargeNamed::FF,
  pbeast::LargeNamed::GG,
  pbeast::LargeNamed::HH,
  pbeast::LargeNamed::KK,
  pbeast::LargeNamed::LL
};


void init_small(pbeast::SmallNamed& o, unsigned int idx, const char * str_prefix)
{
  o.Boolean = false;
  o.S8_integer = 0;
  o.U8_integer = 0;
  o.S16_integer = 0;
  o.U16_integer = 0;
  o.S32_integer = 0L;
  o.U32_integer = 0L;
  o.S64_integer = 0L;
  o.U64_integer = 0L;
  o.Float = 0.0;
  o.Double = 0.0;
  o.String = generate_str(str_prefix, ' ', "string ", idx, 10000, 0);
  o.Enumeration = pbeast::SmallNamed::A;
  o.Date = OWLDate();
  o.Time = OWLTime();
}

unsigned int get_little_prime()
{
  static unsigned int numbers[] = {3, 5, 7, 11, 13};

  static unsigned int count(0);

  const int len(sizeof(numbers)/sizeof(numbers[0]));

  return numbers[count++ % len];

}

unsigned int get_small_prime()
{
  static unsigned int numbers[] = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31};
  //static unsigned int numbers[] = {3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107};

  static unsigned int count(0);

  const int len(sizeof(numbers)/sizeof(numbers[0]));

  return numbers[count++ % len];
}

unsigned int get_prime()
{
  static unsigned int numbers[] = {
    3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107,
    109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199, 211, 223, 227, 229,
    233, 239, 241, 251, 257, 263, 269, 271, 277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359,
    367, 373, 379, 383, 389, 397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491,
    499, 503, 509, 521, 523, 541, 547, 557, 563, 569, 571, 577, 587, 593, 599, 601, 607, 613, 617, 619, 631, 641,
    643, 647, 653, 659, 661, 673, 677, 683, 691, 701, 709, 719, 727, 733, 739, 743, 751, 757, 761, 769, 773, 787,
    797, 809, 811, 821, 823, 827, 829, 839, 853, 857, 859, 863, 877, 881, 883, 887, 907, 911, 919, 929, 937, 941,
    947, 953, 967, 971, 977, 983, 991, 997 };

  static unsigned int count(0);

  const int len(sizeof(numbers)/sizeof(numbers[0]));

  return numbers[count++ % len];
}

void init_large(pbeast::LargeNamed& o)
{
  o.Array_Boolean_.resize( 2 );
  o.Array_Boolean_[0] = true;
  o.Array_Boolean_[1] = false;

  o.Array_S8_integer_.resize( 3 );
  o.Array_S8_integer_[0] = 0;
  o.Array_S8_integer_[1] = 1;
  o.Array_S8_integer_[2] = 2;

  o.Array_U8_integer_.resize( 3 );
  o.Array_U8_integer_[0] = 128;
  o.Array_U8_integer_[1] = 129;
  o.Array_U8_integer_[2] = 130;

  o.Array_Enumeration_.resize( 2 );
  o.Array_Enumeration_[0] = pbeast::LargeNamed::BB;
  o.Array_Enumeration_[1] = pbeast::LargeNamed::CC;
}

unsigned int update_small(pbeast::SmallNamed& o, unsigned int count, unsigned int idx, unsigned int j)
{
  unsigned int total(0);
  if(!(count % get_prime())) {
    o.Boolean = ((count % 3) == 0); total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] Boolean     => " << o.Boolean);
  }
  if(!(count % get_small_prime())) {
    o.S8_integer = count % 127; total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] S8_integer  => " << static_cast<int>(o.S8_integer));
  }
  if(!(count % get_small_prime())) {
    o.U8_integer = count % 255; total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] U8_integer  => " << static_cast<int>(o.U8_integer));
  }
  if(!(count % get_small_prime())) {
    o.S16_integer = count % 32767; total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] S16_integer => " << o.S16_integer);
  }
  if(!(count % get_small_prime())) {
    o.U16_integer = count % 65535; total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] U16_integer => " << o.U16_integer);
  }
  if(!(count % get_small_prime())) {
    o.S32_integer = idx * 10000 + j * (((2 * count) % 3) ? 1 : -1); total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] S32_integer => " << o.S32_integer);
  }
  if(!(count % get_small_prime())) {
    o.U32_integer = idx * 10000 + j * (((2 * count) % 3) ? 1 : -1); total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] U32_integer => " << o.U32_integer);
  }
  if(!(count % get_small_prime())) {
    o.S64_integer = idx * 1000000L + j * (((2 * count) % 3) ? 1 : -1); total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] S64_integer => " << o.S64_integer);
  }
  if(!(count % get_small_prime())) {
    o.U64_integer = idx * 1000000L + j * (((2 * count) % 3) ? 1 : -1); total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] U64_integer => " << o.U64_integer);
  }
  if(!(count % get_small_prime())) {
    o.Float = static_cast<float>(idx) + static_cast<float>(j)/10000.0 * (((2 * count) % 3) ? 1 : -1); total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] Float       => " << o.Float);
  }
  if(!(count % get_small_prime())) {
    o.Double = static_cast<double>(idx) + static_cast<double>(j)/1000000.0 * (((2 * count) % 3) ? 1 : -1); total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] Double      => " << o.Double);
  }
  if(!(count % get_small_prime())) {
    o.String = generate_str("test small", ' ', "string ", idx, 10000, j);
    ERS_DEBUG(2, " [" << j << ':' << idx << "] String      => \"" << o.String << '\"');
  }
  if(!(count % get_prime())) {
    o.Enumeration = small_enums[count % 10]; total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] Enumeration => " << o.Enumeration);
  }
  if(!(count % get_prime())) {
    time_t now;
    time(&now);
    now += (24 * 3600) * (j % 100 + 1);
    o.Date = OWLDate(now); total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] Date        => " << o.Date);
  }
  if(!(count % get_prime())) {
    time_t now;
    time(&now);
    now += (24 * 3600) * (j % 100 + 1);
    o.Time = OWLTime(now); total++;
    ERS_DEBUG(2, " [" << j << ':' << idx << "] Time        => " << o.Time);
  }

  return total;
}

unsigned int gen_len(unsigned int len, unsigned int count, unsigned int i1, unsigned int i2, unsigned int i3)
{
  if(!(count % i1)) return (len + 1);
  else if( (len > 1 && !(count % i2)) ) return (len - 1);
  else if( len > 10 && !(count % (31 + i3)) ) return (len / 2);
  else return len;
}

void update_large(pbeast::LargeNamed& o, unsigned int count, unsigned int idx, unsigned int j)
{
  if(!(count % get_prime())) {
    unsigned int len = gen_len(o.Array_Boolean_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_Boolean_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_Boolean_[i] = (((11 * count / (i+1)) % 2) == 0);
    }
  }
  if(!(count % get_prime())) {
    unsigned int len = gen_len(o.Array_S8_integer_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_S8_integer_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_S8_integer_[i] = (count+i) % 127;
    }
  }
  if(!(count % get_prime())) {
    unsigned int len = gen_len(o.Array_U8_integer_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_U8_integer_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_U8_integer_[i] = (count+i) % 255;
    }
  }
  if(!(count % get_small_prime())) {
    unsigned int len = gen_len(o.Array_S16_integer_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_S16_integer_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_S16_integer_[i] = (count+i) % 32767;
    }
  }
  if(!(count % get_small_prime())) {
    unsigned int len = gen_len(o.Array_U16_integer_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_U16_integer_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_U16_integer_[i] = (count+i) % 65535;
    }
  }
  if(!(count % get_small_prime())) {
    unsigned int len = gen_len(o.Array_S32_integer_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_S32_integer_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_S32_integer_[i] = idx * 10000 + j * 100 + i;
    }
  }
  if(!(count % get_small_prime())) {
    unsigned int len = gen_len(o.Array_U32_integer_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_U32_integer_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_U32_integer_[i] = idx * 10000 + i * 100 + j;
    }
  }
  if(!(count % get_small_prime())) {
    unsigned int len = gen_len(o.Array_S64_integer_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_S64_integer_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_S64_integer_[i] = idx * 1000000L + j * 10000 + i;
    }
  }
  if(!(count % get_small_prime())) {
    unsigned int len = gen_len(o.Array_U64_integer_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_U64_integer_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_U64_integer_[i] = idx * 1000000L + i * 10000 + j;
    }
  }
  if(!(count % get_prime())) {
    unsigned int len = gen_len(o.Array_Float_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_Float_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_Float_[i] = static_cast<float>(idx) + static_cast<float>(j * 100 + i)/10000.0;
    }
  }
  if(!(count % get_prime())) {
    unsigned int len = gen_len(o.Array_Double_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_Double_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_Double_[i] = static_cast<float>(idx) * 1000 + static_cast<float>(j * 1000 + i)/10000.0;
    }
  }
  if(!(count % get_small_prime())) {
    unsigned int len = gen_len(o.Array_String_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_String_.resize(len);
    for(unsigned int i = 0; i < len; ++i ) {
      o.Array_String_[i] = generate_str("test large", ' ', "string ", idx, 100000 + (i * 100), j);
    }
  }
  if(!(count % get_prime())) {
    time_t now;
    time(&now);
    unsigned int len = gen_len(o.Array_Date_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_Date_.resize(len);
    now += (24 * 3600) * (j % 100 + 1);
    for(unsigned int i = 0; i < len; ++i ) {
      now += (24 * 3600) * (j % 100 + 1 + i);
      o.Array_Date_[i] = OWLDate(now);
    }
  }
  if(!(count % get_prime())) {
    time_t now;
    time(&now);
    unsigned int len = gen_len(o.Array_Time_.size(), count, get_little_prime(), get_prime(), get_little_prime());
    o.Array_Time_.resize(len);
    now += (24 * 3600) * (j % 100 + 1);
    for(unsigned int i = 0; i < len; ++i ) {
      now += 3600 * (j % 100 + 1 + i);
      o.Array_Time_[i] = OWLTime(now);
    }
  }
}

unsigned int update_simple_line(pbeast::SimpleLineNamed& o, unsigned int count, unsigned int idx)
{
  unsigned int total(0);

  if(!(count % get_prime()))
    {
      o.first_point.x = idx + count % 10;
      total++;
    }

  if(!(count % get_prime()))
    {
      o.first_point.y = idx + 1 + count % 10;
      total++;
    }

  if(!(count % get_prime()))
    {
      o.first_point.x = idx * 2 + count % 10;
      total++;
    }

  if(!(count % get_prime()))
    {
      o.first_point.y = idx * 2 + 1 + count % 10;
      total++;
    }

  return total;
}

unsigned int update_line(pbeast::LineNamed& o, unsigned int count, unsigned int idx, unsigned int j)
{
  unsigned int total(0);
  if(!(count % get_prime()))
    {
      std::ostringstream text;
      text << "line:red" << (count % 3);
      o.Color = text.str();
      total++;
    }

  if(!(count % get_prime()))
    {
      o.Width = 1 + (float)2.719 * (float)idx + static_cast<float>(j)/10000.0;
      total++;
    }

  return (total + update_simple_line(o, count, idx));
}

unsigned int update_polyline(pbeast::PolyLineNamed& o, unsigned int count, unsigned int idx, unsigned int idx2)
{
  unsigned int total(0);

  // regenerate all points
  if(!(count % get_small_prime()))
    {
      o.Points.clear();
      for(unsigned int j = 0; j < (2 + (count % 5)); j++)
        {
          pbeast::Point p;
          p.x = idx + (count % 7) * 3;
          p.y = idx2 + (count % 11) * 2;
          p.color = "polyline:green";
          o.Points.push_back(p);
          total++;
        }
    }

  if(!(count % get_small_prime()))
    {
      for(auto & p : o.Points)
        {
          p.x = (count % 7) * 2;
          p.y = (count % 13) * 3;
          total++;
        }
    }

  if (!(count % get_small_prime()))
    {
      o.Width = 1 + (float)3.14 * ( (idx + count) % 7);
      total++;
    }

  if (!(count % get_prime()))
    {
      std::ostringstream t;
      t << "red" << ((idx2 + count) % 255);
      o.Color = t.str();
      total++;
    }

  return total;
}

unsigned int update_graph(pbeast::SimpleGraphNamed& o, unsigned int count, unsigned int idx, unsigned int idx2)
{
  unsigned int total(0);

  // regenerate all polylines
  if(!(count % get_small_prime()))
    {
      o.Lines.clear();

      for(unsigned int k = 0; k < (2 + (idx % 5)); k++)
        {
          pbeast::PolyLine pl;
          for(unsigned int j = 0; j < (3 * k + (idx % 3)); j++)
            {
              pbeast::Point p;
              p.x = k + (j+1) * 7 + idx * 3;
              p.y = k + (j+1) * 7 + idx2 * 5;
              p.color = "graph:green";
              pl.Points.push_back(p);
              total++;
            }
          pl.Color = "white";
          pl.Width = 1 + (float)2.97 * (float)idx;
          o.Lines.push_back(pl);
        }
    }

  if(!(count % get_small_prime()))
    {
      o.Points.clear();
      for(unsigned int j = 0; j < (2 + (count % 5)); j++)
        {
          pbeast::Point p;
          p.x = ((count + idx) % 7) * 3;
          p.y = ((count + idx2) % 11) * 2;
          p.color = "graph:green";
          o.Points.push_back(p);
          total++;
        }
    }

  if(!(count % get_small_prime()))
    {
      for(auto & p : o.Points)
        {
          p.x = ((count + idx) % 17) * 2;
          p.y = ((count + idx2) % 13) * 3;
          total++;
        }
    }

  if (!(count % get_prime()))
    {
      std::ostringstream t;
      t << "yellow" << (count % 257);
      o.Color = t.str();
      total++;
    }

  return total;
}

int main(int argc, char ** argv)
{
    // initialise IPC and set default CORBA parameters

  try {
    IPCCore::init( argc, argv );
  }
  catch(ers::Issue & ex) {
    ers::fatal(ex);
    return 1;
  }


    // parse command line

  std::string partition_name;
  std::string server_name;
  std::string small_prefix("pBeast test small object ");
  std::string large_prefix("pBeast test large object ");
  std::string simple_line_prefix("simple line ");
  std::string line_prefix("line ");
  std::string polyline_prefix("polyline ");
  std::string graph_prefix("graph ");
  unsigned int number_of_updates = 10;
  unsigned int number_of_small_objects = 111;
  unsigned int number_of_large_objects = 11;
  unsigned int number_of_line_objects = 19;
  unsigned int number_of_simple_line_objects = 23;
  unsigned int number_of_polyline_objects = 17;
  unsigned int number_of_graph_objects = 2;
  unsigned int interval = 250;

  po::options_description desc("pBeast IS data generator");

  try {
    desc.add_options()
      (
        "partition,p",
        po::value<std::string>(&partition_name)->required(),
        "Name of the partition"
      )
      (
        "server,n",
        po::value<std::string>(&server_name)->required(),
        "Name of IS server"
      )
      (
        "small-object-prefix,x",
        po::value<std::string>(&small_prefix)->default_value(small_prefix),
        "Prefix of small object name"
      )
      (
        "large-object-prefix,X",
        po::value<std::string>(&large_prefix)->default_value(large_prefix),
        "Prefix of large object name"
      )
      (
        "simple-line-object-prefix,S",
        po::value<std::string>(&simple_line_prefix)->default_value(simple_line_prefix),
        "Prefix of simple line object name"
      )
      (
        "line-object-prefix,y",
        po::value<std::string>(&line_prefix)->default_value(line_prefix),
        "Prefix of line object name"
      )
      (
        "polyline-object-prefix,Y",
        po::value<std::string>(&polyline_prefix)->default_value(polyline_prefix),
        "Prefix of polyline object name"
      )
      (
        "graph-object-prefix,H",
        po::value<std::string>(&graph_prefix)->default_value(graph_prefix),
        "Prefix of graph object name"
      )
      (
        "updates-number,u",
        po::value<unsigned int>(&number_of_updates)->default_value(number_of_updates),
        "Number of updates"
      )
      (
        "small-objects-number,o",
        po::value<unsigned int>(&number_of_small_objects)->default_value(number_of_small_objects),
        "Number of small objects"
      )
      (
        "large-objects-number,O",
        po::value<unsigned int>(&number_of_large_objects)->default_value(number_of_large_objects),
        "Number of large objects"
      )
      (
        "line-objects-number,l",
        po::value<unsigned int>(&number_of_line_objects)->default_value(number_of_line_objects),
        "Number of line objects"
      )
      (
        "simple-line-objects-number,s",
        po::value<unsigned int>(&number_of_simple_line_objects)->default_value(number_of_simple_line_objects),
        "Number of simple line objects"
      )
      (
        "polyline-objects-number,L",
        po::value<unsigned int>(&number_of_polyline_objects)->default_value(number_of_polyline_objects),
        "Number of polyline objects"
      )
      (
        "graph-objects-number,G",
        po::value<unsigned int>(&number_of_graph_objects)->default_value(number_of_graph_objects),
        "Number of graph objects"
      )
      (
        "update-interval,i",
        po::value<unsigned int>(&interval)->default_value(interval),
        "Update interval (in ms)"
      )
      (
        "help,h","Print help message"
      );

    const po::positional_options_description p; // empty positional options
    po::variables_map vm;
    po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

    if(vm.count("help")) {
      std::cout << desc << std::endl;
      return 0;
    }

    po::notify(vm);
  }
  catch(std::exception& ex) {
    std::ostringstream text;
    text << ex.what() << "\nUsage: " << desc;
    return 1;
  }

  interval *= 1000;  // microseconds => milliseconds

  ERS_LOG("INTERVAL: " << interval);

  try {
    IPCPartition partition(partition_name);

    std::vector<pbeast::SmallNamed *> small_objects;
    std::vector<pbeast::SmallNamed *> small_objects_rarely_updated;
    std::vector<pbeast::LargeNamed *> large_objects;
    std::vector<pbeast::SimpleLineNamed *> simple_line_objects;
    std::vector<pbeast::LineNamed *> line_objects;
    std::vector<pbeast::PolyLineNamed *> polyline_objects;
    std::vector<pbeast::SimpleGraphNamed *> graph_objects;

    // create small objects
    for(unsigned long i = 0; i < number_of_small_objects; ++i) {
      pbeast::SmallNamed * o = new pbeast::SmallNamed(partition, generate_str(server_name, '.', small_prefix, i, 1, 0));
      init_small(*o, i, "small");
      o->checkin( );
      small_objects.push_back(o);
    }

    ERS_LOG("created " << number_of_small_objects << " small objects");
    // create small objects
    for(unsigned long i = 0; i < number_of_small_objects; ++i) {
      pbeast::SmallNamed * o = new pbeast::SmallNamed(partition, generate_str(server_name, '.', small_prefix + "_rare", i, 1, 0));
      init_small(*o, i, "small");
      o->checkin( );
      small_objects_rarely_updated.push_back(o);
    }

    ERS_LOG("created " << number_of_small_objects << " rarely updated small objects");

    // create large objects
    for(unsigned long i = 0; i < number_of_large_objects; ++i) {
      pbeast::LargeNamed * o = new pbeast::LargeNamed(partition, generate_str(server_name, '.', large_prefix, i, 1, 0));
      init_small(*o, i, "large");
      init_large(*o);
      o->checkin( );
      large_objects.push_back(o);
    }

    ERS_LOG("created " << number_of_large_objects << " large objects");


    // create simple line objects
    for(unsigned long i = 0; i < number_of_simple_line_objects; ++i) {
      pbeast::SimpleLineNamed * o = new pbeast::SimpleLineNamed(partition, generate_str(server_name, '.', simple_line_prefix, i, 1, 0));
      o->first_point.x = 0;
      o->first_point.y = i * 3;
      o->second_point.x = i * 5;
      o->second_point.y = 0;
      o->checkin( );
      simple_line_objects.push_back(o);
    }

    // create line objects
    for(unsigned long i = 0; i < number_of_line_objects; ++i) {
      pbeast::LineNamed * o = new pbeast::LineNamed(partition, generate_str(server_name, '.', line_prefix, i, 1, 0));
      o->Color = "line:red";
      o->Width = 1 + (float)2.719 * (float)i;
      o->first_point.x = 0;
      o->first_point.y = i;
      o->second_point.x = i * 2;
      o->second_point.y = 0;
      o->checkin( );
      line_objects.push_back(o);
    }

    // create polyline objects
    for(unsigned long i = 0; i < number_of_polyline_objects; ++i) {
      pbeast::PolyLineNamed * o = new pbeast::PolyLineNamed(partition, generate_str(server_name, '.', polyline_prefix, i, 1, 0));
      for(unsigned int j = 0; j < (2 + (i % 5)); j++)
        {
          pbeast::Point p;
          p.x = i * 3;
          p.y = i * 2;
          p.color = "polyline:green";
          o->Points.push_back(p);
        }
      o->Color = "blue";
      o->Width = 1 + (float)3.14 * (float)i;
      o->checkin( );
      polyline_objects.push_back(o);
    }

    // create graph objects
    for(unsigned long i = 0; i < number_of_graph_objects; ++i) {
      pbeast::SimpleGraphNamed * o = new pbeast::SimpleGraphNamed(partition, generate_str(server_name, '.', graph_prefix, i, 1, 0));
      for(unsigned int k = 0; k < (i % 5); k++)
        {
          pbeast::PolyLine pl;
          for(unsigned int j = 0; j < (3 * k + (i % 3)); j++)
            {
              pbeast::Point p;
              p.x = k + (j+1) * 7 + i * 3;
              p.y = k + (j+1) * 7 + i * 5;
              p.color = "polyline:green";
              pl.Points.push_back(p);
            }
          pl.Color = "white";
          pl.Width = 1 + (float)2.97 * (float)i;
          o->Lines.push_back(pl);
        }
      for(unsigned int j = 0; j < (i % 7); j++)
        {
          pbeast::Point p;
          p.x = ((j+1) % 7) * 3;
          p.y = ((j+1) % 11) * 2;
          p.color = "graph:black";
          o->Points.push_back(p);
        }
      o->checkin( );
      graph_objects.push_back(o);
    }

    unsigned long count(0);

    for(unsigned int j = 0; j < number_of_updates; ++j) {
      unsigned int total_attrs(0);
      unsigned int total_objs(0);
      if(interval>0) usleep(interval);
      for(unsigned int i = 0; i < number_of_small_objects; ++i) {
        pbeast::SmallNamed * o = small_objects[i];
        count++;
        if(unsigned int x = update_small(*o, ++count, i, j)) {
          total_attrs += x;
          total_objs++;
        }
        ERS_DEBUG(1, "=> update small object " << i);
        o->checkin( );
        if(!(count % 1001)) {
          ERS_DEBUG(1, "=> remove small object " << i);
          o->remove();
        }
      }
      for(unsigned int i = 0; i < number_of_small_objects; ++i) {
        pbeast::SmallNamed * o = small_objects_rarely_updated[i];
        count++;
        if(!(count % 7001)) {
          if(unsigned int x = update_small(*o, ++count, i, j)) {
            total_attrs += x;
            total_objs++;
          }
          ERS_DEBUG(1, "=> update small object " << i);
          o->checkin( );
        }
        if(!(count % 2017) && o->isExist()) {
          if(o->isExist()) {
          ERS_DEBUG(1, "=> remove small object " << i);
          o->remove();
          }
          else {
            ERS_DEBUG(1, "=> reinsert small object " << i);
            o->checkin();
          }
        }
      }

     for(unsigned int i = 0; i < number_of_large_objects; ++i) {
        pbeast::LargeNamed * o = large_objects[i];
        count++;
        if(unsigned int x = update_small(*o, ++count, i, j)) {
          total_attrs += x;
          total_objs++;
        }
        update_large(*o, count, i, j);
        ERS_DEBUG(1, "=> update large object " << i);
        o->checkin( );
        if(!(count % 1003)) {
          ERS_DEBUG(1, "=> remove large object " << i);
          o->remove();
        }
      }

      for(unsigned int i = 0; i < number_of_line_objects; ++i) {
        pbeast::LineNamed * o = line_objects[i];
        count++;
        if(unsigned int x = update_line(*o, ++count, i, j)) {
          total_attrs += x;
          total_objs++;
        }
        ERS_DEBUG(1, "=> update line object " << i);
        o->checkin( );
        if(!(count % 1001)) {
          ERS_DEBUG(1, "=> remove line object " << i);
          o->remove();
        }
      }

      for(unsigned int i = 0; i < number_of_simple_line_objects; ++i) {
        pbeast::SimpleLineNamed * o = simple_line_objects[i];
        count++;
        if(unsigned int x = update_simple_line(*o, ++count, i)) {
          total_attrs += x;
          total_objs++;
        }
        ERS_DEBUG(1, "=> update simple line object " << i);
        o->checkin( );
        if(!(count % 1001)) {
          ERS_DEBUG(1, "=> remove simple line object " << i);
          o->remove();
        }
      }

      for(unsigned int i = 0; i < number_of_polyline_objects; ++i) {
        pbeast::PolyLineNamed * o = polyline_objects[i];
        update_polyline(*o, ++count, i, j);

        ERS_DEBUG(1, "=> update polyline object " << i);
        o->checkin( );
        if(!(count % 1001)) {
          ERS_DEBUG(1, "=> remove polyline object " << i);
          o->remove();
        }
      }

      for(unsigned int i = 0; i < number_of_graph_objects; ++i) {
        pbeast::SimpleGraphNamed * o = graph_objects[i];
        update_graph(*o, ++count, i, j);

        ERS_DEBUG(1, "=> update graph object " << i);
        o->checkin( );
        if(!(count % 1001)) {
          ERS_DEBUG(1, "=> remove graph object " << i);
          o->remove();
        }
      }
      ERS_LOG(j << '\t' << " a: " << total_attrs << " o: " << total_objs << " of " << (number_of_small_objects + number_of_large_objects));
    }
  }
  catch(ers::Issue& ex) {
    std::cerr << "Caught exception: " << ex << std::endl;
    return 1;
  }

  return 0;
}
