////////////////////////////////////////////////////////////////////////
//            f_receiver.cc
//
//            Test application for the IS library
//
//            Sergei Kolos,    January 2000
//
//            description:
//	            Test the functionality of the ISInfoReceiver class
//	
////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <atomic>

#include <boost/type_traits/integral_promotion.hpp>

#include <ers/ers.h>
#include <cmdl/cmdargs.h>
#include <owl/semaphore.h>
#include <ipc/core.h>
#include <ipc/signal.h>
#include <is/infoany.h>
#include <is/inforeceiver.h>

#include <oh/core/Histogram.h>

OWLSemaphore sem;

std::ostream& operator<< ( std::ostream& , const ISInfoAny & );

std::ostream & Print(const ISInfoAny &, std::ostream &);

namespace pbeast
{
  size_t print_attribute_value( std::ostream & out, ISInfoAny & isa, const ISType & type, size_t entry, int how_many = -1 );
  void   print_attribute_type ( std::ostream & out, const ISType & type, size_t entry, size_t size );
}

namespace pbeast
{
    template <typename T>
    struct attribute
    {
        static unsigned int
        print( std::ostream & out, ISInfoAny & isa, const ISType & type, unsigned int entry, int how_many = -1 )
        {
            if ( type.entryArray( entry ) )
            {
                std::vector<T> value;
                isa >> value;

                unsigned int printed_size = how_many > -1 && (int)value.size() > how_many ? how_many : value.size();

                if ( printed_size > 0 )
                {
                    for ( unsigned int i = 0; i < printed_size - 1; i++ )
                        out << static_cast<typename boost::integral_promotion<T>::type>( value[i] ) << ", ";
                    out << static_cast<typename boost::integral_promotion<T>::type>( value[printed_size - 1] );
                    if ( printed_size != value.size() )
                        out << " {... " << value.size() - printed_size << " more values}";
                }
                return value.size();
            }
            else
            {
                T value;
                isa >> value;
                out << static_cast<typename boost::integral_promotion<T>::type>( value );
                return 1;
            }
        }
    };

    template <>
    struct attribute<ISInfoAny>
    {
        static size_t
        print( std::ostream & out, ISInfoAny & isa, const ISType & type, size_t entry, int how_many = -1 )
        {
            size_t size = type.entryArray( entry ) ? isa.readArraySize() : 1;

            ISType entry_type = type.entryInfoType( entry );
            for ( size_t i = 0; i < size; i++ )
            {
                out << "[";
                for ( size_t j = 0; j < entry_type.entries(); j++ )
                {
                    pbeast::print_attribute_value( out, isa, entry_type, j, how_many );
                    if ( j < entry_type.entries() - 1 )
                        out << ", ";
                }
                if ( i < size - 1 )
                    out << "], ";
                else
                    out << "]";
            }
            return size;
        }
    };

#define PRINT_ATTRIBUTE(x,y)            case ISType::y : return attribute<x>::print( out, isa, type, entry, how_many );
#define PRINT_ATTRIBUTE_SEPARATOR
#define PRINT_ATTRIBUTE_OBJECT_TYPE     ISInfoAny

    size_t
    print_attribute_value( std::ostream & out, ISInfoAny & isa, const ISType & type, size_t entry, int how_many )
    {
        switch ( type.entryType( entry ) )
        {
            IS_TYPES( PRINT_ATTRIBUTE )
            default :   return 0;
        }
    }
    void
    print_attribute_type( std::ostream & out, const ISType & type, size_t entry, size_t size )
    {
        std::string type_name( type.entryTypeName( entry ) );
        if ( type.entryArray( entry ) )
        {
            std::ostringstream os;
            os << size;
            type_name += "[" + os.str() + "]";
        }
        out << type_name;
    }
}

std::ostream & Print(const ISInfoAny & isa, std::ostream & out)
{
    size_t count = isa.countAttributes( );

    out << " HAS " << count << " attribute(s):" << std::endl;
    for ( size_t i = 0; i < count; i++ )
    {
        std::ostringstream value;

        size_t size = pbeast::print_attribute_value( value, const_cast<ISInfoAny&>( isa ), isa.type(), i );

        out.width( ISType::max_type_name_width );
        out.setf( std::ios::left, std::ios::adjustfield );
        pbeast::print_attribute_type( out, isa.type(), i, size );

        out << value.str() << std::endl;
    }

    const_cast<ISInfoAny&>( isa ).reset();

    return out;
}


ISInfoReceiver * Receiver;

static double
get_time_interval(const timeval * t2, const timeval * t1)
{
  return (t2->tv_sec - t1->tv_sec) + (t2->tv_usec - t1->tv_usec) / 1000000.;
}

struct InfoCallback
{
    void callback( ISCallbackInfo * isc )
    {
        static std::atomic<uint_least64_t> count(0);
        static struct timeval t1;

        if(count == 0)
          {
            gettimeofday( &t1, 0 );
          }

        ISInfoAny isa;
        isc->value(isa);
        Print(isa, std::cout);

        if( (++count % 10000) == 0) {
          struct timeval t2;
          gettimeofday( &t2, 0 );
          std::cerr << "rate = " << (double)10000.0 / get_time_interval(&t2, &t1) << " Hz" << std::endl;
          t1 = t2;
        }
    }
};

int
main(int argc, char ** argv)
{
  try
    {
      IPCCore::init(argc, argv);
    }
  catch (daq::ipc::Exception & ex)
    {
      is::fatal(ex);
    }

  // Declare arguments
  CmdArgStr partition_name('p', "partition", "partition-name", "partition to work in.");
  CmdArgStrList servers('n', "servers", "[servers...]", "names of servers to subscribe to.", CmdArg::isREQ);

  // Declare command object and its argument-iterator
  CmdLine cmd(*argv, &partition_name, &servers, NULL);

  CmdArgvIter arg_iter(--argc, ++argv);

  cmd.description("TEST");

  // Parse arguments
  cmd.parse(arg_iter);

  IPCPartition p(partition_name);

  ISInfoReceiver receiver(p);
  Receiver = &receiver;

  try
    {
      InfoCallback info_callback;

      ISType type = oh::Histogram::type();
      type = ~type;
      type = !type;
      ISCriteria criteria(".*", type);

      for (unsigned int i = 0; i < servers.count(); i++)
        {
          receiver.subscribe((const char *) servers[i], criteria, &InfoCallback::callback, &info_callback);
          ERS_LOG("subscribe " << servers[i]);
        }

      auto sig_received = daq::ipc::signal::wait_for();

      ERS_LOG("Caught signal " << sig_received << ", exiting...");

      for (unsigned int i = 0; i < servers.count(); i++)
        {
          receiver.unsubscribe((const char *) servers[i], criteria);
          ERS_LOG("unsubscribe " << servers[i]);
        }
    }
  catch (daq::is::Exception & ex)
    {
      std::cerr << "FATAL: " << ex << std::endl;
      return 1;
    }

  return 0;
}
