#!/bin/sh

export LD_LIBRARY_PATH=~/pbeast_test:$LD_LIBRARY_PATH
export PATH=~/pbeast_test:$PATH

ff=$1

cp $ff /data/test

f=/data/test/`basename $ff`

pbeast_zip -f $f -o $f.n

pbeast_zip -c -f $f.n -o $f.c &
pbeast_zip -d -f $f.n -o $f.d &
pbeast_zip -s -f $f.n -o $f.s &
pbeast_zip -z -f $f.n -o $f.z &
pbeast_zip -c -d -f $f.n -o $f.cd &
pbeast_zip -d -s -f $f.n -o $f.ds &

for job in `jobs -p`
do
echo $job
    echo wait pbeast_zip $job
    wait $job 
done

pbeast_read_file -f $f.n -p > $f.n.out &
pbeast_read_file -f $f.c -p > $f.c.out &
pbeast_read_file -f $f.d -p > $f.d.out &
pbeast_read_file -f $f.s -p > $f.s.out &
pbeast_read_file -f $f.z -p > $f.z.out &
pbeast_read_file -f $f.cd -p > $f.cd.out &
pbeast_read_file -f $f.ds -p > $f.ds.out &

for job in `jobs -p`
do
echo $job
    echo wait pbeast_read_file $job
    wait $job
done

diff $f.n.out $f.c.out > $f.c.out.diff &
diff $f.n.out $f.d.out > $f.d.out.diff &
diff $f.n.out $f.s.out > $f.s.out.diff &
diff $f.n.out $f.z.out > $f.z.out.diff &
diff $f.n.out $f.cd.out > $f.cd.out.diff &
diff $f.n.out $f.ds.out > $f.ds.out.diff &

for job in `jobs -p`
do
echo $job
    echo wait diff $job
    wait $job
done

set -x
set -v

cat $f.c.out.diff
cat $f.d.out.diff
cat $f.s.out.diff
cat $f.z.out.diff
cat $f.cd.out.diff
cat $f.ds.out.diff
