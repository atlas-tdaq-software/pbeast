#include <unistd.h>
#include <time.h>

#include <filesystem>
#include <string>
#include <iostream>
#include <sstream>
#include <list>

#include <boost/program_options.hpp>

#include <ipc/core.h>

#include <pbeast/meta.h>
#include <pbeast/repository.h>
#include <pbeast/thread_pool.h>

#include "pbeast/Pf32Named.h"
#include "pbeast/Pf64Named.h"
#include "pbeast/Ps64Named.h"
#include "pbeast/Pu32Named.h"
#include "pbeast/Pu64Named.h"
#include "pbeast/Pf32sNamed.h"
#include "pbeast/Pf64sNamed.h"
#include "pbeast/Ps64sNamed.h"
#include "pbeast/Pu32sNamed.h"
#include "pbeast/Pu64sNamed.h"
#include "pbeast/PstrNamed.h"


namespace po = boost::program_options;


template<class T, class V, typename IS>
  class Object
  {

  public:

    Object(IPCPartition& partition, const std::string &id, const std::vector<V> data, const std::set<uint64_t> &upd) :
        m_idx(0), m_mem_size(0), m_obj(new IS (partition, id))
    {
      m_data.reserve(upd.size());

      std::set<uint64_t>::const_iterator udt_it = upd.begin();
      uint64_t udt_next = *udt_it;

      for (const auto &x : data)
        {
          const auto &value(x.get_value());
          const uint64_t created_ts(x.get_ts_created());
          const uint64_t last_updated_ts(x.get_ts_last_updated());

          while (udt_next != 0 && udt_next <= created_ts)
            {
              udt_it++;
              udt_next = (udt_it != upd.end()) ? *udt_it : 0;
            }

          m_data.emplace_back(value);
          m_mem_size += x.get_data_size();

          while (udt_next != 0 && udt_next < last_updated_ts)
            {
              m_data.emplace_back(value);
              m_mem_size += x.get_data_size();
              udt_it++;
              udt_next = (udt_it != upd.end()) ? *udt_it : 0;
            }

          if (created_ts != last_updated_ts)
            {
              m_data.emplace_back(value);
              m_mem_size += x.get_data_size();
            }
        }

      ERS_DEBUG(0, "read object " << m_obj->name() << " containing " << m_data.size() << " data points (" << (m_mem_size / 1024 / 1024) << " MB of RAM)");

      m_base = m_data.size() / 20;
    }

    ~Object()
    {
      delete m_obj;
    }

    uint64_t
    size() const
    {
      return m_data.size();
    }

    uint64_t
    ram_size() const
    {
      return m_mem_size;
    }

    IS*
    get()
    {
      if (m_obj)
        {
          if (m_idx < m_base)
            {
              m_obj->a0 = m_data[m_base * 0 + m_idx];
              m_obj->a1 = m_data[m_base * 1 + m_idx];
              m_obj->a2 = m_data[m_base * 2 + m_idx];
              m_obj->a3 = m_data[m_base * 3 + m_idx];
              m_obj->a4 = m_data[m_base * 4 + m_idx];
              m_obj->a5 = m_data[m_base * 5 + m_idx];
              m_obj->a6 = m_data[m_base * 6 + m_idx];
              m_obj->a7 = m_data[m_base * 7 + m_idx];
              m_obj->a8 = m_data[m_base * 8 + m_idx];
              m_obj->a9 = m_data[m_base * 9 + m_idx];
              m_obj->aa = m_data[m_base * 10 + m_idx];
              m_obj->ab = m_data[m_base * 11 + m_idx];
              m_obj->ac = m_data[m_base * 12 + m_idx];
              m_obj->ad = m_data[m_base * 13 + m_idx];
              m_obj->ae = m_data[m_base * 14 + m_idx];
              m_obj->af = m_data[m_base * 15 + m_idx];
              m_obj->ag = m_data[m_base * 16 + m_idx];
              m_obj->ah = m_data[m_base * 17 + m_idx];
              m_obj->ai = m_data[m_base * 18 + m_idx];
              m_obj->aj = m_data[m_base * 19 + m_idx];

              ++m_idx;

              return m_obj;
            }
          else
            {
              delete m_obj;
              m_obj = nullptr;
            }
        }

      return nullptr;
    }


  private:
    std::vector<T> m_data;
    uint32_t m_idx;
    uint32_t m_base;
    uint64_t m_mem_size;
    IS * m_obj;
  };


struct task
{

  template<typename T, typename V, typename IS>
    static void
    run(std::mutex &mutex, uint64_t &count, std::chrono::time_point<std::chrono::steady_clock> &tp, std::list<Object<T, V, IS>*> &pool, uint64_t id)
    {
      {
        std::lock_guard<std::mutex> lock(mutex);
        ERS_DEBUG(0, "start thread " << id);
      }

      try
        {
          while (!pool.empty())
            {
              auto o = pool.front();
              pool.pop_front();

              while (ISNamedInfo *obj = o->get())
                {
                  obj->checkin();

                  std::lock_guard<std::mutex> lock(mutex);

                  if ((++count % 500) == 0)
                    {
                      std::cout << count / 500 << ' ' << std::chrono::duration_cast<std::chrono::nanoseconds>(std::chrono::steady_clock::now() - tp).count() << std::endl;
                      tp = std::chrono::steady_clock::now();
                    }
                }

              delete o;
            }
        }
      catch (std::exception &ex)
        {
          std::cerr << "ERROR: " << ex.what() << std::endl;
        }

      std::lock_guard<std::mutex> lock(mutex);
      ERS_DEBUG(0, "finish thread " << id);
    }

};


template<typename T, typename V, typename IS>
  void
  process(pbeast::Repository &db, IPCPartition& partition, const std::string& server_name, uint64_t num_of_threads, uint64_t max_data_points, const std::string& partition_name, const std::string &class_name, const std::filesystem::path &attribute_path, uint64_t since, uint64_t until, uint64_t max_ram_size)
  {
    pbeast::ThreadPool thread_pool(num_of_threads);

    std::map<std::string, std::set<uint64_t>> eov, upd;
    pbeast::CallStats stat;

    std::set<std::string> objects;
    db.list_updated_objects(objects, partition_name, class_name, attribute_path, since, until, "", stat);

    std::vector<std::list<Object<T, V, IS>*>> pool(num_of_threads);

    const uint64_t max_data_points_per_pool = max_data_points / num_of_threads;
    const uint64_t max_ram_size_per_pool = max_ram_size / num_of_threads;

    uint64_t num_of_data_points = 0;
    uint64_t count = 0;

    while (num_of_data_points < max_data_points)
      {
        uint64_t idx = 0;

        for (auto &x : pool)
          {
            uint64_t num = 0;
            uint64_t ram_size = 0;

            while (!objects.empty())
              {
                auto it = objects.begin();
                const std::string id(*it);
                objects.erase(it);

                std::map<std::string, std::set<uint64_t>> eov, tmp_upd;

                std::map<std::string, std::vector<V>> tmp_data;
                db.read_attribute<V>(partition_name, class_name, attribute_path, since, until, 0, 0, id, false, "", false, tmp_data, eov, &tmp_upd, stat);

                if (tmp_upd.empty() || tmp_upd.begin()->second.empty())
                  {
                    ERS_DEBUG(1, "skip empty " << id << " data series");
                  }
                else
                  {
                    auto o = new Object<T, V, IS>(partition, server_name + id.substr(id.find_first_of(".")), tmp_data.begin()->second, tmp_upd.begin()->second);
                    num += o->size();
                    ram_size += o->ram_size();

                    x.push_back(o);

                    if (num > max_data_points_per_pool || ram_size > max_ram_size_per_pool)
                      {
                        num_of_data_points += num;
                        break;
                      }
                  }
              }

            ERS_LOG("put " << num << " data points (" << ram_size / 1024 / 1024 << " MB in RAM) into pool " << ++idx);
          }

        idx = 0;

        std::mutex mutex;
        std::vector<std::future<void> > results;
        std::chrono::time_point<std::chrono::steady_clock> tp = std::chrono::steady_clock::now();

        for (auto &i : pool)
          {
            results.push_back(thread_pool.enqueue(std::bind(&task::run<T, V, IS>, std::ref(mutex), std::ref(count), std::ref(tp), std::ref(i), ++idx)));
          }

        for (unsigned int i = 0; i < results.size(); ++i)
          {
            results[i].get();
          }

        if (objects.empty())
          break;
      }

    ERS_LOG("inserted " << num_of_data_points << " data points");
  }


int main(int argc, char ** argv)
{
    // initialise IPC and set default CORBA parameters

  try {
    IPCCore::init( argc, argv );
  }
  catch(ers::Issue & ex) {
    ers::fatal(ex);
    return 1;
  }


    // parse command line

  std::filesystem::path repository_path, local_repository_path;
  std::string server_partition("initial"), server_name;
  std::string partition_name, class_name, _attribute_path;
  uint64_t num_of_threads = 2;
  uint64_t num_of_data_points = 30000 * 10240;
  uint64_t since = 0;
  uint64_t until = 0;
  uint64_t max_ram_size = 4096;

  po::options_description desc("Read P-BEAST data and store into predefined classes for performance tests");

  try
    {
      desc.add_options()
          ("repository-dir,r", po::value<std::filesystem::path>(&repository_path)->required(), "Name of the repository directory containing P-BEAST files")
          ("partition,p", po::value<std::string>(&partition_name)->required(), "Name of the partition")
          ("class-name,c", po::value<std::string>(&class_name)->required(), "Name of class")
          ("attribute-name,a", po::value<std::string>(&_attribute_path)->required(), "Name of attribute or path in case of nested type")
          ("server-partition,P", po::value<std::string>(&server_partition)->default_value(server_partition), "Name of the partition to run IS server")
          ("server,n", po::value<std::string>(&server_name)->required(), "Name of IS server")
          ("threads-num,t", po::value<uint64_t>(&num_of_threads)->default_value(num_of_threads), "Number of processing threads")
          ("data-points-num,m", po::value<uint64_t>(&num_of_data_points)->default_value(num_of_data_points), "Number of data points")
          ("since,s", po::value<uint64_t>(&since)->required(), "Read data since given timestamp (number of micro-seconds since Epoch)")
          ("until,u", po::value<uint64_t>(&until)->required(), "Read data until given timestamp (number of micro-seconds since Epoch)")
          ("max-ram-size,b", po::value<uint64_t>(&max_ram_size)->default_value(max_ram_size), "Maximum RAM size (in MB)")
          ("help,h", "Print help message");

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);

    }
  catch (const std::exception &ex)
    {
      std::cerr << "ERROR: " << ex.what() << std::endl;
      return 1;
    }

  std::filesystem::path attribute_path(_attribute_path);
  std::string attribute_name;

  bool is_array;

  try
    {
      IPCPartition partition(server_partition);

      pbeast::DownsampleConfig ds_config;
      pbeast::Repository db(repository_path, local_repository_path, ds_config);

      db.init_lock();

      std::string attribute_class_name;

      pbeast::Meta::get_attribute_class_and_name(attribute_path, attribute_class_name, attribute_name);

      if (attribute_class_name.empty())
        attribute_class_name = class_name;

      pbeast::Meta info(repository_path, partition_name, attribute_class_name);

      const std::vector<pbeast::SeriesData<std::string>> &types = info.get_attribute_types(attribute_name);

      const unsigned int len = types.size();

      if (len == 0)
        {
          throw std::runtime_error("cannot find data type attribute information");
        }
      else if (len > 1)
        {
          throw std::runtime_error("the data type has been changed inside the time interval");
        }

      pbeast::DataType data_type;
      std::string type_str;

      pbeast::Meta::decode_type(types[0].get_value(), type_str, is_array);
      data_type = pbeast::str2type(type_str);
      ERS_DEBUG(0, "read type: " << types[0].get_value() << " => " << (int)data_type << ", is array: " << std::boolalpha << is_array);

      if (data_type == pbeast::OBJECT)
        {
          std::ostringstream text;
          text << "attribute " << attribute_path << " points to nested object type; provide full path to leave attribute";
          throw std::runtime_error(text.str().c_str());
        }

      max_ram_size *= 1024 * 1024;

      if (data_type == pbeast::U32)
        {
          if (is_array == false)
            process<uint32_t, pbeast::SeriesData<uint32_t>, pbeast::Pu32Named>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
          else
            process<std::vector<uint32_t>, pbeast::SeriesVectorData<uint32_t>, pbeast::Pu32sNamed>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
        }
      else if (data_type == pbeast::U64)
        {
          if (is_array == false)
            process<uint64_t, pbeast::SeriesData<uint64_t>, pbeast::Pu64Named>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
          else
            process<std::vector<uint64_t>, pbeast::SeriesVectorData<uint64_t>, pbeast::Pu64sNamed>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
        }
      else if (data_type == pbeast::S64)
        {
          if (is_array == false)
            process<uint64_t, pbeast::SeriesData<int64_t>, pbeast::Ps64Named>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
          else
            process<std::vector<int64_t>, pbeast::SeriesVectorData<int64_t>, pbeast::Ps64sNamed>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
        }
      else if (data_type == pbeast::FLOAT)
        {
          if (is_array == false)
            process<float, pbeast::SeriesData<float>, pbeast::Pf32Named>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
          else
            process<std::vector<float>, pbeast::SeriesVectorData<float>, pbeast::Pf32sNamed>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
        }
      else if (data_type == pbeast::DOUBLE)
        {
          if (is_array == false)
            process<double, pbeast::SeriesData<double>, pbeast::Pf64Named>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
          else
            process<std::vector<double>, pbeast::SeriesVectorData<double>, pbeast::Pf64sNamed>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
        }
      else if (data_type == pbeast::STRING)
        process<std::string, pbeast::SeriesData<std::string>, pbeast::PstrNamed>(db, partition, server_name, num_of_threads, num_of_data_points, partition_name, class_name, attribute_path, since, until, max_ram_size);
      else
        {
          std::ostringstream text;
          text << "data type \"" << type_str << "\" is not supported";
          throw std::runtime_error(text.str().c_str());
        }

    }
  catch (const std::exception &ex)
    {
      std::cerr << "Caught exception: " << ex.what() << std::endl;
      return 1;
    }

  return 0;
}
