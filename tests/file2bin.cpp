#include <algorithm>
#include <cstdint>
#include <chrono>
#include <iostream>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <boost/program_options.hpp>

#include <errno.h>
#include <fcntl.h>
#include <string.h>
#include <unistd.h>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite.h>
#if GOOGLE_PROTOBUF_VERSION < 3007000
#include <google/protobuf/wire_format_lite_inl.h>
#endif

#include <pbeast/input-data-file.h>
#include <pbeast/series-print.h>
#include <pbeast/data-type.h>

namespace po = boost::program_options;

static uint64_t
size(uint64_t val)
{
  if (val < 0xff) return 1;
  else if (val < 0xffff)  return 2;
  else if (val < 0xffffff)  return 3;
  else if (val < 0xffffffff)  return 4;
  else if (val < 0xffffffffff)  return 5;
  else if (val < 0xffffffffffff)  return 6;
  else if (val < 0xffffffffffffff)  return 7;
  else return 8;
}

template<class T>
  void
  get_data(pbeast::InputDataFile &file, std::ofstream &ofs1, google::protobuf::io::CodedOutputStream * ofs2, google::protobuf::io::CodedOutputStream * ofs3, google::protobuf::io::CodedOutputStream * ofs4)
  {
    std::string name;

    uint64_t total = 0;

    std::vector<pbeast::SeriesData<T> > data;
    uint64_t prev = 0;
    uint64_t delta = 0;

    uint64_t diff_total(0), xor1_total(0), xor2_total(0);

    while (file.read_next(name, data))
      {
        total += data.size();
        for (const auto &x : data)
          {
            const T &val = x.get_value();
            std::cout << std::setfill(' ') << std::setw(16); pbeast::print(std::cout, val);
            ofs1.write(reinterpret_cast<const char*>(&val), sizeof(T));

            const uint64_t v64 = *(reinterpret_cast<const uint64_t*>(&val));

            uint64_t dt = delta;

            uint64_t diff = static_cast<uint64_t>(v64 - prev);
            uint64_t xor1 = static_cast<uint64_t>(v64 ^ prev);
            uint64_t pred = static_cast<uint64_t>(prev + delta);
            uint64_t dif2 = static_cast<uint64_t>(pred - v64);
            uint64_t spn2 = static_cast<uint64_t>(~dif2);
            uint64_t xor2 = static_cast<uint64_t>(dt ^ diff);
            uint64_t spin = static_cast<uint64_t>(~xor2);

            google::protobuf::internal::WireFormatLite::WriteUInt64NoTag(diff, ofs2);
            google::protobuf::internal::WireFormatLite::WriteUInt64NoTag(xor1, ofs3);
            google::protobuf::internal::WireFormatLite::WriteUInt64NoTag(xor2, ofs4);

            diff_total += size(diff);
            xor1_total += size(xor1);
            xor2_total += size(xor2);

            if (( prev ^ xor1) != v64) { std::cout << "error "; exit(1); }

            std::cout << " => " << std::hex << std::setfill(' ') << std::setw(16) << v64 << " diff: " << std::setw(16) << diff << ", xor1: " << std::setw(16) << xor1 << ", delta: " << std::setw(16) << delta << ", predicted: " << std::setw(16) << pred << ", diff-predicted: " << std::setw(16) << dif2 << ", spin-diff-predicted: " << std::setw(16) << spn2 << ", xor2: "  << std::setw(16) << xor2 << ", xor2-spin: " << std::setw(16) << spin << std::oct << std::endl;
            delta = v64 - prev;
            prev = v64;
          }

        data.clear();
      }

    std::cout << "wrote " << total << " items\n";

    std::cout << "uncompressed: " << total * sizeof(T) << std::endl;
    std::cout << "diff total: " << diff_total << " (" << ((float)(total * sizeof(T) - diff_total) / (float)(total * sizeof(T))) << ')' << std::endl;
    std::cout << "xor1 total: " << xor1_total << " (" << ((float)(total * sizeof(T) - xor1_total) / (float)(total * sizeof(T))) << ')' << std::endl;
    std::cout << "xor2 total: " << xor2_total << " (" << ((float)(total * sizeof(T) - xor2_total) / (float)(total * sizeof(T))) << ')' << std::endl;
  }


int main(int argc, char* argv[]) {

  std::string data_file, out_file;
  std::string series_name;
  bool list_series(false);
  bool print_series(false);
  bool print_info(false);
  bool print_merge_memory_info(false);

  po::options_description desc("Save simple data into binary file.");

  try
    {
      desc.add_options()
          ("data-file,f", po::value<std::string>(&data_file)->required(), "Name of data file")
          ("out-data-file,o", po::value<std::string>(&out_file)->required(), "Name of binary data output file")
          ("help,h", "Print help message");

      const po::positional_options_description p; // empty positional options
      po::variables_map vm;
      po::store(po::command_line_parser(argc, argv).options(desc).positional(p).run(), vm);

      if (vm.count("help"))
        {
          std::cout << desc << std::endl;
          return 0;
        }

      po::notify(vm);
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      std::cerr << "ERROR: " << ex.what() << std::endl;
      return 1;
    }

  try
    {
      auto t1 = std::chrono::steady_clock::now();

      std::string bin_name = out_file + ".bin";
      std::string diff_name = out_file + ".diff";
      std::string delta_name = out_file + ".delta";
      std::string dd_name = out_file + ".dd";

      std::ofstream ofs1(bin_name , std::ios::binary );

      int ofs2_fd = open(diff_name.c_str(), O_CREAT | O_WRONLY | O_TRUNC, 0664);
      int ofs3_fd = open(delta_name.c_str(), O_CREAT | O_WRONLY | O_TRUNC, 0664);
      int ofs4_fd = open(dd_name.c_str(), O_CREAT | O_WRONLY | O_TRUNC, 0664);

      google::protobuf::io::ZeroCopyOutputStream * ofs_ros2 = new google::protobuf::io::FileOutputStream(ofs2_fd);
      google::protobuf::io::ZeroCopyOutputStream * ofs_ros3 = new google::protobuf::io::FileOutputStream(ofs3_fd);
      google::protobuf::io::ZeroCopyOutputStream * ofs_ros4 = new google::protobuf::io::FileOutputStream(ofs4_fd);

      google::protobuf::io::CodedOutputStream * ofs2 = new google::protobuf::io::CodedOutputStream(ofs_ros2);
      google::protobuf::io::CodedOutputStream * ofs3 = new google::protobuf::io::CodedOutputStream(ofs_ros3);
      google::protobuf::io::CodedOutputStream * ofs4 = new google::protobuf::io::CodedOutputStream(ofs_ros4);

      if (!ofs1 || !ofs2 || !ofs3 || !ofs4)
        {
          std::cerr << "Cannot open file " << out_file << " for writing" << std::endl;
          return 1;
        }

      pbeast::InputDataFile file(data_file);

      if (file.is_downsampled())
        {
          std::cout << "downsampled files are not supported\n";
          return -1;
        }

      if (file.get_is_array())
        {
          std::cout << "array data are not supported\n";
          return -1;
        }

      if (file.get_data_type() == pbeast::BOOL)
        {
          get_data<bool>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::S8)
        {
          get_data<int8_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::U8)
        {
          get_data<uint8_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::S16)
        {
          get_data<int16_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::U16)
        {
          get_data<uint16_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::S32)
        {
          get_data<int32_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::U32)
        {
          get_data<uint32_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::S64)
        {
          get_data<int64_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::U64)
        {
          get_data<uint64_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::FLOAT)
        {
          get_data<float>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::DOUBLE)
        {
          get_data<double>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::ENUM)
        {
          get_data<int32_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::DATE)
        {
          get_data<uint32_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
      else if (file.get_data_type() == pbeast::TIME)
        {
          get_data<uint64_t>(file, ofs1, ofs2, ofs3, ofs4);
        }
//      else if (file.get_data_type() == pbeast::STRING)
//        {
//          get_data<std::string>(file, ofs1, ofs2, ofs3, ofs4);
//        }
//      else if (file.get_data_type() == pbeast::VOID)
//        {
//          get_data<pbeast::Void>(file, ofs1, ofs2, ofs3, ofs4);
//        }

      ofs1.close();

      delete ofs2;
      delete ofs3;
      delete ofs4;

      delete ofs_ros2;
      delete ofs_ros3;
      delete ofs_ros4;

      close(ofs2_fd);
      close(ofs3_fd);
      close(ofs4_fd);

      std::cout << " * process " << file.get_total_data_size() << " data items in  " << std::chrono::duration_cast<std::chrono::microseconds>(std::chrono::steady_clock::now()-t1).count() / 1000. << " ms" << std::endl;

      return 0;
    }
  catch (std::exception & ex)
    {
      std::cerr << "caught exception: " << ex.what() << std::endl;
      return -1;
    }

  return 0;
}
