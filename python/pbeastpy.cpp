/**
 * @file python/pbeastpy.cpp
 * @brief Boost.Python interface to the "pbeast" namespace
 */

#include <mutex>
#include <set>

#include <boost/python.hpp>
#include <boost/python/suite/indexing/vector_indexing_suite.hpp>

#include "pbeast/def-intervals.h"
#include "pbeast/functions.h"
#include "pbeast/query.h"

using namespace boost::python;

static void
init_default_intervals(uint64_t& since, uint64_t& until)
{
  if (since == pbeast::min_ts_const)
    until = pbeast::get_def_since();

  if (until == pbeast::max_ts_const)
    until = pbeast::get_def_until();
}

static std::string
make_ers_message(ers::Issue const &e)
{
  const ers::Context& curr = e.context();
  std::ostringstream oss;
  oss << "[" << curr.package_name() << "]" 
      << curr.function_name() << "@" << curr.file_name() << "+"
      << curr.line_number() << ": " << e.message();
  if (e.cause()) oss << " caused by " << make_ers_message(*e.cause());
  return oss.str();
}

static void
translate_ers_issue(ers::Issue const &e)
{
  PyErr_SetString(PyExc_RuntimeError, make_ers_message(e).c_str());
}

static boost::python::list
to_list(const std::vector<std::string>& vec)
{
  boost::python::list l;
  for (unsigned i = 0; i < vec.size(); ++i)
    l.append(vec[i]);
  return l;
}

template <typename T>
static boost::python::dict
eov_to_dict(const std::map<std::string, std::set<T>> &map)
{
  boost::python::dict d;

  for (const auto &x : map)
    {
      boost::python::list l;

      for (const auto &v : x.second)
        l.append(v);

      d[x.first] = l;
    }

  return d;
}


// Server Proxy

static boost::python::list
get_partitions(pbeast::ServerProxy& srv)
{
  return to_list(srv.get_partitions());
}

static boost::python::list
get_classes(pbeast::ServerProxy& srv, const std::string& partition_name)
{
  return to_list(srv.get_classes(partition_name));
}

static boost::python::list
get_attributes(pbeast::ServerProxy& srv, const std::string& partition_name, const std::string& class_name)
{
  return to_list(srv.get_attributes(partition_name, class_name));
}

static boost::python::list
get_objects(pbeast::ServerProxy& srv, const std::string& partition_name, const std::string& class_name, const std::string& attribute_path, const std::string& object_mask = "", uint64_t since = pbeast::min_ts_const, uint64_t until = pbeast::max_ts_const)
{
  init_default_intervals(since, until);

  return to_list(srv.get_objects(partition_name, class_name, attribute_path, object_mask, since, until));
}

static bool named_tuples_inited(false);
static boost::python::api::object PythonDataPoint;
static boost::python::api::object PythonDownsampledDataPoint;
static boost::python::api::object PythonQueryData;

static void
init_named_types()
{
  static std::mutex s_mutex;
  std::lock_guard<std::mutex> scoped_lock(s_mutex);

  if (named_tuples_inited == false)
    {
      auto collections = boost::python::import("collections");
      boost::python::api::object_attribute namedtuple = collections.attr("namedtuple");

      boost::python::list data_point_fields;
      data_point_fields.append("ts");
      data_point_fields.append("value");
      PythonDataPoint = namedtuple("DataPoint", data_point_fields);

      boost::python::list downsampled_data_point_fields;
      downsampled_data_point_fields.append("ts");
      downsampled_data_point_fields.append("min_value");
      downsampled_data_point_fields.append("value");
      downsampled_data_point_fields.append("max_value");
      PythonDownsampledDataPoint = namedtuple("DownsampledDataPoint", downsampled_data_point_fields);

      boost::python::list data_query_fields;
      data_query_fields.append("since");
      data_query_fields.append("until");
      data_query_fields.append("type");
      data_query_fields.append("is_array");
      data_query_fields.append("data");
      PythonQueryData = namedtuple("QueryData", data_query_fields);

      named_tuples_inited = true;
    }
}


template<class T>
  boost::python::list
  vector2list(const std::vector<T>& vec)
  {
    boost::python::list list;
    for (const auto& v : vec)
      list.append(v);
    return list;
  }

template<class T>
  boost::python::list
  convert_data_points(const std::vector<pbeast::DataPoint<T> >& data, const pbeast::AggregationType /* at */)
  {
    boost::python::list list;

    for (const auto& x : data)
      {
        if (!x.is_null())
          list.append(PythonDataPoint(x.ts(), x.value()));
        else
          list.append(PythonDataPoint(x.ts(), boost::python::object()));
      }

    return list;
  }

template<class T>
  boost::python::list
  convert_data_points(const std::vector<pbeast::DataPoint<std::vector<T>> >& data, const pbeast::AggregationType /* at */)
  {
    boost::python::list list;

    for (const auto& x : data)
      {
        if(!x.is_null())
          list.append(PythonDataPoint(x.ts(), vector2list(x.value())));
        else
          list.append(PythonDataPoint(x.ts(), boost::python::object()));
      }

    return list;
  }

template<class T>
  boost::python::list
  convert_data_points(const std::vector<pbeast::DownsampledDataPoint<T> >& data, const pbeast::AggregationType at)
  {
    boost::python::list list;

    for (const auto& x : data)
      {
        if (!x.is_null())
          {
            if (at == pbeast::AggregationNone)
              list.append(PythonDownsampledDataPoint(x.ts(), x.min_value(), x.value(), x.max_value()));
            else if (at == pbeast::AggregationAverage || at == pbeast::AggregationSum)
              list.append(PythonDataPoint(x.ts(), x.value()));
            else
              list.append(PythonDataPoint(x.ts(), x.min_value()));
          }
        else
          list.append(PythonDownsampledDataPoint(x.ts(), boost::python::object(), boost::python::object(), boost::python::object()));
      }

    return list;
  }

template<class T>
  boost::python::list
  convert_data_points(const std::vector<pbeast::DownsampledDataPoint<std::vector<T>> >& data, const pbeast::AggregationType at)
  {
    boost::python::list list;

    for (const auto& x : data)
      {
        if (!x.is_null())
          {
            if (at == pbeast::AggregationNone)
              list.append(PythonDownsampledDataPoint(x.ts(), vector2list(x.min_value()), vector2list(x.value()), vector2list(x.max_value())));
            else if (at == pbeast::AggregationAverage || at == pbeast::AggregationSum)
              list.append(PythonDataPoint(x.ts(), vector2list(x.value())));
            else
              list.append(PythonDataPoint(x.ts(), vector2list(x.min_value())));
          }
        else
          list.append(PythonDownsampledDataPoint(x.ts(), boost::python::object(), boost::python::object(), boost::python::object()));
      }

    return list;
  }


template<class T>
  void
  convert_series(const std::map<std::string, std::vector<T>> &data, const pbeast::FunctionConfig &fc, boost::python::dict &to)
  {
    for (const auto &x : data)
      to[x.first] = convert_data_points(x.second, (fc.no_aggregations() ? pbeast::AggregationNone : fc.get_type(x.first)));
  }

template<class T>
  static void
  to_python(pbeast::QueryDataBase * db, bool downsampled, const pbeast::FunctionConfig& fc, boost::python::dict& to)
  {
    if (downsampled == false)
      {
        if (db->is_array() == false)
          convert_series(static_cast<pbeast::QueryData<pbeast::DataPoint<T>>*>(db)->get_data(), fc, to);
        else
          convert_series(static_cast<pbeast::QueryData<pbeast::DataPoint<std::vector<T>>>*>(db)->get_data(), fc, to);
      }
    else
      {
        if (db->is_array() == false)
          convert_series(static_cast<pbeast::QueryData<pbeast::DownsampledDataPoint<T>>*>(db)->get_data(), fc, to);
        else
          convert_series(static_cast<pbeast::QueryData<pbeast::DownsampledDataPoint<std::vector<T>>>*>(db)->get_data(), fc, to);
      }
  }


static boost::python::list
get_data(pbeast::ServerProxy& srv, const std::string& partition_name, const std::string& class_name, const std::string& attribute_path, const std::string& object_mask = "", bool is_regexp = true, uint64_t since = pbeast::min_ts_const, uint64_t until = pbeast::max_ts_const, uint32_t sample_interval = 0, bool all_updates = false, const std::string& function_config = "", bool use_fqn = false, uint64_t prev_interval = pbeast::def_lookup_prev_const, uint64_t next_interval = pbeast::def_lookup_next_const, pbeast::FillGaps::Type fill_gaps = pbeast::FillGaps::Near)
{
  init_named_types();
  init_default_intervals(since, until);

  pbeast::FunctionConfig fc(function_config, sample_interval);

  std::vector<std::shared_ptr<pbeast::QueryDataBase>> result = srv.get_data(partition_name, class_name, attribute_path, object_mask, is_regexp, since, until, prev_interval, next_interval, all_updates, sample_interval, fill_gaps, fc, pbeast::ServerProxy::ZipCatalog | pbeast::ServerProxy::ZipData, use_fqn);

  boost::python::list out;

  for (const auto& x : result)
    {
      boost::python::dict data;

      switch (x->type())
        {
          case pbeast::BOOL:
            to_python<bool>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::S8:
            to_python<int8_t>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::U8:
            to_python<uint8_t>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::S16:
            to_python<int16_t>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::U16:
            to_python<uint16_t>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::S32:
          case pbeast::ENUM:
            to_python<int32_t>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::U32:
          case pbeast::DATE:
            to_python<uint32_t>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::S64:
            to_python<int64_t>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::U64:
          case pbeast::TIME:
            to_python<uint64_t>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::FLOAT:
            to_python<float>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::DOUBLE:
            to_python<double>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::STRING:
            to_python<std::string>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::VOID:
            to_python<pbeast::Void>(x.get(), sample_interval, fc, data);
            break;
          case pbeast::OBJECT:
            {
              throw std::runtime_error("cannot convert object of nested type");
            }
          default:
            {
              std::ostringstream text;
              text << "data type " << x->type() << " is not supported";
              throw std::runtime_error(text.str().c_str());
            }
        }

      out.append(PythonQueryData(x->since(), x->until(), x->type(), x->is_array(), data));
    }

  return out;
}


BOOST_PYTHON_FUNCTION_OVERLOADS(get_objects_overloads, get_objects, 4, 7)
BOOST_PYTHON_FUNCTION_OVERLOADS(get_data_overloads, get_data, 4, 15)


BOOST_PYTHON_MODULE(libpbeastpy)
{
  register_exception_translator<daq::pbeast::Exception>(&translate_ers_issue);

  enum_<pbeast::ServerProxy::CookieSetup>("CookieSetup")
      .value("NoCookie", pbeast::ServerProxy::None)
      .value("AutoUpdateKerberos", pbeast::ServerProxy::AutoUpdateKerberos)
      /*.value("AutoUpdateUserCertificates", pbeast::ServerProxy::AutoUpdateUserCertificates)*/
      ;

  enum_<pbeast::DataType>("DataType")
      .value("BOOL", pbeast::BOOL)
      .value("S8", pbeast::S8)
      .value("U8", pbeast::U8)
      .value("S16", pbeast::S16)
      .value("U16", pbeast::U16)
      .value("S32", pbeast::S32)
      .value("U32", pbeast::U32)
      .value("S64", pbeast::S64)
      .value("U64", pbeast::U64)
      .value("FLOAT", pbeast::FLOAT)
      .value("DOUBLE", pbeast::DOUBLE)
      .value("ENUM", pbeast::ENUM)
      .value("DATE", pbeast::DATE)
      .value("TIME", pbeast::TIME)
      .value("STRING", pbeast::STRING)
      .value("OBJECT", pbeast::OBJECT)
      .value("VOID", pbeast::VOID)
      ;

  enum_<pbeast::FillGaps::Type>("FillGaps")
      .value("NoFill", pbeast::FillGaps::None)
      .value("Near", pbeast::FillGaps::Near)
      .value("All", pbeast::FillGaps::All)
      ;


  const char * server_proxy_class_description =
      "The server proxy object for remote access to P-BEAST data.\n\n"
      "Data can be accessed on GPN (use \"https://atlasop.cern.ch\" base URL) and on ATCN (use \"http://pc-tdq-bst-12.cern.ch:8080\" base URL).\n"
      "The URL can be defined using TDAQ_PBEAST_SERVER_URL process environment variable.\n\n"
      "To access data from GPN one has to use CERN SSO cookie. There are two options:\n"
      " * Kerberos based authentication (a valid CERN Kerberos ticket must be acquired by user process)\n"
      " * User Certificates based authentication (\"~/private\" directory contains myCert.pem and myCert.key files) ! note: not supported with CentOS7 CERN SSO\n"
      "The SSO cookie setup can be defined using PBEAST_SERVER_SSO_SETUP_TYPE process environment variable or using arg3.\n"
      "Set this variable to case insensitive autoupdatekerberos or autoupdateusercertificates values.\n\n"
      "The directory for user certificates can be defined using PBEAST_SERVER_SSO_USER_CERT_DIR process environment variable.\n"
      "At the same time the names of the files cannot be changed.\n\n"
      "For more information about CERN SSO cookie see http://linux.web.cern.ch/linux/docs/cernssocookie.shtml\n\n"
      "Arguments:\n"
      " * arg2 - specifies base P-BEAST server URL;\n"
      " * arg3 - specifies cookie-setup-type (only if CERN SSO is needed);\n"
      " * arg4 - path to redefine default directory storing user files certificate files (optional).";

  const char * server_proxy_get_objects_description =
      "Get names of objects updated for given attribute (in user-defined time interval).\n\n"
      "Arguments:\n"
      " * partition     - partition name;\n"
      " * class         - class name;\n"
      " * attribute     - attribute name or attribute path for nested types;\n"
      " * objects_mask  - optional regular expression on object names, return all if empty (default value is empty);\n"
      " * since         - optional since timestamp in microseconds since Epoch (default value is 0);\n"
      " * until         - optional until timestamp in microseconds since Epoch (default value is maximum value).";

  std::string server_proxy_get_data_description = std::string(
      "Get raw or downsampled time series data.\n\n"
      "Since the types of the returned data are not known in advance,\n"
      "the method returns sequence of QueryData named tuples of the following structure:\n"
      " * \"since\" - timestamp in microseconds since Epoch of start of validity of given type\n"
      " * \"until\" - timestamp in microseconds since Epoch of end of validity of given type\n"
      " * \"type\" - P-BEAST data type (see DataType enum)\n"
      " * \"is_array\" - the data are values of arrays of values of above type\n"
      " * \"data\" - map of data with object name as a key and a sequence of DataPoint (raw data) or DownsampledDataPoint (sampled data).\n\n"
      "The DataPoint has \"since\", \"until\" and \"value\" attributes. The value can be of different types (number, string, sequence).\n"
      "The DownsampledDataPoint has the same attributes, and, in addition, \"min_value\" and \"max_value\" attributes\n"
      "to provide information about minimal and maximal values inside given time interval.\n\n"
      "Arguments:\n"
      " * partition         - partition name;\n"
      " * class             - class name;\n"
      " * attribute         - attribute name or attribute path for nested types;\n"
      " * obj               - optional object name or regular expression on object names, see next argument for more details (default value is empty);\n"
      " * is_regexp         - optional is_regexp flag to switch between explicit object name and regular expression  (default value is true);\n"
      " * since             - optional since timestamp in microseconds since Epoch (default value is 0);\n"
      " * until             - optional until timestamp in microseconds since Epoch (default value is maximum value)\n"
      " * ds_interval       - optional data sample interval, if 0, return raw data, if not, downsample data by given time interval in seconds (default value is 0)\n"
      " * all_updates       - optional all_updates flag to hide intermediate IS updates not modifying attribute value (false) or to show all (true)\n"
      " * functions_config  - optional aggregation configuration describing which functions to use (semicolon separated string ") + pbeast::FunctionConfig::make_description_of_functions() + "; \"keep-data:true\" keeps original data if aggregation functions are used\n"
      " * use_fqn           - optional flag to use fully qualified names (put dot-separated partition, class and attribute names prefix for object names)\n"
      " * prev_interval     - optional value in microseconds defining the time lapse to lookup closest previous data point outside query time interval\n"
      " * next_interval     - optional value in microseconds defining the time lapse to lookup closest next data point outside query time interval"
      " * fill_ds_gaps      - optional parameter to define how to fill missing downsampled data points: None, Near or All (default value is Near)\n"
      ;

  class_<pbeast::ServerProxy>("ServerProxy", server_proxy_class_description, init<optional<const std::string&, pbeast::ServerProxy::CookieSetup, const std::filesystem::path&> >())
    .def("get_partitions", &get_partitions, "Get names of P-BEAST partitions.")
    .def("get_classes", &get_classes, (arg("partition")), "Get names of classes for given P-BEAST partition.")
    .def("get_attributes", &get_attributes, (arg("partition"),arg("class")), "Get names of attributes for given P-BEAST partition and class.")
    .def("get_objects", &get_objects, get_objects_overloads(server_proxy_get_objects_description, args("partition", "class", "attribute", "objects_mask", "since", "until")))
    .def("get_data", &get_data, get_data_overloads(server_proxy_get_data_description.c_str(), args("partition", "class", "attribute", "obj", "is_regexp", "since", "until", "ds_interval", "all_updates", "functions_config", "use_fqn", "prev_interval", "next_interval", "fill_ds_gaps")))
    ;
}
