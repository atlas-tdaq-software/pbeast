package pbeast.helper;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.URL;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;
import java.util.function.ToLongFunction;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;

import pbeast.DataPoint;
import pbeast.DownSampledDataPoint;
import pbeast.JQueryDataBase;
import pbeast.JServerProxy;
import pbeast.JServerProxy.DATA_COMPRESSION;
import pbeast.JServerProxy.DOWNSAMPLE_FILL_GAPS;
import pbeast.JServerProxy.FunctionDefinition;

/**
 * Class implementing a simple application able to query the P-Beast server and then dump the results on screen.
 * <p>
 * The application can be started using the <em>pbeast_read_j</em> script.
 */
public class ReadPBeast {
    private final static Object REF = new Object();
    private final static Object NULL = new Object() {
        @Override
        public String toString() {
            return "(NULL)";
        }
    };

    private final static long defPrevDefValue = TimeUnit.SECONDS.toMicros(60);
    private final static long defNextDefValue = TimeUnit.SECONDS.toMicros(60);

    private final static DateTimeFormatter DATE_PRINT_FORMATTER = DateTimeFormatter.ofPattern("uuuu-MMM-dd HH:mm:ss[.SSSSSS]");
    private final static DateTimeFormatter DATE_PARSER_FORMATTER = DateTimeFormatter.ofPattern("uuuu-MM-dd HH:mm:ss[.SSS][ VV]");
    private final static NumberFormat NUMBER_FORMAT = NumberFormat.getInstance();

    static {
        ReadPBeast.NUMBER_FORMAT.setMinimumFractionDigits(0);
        ReadPBeast.NUMBER_FORMAT.setMaximumFractionDigits(5);
        if(ReadPBeast.NUMBER_FORMAT instanceof DecimalFormat) {
            final DecimalFormat df = (DecimalFormat) ReadPBeast.NUMBER_FORMAT;
            df.setGroupingUsed(false);
        }
    }

    private static String printTimeStamp(final long ts, final boolean rawTimeStamp) {
        if(rawTimeStamp == true) {
            return Long.toString(ts);
        }

        final LocalDateTime s = LocalDateTime.ofEpochSecond(ts / 1000000, ((int) (ts % 1000000) * 1000), ZoneOffset.UTC);
        return s.format(ReadPBeast.DATE_PRINT_FORMATTER);
    }

    private static String printDataPoint(final Object dp) {
        final StringBuilder str = new StringBuilder();

        if(dp instanceof Number) {
            str.append(ReadPBeast.NUMBER_FORMAT.format(dp));
        } else if(dp instanceof List<?>) {
            final List<String> stringList = new ArrayList<>();

            final List<?> l = (List<?>) dp;
            str.append(l.size());
            str.append(" items [");

            for(final Object i : l) {
                if(i instanceof Number) {
                    stringList.add(ReadPBeast.NUMBER_FORMAT.format(i));
                } else {
                    stringList.add(i.toString());
                }
            }

            str.append(String.join(", ", stringList));
            str.append("]");
        } else {
            str.append(dp);
        }

        return str.toString();
    }

    private static void printDownSampled(final Map<String, List<DownSampledDataPoint<Object, Object>>> data,
                                         final boolean rawTimeStamp,
                                         final FunctionDefinition fd)
    {
        for(final Entry<String, List<DownSampledDataPoint<Object, Object>>> entry : data.entrySet()) {
            final String dataName = entry.getKey();
            System.out.println(dataName + ":");

            final List<DownSampledDataPoint<Object, Object>> dataList = entry.getValue();
            final int dataListSize = dataList.size();
            int idx = 0;
            for(idx = 0; idx < dataListSize; ++idx) {
                DownSampledDataPoint<Object, Object> dp = dataList.get(idx);
                long thisTS = dp.getTimeStamp().longValueExact();

                final Object mean = dp.getValue();
                if(mean != null) {
                    final Object min = dp.minValue();
                    final Object max = dp.maxValue();

                    switch(fd.getType(dataName)) {
                        case AVERAGE:
                        case SUM:
                            System.out.println("    [" + ReadPBeast.printTimeStamp(thisTS, rawTimeStamp) + "] "
                                               + ReadPBeast.printDataPoint(mean));
                            break;
                        case MAX:
                        case MIN:
                            System.out.println("    [" + ReadPBeast.printTimeStamp(thisTS, rawTimeStamp) + "] "
                                               + ReadPBeast.printDataPoint(min));
                            break;
                        case NONE:
                        default:
                            System.out.println("    [" + ReadPBeast.printTimeStamp(thisTS, rawTimeStamp) + "]" + " ("
                                               + ReadPBeast.printDataPoint(min) + "," + ReadPBeast.printDataPoint(mean) + ","
                                               + ReadPBeast.printDataPoint(max) + ")");
                            break;

                    }
                } else {
                    System.out.println("    [" + ReadPBeast.printTimeStamp(thisTS, rawTimeStamp) + "]" + " (NULL)");
                }
            }
        }
    }

    private static void print(final Map<String, List<DataPoint<Object>>> data, final boolean rawTimeStamp, final boolean printAllUpdates) {
        for(final Entry<String, List<DataPoint<Object>>> entry : data.entrySet()) {
            System.out.println(entry.getKey() + ":");

            Object last = ReadPBeast.REF;
            long start = -1;
            long end = -1;
            int idx = 0;
            final List<DataPoint<Object>> dataList = entry.getValue();
            final int size = dataList.size();
            for(idx = 0; idx < size; ++idx) {
                final DataPoint<Object> dp = dataList.get(idx);
                final Object value = dp.getValue();
                final long ts = dp.getTimeStamp().longValueExact();

                if(printAllUpdates == false) {
                    if(last.equals(ReadPBeast.REF)) {
                        start = ts;
                        last = value == null ? ReadPBeast.NULL : value;
                    } else if(last.equals(value)) {
                        end = ts;
                    }

                    if((last.equals(value) == false) || (idx == (size - 1))) {
                        final StringBuilder separator = new StringBuilder(" ");
                        if(last instanceof List<?>) {
                            final List<?> l = (List<?>) last;
                            separator.append(l.size());
                            separator.append(" items ");
                        }

                        if(end != -1) {
                            System.out.println("    [" + ReadPBeast.printTimeStamp(start, rawTimeStamp) + " - "
                                               + ReadPBeast.printTimeStamp(end, rawTimeStamp) + "] " + ReadPBeast.printDataPoint(last));
                        } else {
                            System.out.println("    [" + ReadPBeast.printTimeStamp(start, rawTimeStamp) + "] "
                                               + ReadPBeast.printDataPoint(last));
                        }

                        start = ts;
                        end = -1;
                        last = value == null ? ReadPBeast.NULL : value;
                    }
                } else {
                    System.out.println("    [" + ReadPBeast.printTimeStamp(ts, rawTimeStamp) + "] "
                                       + ((value != null) ? ReadPBeast.printDataPoint(value) : ReadPBeast.NULL.toString()));
                }
            }
        }
    }

    /**
     * The main function.
     * 
     * @param args Command line arguments
     */
    public static void main(final String[] args) {
        // Note: the handling of the command line options is taken from the C++ implementation for uniformity

        final Options opts = new Options();
        opts.addOption("n", "base-url", true, "URL of the P-Beast server");
        opts.addOption("p", "partition-name", true, "Name of the partition");
        opts.addOption("c", "class-name", true, "Name of the class");
        opts.addOption("a", "attribute-name", true, "Name of the attribute or of the path in case of nested type");
        opts.addOption("o", "object-name", true, "Name of the object");
        opts.addOption("O", "object-name-regexp", true, "Regular expression matching object names");
        opts.addOption("F",
                       "use-fully-qualified-object-names",
                       false,
                       "Use fully qualified names for objects (i.e., include partition, class and attribute names in returned data)");
        opts.addOption("l", "list-updated-objects", false, "List names of updated objects");
        opts.addOption("L", "list-partitions", false, "List partitions");
        opts.addOption("C", "list-classes", false, "List classes");
        opts.addOption("A", "list-attributes", false, "List attributes");
        opts.addOption("s", "since", true, "Start of the query time interval");
        opts.addOption("S",
                       "since-raw-timestamp",
                       true,
                       "Start of the query time interval (raw format, number of microseconds since Epoch)");
        opts.addOption("t", "until", true, "End of the query time interval");
        opts.addOption("T", "until-raw-timestamp", true, "End of the query time interval (raw format, number of microseconds since Epoch)");
        opts.addOption("P",
                       "lookup-previous-value-time-lapse",
                       true,
                       "Time window (in microseconds) to look at before the start of the query time interval");
        opts.addOption("N",
                       "lookup-next-value-time-lapse",
                       true,
                       "Time window (in microseconds) to look at after the end of the query time interval");
        opts.addOption("i",
                       "downsample-interval",
                       true,
                       "Down-sampling interval in seconds (if not set or zero, then raw data are received)");
        opts.addOption("g",
                       "fill-gaps",
                       true,
                       "\"When down-sampling, approximate missing values: \"none\", \"near\" (default) or \"all\"");

        // Special handling for option with multiple values
        final Option funcOption = new Option("f",
                                             "functions",
                                             true,
                                             "Space-separated " + JServerProxy.FunctionDefinition.printFunctionDescription());
        funcOption.setArgs(Option.UNLIMITED_VALUES);
        opts.addOption(funcOption);

        opts.addOption("K", "keep-aggregated-data", false, "Keep original data after applying functions to data");
        opts.addOption("u", "print-value-per-update", false, "Print IS data for every update, even in case of unchanged data");
        opts.addOption("z", "time-zones", false, "List all the known time zones");
        opts.addOption("w", "print-raw-timestamp", false, "Print timestamps as number of micro-seconds since Epoch");
        opts.addOption("h", "help", false, "Print help message");

        // TODO: missing options
        // W - print raw date/time

        final CommandLineParser parser = new DefaultParser();
        try {
            final CommandLine cmdLine = parser.parse(opts, args, false);

            if(cmdLine.hasOption("h") == true) {
                final StringWriter helpMessage = new StringWriter();

                final HelpFormatter hf = new HelpFormatter();
                hf.printHelp(new PrintWriter(helpMessage, true),
                             80,
                             "this_app",
                             "\n\nData reader from P-Beast server using the Java API\n\n",
                             opts,
                             0,
                             0,
                             "\nIn order to get Point-1 data from GPN, install the \"cern-get-sso-cookie\" package and eventually create the needed user certificates. "
                                + "See http://linux.web.cern.ch/linux/docs/cernssocookie.shtml for more information. "
                                + "\n\nSet the PBEAST_SERVER_SSO_SETUP_TYPE process environment variable to case insensitive \"autoupdatekerberos\" "
                                + "or \"autoupdateusercertificates\", in order to use CERN SSO."
                                + "\n\nFor timestamps use ISO 8601 date-time format, i.e. uuuu-MM-dd HH:mm:ss[.SSS][ VV]."
                                + "\n\nFor listing all the available timezones, please execute with the -z option.",
                             true);

                System.out.println(helpMessage.toString());
            } else if(cmdLine.hasOption("z") == true) {
                System.out.println("Available time zones are");
                for(final String tz : ZoneId.getAvailableZoneIds()) {
                    System.out.println(tz);
                }
            } else {
                final String baseURL = cmdLine.getOptionValue("n");
                if((baseURL == null) || baseURL.isEmpty()) {
                    throw new IllegalArgumentException("The P-Beast server base URL must be defined");
                }

                final String partitionName = cmdLine.getOptionValue("p", "");
                final String className = cmdLine.getOptionValue("c", "");
                final String attributeName = cmdLine.getOptionValue("a", "");

                final boolean listPartitions = cmdLine.hasOption("L");
                if((listPartitions == false) && (partitionName.isEmpty() == true)) {
                    throw new IllegalArgumentException("The partition name must be defined");
                }

                final boolean listClasses = cmdLine.hasOption("C");
                if((listClasses == false) && (listPartitions == false) && (className.isEmpty() == true)) {
                    throw new IllegalArgumentException("The class name must be defined");
                }

                final boolean listAttributes = cmdLine.hasOption("A");
                if((listAttributes == false) && (listPartitions == false) && (listClasses == false) && (attributeName.isEmpty() == true)) {
                    throw new IllegalArgumentException("The attribute name must be defined");
                }

                String objectName = cmdLine.getOptionValue("o", "");
                final boolean isRegEx = cmdLine.hasOption("O");
                if(isRegEx == true) {
                    final String objectNameRegEx = cmdLine.getOptionValue("O");
                    if(objectNameRegEx.equals(".*") == false) {
                        objectName = objectNameRegEx;
                    }
                }

                final ToLongFunction<String> parseDateTimeString = (dateAsString) -> {
                    final long time;

                    final TemporalAccessor ta = ReadPBeast.DATE_PARSER_FORMATTER.parseBest(dateAsString,
                                                                                           ZonedDateTime::from,
                                                                                           LocalDateTime::from);
                    if(ta instanceof ZonedDateTime) {
                        final ZonedDateTime tmp = ((ZonedDateTime) ta);
                        time = (tmp.toEpochSecond() * 1000000) + tmp.getLong(ChronoField.MICRO_OF_SECOND);
                    } else {
                        final LocalDateTime tmp = ((LocalDateTime) ta);
                        time = (tmp.toEpochSecond(ZoneOffset.UTC) * 1000000) + tmp.getLong(ChronoField.MICRO_OF_SECOND);
                    }

                    return time;
                };

                long since = parseDateTimeString.applyAsLong(cmdLine.getOptionValue("s", "1970-01-01 00:00:01 UTC"));
                long until = parseDateTimeString.applyAsLong(cmdLine.getOptionValue("t", "2200-01-01 00:00:01 UTC"));

                if(cmdLine.hasOption("S") == true) {
                    final long sinceRaw = Long.parseLong(cmdLine.getOptionValue("S"));
                    since = sinceRaw;
                }

                if(cmdLine.hasOption("T") == true) {
                    final long untilRaw = Long.parseLong(cmdLine.getOptionValue("T"));
                    until = untilRaw;
                }

                final boolean useFQN = cmdLine.hasOption("F");
                final boolean listUpdatedObjects = cmdLine.hasOption("l");
                final boolean getAllUpdates = cmdLine.hasOption("u");
                final long lookupPreviousTime = Long.parseLong(cmdLine.getOptionValue("P", String.valueOf(ReadPBeast.defPrevDefValue)));
                final long lookupNextTime = Long.parseLong(cmdLine.getOptionValue("N", String.valueOf(ReadPBeast.defNextDefValue)));
                final long downSamplingInterval = Long.parseLong(cmdLine.getOptionValue("i", "0"));
                final DOWNSAMPLE_FILL_GAPS fillGaps = DOWNSAMPLE_FILL_GAPS.valueOf(cmdLine.getOptionValue("g", "near").toUpperCase());

                final FunctionDefinition funcDef;
                if(cmdLine.hasOption("f") == true) {
                    funcDef = new FunctionDefinition(Arrays.asList(cmdLine.getOptionValues("f")), downSamplingInterval);
                } else {
                    funcDef = new FunctionDefinition();
                }

                final boolean keepData = cmdLine.hasOption("K");
                if((keepData == true) && (funcDef.noAggregations() == true)) {
                    throw new IllegalArgumentException("Cannot set the \"keep-aggregated-data\" option without the definition of any \"aggregate\" function");
                }
                funcDef.setKeepData(keepData);

                final boolean printRawTimeStamp = cmdLine.hasOption("w");

                // End of parsing, the real work now
                final JServerProxy proxy = new JServerProxy(new URL(baseURL));

                if(listPartitions == true) {
                    final List<String> partitions = proxy.get_partitions();
                    for(final String p : partitions) {
                        System.out.println(p);
                    }
                } else if(listClasses == true) {
                    final List<String> classes = proxy.get_classes(partitionName);
                    for(final String c : classes) {
                        System.out.println(c);
                    }
                } else if(listAttributes == true) {
                    final List<String> attributes = proxy.get_attributes(partitionName, className);
                    for(final String a : attributes) {
                        System.out.println(a);
                    }
                } else if(listUpdatedObjects == true) {
                    final List<String> objects = proxy.get_objects(partitionName, className, attributeName, objectName, since, until);
                    for(final String o : objects) {
                        System.out.println(o);
                    }
                } else {
                    final List<JQueryDataBase> results = proxy.get_data(partitionName,
                                                                        className,
                                                                        attributeName,
                                                                        objectName,
                                                                        isRegEx,
                                                                        since,
                                                                        until,
                                                                        lookupPreviousTime,
                                                                        lookupNextTime,
                                                                        getAllUpdates,
                                                                        downSamplingInterval,
                                                                        funcDef,
                                                                        EnumSet.of(DATA_COMPRESSION.ZIP_CATALOG,
                                                                                   DATA_COMPRESSION.ZIP_DATA,
                                                                                   DATA_COMPRESSION.CATALOGIZE_STRINGS),
                                                                        fillGaps,
                                                                        useFQN);

                    for(final JQueryDataBase qdb : results) {
                        if(qdb.isDownSampled() == false) {
                            ReadPBeast.print(JQueryDataBase.getData(qdb), printRawTimeStamp, getAllUpdates);
                        } else {
                            ReadPBeast.printDownSampled(JQueryDataBase.getDownSampledData(qdb), printRawTimeStamp, funcDef);
                        }
                    }
                }
            }
        }
        catch(final java.lang.Exception ex) {
            System.err.println("\nSome error occurred: " + ex.getMessage());
            System.err.println("\nPrinting stack-trace:");
            ex.printStackTrace();

            System.exit(-1);
        }

        System.exit(0);
    }
}
