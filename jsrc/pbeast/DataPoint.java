package pbeast;

import java.math.BigInteger;


/**
 * This class represents a data-point returned by a P-Beast query.
 * <p>
 * The data-point consists of a time-stamp and a value.
 * <p>
 * NOTE: The type of the data-point value corresponds to the one returned by {@link JQueryDataBase#type()}. In case of vector data, the type
 * is {@code List<T>}.
 * 
 * @param <T> The type of the data-point
 * @see JQueryDataBase#type()
 * @see JQueryDataBase#getData(JQueryDataBase)
 */
public class DataPoint<T> {
    private final BigInteger ts;
    private final T value;

    /**
     * Constructor
     * 
     * @param ts Time stamp
     * @param value Value of the data-point
     */
    @SuppressWarnings("unchecked")
    DataPoint(final BigInteger ts, final Object value) {
        this.ts = ts;
        this.value = (T) value;
    }

    /**
     * It returns the data-point time stamp
     * 
     * @return The data-point time stamp as micro-seconds since Epoch
     */
    public BigInteger getTimeStamp() {
        return this.ts;
    }

    /**
     * It returns the data-point value
     * 
     * @return The data-point value
     */
    public T getValue() {
        return this.value;
    }

    /**
     * It returns <em>true</em> is the data point is <em>null</em>
     * 
     * @return <em>true</em> is the data point is <em>null</em>
     */
    public boolean isNull() {
        return(this.value == null);
    }
}
