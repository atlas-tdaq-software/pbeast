/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class String_To_VectorDataPoint_vfloat {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected String_To_VectorDataPoint_vfloat(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(String_To_VectorDataPoint_vfloat obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_String_To_VectorDataPoint_vfloat(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public String_To_VectorDataPoint_vfloat() {
    this(pbeast_ServerProxyJNI.new_String_To_VectorDataPoint_vfloat__SWIG_0(), true);
  }

  public String_To_VectorDataPoint_vfloat(String_To_VectorDataPoint_vfloat arg0) {
    this(pbeast_ServerProxyJNI.new_String_To_VectorDataPoint_vfloat__SWIG_1(String_To_VectorDataPoint_vfloat.getCPtr(arg0), arg0), true);
  }

  public long size() {
    return pbeast_ServerProxyJNI.String_To_VectorDataPoint_vfloat_size(swigCPtr, this);
  }

  public boolean empty() {
    return pbeast_ServerProxyJNI.String_To_VectorDataPoint_vfloat_empty(swigCPtr, this);
  }

  public void clear() {
    pbeast_ServerProxyJNI.String_To_VectorDataPoint_vfloat_clear(swigCPtr, this);
  }

  public Vector_Datapoint_vfloat get(String key) {
    return new Vector_Datapoint_vfloat(pbeast_ServerProxyJNI.String_To_VectorDataPoint_vfloat_get(swigCPtr, this, key), false);
  }

  public void set(String key, Vector_Datapoint_vfloat x) {
    pbeast_ServerProxyJNI.String_To_VectorDataPoint_vfloat_set(swigCPtr, this, key, Vector_Datapoint_vfloat.getCPtr(x), x);
  }

  public void del(String key) {
    pbeast_ServerProxyJNI.String_To_VectorDataPoint_vfloat_del(swigCPtr, this, key);
  }

  public boolean has_key(String key) {
    return pbeast_ServerProxyJNI.String_To_VectorDataPoint_vfloat_has_key(swigCPtr, this, key);
  }

}
