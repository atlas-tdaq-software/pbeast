/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class DataPointCore_string {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected DataPointCore_string(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(DataPointCore_string obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_DataPointCore_string(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public java.math.BigInteger ts() {
    return pbeast_ServerProxyJNI.DataPointCore_string_ts(swigCPtr, this);
  }

  public boolean is_null() {
    return pbeast_ServerProxyJNI.DataPointCore_string_is_null(swigCPtr, this);
  }

  public String value() {
    return pbeast_ServerProxyJNI.DataPointCore_string_value(swigCPtr, this);
  }

}
