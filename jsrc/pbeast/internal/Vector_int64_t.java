/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class Vector_int64_t extends java.util.AbstractList<Long> implements java.util.RandomAccess {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected Vector_int64_t(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Vector_int64_t obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_Vector_int64_t(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Vector_int64_t(long[] initialElements) {
    this();
    for (long element : initialElements) {
      add(element);
    }
  }

  public Vector_int64_t(Iterable<Long> initialElements) {
    this();
    for (long element : initialElements) {
      add(element);
    }
  }

  public Long get(int index) {
    return doGet(index);
  }

  public Long set(int index, Long e) {
    return doSet(index, e);
  }

  public boolean add(Long e) {
    modCount++;
    doAdd(e);
    return true;
  }

  public void add(int index, Long e) {
    modCount++;
    doAdd(index, e);
  }

  public Long remove(int index) {
    modCount++;
    return doRemove(index);
  }

  protected void removeRange(int fromIndex, int toIndex) {
    modCount++;
    doRemoveRange(fromIndex, toIndex);
  }

  public int size() {
    return doSize();
  }

  public Vector_int64_t() {
    this(pbeast_ServerProxyJNI.new_Vector_int64_t__SWIG_0(), true);
  }

  public Vector_int64_t(Vector_int64_t other) {
    this(pbeast_ServerProxyJNI.new_Vector_int64_t__SWIG_1(Vector_int64_t.getCPtr(other), other), true);
  }

  public java.math.BigInteger capacity() {
    return pbeast_ServerProxyJNI.Vector_int64_t_capacity(swigCPtr, this);
  }

  public void reserve(java.math.BigInteger n) {
    pbeast_ServerProxyJNI.Vector_int64_t_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return pbeast_ServerProxyJNI.Vector_int64_t_isEmpty(swigCPtr, this);
  }

  public void clear() {
    pbeast_ServerProxyJNI.Vector_int64_t_clear(swigCPtr, this);
  }

  public Vector_int64_t(int count) {
    this(pbeast_ServerProxyJNI.new_Vector_int64_t__SWIG_2(count), true);
  }

  public Vector_int64_t(int count, long value) {
    this(pbeast_ServerProxyJNI.new_Vector_int64_t__SWIG_3(count, value), true);
  }

  private int doSize() {
    return pbeast_ServerProxyJNI.Vector_int64_t_doSize(swigCPtr, this);
  }

  private void doAdd(long value) {
    pbeast_ServerProxyJNI.Vector_int64_t_doAdd__SWIG_0(swigCPtr, this, value);
  }

  private void doAdd(int index, long value) {
    pbeast_ServerProxyJNI.Vector_int64_t_doAdd__SWIG_1(swigCPtr, this, index, value);
  }

  private long doRemove(int index) {
    return pbeast_ServerProxyJNI.Vector_int64_t_doRemove(swigCPtr, this, index);
  }

  private long doGet(int index) {
    return pbeast_ServerProxyJNI.Vector_int64_t_doGet(swigCPtr, this, index);
  }

  private long doSet(int index, long value) {
    return pbeast_ServerProxyJNI.Vector_int64_t_doSet(swigCPtr, this, index, value);
  }

  private void doRemoveRange(int fromIndex, int toIndex) {
    pbeast_ServerProxyJNI.Vector_int64_t_doRemoveRange(swigCPtr, this, fromIndex, toIndex);
  }

}
