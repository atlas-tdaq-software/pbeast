/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class Vector_AverageDataPoint_float extends java.util.AbstractList<AverageDataPoint_vfloat> implements java.util.RandomAccess {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected Vector_AverageDataPoint_float(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Vector_AverageDataPoint_float obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_Vector_AverageDataPoint_float(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Vector_AverageDataPoint_float(AverageDataPoint_vfloat[] initialElements) {
    this();
    for (AverageDataPoint_vfloat element : initialElements) {
      add(element);
    }
  }

  public Vector_AverageDataPoint_float(Iterable<AverageDataPoint_vfloat> initialElements) {
    this();
    for (AverageDataPoint_vfloat element : initialElements) {
      add(element);
    }
  }

  public AverageDataPoint_vfloat get(int index) {
    return doGet(index);
  }

  public AverageDataPoint_vfloat set(int index, AverageDataPoint_vfloat e) {
    return doSet(index, e);
  }

  public boolean add(AverageDataPoint_vfloat e) {
    modCount++;
    doAdd(e);
    return true;
  }

  public void add(int index, AverageDataPoint_vfloat e) {
    modCount++;
    doAdd(index, e);
  }

  public AverageDataPoint_vfloat remove(int index) {
    modCount++;
    return doRemove(index);
  }

  protected void removeRange(int fromIndex, int toIndex) {
    modCount++;
    doRemoveRange(fromIndex, toIndex);
  }

  public int size() {
    return doSize();
  }

  public Vector_AverageDataPoint_float() {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_float__SWIG_0(), true);
  }

  public Vector_AverageDataPoint_float(Vector_AverageDataPoint_float other) {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_float__SWIG_1(Vector_AverageDataPoint_float.getCPtr(other), other), true);
  }

  public java.math.BigInteger capacity() {
    return pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_capacity(swigCPtr, this);
  }

  public void reserve(java.math.BigInteger n) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_isEmpty(swigCPtr, this);
  }

  public void clear() {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_clear(swigCPtr, this);
  }

  public Vector_AverageDataPoint_float(int count) {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_float__SWIG_2(count), true);
  }

  public Vector_AverageDataPoint_float(int count, AverageDataPoint_vfloat value) {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_float__SWIG_3(count, AverageDataPoint_vfloat.getCPtr(value), value), true);
  }

  private int doSize() {
    return pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_doSize(swigCPtr, this);
  }

  private void doAdd(AverageDataPoint_vfloat value) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_doAdd__SWIG_0(swigCPtr, this, AverageDataPoint_vfloat.getCPtr(value), value);
  }

  private void doAdd(int index, AverageDataPoint_vfloat value) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_doAdd__SWIG_1(swigCPtr, this, index, AverageDataPoint_vfloat.getCPtr(value), value);
  }

  private AverageDataPoint_vfloat doRemove(int index) {
    return new AverageDataPoint_vfloat(pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_doRemove(swigCPtr, this, index), true);
  }

  private AverageDataPoint_vfloat doGet(int index) {
    return new AverageDataPoint_vfloat(pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_doGet(swigCPtr, this, index), false);
  }

  private AverageDataPoint_vfloat doSet(int index, AverageDataPoint_vfloat value) {
    return new AverageDataPoint_vfloat(pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_doSet(swigCPtr, this, index, AverageDataPoint_vfloat.getCPtr(value), value), true);
  }

  private void doRemoveRange(int fromIndex, int toIndex) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_float_doRemoveRange(swigCPtr, this, fromIndex, toIndex);
  }

}
