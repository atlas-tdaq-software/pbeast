/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class CType_vint16_t {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected CType_vint16_t(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(CType_vint16_t obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_CType_vint16_t(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public static DataType type() {
    return DataType.swigToEnum(pbeast_ServerProxyJNI.CType_vint16_t_type());
  }

  public CType_vint16_t() {
    this(pbeast_ServerProxyJNI.new_CType_vint16_t(), true);
  }

}
