/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class Vector_Datapoint_float extends java.util.AbstractList<DataPoint_float> implements java.util.RandomAccess {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected Vector_Datapoint_float(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Vector_Datapoint_float obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_Vector_Datapoint_float(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Vector_Datapoint_float(DataPoint_float[] initialElements) {
    this();
    for (DataPoint_float element : initialElements) {
      add(element);
    }
  }

  public Vector_Datapoint_float(Iterable<DataPoint_float> initialElements) {
    this();
    for (DataPoint_float element : initialElements) {
      add(element);
    }
  }

  public DataPoint_float get(int index) {
    return doGet(index);
  }

  public DataPoint_float set(int index, DataPoint_float e) {
    return doSet(index, e);
  }

  public boolean add(DataPoint_float e) {
    modCount++;
    doAdd(e);
    return true;
  }

  public void add(int index, DataPoint_float e) {
    modCount++;
    doAdd(index, e);
  }

  public DataPoint_float remove(int index) {
    modCount++;
    return doRemove(index);
  }

  protected void removeRange(int fromIndex, int toIndex) {
    modCount++;
    doRemoveRange(fromIndex, toIndex);
  }

  public int size() {
    return doSize();
  }

  public Vector_Datapoint_float() {
    this(pbeast_ServerProxyJNI.new_Vector_Datapoint_float__SWIG_0(), true);
  }

  public Vector_Datapoint_float(Vector_Datapoint_float other) {
    this(pbeast_ServerProxyJNI.new_Vector_Datapoint_float__SWIG_1(Vector_Datapoint_float.getCPtr(other), other), true);
  }

  public java.math.BigInteger capacity() {
    return pbeast_ServerProxyJNI.Vector_Datapoint_float_capacity(swigCPtr, this);
  }

  public void reserve(java.math.BigInteger n) {
    pbeast_ServerProxyJNI.Vector_Datapoint_float_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return pbeast_ServerProxyJNI.Vector_Datapoint_float_isEmpty(swigCPtr, this);
  }

  public void clear() {
    pbeast_ServerProxyJNI.Vector_Datapoint_float_clear(swigCPtr, this);
  }

  public Vector_Datapoint_float(int count) {
    this(pbeast_ServerProxyJNI.new_Vector_Datapoint_float__SWIG_2(count), true);
  }

  public Vector_Datapoint_float(int count, DataPoint_float value) {
    this(pbeast_ServerProxyJNI.new_Vector_Datapoint_float__SWIG_3(count, DataPoint_float.getCPtr(value), value), true);
  }

  private int doSize() {
    return pbeast_ServerProxyJNI.Vector_Datapoint_float_doSize(swigCPtr, this);
  }

  private void doAdd(DataPoint_float value) {
    pbeast_ServerProxyJNI.Vector_Datapoint_float_doAdd__SWIG_0(swigCPtr, this, DataPoint_float.getCPtr(value), value);
  }

  private void doAdd(int index, DataPoint_float value) {
    pbeast_ServerProxyJNI.Vector_Datapoint_float_doAdd__SWIG_1(swigCPtr, this, index, DataPoint_float.getCPtr(value), value);
  }

  private DataPoint_float doRemove(int index) {
    return new DataPoint_float(pbeast_ServerProxyJNI.Vector_Datapoint_float_doRemove(swigCPtr, this, index), true);
  }

  private DataPoint_float doGet(int index) {
    return new DataPoint_float(pbeast_ServerProxyJNI.Vector_Datapoint_float_doGet(swigCPtr, this, index), false);
  }

  private DataPoint_float doSet(int index, DataPoint_float value) {
    return new DataPoint_float(pbeast_ServerProxyJNI.Vector_Datapoint_float_doSet(swigCPtr, this, index, DataPoint_float.getCPtr(value), value), true);
  }

  private void doRemoveRange(int fromIndex, int toIndex) {
    pbeast_ServerProxyJNI.Vector_Datapoint_float_doRemoveRange(swigCPtr, this, fromIndex, toIndex);
  }

}
