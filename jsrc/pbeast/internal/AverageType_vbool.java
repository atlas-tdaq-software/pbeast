/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class AverageType_vbool {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected AverageType_vbool(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(AverageType_vbool obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_AverageType_vbool(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public AverageType_vbool() {
    this(pbeast_ServerProxyJNI.new_AverageType_vbool(), true);
  }

}
