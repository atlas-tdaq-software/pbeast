/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class Vector_Datapoint_vint64_t extends java.util.AbstractList<DataPoint_vint64_t> implements java.util.RandomAccess {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected Vector_Datapoint_vint64_t(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Vector_Datapoint_vint64_t obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_Vector_Datapoint_vint64_t(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Vector_Datapoint_vint64_t(DataPoint_vint64_t[] initialElements) {
    this();
    for (DataPoint_vint64_t element : initialElements) {
      add(element);
    }
  }

  public Vector_Datapoint_vint64_t(Iterable<DataPoint_vint64_t> initialElements) {
    this();
    for (DataPoint_vint64_t element : initialElements) {
      add(element);
    }
  }

  public DataPoint_vint64_t get(int index) {
    return doGet(index);
  }

  public DataPoint_vint64_t set(int index, DataPoint_vint64_t e) {
    return doSet(index, e);
  }

  public boolean add(DataPoint_vint64_t e) {
    modCount++;
    doAdd(e);
    return true;
  }

  public void add(int index, DataPoint_vint64_t e) {
    modCount++;
    doAdd(index, e);
  }

  public DataPoint_vint64_t remove(int index) {
    modCount++;
    return doRemove(index);
  }

  protected void removeRange(int fromIndex, int toIndex) {
    modCount++;
    doRemoveRange(fromIndex, toIndex);
  }

  public int size() {
    return doSize();
  }

  public Vector_Datapoint_vint64_t() {
    this(pbeast_ServerProxyJNI.new_Vector_Datapoint_vint64_t__SWIG_0(), true);
  }

  public Vector_Datapoint_vint64_t(Vector_Datapoint_vint64_t other) {
    this(pbeast_ServerProxyJNI.new_Vector_Datapoint_vint64_t__SWIG_1(Vector_Datapoint_vint64_t.getCPtr(other), other), true);
  }

  public java.math.BigInteger capacity() {
    return pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_capacity(swigCPtr, this);
  }

  public void reserve(java.math.BigInteger n) {
    pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_isEmpty(swigCPtr, this);
  }

  public void clear() {
    pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_clear(swigCPtr, this);
  }

  public Vector_Datapoint_vint64_t(int count) {
    this(pbeast_ServerProxyJNI.new_Vector_Datapoint_vint64_t__SWIG_2(count), true);
  }

  public Vector_Datapoint_vint64_t(int count, DataPoint_vint64_t value) {
    this(pbeast_ServerProxyJNI.new_Vector_Datapoint_vint64_t__SWIG_3(count, DataPoint_vint64_t.getCPtr(value), value), true);
  }

  private int doSize() {
    return pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_doSize(swigCPtr, this);
  }

  private void doAdd(DataPoint_vint64_t value) {
    pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_doAdd__SWIG_0(swigCPtr, this, DataPoint_vint64_t.getCPtr(value), value);
  }

  private void doAdd(int index, DataPoint_vint64_t value) {
    pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_doAdd__SWIG_1(swigCPtr, this, index, DataPoint_vint64_t.getCPtr(value), value);
  }

  private DataPoint_vint64_t doRemove(int index) {
    return new DataPoint_vint64_t(pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_doRemove(swigCPtr, this, index), true);
  }

  private DataPoint_vint64_t doGet(int index) {
    return new DataPoint_vint64_t(pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_doGet(swigCPtr, this, index), false);
  }

  private DataPoint_vint64_t doSet(int index, DataPoint_vint64_t value) {
    return new DataPoint_vint64_t(pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_doSet(swigCPtr, this, index, DataPoint_vint64_t.getCPtr(value), value), true);
  }

  private void doRemoveRange(int fromIndex, int toIndex) {
    pbeast_ServerProxyJNI.Vector_Datapoint_vint64_t_doRemoveRange(swigCPtr, this, fromIndex, toIndex);
  }

}
