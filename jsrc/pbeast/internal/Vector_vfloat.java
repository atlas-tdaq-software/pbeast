/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class Vector_vfloat extends java.util.AbstractList<Vector_float> implements java.util.RandomAccess {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected Vector_vfloat(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Vector_vfloat obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_Vector_vfloat(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Vector_vfloat(Vector_float[] initialElements) {
    this();
    for (Vector_float element : initialElements) {
      add(element);
    }
  }

  public Vector_vfloat(Iterable<Vector_float> initialElements) {
    this();
    for (Vector_float element : initialElements) {
      add(element);
    }
  }

  public Vector_float get(int index) {
    return doGet(index);
  }

  public Vector_float set(int index, Vector_float e) {
    return doSet(index, e);
  }

  public boolean add(Vector_float e) {
    modCount++;
    doAdd(e);
    return true;
  }

  public void add(int index, Vector_float e) {
    modCount++;
    doAdd(index, e);
  }

  public Vector_float remove(int index) {
    modCount++;
    return doRemove(index);
  }

  protected void removeRange(int fromIndex, int toIndex) {
    modCount++;
    doRemoveRange(fromIndex, toIndex);
  }

  public int size() {
    return doSize();
  }

  public Vector_vfloat() {
    this(pbeast_ServerProxyJNI.new_Vector_vfloat__SWIG_0(), true);
  }

  public Vector_vfloat(Vector_vfloat other) {
    this(pbeast_ServerProxyJNI.new_Vector_vfloat__SWIG_1(Vector_vfloat.getCPtr(other), other), true);
  }

  public java.math.BigInteger capacity() {
    return pbeast_ServerProxyJNI.Vector_vfloat_capacity(swigCPtr, this);
  }

  public void reserve(java.math.BigInteger n) {
    pbeast_ServerProxyJNI.Vector_vfloat_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return pbeast_ServerProxyJNI.Vector_vfloat_isEmpty(swigCPtr, this);
  }

  public void clear() {
    pbeast_ServerProxyJNI.Vector_vfloat_clear(swigCPtr, this);
  }

  public Vector_vfloat(int count) {
    this(pbeast_ServerProxyJNI.new_Vector_vfloat__SWIG_2(count), true);
  }

  public Vector_vfloat(int count, Vector_float value) {
    this(pbeast_ServerProxyJNI.new_Vector_vfloat__SWIG_3(count, Vector_float.getCPtr(value), value), true);
  }

  private int doSize() {
    return pbeast_ServerProxyJNI.Vector_vfloat_doSize(swigCPtr, this);
  }

  private void doAdd(Vector_float value) {
    pbeast_ServerProxyJNI.Vector_vfloat_doAdd__SWIG_0(swigCPtr, this, Vector_float.getCPtr(value), value);
  }

  private void doAdd(int index, Vector_float value) {
    pbeast_ServerProxyJNI.Vector_vfloat_doAdd__SWIG_1(swigCPtr, this, index, Vector_float.getCPtr(value), value);
  }

  private Vector_float doRemove(int index) {
    return new Vector_float(pbeast_ServerProxyJNI.Vector_vfloat_doRemove(swigCPtr, this, index), true);
  }

  private Vector_float doGet(int index) {
    return new Vector_float(pbeast_ServerProxyJNI.Vector_vfloat_doGet(swigCPtr, this, index), false);
  }

  private Vector_float doSet(int index, Vector_float value) {
    return new Vector_float(pbeast_ServerProxyJNI.Vector_vfloat_doSet(swigCPtr, this, index, Vector_float.getCPtr(value), value), true);
  }

  private void doRemoveRange(int fromIndex, int toIndex) {
    pbeast_ServerProxyJNI.Vector_vfloat_doRemoveRange(swigCPtr, this, fromIndex, toIndex);
  }

}
