/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class Vector_AverageDataPoint_uint8_t extends java.util.AbstractList<AverageDataPoint_vuint8_t> implements java.util.RandomAccess {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected Vector_AverageDataPoint_uint8_t(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Vector_AverageDataPoint_uint8_t obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_Vector_AverageDataPoint_uint8_t(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Vector_AverageDataPoint_uint8_t(AverageDataPoint_vuint8_t[] initialElements) {
    this();
    for (AverageDataPoint_vuint8_t element : initialElements) {
      add(element);
    }
  }

  public Vector_AverageDataPoint_uint8_t(Iterable<AverageDataPoint_vuint8_t> initialElements) {
    this();
    for (AverageDataPoint_vuint8_t element : initialElements) {
      add(element);
    }
  }

  public AverageDataPoint_vuint8_t get(int index) {
    return doGet(index);
  }

  public AverageDataPoint_vuint8_t set(int index, AverageDataPoint_vuint8_t e) {
    return doSet(index, e);
  }

  public boolean add(AverageDataPoint_vuint8_t e) {
    modCount++;
    doAdd(e);
    return true;
  }

  public void add(int index, AverageDataPoint_vuint8_t e) {
    modCount++;
    doAdd(index, e);
  }

  public AverageDataPoint_vuint8_t remove(int index) {
    modCount++;
    return doRemove(index);
  }

  protected void removeRange(int fromIndex, int toIndex) {
    modCount++;
    doRemoveRange(fromIndex, toIndex);
  }

  public int size() {
    return doSize();
  }

  public Vector_AverageDataPoint_uint8_t() {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_uint8_t__SWIG_0(), true);
  }

  public Vector_AverageDataPoint_uint8_t(Vector_AverageDataPoint_uint8_t other) {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_uint8_t__SWIG_1(Vector_AverageDataPoint_uint8_t.getCPtr(other), other), true);
  }

  public java.math.BigInteger capacity() {
    return pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_capacity(swigCPtr, this);
  }

  public void reserve(java.math.BigInteger n) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_isEmpty(swigCPtr, this);
  }

  public void clear() {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_clear(swigCPtr, this);
  }

  public Vector_AverageDataPoint_uint8_t(int count) {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_uint8_t__SWIG_2(count), true);
  }

  public Vector_AverageDataPoint_uint8_t(int count, AverageDataPoint_vuint8_t value) {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_uint8_t__SWIG_3(count, AverageDataPoint_vuint8_t.getCPtr(value), value), true);
  }

  private int doSize() {
    return pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_doSize(swigCPtr, this);
  }

  private void doAdd(AverageDataPoint_vuint8_t value) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_doAdd__SWIG_0(swigCPtr, this, AverageDataPoint_vuint8_t.getCPtr(value), value);
  }

  private void doAdd(int index, AverageDataPoint_vuint8_t value) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_doAdd__SWIG_1(swigCPtr, this, index, AverageDataPoint_vuint8_t.getCPtr(value), value);
  }

  private AverageDataPoint_vuint8_t doRemove(int index) {
    return new AverageDataPoint_vuint8_t(pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_doRemove(swigCPtr, this, index), true);
  }

  private AverageDataPoint_vuint8_t doGet(int index) {
    return new AverageDataPoint_vuint8_t(pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_doGet(swigCPtr, this, index), false);
  }

  private AverageDataPoint_vuint8_t doSet(int index, AverageDataPoint_vuint8_t value) {
    return new AverageDataPoint_vuint8_t(pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_doSet(swigCPtr, this, index, AverageDataPoint_vuint8_t.getCPtr(value), value), true);
  }

  private void doRemoveRange(int fromIndex, int toIndex) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_uint8_t_doRemoveRange(swigCPtr, this, fromIndex, toIndex);
  }

}
