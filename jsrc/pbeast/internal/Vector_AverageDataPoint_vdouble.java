/* ----------------------------------------------------------------------------
 * This file was automatically generated by SWIG (http://www.swig.org).
 * Version 4.0.0
 *
 * Do not make changes to this file unless you know what you are doing--modify
 * the SWIG interface file instead.
 * ----------------------------------------------------------------------------- */

package pbeast.internal;

public class Vector_AverageDataPoint_vdouble extends java.util.AbstractList<AverageDataPoint_double> implements java.util.RandomAccess {
  private transient long swigCPtr;
  protected transient boolean swigCMemOwn;

  protected Vector_AverageDataPoint_vdouble(long cPtr, boolean cMemoryOwn) {
    swigCMemOwn = cMemoryOwn;
    swigCPtr = cPtr;
  }

  protected static long getCPtr(Vector_AverageDataPoint_vdouble obj) {
    return (obj == null) ? 0 : obj.swigCPtr;
  }

  protected void finalize() {
    delete();
  }

  public synchronized void delete() {
    if (swigCPtr != 0) {
      if (swigCMemOwn) {
        swigCMemOwn = false;
        pbeast_ServerProxyJNI.delete_Vector_AverageDataPoint_vdouble(swigCPtr);
      }
      swigCPtr = 0;
    }
  }

  public Vector_AverageDataPoint_vdouble(AverageDataPoint_double[] initialElements) {
    this();
    for (AverageDataPoint_double element : initialElements) {
      add(element);
    }
  }

  public Vector_AverageDataPoint_vdouble(Iterable<AverageDataPoint_double> initialElements) {
    this();
    for (AverageDataPoint_double element : initialElements) {
      add(element);
    }
  }

  public AverageDataPoint_double get(int index) {
    return doGet(index);
  }

  public AverageDataPoint_double set(int index, AverageDataPoint_double e) {
    return doSet(index, e);
  }

  public boolean add(AverageDataPoint_double e) {
    modCount++;
    doAdd(e);
    return true;
  }

  public void add(int index, AverageDataPoint_double e) {
    modCount++;
    doAdd(index, e);
  }

  public AverageDataPoint_double remove(int index) {
    modCount++;
    return doRemove(index);
  }

  protected void removeRange(int fromIndex, int toIndex) {
    modCount++;
    doRemoveRange(fromIndex, toIndex);
  }

  public int size() {
    return doSize();
  }

  public Vector_AverageDataPoint_vdouble() {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_vdouble__SWIG_0(), true);
  }

  public Vector_AverageDataPoint_vdouble(Vector_AverageDataPoint_vdouble other) {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_vdouble__SWIG_1(Vector_AverageDataPoint_vdouble.getCPtr(other), other), true);
  }

  public java.math.BigInteger capacity() {
    return pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_capacity(swigCPtr, this);
  }

  public void reserve(java.math.BigInteger n) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_reserve(swigCPtr, this, n);
  }

  public boolean isEmpty() {
    return pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_isEmpty(swigCPtr, this);
  }

  public void clear() {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_clear(swigCPtr, this);
  }

  public Vector_AverageDataPoint_vdouble(int count) {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_vdouble__SWIG_2(count), true);
  }

  public Vector_AverageDataPoint_vdouble(int count, AverageDataPoint_double value) {
    this(pbeast_ServerProxyJNI.new_Vector_AverageDataPoint_vdouble__SWIG_3(count, AverageDataPoint_double.getCPtr(value), value), true);
  }

  private int doSize() {
    return pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_doSize(swigCPtr, this);
  }

  private void doAdd(AverageDataPoint_double value) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_doAdd__SWIG_0(swigCPtr, this, AverageDataPoint_double.getCPtr(value), value);
  }

  private void doAdd(int index, AverageDataPoint_double value) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_doAdd__SWIG_1(swigCPtr, this, index, AverageDataPoint_double.getCPtr(value), value);
  }

  private AverageDataPoint_double doRemove(int index) {
    return new AverageDataPoint_double(pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_doRemove(swigCPtr, this, index), true);
  }

  private AverageDataPoint_double doGet(int index) {
    return new AverageDataPoint_double(pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_doGet(swigCPtr, this, index), false);
  }

  private AverageDataPoint_double doSet(int index, AverageDataPoint_double value) {
    return new AverageDataPoint_double(pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_doSet(swigCPtr, this, index, AverageDataPoint_double.getCPtr(value), value), true);
  }

  private void doRemoveRange(int fromIndex, int toIndex) {
    pbeast_ServerProxyJNI.Vector_AverageDataPoint_vdouble_doRemoveRange(swigCPtr, this, fromIndex, toIndex);
  }

}
