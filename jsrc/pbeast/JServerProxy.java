package pbeast;

import java.math.BigInteger;
import java.net.URL;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import pbeast.internal.AggregationType;
import pbeast.internal.FunctionConfig;
import pbeast.internal.QueryDataBase;
import pbeast.internal.QueryDataBaseVector;
import pbeast.internal.ServerProxy;
import pbeast.internal.ServerProxy.CookieSetup;
import pbeast.internal.ServerProxy.DataCompression;
import pbeast.internal.Type;
import pbeast.internal.Vector_string;


/**
 * Class used to query the P-Beast server.
 */
public class JServerProxy {
    @SuppressWarnings("unused")
    private final static NativeLibraryLoader loader = NativeLibraryLoader.instance();
    private final ServerProxy sp_impl; // From native code

    /**
     * Enumeration for configuring the cookie need to the query the P-Beast server. Cookies are needed when the P-Beast server is accessed
     * from GPN.
     * <p>
     * For details about the cookie setup and its interaction with the CERN SSO authentication, please look at <a href=
     * "https://atlas-tdaq-sw.web.cern.ch/atlas-tdaq-sw/doxygen/tdaq/production/html/da/d1e/classpbeast_1_1ServerProxy.html#details">this</a>
     * page.
     */
    public static enum COOKIE_SETUP {
        /**
         * The setup has not been done yet
         */
        NOT_INITIATED(CookieSetup.NotInitited),
        /**
         * Cookies are not used (i.e., SSO is not needed)
         */
        NONE(CookieSetup.None),
        /**
         * Kerberos authentication using a valid CERN Kerberos ticket acquired by user process
         */
        AUTO_UPDATE_KERBEROS(CookieSetup.AutoUpdateKerberos),
        /**
         * User Certificates authentication using ~/private/myCert.pem and ~/private/myCert.key files
         */
        AUTO_UPDATE_USER_CERTIFICATES(CookieSetup.AutoUpdateUserCertificates);

        @SuppressWarnings("unused")
        private final static NativeLibraryLoader ll = NativeLibraryLoader.instance();
        private final CookieSetup origin; // From native code

        private COOKIE_SETUP(final CookieSetup origin) {
            this.origin = origin;
        }

        CookieSetup getOriginal() {
            return this.origin;
        }
    }

    /**
     * Enumeration to configure how P-Beast will compress data.
     */
    public static enum DATA_COMPRESSION {
        /**
         * Data are not compressed
         */
        NO_COMPRESSION(DataCompression.NoCompression),
        /**
         * Names of objects are compressed
         */
        ZIP_CATALOG(DataCompression.ZipCatalog),
        /**
         * All the data are compressed
         */
        ZIP_DATA(DataCompression.ZipData),
        /**
         * A catalog of strings is built and every string is passed only once
         */
        CATALOGIZE_STRINGS(DataCompression.CatalogizeStrings);

        @SuppressWarnings("unused")
        private final static NativeLibraryLoader ll = NativeLibraryLoader.instance();
        private final DataCompression origin; // From native code

        private DATA_COMPRESSION(final DataCompression origin) {
            this.origin = origin;
        }

        DataCompression getOriginal() {
            return this.origin;
        }
    }

    /**
     * Enumeration to configure how P-Beast will approximate missing data in case of downsampling
     */
    public static enum DOWNSAMPLE_FILL_GAPS {
        /**
         * Do not add any approximated data in the sparse intervals
         */
        NONE(Type.None),
        /**
         * Add approximated data to previous and next intervals, if they are empty
         */
        NEAR(Type.Near),
        /**
         * Add approximated data to all sparse intervals
         */
        ALL(Type.All);

        @SuppressWarnings("unused")
        private final static NativeLibraryLoader ll = NativeLibraryLoader.instance();
        private final Type origin; // From native code

        private DOWNSAMPLE_FILL_GAPS(final Type origin) {
            this.origin = origin;
        }

        Type getOriginal() {
            return this.origin;
        }
    }
    
    /**
     * This class allows to define the functions that the P-Beast server will apply to data before returning them.
     * <p>
     * Functions supported by P-Beast are described <a href="https://its.cern.ch/jira/browse/ADAMATLAS-327">here</a>.
     * <p>
     * In order to know the type of aggregation used for a certain data series, use the {@link #getType(String)} method (the
     * {@link FunctionDefinition} instance must be the same instance used to query P-Beast).
     */
    public static class FunctionDefinition {
        /**
         * Enumeration defining which type of aggregation is used
         */
        public static enum AGGREGATION_TYPE {
            /**
             * Data are aggregated summing them
             */
            SUM(AggregationType.AggregationSum),
            /**
             * Data are aggregated calculating their average values
             */
            AVERAGE(AggregationType.AggregationAverage),
            /**
             * Data are aggregated extracting their minimum values
             */
            MIN(AggregationType.AggregationMin),
            /**
             * Data are aggregated extracting their maximum values
             */
            MAX(AggregationType.AggregationMax),
            /**
             * No aggregation if performed
             */
            NONE(AggregationType.AggregationNone);

            private final AggregationType original; // From native code

            private final static Map<AggregationType, AGGREGATION_TYPE> mapping = new HashMap<>();

            static {
                for(final AGGREGATION_TYPE i : EnumSet.allOf(AGGREGATION_TYPE.class)) {
                    AGGREGATION_TYPE.mapping.put(i.getOriginal(), i);
                }
            }

            private AGGREGATION_TYPE(final AggregationType orig) {
                this.original = orig;
            }

            static AGGREGATION_TYPE getType(final AggregationType t) {
                return AGGREGATION_TYPE.mapping.get(t);
            }

            AggregationType getOriginal() {
                return this.original;
            }
        }

        @SuppressWarnings("unused")
        private final static NativeLibraryLoader ll = NativeLibraryLoader.instance();
        private final FunctionConfig funConfig = new FunctionConfig(); // From Native code

        /**
         * Constructor.
         * <p>
         * No functions are defined.
         */
        public FunctionDefinition() {
        }

        /**
         * Constructor.
         * <p>
         * Use this constructor to define functions to be applied by P-Beast.
         * 
         * @param funcNames List of function definitions, in the form of <em>func_name:func_paramater</em>
         * @param interval Down-sampling value
         * @throws Exception Thrown when an aggregation function is used but no down-sampling is defined
         */
        public FunctionDefinition(final List<String> funcNames, final long interval) throws Exception {
            this.funConfig.construct(new Vector_string(funcNames), interval);
        }

        /**
         * Set whether original raw data should be returned together with aggregated data.
         * 
         * @param value <em>true</em> to have raw data returned
         */
        public void setKeepData(final boolean value) {
            this.funConfig.setM_keep_data(value);
        }

        /**
         * Helper function to print a description of all the available functions.
         * 
         * @return A string with the description of all the available functions
         */
        public static String printFunctionDescription() {
            return FunctionConfig.make_description_of_functions();
        }

        /**
         * It returns <em>true</em> if no aggregation is performed by the functions.
         * 
         * @return <em>true</em> if no aggregation is performed by the functions
         */
        public boolean noAggregations() {
            return this.funConfig.no_aggregations();
        }

        /**
         * It returns the type of aggregation used for series with the specified name.
         * 
         * @param name The name of the data series
         * @return The type of aggregation used for series with the specified name
         */
        public AGGREGATION_TYPE getType(final String name) {
            return AGGREGATION_TYPE.getType(this.funConfig.get_type(name));
        }

        FunctionConfig getFunctionConfig() {
            return this.funConfig;
        }
    }

    /**
     * Constructor.
     * 
     * @param baseUrl The URL of the P-Beast server (i.e., <em>https://atlasop.cern.ch</em> to access data from GPN or
     *            <em>http://pc-tdq-bst-05.cern.ch</em> to access data from ATCN)
     * @param cookieSetup The cookie setup used to connect to the P-Beast server
     * @param userCertDir Directory storing files for authentication based on user certificates
     * @throws Exception Thrown in case of any error
     */
    public JServerProxy(final String baseUrl, final COOKIE_SETUP cookieSetup, final String userCertDir) throws Exception {
        this.sp_impl = new ServerProxy(baseUrl, cookieSetup.getOriginal(), userCertDir);
    }

    /**
     * {@inheritDoc}
     * <p>
     * No directory is defined for the user certificates.
     * 
     * @param baseUrl The URL of the P-Beast server (i.e., <em>https://atlasop.cern.ch</em> to access data from GPN or
     *            <em>http://pc-tdq-bst-05.cern.ch</em> to access data from ATCN)
     * @param cookieSetup The cookie setup used to connect to the P-Beast server
     * @throws Exception Thrown in case of any error
     */
    public JServerProxy(final URL baseUrl, final COOKIE_SETUP cookieSetup) throws Exception {
        this(baseUrl.toString(), cookieSetup, "");
    }

    /**
     * {@inheritDoc}
     * <p>
     * No directory is defined for the user certificates.
     * <p>
     * The cookie configuration is set to {@link COOKIE_SETUP#NOT_INITIATED}.
     * 
     * @param baseUrl The URL of the P-Beast server (i.e., <em>https://atlasop.cern.ch</em> to access data from GPN or
     *            <em>http://pc-tdq-bst-05.cern.ch</em> to access data from ATCN)
     * @throws Exception Thrown in case of any error
     */
    public JServerProxy(final URL baseUrl) throws Exception {
        this(baseUrl.toString(), COOKIE_SETUP.NOT_INITIATED, "");
    }

    /**
     * It returns the P-Beast server base URL as a string.
     * 
     * @return The P-Beast server base URL as a string
     */
    public String get_base_url() {
        return this.sp_impl.get_base_url();
    }

    /**
     * It returns the name of the cookie file.
     * 
     * @return The name of the cookie file
     */
    public String get_cookie_file_name() {
        return this.sp_impl.get_cookie_file_name();
    }

    /**
     * It returns the names of all the partitions archived in P-Beast.
     * 
     * @return The names of all the partitions archived in P-Beast
     * @throws Exception Thrown in case of any error
     */
    public List<String> get_partitions() throws Exception {
        return new ArrayList<>(this.sp_impl.get_partitions());
    }

    /**
     * It returns a list of all the classes archived for a given partition
     * 
     * @param partition_name The name of the partition
     * @return The list of all the classes archived for a given partition
     * @throws Exception Thrown in case of any error
     */
    public List<String> get_classes(final String partition_name) throws Exception {
        return new ArrayList<>(this.sp_impl.get_classes(partition_name));
    }

    /**
     * It returns a list of all the attributes available for a given class in the context of the specified partition.
     * 
     * @param partition_name The partition name
     * @param class_name The class name
     * @return The list of all the attributes available for a given class in the context of the specified partition
     * @throws Exception Thrown in case of any error
     */
    public List<String> get_attributes(final String partition_name, final String class_name) throws Exception {
        return new ArrayList<>(this.sp_impl.get_attributes(partition_name, class_name));
    }

    /**
     * It returns the list of objects for a given attribute, belonging to a given class in the context of the specified partition.
     * 
     * @param partition_name The name of the partition
     * @param class_name The class name
     * @param attribute_path The name of the attribute
     * @param object_mask Regular expression matching the required object names (if empty, all objects are returned)
     * @param since The beginning of the time interval (in microseconds from Epoch)
     * @param until The end of the time interval (in microseconds from Epoch)
     * @return The list of objects
     * @throws Exception Thrown in case of any error
     */
    public List<String> get_objects(final String partition_name,
                                    final String class_name,
                                    final String attribute_path,
                                    final String object_mask,
                                    final long since,
                                    final long until)
        throws Exception
    {
        return new ArrayList<>(this.sp_impl.get_objects(partition_name,
                                                        class_name,
                                                        attribute_path,
                                                        object_mask,
                                                        BigInteger.valueOf(since),
                                                        BigInteger.valueOf(until)));
    }

    /**
     * As {@link #get_objects(String, String, String, String, long, long)}, with:
     * <ul>
     * <li><em>object_mask</em> is set in such a way to return all the objects;</li>
     * <li>The time interval extends to all the available data.</li>
     * </ul>
     * 
     * @param partition_name The name of the partition
     * @param class_name The class name
     * @param attribute_path The name of the attribute
     * @return The list of objects
     * @throws Exception Thrown in case of any error
     */
    public List<String> get_objects(final String partition_name, final String class_name, final String attribute_path) throws Exception {
        return this.get_objects(partition_name, class_name, attribute_path, "", 0L, Long.MAX_VALUE);
    }

    /**
     * This method performs the actual query to the P-Beast server.
     * 
     * @param partition_name The name of the partition
     * @param class_name The name of the class
     * @param attribute_path The name of the attribute (or the path to the attribute, in case of nested classes)
     * @param object_mask The name of the object or a regular expression matching several objects (if empty, return all objects)
     * @param is_regexp <em>true</em> if the <em>object_mask</em> parameter is a regular expression
     * @param since The beginning of the time interval (in microseconds from Epoch)
     * @param until The end of the time interval (in microseconds from Epoch)
     * @param prev_interval Time window to look at before the start of the query interval (in microseconds)
     * @param next_interval Time window to look at after the start of the query interval (in microseconds)
     * @param get_all_updates If <em>true</em> it returns all data-points, even when the corresponding value is not modified
     * @param sample_interval Interval for data down-sampling (if <em>0</em> raw data are returned)
     * @param fd Object describing the functions to be applied to data
     * @param data_compression Data compression options
     * @param use_fqn Use fully qualified names for data series (i.e., dot-separated string including partition, class and attribute names
     *            prefix for any object)
     * @return A {@link JQueryDataBase} instance that can be used to inspect returned data
     * @throws Exception Thrown in case of any error
     * @see JQueryDataBase#getData(JQueryDataBase)
     * @see JQueryDataBase#getDownSampledData(JQueryDataBase)
     */
    public List<JQueryDataBase> get_data(final String partition_name,
                                         final String class_name,
                                         final String attribute_path,
                                         final String object_mask,
                                         final boolean is_regexp,
                                         final long since,
                                         final long until,
                                         final long prev_interval,
                                         final long next_interval,
                                         final boolean get_all_updates,
                                         final long sample_interval,
                                         final FunctionDefinition fd,
                                         final EnumSet<DATA_COMPRESSION> data_compression,
                                         DOWNSAMPLE_FILL_GAPS fillGaps,
                                         final boolean use_fqn)
        throws Exception
    {
        final List<JQueryDataBase> result = new ArrayList<>();

        int compressionOptions = 0;
        for(final DATA_COMPRESSION opt : data_compression) {
            compressionOptions = compressionOptions | opt.getOriginal().swigValue();
        }

        final QueryDataBaseVector raw = this.sp_impl.get_data(partition_name,
                                                              class_name,
                                                              attribute_path,
                                                              object_mask,
                                                              is_regexp,
                                                              BigInteger.valueOf(since),
                                                              BigInteger.valueOf(until),
                                                              BigInteger.valueOf(prev_interval),
                                                              BigInteger.valueOf(next_interval),
                                                              get_all_updates,
                                                              sample_interval,
                                                              fillGaps.getOriginal(),
                                                              fd.getFunctionConfig(),
                                                              compressionOptions,
                                                              use_fqn);

        for(final QueryDataBase el : raw) {
            result.add(new JQueryDataBase(el, sample_interval > 0 ? true : false));
        }

        return result;
    }

    /**
     * As
     * {@link #get_data(String, String, String, String, boolean, long, long, long, long, boolean, long, FunctionDefinition, EnumSet, boolean)},
     * with:
     * <ul>
     * <li>Data are returned only in case of modifications (<em>get_all_updates</em> is set to <em>false</em>;</li>
     * <li>No down-sampling (<em>sample_interval</em> is set to <em>false</em>);</li>
     * <li>No functions are applied to data;</li>
     * <li>Data and name of objects are compressed;</li>
     * <li>Fully qualified names are not used.</li>
     * </ul>
     * 
     * @param partition_name The name of the partition
     * @param class_name The name of the class
     * @param attribute_path The name of the attribute (or the path to the attribute, in case of nested classes)
     * @param object_mask The name of the object or a regular expression matching several objects (if empty, return all objects)
     * @param is_regexp <em>true</em> if the <em>object_mask</em> parameter is a regular expression
     * @param since The beginning of the time interval (in microseconds from Epoch)
     * @param until The end of the time interval (in microseconds from Epoch)
     * @param prev_interval Time window to look at before the start of the query interval (in microseconds)
     * @param next_interval Time window to look at after the start of the query interval (in microseconds)
     * @return A {@link JQueryDataBase} instance that can be used to inspect returned data
     * @throws Exception Thrown in case of any error
     * @see JQueryDataBase#getData(JQueryDataBase)
     * @see JQueryDataBase#getDownSampledData(JQueryDataBase)
     */
    public List<JQueryDataBase> get_data(final String partition_name,
                                         final String class_name,
                                         final String attribute_path,
                                         final String object_mask,
                                         final boolean is_regexp,
                                         final long since,
                                         final long until,
                                         final long prev_interval,
                                         final long next_interval)
        throws Exception
    {
        return this.get_data(partition_name,
                             class_name,
                             attribute_path,
                             object_mask,
                             is_regexp,
                             since,
                             until,
                             prev_interval,
                             next_interval,
                             false,
                             0,
                             new FunctionDefinition(),
                             EnumSet.of(DATA_COMPRESSION.ZIP_DATA, DATA_COMPRESSION.ZIP_CATALOG),
                             DOWNSAMPLE_FILL_GAPS.NONE,
                             false);
    }
}
