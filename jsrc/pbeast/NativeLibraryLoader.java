package pbeast;

/**
 * Utility class to load the P-Beast native library.
 * <p>
 * The library is lazy loaded (i.e., is it not loaded until an instance of this class is created). At the same time it assures that the
 * library is attempted to be loaded once and only once.
 * <p>
 * If the library cannot be loaded, an error message is reported and the application is exited.
 * <p>
 * In order to be sure that the native library is loaded before any class using it is used, just link an instance of this class (using the
 * {@link #instance()} method to the concerned classes.
 */
class NativeLibraryLoader {
    static {
        try {
            System.loadLibrary("pbeastj");
        }
        catch(final UnsatisfiedLinkError ex) {
            ex.printStackTrace();
            System.exit(-1);
        }

    }

    private final static NativeLibraryLoader INSTANCE = new NativeLibraryLoader();

    private NativeLibraryLoader() {
    }

    static NativeLibraryLoader instance() {
        return NativeLibraryLoader.INSTANCE;
    }
}
