package pbeast;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import pbeast.internal.*;
import pbeast.internal.DataType;


/**
 * Entry point class to have access to data returned by a P-Beast query.
 * <p>
 * Here is an example on how to use this class and keep type-safety (<em>i.e.</em>, data collections can be iterated with the proper
 * expected types and exceptions will be thrown when the type is not compatible with the expected one). This way of operating is also very
 * helpful when the type of data is known a priory.
 * 
 * <pre>
 * {@code
 *            final List<JQueryDataBase> data = server.get_data(...);
 *            for(final JQueryDataBase d : data) {
 *               if(d.type().equals(JQueryDataBase.DataTypes.LONG)) {
 *                   if(d.isDownSampled() == false) {
 *                       if(d.isArray() == false) {
 *                           final Map<String, List<DataPoint<Long>>> dpm = JQueryDataBase.getData(d);
 *                           for(final Entry<String, List<DataPoint<Long>>> dps : dpm.entrySet()) {
 *                               // Do something...
 *                           }
 *                       } else {
 *                           final Map<String, List<DataPoint<List<Long>>>> dpm = JQueryDataBase.getData(d);
 *                           for(final Entry<String, List<DataPoint<List<Long>>>> dps : dpm.entrySet()) {
 *                               // Do something...
 *                           }
 *                       }
 *                   } else {
 *                       if(d.isArray() == false) {
 *                           final Map<String, List<DownSampledDataPoint<Long, Double>>> dpm = JQueryDataBase.getDownSampledData(d);
 *                           for(final Entry<String, List<DownSampledDataPoint<Long, Double>>> dps : dpm.entrySet()) {
 *                               // Do something...
 *                           }
 *                       } else {
 *                           final Map<String, List<DownSampledDataPoint<List<Long>, List<Double>>>> dpm = JQueryDataBase.getDownSampledData(d);
 *                           for(final Entry<String, List<DownSampledDataPoint<List<Long>, List<Double>>>> dps : dpm.entrySet()) {
 *                               // Do something...
 *                           }
 *                       }
 *                   }
 *               } else {
 *                   // Handle a different type or just give up...
 *               }
 *           }
 * }
 * </pre>
 * 
 * A simpler and not strictly type safe way to use this class is shown in the next code snippet. In that case the information about the
 * original type is lost (the user, if needed, can then cast data to the expected type). This way may be particularly useful in case of
 * simple data inspection (i.e., print string representation of data).
 * 
 * <pre>
 * {@code
 *           final List<JQueryDataBase> data = server.get_data(...);
 *           for(final JQueryDataBase d : data) {
 *               if(d.isDownSampled() == false) {
 *                   final Map<String, List<DataPoint<Object>>> dpm = JQueryDataBase.getData(d);
 *                   for(final Entry<String, List<DataPoint<Object>>> dps : dpm.entrySet()) {
 *                       // Do something...
 *                   }
 *               } else {
 *                   final Map<String, List<DownSampledDataPoint<Object, Object>>> dpm = JQueryDataBase.getDownSampledData(d);
 *                   for(final Entry<String, List<DownSampledDataPoint<Object, Object>>> dps : dpm.entrySet()) {
 *                       // Do something...
 *                   }
 *               }
 *           }
 * }
 * </pre>
 * 
 * If the returned data have been processed by P-Beast using some of the supported functions (see {@link JServerProxy.FunctionDefinition}),
 * then give a look at the {@link DownSampledDataPoint} class documentation for a proper usage.
 * 
 * @see DataPoint
 * @see DownSampledDataPoint
 * @see JServerProxy.FunctionDefinition
 * @see JQueryDataBase#getData(JQueryDataBase)
 * @see JQueryDataBase#getDownSampledData(JQueryDataBase)
 */
public class JQueryDataBase {
    private final QueryDataBase m_impl;
    private final boolean isDownSampled;

    /**
     * Enumeration describing the type of data returned by a P-Beast query.
     * <p>
     * In case of not down-sampled data, the type corresponds to the type of the data-point value.
     * <p>
     * In case of down-sampled data, the type corresponds to the type of the maximum and minimum values of the data point in the
     * down-sampled time interval.
     * <p>
     * For vector data, the type of the collection is always {@link List}.
     */
    public static enum DataTypes {
        /**
         * This is mapped to {@link Boolean}
         */
        BOOLEAN(Arrays.asList(DataType.DOUBLE)),
        /**
         * This is mapped to {@link Double}
         */
        DOUBLE(Arrays.asList(DataType.DOUBLE)),
        /**
         * This is mapped to {@link Float}
         */
        FLOAT(Arrays.asList(DataType.FLOAT)),
        /**
         * This is mapped to {@link Byte}
         */
        BYTE(Arrays.asList(DataType.S8)),
        /**
         * This is mapped to {@link Short}
         */
        SHORT(Arrays.asList(DataType.U8, DataType.S16)),
        /**
         * This is mapped to {@link Integer}
         */
        INTEGER(Arrays.asList(DataType.U16, DataType.S32, DataType.ENUM)),
        /**
         * This is mapped to {@link Long}
         */
        LONG(Arrays.asList(DataType.DATE, DataType.U32, DataType.S64)),
        /**
         * This is mapped to {@link BigInteger}
         */
        BIG_INTEGER(Arrays.asList(DataType.TIME, DataType.U64)),
        /**
         * This is mapped to {@link String}
         */
        STRING(Arrays.asList(DataType.STRING)),
        /**
         * Not used
         */
        VOID(Arrays.asList(DataType.VOID)),
        /**
         * Not used
         */
        OBJECT(Arrays.asList(DataType.OBJECT));

        private final List<DataType> originalTypes;

        private final static Map<DataType, DataTypes> mapping = new EnumMap<>(DataType.class);

        static {
            for(final DataTypes dt : DataTypes.values()) {
                for(final DataType orig : dt.originalTypes) {
                    DataTypes.mapping.put(orig, dt);
                }
            }
        }

        private DataTypes(final List<DataType> orig) {
            this.originalTypes = orig;
        }

        public static DataTypes convert(final DataType original) {
            return DataTypes.mapping.get(original);
        }
    }

    /**
     * Constructor.
     * 
     * @param qdb Object returned by the JNI P-Beast call
     * @param isDownSampled <em>true</em> if the contained data are down-sampled
     */
    JQueryDataBase(final QueryDataBase qdb, final boolean isDownSampled) {
        this.m_impl = qdb;
        this.isDownSampled = isDownSampled;
    }

    /**
     * It returns a time-stamp (as micro-seconds since Epoch) indicating the beginning of the IOV for the returned data.
     * 
     * @return A time-stamp (as micro-seconds since Epoch) indicating the beginning of the IOV for the returned data
     */
    public BigInteger since() {
        return this.m_impl.since();
    }

    /**
     * It returns a time-stamp (as micro-seconds since Epoch) indicating the end of the IOV for the returned data.
     * 
     * @return A time-stamp (as micro-seconds since Epoch) indicating the end of the IOV for the returned data
     */
    public BigInteger until() {
        return this.m_impl.until();
    }

    /**
     * It returns the type of the data-points.
     * <p>
     * Use this information to properly infer the types to be used in {@link #getData(JQueryDataBase)} and
     * {@link #getDownSampledData(JQueryDataBase)}.
     * 
     * @return The type of the data-points
     * @see DataPoint
     * @see DownSampledDataPoint
     * @see JQueryDataBase#getData(JQueryDataBase)
     * @see JQueryDataBase#getDownSampledData(JQueryDataBase)
     */
    public DataTypes type() {
        return DataTypes.convert(this.m_impl.type());
    }

    /**
     * It returns <em>true</em> if the P-Beast data are actually data arrays.
     * 
     * @return <em>true</em> if the P-Beast data are actually data arrays
     */
    public boolean isArray() {
        return this.m_impl.is_array();
    }

    /**
     * It returns <em>true</em> if data are down-sampled.
     * 
     * @return <em>true</em> if data are down-sampled
     */
    public boolean isDownSampled() {
        return this.isDownSampled;
    }

    /**
     * It expands down-sampled data as a result of a P-Beast query.
     * 
     * @param db The {@link JQueryDataBase} object returned by {@link JServerProxy#get_data)}
     * @param <T> The "raw" type of the data represented by the {@link DownSampledDataPoint}
     * @param <V> The type of the data-point mean value
     * @return A map whose keys represent the names of the data series and whose values represent the data-series data.
     * @throws Exception The type of data is not supported
     * @throws IllegalArgumentException When the <em>db</em> argument does not correspond to down-sampled data
     * @throws ClassCastException If the data type are not compatible with the defined template parameters
     */
    public static <T, V> Map<String, List<DownSampledDataPoint<T, V>>> getDownSampledData(final JQueryDataBase db) throws Exception {
        if(db.isDownSampled() == false) {
            throw new IllegalArgumentException("The returned data are not down-sampled, \"getData()\" should be used instead!");
        }

        final Map<String, List<DownSampledDataPoint<T, V>>> map = new TreeMap<>();

        switch(db.m_impl.type()) {
            case BOOL: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_bool obj =
                                                              pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_bool(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_bool data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vbool dataPoints = data.get(n);
                        for(final AverageDataPoint_bool dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Boolean.valueOf(dp.min_value()),
                                                                        Boolean.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vbool obj =
                                                               pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vbool(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vbool data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_bool dataPoints = data.get(n);
                        for(final AverageDataPoint_vbool dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Boolean>(dp.min_value()),
                                                                        new ArrayList<Boolean>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case DOUBLE: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_double obj =
                                                                pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_double(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_double data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vdouble dataPoints = data.get(n);
                        for(final AverageDataPoint_double dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Double.valueOf(dp.min_value()),
                                                                        Double.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vdouble obj =
                                                                 pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vdouble(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vdouble data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_double dataPoints = data.get(n);
                        for(final AverageDataPoint_vdouble dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Double>(dp.min_value()),
                                                                        new ArrayList<Double>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case FLOAT: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_float obj =
                                                               pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_float(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_float data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vfloat dataPoints = data.get(n);
                        for(final AverageDataPoint_float dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Float.valueOf(dp.min_value()),
                                                                        Float.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vfloat obj =
                                                                pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vfloat(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vfloat data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_float dataPoints = data.get(n);
                        for(final AverageDataPoint_vfloat dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Float>(dp.min_value()),
                                                                        new ArrayList<Float>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case S16: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_int16_t obj =
                                                                 pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_int16_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_int16_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vint16_t dataPoints = data.get(n);
                        for(final AverageDataPoint_int16_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Short.valueOf(dp.min_value()),
                                                                        Short.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vint16_t obj =
                                                                  pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vint16_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vint16_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_int16_t dataPoints = data.get(n);
                        for(final AverageDataPoint_vint16_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Short>(dp.min_value()),
                                                                        new ArrayList<Short>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case ENUM:
            case S32: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_int32_t obj =
                                                                 pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_int32_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_int32_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vint32_t dataPoints = data.get(n);
                        for(final AverageDataPoint_int32_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Integer.valueOf(dp.min_value()),
                                                                        Integer.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vint32_t obj =
                                                                  pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vint32_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vint32_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_int32_t dataPoints = data.get(n);
                        for(final AverageDataPoint_vint32_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Integer>(dp.min_value()),
                                                                        new ArrayList<Integer>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case S64: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_int64_t obj =
                                                                 pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_int64_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_int64_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vint64_t dataPoints = data.get(n);
                        for(final AverageDataPoint_int64_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Long.valueOf(dp.min_value()),
                                                                        Long.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vint64_t obj =
                                                                  pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vint64_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vint64_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_int64_t dataPoints = data.get(n);
                        for(final AverageDataPoint_vint64_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Long>(dp.min_value()),
                                                                        new ArrayList<Long>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case S8: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_int8_t obj =
                                                                pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_int8_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_int8_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vint8_t dataPoints = data.get(n);
                        for(final AverageDataPoint_int8_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Byte.valueOf(dp.min_value()),
                                                                        Byte.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vint8_t obj =
                                                                 pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vint8_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vint8_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_int8_t dataPoints = data.get(n);
                        for(final AverageDataPoint_vint8_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Byte>(dp.min_value()),
                                                                        new ArrayList<Byte>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case U16: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_uint16_t obj =
                                                                  pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_uint16_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_uint16_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vuint16_t dataPoints = data.get(n);
                        for(final AverageDataPoint_uint16_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Integer.valueOf(dp.min_value()),
                                                                        Integer.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vuint16_t obj =
                                                                   pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vuint16_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vuint16_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_uint16_t dataPoints = data.get(n);
                        for(final AverageDataPoint_vuint16_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Integer>(dp.min_value()),
                                                                        new ArrayList<Integer>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case DATE:
            case U32: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_uint32_t obj =
                                                                  pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_uint32_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_uint32_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vuint32_t dataPoints = data.get(n);
                        for(final AverageDataPoint_uint32_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Long.valueOf(dp.min_value()),
                                                                        Long.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vuint32_t obj =
                                                                   pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vuint32_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vuint32_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_uint32_t dataPoints = data.get(n);
                        for(final AverageDataPoint_vuint32_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Long>(dp.min_value()),
                                                                        new ArrayList<Long>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case TIME:
            case U64: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_uint64_t obj =
                                                                  pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_uint64_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_uint64_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vuint64_t dataPoints = data.get(n);
                        for(final AverageDataPoint_uint64_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        dp.min_value(),
                                                                        dp.max_value()));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vuint64_t obj =
                                                                   pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vuint64_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vuint64_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_uint64_t dataPoints = data.get(n);
                        for(final AverageDataPoint_vuint64_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<BigInteger>(dp.min_value()),
                                                                        new ArrayList<BigInteger>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case U8: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_uint8_t obj =
                                                                 pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_uint8_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_uint8_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vuint8_t dataPoints = data.get(n);
                        for(final AverageDataPoint_uint8_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : Double.valueOf(dp.value()),
                                                                        Short.valueOf(dp.min_value()),
                                                                        Short.valueOf(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vuint8_t obj =
                                                                  pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vuint8_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vuint8_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_uint8_t dataPoints = data.get(n);
                        for(final AverageDataPoint_vuint8_t dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<Double>(dp.value()),
                                                                        new ArrayList<Short>(dp.min_value()),
                                                                        new ArrayList<Short>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case STRING: {
                if(db.isArray() == false) {
                    final QueryData_AverageDataPoint_string obj =
                                                                pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_string(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_string data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_vstring dataPoints = data.get(n);
                        for(final AverageDataPoint_string dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : dp.value(),
                                                                        dp.min_value(),
                                                                        dp.max_value()));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_AverageDataPoint_vstring obj =
                                                                 pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_AverageDataPoint_vstring(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorAverageDataPoint_vstring data = obj.get_data();
                    for(final String n : names) {
                        final List<DownSampledDataPoint<T, V>> dataList = new ArrayList<>();
                        final Vector_AverageDataPoint_string dataPoints = data.get(n);
                        for(final AverageDataPoint_vstring dp : dataPoints) {
                            dataList.add(new DownSampledDataPoint<T, V>(dp.ts(),
                                                                        dp.is_null() ? null : new ArrayList<String>(dp.value()),
                                                                        new ArrayList<String>(dp.min_value()),
                                                                        new ArrayList<String>(dp.max_value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case VOID:
            case OBJECT:
            default:
                throw new pbeast.Exception("Unsupported data type");
        }

        return map;
    }

    /**
     * It expands data as a result of a P-Beast query.
     * 
     * @param db The {@link JQueryDataBase} object returned by {@link JServerProxy#get_data)}
     * @param <T> The type of the data represented by the {@link DataPoint}
     * @return A map whose keys represent the names of the data series and whose values represent the data-series data.
     * @throws Exception The type of data is not supported
     * @throws IllegalArgumentException When the <em>db</em> argument corresponds to down-sampled data
     * @throws ClassCastException If the data type are not compatible with the defined template parameters
     */
    public static <T> Map<String, List<DataPoint<T>>> getData(final JQueryDataBase db) throws Exception {
        if(db.isDownSampled() == true) {
            throw new IllegalArgumentException("The returned data are down-sampled, \"getDownSampledData()\" should be used instead!");
        }

        final Map<String, List<DataPoint<T>>> map = new TreeMap<>();

        switch(db.m_impl.type()) {
            case BOOL: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_bool obj = pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_bool(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_bool data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_bool dataPoints = data.get(n);
                        for(final DataPoint_bool dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Boolean.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vbool obj = pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vbool(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vbool data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vbool dataPoints = data.get(n);
                        for(final DataPoint_vbool dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Boolean>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case DOUBLE: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_double obj =
                                                         pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_double(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_double data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_double dataPoints = data.get(n);
                        for(final DataPoint_double dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Double.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vdouble obj =
                                                          pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vdouble(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vdouble data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vdouble dataPoints = data.get(n);
                        for(final DataPoint_vdouble dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Double>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case FLOAT: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_float obj = pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_float(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_float data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_float dataPoints = data.get(n);
                        for(final DataPoint_float dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Float.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vfloat obj =
                                                         pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vfloat(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vfloat data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vfloat dataPoints = data.get(n);
                        for(final DataPoint_vfloat dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Float>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case S16: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_int16_t obj =
                                                          pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_int16_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_int16_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_int16_t dataPoints = data.get(n);
                        for(final DataPoint_int16_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Short.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vint16_t obj =
                                                           pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vint16_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vint16_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vint16_t dataPoints = data.get(n);
                        for(final DataPoint_vint16_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Short>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case ENUM:
            case S32: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_int32_t obj =
                                                          pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_int32_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_int32_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_int32_t dataPoints = data.get(n);
                        for(final DataPoint_int32_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Integer.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vint32_t obj =
                                                           pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vint32_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vint32_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vint32_t dataPoints = data.get(n);
                        for(final DataPoint_vint32_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Integer>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case S64: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_int64_t obj =
                                                          pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_int64_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_int64_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_int64_t dataPoints = data.get(n);
                        for(final DataPoint_int64_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Long.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vint64_t obj =
                                                           pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vint64_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vint64_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vint64_t dataPoints = data.get(n);
                        for(final DataPoint_vint64_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Long>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case S8: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_int8_t obj =
                                                         pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_int8_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_int8_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_int8_t dataPoints = data.get(n);
                        for(final DataPoint_int8_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Byte.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vint8_t obj =
                                                          pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vint8_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vint8_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vint8_t dataPoints = data.get(n);
                        for(final DataPoint_vint8_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Byte>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case U16: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_uint16_t obj =
                                                           pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_uint16_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_uint16_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_uint16_t dataPoints = data.get(n);
                        for(final DataPoint_uint16_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Integer.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vuint16_t obj =
                                                            pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vuint16_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vuint16_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vuint16_t dataPoints = data.get(n);
                        for(final DataPoint_vuint16_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Integer>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case DATE:
            case U32: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_uint32_t obj =
                                                           pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_uint32_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_uint32_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_uint32_t dataPoints = data.get(n);
                        for(final DataPoint_uint32_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Long.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vuint32_t obj =
                                                            pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vuint32_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vuint32_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vuint32_t dataPoints = data.get(n);
                        for(final DataPoint_vuint32_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Long>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case TIME:
            case U64: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_uint64_t obj =
                                                           pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_uint64_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_uint64_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_uint64_t dataPoints = data.get(n);
                        for(final DataPoint_uint64_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : dp.value()));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vuint64_t obj =
                                                            pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vuint64_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vuint64_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vuint64_t dataPoints = data.get(n);
                        for(final DataPoint_vuint64_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<BigInteger>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case U8: {
                if(db.isArray() == false) {
                    final QueryData_DataPoint_uint8_t obj =
                                                          pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_uint8_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_uint8_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_uint8_t dataPoints = data.get(n);
                        for(final DataPoint_uint8_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : Short.valueOf(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vuint8_t obj =
                                                           pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vuint8_t(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vuint8_t data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vuint8_t dataPoints = data.get(n);
                        for(final DataPoint_vuint8_t dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : new ArrayList<Short>(dp.value())));
                        }

                        map.put(n, dataList);
                    }
                }
            }
                break;
            case STRING:
                if(db.isArray() == false) {
                    final QueryData_DataPoint_string obj =
                                                         pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_string(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_string data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_string dataPoints = data.get(n);
                        for(final DataPoint_string dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : dp.value()));
                        }

                        map.put(n, dataList);
                    }
                } else {
                    final QueryData_DataPoint_vstring obj =
                                                          pbeast.internal.ServerProxy.dynamic_cast_to_QueryData_DataPoint_vstring(db.m_impl);
                    final Vector_string names = obj.get_names();
                    final String_To_VectorDataPoint_vstring data = obj.get_data();
                    for(final String n : names) {
                        final List<DataPoint<T>> dataList = new ArrayList<>();
                        final Vector_Datapoint_vstring dataPoints = data.get(n);
                        for(final DataPoint_vstring dp : dataPoints) {
                            dataList.add(new DataPoint<T>(dp.ts(), dp.is_null() ? null : dp.value()));
                        }

                        map.put(n, dataList);
                    }
                }
                break;
            case VOID:
            case OBJECT:
            default:
                throw new pbeast.Exception("Unsupported data type");
        }

        return map;
    }
}
