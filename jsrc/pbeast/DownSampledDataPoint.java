package pbeast;

import java.math.BigInteger;


/**
 * This class represents a data-point returned by a P-Beast query when down-sampling is requested.
 * <p>
 * A data-point of this kind is defined by its mean, maximum and minimum values in the down-sampled time period.
 * <p>
 * NOTE: The type of the mean value for <b>not</b> vector data is {@link Double} in all the cases but {@link DataTypes#STRING}, for which
 * the {@link String} type is used.
 * <p>
 * NOTE: The type of the mean value for vector data is {@code List<Double>} in all the cases but {@link DataTypes#STRING}, for which the
 * {@code List<String>} type is used.
 * <p>
 * NOTE: The type of the maximum and minimum values corresponds to the one returned by {@link JQueryDataBase#type()}.
 * <p>
 * NOTE: When P-Beast functions are used (i.e., sum, average, etc.) then the <em>mean</em> value should be used in case of <b>no
 * aggregation</b> functions, <b>sum</b> and <b>average</b>; the <em>minimum</em> value should be used if case of <b>min</b> and <b>max</b>
 * functions.
 * 
 * @param <T> The "raw" type of the data point (i.e., the type of the maximum and minimum values)
 * @param <V> The type of the mean value
 * @see JQueryDataBase#getDownSampledData(JQueryDataBase)
 */
public class DownSampledDataPoint<T, V> extends DataPoint<V> {
    final T minValue;
    final T maxValue;

    /**
     * COnstructor.
     * 
     * @param ts Time stamp
     * @param mean Mean value
     * @param minValue Minimum value
     * @param maxValue Maximum value
     */
    @SuppressWarnings("unchecked")
    DownSampledDataPoint(final BigInteger ts, final Object mean, final Object minValue, final Object maxValue) {
        super(ts, mean);

        this.minValue = (T) minValue;
        this.maxValue = (T) maxValue;
    }

    /**
     * It returns the data-point minimum value in the down-sampled time period
     * 
     * @return The data-point minimum value in the down-sampled time period
     */
    public T minValue() {
        return this.minValue;
    }

    /**
     * It returns the data-point maximum value in the down-sampled time period
     * 
     * @return The data-point maximum value in the down-sampled time period
     */
    public T maxValue() {
        return this.maxValue;
    }
}
