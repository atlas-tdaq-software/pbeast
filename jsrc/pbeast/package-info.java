/**
 * This package contains classes allowing to query the P-Beast server.
 * <p>
 * It provides:
 * <ul>
 * <li>A class to actually send queries to the P-Beast server: {@link pbeast.JServerProxy};
 * <li>A class to inspect the data received from P-Beast: {@link pbeast.JQueryDataBase};
 * <li>Two classes to access time stamp and values of the received data: {@link pbeast.DataPoint} and {@link pbeast.DownSampledDataPoint}
 * <li>A test application (implemented by the {@link pbeast.helper.ReadPBeast} class and that can be started using the
 * <em>pbeast_read_j</em> script) able to query the P-Beast server and print the received data in an user-friendly way.
 * </ul>
 * <p>
 * Users shall never use any class in the pbeast.internal package!
 */
package pbeast;
