package pbeast;

/**
 * Exception used to report errors coming from the P-Beast layer.
 */
public class Exception extends java.lang.Exception {
    private static final long serialVersionUID = 3545468024078278306L;

    /**
     * {@inheritDoc}
     */
    public Exception(final String message) {
        super(message);
    }

    /**
     * {@inheritDoc}
     */
    public Exception(final String message, final Throwable cause) {
        super(message, cause);
    }
}
