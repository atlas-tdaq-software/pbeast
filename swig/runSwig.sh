#!/bin/sh
rm ./*.cxx ./*.h
rm ../jsrc/pbeast/internal/*.java

# Code generated with version https://github.com/swig/swig/tree/8f224694e78cd17ecc3098be2f9f36378a54632e

swig \
-v \
-DSWIGWORDSIZE64 \
-outdir ../jsrc/pbeast/internal \
-package pbeast.internal -Wall -c++ -java ./pbeast_ServerProxy.i \
                                                                              