%module pbeast_ServerProxy

// Some macro definitions

// Shared pointers for different template types
%define WRAP_SHARED_POINTERS(T)
%shared_ptr(pbeast::QueryData<pbeast::DataPoint<T>>);
%shared_ptr(pbeast::QueryData<pbeast::AverageDataPoint<T, pbeast::AverageType<T>::average_t>>);
%shared_ptr(pbeast::QueryData<pbeast::DataPoint<std::vector<T>>>);
%shared_ptr(pbeast::QueryData<pbeast::AverageDataPoint<std::vector<T>, pbeast::AverageType<std::vector<T>>::average_t>>);
%enddef

// Templates
%define WRAP_TEMPLATES(prefix, T)
%template(CType_v ## prefix) pbeast::CType<std::vector<T>>;
%template(CType_ ## prefix) pbeast::CType<T>;
%template(Vector_ ## prefix) std::vector<T>;
%template(DataPointCore_ ## prefix) pbeast::DataPointCore<T>;
%template(DataPoint_ ## prefix) pbeast::DataPoint<T>;
%template(QueryData_DataPoint_ ## prefix) pbeast::QueryData<pbeast::DataPoint<T>>;
%template(Vector_Datapoint_ ## prefix) std::vector<pbeast::DataPoint<T>>;
%template(String_To_VectorDataPoint_ ## prefix) std::map<std::string, std::vector<pbeast::DataPoint<T>>>;
%template(Vector_v ## prefix) std::vector<std::vector<T>>;
%template(DataPointCore_v ## prefix) pbeast::DataPointCore<std::vector<T>>;
%template(DataPoint_v ## prefix) pbeast::DataPoint<std::vector<T>>;
%template(QueryData_DataPoint_v ## prefix) pbeast::QueryData<pbeast::DataPoint<std::vector<T>>>;
%template(Vector_Datapoint_v ## prefix) std::vector<pbeast::DataPoint<std::vector<T>>>;
%template(String_To_VectorDataPoint_v ## prefix) std::map<std::string, std::vector<pbeast::DataPoint<std::vector<T>>>>;
%template(AverageType_ ## prefix) pbeast::AverageType<T>;
%template(AverageType_v ## prefix) pbeast::AverageType<std::vector<T>>;
%template(QueryData_AverageDataPoint_ ## prefix) pbeast::QueryData<pbeast::AverageDataPoint<T, pbeast::AverageType<T>::average_t>>;
%template(QueryData_AverageDataPoint_v ## prefix) pbeast::QueryData<pbeast::AverageDataPoint<std::vector<T>, pbeast::AverageType<std::vector<T>>::average_t>>;
%template(String_To_VectorAverageDataPoint_ ## prefix) std::map<std::string, std::vector<pbeast::AverageDataPoint<T, pbeast::AverageType<T>::average_t>>>;
%template(String_To_VectorAverageDataPoint_v ## prefix) std::map<std::string, std::vector<pbeast::AverageDataPoint<std::vector<T>, pbeast::AverageType<std::vector<T>>::average_t>>>;
%template(AverageDataPoint_ ## prefix) pbeast::AverageDataPoint<T, pbeast::AverageType<T>::average_t>;
%template(AverageDataPoint_v ## prefix) pbeast::AverageDataPoint<std::vector<T>, pbeast::AverageType<std::vector<T>>::average_t>;
%template(Vector_AverageDataPoint_ ## prefix) std::vector<pbeast::AverageDataPoint<std::vector<T>, pbeast::AverageType<std::vector<T>>::average_t>>;
%template(Vector_AverageDataPoint_v ## prefix) std::vector<pbeast::AverageDataPoint<T, pbeast::AverageType<T>::average_t>>;
%enddef

// Functions to allow down-casting in Java code
// http://www.swig.org/Doc3.0/SWIGDocumentation.html#Java_adding_downcasts
%define WRAP_CAST_FUNCTIONS(prefix, T)
%exception pbeast::ServerProxy::dynamic_cast_to_QueryData_DataPoint_ ## prefix(QueryDataBase* ptr) {
    $action
    if(!result) {
        jclass excep = jenv->FindClass("java/lang/ClassCastException");
        if(excep) {
            jenv->ThrowNew(excep, "dynamic_cast exception");
        }
    }
}

%extend pbeast::ServerProxy {
    static pbeast::QueryData<pbeast::DataPoint<T>>* dynamic_cast_to_QueryData_DataPoint_ ## prefix(QueryDataBase* ptr) {
        return dynamic_cast<pbeast::QueryData<pbeast::DataPoint<T>>*>(ptr);
    }
}

%exception pbeast::ServerProxy::dynamic_cast_to_QueryData_DataPoint_v ## prefix(QueryDataBase* ptr) {
    $action
    if(!result) {
        jclass excep = jenv->FindClass("java/lang/ClassCastException");
        if(excep) {
            jenv->ThrowNew(excep, "dynamic_cast exception");
        }
    }
}

%extend pbeast::ServerProxy {
    static pbeast::QueryData<pbeast::DataPoint<std::vector<T>>>* dynamic_cast_to_QueryData_DataPoint_v ## prefix(QueryDataBase* ptr) {
        return dynamic_cast<pbeast::QueryData<pbeast::DataPoint<std::vector<T>>>*>(ptr);
    }
}      
    
%exception pbeast::ServerProxy::dynamic_cast_to_QueryData_AverageDataPoint_ ## prefix(QueryDataBase* ptr) {
    $action
    if(!result) {
        jclass excep = jenv->FindClass("java/lang/ClassCastException");
        if(excep) {
            jenv->ThrowNew(excep, "dynamic_cast exception");
        }
    }
}

%extend pbeast::ServerProxy {
    static pbeast::QueryData<pbeast::AverageDataPoint<T, pbeast::AverageType<T>::average_t>>* dynamic_cast_to_QueryData_AverageDataPoint_ ## prefix(QueryDataBase* ptr) {
        return dynamic_cast<pbeast::QueryData<pbeast::AverageDataPoint<T, pbeast::AverageType<T>::average_t>>*>(ptr);
    }
}

%exception pbeast::ServerProxy::dynamic_cast_to_QueryData_AverageDataPoint_v ## prefix(QueryDataBase* ptr) {
    $action
    if(!result) {
        jclass excep = jenv->FindClass("java/lang/ClassCastException");
        if(excep) {
            jenv->ThrowNew(excep, "dynamic_cast exception");
        }
    }
}

%extend pbeast::ServerProxy {
    static pbeast::QueryData<pbeast::AverageDataPoint<std::vector<T>, pbeast::AverageType<std::vector<T>>::average_t>>* dynamic_cast_to_QueryData_AverageDataPoint_v ## prefix(QueryDataBase* ptr) {
        return dynamic_cast<pbeast::QueryData<pbeast::AverageDataPoint<std::vector<T>, pbeast::AverageType<std::vector<T>>::average_t>>*>(ptr);
    }   
}
%enddef

// Handling exceptions in native code
%define WRAP_SERVERPROXY_EXCEPTIONS(function)
%javaexception("pbeast.Exception") pbeast::ServerProxy::##function {
  try {
    $action
  } catch(std::exception& ex) {
    jclass clazz = jenv->FindClass("pbeast/Exception");
    jenv->ThrowNew(clazz, ex.what());
    return $null;
  }
}
%enddef

// End of macro definitions

%{
#include "pbeast/data-point.h"
#include "pbeast/functions.h"
#include "pbeast/data-type.h"
#include "pbeast/query.h"
#include "pbeast/downsample-fill-gaps.h"
%}

%include <typemaps.i>
%include <cpointer.i>
%include <carrays.i>
%include <stl.i>
%include <std_list.i>
%include <std_pair.i>
%include <std_map.i>
%include <various.i>
%include <std_shared_ptr.i>
%include <stdint.i>
%include <swiginterface.i>
%include <enums.swg>
%javaconst(1);

// Shared pointer declarations before any of those classes is used
%shared_ptr(pbeast::QueryDataBase);

WRAP_SHARED_POINTERS(bool);
WRAP_SHARED_POINTERS(int8_t);
WRAP_SHARED_POINTERS(uint8_t);
WRAP_SHARED_POINTERS(int16_t);
WRAP_SHARED_POINTERS(uint16_t);
WRAP_SHARED_POINTERS(int32_t);
WRAP_SHARED_POINTERS(uint32_t);
WRAP_SHARED_POINTERS(int64_t);
WRAP_SHARED_POINTERS(uint64_t);
WRAP_SHARED_POINTERS(float);
WRAP_SHARED_POINTERS(double);
WRAP_SHARED_POINTERS(std::string);

%ignore pbeast::s_names_of_types;
%ignore pbeast::DataTypeSize;

// Ignore this constructor to not import std::filesystem
%ignore pbeast::ServerProxy::ServerProxy(const std::string& base_url = "", CookieSetup cookie_setup_type = NotInitited, const std::filesystem::path& user_cert_dir = "");

// Constructors are clashing with SWIG-generated constructors for some specializations
// Just disabling them (anyway those objects are never created on the Java side)
%ignore pbeast::DataPointCore::DataPointCore;
%ignore pbeast::DataPoint::DataPoint;
%ignore pbeast::AverageDataPoint::AverageDataPoint;

// Some additional (and simpler) template declarations
%template(QueryDataBaseVector) std::vector<std::shared_ptr<pbeast::QueryDataBase>>;
%template(StringToAggregationTypeMap) std::map<std::string, pbeast::AggregationType>;
%template(AliasTypeToStringPair) std::pair<pbeast::AliasType, std::string>;
%template(AliasTypeToStringPairVector) std::vector<std::pair<pbeast::AliasType, std::string>>;
%template(FunctionTypeToStringPair) std::pair<pbeast::FunctionType, std::string>;
%template(FunctionTypeToStringPairVector) std::vector<std::pair<pbeast::FunctionType, std::string>>;

// Exception handling
WRAP_SERVERPROXY_EXCEPTIONS(ServerProxy);
WRAP_SERVERPROXY_EXCEPTIONS(get_partitions);
WRAP_SERVERPROXY_EXCEPTIONS(get_classes);
WRAP_SERVERPROXY_EXCEPTIONS(get_attributes);
WRAP_SERVERPROXY_EXCEPTIONS(get_objects);
WRAP_SERVERPROXY_EXCEPTIONS(get_data);

%javaexception("pbeast.Exception") pbeast::FunctionConfig::construct {
  try {
    $action
  } catch(std::exception& ex) {
    jclass clazz = jenv->FindClass("pbeast/Exception");
    jenv->ThrowNew(clazz, ex.what());
    return $null;
  }
}

%include "../pbeast/data-point.h"
%include "../pbeast/data-type.h"
%include "../pbeast/functions.h"
%include "../pbeast/query.h"
%include "../pbeast/downsample-fill-gaps.h"

// The following template definitions must come after the includes
WRAP_TEMPLATES(double, double);
WRAP_TEMPLATES(int8_t, int8_t);
WRAP_TEMPLATES(uint8_t, uint8_t);
WRAP_TEMPLATES(int16_t, int16_t);
WRAP_TEMPLATES(uint16_t, uint16_t);
WRAP_TEMPLATES(int32_t, int32_t);
WRAP_TEMPLATES(uint32_t, uint32_t);
WRAP_TEMPLATES(int64_t, int64_t);
WRAP_TEMPLATES(uint64_t, uint64_t);
WRAP_TEMPLATES(float, float);
WRAP_TEMPLATES(bool, bool);
WRAP_TEMPLATES(string, std::string);

WRAP_CAST_FUNCTIONS(bool, bool);
WRAP_CAST_FUNCTIONS(int8_t, int8_t);
WRAP_CAST_FUNCTIONS(uint8_t, uint8_t);
WRAP_CAST_FUNCTIONS(int16_t, int16_t);
WRAP_CAST_FUNCTIONS(uint16_t, uint16_t);
WRAP_CAST_FUNCTIONS(int32_t, int32_t);
WRAP_CAST_FUNCTIONS(uint32_t, uint32_t);
WRAP_CAST_FUNCTIONS(int64_t, int64_t);
WRAP_CAST_FUNCTIONS(uint64_t, uint64_t);
WRAP_CAST_FUNCTIONS(float, float);
WRAP_CAST_FUNCTIONS(double, double);
WRAP_CAST_FUNCTIONS(string, std::string);
