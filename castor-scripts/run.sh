#!/bin/sh

############################################################################################

ROOT='/det/tdaq/pbeast/scripts/exporter/CastorScript'
CONF_FILE="${ROOT}/Conf.eos.cfg"

############################################################################################

# create log directory and make it group writable

LB=`grep 'LogDir:' $CONF_FILE | sed "s/.* //;s/'//g"`
LOG_BASE=`dirname $LB`

if [ -z "$LOG_BASE" ]
then
  echo "ERROR: bad config file $CONF_FILE"
  echo "the property LogDir is not defined"
  exit 1
fi


if [ ! -d ${LOG_BASE} ]
then
  mkdir "${LOG_BASE}" || (echo "ERROR: cannot create directory ${LOG_BASE}" ; exit 1)
  chgrp TDAQ "${LOG_BASE}" || (echo "ERROR: chgrp TDAQ ${LOG_BASE} failed" ; exit 1)
  chmod g+ws "${LOG_BASE}" || (echo "ERROR: chmod g+ws ${LOG_BASE} failed" ; exit 1)
  echo "create directory ${LOG_BASE}"
fi

############################################################################################

LOGS_HOME="${LOG_BASE}/logs"

LOG_DIR="${LOG_BASE}/`date '+%F-%T'`"
mkdir -p "${LOG_DIR}" || (echo "ERROR: cannot create directory ${LOG_DIR}" ; exit 1)
rm -f "${LOGS_HOME}" || (echo "ERROR: cannot remove ${LOGS_HOME}" ; exit 1)
ln -s "${LOG_DIR}" "${LOGS_HOME}" || (echo "ERROR: cannot create sw link ${LOG_DIR} => ${LOGS_HOME}" ; exit 1)

############################################################################################

source /sw/castor/script_setup.sh
nohup python /sw/castor/Script/CastorScript.py $CONF_FILE > $LOGS_HOME/main.log 2>&1 &
