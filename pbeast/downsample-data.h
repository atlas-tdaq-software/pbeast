#ifndef PBEAST_DOWNSAMPLE_DATA_H
#define PBEAST_DOWNSAMPLE_DATA_H

#include <algorithm>
#include <set>
#include <vector>

#include <ers/ers.h>

#include "pbeast/series-data.h"
#include "pbeast/downsample-fill-gaps.h"

namespace pbeast
{
  uint32_t inline
  mk_downsample_ts(uint32_t t, uint32_t dt)
  {
    uint32_t num = t / dt;
    return (dt * num);
  }


  class DownsampleDataBase
  {
  protected:

    DownsampleDataBase(uint32_t dt) : m_ts(0), m_dt(dt), m_half_dt(dt/2), m_mdt(pbeast::mk_ts(dt))
    {
      ;
    }

    void
    reset(uint64_t ts)
    {
      m_ts = pbeast::mk_ts(mk_downsample_ts(pbeast::ts2secs(ts), m_dt));
    }

    void
    reset(uint32_t ts)
    {
      m_ts = pbeast::mk_ts(mk_downsample_ts(ts, m_dt));
    }

    template<class T>
    bool
    clear_data_and_check_need_of_insert(std::vector<std::pair<uint64_t,T>>& data, uint64_t ts, bool push_value)
    {
      if (auto len = data.size())
        {
          while (--len > 0)
            {
              if (data[len].first < m_ts)
                {
                  data.erase(data.begin(), data.begin() + len);
                  break;
                }
            }
        }

      return(push_value && (data.empty() || data.back().first != ts));
    }

    template<class T>
      static void
      ajust_min_max(DownsampleValue<T>& ds_value, const T& value)
      {
        if (value > ds_value.m_max_value)
          ds_value.m_max_value = value;

        if (value < ds_value.m_min_value)
          ds_value.m_min_value = value;
      }

    template<class T>
      static void
      ajust_min_max(DownsampleValue<T>& ds_value, const DownsampleValue<T>& value)
      {
        if (value.m_max_value > ds_value.m_max_value)
          ds_value.m_max_value = value.m_max_value;

        if (value.m_min_value < ds_value.m_min_value)
          ds_value.m_min_value = value.m_min_value;
      }

    template<class T>
      static void
      ajust_min_max_average(DownsampleValue<T>& ds_value, const double value)
      {
        T max_value = pbeast::AverageValue<T>::max(value);

        if (max_value > ds_value.m_max_value)
          ds_value.m_max_value = max_value;

        if (value < ds_value.m_min_value)
          ds_value.m_min_value = value;
      }

    template<class T>
    void
    calculate_average(std::vector<std::pair<uint64_t,T>>& data, DownsampleValue<T>& value)
    {
      auto len = data.size();

      // left and right edge timestamps
      uint64_t T0 = m_ts;
      uint64_t T1 = T0 + m_mdt;

      // need to be sure that there is no more than one point before T0 (adjust start_idx) and after T1 (adjust len)

      unsigned int start_idx = 0;

      while ((start_idx + 1) < len && data[start_idx + 1].first < T0)
        start_idx++;

      while (len > start_idx && len > 2 && data[len - 2].first > T1)
        len--;

      if (len == 2 && start_idx != 0)
        start_idx = 0;

      if ((len - start_idx) == 2)
        {
          if (data[start_idx].first > T0)
            T0 = data[start_idx].first;

          if (data[start_idx + 1].first < T1)
            T1 = data[start_idx + 1].first;

          value.m_value = static_cast<double>(data[start_idx].second) + (static_cast<double>(data[start_idx + 1].second) - static_cast<double>(data[start_idx].second)) * (static_cast<double>((T0 + T1) / 2) - static_cast<double>(data[start_idx].first)) / static_cast<double>(data[start_idx + 1].first - data[start_idx].first);
          ajust_min_max_average(value, value.m_value);
        }
      else
        {
          uint64_t t0 = 0;
          uint64_t t1 = 0;

          if (data[start_idx].first < T0)
            {
              double v = static_cast<double>(data[start_idx].second) + (static_cast<double>(data[start_idx+1].second) - static_cast<double>(data[start_idx].second)) * static_cast<double>(T0 - data[start_idx].first) / static_cast<double>(data[start_idx+1].first - data[start_idx].first);
              value.m_value = v * static_cast<double>(std::min(data[start_idx+1].first, T1) - T0);
              ajust_min_max_average(value, v);
              t0 = data[start_idx].first;
              data[start_idx].first = T0;
            }
          else
            {
              value.m_value = static_cast<double>(data[start_idx].second) * static_cast<double>(std::min(data[start_idx+1].first, T1) - data[start_idx].first);
            }

          if (data[len - 1].first > T1)
            {
              double v = static_cast<double>(data[len - 2].second) + (static_cast<double>(data[len - 1].second) - static_cast<double>(data[len - 2].second)) * static_cast<double>(T1 - data[len - 2].first) / static_cast<double>(data[len - 1].first - data[len - 2].first);
              value.m_value += v * static_cast<double>(T1 - data[len - 2].first);
              ajust_min_max_average(value, v);
              t1 = data[len - 1].first;
              data[len - 1].first = T1;
            }
          else
            {
              value.m_value += static_cast<double>(data[len - 1].second) * static_cast<double>(data[len - 1].first - data[len - 2].first);
            }

          for (uint64_t i = start_idx+2; i < len; ++i)
            {
              value.m_value += static_cast<double>(data[i - 1].second) * (data[i].first - data[i - 2].first);
            }

          value.m_value /= static_cast<double>(2 * (data[len - 1].first - data[start_idx].first));

          // restore original timestamps if needed

          if (t0)
            data[start_idx].first = t0;

          if (t1)
            data[len - 1].first = t1;
        }

      // ignore round errors

      static constexpr double tolerable_error = 1.005;

      if (value.m_min_value > value.m_value)
        {
          if (value.m_value == 0 || static_cast<double>(value.m_min_value) / value.m_value < tolerable_error)
            {
              ERS_DEBUG(2, "adjust average value \"" << value.m_value << "\" to min value \"" << value.m_min_value << "\"");
            }
          else
            {
              ERS_DEBUG(0, "fix average value \"" << value.m_value << "\" < min value \"" << value.m_min_value << "\"");
            }

          value.m_value = value.m_min_value;
        }

      if (value.m_value > value.m_max_value)
        {
          if (value.m_max_value == 0 || static_cast<double>(value.m_value) / value.m_max_value < tolerable_error)
            {
              ERS_DEBUG(2, "adjust average value \"" << value.m_value << "\" to max value \"" << value.m_max_value << "\"");
            }
          else
            {
              ERS_DEBUG(0, "fix average value \"" << value.m_value << "\" > max value \"" << value.m_max_value << "\"");
            }

          value.m_value = value.m_max_value;
        }
    }


  public:

    bool
    check(uint64_t ts)
    {
      return ((ts - m_ts) > m_mdt);
    }

    /*
     *  To be used by server functions only.
     */

    void
    set_ts(uint64_t ts, uint64_t mdt)
    {
      m_ts = ts;
      m_mdt = mdt;
    }

  protected:

    uint64_t m_ts;

    uint32_t m_dt;       // in seconds
    uint32_t m_half_dt;  // half of dt
    uint64_t m_mdt;      // in microseconds

  };

  template<>
    inline void
    DownsampleDataBase::calculate_average(std::vector<std::pair<uint64_t, std::string>>& data, DownsampleValue<std::string>& value)
    {
      auto len = data.size();

      // left and right edge timestamps
      uint64_t T0 = m_ts;
      uint64_t T1 = T0 + m_mdt;

      uint64_t t0 = 0;
      uint64_t t1 = 0;

      std::map<std::string, uint64_t> weigths;

      if (data[0].first < T0)
        {
          t0 = data[0].first;
          data[0].first = T0;
          ajust_min_max(value, data[0].second);
        }

      if (data[len - 1].first > T1)
        {
          t1 = data[len - 1].first;
          data[len - 1].first = T1;
        }

      unsigned int idx = 0;
      uint64_t max_value = 0;

      for (uint64_t i = 1; i < len; ++i)
        {
          uint64_t v = (weigths[data[i - 1].second] += data[i].first - data[i - 1].first);

          if (v > max_value)
            {
              max_value = v;
              idx = i - 1;
            }
        }

      value.m_value = data[idx].second;

      if (t0)
        data[0].first = t0;

      if (t1)
        data[len - 1].first = t1;
    }

  template<>
    inline void
    DownsampleDataBase::calculate_average(std::vector<std::pair<uint64_t, pbeast::Void>>& data, DownsampleValue<pbeast::Void>& value)
    {

    }

  template<>
    inline void
    DownsampleDataBase::ajust_min_max(DownsampleValue<pbeast::Void>& ds_value, const pbeast::Void& value)
    {

    }

  template<>
    inline void
    DownsampleDataBase::ajust_min_max(DownsampleValue<pbeast::Void>& ds_value, const DownsampleValue<pbeast::Void>& value)
    {

    }


  template<class T>
    class DownsampleData : public DownsampleDataBase
    {
    public:

      DownsampleData(uint32_t dt) : DownsampleDataBase(dt)
      {
        ;
      }

      void
      simple_reset(const T& value)
      {
        m_value = value;
      }

      void
      simple_reset(const DownsampleValue<T>& value)
      {
        m_value = value;
      }

      void
      reset(const T& value, uint64_t ts, bool push_value = true)
      {
        m_value = value;
        DownsampleDataBase::reset(ts);

        if (clear_data_and_check_need_of_insert(m_data, ts, push_value))
          m_data.emplace_back(ts, value);

        for (const auto& x : m_data)
          ajust_min_max(m_value, x.second);
      }

      void
      reset(const DownsampleValue<T>& value, uint32_t ts, bool push_value = true)
      {
        m_value = value;
        DownsampleDataBase::reset(ts);

        if (clear_data_and_check_need_of_insert(m_data, pbeast::mk_ts(ts), push_value))
          m_data.emplace_back(pbeast::mk_ts(ts), value.m_value);

        for (const auto& x : m_data)
          ajust_min_max(m_value, x.second);
      }

      void
      add(const T& value, uint64_t ts)
      {
        m_data.emplace_back(ts, value);
        ajust_min_max(m_value, value);
      }

      void
      add(const DownsampleValue<T>& value, uint64_t ts)
      {
        m_data.emplace_back(ts, value.m_value);
        ajust_min_max(m_value, value);
      }

      bool
      check(const T& value, uint64_t ts)
      {
        m_data.emplace_back(ts, value);

        if (DownsampleDataBase::check(ts))
          {
            return true;
          }
        else
          {
            ajust_min_max(m_value, value);

            return false;
          }
      }

      bool
      check(const DownsampleValue<T>& value, uint32_t ts)
      {
        uint64_t ts64 = pbeast::mk_ts(ts);
        m_data.emplace_back(ts64, value.m_value);

        if (DownsampleDataBase::check(ts64))
          {
            return true;
          }
        else
          {
            ajust_min_max(m_value, value);

            return false;
          }
      }

      void
      put(std::vector<DownsampledSeriesData<T>>& to, pbeast::FillGaps::Type fill_gap, uint64_t len = 0)
      {
        uint32_t ts = pbeast::ts2secs(m_ts) + m_half_dt;

        if (m_data.size() > 1)
          calculate_average(m_data, m_value);

        while (len--)
          {
            if (to[len].get_ts() < ts)
              break;
            else if (to[len].get_ts() == ts)
              {
                to[len].merge(m_value);
                return;
              }
          }

        // check distance from previous data and insert inner data if the distance > sampling interval

        if constexpr (!std::is_same<T, std::string>::value && !std::is_same<T, Void>::value)
          if (fill_gap != pbeast::FillGaps::None && m_data.size() > 0 && !to.empty())
            {
              const uint32_t last_ts = to.back().get_ts();
              const uint32_t distance = (ts - last_ts) / m_dt;

              if (distance > 1 && m_data[0].first < pbeast::mk_ts(ts))
                {
                  const uint64_t last_raw_ts = pbeast::mk_ts(last_ts + m_half_dt);

                  unsigned int i = 0;
                  while (++i < m_data.size())
                    if (m_data[i].first >= last_raw_ts)
                      break;

                  DownsampleValue<T> value = m_data[i - 1].second;

                  to.emplace_back(last_ts + m_dt, value);

                  if (distance > 2)
                    {
                      if (fill_gap == pbeast::FillGaps::Near)
                        to.emplace_back(ts - m_dt, value);
                      else
                        for (uint32_t x = last_ts + 2 * m_dt; x < ts; x += m_dt)
                          to.emplace_back(x, value);
                    }
                }
            }

        to.emplace_back(ts, m_value);
      }

      DownsampledSeriesData<T>
      get_average()
      {
        if (m_data.size() > 1)
          calculate_average(m_data, m_value);

        return DownsampledSeriesData<T>(pbeast::ts2secs(m_ts), m_value);
      }


    private:

      DownsampleValue<T> m_value;

      std::vector<std::pair<uint64_t,T>> m_data;
    };


  template<class T>
    class DownsampleVectorData : public DownsampleDataBase
    {
    public:

      DownsampleVectorData(uint32_t dt) :
          DownsampleDataBase(dt)
      {
        ;
      }

      void
      simple_reset(const std::vector<T>& value)
      {
        for (unsigned int i = 0; i < value.size(); ++i)
          m_value.emplace_back(value[i], value[i], value[i]);
      }

      void
      simple_reset(const std::vector<DownsampleValue<T>>& value)
      {
        m_value = value;
      }

      void
      reset(const std::vector<T>& value, uint64_t ts, bool push_value = true)
      {
        const unsigned int val_len(value.size());

        m_value.clear();
        for(unsigned int i = 0; i < val_len; ++i)
          {
            m_value.emplace_back(value[i],value[i],value[i]);
          }
        DownsampleDataBase::reset(ts);

        extend_data(val_len);

        for (unsigned int i = 0; i < val_len; ++i)
          if (clear_data_and_check_need_of_insert(m_data[i], ts, push_value))
            m_data[i].emplace_back(ts, value[i]);

        truncate_data(val_len);
      }

      void
      reset(const std::vector<DownsampleValue<T>>& value, uint32_t ts, bool push_value = true)
      {
        uint64_t ts64 = pbeast::mk_ts(ts);
        const unsigned int val_len(value.size());

        m_value = value;
        DownsampleDataBase::reset(ts);

        extend_data(val_len);

        for (unsigned int i = 0; i < val_len; ++i)
          if (clear_data_and_check_need_of_insert(m_data[i], ts64, push_value))
            m_data[i].emplace_back(ts64, value[i].get_value());

        truncate_data(val_len);
      }

      void
      add(const std::vector<T>& value, uint64_t ts)
      {
        store_value(value, ts);

        const unsigned int vec_len(m_value.size());
        const unsigned int val_len(value.size());
        const unsigned int len = std::min(vec_len, val_len);

        for (unsigned int i = 0; i < len; ++i)
          ajust_min_max(m_value[i], value[i]);

        for (unsigned int i = len; i < val_len; ++i)
          m_value.emplace_back(value[i], value[i], value[i]);
      }

      void
      add(const std::vector<DownsampleValue<T>>& value, uint64_t ts)
      {
        store_value(value, ts);

        const unsigned int vec_len(m_value.size());
        const unsigned int val_len(value.size());
        const unsigned int len = std::min(vec_len, val_len);

        for (unsigned int i = 0; i < len; ++i)
          ajust_min_max(m_value[i], value[i]);

        for (unsigned int i = len; i < val_len; ++i)
          m_value.emplace_back(value[i]);
      }

      bool
      check(const std::vector<T>& value, uint64_t ts)
      {
        store_value(value, ts);

        if (DownsampleDataBase::check(ts))
          {
            return true;
          }
        else
          {
            const unsigned int vec_len(m_value.size());
            const unsigned int val_len(value.size());
            const unsigned int len = std::min(vec_len, val_len);

            for(unsigned int i = 0; i < len; ++i)
              {
                ajust_min_max(m_value[i], value[i]);
              }

            for(unsigned int i = len; i < val_len; ++i)
              {
                m_value.emplace_back(value[i],value[i],value[i]);
              }

            return false;
          }
      }

      bool
      check(const std::vector<DownsampleValue<T>>& value, uint32_t ts)
      {
        uint64_t ts64 = pbeast::mk_ts(ts);

        store_value(value, ts64);

        if (DownsampleDataBase::check(ts64))
          {
            return true;
          }
        else
          {
            const unsigned int vec_len(m_value.size());
            const unsigned int val_len(value.size());
            const unsigned int len = std::min(vec_len, val_len);

            for(unsigned int i = 0; i < len; ++i)
              {
                ajust_min_max(m_value[i], value[i]);
              }

            for(unsigned int i = len; i < val_len; ++i)
              {
                m_value.emplace_back(value[i]);
              }

            return false;
          }
      }


      void
      put(std::vector<DownsampledSeriesVectorData<T> > & to, pbeast::FillGaps::Type fill_gap, uint64_t len = 0)
      {
        uint32_t ts = pbeast::ts2secs(m_ts) + m_half_dt;

        for (unsigned int i = 0; i < m_data.size(); ++i)
          if (m_data[i].size() > 1)
            calculate_average(m_data[i], m_value[i]);

        while (len--)
          {
            if (to[len].get_ts() < ts)
              break;
            else if (to[len].get_ts() == ts)
              {
                to[len].merge(m_value);
                return;
              }
          }

        // check distance from previous data and insert inner data if the distance > sampling interval

        if constexpr (!std::is_same<T, std::string>::value && !std::is_same<T, Void>::value)
          if (fill_gap != pbeast::FillGaps::None && m_data.size() > 0 && !to.empty())
            {
              uint32_t last_ts = to.back().get_ts();
              uint32_t distance = (ts - last_ts) / m_dt;

              if (distance > 1 && m_data[0][0].first < pbeast::mk_ts(ts))
                {
                  std::vector<DownsampleValue<T>> value;
                  value.reserve(m_data.size());

                  for (unsigned int i = 0; i < m_data.size(); ++i)
                    {
                      if (auto len = m_data[i].size())
                        {
                          unsigned int j = 0;
                          while (++j < m_data[i].size())
                            if (m_data[i][j].first >= pbeast::mk_ts(ts))
                              break;

                          value.emplace_back(m_data[i][j-1].second);
                        }
                    }

                  to.emplace_back(last_ts + m_dt, value);

                  if (distance > 2)
                    {
                      if (fill_gap == pbeast::FillGaps::Near)
                        to.emplace_back(ts - m_dt, value);
                      else
                        for (uint32_t x = last_ts + 2 * m_dt; x < ts; x += m_dt)
                          to.emplace_back(x, value);
                    }
                }
            }

        to.emplace_back(ts, m_value);
      }

      DownsampledSeriesVectorData<T>
      get_average()
      {
        for (unsigned int i = 0; i < m_data.size(); ++i)
          if (m_data[i].size() > 1)
            calculate_average(m_data[i], m_value[i]);

        return DownsampledSeriesVectorData<T>(pbeast::ts2secs(m_ts), m_value);
      }

    private:

      void
      extend_data(unsigned int val_len)
      {
        for (unsigned int i = std::min<unsigned int>(m_data.size(), val_len); i < val_len; ++i)
          m_data.emplace_back();
      }

      void
      truncate_data(unsigned int val_len)
      {
        if (m_data.size() > val_len)
          m_data.erase(m_data.begin() + val_len, m_data.begin() + m_data.size());
      }

      void
      store_value(const std::vector<DownsampleValue<T>>& value, uint64_t ts)
      {
        const unsigned int val_len(value.size());

        extend_data(val_len);

        for (unsigned int i = 0; i < val_len; ++i)
          m_data[i].emplace_back(ts, value[i].get_value());
      }

      void
      store_value(const std::vector<T>& value, uint64_t ts)
      {
        const unsigned int val_len(value.size());

        extend_data(val_len);

        for (unsigned int i = 0; i < val_len; ++i)
          m_data[i].emplace_back(ts, value[i]);
      }


    private:

      std::vector<DownsampleValue<T>> m_value;

      // store data by index
      std::vector<std::vector<std::pair<uint64_t,T>>> m_data;
    };


  template<class T>
    void
    downsample(uint32_t dt, const std::set<SeriesData<T> >& from, std::vector<DownsampledSeriesData<T> > & to, pbeast::FillGaps::Type fill_gaps)
    {
      if (from.empty())
        return;

      DownsampleData<T> last(dt);
      last.reset((*from.cbegin()).get_value(), (*from.cbegin()).get_ts_created(), false);

      for (auto& x : from)
        {
          if (last.check(x.get_value(), x.get_ts_created()))
            {
              last.put(to, fill_gaps);
              last.reset(x.get_value(), x.get_ts_created());
            }

          if (x.get_ts_last_updated() != x.get_ts_created() && last.check(x.get_value(), x.get_ts_last_updated()))
            {
              last.put(to, fill_gaps);
              last.reset(x.get_value(), x.get_ts_last_updated());
            }
        }

      last.put(to, fill_gaps);
    }

  template<class T>
    void
    downsample(uint32_t dt, const std::vector<SeriesData<T>>& from, std::vector<DownsampledSeriesData<T>>& to, uint32_t from_idx, uint32_t to_idx, pbeast::FillGaps::Type fill_gaps)
    {
      DownsampleData<T> last(dt);
      last.reset(from[from_idx].get_value(), from[from_idx].get_ts_created(), false);

      const uint64_t len = to.size();

      for (uint32_t i = from_idx; i <= to_idx; ++i)
        {
          auto& x(from[i]);

          // FIXME: add merge code if "to" was not empty

          if (last.check(x.get_value(), x.get_ts_created()))
            {
              last.put(to, fill_gaps, len);
              last.reset(x.get_value(), x.get_ts_created());
            }

          if (x.get_ts_last_updated() != x.get_ts_created() && last.check(x.get_value(), x.get_ts_last_updated()))
            {
              last.put(to, fill_gaps, len);
              last.reset(x.get_value(), x.get_ts_last_updated());
            }
        }

      last.put(to, fill_gaps, len);
    }

  template<class T>
    void
    downsample(uint32_t dt, const std::vector<SeriesData<T> >& from, std::vector<DownsampledSeriesData<T> > & to, pbeast::FillGaps::Type fill_gaps)
    {
      if (from.empty())
        return;
      downsample<T>(dt, from, to, 0, from.size() - 1, fill_gaps);
    }

  template<class T>
    void
    downsample(uint32_t dt, const std::set<SeriesVectorData<T> >& from, std::vector<DownsampledSeriesVectorData<T> > & to, pbeast::FillGaps::Type fill_gaps)
    {
      if (from.empty())
        return;

      DownsampleVectorData<T> last(dt);
      last.reset((*from.cbegin()).get_value(), (*from.cbegin()).get_ts_created(), false);

      for (auto& x : from)
        {
          if (last.check(x.get_value(), x.get_ts_created()))
            {
              last.put(to, fill_gaps);
              last.reset(x.get_value(), x.get_ts_created());
            }

          if (x.get_ts_last_updated() != x.get_ts_created() && last.check(x.get_value(), x.get_ts_last_updated()))
            {
              last.put(to, fill_gaps);
              last.reset(x.get_value(), x.get_ts_last_updated());
            }
        }

      last.put(to, fill_gaps);
    }

  template<class T>
    void
    downsample(uint32_t dt, const std::vector<SeriesVectorData<T> >& from, std::vector<DownsampledSeriesVectorData<T> > & to, uint32_t from_idx, uint32_t to_idx, pbeast::FillGaps::Type fill_gaps)
    {
      DownsampleVectorData<T> last(dt);
      last.reset(from[from_idx].get_value(), from[from_idx].get_ts_created(), false);

      const uint64_t len = to.size();

      for (uint32_t i = from_idx; i <= to_idx; ++i)
        {
          auto& x(from[i]);

          if (last.check(x.get_value(), x.get_ts_created()))
            {
              last.put(to, fill_gaps, len);
              last.reset(x.get_value(), x.get_ts_created());
            }

          if (x.get_ts_last_updated() != x.get_ts_created() && last.check(x.get_value(), x.get_ts_last_updated()))
            {
              last.put(to, fill_gaps, len);
              last.reset(x.get_value(), x.get_ts_last_updated());
            }
        }

      last.put(to, fill_gaps, len);
    }

  template<class T>
    void
    downsample(uint32_t dt, const std::vector<SeriesVectorData<T> >& from, std::vector<DownsampledSeriesVectorData<T> > & to, pbeast::FillGaps::Type fill_gaps)
    {
      if (from.empty())
        return;
      downsample<T>(dt, from, to, 0, from.size() - 1, fill_gaps);
    }

  template<class T>
    void
    downsample(uint32_t dt, const std::vector<DownsampledSeriesData<T> >& from, std::vector<DownsampledSeriesData<T> > & to, pbeast::FillGaps::Type fill_gaps)
    {
      if (from.empty())
        return;

      DownsampleData<T> last(dt);
      last.reset(from[0].data(), from[0].get_ts(), false);

      for (auto& x : from)
        {
          if (last.check(x.data(), x.get_ts()))
            {
              last.put(to, fill_gaps);
              last.reset(x.data(), x.get_ts());
            }
        }

      last.put(to, fill_gaps);
    }

  template<class T>
    void
    downsample(uint32_t dt, const std::set<DownsampledSeriesData<T> >& from, std::vector<DownsampledSeriesData<T> > & to, pbeast::FillGaps::Type fill_gaps)
    {
      if (unsigned int size = from.size())
        {
          DownsampleData<T> last(dt);
          last.reset((*from.begin()).data(), (*from.begin()).get_ts(), false);

          for (auto& x : from)
            {
              if (last.check(x.data(), x.get_ts()))
                {
                  last.put(to, fill_gaps);
                  last.reset(x.data(), x.get_ts());
                }
            }

          last.put(to, fill_gaps);
        }
    }

  template<class T>
    void
    downsample(uint32_t dt, const std::vector<DownsampledSeriesVectorData<T> >& from, std::vector<DownsampledSeriesVectorData<T> > & to, pbeast::FillGaps::Type fill_gaps)
    {
      if (from.empty())
        return;

      DownsampleVectorData<T> last(dt);
      last.reset(from[0].data(), from[0].get_ts(), false);

      for (auto& x : from)
        {
          if (last.check(x.data(), x.get_ts()))
            {
              last.put(to, fill_gaps);
              last.reset(x.data(), x.get_ts());
            }
        }

      last.put(to, fill_gaps);
    }

  template<class T>
    void
    downsample(uint32_t dt, const std::set<DownsampledSeriesVectorData<T> >& from, std::vector<DownsampledSeriesVectorData<T> > & to, pbeast::FillGaps::Type fill_gaps)
    {
      if (unsigned int size = from.size())
        {
          DownsampleVectorData<T> last(dt);
          last.reset((*from.begin()).data(), (*from.begin()).get_ts(), false);

          for (auto& x : from)
            {
              if (last.check(x.data(), x.get_ts()))
                {
                  last.put(to, fill_gaps);
                  last.reset(x.data(), x.get_ts());
                }
            }

          last.put(to, fill_gaps);
        }
    }

  template<>
    inline bool
    DownsampleData<Void>::check(const Void&, uint64_t ts)
    {
      return DownsampleDataBase::check(ts);
    }

  template<>
    inline bool
    DownsampleVectorData<Void>::check(const std::vector<Void>&, uint64_t ts)
    {
      return DownsampleDataBase::check(ts);
    }

  template<>
    inline bool
    DownsampleData<Void>::check(const DownsampleValue<Void>&, uint32_t ts)
    {
      return DownsampleDataBase::check(pbeast::mk_ts(ts));
    }

  template<>
    inline bool
    DownsampleVectorData<Void>::check(const std::vector<DownsampleValue<Void>>&, uint32_t ts)
    {
      return DownsampleDataBase::check(pbeast::mk_ts(ts));
    }

  template<class T, class K>
  void
  downsample(uint32_t dt, const std::map<std::string,std::set<T>>& from, std::map<std::string,std::set<K>>& to, pbeast::FillGaps::Type fill_gaps)
  {
    std::vector<K> v;
    for (const auto& x : from)
      {
        v.clear();
        downsample(dt, x.second, v, fill_gaps);
        to.emplace(std::piecewise_construct, std::forward_as_tuple(x.first), std::forward_as_tuple(v.begin(), v.end()));
      }
  }

  void
  downsample_void(uint32_t dt, const std::set<uint64_t>& from, std::set<uint32_t> & to);

  void
  downsample_void(uint32_t dt, const std::map<std::string, std::set<uint64_t>>& from, std::map<std::string, std::set<uint32_t>> & to);
}

#endif
