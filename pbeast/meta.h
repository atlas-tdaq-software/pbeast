#ifndef PBEAST_META_H
#define PBEAST_META_H

#include <filesystem>

#include <ers/ers.h>

#include <pbeast/data-type.h>
#include <pbeast/series-data.h>


namespace pbeast
{
  namespace receiver
  {
    class DataProvider;
  }


  /**
   * \brief Provides interface to meta information about classes and attributes of data stored on P-BEAST repository.
   *
   *  The class provides read and write access to the meta information.
   *  The write access interface is private and only is used by the P-BEAST service.
   *  The read access interface is available to anyone.
   *
   *  The meta information description uses P-BEAST data files with special names stored on the P-BEAST repository.
   *  For every attribute of a class there are two files:
   *  - types.meta stores information about attribute type
   *  - descriptions.meta stores information about attribute description
   *
   *  The meta information can to vary with time, for example if one modifies IS info description files.
   *  By this reason the description of meta information is presented by time series.
   *
   *  To get a meta information one needs to know P-BEAST repository path, partition and class names, \see Meta() constructor.
   *
   *  The method get_all_types() gets types of all attributes.
   *  The method get_all_descriptions() gets descriptions of all attributes.
   *
   *  The get_attribute_types() gets types of single attribute by name.
   *  The get_attribute_descriptions() gets descriptions of single attribute by name.
   *
   *  Above attribute type string encodes two attribute properties: the primitive type and the cardinality.
   *  To decode attribute type string use decode_type() method.
   *
   *  For nested attribute types one may need to extract class name and attribute name from the attribute path.
   *  This can be done using get_attribute_class_and_name() method.
   */


  class Meta
  {

  public:

    /**
     *  \brief The constructor reads meta information description for given class.
     *
     *  \param  repository_path  path to P-BEAST repository
     *  \param  partition_name   name of partition
     *  \param  class_name       name of class
     *
     *  \throw tdaq::pbeast::Exception in case of error
     */

    Meta(const std::filesystem::path& repository_path, const std::string& partition_name, const std::string& class_name);


    /**
     *  \brief The constructor reads or creates meta information description for given path to class.
     *
     *  \param  path  path to a class in P-BEAST repository
     *
     *  \throw tdaq::pbeast::Exception in case of error
     */

    Meta(const std::filesystem::path& path);


    /**
     *  \brief Get types of all attributes.
     *  \return map of attribute types (key: attribute name; value: time-serie of attribute type)
     */

    const std::map<std::string, std::vector<SeriesData<std::string>>>&
    get_all_types() const noexcept
    {
      return m_types;
    }


    /**
     *  \brief Get descriptions of all attributes.
     *  \return map of attribute descriptions (key: attribute name; value: time-serie of attribute description)
     */

    const std::map<std::string, std::vector<SeriesData<std::string>>>&
    get_all_descriptions() const noexcept
    {
      return m_descriptions;
    }

    /**
     *  \brief Get type of attribute.
     *
     *  \param  name  attribute name
     *
     *  \return time-serie of attribute type
     *
     *  \throw tdaq::pbeast::NoSuchAttribute if class has no attribute with such name
     */

    const std::vector<SeriesData<std::string>>&
    get_attribute_types(const std::string& name) const
    {
      return get_attribute_data(name, m_types);
    }


    /**
     *  \brief Get description of attribute.
     *
     *  \param  name  attribute name
     *
     *  \return time-serie of attribute description
     *
     *  \throw tdaq::pbeast::NoSuchAttribute if class has no attribute with such name
     */

    const std::vector<SeriesData<std::string>>&
    get_attribute_descriptions(const std::string& name) const
    {
      return get_attribute_data(name, m_descriptions);
    }


    /**
     *  \brief Parse raw attribute type.
     *
     *  A raw attribute type returned by the get_all_types() and get_attribute_types() methods
     *  encodes two attribute properties: the primitive type and the cardinality.
     *
     *  \param value   raw attribute type (input parameter)
     *  \param type    decoded primitive attribute type
     *  \param is_mv   decoded attribute cardinality (false => single-value; true => array )
     */

    static void
    decode_type(const std::string& value, std::string& type, bool& is_mv) noexcept;


    /**
     *  If attribute_path points to nested type, then the method extracts leave names of class and attribute from the path.
     *  For example for path "a/b/../x/y" the leave class name is "x" and the attribute name is "y".
     *  If the path is a name of attribute "z", then the leave class name is empty and the attribute name is "z".
     *
     *  /param attribute_path         path to attribute
     *  /param attribute_class_name   path's leave class name or empty
     *  /param attribute_name         path's leave attribute name
     */

    static void
    get_attribute_class_and_name(const std::filesystem::path& attribute_path, std::string& attribute_class_name, std::string& attribute_name);


    static void
    set_info_ts(uint64_t ts)
    {
      s_info_ts = ts;
    }

    static std::string
    encode_type(const std::string& type, bool is_mv);

    void
    commit(const std::string& partition, const std::string& class_name);

    void
    update_attribute(const std::string& name, const std::string& type, const std::string& description);

    void
    update_attribute(std::vector<pbeast::SeriesData<std::string>> & series, const std::string& value);

    // extend types and descriptions
    void
    merge(const Meta& info);

    static void
    check_and_merge_files(const std::filesystem::path& file_from, const std::filesystem::path& file_to);

    inline static const std::string s_types_meta_filename = "types.meta";
    inline static const std::string s_descriptions_meta_filename = "descriptions.meta";


  private:

    void
    initialise(const std::filesystem::path& path2class, const std::string& class_name);

    const std::vector<SeriesData<std::string>>&
    get_attribute_data(const std::string& name, const std::map<std::string, std::vector<SeriesData<std::string>>>& src) const;

  private:

    std::filesystem::path m_types_path;
    std::filesystem::path m_descriptions_path;
    std::map<std::string, std::vector<SeriesData<std::string>>> m_types;
    std::map<std::string, std::vector<SeriesData<std::string>>> m_descriptions;

    static uint64_t s_info_ts;

  };

}
#endif

