#ifndef PBEAST_DOWNSAMPLE_CONFIG_H
#define PBEAST_DOWNSAMPLE_CONFIG_H

#include <stdint.h>

#include <vector>
#include <filesystem>

namespace pbeast {

  class DataCompactionConfig
  {
    friend class Repository;

  public:

    /**
     *  Construct configuration with no downsampling.
     */

    DataCompactionConfig() :
        m_zip_catalog(false), m_zip_string_data(false), m_zip_integer_data(false), m_zip_float_data(false), m_zip_void_data(false), m_catalogize_strings(false)
    {
    }

    /**
     *  Initialize compression parameters of configuration.
     *  Use this method to change default ones.
     *
     *   \param zip_catalog         compress catalog (footer), i.e. information about names of objects and their indices
     *   \param zip_string_data     compress string data
     *   \param zip_integer_data    compress integer type data (boolean, 8, 16, 32 and 64 bits integers, date, time, enumeration)
     *   \param zip_float_data      compress floating type data (float, double)
     *   \param catalogize_strings  if data type is "string", they can be saved efficiently in case of repetitive data belonging to the same object
     */

    void
    init_zip(bool zip_catalog = true, bool zip_string_data = true, bool zip_integer_data = false, bool zip_float_data = false, bool zip_void_data = false, bool catalogize_strings = true)
    {
      m_zip_catalog = zip_catalog;
      m_zip_string_data = zip_string_data;
      m_zip_integer_data = zip_integer_data;
      m_zip_float_data = zip_float_data;
      m_zip_void_data = zip_void_data;
      m_catalogize_strings = catalogize_strings;
    }


    /**
     *  Detect if data of given P-BEAST type have to be compressed or not.
     *  \return true, if configuration described compression for given data type
     */

    bool
    zip_data(uint32_t type)
    {
      if (type <= pbeast::U64 || type == pbeast::ENUM || type == pbeast::DATE || type == pbeast::TIME)
        {
          return m_zip_integer_data;
        }
      else if (type == pbeast::FLOAT || type == pbeast::DOUBLE)
        {
          return m_zip_float_data;
        }
      else if (type == pbeast::VOID)
        {
          return m_zip_void_data;
        }
      else if (type == pbeast::STRING)
        {
          return m_zip_string_data;
        }
      else
        {
          return false;
        }
    }


    /**
     *  Compress catalog containing names of objects and their indices is compressed
     *  \return true, if the catalog is compressed
     */

    bool
    get_zip_catalog() const
    {
      return m_zip_catalog;
    }


    /**
     *  Compress string data
     *  \return true, if the string data are compressed
     */

    bool
    get_zip_string_data() const
    {
      return m_zip_string_data;
    }


    /**
     *  Compress integer type data (boolean, 8, 16, 32 and 64 bits integers, date, time, enumeration)
     *  \return true, if the integer type data are compressed
     */

    bool
    get_zip_integer_data() const
    {
      return m_zip_integer_data;
    }


    /**
     *  Compress floating type data (float, double)
     *  \return true, if the floating type data are compressed
     */

    bool
    get_zip_float_data() const
    {
      return m_zip_float_data;
    }


    /**
     *  Compress void type data (end-of-validity, update-info)
     *  \return true, if the void data are compressed
     */

    bool
    get_zip_void_data() const
    {
      return m_zip_void_data;
    }


    /**
     *  Save repetitive string data of the same object efficiently (every string is stored once in a catalog)
     *  \return true, if the string data are stored in catalog
     */

    bool
    get_catalogize_strings() const
    {
      return m_catalogize_strings;
    }

  private:

    bool m_zip_catalog;
    bool m_zip_string_data;
    bool m_zip_integer_data;
    bool m_zip_float_data;
    bool m_zip_void_data;
    bool m_catalogize_strings;

  };



  /**
   *  Class describes parameters for data files downsampling.
   *  The data are stored in cache directory using closest smaller sample interval.
   *  The data can be compressed or not.
   */

  class DownsampleConfig : public DataCompactionConfig
  {
    friend class Repository;

  public:

    /**
     *  Construct configuration with no downsampling.
     */

    DownsampleConfig() :
        m_cache_path(""), m_max_cache_size(0), m_sample_intervals(), m_downsample_thread_pool_size(0)
    {
    }


    /**
     *  Initialize configuration to use downsampling.
     *
     *   \param cache_path                   path to cache directory containing downsampled data
     *   \param downsample_thread_pool_size  number of downsample threads
     *   \param max_cache_size               maximum size of downsample cache (0 = unlimited)
     *   \param sample_intervals             array of sample intervals (in seconds); be default: 1 minute, 4 minutes, 15 minutes, 1 hour, 5 hours, 1 day
     */

    void
    init(const std::filesystem::path& cache_path, uint32_t downsample_thread_pool_size = 1, uint64_t max_cache_size = 0, const std::vector<uint64_t>& sample_intervals = { 60, 4 * 60, 15 * 60, 60 * 60, 5 * 60 * 60, 24 * 60 * 60 })
    {
      m_max_cache_size = max_cache_size;
      m_cache_path = cache_path;
      m_sample_intervals = sample_intervals;
      m_downsample_thread_pool_size = downsample_thread_pool_size;
      init_zip();
    }


    /**
     *  Get path to cache directory containing downsampled files.
     *  \return path to cache directory.
     */

    const std::filesystem::path&
    get_cache_path() const
    {
      return m_cache_path;
    }


    /**
     *  Get maximum cache directory size.
     *  Is unlimited if 0.
     *  \return maximum cache size
     */

    uint64_t
    get_max_cache_size() const
    {
      return m_max_cache_size;
    }


    /**
     *  Get standard sample intervals (in seconds)
     *  \return sample intervals
     */

    const std::vector<uint64_t>&
    get_sample_intervals()
    {
      return m_sample_intervals;
    }


    /**
     *  Get size of downsample pool.
     *  \return number of downsample threads
     */

    uint32_t
    get_downsample_thread_pool_size() const
    {
      return m_downsample_thread_pool_size;
    }



  private:

    std::filesystem::path m_cache_path;
    uint64_t m_max_cache_size;
    std::vector<uint64_t> m_sample_intervals;
    uint32_t m_downsample_thread_pool_size;

  };
}


#endif
