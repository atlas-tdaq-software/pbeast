#ifndef PBEAST_DOWNSAMPLE_FILL_GAPS_H
#define PBEAST_DOWNSAMPLE_FILL_GAPS_H

#include <string>

namespace pbeast
{
  namespace FillGaps
  {
    /**
     *  When downsample data are calculated, missing data can be approximated.
     */

    enum Type
    {
      None, /*!< do not add any approximated data in the sparse intervals */
      Near, /*!< add approximated data to previous and next intervals, if they are empty */
      All /*!< add approximated data to all sparse intervals */
    };


    /**
     *  Construct enum type from string.
     */

    Type
    mk(const std::string &value);


    /**
     *  Convert type to string.
     */

    const std::string&
    str(Type type);
 }
}

#endif
