#ifndef PBEAST_FILE_RESPONSE_H
#define PBEAST_FILE_RESPONSE_H

#include <sstream>
#include <vector>

namespace pbeast
{

  struct DataFileResponse
  {
    inline DataFileResponse(std::ios_base::openmode mode) :
        m_started(0), m_age(0), m_data(mode)
    {
      ;
    }

    inline void
    put(std::stringstream& s)
    {
      s << m_data.str().size() << ' ';
    }

    inline void
    reserve(uint64_t size)
    {
      m_buffer.reserve(size);
    }

    uint64_t m_started;
    uint64_t m_age;
    std::stringstream m_data;
    std::string m_buffer;
  };


  struct MultiDataFileResponse
  {
    // space allocated for first number defining size of header
    enum
    {
      num_width = 8
    };

    inline
    MultiDataFileResponse(std::ios_base::openmode mode) :
        m_eov(mode), m_upd(mode)
    {
      ;
    }

    virtual inline
    ~MultiDataFileResponse()
    {
      for (auto & x : m_data)
        delete x;
    }

    std::vector<DataFileResponse *> m_data;
    DataFileResponse m_eov;
    DataFileResponse m_upd;
  };

}

#endif
