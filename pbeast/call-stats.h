#ifndef PBEAST_CALL_STATS_H
#define PBEAST_CALL_STATS_H

#include <ostream>

namespace pbeast
{
  struct CallStats
  {
    uint32_t m_num_of_data_files;
    uint32_t m_num_of_void_files;
    uint32_t m_num_of_downsampled_files;
    uint32_t m_num_of_info_series_cache_hits;
    uint64_t m_size_of_downsampled_files;
    uint64_t m_size_of_read_headers;
    uint64_t m_size_of_info_series_cache_hits;
    uint64_t m_num_of_primitive_data_reads;
    uint64_t m_num_of_void_timestamps_reads;

    uint64_t m_objects_num;
    uint64_t m_data_point_num;
    uint64_t m_data_size;

    const uint64_t m_max_data_size;

    CallStats(uint64_t max_data_size = 0) : m_num_of_data_files(0), m_num_of_void_files(0), m_num_of_downsampled_files(0), m_num_of_info_series_cache_hits(0), m_size_of_downsampled_files(0), m_size_of_read_headers(0), m_size_of_info_series_cache_hits(0), m_num_of_primitive_data_reads(0), m_num_of_void_timestamps_reads(0), m_objects_num(0), m_data_point_num(0), m_data_size(0), m_max_data_size(max_data_size)
    {
      ;
    }

    void
    check_size() const;

    void
    print(std::ostream& s);
  };
}

#endif
