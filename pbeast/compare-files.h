#ifndef PBEAST_COMPARE_FILES_H
#define PBEAST_COMPARE_FILES_H

#include <pbeast/exceptions.h>
#include <pbeast/input-data-file.h>

namespace daq
{
  /*! \class pbeast::FileCompareFailed
   *  This issue is reported when files cannot be compared
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileCompareFailed,
    Exception,
    "failed to compare " << file1 << " and " << file2,
    ERS_EMPTY,
    ((std::filesystem::path)file1)
    ((std::filesystem::path)file2)
  )

  /*! \class pbeast::FilesAreDifferent
   *  This issue is reported when files are different (e.g. after copy)
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FilesAreDifferent,
    Exception,
    "files " << file1 << " and " << file2 << " are different: " << why,
    ERS_EMPTY,
    ((std::filesystem::path)file1)
    ((std::filesystem::path)file2)
    ((const char *)why)
  )
}

namespace pbeast
{

  /**
   *  \brief Compare content of two pbeast files
   *
   *  Parameters:
   *  \param f1       input data file stream
   *  \param f2       output data file stream
   *
   *  \throw exception if files are different
   */

  void
  compare_files(const std::string &f1, const std::string &f2);
}


#endif
