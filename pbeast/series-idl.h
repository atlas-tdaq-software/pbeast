#ifndef PBEAST_SERIES_IDL_H
#define PBEAST_SERIES_IDL_H

#include <map>
#include <set>
#include <vector>

#include <pbeast/series-data.h>
#include <pbeast/pbeast.hh>


namespace pbeast
{

  template<typename F, typename T>
    void
    convert(const F& from, std::map<std::string,std::vector<pbeast::SeriesVectorData<T>>>& to)
  {
    const unsigned int len = from.length();
    for(unsigned int i = 0; i < len; ++i)
      {
        std::string object_id(from[i].object_id);
        std::vector<pbeast::SeriesVectorData<T>>& vec = to[object_id];

        unsigned int vlen = from[i].data.length();
        vec.reserve(vlen);
        for(unsigned int idx = 0; idx < vlen; ++idx)
          {
            std::vector<T> data_vec;
            unsigned int data_vect_len = from[i].data[idx].data.length();
            data_vec.reserve(data_vect_len);

            for(unsigned int j = 0; j < data_vect_len; ++j)
              {
                data_vec.push_back(static_cast<T>(from[i].data[idx].data[j]));
              }

            vec.emplace_back(from[i].data[idx].ts.created, from[i].data[idx].ts.updated, data_vec);
          }
      }
  }

  template<typename F, typename T>
    void
    convert(const F& from, std::map<std::string,std::set<pbeast::SeriesVectorData<T>>>& to, const char * prefix)
  {
    const unsigned int len = from.length();
    for (unsigned int i = 0; i < len; ++i)
      {
        std::string object_id(prefix);
        object_id.append(from[i].object_id);
        std::set<pbeast::SeriesVectorData<T>>& v_set = to[object_id];
        typename std::set<pbeast::SeriesVectorData<T>>::const_iterator hint = v_set.end();

        unsigned int vlen = from[i].data.length();
        for (unsigned int idx = 0; idx < vlen; ++idx)
          {
            std::vector<T> data_vec;
            const unsigned int data_vect_len = from[i].data[idx].data.length();
            data_vec.reserve(data_vect_len);

            for (unsigned int j = 0; j < data_vect_len; ++j)
              {
                data_vec.push_back(static_cast<T>(from[i].data[idx].data[j]));
              }

            hint = v_set.emplace_hint(hint, from[i].data[idx].ts.created, from[i].data[idx].ts.updated, data_vec);
          }
      }
  }

  template<typename F, typename T>
    void
    convert(const F& from, std::map<std::string,std::vector<pbeast::SeriesData<T>>>& to)
  {
    const unsigned int len = from.length();
    for(unsigned int i = 0; i < len; ++i)
      {
        std::string object_id(from[i].object_id);
        std::vector<pbeast::SeriesData<T>>& vec = to[object_id];

        unsigned int vlen = from[i].data.length();
        vec.reserve(vlen);
        for(unsigned int idx = 0; idx < vlen; ++idx)
          {
            vec.emplace_back(from[i].data[idx].ts.created, from[i].data[idx].ts.updated, static_cast<T>(from[i].data[idx].data));
          }
      }
  }

  template<typename F, typename T>
    void
    convert(const F& from, std::map<std::string,std::set<pbeast::SeriesData<T>>>& to, const char * prefix)
  {
    const unsigned int len = from.length();
    for (unsigned int i = 0; i < len; ++i)
      {
        std::string object_id(prefix);
        object_id.append(from[i].object_id);
        std::set<pbeast::SeriesData<T>>& v_set = to[object_id];
        typename std::set<pbeast::SeriesData<T>>::const_iterator hint = v_set.end();

        const unsigned int vlen = from[i].data.length();
        for (unsigned int idx = 0; idx < vlen; ++idx)
          {
            hint = v_set.emplace_hint(hint, from[i].data[idx].ts.created, from[i].data[idx].ts.updated, static_cast<T>(from[i].data[idx].data));
          }
      }
  }

  void
  cvt_void(const pbeast::V64ObjectList& from, std::map<std::string, std::set<uint64_t>>& to, const char * prefix);

  void
  cvt_void(const pbeast::V32ObjectList& from, std::map<std::string, std::set<uint32_t>>& to, const char * prefix);



  // convert [and merge]
  template<typename F, typename T>
    void
    convert(const F& from, std::map<std::string, std::set<pbeast::DownsampledSeriesData<T>>>& to, const char * prefix)
      {
        const unsigned int len = from.length();
        for(unsigned int i = 0; i < len; ++i)
          {
            std::string object_id(prefix);
            object_id.append(from[i].object_id);
            std::set<pbeast::DownsampledSeriesData<T>>& set = to[object_id];
            typename std::set<pbeast::DownsampledSeriesData<T>>::const_iterator hint = set.end();

            for(unsigned int idx = 0; idx < from[i].data.length(); ++idx)
              {
                DownsampledSeriesData<T> v(from[i].data[idx].ts, static_cast<T>(from[i].data[idx].data.min), static_cast<typename pbeast::AverageType<T>::average_t>(from[i].data[idx].data.val), static_cast<T>(from[i].data[idx].data.max));

                std::size_t old_size = set.size();
                hint = set.insert(hint, v);

                if (old_size == set.size())
                  {
                    const pbeast::DownsampledSeriesData<T>& old_value(*hint);
                    v.merge(old_value);
                    set.erase(hint);
                    hint = set.insert(v).first;
                  }
              }
          }
      }

  // convert [no merge]
  template<typename F, typename T>
    void
    convert(const F& from, std::map<std::string, std::vector<pbeast::DownsampledSeriesData<T>>>& to)
      {
        const unsigned int len = from.length();
        for(unsigned int i = 0; i < len; ++i)
          {
            std::string object_id(from[i].object_id);
            std::vector<pbeast::DownsampledSeriesData<T>>& vec = to[object_id];

            unsigned int vlen = from[i].data.length();
            vec.reserve(vlen);

            for(unsigned int idx = 0; idx < vlen; ++idx)
              {
                vec.emplace_back(from[i].data[idx].ts, static_cast<T>(from[i].data[idx].data.min), static_cast<typename pbeast::AverageType<T>::average_t>(from[i].data[idx].data.val), static_cast<T>(from[i].data[idx].data.max));
              }
          }
      }


  // convert [and merge]
  template<typename F, typename T>
    void
    convert(const F& from, std::map<std::string, std::set<pbeast::DownsampledSeriesVectorData<T>>>&to, const char * prefix)
    {
      const unsigned int len = from.length();
      for(unsigned int i = 0; i < len; ++i)
        {
          std::string object_id(prefix);
          object_id.append(from[i].object_id);
          std::set<pbeast::DownsampledSeriesVectorData<T>>& set = to[object_id];
          typename std::set<pbeast::DownsampledSeriesVectorData<T>>::const_iterator hint = set.end();

          for (unsigned int idx = 0; idx < from[i].data.length(); ++idx)
            {
              std::vector<DownsampleValue<T>> vec_val;
              vec_val.reserve(from[i].data[idx].data.length());
              for (unsigned int j = 0; j < from[i].data[idx].data.length(); ++j)
                {
                  vec_val.emplace_back(static_cast<T>(from[i].data[idx].data[j].min), static_cast<typename pbeast::AverageType<T>::average_t>(from[i].data[idx].data[j].val), static_cast<T>(from[i].data[idx].data[j].max));
                }

              DownsampledSeriesVectorData<T> v(from[i].data[idx].ts, vec_val);

              std::size_t old_size = set.size();
              hint = set.insert(hint, v);

              if (old_size == set.size())
                {
                  const pbeast::DownsampledSeriesVectorData<T>& old_value(*hint);
                  v.merge(old_value);
                  set.erase(hint);
                  hint = set.insert(v).first;
                }
            }
        }
    }


  // convert [no merge]
  template<typename F, typename T>
    void
    convert(const F& from, std::map<std::string, std::vector<pbeast::DownsampledSeriesVectorData<T>>>&to)
    {
      const unsigned int len = from.length();
      for(unsigned int i = 0; i < len; ++i)
        {
          std::string object_id(from[i].object_id);
          std::vector<pbeast::DownsampledSeriesVectorData<T>>& vec = to[object_id];

          unsigned int vlen = from[i].data.length();
          vec.reserve(vlen);

          for(unsigned int idx = 0; idx < vlen; ++idx)
            {
              std::vector<DownsampleValue<T>> vec_val;
              vec_val.reserve(from[i].data[idx].data.length());
              for (unsigned int j = 0; j < from[i].data[idx].data.length(); ++j)
                {
                  vec_val.emplace_back(static_cast<T>(from[i].data[idx].data[j].min), static_cast<typename pbeast::AverageType<T>::average_t>(from[i].data[idx].data[j].val), static_cast<T>(from[i].data[idx].data[j].max));
                }

              vec.emplace_back(from[i].data[idx].ts, vec_val);
            }
        }
    }

}



#endif
