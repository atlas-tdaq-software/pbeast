#ifndef PBEAST_EXCEPTIONS_H
#define PBEAST_EXCEPTIONS_H

#include <string>
#include <filesystem>
#include <ers/ers.h>

namespace daq
{
  /*! \class pbeast::Exception
   *  This is a base class for all P-BEAST exceptions.
   */
  ERS_DECLARE_ISSUE(pbeast, Exception, ERS_EMPTY, ERS_EMPTY)

  /*! \class pbeast::FileSystemPathDoesNotExist
   *  This issue is reported when some file system path does not exist
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileSystemPathDoesNotExist,
    Exception,
    "file system path " << path << " does not exist",
    ERS_EMPTY,
    ((std::filesystem::path)path)
  )

  /*! \class pbeast::FileSystemPathIsNotDirectory
   *  This issue is reported when some file system path is not a directory as expected
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileSystemPathIsNotDirectory,
    Exception,
    "file system path " << path << " is not a directory",
    ERS_EMPTY,
    ((std::filesystem::path)path)
  )

  /*! \class pbeast::FileSystemAccessError
   *  This issue is reported when some operation with file system fails
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileSystemAccessError,
    Exception,
    "file system access error: " << why,
    ERS_EMPTY,
    ((std::string)why)
  )

  /*! \class pbeast::FileSystemError
   *  This issue is reported when some operation with file system fails
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileSystemError,
    Exception,
    "cannot " << op << ' ' << path << ": " << why,
    ERS_EMPTY,
    ((const char *)op)
    ((std::filesystem::path)path)
    ((std::string)why)
  )

  /*! \class pbeast::FileSystemOperationError
   *  This issue is reported when some operation with file fails
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileSystemOperationError,
    Exception,
    "cannot " << op << ' ' << path1 << " to " << path2 << ": " << why,
    ERS_EMPTY,
    ((const char *)op)
    ((std::filesystem::path)path1)
    ((std::filesystem::path)path2)
    ((std::string)why)
  )


  /*! \class pbeast::FileError
   *  This issue is reported when some operation with file fails
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileError,
    Exception,
    "cannot " << op << " file " << path,
    ERS_EMPTY,
    ((const char *)op)
    ((std::filesystem::path)path)
  )

  /*! \class pbeast::NoSuchClass
   *  This issue is reported when information about class is not found
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    NoSuchClass,
    Exception,
    "cannot find meta information for class \"" << name << '\"',
    ERS_EMPTY,
    ((std::string)name)
  )

  /*! \class pbeast::NoSuchAttribute
   *  This issue is reported when information about attribute is not found
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    NoSuchAttribute,
    Exception,
    "cannot find meta information for attribute \"" << name << '\"',
    ERS_EMPTY,
    ((std::string)name)
  )

  /*! \class pbeast::NoAttributeDirectory
   *  This issue is reported when directory for attribute is not found
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    NoAttributeDirectory,
    Exception,
    "cannot find directory for attribute \"" << attribute_path << '@' << class_name << '@' << partition_name << "\" (path " << dir << " does not exist)",
    ERS_EMPTY,
    ((std::string)attribute_path)
    ((std::string)class_name)
    ((std::string)partition_name)
    ((std::filesystem::path)dir)
  )

  /*! \class pbeast::BadParameter
   *  This issue is reported when user provides bad partition, class or attribute path
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    BadParameter,
    Exception,
    "bad " << parameter << " = \"" << value << '\"',
    ERS_EMPTY,
    ((const char *)parameter)
    ((std::string)value)
  )

  /*! \class pbeast::RegularExpressionError
   *  This issue is reported when there is a problem with regular expression
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    RegularExpressionError,
    Exception,
    op << " \"" << data << '\"',
    ERS_EMPTY,
    ((const char *)op)
    ((std::string)data)
  )

  /*! \class pbeast::MissingObjectNameSeparator
   *  This issue is reported when there is a problem with nested object name format
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    MissingNestedObjectNameSeparator,
    Exception,
    "cannot find separator \'" << sep << "\' in object name \'" << id << '\'',
    ERS_EMPTY,
    ((std::string)sep)
    ((std::string)id)
  )

  /*! \class pbeast::FileTypeError
   *  This issue is reported when data are read from file using inappropriate methods
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    FileTypeError,
    Exception,
    "data type of file \"" << path << "\" is \"" << real_type << (real_type_is_array ? " array" : "") << "\" instead of \"" << expected_type << (expected_type_is_array ? " array" : "") << '\"',
    ,
    ((std::string)path)
    ((std::string)real_type)
    ((bool)real_type_is_array)
    ((std::string)expected_type)
    ((bool)expected_type_is_array)
  )

  /*! \class pbeast::MaxMessageSizeError
   *  This issue is reported when estimated in-memory (network) size of data (message) exceeds a limit
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    MaxMessageSizeError,
    Exception,
    "size of read data exceeds the allowable limit (" << size << " bytes)",
    ERS_EMPTY,
    ((uint64_t)size)
  )
}

#endif
