#ifndef PBEAST_OBJECT_NAME_H
#define PBEAST_OBJECT_NAME_H

#include <filesystem>
#include <string>

namespace pbeast
{
  inline std::string
  get_object_name(const std::string& s)
  {
    return s.substr(0, s.find(std::filesystem::path::preferred_separator));
  }

  inline std::string
  get_object_name(const std::string& s, const std::string& separator)
  {
    return s.substr(0, s.find(separator));
  }

  template<typename T>
  typename T::const_iterator
  find(const T& container, const std::string& name)
  {
    typename T::const_iterator it = container.find(name);

    if (it == container.end())
      it = container.find(get_object_name(name));

    return it;
  }

  template<typename T>
  typename T::iterator
  find(T& container, const std::string& name)
  {
    typename T::iterator it = container.find(name);

    if (it == container.end())
      it = container.find(get_object_name(name));

    return it;
  }
}

#endif
