#ifndef PBEAST_DATA_TYPE_H
#define PBEAST_DATA_TYPE_H

#include <stdint.h>
#include <unistd.h>

#include <ostream>
#include <type_traits>
#include <vector>

namespace pbeast {

  /**
   *  This class "Void" is used to store EOV, that has no values.
   *  Note, every EOV should be stored differently, so operator!= returns TRUE.
   */

  class Void
  {
  public:
    Void() { ; }

    bool operator!= (const Void &) const { return true; }
    bool operator== (const Void &) const { return false; }
  };
  std::ostream& operator<<(std::ostream& s, const Void&);


  enum DataType {
    BOOL   = 0,
    S8     = 1,
    U8     = 2,
    S16    = 3,
    U16    = 4,
    S32    = 5,
    U32    = 6,
    S64    = 7,
    U64    = 8,
    FLOAT  = 9,
    DOUBLE = 10,
    ENUM   = 11,
    DATE   = 12,
    TIME   = 13,
    STRING = 14,
    OBJECT = 15,
    VOID   = 16
  };

  const uint16_t DataTypeSize[] = {
    sizeof(bool),
    sizeof(int8_t),
    sizeof(uint8_t),
    sizeof(int16_t),
    sizeof(uint16_t),
    sizeof(int32_t),
    sizeof(uint32_t),
    sizeof(int64_t),
    sizeof(uint64_t),
    sizeof(float),
    sizeof(double),
    sizeof(int32_t),
    sizeof(uint32_t),
    sizeof(uint64_t),
    sizeof(void*),
    sizeof(void*),
    0
  };

  inline uint16_t get_size(DataType t, bool is_array) {
    return ( (is_array == false) ? DataTypeSize[static_cast<uint16_t>(t)] : sizeof(void*) );
  }

  extern const char * s_names_of_types[];

  inline const char * as_string(DataType t) {
    return s_names_of_types[static_cast<int>(t)];
  }

  pbeast::DataType str2type(const std::string & s);

  inline bool
  are_compartible(DataType t1, DataType t2)
  {
    return (
      (t1 == t2) ||
      (t1 == DATE && t2 == U32) ||
      (t2 == DATE && t1 == U32) ||
      (t1 == TIME && t2 == U64) ||
      (t2 == TIME && t1 == U64) ||
      (t1 == ENUM && t2 == S32) ||
      (t2 == ENUM && t1 == S32)
    );
  }

  // c++ type to pbeast data type

  template<class T>
    class CType
    {

    public:

      static DataType
      type();

    };

  template <> inline DataType CType<bool>::type( ) { return BOOL; }
  template <> inline DataType CType<int8_t>::type( ) { return S8; }
  template <> inline DataType CType<uint8_t>::type( ) { return U8; }
  template <> inline DataType CType<int16_t>::type( ) { return S16; }
  template <> inline DataType CType<uint16_t>::type( ) { return U16; }
  template <> inline DataType CType<int32_t>::type( ) { return S32; }
  template <> inline DataType CType<uint32_t>::type( ) { return U32; }
  template <> inline DataType CType<int64_t>::type( ) { return S64; }
  template <> inline DataType CType<uint64_t>::type( ) { return U64; }
  template <> inline DataType CType<float>::type( ) { return FLOAT; }
  template <> inline DataType CType<double>::type( ) { return DOUBLE; }
  template <> inline DataType CType<std::string>::type( ) { return STRING; }
  template <> inline DataType CType<Void>::type( ) { return VOID; }

#ifndef SWIGJAVA
  /**
   *  Helper to detect STL vector (see https://stackoverflow.com/questions/9407367)
   */

  namespace is_vector_impl
  {
    template <typename T> struct is_vector : std::false_type {};
    template <typename... Args> struct is_vector<std::vector<Args... >> : std::true_type {};
  }

  /**
   *  Type traits to detect STL vector as well as decay the type
   */

  template<typename T>
    struct is_vector
    {
      static constexpr bool const value = is_vector_impl::is_vector<std::decay_t<T>>::value;
    };

  /**
   *  Average all primitive types to double, but:
   *   - strings to string used most often
   *   - Voids to Voids
   *   - vectors to vectors of above types
   */

  template<typename T>
    struct AverageType
    {
      typedef typename std::conditional<is_vector<T>::value,std::vector<double>,double>::type average_t;
    };
#else
  template<typename T>
    struct AverageType
    {
      typedef double average_t;
    };

  // This works fine but SWIG does not really understand it
  // That's why we have the macro defined for explicit types
  template<typename T>
    struct AverageType<std::vector<T>>
    {
      typedef std::vector<double> average_t;
    };

#define SPECIALIZE_AVERAGETYPE_VECTOR(type) \
  template<> \
  struct AverageType<std::vector<type>> \
  { \
    typedef std::vector<double> average_t; \
  }; \

  SPECIALIZE_AVERAGETYPE_VECTOR(int8_t)
  SPECIALIZE_AVERAGETYPE_VECTOR(uint8_t)
  SPECIALIZE_AVERAGETYPE_VECTOR(int16_t)
  SPECIALIZE_AVERAGETYPE_VECTOR(uint16_t)
  SPECIALIZE_AVERAGETYPE_VECTOR(int32_t)
  SPECIALIZE_AVERAGETYPE_VECTOR(uint32_t)
  SPECIALIZE_AVERAGETYPE_VECTOR(int64_t)
  SPECIALIZE_AVERAGETYPE_VECTOR(uint64_t)
  SPECIALIZE_AVERAGETYPE_VECTOR(double)
  SPECIALIZE_AVERAGETYPE_VECTOR(float)
  SPECIALIZE_AVERAGETYPE_VECTOR(bool)
#endif

  template<>
    struct AverageType<std::string>
    {
      typedef std::string average_t;
    };

  template<>
    struct AverageType<std::vector<std::string>>
    {
      typedef std::vector<std::string> average_t;
    };

  template<>
    struct AverageType<Void>
    {
      typedef Void average_t;
    };

  template<>
    struct AverageType<std::vector<Void>>
    {
      typedef std::vector<Void> average_t;
    };

}


#endif
