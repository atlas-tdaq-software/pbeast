#ifndef PBEAST_SERIES_DATA_H
#define PBEAST_SERIES_DATA_H

#include <stdint.h>
#include <cmath>
#include <cctype>
#include <limits>
#include <vector>

#include <iostream>

#include <pbeast/data-type.h>
#include <pbeast/functions.h>

namespace pbeast
{
  inline constexpr uint64_t
  mk_ts(uint32_t secs)
  {
    return static_cast<uint64_t>(secs) * 1000000L;
  }

  inline constexpr uint64_t
  mk_ts(uint32_t secs, uint32_t mksecs)
  {
    return mk_ts(secs) + static_cast<uint64_t>(mksecs);
  }


  const uint64_t min_ts32_const(0);                         /// minimum timestamp in seconds
  const uint64_t max_ts32_const(0xffffffff);                /// maximum timestamp in seconds

  constexpr uint64_t def_lookup_prev_const32(60);           /// default number of seconds to lookup closest previous data point outside query time interval
  constexpr uint64_t def_lookup_next_const32(60);           /// default number of seconds to lookup closest next data point outside query time interval

  constexpr uint64_t min_ts_const = mk_ts(min_ts32_const);
  constexpr uint64_t max_ts_const = mk_ts(max_ts32_const);

  constexpr uint64_t def_lookup_prev_const(mk_ts(def_lookup_prev_const32));
  constexpr uint64_t def_lookup_next_const(mk_ts(def_lookup_next_const32));

  inline uint64_t
  mk_wide_since(uint64_t since, uint64_t delta)
  {
    return (since != pbeast::min_ts_const ? (since - delta) : since);
  }

  inline uint64_t
  mk_wide_until(uint64_t until, uint64_t delta)
  {
    return (until < pbeast::max_ts_const ? (until + delta) : until);
  }


  inline uint32_t
  ts2secs(uint64_t ts)
  {
    return static_cast<uint32_t>(ts / (uint64_t) 1000000L);
  }

  inline uint32_t
  ts2mksecs(uint64_t ts)
  {
    return static_cast<uint32_t>(ts % (uint64_t) 1000000L);
  }

  template<typename T>
    bool
    is_good(const T& v)
    {
      return true;
    }

  template<>
    inline bool
    is_good(const std::string &v)
    {
      for (const auto &x : v)
        if (!(std::isprint(x) || std::isspace(x)))
          return false;

      return true;
    }

  template<>
    inline bool
    is_good(const float &v)
    {
      return !std::isnan(v);
    }

  template<>
    inline bool
    is_good(const double &v)
    {
      return !std::isnan(v);
    }

  // compare values
  // floating point numbers requires special treatments because of NaNs (nan != nan)
  // pbeast::Void type requires special treatments because of pbeast::Void != pbeast::Void
  template<typename T>
    bool
    is_equal(const T& v1, const T& v2)
    {
      return (v1 == v2);
    }

  template<>
    inline bool
    is_equal(const pbeast::Void &v1, const pbeast::Void &v2)
    {
      return true;
    }

  template<>
    inline bool
    is_equal(const float &v1, const float &v2)
    {
      return (v1 == v2 || (std::isnan(v1) && std::isnan(v2)));
    }

  template<>
    inline bool
    is_equal(const double &v1, const double &v2)
    {
      return (v1 == v2 || (std::isnan(v1) && std::isnan(v2)));
    }

  template<>
    inline bool
    is_equal(const std::vector<float> &v1, const std::vector<float> &v2)
    {
      if (v1.size() != v2.size())
        return false;

      for (std::size_t i = 0; i < v1.size(); ++i)
        if (is_equal(v1[i], v2[i]) == false)
          return false;

      return true;
    }

  template<>
    inline bool
    is_equal(const std::vector<double> &v1, const std::vector<double> &v2)
    {
      if (v1.size() != v2.size())
        return false;

      for (std::size_t i = 0; i < v1.size(); ++i)
        if (is_equal(v1[i], v2[i]) == false)
          return false;

      return true;
    }

  class SeriesDataBase
  {

  public:

    SeriesDataBase(uint64_t ts_created, const uint64_t ts_last_updated) :
        m_ts_created(ts_created), m_ts_last_updated(ts_last_updated)
    {
      ;
    }

    bool operator<(const SeriesDataBase & s) const
    {
      return (m_ts_created < s.m_ts_created);
    }

    uint64_t
    get_ts_created() const
    {
      return m_ts_created;
    }

    uint64_t
    get_ts_last_updated() const
    {
      return m_ts_last_updated;
    }

    void
    set_ts_last_updated(uint64_t v)
    {
      m_ts_last_updated = v;
    }

    void
    set_ts_created(uint64_t v)
    {
      m_ts_created = v;
    }

    bool
    operator==(const SeriesDataBase &d) const
    {
      return (m_ts_created == d.m_ts_created && m_ts_last_updated == d.m_ts_last_updated);
    }


  protected:

    uint64_t m_ts_created;
    uint64_t m_ts_last_updated;

  };


  template<typename Type> class DownsampledSeriesData;
  template<typename Type> class DownsampledSeriesVectorData;


  template<class T>
    class SeriesData : public SeriesDataBase, public CType<T>
    {

    public:

      SeriesData(uint64_t ts_created, uint64_t ts_last_update, const T& v) :
          SeriesDataBase(ts_created, ts_last_update), m_value(v)
      {
        ;
      }

      bool
      operator==(const SeriesData &d) const
      {
        return (SeriesDataBase::operator==(d) && is_equal(m_value,d.m_value));
      }

      const T&
      get_value() const
      {
        return m_value;
      }

      static bool
      is_array()
      {
        return false;
      }

      /// used to calculate size of data in-memory
      uint64_t
      get_data_size() const
      {
        return (sizeof(SeriesDataBase) + sizeof(T));
      }

      void
      print_json_value(std::ostringstream& ss) const;

      void
      sum_array_elements()
      {
        throw std::runtime_error("sum_array_elements() was called on single-value raw data");
      }

      /// Check that value is less than parameter; return true for NaN value
      bool
      is_less(const T& v) const
      {
        return !(m_value > v);
      }

      /// Check that value is greater than parameter; return true for NaN value
      bool
      is_greater(const T& v) const
      {
        return !(m_value < v);
      }

      /// Check that value is corrupted (a NaN for floating point or has nonprintable characters)
      bool
      is_corrupted() const
      {
        return !is_good(m_value);
      }

      typedef T data_type;
      typedef T basic_type;
      typedef DownsampledSeriesData<T> ds_type;

    private:

      T m_value;

    };


  /// size of string data point in-memory
  template<>
    inline uint64_t
    SeriesData<std::string>::get_data_size() const
    {
      return (sizeof(SeriesDataBase) + sizeof(uint64_t) + m_value.size());
    }

//  // default pbeast::Void::operator== always return false
//  template<>
//    inline bool
//    SeriesData<pbeast::Void>::operator==(const SeriesData &d) const
//    {
//      return (SeriesDataBase::operator==(d));
//    }

  template<class T>
    class SeriesVectorData : public SeriesDataBase, public CType<T>
    {

    public:

      SeriesVectorData(uint64_t ts_created, uint64_t ts_last_update, const std::vector<T>& v) :
          SeriesDataBase(ts_created, ts_last_update), m_value(v)
      {
        ;
      }

      bool
      operator==(const SeriesVectorData &d) const
      {
        return (SeriesDataBase::operator==(d) && is_equal(m_value,d.m_value));
      }

      const std::vector<T>&
      get_value() const
      {
        return m_value;
      }

      inline unsigned int
      get_size() const
      {
        return m_value.size();
      }

      static bool
      is_array()
      {
        return true;
      }

      /// used to calculate size of data in-memory
      uint64_t
      get_data_size() const
      {
        return (sizeof(SeriesDataBase) + sizeof(uint64_t) + sizeof(T) * m_value.size());
      }

      void
      print_json_value(std::ostringstream& ss) const;

      void
      sum_array_elements()
      {
        const unsigned int len = m_value.size();

        if (len > 1)
          {
            for (unsigned int i = 1; i < len; ++i)
              m_value[0] = m_value[0] + m_value[i];

            m_value.erase(m_value.begin() + 1, m_value.end());
          }
      }

      /// Check that there is at least one element less than parameter; return true for NaN value
      bool
      is_less(const T& v) const
      {
        for (const auto& x : m_value)
          if (x < v)
            return true;

        return false;
      }

      /// Check that there is at least one element greater than parameter; return true for NaN value
      bool
      is_greater(const T& v) const
      {
        for (const auto& x : m_value)
          if (!(x < v))
            return true;

        return false;
      }

      /// Check that value is corrupted (a NaN for floating point or has nonprintable characters)
      bool
      is_corrupted() const
      {
        for (const auto &x : m_value)
          if (!is_good(x))
            return true;

        return false;
      }

      typedef std::vector<T> data_type;
      typedef T basic_type;
      typedef DownsampledSeriesVectorData<T> ds_type;

    private:

      std::vector<T> m_value;

    };

  /// size of vector-of-strings data point in-memory
  template<>
    inline uint64_t
    SeriesVectorData<std::string>::get_data_size() const
    {
      uint64_t size = sizeof(SeriesDataBase) + sizeof(uint64_t) + sizeof(uint64_t) * m_value.size();
      for (const auto & x : m_value)
        size += x.size();
      return size;
    }

//  // default pbeast::Void::operator== always return false
//  template<>
//    inline bool
//    SeriesVectorData<pbeast::Void>::operator==(const SeriesVectorData &d) const
//    {
//      return (SeriesDataBase::operator==(d));
//    }

  template<class T>
    class Serie
    {

    public:

      std::vector<pbeast::SeriesData<T>> m_data;

    };


  /// return true, if there are data in [since,until] time interval existence
  template<class T>
    bool
    get_query_result_existence(const std::vector<T>& from, uint64_t since, uint64_t until)
    {
      if (unsigned int size = from.size())
        {
          if (from[size - 1].get_ts_last_updated() >= since && from[0].get_ts_created() <= until)
            return true;
        }

      return false;
    }


  template<class T>
    bool
    get_raw_data_bounds(const std::vector<T>& from, uint64_t since, uint64_t until, unsigned int& left_bound,  unsigned int& right_bound)
    {
      left_bound = right_bound = std::numeric_limits<unsigned int>::max();

      //
      //                 SINCE                 UNTIL
      //                   |                     |
      // [created,updated] |                     |                      (1)
      //                   |                     | [created,updated]    (2)
      //                   |  [created,updated]  |                      (3)
      //           [created,updated]             |                      (4)
      //                   |              [created,updated]             (5)
      //                [created,...........,updated]                   (6)
      //                   |                     |
      //

      const unsigned int len(from.size());

      for (unsigned int i = 0; i < len; ++i)
        {
          const uint64_t created(from[i].get_ts_created()), updated(from[i].get_ts_last_updated());

          // cases (1,4,6)
          if (created <= since)
            {
              left_bound = i;

              if (updated >= since)
                {
                  right_bound = i;
                }
            }

          // case (3)
          else if (updated < until)
            {
              if (left_bound == std::numeric_limits<unsigned int>::max())
                {
                  left_bound = i;
                }
              right_bound = i;
            }

          // cases (2,5,6)
          if (updated >= until)
            {
              // include special case (2) for closest "after"
              if (left_bound == std::numeric_limits<unsigned int>::max())
                {
                  left_bound = right_bound = i;
                }
              else
                {
                  right_bound = i;
                }

              break;
            }
        }

      // include special case (1) for closest "before"
      if (right_bound == std::numeric_limits<unsigned int>::max() && left_bound != right_bound)
        {
          right_bound = left_bound;
        }

      return (right_bound != std::numeric_limits<unsigned int>::max());
    }


  template<class T>
    bool
    get_raw_data_bounds(const std::vector<T>& from, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, unsigned int& left_bound,  unsigned int& right_bound)
    {
      left_bound = right_bound = std::numeric_limits<unsigned int>::max();

      //
      //                 SINCE                 UNTIL
      //                   |                     |
      // [created,updated] |                     |                      (1)  only if (updated >= w_since)
      //                   |                     | [created,updated]    (2)  only if (created <= w_until)
      //                   |  [created,updated]  |                      (3)
      //           [created,updated]             |                      (4)
      //                   |              [created,updated]             (5)
      //                [created,...........,updated]                   (6)
      //                   |                     |
      //

      const unsigned int len(from.size());

      for (unsigned int i = 0; i < len; ++i)
        {
          const uint64_t created(from[i].get_ts_created()), updated(from[i].get_ts_last_updated());

          // cases (1,4,6)
          if (created <= since)
            {
              // skip any too young data
              if (updated < w_since)
                continue;

              left_bound = i;

              if (updated >= since)
                {
                  right_bound = i;
                }
            }

          // case (3)
          else if (updated < until)
            {
              if (left_bound == std::numeric_limits<unsigned int>::max())
                {
                  left_bound = i;
                }
              right_bound = i;
            }

          // cases (2,5,6)
          if (updated >= until)
            {
              if (created > w_until)
                break;

              // include special case (2) for closest "after"
              if (left_bound == std::numeric_limits<unsigned int>::max())
                {
                  left_bound = right_bound = i;
                }
              else
                {
                  right_bound = i;
                }

              break;
            }
        }

      // include special case (1) for closest "before"
      if (right_bound == std::numeric_limits<unsigned int>::max() && left_bound != right_bound)
        {
          right_bound = left_bound;
        }

      return (right_bound != std::numeric_limits<unsigned int>::max());
    }


  class DownsampledSeriesDataBase
  {

  public:

    DownsampledSeriesDataBase(uint32_t ts) :
        m_ts(ts)
    {
      ;
    }

    bool operator<(const DownsampledSeriesDataBase & s) const
    {
      return (m_ts < s.m_ts);
    }

    bool
    operator==(const DownsampledSeriesDataBase &d) const
    {
      return (m_ts == d.m_ts);
    }

    uint32_t
    get_ts() const
    {
      return m_ts;
    }

    void
    set_ts(uint32_t v)
    {
      m_ts = v;
    }


  protected:

    uint32_t m_ts;

  };


  template<class T, class V = typename AverageType<T>::average_t>
    class AverageValue
    {
    public:

      AverageValue()
      {
        ;
      }

      AverageValue(const T& v_min, const V& v, const T& v_max) :
          m_min_value(v_min), m_max_value(v_max), m_value(v)
      {
        ;
      }

      AverageValue(double v) :
          m_min_value(v), m_max_value(max(v)), m_value(v)
      {
        ;
      }

      static T
      max(double v)
      {
        return ceil(v);
      }

      AverageValue&
      operator=(double v)
      {
        m_min_value = v;
        m_value = v;
        m_max_value = max(v);
        return *this;
      }

      AverageValue&
      operator=(const std::string& v)
      {
        m_min_value = m_value = m_max_value = v;
        return *this;
      }

      AverageValue&
      operator=(Void)
      {
        return *this;
      }

      bool
      operator==(const AverageValue &v) const
      {
        return (is_equal(m_min_value,v.m_min_value) && is_equal(m_value,v.m_value) && is_equal(m_max_value,v.m_max_value));
      }

      // FIXME m_value
      void
      merge(const AverageValue& v)
      {
        if (v.m_min_value < m_min_value)
          m_min_value = v.m_min_value;
        if (v.m_max_value > m_max_value)
          m_max_value = v.m_max_value;
      }

      const T&
      get_min_value() const
      {
        return m_min_value;
      }

      const V&
      get_value() const
      {
        return m_value;
      }

      const T&
      get_max_value() const
      {
        return m_max_value;
      }

      void
      print_json_value(std::ostringstream& ss, const pbeast::AggregationType at) const;

      T m_min_value;
      T m_max_value;
      V m_value;
    };

  template<class T>
  using DownsampleValue = AverageValue<T, typename AverageType<T>::average_t>;

  template<>
    inline float
    AverageValue<float>::max(double v)
    {
      return static_cast<float>(v);
    }

  template<>
    inline double
    AverageValue<double>::max(double v)
    {
      return v;
    }

  template<>
    inline void
    DownsampleValue<Void>::merge(const DownsampleValue<Void>&)
    {
      ;
    }

  template<class T>
    class DownsampledSeriesData : public DownsampledSeriesDataBase, public CType<T>
    {

    public:

      DownsampledSeriesData(uint32_t ts) :
          DownsampledSeriesDataBase(ts)
      {
        ;
      }

      DownsampledSeriesData(uint32_t ts, const DownsampleValue<T>& d) :
          DownsampledSeriesDataBase(ts), m_data(d)
      {
        ;
      }

      DownsampledSeriesData(uint32_t ts, const T& v_min, const typename pbeast::AverageType<T>::average_t& v, const T& v_max) :
          DownsampledSeriesDataBase(ts), m_data(v_min, v, v_max)
      {
        ;
      }

      bool
      operator==(const DownsampledSeriesData &d) const
      {
        return (DownsampledSeriesDataBase::operator==(d) && m_data == d.m_data);
      }

      void
      merge(const DownsampledSeriesData& v)
      {
        m_data.merge(v.m_data);
      }

      void
      merge(const DownsampleValue<T>& v)
      {
        m_data.merge(v);
      }

      const DownsampleValue<T>&
      data() const
      {
        return m_data;
      }

      static bool
      is_array()
      {
        return false;
      }

      /// used to calculate size of data in-memory
      uint64_t
      get_data_size() const
      {
        return (sizeof(DownsampledSeriesDataBase) + sizeof(DownsampleValue<T>));
      }

      void
      print_json_value(std::ostringstream& ss, const pbeast::AggregationType at) const;

      void
      sum_array_elements()
      {
        throw std::runtime_error("sum_array_elements() was called on single-value downsampled data");
      }

      /// Check that min value is less than parameter; return true for NaN value
      bool
      is_less(const T& v) const
      {
        return !(m_data.m_min_value > v);
      }

      /// Check that max value is greater than parameter; return true for NaN value
      bool
      is_greater(const T& v) const
      {
        return !(m_data.m_max_value < v);
      }

      /// Check that value is corrupted (a NaN for floating point or has nonprintable characters)
      bool
      is_corrupted() const
      {
        return !is_good(m_data.m_value);
      }

      typedef T basic_type;
      typedef SeriesData<T> raw_type;
      typedef SeriesData<typename pbeast::AverageType<T>::average_t> average_type;

    private:

      DownsampleValue<T> m_data;

    };


  /// size of downsampled string data point in-memory
  template<>
    inline uint64_t
    DownsampledSeriesData<std::string>::get_data_size() const
    {
      return (sizeof(DownsampledSeriesDataBase) + m_data.m_min_value.size() + m_data.m_value.size() + m_data.m_max_value.size());
    }

//  // default pbeast::Void::operator== always return false
//  template<>
//    inline bool
//    DownsampledSeriesData<pbeast::Void>::operator==(const DownsampledSeriesData &d) const
//    {
//      return (DownsampledSeriesDataBase::operator==(d));
//    }


  template<class T>
    class DownsampledSeriesVectorData : public DownsampledSeriesDataBase, public CType<T>
    {

    public:

      DownsampledSeriesVectorData(uint32_t ts) :
          DownsampledSeriesDataBase(ts)
      {
        ;
      }

      DownsampledSeriesVectorData(uint32_t ts, const std::vector<DownsampleValue<T>>& data) :
          DownsampledSeriesDataBase(ts), m_data(data)
      {
        ;
      }

      bool
      operator==(const DownsampledSeriesVectorData &d) const
      {
        return (DownsampledSeriesDataBase::operator==(d) && m_data == d.m_data);
      }

      void
      merge(const DownsampledSeriesVectorData& v)
      {
        merge(v.m_data);
      }

      void
      merge(const std::vector<DownsampleValue<T>>& v)
      {
        const unsigned int vec_len(m_data.size());
        const unsigned int val_len(v.size());
        const unsigned int len = std::min(vec_len, val_len);

        for(unsigned int i = 0; i < len; ++i)
          {
            m_data[i].merge(v[i]);
          }

        for(unsigned int i = len; i < val_len; ++i)
          {
            m_data.emplace_back(v[i].m_min_value,v[i].m_value,v[i].m_max_value);
          }
      }

      const std::vector<DownsampleValue<T>>&
      data() const
      {
        return m_data;
      }

      const DownsampleValue<T>&
      data(unsigned int j) const
      {
        return m_data[j];
      }

      inline unsigned int
      get_size() const
      {
        return m_data.size();
      }

      static bool
      is_array()
      {
        return true;
      }

      /// used to calculate size of data in-memory
      uint64_t
      get_data_size() const
      {
        return (sizeof(DownsampledSeriesDataBase) + sizeof(uint64_t) + sizeof(DownsampleValue<T>) * m_data.size());
      }

      void
      print_json_value(std::ostringstream& ss, const pbeast::AggregationType at) const;

      void
      sum_array_elements()
      {
        const unsigned int len = m_data.size();

        if (len > 1)
          {
            for (unsigned int i = 1; i < len; ++i)
              {
                m_data[0].m_min_value += m_data[i].m_min_value;
                m_data[0].m_value += m_data[i].m_value;
                m_data[0].m_max_value += m_data[i].m_max_value;
              }

            m_data.erase(m_data.begin() + 1, m_data.end());
          }
      }

      /// Check that there is at least one min element less than parameter; return true for NaN value
      bool
      is_less(const T& v) const
      {
        for (const auto& x : m_data)
          if (x.m_min_value < v)
            return true;

        return false;
      }

      /// Check that there is at least one max element greater than parameter; return true for NaN value
      bool
      is_greater(const T& v) const
      {
        for (const auto& x : m_data)
          if (!(x.m_max_value < v))
            return true;

        return false;
      }

      /// Check that value is corrupted (a NaN for floating point or has nonprintable characters)
      bool
      is_corrupted() const
      {
        for (const auto &x : m_data)
          if (!is_good(x.m_value))
            return true;

        return false;
      }

      typedef T basic_type;
      typedef SeriesVectorData<T> raw_type;
      typedef SeriesVectorData<typename pbeast::AverageType<T>::average_t> average_type;

    private:

      std::vector<DownsampleValue<T>> m_data;

    };


  /// size of downsampled vector-of-strings data point in-memory
  template<>
    inline uint64_t
    DownsampledSeriesVectorData<std::string>::get_data_size() const
    {
      uint64_t size = sizeof(DownsampledSeriesDataBase) + sizeof(uint64_t);
      for (const auto & x : m_data)
        size += x.m_min_value.size() + x.m_value.size() + x.m_max_value.size();
      return size;
    }

//  // default pbeast::Void::operator== always return false
//  template<>
//    inline bool
//    DownsampledSeriesVectorData<pbeast::Void>::operator==(const DownsampledSeriesVectorData &d) const
//    {
//      return (DownsampledSeriesDataBase::operator==(d));
//    }

}

#endif
