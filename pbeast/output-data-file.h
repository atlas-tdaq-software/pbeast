#ifndef PBEAST_OUTPUT_DATA_FILE_H
#define PBEAST_OUTPUT_DATA_FILE_H

#include <filesystem>
#include <type_traits>
#include <stdint.h>
#include <sstream>
#include <string>
#include <vector>

#include "pbeast/output-stream.h"
#include "pbeast/data-file.h"
#include "pbeast/series-data.h"
#include "pbeast/series-info.h"
#include "pbeast/series-print.h"

namespace pbeast {


    /**
     *   Class OutputDataFile is used to store series of data to a file.
     *
     *   The constructor does not create a file.
     *   Real data can be written by the following methods (the order is important):
     *   * write_header()
     *   * multiple write_data<T> (note, names of series must be ordered alphabetically)
     *   * write_footer()
     *   * close() - flush file and check status use close() method.
     */

  class OutputDataFile : public DataFile {

    public:


        /**
         *   Create output data file object.
         *
         *   \param file_name name of the output file
         *   \param partition partition name
         *   \param class_name class name
         *   \param class_path path to class inside repository (for normal classes is equal to class name; is different for nested classes)
         *   \param attribute attribute name
         *   \param base_timestamp base timestamp defining oldest data stored in the file (in microseconds since Epoch)
         *   \param data_type type of data as defined by pbeast::DataType
         *   \param is_array if true, the data are array
         *   \param number_of_series number of series to be stored by the write_data<T> method
         *   \param zip_catalog compress catalog (footer), i.e. information about names of objects and their indices
         *   \param zip_data compress data
         *   \param catalogize_strings if data type is "string", they can be saved efficiently in case of repetitive data belonging to the same object
         *   \param downsample_interval if it is 0, then data are in raw format; otherwise they are downsampled with given time interval
         */

      OutputDataFile(
        const std::string& file_name,
        const std::string& partition,
        const std::string& class_name,
        const std::filesystem::path& class_path,
        const std::string& attribute,
        uint64_t base_timestamp,
        uint32_t data_type,
        bool is_array,
        uint32_t number_of_series,
        bool zip_catalog,
        bool zip_data,
        bool catalogize_strings,
        uint32_t downsample_interval
      ) :
        DataFile(file_name, partition, class_name, class_path, attribute, base_timestamp, data_type, is_array, number_of_series, zip_catalog, zip_data, catalogize_strings, downsample_interval),
        m_string_stream(nullptr), m_output_stream(nullptr)
      {
        m_series_info.reserve(number_of_series);
      }

      OutputDataFile(
        std::stringstream * stream,
        const std::string& partition,
        const std::string& class_name,
        const std::filesystem::path& class_path,
        const std::string& attribute,
        uint64_t base_timestamp,
        uint32_t data_type,
        bool is_array,
        uint32_t number_of_series,
        bool zip_catalog,
        bool zip_data,
        bool catalogize_strings,
        uint32_t downsample_interval
      ) :
        DataFile("", partition, class_name, class_path, attribute, base_timestamp, data_type, is_array, number_of_series, zip_catalog, zip_data, catalogize_strings, downsample_interval),
        m_string_stream(stream), m_output_stream(nullptr)
      {
        m_series_info.reserve(number_of_series);
      }


        /**
         *  Free resources used by output file object.
         */

      ~OutputDataFile();

        /**
         *   Create name for output data file.
         *
         *   \param repository_path path to repository
         *   \param partition_name encoded partition name
         *   \param class_name encoded class name
         *   \param attribut_name encoded attribute name
         *   \param secs_min - minimal timestamp
         *   \param secs_max - maximum timestamp
         *   \param extension - file suffix
         *   \param unique - when true, add a number before suffix, if file exists
         *   \param downsample_interval - if non-zero, add after extension to distinguish downsampled data
         *   \return the name of the output file
         */

      static std::filesystem::path make_file_name(
        const std::filesystem::path& repository_path,
        const std::string& partition_name,
        const std::filesystem::path& class_path,
        const std::string& attribute_name,
        uint32_t secs_min,
        uint32_t secs_max,
        const char * extension = ".pb",
        bool unique = false,
        uint32_t downsample_interval = 0
      );


        /**
         *   Write header data (partition, class and attribute names; creation timestamp; data type; number of series)
         *   \exception std::runtime_error in case of write problems
         */

      void write_header();


        /**
         *   The file has to contain raw data
         *   \throw std::runtime_error exception, if m_downsample_interval is non-zero
         */

      void assert_raw_type();


        /**
         *   The file has to contain downsampled data
         *   \throw std::runtime_error exception, if m_downsample_interval is zero
         */

      void assert_downsample_type();


        /**
         *   Write timestamp without any extra bits
         */

      inline void
      write_timestamp(const uint64_t ts_created, uint64_t &ts_prev, uint64_t &ts_prev_delta)
      {
        uint64_t diff = ts_created - ts_prev;
        const uint64_t delta = ts_prev_delta;
        ts_prev_delta = diff;
        diff ^= delta;
        m_output_stream->write(diff);
        ts_prev = ts_created;
      }


      inline void
      write_timestamp(const uint64_t ts_created, uint64_t &ts_prev, uint64_t &ts_prev_delta, uint64_t flag)
      {
        uint64_t diff = ts_created - ts_prev;
        const uint64_t delta = ts_prev_delta;
        ts_prev_delta = diff;
        diff ^= delta;

        diff <<= 1;
        diff |= flag;

        m_output_stream->write(diff);
        ts_prev = ts_created;
      }

      inline void
      write_timestamp(const uint64_t ts_created, uint64_t &ts_prev, uint64_t &ts_prev_delta, uint64_t flag1, uint64_t flag2)
      {
        uint64_t diff = ts_created - ts_prev;
        const uint64_t delta = ts_prev_delta;
        ts_prev_delta = diff;
        diff ^= delta;

        diff <<= 2;
        diff |= flag2;
        diff |= flag1 << 1;

        m_output_stream->write(diff);
        ts_prev = ts_created;
      }

      inline void
      write_interval(const uint64_t ts_created, const uint64_t ts_last_updated, uint64_t &ts_prev, uint64_t &ts_prev_delta)
      {
        const uint64_t interval = ts_last_updated - ts_created;
        uint64_t diff = ts_created - ts_prev;
        const uint64_t delta = ts_prev_delta;
        ts_prev_delta = diff;
        diff ^= delta;

        diff <<= 1;

        if (interval)
          diff |= 1UL;

        m_output_stream->write(diff);

        if (interval)
          m_output_stream->write(interval);

        ts_prev = ts_created;
      }

      inline void
      write_interval(const uint64_t ts_created, const uint64_t ts_last_updated, uint64_t &ts_prev, uint64_t &ts_prev_delta, uint64_t flag)
      {
        const uint64_t interval = ts_last_updated - ts_created;
        uint64_t diff = ts_created - ts_prev;
        const uint64_t delta = ts_prev_delta;
        ts_prev_delta = diff;

        diff ^= delta;

        diff <<= 2;

        diff |= flag;

        if (interval)
          diff |= 1UL << 1;

        m_output_stream->write(diff);

        if (interval)
          m_output_stream->write(interval);

        ts_prev = ts_created;
      }

      inline void
      write_interval(const uint64_t ts_created, const uint64_t ts_last_updated, uint64_t &ts_prev, uint64_t &ts_prev_delta, uint64_t flag1, uint64_t flag2)
      {
        const uint64_t interval = ts_last_updated - ts_created;
        uint64_t diff = ts_created - ts_prev;
        const uint64_t delta = ts_prev_delta;
        ts_prev_delta = diff;
        diff ^= delta;

        diff <<= 3;

        diff |= flag2;
        diff |= flag1 << 1;

        if (interval)
          diff |= 1UL << 2;

        m_output_stream->write(diff);

        if (interval)
          m_output_stream->write(interval);

        ts_prev = ts_created;
      }


      void inline
      write_timestamp(const uint32_t ts, uint32_t& prev)
      {
        m_output_stream->write(ts-prev-get_downsample_interval());
        prev = ts;
      }


        /**
         *   Write single value series of raw data.
         *
         *   The template parameter is one of supported type: bool, int8_t, int16_t, uint16_t,
         *   int32_t, uint32_t, int64_t, uint64_t, float, double or std::string.
         *
         *   \param name name of data
         *   \param data series of raw data
         *   \exception std::runtime_error in case of write problems
         */

      template<typename T>
        void
        write_data(const std::string& name, const std::vector<SeriesData<T> >& data)
        {
          write_data_imp(name, data);
        }


      template<typename T>
        void
        write_data_imp(const std::string& name, const std::vector<SeriesData<T> >& data)
        {
          assert_raw_type();

          if (data.size())
            {
              uint64_t pos = m_output_stream->check_and_reset(m_zip_data);
              uint64_t prev_ts = m_base_timestamp;
              uint64_t delta_ts = 0;
              m_series_info.push_back(new SeriesInfo(name, pos));
              m_output_stream->write(data.size());

              if constexpr (std::is_same_v<T, pbeast::Void>)
                {
                  for (const auto x : data)
                    write_timestamp(x.get_ts_created(), prev_ts, delta_ts);
                }
              else
                {
                  T prev_value, diff_value;
                  if constexpr (std::is_integral_v<T>)
                    prev_value = diff_value = 0;

                  for (uint64_t i = 0; i < data.size(); i += c_block_size)
                    {
                      uint64_t intervals = 0;
                      const uint64_t end = std::min(data.size(), i + c_block_size);

                      for (uint64_t j = i; j < end; ++j)
                        if (data[j].get_ts_created() != data[j].get_ts_last_updated())
                          {
                            intervals = 1;
                            break;
                          }

                      if constexpr (std::is_same_v<T, bool>)
                        write_boolean_type(data, prev_ts, delta_ts, intervals, i, end);
                      else if constexpr (std::is_integral_v<T>)
                        write_integral_type<T>(data, prev_ts, delta_ts, prev_value, diff_value, intervals, i, end);
                      else
                        write_other_type<T>(data, prev_ts, delta_ts, intervals, i, end);
                    }
                }
            }
        }

      void
      write_boolean_type(const std::vector<SeriesData<bool>> &data, uint64_t &prev_ts, uint64_t &delta_ts, uint64_t intervals, const uint64_t start, const uint64_t end);

      template<typename T>
      void
      write_other_type(const std::vector<SeriesData<T>> &data, uint64_t &prev_ts, uint64_t &delta_ts, uint64_t intervals, const uint64_t start, const uint64_t end)
      {
        if (intervals)
          write_interval(data[start].get_ts_created(), data[start].get_ts_last_updated(), prev_ts, delta_ts, 1);
        else
          write_timestamp(data[start].get_ts_created(), prev_ts, delta_ts, 0);
        m_output_stream->write(data[start].get_value());

        for (uint64_t i = start + 1; i < end; ++i)
          {
            if (intervals)
              write_interval(data[i].get_ts_created(), data[i].get_ts_last_updated(), prev_ts, delta_ts);
            else
              write_timestamp(data[i].get_ts_created(), prev_ts, delta_ts);

            m_output_stream->write(data[i].get_value());
          }
      }

      template<typename T>
      inline bool
      needs_negation(const T val)
      {
        return (val < 0 && val != std::numeric_limits<T>::min());
      }

      template<typename T>
      void
      write_integral_type(const std::vector<SeriesData<T>>& data, uint64_t& prev_ts, uint64_t& delta_ts, T& prev_value, T& diff_value, uint64_t intervals, const uint64_t start, const uint64_t end)
      {
        for (uint64_t i = start; i < end; ++i)
          {
            const SeriesData<T> &d(data[i]);
            const T &val(d.get_value());

            const T dt = diff_value;
            diff_value = val - prev_value;
            prev_value = val;

            T p;
            uint64_t flag;

            if constexpr (std::is_unsigned_v<T>)
              {
                uint64_t flag = 0;
                constexpr T half_max_const = std::numeric_limits<T>::max() / 2;

                p = static_cast<T>(diff_value - dt);

                if (p > half_max_const)
                  {
                    p = ~p;
                    flag = 1;
                  }

                if (intervals)
                  {
                    if (i != start)
                      write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, flag);
                    else
                      write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, flag, 1);
                  }
                else
                  {
                    if (i != start)
                      write_timestamp(d.get_ts_created(), prev_ts, delta_ts, flag);
                    else
                      write_timestamp(d.get_ts_created(), prev_ts, delta_ts, flag, 0);
                  }

                m_output_stream->write(p);
              }
            else
              {
                p = static_cast<T>(diff_value - dt);
                typename std::make_unsigned<T>::type v;

                if (needs_negation(p))
                 {
                    v = -p;
                    flag = 1;
                  }
                else
                  {
                    v = p;
                    flag = 0;
                  }

                if (intervals)
                  {
                    if (i != start)
                      write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, flag);
                    else
                      write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, flag, 1);
                  }
                else
                  {
                    if (i != start)
                      write_timestamp(d.get_ts_created(), prev_ts, delta_ts, flag);
                    else
                      write_timestamp(d.get_ts_created(), prev_ts, delta_ts, flag, 0);
                  }

                m_output_stream->write(v);
              }
          }
      }

        /**
         *   Write multi value series of raw data.
         *
         *   This method is similar to above except is stores vectors of data instead of single data.
         */

      template<class T> void write_data(const std::string& name, const std::vector<SeriesVectorData<T> >& data) {
        assert_raw_type();

        if(const uint64_t data_size = data.size()) {
          uint64_t pos = m_output_stream->check_and_reset(m_zip_data);
          uint64_t prev_ts = m_base_timestamp;
          uint64_t delta_ts = 0;
          m_series_info.push_back(new SeriesInfo(name, pos));

          // detect if array has fixed length
          bool fixed_len_flag = true;
          const uint64_t fixed_len = data[0].get_value().size();

          for (uint64_t i = 1; i < data_size; ++i)
            if (data[i].get_value().size() != fixed_len)
              {
                fixed_len_flag = false;
                break;
              }

          // if length is fixed, detect indices with constant value (if any)
          uint64_t const_indices_len = 0;
          std::vector<bool> const_indices;
          if constexpr (!std::is_same_v<T, bool>)
            {
              if (fixed_len_flag)
                {
                  std::vector<T> first = data[0].get_value();
                  const_indices.assign(fixed_len, true);

                  for (uint64_t i = 1; i < data_size; ++i)
                    for (uint64_t j = 0; j < fixed_len; ++j)
                      if (const_indices[j] && data[i].get_value()[j] != first[j])
                        const_indices[j] = false;

                  for (uint64_t j = 0; j < fixed_len; ++j)
                    if (const_indices[j])
                      const_indices_len++;
                }
            }

          uint64_t ds = data_size;

          if (fixed_len_flag)
            {
              ds <<= 2;
              ds |= 1L;
              if (const_indices_len)
                ds |= 1UL << 1;
           }
          else
            {
              ds <<= 1;
            }

          m_output_stream->write(ds);

          if (fixed_len_flag)
            m_output_stream->write(fixed_len);

          if (const_indices_len)
            {
              m_output_stream->write(const_indices_len);

              for (uint64_t j = 0; j < fixed_len; ++j)
                if (const_indices[j])
                  {
                    m_output_stream->write(j);
                    m_output_stream->write(data[0].get_value()[j]);
                  }
            }

          std::vector<T> prev_value, diff_value, dt, p;
          std::vector<bool> bools, prev_bools;
          uint64_t prev_len = std::numeric_limits<uint64_t>::max();

          if constexpr (std::is_integral_v<T>)
            if constexpr (!std::is_same_v<T, bool>)
              if (fixed_len_flag)
                prev_bools.resize(fixed_len - const_indices_len, false);

          for(uint64_t i = 0; i < data.size(); ++i) {
            const SeriesVectorData<T>& d(data[i]);
            const std::vector<T>& vec(d.get_value());
            const uint64_t len(vec.size());

            if constexpr (std::is_integral_v<T>)
              {
                if constexpr (std::is_same_v<T, bool>)
                  {
                    if (fixed_len_flag)
                      {
                        write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts);
                      }
                    else if (prev_len == len)
                      {
                        write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, 0);
                      }
                    else
                      {
                        write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, 1);
                        m_output_stream->write(len);
                        prev_len = len;
                      }

                    write_bools(vec);
                  }
                else
                  {
                    const uint64_t var_len = len - const_indices_len;

                    prev_value.resize(var_len, 0);
                    diff_value.resize(var_len, 0);
                    dt.resize(var_len, 0);
                    p.resize(var_len, 0);

                    for (uint64_t j = 0, k = 0; j < len; ++j)
                      {
                        if (const_indices_len && const_indices[j])
                          continue;

                        dt[k] = diff_value[k];
                        diff_value[k] = vec[j] - prev_value[k];
                        prev_value[k] = vec[j];
                        p[k] = static_cast<T>(diff_value[k] - dt[k]);
                        k++;
                      }

                    bools.reserve(len);
                    bools.clear();

                     if constexpr (std::is_unsigned_v<T>)
                       {
                         constexpr T half_max_const = std::numeric_limits<T>::max() / 2;

                         for (uint64_t j = 0, k = 0; j < len; ++j)
                           if (!const_indices_len || !const_indices[j])
                             bools.push_back(p[k++] > half_max_const);

                         write_interval_and_bools(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, fixed_len_flag, bools, prev_bools);

                         for (uint64_t j = 0, k = 0; j < len; ++j)
                           if (!const_indices_len || !const_indices[j])
                             {
                               m_output_stream->write(p[k] > half_max_const ? static_cast<T>(~p[k]) : p[k]);
                               k++;
                             }
                       }
                     else
                       {
                         for (uint64_t j = 0, k = 0; j < len; ++j)
                           if (!const_indices_len || !const_indices[j])
                             bools.push_back(needs_negation(p[k++]));

                         write_interval_and_bools(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, fixed_len_flag, bools, prev_bools);

                         for (uint64_t j = 0, k = 0; j < len; ++j)
                           if (!const_indices_len || !const_indices[j])
                             {
                               const typename std::make_unsigned<T>::type val(needs_negation(p[k]) ? -p[k] : p[k]);
                               m_output_stream->write(val);
                               k++;
                             }
                       }
                  }
              }
            else
              {
                if (fixed_len_flag)
                  {
                    write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts);
                  }
                else if (prev_len == len)
                  {
                    write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, 0);
                  }
                else
                  {
                    write_interval(d.get_ts_created(), d.get_ts_last_updated(), prev_ts, delta_ts, 1);
                    m_output_stream->write(len);
                    prev_len = len;
                  }

                for (uint64_t j = 0; j < len; ++j)
                  if (!const_indices_len || !const_indices[j])
                    m_output_stream->write(vec[j]);
              }
          }
        }
      }

      /**
       *   Write single value series of downsampled data.
       *
       *   The template parameter is one of supported type: bool, int8_t, int16_t, uint16_t,
       *   int32_t, uint32_t, int64_t, uint64_t, float, double or std::string.
       *
       *   \param name name of data
       *   \param data series of downsampled data
       *   \exception std::runtime_error in case of write problems
       */

    template<typename T>
      void
      write_data(const std::string& name, const std::vector<DownsampledSeriesData<T> >& data)
      {
        write_data_imp(name, data);
      }

    template<typename T>
      void
      write_data_imp(const std::string& name, const std::vector<DownsampledSeriesData<T> >& data)
      {
        assert_downsample_type();

        if (data.size())
          {
            uint32_t prev = 0;
            uint64_t pos = m_output_stream->check_and_reset(m_zip_data);
            m_series_info.push_back(new SeriesInfo(name, pos));
            m_output_stream->write(data.size());

            for (unsigned int i = 0; i < data.size(); ++i)
              {
                const DownsampledSeriesData<T>& d(data[i]);
                write_timestamp(d.get_ts(), prev);
                m_output_stream->write(d.data().get_min_value());
                m_output_stream->write(d.data().get_value());
                m_output_stream->write(d.data().get_max_value());
              }
          }
      }

      /**
       *   Write multi value series of downsampled data.
       *
       *   This method is similar to above except is stores vectors of data instead of single data.
       */

    template<class T>
      void
      write_data(const std::string& name, const std::vector<DownsampledSeriesVectorData<T> >& data)
      {
        assert_downsample_type();

        if (data.size())
          {
            uint32_t prev = 0;
            uint64_t pos = m_output_stream->check_and_reset(m_zip_data);
            m_series_info.push_back(new SeriesInfo(name, pos));
            m_output_stream->write(data.size());
            for (unsigned int i = 0; i < data.size(); ++i)
              {
                const DownsampledSeriesVectorData<T>& d(data[i]);
                write_timestamp(d.get_ts(), prev);
                const uint32_t len(d.data().size());
                m_output_stream->write(len);
                for (unsigned int j = 0; j < len; ++j)
                  {
                    m_output_stream->write(d.data(j).get_min_value());
                    m_output_stream->write(d.data(j).get_value());
                    m_output_stream->write(d.data(j).get_max_value());
                  }
              }
          }
      }

        /**
         *   Write footer data (names and positions of series)
         *   \exception std::runtime_error in case of write problems
         */

      void write_footer();


        /**
         *   Flush stream buffer and close the file.
         *   \exception std::runtime_error in case of write problems
         *   \return size of output file
         */

      int64_t close();

      const std::vector<SeriesInfo *>&
      get_series_info() const
      {
        return m_series_info;
      }


      /**
       *   Write vector of boolean values as an array of 64 varints.
       */

      void
      write_bools(const std::vector<bool>& in);

      void
      write_interval_and_bools(const uint64_t ts_created, const uint64_t ts_last_updated, uint64_t &ts_prev, uint64_t &ts_prev_delta, bool fixed_len_flag, const std::vector<bool>& vec, std::vector<bool>& prev_vec);



    private:

      std::stringstream * m_string_stream;
      OutputStream * m_output_stream;
      std::vector<SeriesInfo *> m_series_info;

  };

  template<>
    void
    OutputDataFile::write_data<std::string>(const std::string& name, const std::vector<SeriesData<std::string> >& data);

  template<>
    void
    OutputDataFile::write_data<std::string>(const std::string& name, const std::vector<DownsampledSeriesData<std::string> >& data);

}

#endif
