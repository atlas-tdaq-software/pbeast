#ifndef PBEAST_JSON_SERIALIZE_STRING_H
#define PBEAST_JSON_SERIALIZE_STRING_H

#include <string>
#include <ostream>
#include <pbeast/series-print.h>

namespace pbeast
{
  namespace json
  {

    struct serisalize
    {
      const std::string& m_val;

      serisalize(const std::string &s) :
          m_val(s)
      {
        ;
      }
    };

    inline std::ostream&
    operator<<(std::ostream& s, const serisalize& str)
    {
      pbeast::print_json(s, str.m_val);
      return s;
    }
  }
}

#endif
