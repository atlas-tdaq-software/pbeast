#ifndef PBEAST_DATA_POINT_H
#define PBEAST_DATA_POINT_H

#include <stdint.h>
#include <vector>

#include <iostream>

#include <pbeast/data-type.h>

namespace pbeast
{
  template<class T>
    class DataPointCore
    {

    public:

      DataPointCore(uint64_t ts, const T& v) :
          m_ts(ts), m_value(v), m_is_null(false)
      {
        ;
      }

      DataPointCore(uint64_t ts) :
          m_ts(ts), m_is_null(true)
      {
        ;
      }

#ifdef SWIGJAVA
      DataPointCore() :
          m_ts(0), m_is_null(true)
      {
        ;
      }

      DataPointCore(uint64_t ts, const T& v, bool isNull) :
          m_ts(ts), m_value(v), m_is_null(isNull)
      {
        ;
      }

      DataPointCore operator=(const DataPointCore& rhs) {
          return DataPointCore(rhs.m_ts, rhs.m_value, rhs.m_is_null);
      }
#endif

      uint64_t
      ts() const
      {
        return m_ts;
      }

      bool
      is_null() const
      {
        return m_is_null;
      }

      const T&
      value() const
      {
        return m_value;
      }

      typedef T data_type;

    protected:

      uint64_t m_ts;
      T m_value;
      const bool m_is_null;
    };

  template<class T>
    class DataPoint : public DataPointCore<T>, public CType<T>
    {
    public:
      DataPoint(uint64_t ts, const T& v) :
          DataPointCore<T>(ts, v)
      {
        ;
      }

      DataPoint(uint64_t ts) :
          DataPointCore<T>(ts)
      {
        ;
      }

#ifdef SWIGJAVA
      DataPoint() : DataPointCore<T>()
      {
          ;
      }
#endif

    };

  template<class T, class V = typename AverageType<T>::average_t>
    class AverageDataPoint : public DataPointCore<V>, public CType<T>
    {

    public:

      AverageDataPoint(uint64_t ts, const T& v_min, const V& v, const T& v_max) :
          DataPointCore<V>(ts, v), m_min_value(v_min), m_max_value(v_max)
      {
        ;
      }

      AverageDataPoint(uint64_t ts) :
          DataPointCore<V>(ts)
      {
        ;
      }

#ifdef SWIGJAVA
      AverageDataPoint() : DataPointCore<V>()
      {
          ;
      }
#endif

      const T&
      min_value() const
      {
        return m_min_value;
      }

      const T&
      max_value() const
      {
        return m_max_value;
      }

    protected:

      T m_min_value;
      T m_max_value;

    };

  template<class T>
    using DownsampledDataPoint = AverageDataPoint<T, typename AverageType<T>::average_t>;

}

#endif
