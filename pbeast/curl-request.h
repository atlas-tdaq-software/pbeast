#ifndef PBEAST_CURL_REQUEST_H
#define PBEAST_CURL_REQUEST_H

#include <curl/curl.h>

#include <sstream>
#include <string>

namespace pbeast
{
  class ServerProxy;

  namespace curl
  {
    // internally managed CURL handle, one per thread
    thread_local std::unique_ptr<CURL, decltype(&curl_easy_cleanup)> curl{nullptr, nullptr};


    // Get thread local global handle, create it if necessary
    CURL*
    get_handle()
    {
      if (!curl)
        curl = std::unique_ptr<CURL, decltype(&curl_easy_cleanup)>(curl_easy_init(), curl_easy_cleanup);

      return curl.get();
    }
  }

  struct url_var
  {
    static inline const char * s_server = "TDAQ_PBEAST_SERVER_URL";
    static inline const char * s_receiver = "TDAQ_PBEAST_RECEIVER_URL";
  };

  std::string
  url_encode(CURL * curl, const std::string& value);

  class CurlRequest
  {
    typedef size_t
    (*WriterFunction)(void *v_ptr, size_t size, size_t nmemb, void *stream);

  public:

    virtual bool
    is_failed() const = 0;

    virtual void
    process_result() = 0;

    virtual const std::string
    get_error_text() const = 0;

    std::ostringstream&
    request()
    {
      return m_request;
    }

    void
    add_post_multipart(const char *name, const char *ptr, long size = CURL_ZERO_TERMINATED);

    CurlRequest(void * result_handler, const pbeast::ServerProxy& proxy);

    void
    execute(WriterFunction f);

    virtual
    ~CurlRequest();

    const pbeast::ServerProxy& m_proxy;
    void * m_result_handler;
    curl_mime * m_multipart;
    std::ostringstream m_request;

  };

  struct CurlSimpleBuffer
  {
    CurlSimpleBuffer() : m_failed(false)
    {
    }

    static size_t
    get(void *v_ptr, size_t size, size_t nmemb, void *stream);

    std::string m_buffer;
    bool m_failed;
  };

  struct SimpleRequest : pbeast::CurlRequest
  {
    std::string m_out;
    pbeast::CurlSimpleBuffer m_resp;

    SimpleRequest(const pbeast::ServerProxy& proxy) :
      pbeast::CurlRequest(reinterpret_cast<void *>(&m_resp), proxy)
    {
    }

    bool
    is_failed() const
    {
      return false;
    }

    const std::string
    get_error_text() const
    {
      return m_resp.m_buffer;
    }

    virtual void
    process_result()
    {
      ERS_DEBUG(1 , m_resp.m_buffer);
      m_out = m_resp.m_buffer;
    }

  };
}

#endif
