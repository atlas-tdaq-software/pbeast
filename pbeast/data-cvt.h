#ifndef PBEAST_DATA_CVT_H
#define PBEAST_DATA_CVT_H

#include "pbeast/series-data.h"
#include "pbeast/data-point.h"

namespace pbeast
{

  // pbeast::DownsampledSeriesData<T> to DataPoint<T>
  // pbeast::DownsampledSeriesVectorData<T> to DataPoint<std::vector<T>>
  template<class T>
    void
    extract_average_value(const std::vector<pbeast::DownsampledSeriesData<T>>& from, std::vector<DataPoint<typename pbeast::AverageType<T>::average_t>>& to)
    {
      to.reserve(from.size());
      for (const auto& x : from)
        to.emplace_back(pbeast::mk_ts(x.get_ts()), x.data().get_value());
    }

  template<class T>
    void
    extract_average_value(const std::vector<pbeast::DownsampledSeriesVectorData<T>>& from, std::vector<DataPoint<std::vector<typename pbeast::AverageType<T>::average_t>>>&to)
    {
      to.reserve(from.size());
      for (const auto& x : from)
        {
          std::vector<typename pbeast::AverageType<T>::average_t> vec;
          vec.reserve(x.get_size());

          for (const auto& y : x.data())
            vec.emplace_back(y.get_value());

          to.emplace_back(pbeast::mk_ts(x.get_ts()), vec);
        }
    }

  template<class T>
    void
    extract_raw_value(const std::vector<pbeast::DownsampledSeriesData<T>>& from, std::vector<DataPoint<T>>& to)
    {
      to.reserve(from.size());
      for (const auto& x : from)
        to.emplace_back(pbeast::mk_ts(x.get_ts()), x.data().get_min_value());
    }

  template<class T>
    void
    extract_raw_value(const std::vector<pbeast::DownsampledSeriesVectorData<T>>& from, std::vector<DataPoint<std::vector<T>>>&to)
    {
      to.reserve(from.size());
      for (const auto& x : from)
        {
          std::vector<T> vec;
          vec.reserve(x.get_size());

          for (const auto& y : x.data())
            vec.emplace_back(y.get_min_value());

          to.emplace_back(pbeast::mk_ts(x.get_ts()), vec);
        }
    }

}


#endif
