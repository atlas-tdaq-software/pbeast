#ifndef PBEAST_COPY_FILE_H
#define PBEAST_COPY_FILE_H

#include <type_traits>

#include <boost/regex.hpp>

#include <pbeast/exceptions.h>
#include <pbeast/input-data-file.h>
#include <pbeast/output-data-file.h>
#include <pbeast/downsample-data.h>

// copy raw primitive data serie
template<class T>
  void
  write_raw_primitive_data(const std::string &name, pbeast::OutputDataFile &to, std::vector<pbeast::SeriesData<T>> &data, uint64_t &count)
  {
    to.write_data(name, data);
    count += data.size() * 3 + 1;
    data.clear();
  }

// copy raw vector data serie
template<class T>
  void
  write_raw_vector_data(const std::string &name, pbeast::OutputDataFile &to, std::vector<pbeast::SeriesVectorData<T>> &data, uint64_t &count)
  {
    to.write_data(name, data);
    count += data.size() * 2 + 1;
    for (const auto &x : data)
      count += x.get_value().size();
    data.clear();
  }

// copy downsample primitive data serie
template<class T>
  void
  write_ds_primitive_data(const std::string &name, pbeast::OutputDataFile &to, std::vector<pbeast::SeriesData<T>> &data,
                          std::vector<pbeast::DownsampledSeriesData<T>> &downsampled_data, uint64_t &count)
  {
    downsample(to.get_downsample_interval(), data, downsampled_data, std::is_same<T, pbeast::Void>::value ? pbeast::FillGaps::None : pbeast::FillGaps::Near);
    to.write_data(name, downsampled_data);
    count += data.size() * 4 + 1;
    data.clear();
    downsampled_data.clear();
  }

// copy downsample vector data serie
template<class T>
  void
  write_ds_vector_data(const std::string &name, pbeast::OutputDataFile &to, std::vector<pbeast::SeriesVectorData<T>> &data,
                       std::vector<pbeast::DownsampledSeriesVectorData<T>> &downsampled_data, uint64_t &count)
  {
    downsample(to.get_downsample_interval(), data, downsampled_data, pbeast::FillGaps::Near);
    to.write_data(name, downsampled_data);
    count += data.size() * 2 + 1;
    for (const auto &x : data)
      count += 3 * x.get_value().size();
    data.clear();
    downsampled_data.clear();
  }

namespace pbeast {

  /**
   *  \brief Template implementation of input data file stream copy to output one.
   *
   *  It is used by the copy_file(pbeast::InputDataFile&, pbeast::OutputDataFile&) method.
   *
   *  Parameters:
   *  \param from      input data file stream
   *  \param to        output data file stream
   *  \param ids       only copy this serie
   *  \param re        only copy series with names matching given regular expression
   *  \return          number of copied primitive data types (used for monitoring)
   */

  template<class T>
    uint64_t
    copy(pbeast::InputDataFile& from, pbeast::OutputDataFile& to, const std::string& ids, boost::regex * re)
    {
      uint64_t count(0);
      std::string name;

      to.write_header();

      if (from.get_is_array() == false)
        {
          std::vector<pbeast::SeriesData<T> > data;

          if (to.is_downsampled() == false)
            {
              // copy all
              if (ids.empty())
                {
                  while (from.read_next(name, data))
                    {
                      write_raw_primitive_data(name, to, data, count);
                    }
                }
              // copy using regexp
              else if (re)
                {
                  for (const auto & x : from.get_series_info())
                    {
                      try
                        {
                          if (boost::regex_match(x.first, *re))
                            {
                              from.read_serie(x.second, data);
                              write_raw_primitive_data(x.first, to, data, count);
                            }
                        }
                      catch (std::exception& ex)
                        {
                          throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
                        }
                    }
                }
              // copy one
              else
                {
                  if (from.read_serie(ids, data))
                    {
                      write_raw_primitive_data(ids, to, data, count);
                    }
                }
            }
          else
            {
              std::vector<pbeast::DownsampledSeriesData<T> > downsampled_data;

              // copy all
              if (ids.empty())
                {
                  while (from.read_next(name, data))
                    {
                      write_ds_primitive_data(name, to, data, downsampled_data, count);
                    }
                }
              // copy using regexp
              else if (re)
                {
                  for (const auto & x : from.get_series_info())
                    {
                      try
                        {
                          if (boost::regex_match(x.first, *re))
                            {
                              from.read_serie(x.second, data);
                              write_ds_primitive_data(x.first, to, data, downsampled_data, count);
                            }
                        }
                      catch (std::exception& ex)
                        {
                          throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
                        }
                    }
                }
              // copy one
              else
                {
                  if (from.read_serie(ids, data))
                    {
                      write_ds_primitive_data(ids, to, data, downsampled_data, count);
                    }
                }
            }
        }
      else
        {
          std::vector<pbeast::SeriesVectorData<T> > data;

          if (to.is_downsampled() == false)
            {
              // copy all
              if (ids.empty())
                {
                  while (from.read_next(name, data))
                    {
                      write_raw_vector_data(name, to, data, count);
                    }
                }
              // copy using regexp
              else if (re)
                {
                  for (const auto & x : from.get_series_info())
                    {
                      try
                        {
                          if (boost::regex_match(x.first, *re))
                            {
                              from.read_serie(x.second, data);
                              write_raw_vector_data(x.first, to, data, count);
                            }
                        }
                      catch (std::exception& ex)
                        {
                          throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
                        }
                    }
                }
              // copy one
              else
                {
                  if (from.read_serie(ids, data))
                    {
                      write_raw_vector_data(ids, to, data, count);
                    }
                }
            }
          else
            {
              std::vector<pbeast::DownsampledSeriesVectorData<T> > downsampled_data;

              // copy all
              if (ids.empty())
                {
                  while (from.read_next(name, data))
                    {
                      write_ds_vector_data(name, to, data, downsampled_data, count);
                    }
                }
              // copy using regexp
              else if (re)
                {
                  for (const auto & x : from.get_series_info())
                    {
                      try
                        {
                          if (boost::regex_match(x.first, *re))
                            {
                              from.read_serie(x.second, data);
                              write_ds_vector_data(x.first, to, data, downsampled_data, count);
                           }
                        }
                      catch (std::exception& ex)
                        {
                          throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
                        }
                    }
                }
              // copy one
              else
                {
                  if (from.read_serie(ids, data))
                    {
                      write_ds_vector_data(ids, to, data, downsampled_data, count);
                    }
                }
            }
        }

      return count;
    }

  /**
   *  \brief Makes a copy of input data stream to output one.
   *
   *  Typically is used for downsampling
   *
   *  Parameters:
   *  \param from      input data file stream
   *  \param to        output data file stream
   *  \return          total number of copied primitive data types (used for monitoring)
   */

  uint64_t
  copy_file(pbeast::InputDataFile& from, pbeast::OutputDataFile& to, const std::string& ids = "", boost::regex * re = nullptr);

}


#endif
