#ifndef PBEAST_DATA_FILE_H
#define PBEAST_DATA_FILE_H

#include <stdint.h>
#include <string>

#include <filesystem>

namespace pbeast {

  inline std::string
  extract_file_name(const std::string& path)
  {
    return path.substr(path.find_last_of('/') + 1);
  }


  /**
   * \brief The DataFile is a base class to define common properties of P-BEAST data file streams.
   *
   *  There are two derived classes:
   *  - InputDataFile to create an input data file stream
   *  - OutputDataFile to create an output data file stream
   *
   *  The common properties are:
   *  - name of the file (fully qualified file name)
   *  - names of the information service partition, class and attribute
   *  - class path used for nested types
   *  - base timestamp is a timestamp of oldest data (in microseconds since Epoch); other data are stored with timestamps relative to the base one to profit from "Varints" encoding technique)
   *  - type of data as defined by pbeast::DataType and is_array property
   *  - number of data series
   *  - data compression for catalog and data (catalogization of "string" data type)
   *  - downsampling interval
   */

  class DataFile
  {

  public:

    /**
     *  \brief Encode string excluding characters not allowed by EOS. Is used for file names.
     *
     *  The alpha-numeric and few more symbols (dot, minus, underscore, colon and caret) remain unchanged.
     *  Others are encoded using hash and hexadecimal number, e.g. space symbol is encoded as "#20".
     *
     *  Parameters:
     *  \param str the input string
     *
     *  \return encoded string
     */

    static std::string
    encode(const std::string& str) noexcept;

    /**
     *  \brief Encode path excluding characters not allowed by EOS.
     *
     *  Use encode(const std::string&) for paths (i.e. keep path separator unmodified)
     *
     *  Parameters:
     *  \param path the input path
     *
     *  \return encoded path string
     */

    static std::string
    encode(const std::filesystem::path& path) noexcept;

    /**
     *  \brief Decode string encoded using encode(const std::string&) method.
     *
     *  Parameters:
     *  \param str the input encoded string
     *  \param escape_symbol the input escape symbol (use # for encoded file names and % for URLs)
     *
     *  \return decoded string
     *
     *  \throw std::runtime_error in case of an error
     */

    static std::string
    decode(const std::string& str, const unsigned char escape_symbol = '#');


    /**
     *  \brief Validate standard partition, class and attribute parameters.
     *
     *  Check encoded parameter length is shorter NAME_MAX to avoid exceptions from std::filesystem methods.
     *
     *  Parameters:
     *  \param partition_name name of partition
     *  \param class_name name of class
     *  \param attribute_path path to attribute
     *
     *  \throw daq::pbeast::BadParameter in case of an error
     */

    static void
    validate_parameters(const std::string& partition_name, const std::string& class_name, const std::filesystem::path& attribute_path);

  protected:

    DataFile(const std::string& file_name) : m_file_name(file_name) { ; }

    DataFile(
        const std::string& file_name,
        const std::string& partition,
        const std::string& class_name,
        const std::filesystem::path& class_path,
        const std::string& attribute,
        uint64_t base_timestamp,
        uint32_t data_type,
        bool is_array,
        uint32_t number_of_series,
        bool zip_catalog,
        bool zip_data,
        bool catalogize_strings,
        uint32_t downsample_interval
    ) :
        m_file_name          (file_name),
        m_file_version       (6),             // current pBEAST version
        m_partition          (partition),
        m_class              (class_name),
        m_class_path         (class_path),
        m_attribute          (attribute),
        m_base_timestamp     (base_timestamp),
        m_data_type          (data_type),
        m_is_array           (is_array),
        m_number_of_series   (number_of_series),
        m_start_of_names     (0),
        m_zip_catalog        (zip_catalog),
        m_zip_data           (zip_data),
        m_catalogize_strings (catalogize_strings),
        m_downsample_interval(downsample_interval),
        m_file_length        (0),
        m_info_size          (0),
        m_duration           (std::numeric_limits<uint32_t>::max())
    {
      ;
    }

  public:

    const std::string&
    get_file_name() const
    {
      return m_file_name;
    }

    uint32_t
    get_file_version() const
    {
      return m_file_version;
    }

    const std::string&
    get_partition() const
    {
      return m_partition;
    }

    const std::string&
    get_class() const
    {
      return m_class;
    }

    const std::filesystem::path&
    get_class_path() const
    {
      return m_class_path;
    }

    const std::string&
    get_attribute() const
    {
      return m_attribute;
    }

    uint64_t
    get_base_timestamp() const
    {
      return m_base_timestamp;
    }

    uint32_t
    get_data_type() const
    {
      return m_data_type;
    }

    bool
    get_is_array() const
    {
      return m_is_array;
    }

    uint32_t
    get_number_of_series() const
    {
      return m_number_of_series;
    }

    bool
    get_zip_catalog_flag() const
    {
      return m_zip_catalog;
    }

    bool
    get_zip_data_flag() const
    {
      return m_zip_data;
    }

    bool
    get_catalogize_strings_flag() const
    {
      return m_catalogize_strings;
    }

    uint32_t
    get_downsample_interval() const
    {
      return m_downsample_interval;
    }

    bool
    is_downsampled() const
    {
      return (m_downsample_interval != 0);
    }

    uint64_t
    get_start_of_names() const
    {
      return m_start_of_names;
    }

    uint64_t
    get_file_length() const
    {
      return m_file_length;
    }

    /// @return size of header and catalog
    uint64_t
    get_info_size() const
    {
      return m_info_size;
    }

    // return duration calculated from name
    uint32_t
    get_duration() const;


  protected:

    std::string m_file_name;
    uint32_t m_file_version;
    std::string m_partition;
    std::string m_class;
    std::filesystem::path m_class_path;
    std::string m_attribute;
    uint64_t m_base_timestamp;
    uint32_t m_data_type;
    bool m_is_array;
    uint32_t m_number_of_series;
    uint64_t m_start_of_names;
    bool m_zip_catalog;
    bool m_zip_data;
    bool m_catalogize_strings;
    uint32_t m_downsample_interval;
    uint64_t m_file_length;
    uint64_t m_info_size;
    mutable uint32_t m_duration;

    static constexpr uint64_t c_block_size = 64;

  };
}

#endif
