#ifndef PBEAST_INPUT_DATA_FILE_H
#define PBEAST_INPUT_DATA_FILE_H

#include <stdint.h>
#include <string>
#include <sstream>
#include <map>

#include <boost/regex.hpp>

#include "pbeast/call-stats.h"
#include "pbeast/input-stream.h"
#include "pbeast/data-file.h"
#include "pbeast/series-data.h"
#include "pbeast/series-info.h"

namespace pbeast
{
  struct File;

  class InputDataFileBase : public DataFile
  {
  public:

    InputDataFileBase(const std::string& file_name) :
        DataFile(file_name)
    {
      ;
    }

    InputDataFileBase(const DataFile& data_file, const std::shared_ptr<std::map<std::string, uint64_t>>& series_info) :
        DataFile(data_file), m_series_info(series_info)
    {
      ;
    }

    InputDataFileBase(const DataFile& data_file, const std::vector<SeriesInfo *>& series_info) :
        DataFile(data_file)
    {
      init_series_info();

      for (const auto& x : series_info)
        (*m_series_info)[x->get_name()] = x->get_data_position();
    }

    const std::map<std::string, uint64_t>&
    get_series_info() const
    {
      return *m_series_info;
    }

    const std::shared_ptr<std::map<std::string, uint64_t>>&
    get_series_info_ptr() const
    {
      return m_series_info;
    }

  protected:

    void
    init_series_info()
    {
      m_series_info = std::make_shared<std::map<std::string, uint64_t>>();
    }

    std::shared_ptr<std::map<std::string, uint64_t>> m_series_info;
  };

  class InputDataFile : public InputDataFileBase
  {

  public:

    InputDataFile(const std::string& file_name) :
        InputDataFileBase(file_name), m_string_stream(nullptr), m_input_stream(nullptr), m_it_inited(false), m_total_data_size(0)
    {
      read_info();
    }

    InputDataFile(std::stringstream * stream) :
        InputDataFileBase(""), m_string_stream(stream), m_input_stream(nullptr), m_it_inited(false)
    {
      read_info();
    }

    InputDataFile(const std::filesystem::path& path, const std::string& name, pbeast::File& file, CallStats& stat);

    ~InputDataFile()
    {
      close();
    }

    void
    open()
    {
      if (!m_input_stream)
        {
          m_input_stream = (!m_string_stream ? new pbeast::InputStream(m_file_name) : new pbeast::InputStream(m_string_stream));
          m_it_inited = false;
        }
    }

    void
    close()
    {
      if (m_input_stream)
        {
          delete m_input_stream;
          m_input_stream = nullptr;
        }
    }

    InputDataFile(const InputDataFile& other) = delete;
    InputDataFile&
    operator=(const InputDataFile&) = delete;

    void
    read_info();

    inline void
    read_timestamp(uint64_t &ts_created, uint64_t &prev_ts, uint64_t &prev_delta)
    {
      const uint64_t delta = prev_delta;
      uint64_t diff;
      m_input_stream->read(diff);
      prev_delta = diff ^ delta;
      prev_ts = ts_created = prev_ts + prev_delta;
    }

    inline void
    read_timestamp(uint64_t &ts_created, uint64_t &prev_ts, uint64_t &prev_delta, bool& flag)
    {
     const uint64_t delta = prev_delta;
      uint64_t diff;
      m_input_stream->read(diff);

      flag = diff & 1U;
      diff >>= 1;

      prev_delta = diff ^ delta;
      prev_ts = ts_created = prev_ts + prev_delta;
    }

    inline void
    read_timestamp(uint64_t &ts_created, uint64_t &prev_ts, uint64_t &prev_delta, bool& flag1, bool& flag2)
    {
      const uint64_t delta = prev_delta;
      uint64_t diff;
      m_input_stream->read(diff);

      flag1 = diff & 1U;
      flag2 = (diff >> 1) & 1U;
      diff >>= 2;

      prev_delta = diff ^ delta;
      prev_ts = ts_created = prev_ts + prev_delta;
    }

    inline void
    read_interval(uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t &prev_delta)
    {
      uint64_t diff;
      m_input_stream->read(diff);

      if (diff & 1UL)
        m_input_stream->read(ts_last_updated);
      else
        ts_last_updated = 0;

      diff >>= 1;

      const uint64_t delta = prev_delta;
      prev_delta = diff ^ delta;
      prev_ts = ts_created = prev_ts + prev_delta;
      ts_last_updated += ts_created;
    }

    inline void
    read_interval_impl(uint64_t diff, uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t &prev_delta, bool& flag)
    {
      if ((diff >> 1) & 1UL)
        m_input_stream->read(ts_last_updated);
      else
        ts_last_updated = 0;

      flag = (diff & 1U);
      diff >>= 2;

      const uint64_t delta = prev_delta;
      prev_delta = diff ^ delta;
      prev_ts = ts_created = prev_ts + prev_delta;
      ts_last_updated += ts_created;
    }

    inline void
    read_interval(uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t &prev_delta, bool& flag)
    {
      uint64_t diff;
      m_input_stream->read(diff);

      read_interval_impl(diff, ts_created, ts_last_updated, prev_ts, prev_delta, flag);
    }

    inline void
    read_block(uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t &prev_delta, bool& intervals)
    {
      uint64_t diff;
      m_input_stream->read(diff);

      intervals = (diff & 1UL);

       if (intervals)
         {
           if ((diff >> 1) & 1U)
             m_input_stream->read(ts_last_updated);
           else
             ts_last_updated = 0;

           diff >>= 2;
         }
       else
         {
           ts_last_updated = 0;

           diff >>= 1;
         }

       const uint64_t delta = prev_delta;
       prev_delta = diff ^ delta;
       prev_ts = ts_created = prev_ts + prev_delta;

       ts_last_updated += ts_created;


    }

    inline void
    read_block_impl(uint64_t diff, uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t &prev_delta, bool& intervals, bool& flag)
    {
      intervals = (diff & 1UL);

      if (intervals)
        {
          flag = (diff >> 1) & 1U;

          if ((diff >> 2) & 1UL)
            m_input_stream->read(ts_last_updated);
          else
            ts_last_updated = 0;

          diff >>= 3;
        }
      else
        {
          flag = (diff >> 1) & 1U;
          ts_last_updated = 0;

          diff >>= 2;
       }

      const uint64_t delta = prev_delta;
      prev_delta = diff ^ delta;
      prev_ts = ts_created = prev_ts + prev_delta;

      ts_last_updated += ts_created;
    }

    inline void
    read_block(uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t &prev_delta, bool& intervals, bool& flag)
    {
      uint64_t diff;
      m_input_stream->read(diff);
      read_block_impl(diff, ts_created, ts_last_updated, prev_ts, prev_delta, intervals, flag);
    }

    template<typename T>
      void
      read_timestamps(uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t& prev_delta)
      {
        if (m_file_version > 4)
          {
            const uint64_t delta = prev_delta;
            uint64_t created;
            m_input_stream->read(created);

            if(created & 1UL)
              m_input_stream->read(ts_last_updated);
            else
              ts_last_updated = 0;

            created >>= 1;

            prev_delta = created ^ delta;
            ts_created = prev_ts + prev_delta;
          }
        else
          {
            m_input_stream->read(ts_created);
            ts_created += prev_ts;
            m_input_stream->read(ts_last_updated);
          }

        ts_last_updated += ts_created;

        if (m_file_version > 3)
          prev_ts = ts_last_updated;
      }

    bool
    read_ts(uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t& prev_delta)
    {
      const uint64_t delta = prev_delta;
      uint64_t created;
      m_input_stream->read(created);

      if(created & 1U)
        m_input_stream->read(ts_last_updated);
      else
        ts_last_updated = 0;

      bool b3 = ((created >> 1) & 1U);

      created >>= 2;

      prev_delta = created ^ delta;

      ts_created = prev_ts + prev_delta;
      ts_last_updated += ts_created;

      prev_ts = ts_last_updated;

      return b3;
    }

    inline void
    read_timestamp(uint32_t &ts, uint32_t &prev)
    {
      m_input_stream->read(ts);
      if (m_file_version > 3)
        {
          ts += prev + get_downsample_interval();
          prev = ts;
        }
    }

    template<class T>
      uint64_t
      read_serie(const std::string& name, std::vector<T>& data)
      {
        const auto it = m_series_info->find(name);

        if (it != m_series_info->end())
          {
            reset(m_zip_data, it->second);
            return read_data(data);
          }
        else
          {
            return 0;
          }
      }

    template<class V, class T>
      uint64_t
      read_serie(const std::string& name, V since, V until, std::vector<T>& data)
      {
        const auto it = m_series_info->find(name);

        if (it != m_series_info->end())
          {
            reset(m_zip_data, it->second);
            return read_data(since, until, data);
          }
        else
          {
            return 0;
          }
      }

    template<class T>
      uint64_t
      read_serie(uint64_t pos, std::vector<T>& data)
      {
        reset(m_zip_data, pos);
        return read_data(data);
      }

    template<class V, class T>
      uint64_t
      read_serie(uint64_t pos, V since, V until, std::vector<T>& data)
      {
        reset(m_zip_data, pos);
        return read_data(since, until, data);
      }

    inline void
    reset_read_next()
    {
      init_iterator(m_zip_data);
    }

    template<class T>
      bool
      read_next(std::string& name, std::vector<T>& data)
      {
        if (!m_it_inited)
          reset_read_next();

        if (m_it == m_series_info->end())
          return false;

        if (m_zip_data)
          m_input_stream->check_and_reset(true, m_it->second);
        else
          m_input_stream->check_and_reset(false);

        name = m_it->first;

        read_data(data);
        ++m_it;
        return true;
      }

    /// return size of every serie when it is read in-memory for merge operation (order is defined by m_series_info)
    std::shared_ptr<std::vector<uint64_t>>
    estimate_in_memory_merge_size();

    uint64_t
    get_num_of_series(const std::string& ids, boost::regex *re) const;

    uint64_t
    get_total_data_size() const
    {
      return m_total_data_size;
    }

    /**
     *   Read vector of boolean values stored as an array of 64 varints.
     */

    void
    read_bools(std::vector<bool>& vec, uint64_t size);

    void
    read_interval_and_bools(uint64_t& ts_created, uint64_t& ts_last_updated, uint64_t &ts_prev, uint64_t &ts_prev_delta, bool fixed_len_flag, std::vector<bool>& vec, std::vector<bool>& prev_vec);


  private:

    std::stringstream * m_string_stream;
    InputStream * m_input_stream;
    std::map<std::string, uint64_t>::const_iterator m_it;
    std::shared_ptr<std::vector<uint64_t>> m_in_memory_merge_size;
    bool m_it_inited;
    uint64_t m_total_data_size;


#define EMPTY_CONDITION                                                        \


#define RAW_DATA_POINT_CONDITION                                               \
  if (ts_last_updated < since) continue;                                       \
  else if (ts_created > until) {i = data_len; break;}                          \
  else


#define READ_SIMPLE_SERIES_RAW_DATA( CONDITION )                                                               \
    uint64_t data_len;                                                                                         \
    uint64_t ts_created, ts_last_updated;                                                                      \
    T val;                                                                                                     \
                                                                                                               \
    try                                                                                                        \
      {                                                                                                        \
        uint64_t prev_ts = m_base_timestamp;                                                                   \
        uint64_t prev_delta = 0;                                                                               \
        m_input_stream->read(data_len);                                                                        \
        data.reserve(data_len);                                                                                \
                                                                                                               \
        if (m_file_version > 4)                                                                                \
          {                                                                                                    \
            if constexpr (std::is_same_v<T, pbeast::Void>)                                                     \
              {                                                                                                \
                for (uint64_t i = 0; i < data_len; i++)                                                        \
                  {                                                                                            \
                    read_timestamp(ts_created, prev_ts, prev_delta) ;                                          \
                    ts_last_updated = ts_created;                                                              \
                                                                                                               \
                    CONDITION                                                                                  \
                      {                                                                                        \
                        data.emplace_back(ts_created, ts_created, pbeast::Void());                             \
                      }                                                                                        \
                  }                                                                                            \
              }                                                                                                \
            else                                                                                               \
              {                                                                                                \
                T prev_value, delta_value;                                                                     \
                if constexpr (std::is_integral_v<T>)                                                           \
                  prev_value = delta_value = 0;                                                                \
                                                                                                               \
                for (uint64_t i = 0; i < data_len; i += c_block_size)                                          \
                  {                                                                                            \
                    bool intervals(false);                                                                     \
                    const uint64_t end = std::min(data_len, i + c_block_size);                                 \
                                                                                                               \
                    for (uint64_t j = i; j < end; ++j)                                                         \
                      {                                                                                        \
                        if constexpr (std::is_integral_v<T>)                                                   \
                          {                                                                                    \
                            bool flag;                                                                         \
                            if (i == j)                                                                        \
                              {                                                                                \
                                read_block(ts_created, ts_last_updated, prev_ts, prev_delta, intervals, flag); \
                                /* m_input_stream->check_and_reset_object(m_zip_data); */                      \
                              }                                                                                \
                            else                                                                               \
                              {                                                                                \
                                if (intervals)                                                                 \
                                  {                                                                            \
                                    read_interval(ts_created, ts_last_updated, prev_ts, prev_delta, flag);     \
                                  }                                                                            \
                                else                                                                           \
                                  {                                                                            \
                                    read_timestamp(ts_created, prev_ts, prev_delta, flag);                     \
                                    ts_last_updated = ts_created;                                              \
                                  }                                                                            \
                              }                                                                                \
                                                                                                               \
                            if constexpr (std::is_same_v<T, bool>)                                             \
                              {                                                                                \
                                val = flag;                                                                    \
                              }                                                                                \
                            else if constexpr (std::is_unsigned_v<T>)                                          \
                              {                                                                                \
                                m_input_stream->read(val);                                                     \
                                const T dt = delta_value;                                                      \
                                if (flag == 1)                                                                 \
                                  val = ~val;                                                                  \
                                delta_value = val + dt;                                                        \
                                val = prev_value + delta_value;                                                \
                                prev_value = val;                                                              \
                              }                                                                                \
                            else                                                                               \
                              {                                                                                \
                                uint64_t p;  /* typename std::make_unsigned<T>::type p; // TODO after 2024 */  \
                                m_input_stream->read(p);                                                       \
                                val = static_cast<T>(p);                                                       \
                                if (flag)                                                                      \
                                  val = -val;                                                                  \
                                const T dt = delta_value;                                                      \
                                delta_value = dt + val;                                                        \
                                val = prev_value + delta_value;                                                \
                                prev_value = val;                                                              \
                              }                                                                                \
                          }                                                                                    \
                        else                                                                                   \
                          {                                                                                    \
                            if (i == j)                                                                        \
                              {                                                                                \
                                read_block(ts_created, ts_last_updated, prev_ts, prev_delta, intervals);       \
                                /* m_input_stream->check_and_reset_object(m_zip_data); */                      \
                              }                                                                                \
                            else                                                                               \
                              {                                                                                \
                                if (intervals)                                                                 \
                                  {                                                                            \
                                    read_interval(ts_created, ts_last_updated, prev_ts, prev_delta);           \
                                  }                                                                            \
                                else                                                                           \
                                  {                                                                            \
                                    read_timestamp(ts_created, prev_ts, prev_delta);                           \
                                    ts_last_updated = ts_created;                                              \
                                  }                                                                            \
                              }                                                                                \
                            m_input_stream->read(val);                                                         \
                          }                                                                                    \
                                                                                                               \
                                                                                                               \
                        CONDITION                                                                              \
                          {                                                                                    \
                            data.emplace_back(ts_created, ts_last_updated, val);                               \
                          }                                                                                    \
                      }                                                                                        \
                  }                                                                                            \
              }                                                                                                \
          }                                                                                                    \
        else                                                                                                   \
          {                                                                                                    \
            for (uint64_t i = 0; i < data_len; ++i)                                                            \
              {                                                                                                \
                read_timestamps<T>(ts_created, ts_last_updated, prev_ts, prev_delta);                          \
                m_input_stream->read(val);                                                                     \
                                                                                                               \
                /* m_input_stream->check_and_reset_object(m_zip_data); */                                      \
                                                                                                               \
                CONDITION                                                                                      \
                  {                                                                                            \
                    data.emplace_back(ts_created, ts_last_updated, val);                                       \
                  }                                                                                            \
              }                                                                                                \
          }                                                                                                    \
                                                                                                               \
        m_total_data_size += data_len;                                                                         \
                                                                                                               \
        return (1 + data_len * 3);                                                                             \
      }                                                                                                        \
    catch (const std::exception& ex)                                                                           \
      {                                                                                                        \
        throw std::runtime_error(make_exception_report("file", ex).c_str());                                   \
      }


    template<class T>
      uint64_t
      read_data(std::vector<SeriesData<T> >& data)
      {
        READ_SIMPLE_SERIES_RAW_DATA( EMPTY_CONDITION )
      }

    template<class T>
      uint64_t
      read_data(uint64_t since, uint64_t until, std::vector<SeriesData<T> >& data)
      {
        READ_SIMPLE_SERIES_RAW_DATA( RAW_DATA_POINT_CONDITION )
      }


    uint64_t
    read_data(std::vector<SeriesData<std::string> >& data);

    uint64_t
    read_data(uint64_t since, uint64_t until, std::vector<SeriesData<std::string> >& data);


#define READ_VECTOR_SERIES_RAW_DATA( CONDITION )                                                                                      \
    uint64_t data_len, vec_len;                                                                                                       \
    uint64_t ts_created, ts_last_updated;                                                                                             \
    T val;                                                                                                                            \
                                                                                                                                      \
    try                                                                                                                               \
      {                                                                                                                               \
        uint64_t prev_ts = m_base_timestamp;                                                                                          \
        uint64_t prev_delta = 0;                                                                                                      \
        m_input_stream->read(data_len);                                                                                               \
        data.reserve(data_len);                                                                                                       \
        const bool fixed_len_flag = (m_file_version > 4) ? (data_len & 1UL) : false;                                                  \
        std::vector<bool> const_indices;                                                                                              \
        uint64_t const_indices_len = 0;                                                                                               \
        uint64_t fixed_len = 0;                                                                                                       \
        std::vector<T> const_indices_val;                                                                                             \
        if (fixed_len_flag)                                                                                                           \
          {                                                                                                                           \
            m_input_stream->read(fixed_len);                                                                                          \
            vec_len = fixed_len;                                                                                                      \
                                                                                                                                      \
            if ((data_len >> 1) & 1U)                                                                                                 \
              {                                                                                                                       \
                if constexpr (!std::is_same_v<T, bool>)                                                                               \
                  {                                                                                                                   \
                    const_indices.assign(fixed_len, false);                                                                           \
                    m_input_stream->read(const_indices_len);                                                                          \
                    const_indices_val.reserve(const_indices_len);                                                                     \
                    for (uint64_t i = 0; i < const_indices_len; ++i)                                                                  \
                      {                                                                                                               \
                        uint64_t pos;                                                                                                 \
                        T val;                                                                                                        \
                        m_input_stream->read(pos);                                                                                    \
                        const_indices[pos] = true;                                                                                    \
                        m_input_stream->read(val);                                                                                    \
                        const_indices_val.emplace_back(val);                                                                          \
                      }                                                                                                               \
                  }                                                                                                                   \
              }                                                                                                                       \
            data_len >>= 2;                                                                                                           \
          }                                                                                                                           \
        else if (m_file_version > 4)                                                                                                  \
          data_len >>= 1;                                                                                                             \
                                                                                                                                      \
        uint64_t ret = 1 + data_len * 3;                                                                                              \
                                                                                                                                      \
        std::vector<T> prev_value, delta_value, dt;                                                                                   \
        std::vector<bool> bools, prev_bools;                                                                                          \
        uint64_t prev_len = 0;                                                                                                        \
                                                                                                                                      \
        if constexpr (std::is_integral_v<T>)                                                                                          \
          {                                                                                                                           \
            if constexpr (!std::is_same_v<T, bool>)                                                                                   \
              {                                                                                                                       \
                if (fixed_len_flag)                                                                                                   \
                  prev_bools.resize(fixed_len-const_indices_len, false);                                                              \
              }                                                                                                                       \
          }                                                                                                                           \
                                                                                                                                      \
        for (uint64_t i = 0; i < data_len; ++i)                                                                                       \
          {                                                                                                                           \
            std::vector<T> vec_val;                                                                                                   \
            if (m_file_version > 4)                                                                                                   \
              {                                                                                                                       \
                if constexpr (std::is_integral_v<T>)                                                                                  \
                  {                                                                                                                   \
                    if constexpr (std::is_same_v<T, bool>)                                                                            \
                      {                                                                                                               \
                        if (fixed_len_flag)                                                                                           \
                          {                                                                                                           \
                            read_interval(ts_created, ts_last_updated, prev_ts, prev_delta);                                          \
                          }                                                                                                           \
                        else                                                                                                          \
                          {                                                                                                           \
                            bool flag;                                                                                                \
                            read_interval(ts_created, ts_last_updated, prev_ts, prev_delta, flag);                                    \
                            if (flag)                                                                                                 \
                              {                                                                                                       \
                                m_input_stream->read(vec_len);                                                                        \
                                prev_len = vec_len;                                                                                   \
                              }                                                                                                       \
                            else                                                                                                      \
                              {                                                                                                       \
                                vec_len = prev_len;                                                                                   \
                              }                                                                                                       \
                          }                                                                                                           \
                        read_bools(vec_val, vec_len);                                                                                 \
                      }                                                                                                               \
                    else                                                                                                              \
                      {                                                                                                               \
                        read_interval_and_bools(ts_created, ts_last_updated, prev_ts, prev_delta, fixed_len_flag, bools, prev_bools); \
                        vec_len = bools.size();                                                                                       \
                        vec_val.reserve(vec_len);                                                                                     \
                        prev_value.resize(vec_len, 0);                                                                                \
                        delta_value.resize(vec_len, 0);                                                                               \
                        dt.resize(vec_len, 0);                                                                                        \
                                                                                                                                      \
                        uint64_t k = 0;                                                                                               \
                        for (uint64_t j = 0; j < vec_len + const_indices_len; ++j)                                                    \
                          {                                                                                                           \
                            if (const_indices_len && const_indices[j])                                                                \
                              {                                                                                                       \
                                val = const_indices_val[k++];                                                                         \
                              }                                                                                                       \
                            else                                                                                                      \
                              {                                                                                                       \
                                const uint64_t idx = j - k;                                                                           \
                                if constexpr (std::is_unsigned_v<T>)                                                                  \
                                  {                                                                                                   \
                                    m_input_stream->read(val);                                                                        \
                                    if (bools[idx])                                                                                   \
                                      val = ~val;                                                                                     \
                                  }                                                                                                   \
                                else                                                                                                  \
                                  {                                                                                                   \
                                    uint64_t p; /* typename std::make_unsigned<T>::type p; // TODO post 2024 release after data migration*/ \
                                    m_input_stream->read(p);                                                                          \
                                    val = static_cast<T>(p);                                                                          \
                                    if (bools[idx])                                                                                   \
                                      val = -val;                                                                                     \
                                  }                                                                                                   \
                                                                                                                                      \
                                dt[idx] = delta_value[idx];                                                                           \
                                delta_value[idx] = val + dt[idx];                                                                     \
                                val = delta_value[idx] + prev_value[idx];                                                             \
                                prev_value[idx] = val;                                                                                \
                              }                                                                                                       \
                                                                                                                                      \
                            vec_val.emplace_back(val);                                                                                \
                          }                                                                                                           \
                      }                                                                                                               \
                  }                                                                                                                   \
                else                                                                                                                  \
                  {                                                                                                                   \
                    if (fixed_len_flag)                                                                                               \
                      {                                                                                                               \
                        read_interval(ts_created, ts_last_updated, prev_ts, prev_delta);                                              \
                      }                                                                                                               \
                    else                                                                                                              \
                      {                                                                                                               \
                        bool flag;                                                                                                    \
                        read_interval(ts_created, ts_last_updated, prev_ts, prev_delta, flag);                                        \
                        if (flag)                                                                                                     \
                          {                                                                                                           \
                            m_input_stream->read(vec_len);                                                                            \
                            prev_len = vec_len;                                                                                       \
                          }                                                                                                           \
                        else                                                                                                          \
                          {                                                                                                           \
                            vec_len = prev_len;                                                                                       \
                          }                                                                                                           \
                      }                                                                                                               \
                                                                                                                                      \
                    vec_val.reserve(vec_len);                                                                                         \
                                                                                                                                      \
                                                                                                                                      \
                    for (uint64_t j = 0, k = 0; j < vec_len; ++j)                                                                     \
                      {                                                                                                               \
                        if (const_indices_len && const_indices[j])                                                                    \
                          val = const_indices_val[k++];                                                                               \
                        else                                                                                                          \
                          m_input_stream->read(val);                                                                                  \
                                                                                                                                      \
                        vec_val.emplace_back(val);                                                                                    \
                      }                                                                                                               \
                  }                                                                                                                   \
              }                                                                                                                       \
            else                                                                                                                      \
              {                                                                                                                       \
                read_timestamps<T>(ts_created, ts_last_updated, prev_ts, prev_delta);                                                 \
                m_input_stream->read(vec_len);                                                                                        \
                if (vec_len > 0)                                                                                                      \
                  {                                                                                                                   \
                    vec_val.reserve(vec_len);                                                                                         \
                    for (uint64_t j = 0; j < vec_len; ++j)                                                                            \
                      {                                                                                                               \
                        m_input_stream->read(val);                                                                                    \
                        vec_val.emplace_back(val);                                                                                    \
                      }                                                                                                               \
                  }                                                                                                                   \
              }                                                                                                                       \
                                                                                                                                      \
            m_input_stream->check_and_reset_object(m_zip_data);                                                                       \
            ret += vec_len;                                                                                                           \
                                                                                                                                      \
            CONDITION                                                                                                                 \
              {                                                                                                                       \
                data.emplace_back(ts_created, ts_last_updated, vec_val);                                                              \
              }                                                                                                                       \
          }                                                                                                                           \
                                                                                                                                      \
        m_total_data_size += data_len;                                                                                                \
                                                                                                                                      \
        return ret;                                                                                                                   \
      }                                                                                                                               \
    catch (const std::exception& ex)                                                                                                  \
      {                                                                                                                               \
        throw std::runtime_error(make_exception_report("file", ex).c_str());                                                          \
      }


    template<class T>
      uint64_t
      read_data(std::vector<SeriesVectorData<T> >& data)
      {
        READ_VECTOR_SERIES_RAW_DATA( EMPTY_CONDITION )
      }

    template<class T>
      uint64_t
      read_data(uint64_t since, uint64_t until, std::vector<SeriesVectorData<T> >& data)
      {
        READ_VECTOR_SERIES_RAW_DATA( RAW_DATA_POINT_CONDITION )
      }

    template<class T>
      uint64_t
      read_data(std::vector<DownsampledSeriesData<T> >& data)
      {
        uint32_t data_len;
        uint32_t ts;
        DownsampleValue<T> val;

        try
          {
            uint32_t prev = 0;

            m_input_stream->read(data_len);
            data.reserve(data_len);

            for (uint32_t i = 0; i < data_len; ++i)
              {
                read_timestamp(ts, prev);
                m_input_stream->read(val.m_min_value);
                m_input_stream->read(val.m_value);
                m_input_stream->read(val.m_max_value);
                data.emplace_back(ts, val);
              }

            m_total_data_size += data_len;

            return (4 * data_len + 1);
          }
        catch (const std::exception& ex)
          {
            throw std::runtime_error(make_exception_report("file", ex).c_str());
          }
      }

    uint64_t
    read_data(std::vector<DownsampledSeriesData<std::string> >& data);


    template<class T>
      uint64_t
      read_data(std::vector<DownsampledSeriesVectorData<T> >& data)
      {
      uint32_t data_len, vec_len;
      uint32_t ts;
      DownsampleValue<T> val;

      try
        {
          uint32_t prev = 0;

          m_input_stream->read(data_len);
          data.reserve(data_len);

          uint64_t ret = 1 + data_len * 2;

          for (uint32_t i = 0; i < data_len; ++i)
            {
              std::vector<DownsampleValue<T>> vec_val;

              read_timestamp(ts, prev);
              m_input_stream->read(vec_len);

              if (vec_len > 0)
                {
                  vec_val.reserve(vec_len);

                  ret += vec_len * 3;

                  for (uint32_t j = 0; j < vec_len; ++j)
                    {
                      m_input_stream->read(val.m_min_value);
                      m_input_stream->read(val.m_value);
                      m_input_stream->read(val.m_max_value);
                      vec_val.emplace_back(val);
                    }
                }

              data.emplace_back(ts, vec_val);

              m_input_stream->check_and_reset_object(m_zip_data);
            }

          m_total_data_size += data_len;

          return ret;
        }
      catch (const std::exception& ex)
        {
          throw std::runtime_error(make_exception_report("file", ex).c_str());
        }
      }


    inline void
    reset(bool use_zip, uint64_t pos)
    {
      m_input_stream->reset(use_zip, pos);
    }

    inline void
    init_iterator(bool use_zip)
    {
      m_it = m_series_info->begin();
      m_it_inited = true;
      reset(use_zip, m_it->second);
    }

    std::string
    make_exception_report(const char * what, const std::exception& ex) const;

  };


  template<>
    void
    InputDataFile::read_timestamps<pbeast::Void>(uint64_t &ts_created, uint64_t &ts_last_updated, uint64_t &prev_ts, uint64_t& prev_delta);


  // first try to order set of files by base time-stamp and if equal, order by filename

  struct filename_compare
  {
    bool
    operator()(const std::unique_ptr<pbeast::InputDataFile>& lhs, const std::unique_ptr<pbeast::InputDataFile>& rhs) const
    {
      if (lhs->get_base_timestamp() != rhs->get_base_timestamp())
        return lhs->get_base_timestamp() < rhs->get_base_timestamp();

      return pbeast::extract_file_name(lhs->get_file_name()) < pbeast::extract_file_name(rhs->get_file_name());
    }
  };

  typedef std::set<std::unique_ptr<pbeast::InputDataFile>, filename_compare> FileSet;

  struct FileOpenGuard
  {
    FileOpenGuard(pbeast::InputDataFile * file) : m_file(file)
    {
      m_file->open();
    }

    ~FileOpenGuard()
    {
      m_file->close();
    }

    pbeast::InputDataFile * m_file;
  };
}

#endif
