#ifndef PBEAST_INPUT_STREAM_H
#define PBEAST_INPUT_STREAM_H

#include <stdint.h>

#include <memory>
#include <sstream>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/io/zero_copy_stream.h>
#include <google/protobuf/io/zero_copy_stream_impl.h>
#include <google/protobuf/io/gzip_stream.h>
#include <google/protobuf/wire_format_lite.h>
#if GOOGLE_PROTOBUF_VERSION < 3007000
#include <google/protobuf/wire_format_lite_inl.h>
#endif

#include "pbeast/data-type.h"

namespace pbeast
{

  class InputStream
  {
    enum {
      c_stream_size_limit = 0x1FFFFFFF
    };

  public:

    InputStream(const std::string& file);
    InputStream(std::stringstream * stream);

    ~InputStream();

    void
    check_and_reset(bool uze_zip, int64_t limit = c_stream_size_limit);

    void
    reset(bool uze_zip, int64_t position = -1);

    inline void
    check_and_reset_object(bool m_zip_data)
    {
      if (!m_zip_data)
        {
          check_and_reset(false);
        }
      else
        {
          check_and_reset_zip_stream();
        }
    }

    void
    seek_pos(int64_t offset);

    void
    seek_end(int64_t offset);

    int64_t
    tell_pos();

    inline void
    read(std::string& v, uint64_t len)
    {
      std::unique_ptr<char[]> buffer(new char[len]);
      if (__builtin_expect(!m_coded_input->ReadRaw(buffer.get(), len), 0))
        throw std::runtime_error("string read error");
      v.assign(buffer.get(), len);
    }

    inline void
    read(std::string& v)
    {
      uint32_t size;
      read(size);
      read(v, size);
    }

    inline void
    read(bool& v)
    {
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<bool, google::protobuf::internal::WireFormatLite::TYPE_BOOL>(m_coded_input, &v), 0))
        throw std::runtime_error("bool read error");
    }

    inline void
    read(int8_t& v)
    {
      int32_t t(0);
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::int32, google::protobuf::internal::WireFormatLite::TYPE_SINT32>(m_coded_input, &t), 0))
        throw std::runtime_error("int8_t read error");
      v = static_cast<int8_t>(t);
    }

    inline void
    read(uint8_t& v)
    {
      uint32_t t;
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::uint32, google::protobuf::internal::WireFormatLite::TYPE_UINT32>(m_coded_input, &t), 0))
        throw std::runtime_error("uint8_t read error");
      v = static_cast<uint8_t>(t);
    }

    inline void
    read(int16_t& v)
    {
      int32_t t(0);
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::int32, google::protobuf::internal::WireFormatLite::TYPE_SINT32>(m_coded_input, &t), 0))
        throw std::runtime_error("int16_t read error");
      v = static_cast<int16_t>(t);
    }

    inline void
    read(uint16_t& v)
    {
      uint32_t t;
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::uint32, google::protobuf::internal::WireFormatLite::TYPE_UINT32>(m_coded_input, &t), 0))
        throw std::runtime_error("uint16_t read error");
      v = static_cast<uint16_t>(t);
    }

    inline void
    read(int32_t& v)
    {
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::int32, google::protobuf::internal::WireFormatLite::TYPE_SINT32>(m_coded_input, &v), 0))
        throw std::runtime_error("int32_t read error");
    }

    inline void
    read(uint32_t& v)
    {
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::uint32, google::protobuf::internal::WireFormatLite::TYPE_UINT32>(m_coded_input, &v), 0))
        throw std::runtime_error("uint32_t read error");
    }

    inline void
    read(int64_t& v)
    {
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::int64, google::protobuf::internal::WireFormatLite::TYPE_SINT64>(m_coded_input, &v), 0))
        throw std::runtime_error("int64_t read error");
    }

    inline void
    read(uint64_t& v)
    {
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::uint64, google::protobuf::internal::WireFormatLite::TYPE_UINT64>(m_coded_input, &v), 0))
        throw std::runtime_error("uint64_t read error");
    }

    inline void
    read(float& v)
    {
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<float, google::protobuf::internal::WireFormatLite::TYPE_FLOAT>(m_coded_input, &v), 0))
        throw std::runtime_error("float read error");
    }

    inline void
    read(double& v)
    {
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<double, google::protobuf::internal::WireFormatLite::TYPE_DOUBLE>(m_coded_input, &v), 0))
        throw std::runtime_error("double read error");
    }

    inline void
    read(Void&)
    {
      ;
    }

    // fixed 4 / 8 bytes

    inline void
    read32(uint32_t& v)
    {
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::uint32, google::protobuf::internal::WireFormatLite::TYPE_FIXED32>(m_coded_input, &v), 0))
        throw std::runtime_error("32-bits int read error");
    }

    inline void
    read64(uint64_t& v)
    {
      if (__builtin_expect(!google::protobuf::internal::WireFormatLite::ReadPrimitive<google::protobuf::uint64, google::protobuf::internal::WireFormatLite::TYPE_FIXED64>(m_coded_input, &v), 0))
        throw std::runtime_error("64-bits int read error");
    }

    const std::string&
    get_file_name() const
    {
      const static std::string s_string_stream("(stringstream)");
      return (!m_string_stream ? m_file : s_string_stream);
    }

    void
    clear_ss_eof();

  private:

    void
    create_streams(bool use_zip);

    void
    delete_streams();

    void
    check_and_reset_zip_stream();

    int m_fd;
    std::string m_file;
    std::stringstream * m_string_stream;

    google::protobuf::io::ZeroCopyInputStream * m_raw_input;
    google::protobuf::io::CodedInputStream * m_coded_input;
    google::protobuf::io::GzipInputStream * m_zip_input;

    int64_t m_seek_pos;
  };
}

#endif
