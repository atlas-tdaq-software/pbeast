#ifndef PBEAST_SERIES_DATA_CVT_H
#define PBEAST_SERIES_DATA_CVT_H

#include <sstream>

#include "pbeast/series-data.h"

namespace pbeast {

    // convert numerics to numerics

  template <class F, class T> void convert( const std::vector< SeriesData<F> >& from, std::vector< SeriesData<T> > & to ) {
    const unsigned int num(from.size());
    to.reserve(num);
    for(unsigned int i = 0 ; i < num; ++i) {
      to.push_back(pbeast::SeriesData<T>(from[i].get_ts_created(), from[i].get_ts_last_updated(), static_cast<T>(from[i].get_value())));
    }
  }


    // convert numerics to strings

  template <class F> void convert( const std::vector< SeriesData<F> >& from, std::vector< SeriesData<std::string> > & to ) {
    const unsigned int num(from.size());
    to.reserve(num);
    std::ostringstream s;
    for(unsigned int i = 0 ; i < num; ++i) {
      s << from[i].get_value();
      to.push_back(pbeast::SeriesData<std::string>(from[i].get_ts_created(), from[i].get_ts_last_updated(), s.str()));
      s.str("");
      s.seekp(0);
    }
  }


    // convert strings to numerics

  template <class T> void convert( const std::vector< SeriesData<std::string> >& from, std::vector< SeriesData<T> > & to ) {
    const unsigned int num(from.size());
    to.reserve(num);
    for(unsigned int i = 0 ; i < num; ++i) {
      T val;
      std::istringstream s(from[i].get_value());
      s >> val;
      to.push_back(pbeast::SeriesData<T>(from[i].get_ts_created(), from[i].get_ts_last_updated(), val));
    }
  }


    // copy strings to strings

  void convert( const std::vector< SeriesData<std::string> >& from, std::vector< SeriesData<std::string> > & to ) {
    to = from;
  }

}

#endif
