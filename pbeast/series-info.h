#ifndef PBEAST_SERIES_INFO_H
#define PBEAST_SERIES_INFO_H

namespace pbeast
{

  class InputStream;

  class SeriesInfo
  {

  public:

    SeriesInfo(const std::string& name, uint64_t dp) :
        m_name(name), m_data_pos(dp)
    {
      ;
    }

    const std::string&
    get_name() const
    {
      return m_name;
    }

    uint64_t
    get_data_position() const
    {
      return m_data_pos;
    }

  private:

    std::string m_name;
    uint64_t m_data_pos;

  };

}

#endif
