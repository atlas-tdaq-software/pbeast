#ifndef PBEAST_FUNCTIONS_H
#define PBEAST_FUNCTIONS_H

#include <map>
#include <string>
#include <vector>
#include <cstdint>

namespace pbeast
{

  enum AggregationType
  {
    AggregationSum = 1, AggregationAverage = 2, AggregationMin = 4, AggregationMax = 8, AggregationNone = 0
  };

  enum FunctionType
  {
    SelectAverageAbove, SelectAverageBelow, SelectCurrentAbove, SelectCurrentBelow, SelectHighestAverage, SelectHighestCurrent, SelectHighestMax, SelectLowestAverage, SelectLowestCurrent, SumArray, Summarize, RemoveAboveValue, RemoveBelowValue, RemoveCorrupted
  };

  enum AliasType
  {
    Alias, AliasByMetric, AliasByNode, AliasSub
  };

  struct FunctionConfig
  {
#ifndef SWIGJAVA
    inline static const std::string s_aggegation_min = "min";
    inline static const std::string s_aggegation_max = "max";
    inline static const std::string s_aggegation_sum = "sum";
    inline static const std::string s_aggegation_avg = "average";

    inline static const std::string s_select_average_above = "select_average_above";
    inline static const std::string s_select_average_below = "select_average_below";
    inline static const std::string s_select_current_above = "select_current_above";
    inline static const std::string s_select_current_below = "select_current_below";
    inline static const std::string s_select_highest_average = "select_highest_average";
    inline static const std::string s_select_highest_current = "select_highest_current";
    inline static const std::string s_select_highest_max = "select_highest_max";
    inline static const std::string s_select_lowest_average = "select_lowest_average";
    inline static const std::string s_select_lowest_current = "select_lowest_current";
    inline static const std::string s_sum_array = "sum_array";
    inline static const std::string s_summarize = "summarize";
    inline static const std::string s_remove_above_value = "remove_above_value";
    inline static const std::string s_remove_below_value = "remove_below_value";
    inline static const std::string s_remove_corrupted = "remove_corrupted";

    inline static const std::string s_alias = "alias";
    inline static const std::string s_alias_by_metric = "alias_by_metric";
    inline static const std::string s_alias_by_node = "alias_by_node";
    inline static const std::string s_alias_sub = "alias_sub";
#endif

    FunctionConfig() :
      m_types(0), m_keep_data(false), m_use_summarize(false), m_interval(0)
    {
      ;
    }

    FunctionConfig(const std::string& stream, uint32_t interval) :
        m_types(0), m_keep_data(false), m_use_summarize(false), m_interval(0)
    {
      construct(stream, interval);
    }

    void
    construct(const std::string& stream, uint32_t interval);

    void
    construct(const std::vector<std::string>& command_line, uint32_t interval);

    void
    set_interval(uint32_t val) const
    {
      m_interval = val;
    }


  private:

    void
    validate(AggregationType type, const std::string& as);

    void
    validate(FunctionType type, const std::string& value);

    void
    validate(AliasType type, const std::string& value);

    void
    validate(const std::string& command_line);

    void
    validate(uint32_t interval) const;

  public:

    bool
    no_aggregations() const
    {
      return (m_types == 0);
    }

    bool
    use_summarize() const
    {
      return m_use_summarize;
    }

    bool
    is_null() const
    {
      return (no_aggregations() && m_data.empty() && m_aliases.empty());
    }

    AggregationType
    get_type(const std::string& s) const;

    std::string
    to_string() const;

    static std::string
    make_list_of_alias_names();

    static std::string
    make_list_of_function_names();

    static std::string
    make_list_of_aggregation_names();

    static std::string
    make_description_of_functions();

    static const std::string&
    function_name(AliasType type);

    static const std::string&
    function_name(FunctionType type);

    static char
    get_value_separator();

    // aggregations config
    uint64_t m_types;
    bool m_keep_data;
    std::map<std::string,AggregationType> m_data_names;

    // alias config
    std::vector<std::pair<AliasType,std::string>> m_aliases;

    // other functions
    std::vector<std::pair<FunctionType,std::string>> m_data;
    bool m_use_summarize;
    mutable uint32_t m_interval;
  };
}


#endif
