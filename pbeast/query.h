#ifndef PBEAST_QUERY_H
#define PBEAST_QUERY_H

#include <time.h>

#include <filesystem>

#include <ers/ers.h>

#include <pbeast/exceptions.h>
#include <pbeast/data-type.h>
#include <pbeast/series-data.h>
#include <pbeast/data-point.h>
#include <pbeast/functions.h>
#include <pbeast/downsample-fill-gaps.h>

struct curl_slist;

namespace pbeast
{

  /**
   * \brief Describes time series data returned by the ServerProxy::get_data() method.
   */

  class QueryDataBase
  {

  public:

    /**
     * Get timestamp of the youngest data.
     */

    uint64_t
    since() const noexcept
    {
      return m_since;
    }


    /**
     * Get timestamp of the oldest data.
     */

    uint64_t
    until() const noexcept
    {
      return m_until;
    }


    /**
     * Get type of data.
     */

    pbeast::DataType
    type() const noexcept
    {
      return m_type;
    }


    /**
     * Get array type of data.
     * \return true, when data are an array
     */

    bool
    is_array() const noexcept
    {
      return m_is_array;
    }


    /**
     * Align since timestamp.
     */

    void align_since(uint64_t v)
    {
      if (v < m_since)
        m_since = v;
    }


    /**
     * Align until timestamp.
     */

    void align_until(uint64_t v)
    {
      if (v > m_until)
        m_until = v;
    }


    virtual
    ~QueryDataBase()
    {
      ;
    }


  protected:

    QueryDataBase(pbeast::DataType type, bool is_array) :
        m_since(pbeast::max_ts_const), m_until(pbeast::min_ts_const), m_type(type), m_is_array(is_array)
    {
      ;
    }


  private:

    uint64_t m_since;
    uint64_t m_until;
    pbeast::DataType m_type;
    bool m_is_array;
  };


  /**
   * \brief Template implementation of time series data returned ServerProxy::get_data() method.
   *
   * Check time series data type (using get_type() and is_array() methods)
   * to cast DataQueryBase pointer to an appropriate template type.
   */

  template<class T>
    struct QueryData : public QueryDataBase
    {
      const std::map<std::string, std::vector<T>>
      get_data() const noexcept
      {
        return m_data;
      }

      QueryData(pbeast::DataType type, bool is_array) :
          QueryDataBase(type, is_array)
      {
      }

#ifdef SWIGJAVA
      std::vector<std::string> get_names() {
          std::vector<std::string> names;
          names.reserve(m_data.size());

          for(const auto& item : m_data) {
              names.push_back(item.first);
          }

          return names;
      }
#endif

      std::map<std::string, std::vector<T>> m_data;
    };


  /**
   * \brief The class provides remote access to P-BEAST data.
   *
   * It is possible to get:
   * - list of P-BEAST partitions using get_partitions() method
   * - list of classes per partition using get_classes() method
   * - list of attributes per class  using get_attributes() method
   * - get data per attribute using get_datas() method
   *
   * To access P-BEAST data from GPN the cookie file containing CERN SSO information is used.
   * For more information see http://linux.web.cern.ch/linux/docs/cernssocookie.shtml
   *
   * In brief:
   *
   * 1. The auth-get-sso-cookie has to be installed. The description will be updated once this package will be available as normal CERN package.
   * The cern-get-sso-cookie is no more supported after migration to new SSO server on CentOS7.
   *
   * Obsolete old description:
   * The cern-get-sso-cookie has to be installed.
     \code
     # yum install cern-get-sso-cookie
     \endcode
   *
   * 2.1. For Kerberos authentication a valid CERN Kerberos ticket must be acquired by user process
   * (use "kinit" command to refresh it and check status with "klist" command).
   *
   * 2.2. For long running processes the user certificates authentication is preferable.
   * In this case the user has to create "private" directory at $HOME area (remove read access for others),
   * and using the myCert.p12 certificate acquired at CERN Certification Authority execute once
   * the following commands:
     \code
     # openssl pkcs12 -clcerts -nokeys -in myCert.p12 -out ~/private/myCert.pem
     # openssl pkcs12 -nocerts -in myCert.p12 -out ~/private/myCert.tmp.key
     # openssl rsa -in ~/private/myCert.tmp.key -out ~/private/myCert.key
     # rm ~/private/myCert.tmp.key
     # chmod 644 ~/private/myCert.pem
     # chmod 400 ~/private/myCert.key
     \endcode
   * The location of user private directory can be redefined using PBEAST_SERVER_SSO_USER_CERT_DIR process environment variable.
   * However the names of the "pem" and "key" files cannot be changed.
   *
   */

  class ServerProxy
  {
    friend class CurlRequest;

  public:

    enum CookieSetup
    {
      NotInitited,                /**< The setup has not been done yet */
      None,                       /**< SSO is not needed, do not create cookie file */
      AutoUpdateKerberos,         /**< Kerberos authentication using a valid CERN Kerberos ticket acquired by user process */
      AutoUpdateUserCertificates  /**< User Certificates authentication using ~/private/myCert.pem and ~/private/myCert.key files (does not work with auth-get-sso-cookie) */
    };

    enum DataCompression
    {
      NoCompression = 0,          /**< No compression */
      ZipCatalog = 1,             /**< Zip names of objects */
      ZipData = 2,                /**< Zip data */
      CatalogizeStrings = 4       /**< Build catalog of strings and pass every string once */
    };

    /**
     * \brief Create server proxy object for remote access to P-BEAST data.
     *
     * The data are accessed over http(s) protocol using CURL package.
     * To access Point-1 data CERN SSO cookie file is generated.
     *
     * \param base_url           specify P-BEAST server URL, e.g. "https://atlasop.cern.ch" to access data on GPN
     *                           or "http://pc-tdq-bst-12.cern.ch:8080" to access data on ATCN;
     *                           can be defined using TDAQ_PBEAST_SERVER_URL process environment variable
     *
     * \param cookie_setup_type  only if SSO login is needed, select AutoUpdateKerberos for Kerberos based authentication and
     *                           AutoUpdateUserCertificates for User Certificates based authentication;
     *                           can be defined using PBEAST_SERVER_SSO_SETUP_TYPE process environment variable
     *                           set to case insensitive none, autoupdatekerberos or autoupdateusercertificates values
     *
     * \param user_cert_dir      can be used to redefine default "~/private" directory storing user files required
     *                           for user certificates based authentication;
     *                           can be defined using PBEAST_SERVER_SSO_USER_CERT_DIR process environment variable
     *
     *  \throw \b daq::pbeast::Exception in case of an error
     *
     */

    ServerProxy(const std::string& base_url = "", CookieSetup cookie_setup_type = NotInitited, const std::filesystem::path& user_cert_dir = "");

#ifdef SWIGJAVA
    // This is for SWIG only: avoid importing std::filesystem
    ServerProxy(const std::string& base_url = "", CookieSetup cookie_setup_type = NotInitited, const std::string& user_cert_dir = "")
        : ServerProxy(base_url, cookie_setup_type, std::filesystem::path(user_cert_dir))
    {
    };
#endif

    ~ServerProxy() noexcept;


    /**
     * \return the base url
     */

    const std::string&
    get_base_url() const noexcept
    {
      return m_base_url;
    }


    /**
     * Get cookie file name.
     *
     * The cookie file is generated in a protected folder of /tmp area. When the ServerProxy object is destroyed,
     * this folder and the cookie file are deleted as well.
     *
     * \return the temporal cookie file name
     */

    const std::string&
    get_cookie_file_name() const noexcept
    {
      return m_cookie_file_name.native();
    }

    const std::string&
    get_https_proxy() const noexcept
    {
      return m_https_proxy;
    }


    /**
     * Get names of P-BEAST partitions.
     * \return list of partitions
     * \throw \b daq::pbeast::Exception in case of an error
     */

    std::vector<std::string>
    get_partitions() const;


    /**
     * Get names of classes for given P-BEAST partitions.
     * \param partition_name   the name of P-BEAST partition
     * \return list of classes
     * \throw \b daq::pbeast::Exception in case of an error
     */

    std::vector<std::string>
    get_classes(const std::string& partition_name) const;


    /**
     * Get names of attributes for given class.
     * \param partition_name   the name of P-BEAST partition
     * \param class_name       the name of class
     * \return list of attributes
     * \throw \b daq::pbeast::Exception in case of an error
     */

    std::vector<std::string>
    get_attributes(const std::string& partition_name, const std::string& class_name) const;


    /**
     * Get names of objects updated for given attribute (in user-defined time interval).
     * \param partition_name   the name of P-BEAST partition
     * \param class_name       the name of class
     * \param attribute_path   the name of attribute or path to attribute in case of nested types
     * \param object_mask      the regular expression for returned object names (if empty, return all)
     * \param since            look for objects updated \b since given time interval (number of microseconds since Epoch)
     * \param until            look for objects updated \b until given time interval (number of microseconds since Epoch)
     * \return list of objects
     * \throw \b daq::pbeast::Exception in case of an error
     */

    std::vector<std::string>
    get_objects(const std::string& partition_name, const std::string& class_name, const std::string& attribute_path, const std::string& object_mask = "", uint64_t since = pbeast::min_ts_const, uint64_t until = pbeast::max_ts_const) const;


    /**
     * Get raw or downsampled time series data.
     *
     * The type of the returned data is not known in advance and user has to cast result after analyzing data description.
     * The attribute type modification is allowed inside query time interval, so the result may contain several blocks of data of different types.
     * A fragment of data cast is shown below. For full example see https://gitlab.cern.ch/atlas-tdaq-software/pbeast/blob/master/bin/read.cpp
       \code
       pbeast::ServerProxy proxy(...);
       uint32_t sample_interval = ...; // 0 => raw; otherwise => downsampled
       std::vector<std::shared_ptr<pbeast::QueryDataBase>> result = proxy.get_data(..., sample_interval);
       for (const auto& x : result)
         {
           std::cout << '[' << x->since() << ',' << x->until() << "] type: " << pbeast::as_string(x->type()) << ", is array: " << std::boolalpha << x->is_array() << std::endl;

           if (x->type() == pbeast::STRING && sample_interval == 0)
             {
               if (x->is_array() == false)
                 {
                   pbeast::QueryData<pbeast::DataPoint<std::string>> * data = static_cast<pbeast::QueryData<pbeast::DataPoint<std::string>>*>(x);
                   ...
                 }
               else
                 {
                   pbeast::QueryData<pbeast::DataPoint<std::vector<std::string>> * data = static_cast<pbeast::QueryData<pbeast::DataPoint<std::vector<std::string>>>*>(x);
                   ...
                 }
             }
           else if (x->get_type() == pbeast::U32 && sample_interval > 0)
             {
               if (x->is_array() == false)
                 {
                   pbeast::QueryData<pbeast::DownsampledDataPoint<uint32_t>> * data = static_cast<pbeast::QueryData<pbeast::DownsampledDataPoint<uint32_t>>*>(x);
                   ...
                 }
             }
           else ...
         }
       \endcode
     *
     * \param out              return value
     * \param partition_name   the name of P-BEAST partition
     * \param class_name       the name of class
     * \param attribute_path   the name of attribute or path to attribute in case of nested types
     * \param object_mask      the object name of regular expression to select several objects (if empty, return all)
     * \param is_regexp        if true, the object_mask parameter is a regular expression; otherwise it is exact object name
     * \param since            look for objects updated \b since given time interval (number of microseconds since Epoch)
     * \param until            look for objects updated \b until given time interval (number of microseconds since Epoch)
     * \param prev_interval    time lapse to lookup closest previous data point outside query time interval (in microseconds)
     * \param next_interval    time lapse to lookup closest next data point outside query time interval (in microseconds)
     * \param get_all_updates  if false, return span data points (since, until time-stamps); otherwise return simple data points (one time-stamp)
     * \param sample_interval  if 0, return raw data; otherwise return downsampled data
     * \param fill_gaps        when downsample data, approximate missing values: \"none\", \"near\" or \"all\"
     * \param fc               configuration describing which functions to use
     * \param compress_data    performance optimisation parameter to enable / disable data compression
     * \param use_fqn          use fully qualified names, i.e. put dot-separated partition, class and attribute names prefix for object names
     *
     * \return                 the base pointers to data; cast pointer to one of pbeast::DataPoint<T>, pbeast::DataPoint<std::vector<T>>,
     *                         pbeast::DownsampledDataPoint<T> or pbeast::DownsampledDataPoint<std::vector<T>> depending on data type,
     *                         cardinality and sampling parameter
     *
     * \throw \b daq::pbeast::Exception in case of an error
     */

    std::vector<std::shared_ptr<QueryDataBase>>
    get_data(const std::string& partition_name, const std::string& class_name, const std::string& attribute_path, const std::string& object_mask, bool is_regexp, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, bool get_all_updates = false, uint32_t sample_interval = 0, pbeast::FillGaps::Type fill_gaps = pbeast::FillGaps::Near, const pbeast::FunctionConfig& fc = pbeast::FunctionConfig(), int compress_data = ZipCatalog | ZipData, bool use_fqn = false) const;


  private:

    void
    refresh_cookie() const;

    std::string m_base_url;
    CookieSetup m_cookie_setup_type;
    std::string m_https_proxy;
    std::filesystem::path m_user_cert_dir;
    std::filesystem::path m_cookie_file_name;
    mutable time_t m_cookie_ts;
    uint32_t m_cookie_refresh_interval;
    curl_slist * m_http_header;
  };
}

#ifndef SWIG
namespace daq
{
  /*! \class pbeast::HttpRequestFailure
   *  This issue is reported when HTTP request failed
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    HttpRequestFailure,
    Exception,
    "request \"" << request << "\" failed with code " << code << ": " << reason,
    ,
    ((std::string)request)
    ((std::string)reason)
    ((long)code)
 )

  /*! \class pbeast::BadHttpResponse
   *  This issue is reported when HTTP request has unexpected format
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    BadHttpResponse,
    Exception,
    "bad http response: " << reason,
    ,
    ((std::string)reason)
  )

  /*! \class pbeast::BadUserConfigurationParameter
   *  This issue is reported when user passes bad configuration parameter
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    BadUserConfigurationParameter,
    Exception,
    "bad parameter: " << reason,
    ,
    ((std::string)reason)
  )

  /*! \class pbeast::CannotCreateCookieFile
   *  This issue is reported when CERN SSO cookie file cannot be created
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotCreateCookieFile,
    Exception,
    "cannot create CERN SSO cookie",
    ,
  )
}
#endif

#endif
