#ifndef PBEAST_DOWNSAMPLE_ALGO_H
#define PBEAST_DOWNSAMPLE_ALGO_H

#define APPLY_DS_ALGO(DATA, EOV, UPD, EOV_ALGO_CODE, DATA_ALGO_CODE)                                            \
{                                                                                                               \
  std::set<uint32_t>::const_iterator eov_it = EOV.begin();                                                      \
  uint32_t eov_next = (eov_it != EOV.end()) ? *eov_it : std::numeric_limits<uint32_t>::max();                   \
                                                                                                                \
  std::set<uint32_t>::const_iterator upd_it = UPD.begin();                                                      \
  uint32_t upd_next = (upd_it != UPD.end()) ? *upd_it : 0;                                                      \
                                                                                                                \
  uint32_t last_printed = 0;                                                                                    \
  const auto data_size( DATA.size());                                                                           \
                                                                                                                \
  /* print eov (if any) before data or if there are no data*/                                                   \
  if (eov_next != std::numeric_limits<uint32_t>::max() && !data_size)                                           \
    { EOV_ALGO_CODE }                                                                                           \
                                                                                                                \
  for (auto it = DATA.begin(); it != DATA.end(); ++it)                                                          \
    {                                                                                                           \
      const uint32_t data_ts = (*it).get_ts();                                                                  \
      bool last_printed_is_eov = false;                                                                         \
                                                                                                                \
      while (eov_next <= data_ts)                                                                               \
        {                                                                                                       \
          last_printed = eov_next ;                                                                             \
          if (last_printed_is_eov == false) { last_printed_is_eov = true; EOV_ALGO_CODE }                       \
          eov_it++;                                                                                             \
          eov_next = (eov_it != EOV .end()) ? *eov_it : std::numeric_limits<uint32_t>::max();                   \
        }                                                                                                       \
                                                                                                                \
      if (data_ts != last_printed) { last_printed = data_ts ; DATA_ALGO_CODE((*it).get_ts()) }                  \
                                                                                                                \
      if (upd_next)                                                                                             \
        {                                                                                                       \
          auto nx = std::next(it);                                                                              \
          uint32_t t_next = (nx != DATA.end()) ? (*nx).get_ts() : std::numeric_limits<uint32_t>::max();         \
                                                                                                                \
          while (upd_next != 0 && upd_next < t_next)                                                            \
            {                                                                                                   \
              if (upd_next > data_ts && upd_next < t_next)                                                      \
                {                                                                                               \
                  last_printed_is_eov = false;                                                                  \
                                                                                                                \
                  /* not sure, this loop is ever executed */                                                    \
                  while (eov_next <= upd_next)                                                                  \
                    {                                                                                           \
                      last_printed = eov_next ;                                                                 \
                      if (last_printed_is_eov == false) { last_printed_is_eov = true; EOV_ALGO_CODE }           \
                      eov_it++;                                                                                 \
                      eov_next = (eov_it != EOV.end()) ? *eov_it : std::numeric_limits<uint32_t>::max();        \
                    }                                                                                           \
                                                                                                                \
                  if (last_printed != upd_next) { last_printed = upd_next ; DATA_ALGO_CODE(upd_next) }          \
                                                                                                                \
                }                                                                                               \
              upd_it++;                                                                                         \
              upd_next = (upd_it != UPD.end()) ? *upd_it : 0;                                                   \
            }                                                                                                   \
        }                                                                                                       \
    }                                                                                                           \
                                                                                                                \
  /* print eov (if any) after last data printed */                                                              \
  if (eov_next != std::numeric_limits<uint32_t>::max() && data_size && last_printed < eov_next)                 \
    { EOV_ALGO_CODE }                                                                                           \
}

#endif
