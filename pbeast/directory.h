#ifndef PBEAST_DIRECTOIRY_H
#define PBEAST_DIRECTOIRY_H

#include <shared_mutex>

#include "pbeast/call-stats.h"
#include "pbeast/input-data-file.h"
#include "pbeast/output-data-file.h"

namespace pbeast
{
  class InputDataFileBase;

  struct File
  {
    uint32_t m_base;
    uint32_t m_duration;
    std::unique_ptr<InputDataFileBase> m_file_info;
    uint64_t m_file_info_size;

    std::mutex m_mutex;
    bool m_confirmed;

    File(uint32_t base, uint32_t duration) :
        m_base(base), m_duration(duration), m_file_info(), m_file_info_size(0), m_confirmed(true)
    {
      ;
    }

    void
    calculate_file_info_size(const std::filesystem::path& path);
  };

  class Directory
  {

  public:

    Directory(const std::filesystem::path& path);

    void
    refresh();

    bool
    exist();

    void
    create();

    void
    get_files(uint32_t since, uint32_t until, FileSet& files, CallStats& stat);

    bool
    get_file(const std::string& filename, FileSet& files, bool access, CallStats& stat);

    void
    add_file(const std::string& filename, const pbeast::OutputDataFile& file, FileSet& files, CallStats& stat);

    void
    remove_files(const std::vector<std::string>& paths);

    void
    remove_all_files();

    void
    remove_file(const std::filesystem::path& path);

    const std::filesystem::path&
    get_path() const
    {
      return m_path;
    }

    std::time_t
    get_last_accessed() const
    {
      return m_last_accessed;
    }

    uint64_t
    get_size_of_file_infos();

    uint64_t
    get_number_of_files();


  private:

    bool
    exist_no_lock();

    std::shared_mutex m_mutex;

    const std::filesystem::path m_path;
    std::map<std::string, std::unique_ptr<File>> m_files;

    std::time_t m_last_updated;  // FIXME: use atomic<> ?
    std::time_t m_last_accessed; // FIXME: use atomic<> ?
  };
}

#endif
