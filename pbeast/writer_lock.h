#ifndef PBEAST_WRITER_LOCK_H
#define PBEAST_WRITER_LOCK_H

#include <unistd.h>

#include <boost/interprocess/sync/interprocess_mutex.hpp>
#include <boost/interprocess/shared_memory_object.hpp>
#include <boost/interprocess/mapped_region.hpp>

#include <ers/ers.h>

#include "pbeast/exceptions.h"

namespace pbeast
{
  /**
   * \brief The class is used to implement shared writer lock of P-BEAST repository.
   *
   *  Both write and read P-BEAST applications are sharing the same disk resources.
   *
   *  The receiver application has absolute priority, when flushes data on disk,
   *  since this operation may block others operations of the receiver. It is especially
   *  important to have maximum write performance, when receiver is shut down, since it
   *  has very limited time to flush buffers before it will be killed.
   *
   *  From another side we have P-BEAST merger and repository servers, which have much
   *  less importance vs. receiver applications.
   *
   *  The class provides the "write lock" functionality, which allows to prevent disk usage
   *  by the merger and repository servers, while the lock is set.
   *
   *  See https://its.cern.ch/jira/browse/ADAMATLAS-213 for more information.
   *
   *  Implementation is based on shared memory.
   */

  class WriterLock
  {

    friend class DownsampleLock;

    struct Data
    {
      Data() :
        m_count(0)
      {
      }

      boost::interprocess::interprocess_mutex m_mutex;
      boost::interprocess::interprocess_mutex m_downsample_mutex;
      volatile int m_count;
    };


  public:

    /**
     *  \brief Construct the lock object
     *
     *  \throw Exception in case of failure
     */

    WriterLock();


    /**
     *  \brief Destroy the object removing any lock
     *
     *  Report ers::error in case removal of lock failed.
     */

    ~WriterLock();


    void lock();

    void unlock();


    /*
     *  Set timeout for wait_read_permission() method.
     *
     *  Parameters:
     *  \param timeout     maximum wait timeout (in seconds); minimum 10 seconds
     */

    void
    set_read_timeout(uint32_t timeout) noexcept
    {
      m_timeout = (timeout < 10 ? 10 : timeout);
    }

    /*
     *  Set retry interval for wait_read_permission() method.
     *
     *  Parameters:
     *  \param retry       retry interval (in milliseconds); minimum 100 milliseconds
     */

    void
    set_read_retry_interval(uint16_t retry) noexcept
    {
      m_retry = (retry < 100 ? 100 : retry);
    }

    /**
     *  \brief Wait for read permissions
     *
     *  In case if there are no processes wring P-BEAST data the method exits immediately.
     *  If there are writer applications, it goes into a loop and retries a number of times
     *  with sleep interval unless get a permission or timeout. See set_read_timeout() and
     *  set_read_retry_interval() methods to set timeout and retry interval parameters.
     */

    void wait_read_permission();

  public:

    static const char * mutex_name;
    static const char * memory_name;

  private:

    boost::interprocess::shared_memory_object m_shm;
    boost::interprocess::mapped_region m_region;
    Data * m_data;
    bool m_locked;
    uint32_t m_timeout;
    uint16_t m_retry;

  };


  class DownsampleLock
  {
  public:

    DownsampleLock(WriterLock& obj) :
        m_obj(obj)
    {
      m_obj.m_data->m_downsample_mutex.lock();
    }

    ~DownsampleLock()
    {
      m_obj.m_data->m_downsample_mutex.unlock();
    }

  private:
    WriterLock& m_obj;
  };
}

namespace daq
{
  /*! \class pbeast::CannotOpenSharedMemoryObject
   *  This issue is reported when application cannot create or open shared memory object
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotOpenSharedMemoryObject,
    Exception,
    "cannot create or open " << name << " shared memory object (check permissions in /dev/shm)",
    ERS_EMPTY,
    ((const char *)name)
  )

  /*! \class pbeast::CannotRemoveMutexObject
   *  This issue is reported when application cannot remove shared memory object
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotRemoveMutexObject,
    Exception,
    "cannot remove " << name << " shared memory mutex (check permissions and existence in /dev/shm)",
    ERS_EMPTY,
    ((const char *)name)
  )

  /*! \class pbeast::ResetWritersCount
   *  This issue is reported when application waiting read permission cleans up writers counter (happens after timeout)
   */
  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    ResetWritersCount,
    Exception,
    "reset number of writers from " << count << " to zero after " << timeout << " seconds of waiting",
    ERS_EMPTY,
    ((uint32_t)timeout)
    ((uint32_t)count)
  )
}

#endif
