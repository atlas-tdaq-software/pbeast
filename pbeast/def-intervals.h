#ifndef PBEAST_DEF_INTERVALS_H
#define PBEAST_DEF_INTERVALS_H

#include <stdint.h>

namespace pbeast
{
  uint64_t
  get_def_since();

  uint64_t
  get_def_until();
}

#endif
