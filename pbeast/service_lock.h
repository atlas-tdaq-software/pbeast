#ifndef PBEAST_REPOSITORY_LOCK_H
#define PBEAST_REPOSITORY_LOCK_H

#include <fstream>
#include <filesystem>

#include <boost/interprocess/sync/file_lock.hpp>

#include <ers/ers.h>

#include "pbeast/exceptions.h"

namespace pbeast
{
  /**
   * \brief The class is used to make exclusive lock of a P-BEAST service by name (file system based lock).
   *
   *  The constructor creates lock file with service name in the root directory of P-BEAST repository.
   *  The lock is created when service is started and the lock is removed, when service is shut down.
   *  In case of service failure, such file may remain on the file system until it will be overwritten on service restart.
   *  However it is not possible to overwrite such file starting second instance of service process,
   *  while original service process is still running.
   */

  class ServiceLock
  {

  public:

    /**
     *  \brief Lock service by name
     *
     *  Parameters:
     *  \param repository_path   path to P-BEAST repository (i.e. directory containing names of partitions)
     *  \param lock_name         name of the service
     *
     *  \throw tdaq::pbeast::Exception in case of failure
     */

    ServiceLock(const std::filesystem::path& repository_path, const std::string& lock_name);


    /**
     *  \brief Remove service lock
     *
     *  Report ers::error in case removal of lock failed.
     */

    ~ServiceLock();

  private:

    bool m_lock_created;
    const std::filesystem::path m_repository_lock_file;
    boost::interprocess::file_lock m_lock;

    static std::filesystem::path
    check_repository_lock(const std::filesystem::path& repository_path, const std::string& lock_name);

    static const char *
    create_repository_lock_file(const std::filesystem::path& file_path);
  };
}


namespace daq
{
  ERS_DECLARE_ISSUE_BASE(pbeast, RepositoryUnlockFailed, Exception, "repository unlock failed", ERS_EMPTY, ERS_EMPTY)

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    RepositoryAlreadyLocked,
    Exception,
    "the repository " << path << " already locked by \"" << whom << '\"',
    ERS_EMPTY,
    ((std::filesystem::path)path)
    ((std::string)whom)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    RemoveObsoleteRepositoryLock,
    Exception,
    "obsolete lock of repository " << path << " created by \"" << whom << "\" has been removed",
    ERS_EMPTY,
    ((std::filesystem::path)path)
    ((std::string)whom)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotLockRepository,
    Exception,
    "cannot " << op << " repository lock file " << path,
    ERS_EMPTY,
    ((const char *)op)
    ((std::filesystem::path)path)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    NoRepositoryLockFile,
    Exception,
    "repository lock file " << path << " does not exist",
    ERS_EMPTY,
    ((std::filesystem::path)path)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotRemoveRepositoryLockFile,
    Exception,
    "cannot remove repository lock file " << path << ", unlink failed with code " << code << ": " << why,
    ERS_EMPTY,
    ((std::filesystem::path)path)
    ((int)code)
    ((const char *)why)
  )

  ERS_DECLARE_ISSUE_BASE(
    pbeast,
    CannotUnlockRepositoryLockFile,
    Exception,
    "cannot unlock repository lock file " << path,
    ERS_EMPTY,
    ((std::filesystem::path)path)
  )
}

#endif
