#ifndef PBEAST_REPOSITORY_FILE_H
#define PBEAST_REPOSITORY_FILE_H

#include <stdlib.h>

#include <string>
#include <chrono>
#include <climits>
#include <cstdint>
#include <map>
#include <set>
#include <shared_mutex>

#include <boost/regex.hpp>

#include <ers/ers.h>

#include "pbeast/directory.h"
#include "pbeast/input-data-file.h"
#include "pbeast/writer_lock.h"
#include "pbeast/downsample-config.h"
#include "pbeast/downsample-data.h"
#include "pbeast/exceptions.h"
#include "pbeast/thread_pool.h"
#include "pbeast/object-name.h"


namespace pbeast
{

  /**
   * \brief Provides interface to read data from P-BEAST repository.
   *
   *  The class provides interfaces to make a query reading data from P-BEAST repository.
   *  Such query merges result from several P-BEAST data files in accordance with timestamps selection criteria.
   *
   *  A query includes:
   *  - name of partition,
   *  - name of class,
   *  - the path to attribute (i.e. name of attribute for simple type or path in case of nested attribute types)
   *  - since and until timestamps
   *  - optional suffix for queries on nested attribute types
   *
   *  A query returns P-BEAST values for given attributes, end-of-validity for corresponding objects.
   *
   *  There are two versions of query methods read_attribute:
   *  - read values of an attribute of one object using explicit object name
   *  - read values of an attribute of may objects optionally using regular expressions for names of objects and nested attribute type suffixes
   *
   *  One needs to use template parameter for a query, that corresponds to the type of given attribute in saved data. For example, to read single value string data:
   *
\code
std::set<uint64_t> eov;
pbeast::CallStats stat;
std::vector<pbeast::SeriesData<std::string>> data;
db.read_attribute("partition", "class", "string-attribute", 0, 0xffffffffffffffff, "object-id", "", data, eov, stat);
\endcode
   *
   *  If the parameter is not known in advance, it can be read from P-BEAST repository using pbeast::Meta class.
   */

  class Repository
  {

  public:

    /**
     *  \brief Construct the repository object allowing to perform queries on P-BEAST data.
     *
     *  Set PBEAST_PROFILING process environment variable to get profiling information in standard out.
     *
     *  Parameters:
     *  \param path        path to P-BEAST repository (typically repository containing merged files)
     *  \param local_path  path to local P-BEAST repository (typically repository containing fresh files)
     *  \param conf        describe downsampling cache parameters for P-BEAST repository; use of default configuration results in-memory downsampling, otherwise it is persistent
     */

    Repository(const std::filesystem::path& path, const std::filesystem::path& local_path, const DownsampleConfig& conf = DownsampleConfig()) :
        m_writer_lock(nullptr), m_repository_path(path), m_local_repository_path(local_path), m_repository_path_len(get_path_len(path)), m_local_repository_path_len(get_path_len(local_path)), m_downsample_config(conf), m_downsample_thread_pool(m_downsample_config.get_downsample_thread_pool_size())
    {
      m_profiling = (getenv("PBEAST_PROFILING") != 0);
    }

    ~Repository()
    {
      delete m_writer_lock;
    }

    /**
     *  \brief Set writer lock (obligatory operation, cannot be put into constructor because of possible exception conflicting with IPC object)
     *
     *  Parameters:
     *  \param timeout     maximum wait timeout on write lock (in seconds); minimum 10 seconds
     *  \param retry       retry interval on write lock (in milliseconds); minimum 100 milliseconds
     */

    void
    init_lock(uint32_t timeout = 60, uint16_t retry = 100)
    {
      m_writer_lock = new WriterLock();
      m_writer_lock->set_read_timeout(timeout);
      m_writer_lock->set_read_retry_interval(retry);
    }


    pbeast::WriterLock&
    get_writer_lock()
    {
      return *m_writer_lock;
    }


    /**
     *  \brief List names of objects updated in given time interval.
     *
     *  The method reads names of objects from files of given attribute of the class containing given time interval.
     *  The method may return objects which were not updated in given time interval, but been updated close to it.
     *
     *  Parameters:
     *  \param data             output names of objects
     *  \param partition_name   name of partition
     *  \param class_name       name of class
     *  \param attribute_path   name of attribute or path to attributes for nested types
     *  \param since            add objects older this timestamp (in microseconds since Epoch)
     *  \param until            add objects younger this timestamp (in microseconds since Epoch)
     *  \param object_id        regex to select a group of objects (select all objects, if empty)
     *  \param stat             call statistics used for monitoring purposes
     *
     *  \throw std::runtime_error in case of an error
     */

    void
    list_updated_objects(std::set<std::string>& data, const std::string& partition_name,
        const std::string& class_name, const std::filesystem::path& attribute_path,
        uint64_t since, uint64_t until, const std::string& object_id, CallStats& stat);


    /**
     *  \brief Read end-of-validity data of single object.
     *
     *  Parameters:
     *  \param partition_name   name of partition
     *  \param class_name       name of class
     *  \param since            read data older this timestamp (in microseconds since Epoch)
     *  \param until            read data younger this timestamp (in microseconds since Epoch)
     *  \param prev_interval    time lapse to lookup closest previous data point outside query time interval (in microseconds)
     *  \param next_interval    time lapse to lookup closest next data point outside query time interval (in microseconds)
     *  \param object_id        name of object (non-empty string, non-regex)
     *  \param eov              output object's end-of-validity (in microseconds since Epoch)
     *  \param stat             call statistics used for monitoring purposes
     *
     *  \throw std::runtime_error in case of an error
     */

    void
    read_eov(const std::string& partition_name, const std::string& class_name, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, const std::string& object_id, std::set<uint64_t>& eov, CallStats& stat)
    {
      read_void(pbeast::Repository::get_eov_attribute_name(), partition_name, class_name, since, until, pbeast::mk_wide_since(since, prev_interval), pbeast::mk_wide_since(until, next_interval), object_id, eov, stat);
    }


    /**
     *  \brief Read end-of-validity data of many objects.
     *
     *  Parameters:
     *  \param partition_name       name of partition
     *  \param class_name           name of class
     *  \param since                read data older this timestamp (in microseconds since Epoch)
     *  \param until                read data younger this timestamp (in microseconds since Epoch)
     *  \param prev_interval        time lapse to lookup closest previous data point outside query time interval (in microseconds)
     *  \param next_interval        time lapse to lookup closest next data point outside query time interval (in microseconds)
     *  \param re                   regular expression for object names (if NULL, read EoV of all objects)
     *  \param eov                  output map of object's end-of-validity (in microseconds since Epoch)
     *  \param stat                 call statistics used for monitoring purposes
     *
     *  \throw std::runtime_error in case of an error
     */

    void
    read_eov(const std::string& partition_name, const std::string& class_name, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, boost::regex * re, std::map<std::string, std::set<uint64_t>>& eov, CallStats& stat)
    {
      read_void(pbeast::Repository::get_eov_attribute_name(), partition_name, class_name, since, until, pbeast::mk_wide_since(since, prev_interval), pbeast::mk_wide_since(until, next_interval), re, eov, stat);
    }


    /**
     *  \brief Read values of the attribute of many objects.
     *
     *  Parameters:
     *  \param partition_name       name of partition
     *  \param class_name           name of class
     *  \param attribute_path       name of attribute or path to attributes for nested types
     *  \param since                read data older this timestamp (in microseconds since Epoch)
     *  \param until                read data younger this timestamp (in microseconds since Epoch)
     *  \param prev_interval        time lapse to lookup closest previous data point outside query time interval (in microseconds)
     *  \param next_interval        time lapse to lookup closest next data point outside query time interval (in microseconds)
     *  \param object_id            explicit name of object or regex to select a group of objects
     *  \param object_id_is_regex   if true, above parameter is considered as regex
     *  \param suffix               explicit suffix of object or regex to select a group of objects (for nested attribute type)
     *  \param suffix_is_regex      if true, above parameter is considered as regex
     *  \param data                 output map of object names and attribute values (use pbeast::SeriesData<T> for single value data and pbeast::SeriesVectorData<T> for arrays)
     *  \param eov                  output map of object's end-of-validity (in microseconds since Epoch)
     *  \param upd                  if not null, output object's updates info (in microseconds since Epoch)
     *  \param stat                 call statistics used for monitoring purposes
     *
     *  \throw std::runtime_error in case of an error
     */

    template<class T>
      void
      read_attribute(const std::string& partition_name, const std::string& class_name, const std::filesystem::path& attribute_path,
          uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, const std::string& object_id, bool object_id_is_regex, const std::string& suffix, bool suffix_is_regex,
          std::map<std::string,std::vector<T>>& data, std::map<std::string,std::set<uint64_t>>& eov, std::map<std::string,std::set<uint64_t>>* upd, CallStats& stat)
      {
        std::chrono::time_point<std::chrono::steady_clock> t1;

        if (m_profiling)
          t1 = std::chrono::steady_clock::now();

        pbeast::DataFile::validate_parameters(partition_name, class_name, attribute_path);

        // new time interval including deltas
        const uint64_t w_since = pbeast::mk_wide_since(since, prev_interval);
        const uint64_t w_until = pbeast::mk_wide_until(until, next_interval);

        // find data files
        m_writer_lock->wait_read_permission();
        FileSet files, eov_files, upd_files;

        std::shared_lock lock(m_repository_files);

        find_files(partition_name, class_name, attribute_path, w_since, w_until, true, files, stat);
        find_files(partition_name, class_name, pbeast::Repository::get_eov_attribute_name(), w_since, w_until, false, eov_files, stat);
        if (upd)
          find_files(partition_name, class_name, pbeast::Repository::get_upd_attribute_name(), w_since, w_until, false, upd_files, stat);

        read_attribute(files, eov_files, upd_files, attribute_path, since, until, w_since, w_until, object_id, object_id_is_regex, suffix, suffix_is_regex, data, eov, *upd, stat);

        if (m_profiling)
          ERS_LOG( "read " << data.size() << " series in " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count() / 1000. << " seconds" );
      }


    /**
     *  \brief Read values of the attribute of many objects.
     *
     *  Parameters:
     *  \param partition_name       name of partition
     *  \param class_name           name of class
     *  \param attribute_path       name of attribute or path to attributes for nested types
     *  \param since                read data older this timestamp (in microseconds since Epoch)
     *  \param until                read data younger this timestamp (in microseconds since Epoch)
     *  \param prev_interval        time lapse to lookup closest previous data point outside query time interval (in microseconds)
     *  \param next_interval        time lapse to lookup closest next data point outside query time interval (in microseconds)
     *  \param sample_interval      data downsample interval (in seconds)
     *  \param fill_gaps            when downsample data, approximate missing values: \"none\", \"near\" or \"all\"
     *  \param object_id            explicit name of object or regex to select a group of objects
     *  \param object_id_is_regex   if true, above parameter is considered as regex
     *  \param suffix               explicit suffix of object or regex to select a group of objects (for nested attribute type)
     *  \param suffix_is_regex      if true, above parameter is considered as regex
     *  \param data                 output map of object names and attribute values (use pbeast::DownsampledSeriesData<T> for single value data and pbeast::DownsampledSeriesVectorData<T> for arrays)
     *  \param eov                  output map of object's end-of-validity (in seconds since Epoch)
     *  \param upd                  if not null, output object's updates info (in seconds since Epoch)
     *  \param stat                 call statistics used for monitoring purposes
     *
     *  \throw std::runtime_error in case of an error
     */

    template<class T>
      void
      read_downsample_attribute(const std::string& partition_name, const std::string& class_name,
          const std::filesystem::path& attribute_path, uint64_t since, uint64_t until, uint64_t prev_interval, uint64_t next_interval, uint32_t sample_interval, pbeast::FillGaps::Type fill_gaps,
          const std::string& object_id, bool object_id_is_regex, const std::string& suffix, bool suffix_is_regex, std::map<std::string,std::vector<T>>& data,
          std::map<std::string,std::set<uint32_t>>& eov, std::map<std::string,std::set<uint32_t>>* upd, CallStats& stat, CallStats& local_stat)
      {
        std::chrono::time_point<std::chrono::steady_clock> t1;

        if (m_profiling)
          t1 = std::chrono::steady_clock::now();

        pbeast::DataFile::validate_parameters(partition_name, class_name, attribute_path);

        // new time interval including deltas
        const uint64_t w_since = pbeast::mk_wide_since(since, prev_interval);
        const uint64_t w_until = pbeast::mk_wide_until(until, next_interval);

        const uint32_t since32 = pbeast::ts2secs(since);
        const uint32_t until32 = pbeast::ts2secs(until);
        const uint32_t w_since32 = pbeast::ts2secs(w_since);
        const uint32_t w_until32 = pbeast::ts2secs(w_until);

        m_writer_lock->wait_read_permission();

          {
            std::shared_lock lock(m_repository_files);

            if (m_downsample_config.m_cache_path.empty() == false && m_downsample_config.m_sample_intervals.front() <= sample_interval)
              {
                FileSet local_data_files, merged_data_files, local_eov_files, merged_eov_files, local_upd_files, merged_upd_files;
                find_files(partition_name, class_name, attribute_path, w_since, w_until, true, merged_data_files, local_data_files, stat, local_stat);
                find_files(partition_name, class_name, pbeast::Repository::get_eov_attribute_name(), w_since, w_until, false, merged_eov_files, local_eov_files, stat, local_stat);
                find_files(partition_name, class_name, pbeast::Repository::get_upd_attribute_name(), w_since, w_until, false, merged_upd_files, local_upd_files, stat, local_stat);

                std::map<std::string, std::set<uint64_t>> eov64, dummy64;
                std::unique_ptr<boost::regex> object_id_re;

                const bool complex_attribute_path = (attribute_path.native().find(std::filesystem::path::preferred_separator) != std::string::npos);

                // if object_id is regex, then read eov for objects matching regex
                if (object_id_is_regex)
                  {
                    try
                      {
                        if (!object_id.empty())
                          object_id_re.reset(new boost::regex(object_id));
                      }
                    catch (std::exception& ex)
                      {
                        throw daq::pbeast::RegularExpressionError(ERS_HERE, "cannot parse regular expression", object_id, ex);
                      }

                    if(upd)
                      read_downsample_void(merged_upd_files, local_upd_files, since, until, w_since, w_until, sample_interval, object_id, object_id_re.get(), nullptr, *upd, dummy64, stat, local_stat);

                    read_downsample_void(merged_eov_files, local_eov_files, since, until, w_since, w_until, sample_interval, object_id, object_id_re.get(), nullptr, eov, eov64, stat, local_stat);
                  }

                // otherwise use simple method to read eov by explicit name (most efficient way)
                else
                  {
                    std::set<uint32_t>& eov_set(eov[object_id]);
                    std::set<uint64_t>& eov_set64(eov64[object_id]);

                    if (upd)
                      {
                        std::set<uint32_t> &upd_set((*upd)[object_id]);

                        std::set<uint64_t> dummy_set64;
                        read_downsample_void(merged_upd_files, local_upd_files, since, until, w_since, w_until, sample_interval, object_id, upd_set, dummy_set64, stat, local_stat);

                        if (upd_set.empty())
                          upd->erase(object_id);
                      }

                    read_downsample_void(merged_eov_files, local_eov_files, since, until, w_since, w_until, sample_interval, object_id, eov_set, eov_set64, stat, local_stat);
                    if (eov_set.empty())
                      {
                        eov.erase(object_id);
                        eov64.erase(object_id);
                      }
                  }

                // read merged files first; downsample them if necessary
                read_persistent_downsample_data<T>(merged_data_files, attribute_path, since32, until32, sample_interval, fill_gaps, object_id, object_id_is_regex, suffix, suffix_is_regex, data, stat);

                // read raw files from local repository
                std::map<std::string, std::vector<typename T::raw_type>> raw_data;
                FileSet empty;
                read_attribute(local_data_files, empty, empty, attribute_path, since, until, w_since, w_until, object_id, object_id_is_regex, suffix, suffix_is_regex, raw_data, eov64, dummy64, local_stat);

                // downsample raw data and merge with above result
                for (const auto & x : raw_data)
                  pbeast::downsample(sample_interval, x.second, data[x.first], fill_gaps);

                if (complex_attribute_path)
                  {
                    std::set<std::string> names;

                    for (const auto &s : data)
                      names.insert(pbeast::get_object_name(s.first));

                    if (upd)
                      read_downsample_void(merged_upd_files, local_upd_files, since, until, w_since, w_until, sample_interval, object_id, nullptr, &names, *upd, dummy64, stat, local_stat);

                    read_downsample_void(merged_eov_files, local_eov_files, since, until, w_since, w_until, sample_interval, object_id, nullptr, &names, eov, eov64, stat, local_stat);
                  }
              }
            else
              {
                // in-memory attribute downsampling

                FileSet data_files, eov_files, upd_files;
                find_files(partition_name, class_name, attribute_path, w_since, w_until, true, data_files, local_stat);
                find_files(partition_name, class_name, pbeast::Repository::get_eov_attribute_name(), w_since, w_until, false, eov_files, local_stat);
                if (upd)
                  find_files(partition_name, class_name, pbeast::Repository::get_upd_attribute_name(), w_since, w_until, false, upd_files, local_stat);

                std::map<std::string, std::vector<typename T::raw_type>> raw_data;
                std::map<std::string, std::set<uint64_t>> raw_eov, raw_upd;

                read_attribute(data_files, eov_files, upd_files, attribute_path, since, until, w_since, w_until, object_id, object_id_is_regex, suffix, suffix_is_regex, raw_data, raw_eov, raw_upd, local_stat);

                for (const auto & x : raw_data)
                  pbeast::downsample(sample_interval, x.second, data[x.first], fill_gaps);

                if (upd)
                  pbeast::downsample_void(sample_interval, raw_upd, *upd);

                pbeast::downsample_void(sample_interval, raw_eov, eov);
              }
          }

        // remove all but one before "since" and after "until"
        trim_but_one(eov, since32, until32);

        if(upd)
          trim_but_one(*upd, since32, until32);

        trim_but_one(data, since32, until32);

        // align boundaries by downsampling interval (note, ceil) to avoid unexpected data on open EoV interval
        adjust_boundaries(data, eov, upd, pbeast::mk_downsample_ts(w_since32, sample_interval)-sample_interval/2-sample_interval%2, pbeast::mk_downsample_ts(w_until32, sample_interval)+sample_interval/2);

        if (m_profiling)
          ERS_LOG( "read " << data.size() << " series in " << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now()-t1).count() / 1000. << " seconds" );

        make_stat(data, stat);
        add_stat(eov, stat);
        if (upd)
          add_stat(*upd, stat);
        stat.check_size();
     }


    /**
     *  \brief Get name of folder used to store end-of-validity data.
     *
     *  \return the EoV folder name
     */

    static const std::string&
    get_eov_attribute_name() { return s_eov_attribute_name; }


    /**
     *  \brief Get name of folder used to store updated-info data.
     *
     *  \return the updated info folder name
     */

    static const std::string&
    get_upd_attribute_name() { return s_upd_attribute_name; }


    /**
     *  \brief Get suffix separator for given path to nested attribute type.
     *
     *  \param path the path to nested attribute type
     *  \return the suffix separator
     */

    static std::string
    get_suffix_separator(const std::filesystem::path& path);


    /**
     *  \brief Return fully qualified name prefix for P-BEAST objects.
     *
     *  \param partition_name       name of partition
     *  \param class_name           name of class
     *  \param attribute_name       name of attribute or path to attributes for nested types
     *
     *  \return the fully qualified name prefix
     */

    static std::string
    create_fqn_prefix(const std::string& partition_name, const std::string& class_name, const std::string& attribute_name);


    /**
     *  \brief Downsample raw data files.
     *
     *  The original raw data remain unchanged.
     *  Downsampled files are created in the directory defined by the downsample configuration.
     *
     *  Parameters:
     *  \param raw_files            paths to raw data files to be downsampled
     *  \param sample_intervals     sample intervals (in seconds)
     *  \param writer_lock          repository writer lock object
     *  \param repository_path      path to pbeast repository containing raw data files
     *  \param downsample_config    downsample configuration
     *  \param ids                  objects mask
     *  \param re                   regex corresponding to objects mask on nullptr for all objects
     *  \param access               if true, update directory access time
     *  \param stat                 call statistics used for monitoring purposes
     *
     *  \throw std::runtime_error in case of an error
     */

    FileSet
    downsample_files(const FileSet& raw_files, const std::vector<uint64_t> sample_intervals, pbeast::DownsampleConfig downsample_config, const std::string& ids, boost::regex *re, bool access, CallStats& stat);


  private:

    FileSet
    downsample_files(const FileSet& raw_files, uint64_t sample_interval, const std::string& ids, boost::regex *re, CallStats& stat);

    template<class T>
      static void
      make_stat(const std::map<std::string,std::vector<T>>& data, CallStats& stat)
      {
        stat.m_objects_num = data.size();
        stat.m_data_point_num = 0;
        stat.m_data_size = 0;
        for (const auto &x : data)
          {
            stat.m_data_point_num += x.second.size();
            stat.m_data_size += sizeof(uint64_t) + x.first.size();
            for (const auto &y : x.second)
              stat.m_data_size += y.get_data_size();
          }
      }

    static void
    add_stat(std::map<std::string,std::set<uint64_t>>& data, CallStats& stat);

    static void
    add_stat(std::map<std::string,std::set<uint32_t>>& data, CallStats& stat);

    void
    find_files(const std::string& partition_name, const std::string& class_name, const std::filesystem::path& attribute_name, uint64_t since, uint64_t until, bool encode_attribute_name, FileSet& files, CallStats& stat)
    {
      find_files(partition_name, class_name, attribute_name, since, until, encode_attribute_name, files, files, stat, stat);
    }

    void
    find_files(const std::string& partition_name, const std::string& class_name, const std::filesystem::path& attribute_name, uint64_t since, uint64_t until, bool encode_attribute_name, FileSet& repository_files, FileSet& local_repository_files, CallStats& stat, CallStats& local_stat);


    // raw data

    template<class T>
      void
      match_and_add_raw(const std::vector<T>& from, uint64_t since, uint64_t until, std::set<T>& to, CallStats& stat)
      {
        unsigned int left_bound, right_bound;

        if (pbeast::get_raw_data_bounds(from, since, until, left_bound, right_bound))
          {
            if (to.size() > 0)
              {
                // check if data point "before" query interval has to be replaced by closer one
                auto first = to.begin();
                const uint64_t left_inserted = (*first).get_ts_created();
                const uint64_t left_new = from[left_bound].get_ts_created();
                if (left_inserted < since && left_new < since && left_new > left_inserted)
                  {
                    ERS_DEBUG(1, "replace already inserted first data at " << left_inserted << " by data at " << left_new << " (since = " << since << ')');
                    to.erase(first);
                  }

                if (to.size() > 0)
                  {
                    auto last = to.end();
                    last--;
                    const uint64_t last_inserted = (*last).get_ts_created();
                    const uint64_t last_new = from[right_bound].get_ts_created();
                    if (last_inserted > until && last_new > until && last_new < last_inserted)
                      {
                        ERS_DEBUG(1, "replace already inserted last data at " << last_inserted << " by data at " << last_new << " (until = " << until << ')');
                        to.erase(last);
                      }
                    else if ((*to.rbegin()).get_ts_last_updated() > until && left_new > until)
                      {
                        ERS_DEBUG(2, "skip insert of data starting since " << left_new << " because last inserted data have validity until " << (*to.rbegin()).get_ts_last_updated());
                        return;
                      }
                  }
              }

            typename std::set<T>::const_iterator hint = to.end();
            for (unsigned int i = left_bound; i <= right_bound; ++i)
              {
                hint = to.insert(hint, from[i]);
                stat.m_data_size += from[i].get_data_size();
              }

            stat.m_data_point_num += (right_bound - left_bound + 1);

            // here stat and check_size are only used to control size of in-memory read data and
            // stop if query takes too much resources (e.g. for in-memory downsampling)
            //
            // true stat data used to calculate size of message are filled in make_stat() call
            // taking into account compaction by merge() algorithm
            stat.check_size();
          }
      }


    // downsample data

    template<class T>
      void
      match_and_add_downsample(std::vector<T>& from, uint64_t since, uint64_t until, std::set<T>& to)
      {
        const unsigned int len(from.size());
        unsigned int left_bound(std::numeric_limits<unsigned int>::max()), right_bound(std::numeric_limits<unsigned int>::max());

        //
        //       SINCE   UNTIL
        //         |       |
        //      *  |       |        (1)
        //         |       |  *     (2)
        //         |   *   |        (3)
        //         *       |        (4)
        //         |       *        (5)
        //         *********        (6) , i.e. SINCE == UNTIL == TS
        //         |       |
        //

        for (unsigned int i = 0; i < len; ++i)
          {
            const uint64_t ts(from[i].get_ts());

            // cases (1,4,6)
            if (ts <= since)
              {
                left_bound = i;

                if (ts == until)
                  {
                    right_bound = i;
                    break;
                  }
              }

            // case (3)
            else if (ts < until)
              {
                if (left_bound == std::numeric_limits<unsigned int>::max())
                  {
                    left_bound = i;
                  }
                right_bound = i;
              }

            // cases (2,5); note case 6 is already excluded
            if (ts >= until)
              {
                if (left_bound == std::numeric_limits<unsigned int>::max())
                  {
                    if (ts == until)
                      {
                        left_bound = right_bound = i;
                      }
                  }
                else
                  {
                    right_bound = i;
                  }

                break;
              }
          }

        if (left_bound == std::numeric_limits<unsigned int>::max() || right_bound == std::numeric_limits<unsigned int>::max())
          return;

        typename std::set<T>::const_iterator hint = to.end();
        for (unsigned int i = left_bound; i <= right_bound; ++i)
          {
            std::size_t old_size = to.size();
            hint = to.insert(hint, from[i]);

            if (old_size == to.size())
              {
                const T& old_value(*hint);
                from[i].merge(old_value);
                to.erase(hint);
                hint = to.insert(from[i]).first;
              }
          }
      }


  public:

    /**
     *  \brief Merge raw data in-memory taking into account end-of-validity information
     *
     *  This method is declared public because it is shared by the read read_attribute() methods and by the pbeast_merger utility.
     *  It should not be directly called by end user.
     *
     *  Parameters:
     *  \param from       input data to be merged
     *  \param eov        input pointer to end of object validity (can be nullprt if there is no such information)
     *  \param to         merged data
     */

    template<class T>
      static void
      merge(const std::set<T>& from, const std::set<uint64_t> * eov, std::vector<T>& to) noexcept
      {
        if (unsigned int size = from.size())
          {
            to.reserve(size);

            for (auto &i : from)
              {
                if (to.empty() == false)
                  {
                    T& last(to.back());
                    if (last.get_value() == i.get_value())
                      {
                        if (eov)
                          {
                            std::set<uint64_t>::const_iterator j = eov->upper_bound(last.get_ts_last_updated());

                            if (j == eov->end() || (*j) > i.get_ts_last_updated())
                              {
                                ERS_DEBUG( 1, "merge[IoV] [" << last.get_ts_created()
                                    << ',' << last.get_ts_last_updated() << "] and ["
                                    << i.get_ts_created() << ','
                                    << i.get_ts_last_updated() << ']');
                                last.set_ts_last_updated(i.get_ts_last_updated());
                                continue;
                              }
                            else if (i.get_ts_created() < (*j))
                              {
                                ERS_DEBUG( 1, "split and merge[EoV] [" << i.get_ts_created() << ',' << i.get_ts_last_updated() << "] by EoV " << *j );
                                last.set_ts_last_updated(i.get_ts_created());
                                const_cast<T&>(i).set_ts_created(i.get_ts_last_updated());
                              }
                          }
                        else
                          {
                            ERS_DEBUG( 1, "merge[no EoV] [" << last.get_ts_created()
                                << ',' << last.get_ts_last_updated() << "] and ["
                                << i.get_ts_created() << ','
                                << i.get_ts_last_updated() << ']');
                            last.set_ts_last_updated(i.get_ts_last_updated());
                            continue;
                          }
                      }
                  }
                else if (eov)
                  {
                    std::set<uint64_t>::const_iterator j = eov->upper_bound(i.get_ts_created());

                    if (j != eov->end() && i.get_ts_last_updated() > (*j))
                      {
                        uint64_t last_updated(i.get_ts_last_updated());
                        ERS_DEBUG(1, "split [" << i.get_ts_created() << ',' << i.get_ts_last_updated() << "] by EoV " << *j);
                        const_cast<T&>(i).set_ts_last_updated(i.get_ts_created());
                        to.emplace_back(i);
                        const_cast<T&>(i).set_ts_created(last_updated);
                        const_cast<T&>(i).set_ts_last_updated(last_updated);
                      }
                  }

                ERS_DEBUG( 2, "insert IoV " << i.get_ts_created() << " - " << i.get_ts_last_updated());
                to.emplace_back(i);
              }
          }
      }

  public:

    /**
     *  Any repository read sets READ lock
     *  The external merge operation involving "remove" files operation sets WRITE lock
     */
    std::shared_mutex m_repository_files;

    /**
     *  Any find and read downsampled files operations set READ lock
     *  The external cache directory clean operation involving "remove" files operation sets WRITE lock
     */
    std::shared_mutex m_ds_cache_files;

    Directory*
    get_directory(const std::filesystem::path& path, bool add_to_cache_if_not_found);

    /**
     *  Clean least used directories, if cache exceeds max size
     */
    uint64_t
    clean_series_info(uint64_t max_size);

    std::size_t
    get_repository_path_len() const
    {
      return m_repository_path_len;
    }

    std::size_t
    get_local_repository_path_len() const
    {
      return m_local_repository_path_len;
    }

    const std::filesystem::path&
    get_repository_path() const
    {
      return m_repository_path;
    }

    const std::filesystem::path&
    get_local_repository_path() const
    {
      return m_local_repository_path;
    }

    const pbeast::DownsampleConfig&
    get_downsample_config()
    {
      return m_downsample_config;
    }


  private:

    pbeast::WriterLock * m_writer_lock;
    const std::filesystem::path m_repository_path;
    const std::filesystem::path m_local_repository_path;

    const std::size_t m_repository_path_len;
    const std::size_t m_local_repository_path_len;

    inline static const std::string s_eov_attribute_name = "end~of~validity";
    inline static const std::string s_upd_attribute_name = "updated~info";

    bool m_profiling;

    pbeast::DownsampleConfig m_downsample_config;
    pbeast::ThreadPool m_downsample_thread_pool;
    std::map<std::filesystem::path,std::shared_future<void> > m_downsample_queue;

    std::mutex m_directories_mutex;
    std::map<std::string, std::unique_ptr<Directory>> m_directories;

    static std::size_t
    get_path_len(const std::filesystem::path& path);

    void
    get_files(const std::filesystem::path& path, uint32_t since, uint32_t until, pbeast::FileSet& files, CallStats& stat)
    {
      get_directory(path, true)->get_files(since, until, files, stat);
    }

    // return true or throw exception if file is not compatible with data type
    static bool
    check_not_compatible(DataFile& file, DataType data_type, bool is_array, bool throw_on_error);

    void
    read_void(const std::string& attr_name, const std::string& partition_name, const std::string& class_name, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, const std::string& object_id, std::set<uint64_t>& result, CallStats& stat);

    void
    read_void(const std::string& attr_name, const std::string& partition_name, const std::string& class_name, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, boost::regex * re, std::map<std::string, std::set<uint64_t>>& result, CallStats& stat);

    void
    read_void(const FileSet& files, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, const std::string& object_id, std::set<uint64_t>& result, CallStats& stat);

    void
    read_void(const FileSet& files, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, boost::regex * re, const std::set<std::string> *, std::map<std::string, std::set<uint64_t>>& result, CallStats& stat);

    void
    read_downsample_void(const FileSet& merged_files, const FileSet& local_files, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, uint32_t sample_interval, const std::string& object_id, std::set<uint32_t>& eov32, std::set<uint64_t>& eov64, CallStats& stat, CallStats& local_stat);

    void
    read_downsample_void(const FileSet& merged_files, const FileSet& local_files, uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, uint32_t sample_interval, const std::string& ids, boost::regex * re, const std::set<std::string> *, std::map<std::string,std::set<uint32_t>>& eov, std::map<std::string,std::set<uint64_t>>& eov64, CallStats& stat, CallStats& local_stat);

    void
    read_persistent_downsample_void(const FileSet& merged_files, uint32_t since, uint32_t until, uint32_t sample_interval, const std::string& object_id, std::set<uint32_t>& eov, CallStats& stat);

    void
    read_persistent_downsample_void(const FileSet& merged_files, uint32_t since, uint32_t until, uint32_t sample_interval, const std::string& ids, boost::regex * re, const std::set<std::string> *, std::map<std::string,std::set<uint32_t>>& eov, CallStats& stat);


    template<class T>
      void
      read_persistent_downsample_data(const FileSet& merged_files, const std::filesystem::path& attribute_path, uint32_t since, uint32_t until, uint32_t sample_interval, pbeast::FillGaps::Type fill_gaps, const std::string& object_id, bool object_id_is_regex, const std::string& suffix, bool suffix_is_regex, std::map<std::string, std::vector<T>>& data, CallStats& stat)
      {
        std::map<std::string, std::set<T>> m_data;

          {
            std::unique_ptr<boost::regex> object_id_re;
            std::unique_ptr<boost::regex> suffix_re;
            std::string suffix_separator;

            if (object_id_is_regex && !object_id.empty())
              {
                try
                  {
                    object_id_re.reset(new boost::regex(object_id));
                  }
                catch (std::exception& ex)
                  {
                    throw daq::pbeast::RegularExpressionError(ERS_HERE, "cannot parse regular expression", object_id, ex);
                  }
              }

            if (suffix_is_regex && !suffix.empty())
              {
                try
                  {
                    suffix_re.reset(new boost::regex(suffix));
                  }
                catch (std::exception& ex)
                  {
                    throw daq::pbeast::RegularExpressionError(ERS_HERE, "cannot parse regular expression", suffix, ex);
                  }
              }

            if (suffix_is_regex || !suffix.empty())
              {
                suffix_separator = get_suffix_separator(attribute_path);
              }

            std::shared_lock lock(m_ds_cache_files);

            FileSet files = downsample_files(merged_files, sample_interval, object_id, object_id_re.get(), stat);
            stat.m_num_of_data_files += merged_files.size();

            if (files.size() == 0)
              return;

            std::vector<T> v;

            for (auto &f : files)
              {
                m_writer_lock->wait_read_permission();
                pbeast::FileOpenGuard open(f.get());

                if (check_not_compatible(*f, T::type(), T::is_array(), false))
                  continue;

                if (object_id_is_regex == false && !object_id.empty() && suffix_is_regex == false && suffix.empty())
                  {
                    v.clear();
                    stat.m_num_of_primitive_data_reads += f->read_serie(object_id, v);
                    match_and_add_downsample(v, since, until, m_data[object_id]);
                    continue;
                  }

                const std::map<std::string, uint64_t>& info = f->get_series_info();

                for (const auto & x : info)
                  {
                    // non nested types (no suffixes)
                    if (suffix_is_regex == false && suffix.empty())
                      {
                        // regex for object id
                        if (object_id_re)
                          try
                            {
                              if (boost::regex_match(x.first, *object_id_re) == false)
                                continue;
                            }
                          catch (std::exception& ex)
                            {
                              throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
                            }

                        // corner case: object id is empty and the query is not regex
                        else if (!object_id_is_regex && object_id.empty() && !x.first.empty())
                          continue;
                      }

                    // nested types (any, regex or explicit suffix)
                    else
                      {
                        std::size_t idx = x.first.find(suffix_separator);
                        if (idx == std::string::npos)
                          {
                            throw daq::pbeast::MissingNestedObjectNameSeparator(ERS_HERE, suffix_separator, x.first);
                          }

                        std::string obj = x.first.substr(0, idx);

                        if (object_id_re)
                          {
                            try
                              {
                                if (boost::regex_match(obj, *object_id_re) == false)
                                  continue;
                              }
                            catch (std::exception& ex)
                              {
                                throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", obj, ex);
                              }
                          }

                        // explicit object id (this method is non-efficient, use previous one)
                        else if (!object_id.empty())
                          {
                            if (object_id != obj)
                              continue;
                          }

                        std::string obj_suffix = x.first.substr(idx + 1);

                        if (suffix_is_regex && !suffix.empty())
                          {
                            try
                              {
                                if (boost::regex_match(obj_suffix, *suffix_re) == false)
                                  continue;
                              }
                            catch (std::exception& ex)
                              {
                                throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", obj_suffix, ex);
                              }
                          }
                        else if (!suffix.empty())
                          {
                            if (obj_suffix != suffix)
                              continue;
                          }
                      }

                    v.clear();
                    stat.m_num_of_primitive_data_reads += f->read_serie(x.second, v);
                    match_and_add_downsample(v, since, until, m_data[x.first]);
                  }
              }
          }

        for (auto &s : m_data)
          {
            std::set<T>& m_data_set(s.second);

            if (!m_data_set.empty())
              {
                std::vector<T>& serie_data(data[s.first]);
                pbeast::downsample(sample_interval, m_data_set, serie_data, fill_gaps);
              }
          }
      }

    template<class T>
      void
      adjust_bondary_items(uint64_t since, uint64_t until, std::vector<T>& data)
    {
      unsigned long len = data.size();
      if (len > 0)
        {
          if (data[0].get_ts_last_updated() < since)
            {
              data[0].set_ts_created(data[0].get_ts_last_updated());
            }

          len--;

          if (data[len].get_ts_created() > until)
            {
              data[len].set_ts_last_updated(data[len].get_ts_created());
            }
        }
    }


    template<class T>
      void
      read_attribute(const FileSet& files, const FileSet& eov_files, const FileSet& upd_files, const std::filesystem::path& attribute_path,
          uint64_t since, uint64_t until, uint64_t w_since, uint64_t w_until, const std::string& object_id, bool object_id_is_regex, const std::string& suffix, bool suffix_is_regex,
          std::map<std::string,std::vector<T>>& data, std::map<std::string,std::set<uint64_t>>& eov, std::map<std::string,std::set<uint64_t>>& upd, CallStats& stat)
      {
        if (files.size() == 0 && eov_files.size() == 0 && upd_files.size() == 0)
          return;

        const bool complex_attribute_path = (attribute_path.native().find(std::filesystem::path::preferred_separator) != std::string::npos);

        stat.m_num_of_data_files += files.size();

        std::unique_ptr<boost::regex> object_id_re;
        std::unique_ptr<boost::regex> suffix_re;
        std::string suffix_separator;

        if (!suffix.empty())
          {
            if (suffix_is_regex)
              {
                try
                  {
                    suffix_re.reset(new boost::regex(suffix));
                  }
                catch (std::exception& ex)
                  {
                    throw daq::pbeast::RegularExpressionError(ERS_HERE, "cannot parse regular expression", suffix, ex);
                  }
              }
          }

        if (suffix_is_regex || !suffix.empty())
          {
            suffix_separator = get_suffix_separator(attribute_path);
          }

        // object_id is regex
        if (object_id_is_regex)
          {
            try
              {
                if (!object_id.empty())
                  object_id_re.reset(new boost::regex(object_id));
              }
            catch (std::exception& ex)
              {
                throw daq::pbeast::RegularExpressionError(ERS_HERE, "cannot parse regular expression", object_id, ex);
              }

            read_void(eov_files, since, until, w_since, w_until, object_id_re.get(), nullptr, eov, stat);
            read_void(upd_files, since, until, w_since, w_until, object_id_re.get(), nullptr, upd, stat);
          }

        // otherwise use simple method to read eov by explicit name (most efficient way)
        else
          {
            std::set<uint64_t>& eov_set(eov[object_id]);

            read_void(eov_files, since, until, w_since, w_until, object_id, eov_set, stat);

            if (eov_set.empty())
              eov.erase(object_id);

            if (upd_files.empty() == false)
              {
                std::set<uint64_t>& upd_set(upd[object_id]);

                read_void(upd_files, since, until, w_since, w_until, object_id, upd_set, stat);

                if (upd_set.empty())
                  upd.erase(object_id);
              }
          }

        std::map<std::string, std::set<T>> m_data;

        std::vector<T> v;

        for (auto &f : files)
          {
            m_writer_lock->wait_read_permission();
            pbeast::FileOpenGuard open(f.get());

            if (check_not_compatible(*f, T::type(), T::is_array(), false))
              continue;

            // process efficiently special case: non-nested types (no suffixes), explicit non-empty object id
            if (object_id_is_regex == false && !object_id.empty() && suffix_is_regex == false && suffix.empty())
              {
                v.clear();
                stat.m_num_of_primitive_data_reads += f->read_serie(object_id, w_since, w_until, v);
                match_and_add_raw(v, since, until, m_data[object_id], stat);
                continue;
              }

            const std::map<std::string, uint64_t>& info = f->get_series_info();

            for (const auto & x : info)
              {
                // non nested types (no suffixes)
                if (suffix_is_regex == false && suffix.empty())
                  {
                    // regex for object id
                    if (object_id_re)
                      try
                        {
                          if (boost::regex_match(x.first, *object_id_re) == false)
                            continue;
                        }
                      catch (std::exception& ex)
                        {
                          throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", x.first, ex);
                        }

                    // corner case: object id is empty and the query is not regex
                    else if (!object_id_is_regex && object_id.empty() && !x.first.empty())
                      continue;
                  }

                // nested types (any, regex or explicit suffix)
                else
                  {
                    std::size_t idx = x.first.find(suffix_separator);
                    if (idx == std::string::npos)
                      {
                        throw daq::pbeast::MissingNestedObjectNameSeparator(ERS_HERE, suffix_separator, x.first);
                      }

                    std::string obj = x.first.substr(0, idx);

                    if (object_id_re)
                      try
                        {
                          if (boost::regex_match(obj, *object_id_re) == false)
                            continue;
                        }
                      catch (std::exception& ex)
                        {
                          throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", obj, ex);
                        }

                    // explicit object id (this method is non-efficient, use previous one)
                    else if (!object_id.empty())
                      {
                        if (object_id != obj)
                          continue;
                      }

                    std::string obj_suffix = x.first.substr(idx + 1);

                    if (suffix_is_regex && !suffix.empty())
                      {
                        try
                          {
                            if (boost::regex_match(obj_suffix, *suffix_re) == false)
                              continue;
                          }
                        catch (std::exception& ex)
                          {
                            throw daq::pbeast::RegularExpressionError(ERS_HERE, "boost::regex_match() failed for string", obj_suffix, ex);
                          }
                      }
                    else if (!suffix.empty())
                      {
                        if (obj_suffix != suffix)
                          continue;
                      }
                  }

                v.clear();
                stat.m_num_of_primitive_data_reads += f->read_serie(x.second, w_since, w_until, v);
                match_and_add_raw(v, since, until, m_data[x.first], stat);
              }
          }

        if (complex_attribute_path)
          {
            std::set<std::string> names;

            for (const auto &s : m_data)
              names.insert(pbeast::get_object_name(s.first));

            read_void(eov_files, since, until, w_since, w_until, nullptr, &names, eov, stat);
            read_void(upd_files, since, until, w_since, w_until, nullptr, &names, upd, stat);
          }

        for (auto &s : m_data)
          {
            std::set<T>& m_data_set(s.second);

            if (!m_data_set.empty())
              {
                std::vector<T>& serie_data(data[s.first]);

                std::set<uint64_t> * serie_eov(nullptr);
                std::map<std::string, std::set<uint64_t>>::iterator ser_it = eov.find(complex_attribute_path == false ? s.first : pbeast::get_object_name(s.first));

                if (ser_it != eov.end())
                  serie_eov = &ser_it->second;

                merge(m_data_set, serie_eov, serie_data);
                adjust_bondary_items(since, until, serie_data);
              }
          }

        // if there is upd information, adjust edge data points
        if (upd_files.empty() == false)
          {
            for (auto& x : data)
              {
                auto upd_it = upd.find(complex_attribute_path == false ? x.first : pbeast::get_object_name(x.first));
                if(upd_it != upd.end())
                  {
                    T& first = x.second[0];
                    if(first.get_ts_created() != first.get_ts_last_updated() && first.get_ts_created() < since)
                      {
                        const uint64_t closest_prev_update = *(upd_it->second.begin());
                        if(closest_prev_update > first.get_ts_created())
                          {
                            first.set_ts_created(closest_prev_update);
                          }
                      }

                    T& last = x.second[x.second.size()-1];
                    if(last.get_ts_created() != last.get_ts_last_updated() && last.get_ts_last_updated() > until)
                      {
                        const uint64_t closest_next_update = *(upd_it->second.rbegin());
                        if(closest_next_update < last.get_ts_last_updated())
                          {
                            last.set_ts_last_updated(closest_next_update);
                          }
                      }
                  }
              }
          }

        make_stat(data, stat);
        add_stat(eov, stat);
        if (upd_files.empty() == false)
          add_stat(upd, stat);
        stat.check_size();
     }

    static void
    trim(std::map<std::string,std::set<uint32_t>>& vdata, uint32_t w_since, uint32_t w_until);

    static void
    trim_but_one(std::map<std::string,std::set<uint32_t>>& data, uint32_t since, uint32_t until);

    template<class T>
    static void
    trim_but_one(std::map<std::string, std::vector<T>>& data, uint32_t since, uint32_t until)
    {
      for (auto& x : data)
        {
          if (const std::size_t len = x.second.size())
            {
              std::size_t end_idx = len;

              for (std::size_t idx = len; idx-- > 0;)
                if (x.second[idx].get_ts() > until)
                  end_idx = idx;
                else
                  break;

              if (end_idx < (len-1) && end_idx > 0)
                x.second.erase(x.second.begin() + 1 + end_idx, x.second.end());
            }

          if (const std::size_t len = x.second.size())
            {
              std::size_t begin_idx = 0;

              for (std::size_t idx = 0; idx < len; ++idx)
                if (x.second[idx].get_ts() < since)
                  begin_idx = idx;
                else
                  break;

              if (begin_idx != 0 && begin_idx < (len - 1))
                  x.second.erase(x.second.begin(), x.second.begin() + begin_idx);
            }
        }
    }



    template<class T>
      static void
      adjust_boundaries(std::map<std::string, std::vector<T>>& data, std::map<std::string,std::set<uint32_t>>& eovs, std::map<std::string, std::set<uint32_t>>* upds, uint32_t w_since, uint32_t w_until)
      {
        trim(eovs, w_since, w_until);

        if (upds)
          trim(*upds, w_since, w_until);

        for (auto& x : data)
          {
            if (const std::size_t len = x.second.size())
              {
                constexpr std::size_t n_pos = std::numeric_limits<std::size_t>::max();

                std::size_t begin_idx(n_pos), end_idx(n_pos);

                for (std::size_t idx = 0; idx < len; ++idx)
                  if (x.second[idx].get_ts() < w_since)
                    begin_idx = idx;
                  else
                    break;

                for (std::size_t idx = len; idx-- > 0;)
                  if (x.second[idx].get_ts() > w_until)
                    end_idx = idx;
                  else
                    break;

                if (end_idx != n_pos || begin_idx != n_pos)
                  {
                    std::set<uint32_t> * upd = nullptr;

                    if (upds)
                      {
                        auto it = pbeast::find(*upds, x.first);
                        if (it != upds->end())
                          upd = &it->second;
                      }

                    if (end_idx != n_pos)
                      {
                        uint32_t until = w_until;

                        if (upd)
                          {
                            auto prev_it = upd->lower_bound(w_until);
                            if (prev_it != upd->begin() && *prev_it != w_until)
                              {
                                const uint64_t new_until = *std::prev(prev_it);
                                if (end_idx == 0 || x.second[end_idx-1].get_ts() < new_until)
                                  until = new_until;
                              }
                          }

                        x.second[end_idx].set_ts(until);

                        if (end_idx != (len - 1))
                          x.second.erase(x.second.begin() + 1 + end_idx, x.second.end());
                      }

                    if (begin_idx != n_pos)
                      {
                        uint32_t since = w_since;

                        if (upd)
                          {
                            auto next_it = upd->lower_bound(w_since);
                            if (next_it != upd->end())
                              {
                                if(x.second.size() == (begin_idx+1) || x.second[begin_idx + 1].get_ts() > *next_it)
                                  since = *next_it;
                              }
                          }

                        x.second[begin_idx].set_ts(since);

                        if (begin_idx != 0)
                          x.second.erase(x.second.begin(), x.second.begin() + begin_idx);
                      }
                  }
              }
          }
      }

  };
}

#endif
