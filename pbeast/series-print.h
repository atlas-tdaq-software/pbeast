#ifndef PBEAST_SERIES_PRINT_H
#define PBEAST_SERIES_PRINT_H

#include <cmath>
#include <ostream>
#include <set>
#include <cstdio>

#include <boost/date_time/local_time/local_time.hpp>

#include "pbeast/functions.h"
#include "pbeast/series-data.h"
#include "pbeast/data-point.h"
#include "pbeast/data-cvt.h"
#include "pbeast/downsample-algo.h"
#include "pbeast/object-name.h"


namespace pbeast {

  // used to print formatted date from uint32_t
  struct Date
  {
    Date(uint32_t x) : v(x) { ; }
    Date(double x) : v(static_cast<uint32_t>(x)) { ; }
    uint32_t v;

    bool
    operator==(const Date& x) const
    {
      return (v == x.v);
    }

    bool
    operator!=(const Date& x) const
    {
      return (v != x.v);
    }
  };

  // used to print formatted time from uint64_t
  struct Time
  {
    Time(uint64_t x) : v(x) { ; }
    Time(double x) : v(static_cast<uint64_t>(x)) { ; }
    uint64_t v;

    bool
    operator==(const Time& x) const
    {
      return (v == x.v);
    }

    bool
    operator!=(const Time& x) const
    {
      return (v != x.v);
    }
  };

  // used by bstconfig for enum type validation
  struct Enum
  {
    Enum() { ; }
  };

  void print_date(uint32_t value, std::ostream& s);
  void print_time(uint32_t value, boost::local_time::time_zone_ptr tz_ptr, std::ostream& s);
  void print_time(uint64_t value, boost::local_time::time_zone_ptr tz_ptr, std::ostream& s); /// print time with microseconds precision
  void print_tss(std::ostream& s, uint64_t created_ts, uint64_t last_updated_ts, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix);
  void print_ts(std::ostream& s, uint32_t ts, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix);

  void print_eov(std::ostream& s, uint64_t data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix);
  void print_eov(std::ostream& s, uint32_t data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix);

  void print_eov_serie(std::ostream& s, const std::pair<const std::string, std::set<uint64_t>> & x, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix);
  void print_eov_serie(std::ostream& s, const std::pair<const std::string, std::set<uint32_t>> & x, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix);


  template<class T>
    void
    print(std::ostream& s, const T & data, bool is_json = false)
    {
      s << data;
    }

  template<class T>
  void
    print_floating_type(std::ostream& s, const T & data)
    {
      if (std::isnan(data) || std::isinf(data))
        s << '\"' << data << '\"';
      else
        s << data;
    }

  template<>
    void inline
    print<Date>(std::ostream& s, const Date & data, bool )
    {
      print_date(data.v, s);
    }

  template<>
    void inline
    print<Time>(std::ostream& s, const Time & data, bool )
    {
      print_time(data.v, nullptr, s);
    }

  template<>
    void inline
    print<uint8_t>(std::ostream& s, const uint8_t & data, bool )
    {
      s << static_cast<uint16_t>(data);
    }

  template<>
    void inline
    print<int8_t>(std::ostream& s, const int8_t & data, bool )
    {
      s << static_cast<int16_t>(data);
    }

  template<>
    void inline
    print<float>(std::ostream& s, const float & data, bool )
    {
      print_floating_type(s, data);
    }

  template<>
    void inline
    print<double>(std::ostream& s, const double & data, bool )
    {
      print_floating_type(s, data);
    }

  template<>
    void inline
    print<std::string>(std::ostream& s, const std::string & data, bool is_json)
    {
      auto buf = s.rdbuf();

      buf->sputc('\"');

      if (!is_json)
        buf->sputn(data.c_str(), data.size());
      else
        for (const auto& ch : data)
          switch (ch)
            {
              case '\"':
              case '\\' :
              case '/' :
                buf->sputc('\\'); buf->sputc(ch); break;

              case '\b' :
                buf->sputc('\\'); buf->sputc('b'); break;

              case '\f' :
                buf->sputc('\\'); buf->sputc('f'); break;

              case '\n' :
                buf->sputc('\\'); buf->sputc('n'); break;

              case '\r' :
                buf->sputc('\\'); buf->sputc('r'); break;

              case '\t' :
                buf->sputc('\\'); buf->sputc('t'); break;

              default:
                if (static_cast<unsigned char>(ch) < 0x20 || ch == 0x7f)
                  {
                    char utf8_buf[7];
                    auto len = std::snprintf(utf8_buf, sizeof(utf8_buf), "\\u%04x", ch & 0xff);
                    buf->sputn(utf8_buf, len);
                  }
                else
                  buf->sputc(ch);
            }

      buf->sputc('\"');
    }

  template<>
    void inline
    print<pbeast::Void>(std::ostream& s, const pbeast::Void & data, bool )
    {
      ;
    }

  template<class T>
  void
    print_value(std::ostream& s, const T & data, uint64_t t1, uint64_t t2, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix)
    {
       print_tss(s, t1, t2, tz_ptr, ts2str, prefix);
       s << ' ';
       print(s, data);
       s << '\n';
    }

  template<class T>
  void
    print_value(std::ostream& s, const std::vector<T> & data, uint64_t t1, uint64_t t2, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix)
    {
      print_tss(s, t1, t2, tz_ptr, ts2str, prefix);
      s << ' ' << data.size() << " items [";

      for (typename std::vector<T>::const_iterator j = data.cbegin(); j != data.cend(); ++j)
        {
          if (j != data.cbegin())
            s << ", ";
          print(s, *j);
        }

      s << "]\n";
    }

  template<class T>
    void
    print(std::ostream& s, const std::vector<T> & data, bool is_json = false)
    {
      s << data.size() << " items [";

      for (typename std::vector<T>::const_iterator i = data.cbegin(); i != data.cend(); ++i)
        {
          if (i != data.cbegin())
            s << ", ";
          print(s, *i, is_json);
        }

      s << ']';
    }

  template<class T>
  void
    print_json(std::ostream& s, const T & value)
    {
      pbeast::print(s, value, true);
    }

  template<class T>
    void
    print_json(std::ostream &s, const std::vector<T> &value, char c1 = 0, char c2 = 0)
    {
      s << '[';
      bool is_first(true);
      for (auto const &x : value)
        {
          if (is_first)
            is_first = false;
          else
            s << ',';
          if (c1) s << c1;
          pbeast::print(s, x, true);
          if (c2) s << c2;
        }
      s << ']';
    }

  template<class T>
    void
    SeriesData<T>::print_json_value(std::ostringstream& ss) const
    {
      print_json(ss, m_value);
    }

  template<class T>
    void
    SeriesVectorData<T>::print_json_value(std::ostringstream& ss) const
    {
      print_json(ss, m_value, '[', ']');
    }

  template<class T, class V>
    void
    AverageValue<T,V>::print_json_value(std::ostringstream& ss, const pbeast::AggregationType at) const
    {
      if (at == pbeast::AggregationAverage || at == pbeast::AggregationSum)
        {
          print_json(ss, m_value);
        }
      else
        {
          print_json(ss, m_min_value);

          if (at == pbeast::AggregationNone && m_min_value != m_max_value)
            {
              ss << ',';
              print_json(ss, m_value);
              ss << ',';
              print_json(ss, m_max_value);
            }
        }
    }

  template<class T>
    void
    DownsampledSeriesData<T>::print_json_value(std::ostringstream& ss, const pbeast::AggregationType at) const
    {
      m_data.print_json_value(ss, at);
    }

  template<class T>
    void
    DownsampledSeriesVectorData<T>::print_json_value(std::ostringstream& ss, const pbeast::AggregationType at) const
    {
      ss << '[';
      bool is_first(true);
      for (auto const& x : m_data)
        {
          if (is_first)
            is_first = false;
          else
            ss << ',';
          ss << '[';
          x.print_json_value(ss, at);
          ss << ']';
        }
      ss << ']';
    }



  template<class T>
    void
    print(std::ostream& s, const std::vector<SeriesData<T> >& data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str = true, const char * prefix = "  ")
    {
      for (uint32_t i = 0; i < data.size(); ++i)
        {
          print_tss(s, data[i].get_ts_created(), data[i].get_ts_last_updated(), tz_ptr, ts2str, prefix);
          s << ' ';
          print(s, data[i].get_value());
          s << '\n';
        }
    }


  template<class T>
    void
    print(std::ostream& s, const std::vector<SeriesVectorData<T> >& data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str = true, const char * prefix = "  ")
    {
      for (uint32_t i = 0; i < data.size(); ++i)
        {
          print_tss(s, data[i].get_ts_created(), data[i].get_ts_last_updated(), tz_ptr, ts2str, prefix);
          s << ' ' << data[i].get_value().size() << " items [";

          for (typename std::vector<T>::const_iterator j = data[i].get_value().cbegin(); j != data[i].get_value().cend(); ++j)
            {
              if (j != data[i].get_value().cbegin())
                s << ", ";
              print(s, *j);
            }

          s << "]\n";
        }
    }


  template<class T>
    void
    print(std::ostream& s, const std::vector<T>& data, const std::set<uint64_t>& eov, const std::set<uint64_t> * updates, boost::local_time::time_zone_ptr tz_ptr, bool ts2str = true, const char * prefix = "  ")
    {
      std::set<uint64_t>::const_iterator eov_it = eov.begin();
      uint64_t eov_next = (eov_it != eov.end()) ? *eov_it : 0 ;

      std::set<uint64_t>::const_iterator udt_it;
      uint64_t udt_next = 0 ;
      if (updates)
        {
          udt_it = updates->begin();
          udt_next = *udt_it;
        }

      const auto len = data.size();

      for (uint32_t i = 0; i < len; ++i)
        {
          const auto& value(data[i].get_value());
          const uint64_t created_ts(data[i].get_ts_created());

          while(eov_next != 0 && eov_next < created_ts)
            {
              print_eov(s, eov_next, tz_ptr, ts2str, prefix);
              eov_it++;
              eov_next = (eov_it != eov.end()) ? *eov_it : 0 ;
            }

          if (updates)
            {
              while (udt_next != 0 && udt_next <= created_ts)
                {
                  udt_it++;
                  udt_next = (udt_it != updates->end()) ? *udt_it : 0;
                }

              print_value(s, value, created_ts, created_ts, tz_ptr, ts2str, prefix);

              uint64_t until(len - i > 1 ? data[i+1].get_ts_created() : std::numeric_limits<uint64_t>::max());

              if (eov_next && eov_next < until)
                until = eov_next;

              while (udt_next != 0 && udt_next < until)
                {
                  print_value(s, value, udt_next, udt_next, tz_ptr, ts2str, prefix);
                  udt_it++;
                  udt_next = (udt_it != updates->end()) ? *udt_it : 0;
                }
            }
          else
            {
              print_value(s, value, created_ts, data[i].get_ts_last_updated(), tz_ptr, ts2str, prefix);
            }
        }

      if (eov_next != 0)
        {
          print_eov(s, eov_next, tz_ptr, ts2str, prefix);
        }
    }

  template<class T>
    void
    print(std::ostream& s, const DownsampledSeriesData<T>& v, uint32_t ts, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix)
    {
      print_ts(s, ts, tz_ptr, ts2str, prefix);
      s << " (";
      print(s, v.data().get_min_value());
      s << ',';
      print(s, v.data().get_value());
      s << ',';
      print(s, v.data().get_max_value());
      s << ")\n";
    }

  template<class T>
    void
    print(std::ostream& s, const DownsampledSeriesData<T>& v, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix)
    {
      print(s, v, v.get_ts(), tz_ptr, ts2str, prefix);
    }

  template<class T>
    void
    print(std::ostream& s, const DownsampledSeriesVectorData<T>& v, uint32_t ts, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix)
    {
      print_ts(s, ts, tz_ptr, ts2str, prefix);

      s << ' ' << v.data().size() << " items [";

      for (unsigned int j = 0; j < v.data().size(); ++j)
        {
          if (j != 0)
            s << ", ";

          s << '(';
          print(s, v.data(j).get_min_value());
          s << ',';
          print(s, v.data(j).get_value());
          s << ',';
          print(s, v.data(j).get_max_value());
          s << ')';
        }

      s << "]\n";
    }

  template<class T>
    void
    print(std::ostream& s, const DownsampledSeriesVectorData<T>& v, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix)
    {
      print(s, v, v.get_ts(), tz_ptr, ts2str, prefix);
    }

  template<class T>
    void
    print(std::ostream& s, const std::vector<T>& data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str = true, const char * prefix = "  ")
    {
      for (uint32_t i = 0; i < data.size(); ++i)
        print(s, data[i], tz_ptr, ts2str, prefix);
    }

  template<class T>
    void
    print(std::ostream& s, const std::vector<T>& data, const std::set<uint32_t>& eov, const std::set<uint32_t>& upd, boost::local_time::time_zone_ptr tz_ptr, bool ts2str = true, const char * prefix = "  ")
    {
      #define EOV_PRINT_CODE       print_eov(s, eov_next, tz_ptr, ts2str, prefix);
      #define DATA_PRINT_CODE(TS)  print(s, *it, TS, tz_ptr, ts2str, prefix);

      APPLY_DS_ALGO(data, eov, upd, EOV_PRINT_CODE, DATA_PRINT_CODE)
    }


  void print(std::ostream& s, const std::vector<uint64_t>& data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str = true, const char * prefix = "  ");

  void print(std::ostream& s, const std::set<uint64_t>& data, boost::local_time::time_zone_ptr tz_ptr, bool ts2str = true, const char * prefix = "  ");


  template<class T>
    void
    print(std::ostream& s, const std::map<std::string, std::vector<T>>& data, const std::map<std::string, std::set<uint64_t>>& map_eov, const std::map<std::string, std::set<uint64_t>>* map_updates, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix)
    {
      std::set<uint64_t> empty;

      // print time series data
      for (const auto & x : data)
        {
          std::map<std::string, std::set<uint64_t>>::const_iterator e = pbeast::find(map_eov, x.first);
          const std::set<uint64_t>& eov(e != map_eov.end() ? e->second : empty);
          const std::set<uint64_t>* obj_upd(nullptr);

          if (map_updates)
            {
              std::map<std::string, std::set<uint64_t>>::const_iterator u(pbeast::find(*map_updates, x.first));
              if (u != map_updates->end())
                obj_upd = &u->second;
              else
                obj_upd = &empty;
            }

          s << x.first << ":\n";
          pbeast::print(s, x.second, eov, obj_upd, tz_ptr, ts2str, prefix);
        }

      // print deleted objects with no data
      for (const auto& x : map_eov)
        {
          if (data.count(x.first) == 0)
            {
              print_eov_serie(s, x, tz_ptr, ts2str, prefix);
            }
        }
    }


  template<class T>
    void
    print_data_point(std::ostream& s, const DataPoint<T>& data, const pbeast::AggregationType /*type*/)
    {
      print(s, data.value());
    }

  template<class T>
    void
    print_data_point(std::ostream& s, const DownsampledDataPoint<T>& data, const pbeast::AggregationType a_type)
    {
      if (a_type == pbeast::AggregationNone)
        {
          s << '(';
          print(s, data.min_value());
          s << ',';
          print(s, data.value());
          s << ',';
          print(s, data.max_value());
          s << ')';
        }
      else if (a_type == pbeast::AggregationAverage || a_type == pbeast::AggregationSum)
        {
          print(s, data.value());
        }
      else /*if (a_type == pbeast::AggregationMin || a_type == pbeast::AggregationMax)*/
        {
          print(s, data.min_value());
        }
    }


  template<class T>
    void
    print(std::ostream& s, const std::map<std::string, std::vector<T>>& data, boost::local_time::time_zone_ptr tz_ptr, bool print_span, bool ts2str = true, const char * prefix = "  ", const pbeast::FunctionConfig * ac = nullptr)
    {
      for (const auto & x : data)
        {
          s << x.first << ":\n";

          pbeast::AggregationType at = ac ? ac->get_type(x.first) : pbeast::AggregationNone;

          for (uint64_t i = 0; i < x.second.size();)
            {
              const T& point = x.second[i];
              const uint64_t ts1(point.ts());
              uint64_t ts2(ts1);

              while (++i < x.second.size())
                {
                  if (print_span == false || point.value() != x.second[i].value() || point.is_null() != x.second[i].is_null())
                    break;
                  ts2 = x.second[i].ts();
                }

              print_tss(s, ts1, ts2, tz_ptr, ts2str, prefix);

              s << ' ';

              if (point.is_null() == false)
                print_data_point(s, point, at);
              else
                s << "(NULL)";

              s << '\n';
            }
        }
    }

  template<class T>
    void
    print(std::ostream& s, const std::map<std::string, std::vector<T>>& data, const std::map<std::string, std::set<uint32_t>>& map_eov, const std::map<std::string, std::set<uint32_t>>& map_upd, boost::local_time::time_zone_ptr tz_ptr, bool ts2str, const char * prefix, const pbeast::FunctionConfig * fc = nullptr)
    {
      std::set<uint32_t> empty;

      // print time series data
      for (const auto & x : data)
        {
          std::map<std::string, std::set<uint32_t>>::const_iterator e = pbeast::find(map_eov, x.first);
          const std::set<uint32_t>& eov(e != map_eov.end() ? e->second : empty);

          std::map<std::string, std::set<uint32_t>>::const_iterator u = pbeast::find(map_upd, x.first);
          const std::set<uint32_t>& upd(u != map_upd.end() ? u->second : empty);

          if (fc && fc->no_aggregations() == false)
            {
              pbeast::AggregationType at = fc->get_type(x.first);
              if (at == pbeast::AggregationAverage || at == pbeast::AggregationSum)
                {
                  // print aggregated value
                  std::map<std::string, std::vector<pbeast::DataPoint<typename T::average_type::data_type>>>extracted_data;
                  std::vector<pbeast::DataPoint<typename T::average_type::data_type>>& vec = extracted_data[x.first];
                  pbeast::extract_average_value(x.second, vec);
                  pbeast::print(s, extracted_data, tz_ptr, false, ts2str, prefix);
                  continue;
                }
              else if (at == pbeast::AggregationMin || at == pbeast::AggregationMax)
                {
                  // print raw value
                  std::map<std::string, std::vector<pbeast::DataPoint<typename T::raw_type::data_type>>> extracted_data;
                  std::vector<pbeast::DataPoint<typename T::raw_type::data_type>>& vec = extracted_data[x.first];
                  pbeast::extract_raw_value(x.second, vec);
                  pbeast::print(s, extracted_data, tz_ptr, false, ts2str, prefix);
                  continue;
                }
            }
          s << x.first << ":\n";
          pbeast::print(s, x.second, eov, upd, tz_ptr, ts2str, prefix);
        }

      // print deleted objects with no data
      for (const auto& x : map_eov)
        {
          if (data.count(x.first) == 0)
            {
              print_eov_serie(s, x, tz_ptr, ts2str, prefix);
            }
        }
    }

}


#endif
