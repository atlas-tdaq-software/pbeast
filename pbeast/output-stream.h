#ifndef PBEAST_OUTPUT_STREAM_H
#define PBEAST_OUTPUT_STREAM_H

#include <stdint.h>
#include <sstream>

#include <google/protobuf/io/coded_stream.h>
#include <google/protobuf/wire_format_lite.h>
#if GOOGLE_PROTOBUF_VERSION < 3007000
#include <google/protobuf/wire_format_lite_inl.h>
#endif

namespace google
{
  namespace protobuf
  {
    namespace io
    {
      class GzipOutputStream;
      class ZeroCopyOutputStream;
    }
  }
}

namespace pbeast
{

  class Void;

  class OutputStream
  {

  public:

    OutputStream(const std::string& file);
    OutputStream(std::stringstream * stream);

    uint64_t
    close_file(bool no_throw = false);

    ~OutputStream();

    uint64_t
    check_and_reset(bool use_zip, int32_t limit = 0x1FFFFFFF);

    void
    reset_streams(bool use_zip);

    void
    check_state();

    int64_t
    tell_pos() const;

    void
    write(const std::string& v);

    inline void
    write_no_len(const std::string& v)
    {
      m_coded_output->WriteRaw(v.data(), v.size());
    }

    void
    write(const bool& v)
    {
      google::protobuf::internal::WireFormatLite::WriteBoolNoTag(v, m_coded_output);
    }

    void
    write(const int8_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteSInt32NoTag(v, m_coded_output);
    }

    void
    write(const uint8_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteUInt32NoTag(v, m_coded_output);
    }

    void
    write(const int16_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteSInt32NoTag(v, m_coded_output);
    }

    void
    write(const uint16_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteUInt32NoTag(v, m_coded_output);
    }

    void
    write(const int32_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteSInt32NoTag(v, m_coded_output);
    }

    void
    write(const uint32_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteUInt32NoTag(v, m_coded_output);
    }

    void
    write(const int64_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteSInt64NoTag(v, m_coded_output);
    }

    void
    write(const uint64_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteUInt64NoTag(v, m_coded_output);
    }

    void
    write(const float& v)
    {
      google::protobuf::internal::WireFormatLite::WriteFloatNoTag(v, m_coded_output);
    }

    void
    write(const double& v)
    {
      google::protobuf::internal::WireFormatLite::WriteDoubleNoTag(v, m_coded_output);
    }

    void
    write(const Void&)
    {
    }

    // fixed 4 / 8 bytes

    void
    write32(const uint32_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteFixed32NoTag(v, m_coded_output);
    }

    void
    write64(const uint64_t& v)
    {
      google::protobuf::internal::WireFormatLite::WriteFixed64NoTag(v, m_coded_output);
    }

  private:

    void
    create_streams(bool use_zip);
    uint64_t
    delete_streams();

    int m_fd;
    std::string m_file;
    std::stringstream * m_string_stream;

    google::protobuf::io::ZeroCopyOutputStream * m_raw_output;
    google::protobuf::io::CodedOutputStream * m_coded_output;
    google::protobuf::io::GzipOutputStream * m_zip_output;

    int64_t m_position;
  };
}

#endif
