// this is -*- c++ -*-
#ifndef OKSCONFIG_OKSCONFIGURATION_H_
#define OKSCONFIG_OKSCONFIGURATION_H_

//
// $Id$
//

#include <string>
#include <list>
#include <set>
#include <thread>

#include <config/ConfigurationImpl.h>
#include <config/Schema.h>

#include <pbeast/query.h>

  // forward declaration 
class PBeastConfigObject;

struct PBeastQueryInfo;

namespace pbeast
{
  struct AttributesInfo
  {
    std::map<std::string, daq::config::type_t> m_simple_attributes;
    std::map<std::string, daq::config::type_t> m_vector_attributes;
    std::map<std::string, daq::config::cardinality_t> m_relationships;
  };
}

class PBeastConfiguration : public ConfigurationImpl {

  friend class PBeastConfigObject;

  public:

    PBeastConfiguration(Configuration * db) noexcept;
    virtual ~PBeastConfiguration() { ; }

    typedef std::map< std::string , std::set<std::string> > SMap;

  public:

    virtual bool test_object(const std::string& class_name, const std::string& name, unsigned long rlevel, const std::vector<std::string> * rclasses);
    virtual void get(const std::string& class_name, const std::string& name, ConfigObject& object, unsigned long rlevel, const std::vector<std::string> * rclasses);
    virtual void get(const std::string& class_name, std::vector<ConfigObject>& objects, const std::string& query, unsigned long rlevel, const std::vector<std::string> * rclasses);
    virtual void get(const ConfigObject& obj_from, const std::string& query, std::vector<ConfigObject>& objects, unsigned long rlevel, const std::vector<std::string> * rclasses);
    virtual daq::config::class_t * get(const std::string& class_name, bool direct_only);
    virtual void get_superclasses(config::fmap<config::fset>& schema);

    virtual void create(const std::string& at, const std::string& class_name, const std::string& id, ConfigObject& object);
    virtual void create(const ConfigObject& at, const std::string& class_name, const std::string& id, ConfigObject& object);
    virtual void destroy(ConfigObject& object);

    virtual void open_db(const std::string& db_name);
    virtual void close_db();
    virtual bool loaded() const noexcept;
    virtual void create(const std::string& db_name, const std::list<std::string>& includes);
    virtual bool is_writable(const std::string& db_name);
    virtual void add_include(const std::string& db_name, const std::string& include);
    virtual void remove_include(const std::string& db_name, const std::string& include);
    virtual void get_includes(const std::string& db_name, std::list<std::string>& includes) const;
    virtual void get_updated_dbs(std::list<std::string>& dbs) const;
    virtual void set_commit_credentials(const std::string& user, const std::string& password);
    virtual void commit(const std::string& why);
    virtual void abort();
    virtual void prefetch_data() { ; }
    virtual void prefetch_schema() { ; }
    virtual std::vector<daq::config::Version> get_changes();
    virtual std::vector<daq::config::Version> get_versions(const std::string& since, const std::string& until, daq::config::Version::QueryType type, bool skip_irrelevant);

    virtual void subscribe(const std::set<std::string>& class_names, const SMap& objs, ConfigurationImpl::notify cb, ConfigurationImpl::pre_notify pre_cb);
    virtual void unsubscribe();

    virtual void print_profiling_info() noexcept;


  public:

    static std::string s_receiver_url;
    static std::string s_server_url;


  private:

    pbeast::ServerProxy m_web_receiver;
    pbeast::ServerProxy m_web_server;

    std::string m_partition;

    std::map<std::string, pbeast::AttributesInfo> m_attributes_info;

    std::list<std::string> m_postponed_requests_add;
    std::list<std::string> m_postponed_requests_close;

  private:

    PBeastConfigObject * new_object(const std::string& name, const std::string& id);

    void
    run_query(const PBeastQueryInfo& query, const std::string& attribute_path, const std::string& attribute_name, const std::string& type, std::map<std::string, PBeastConfigObject *>& out);

    void
    get(const PBeastQueryInfo& query, const std::string& attribute_path, const std::string& type, std::map<std::string, PBeastConfigObject *>&);

    template<class T, class V = T>
    void
    read_attribute_values(pbeast::QueryDataBase *val, const std::string &class_name, const std::string &attribute_name, std::map<std::string, PBeastConfigObject *>& objects);

    void
    check_db_name(const std::string& db_name) const;


};

#endif // OKSCONFIG_OKSCONFIGURATION_H_
