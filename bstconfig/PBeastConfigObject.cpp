#include <ers/ers.h>

#include <gason.h>

#include <config/ConfigObject.h>
#include <config/Schema.h>
#include <pbeast/series-print.h>

#include "bstconfig/PBeastConfigObject.h"
#include "bstconfig/PBeastConfiguration.h"

template<class T>
  T read_json(const std::string& json)
{
  T value{};
  std::istringstream s(json);
  s >> value;
  return value;
}

#define READ_NUM(NAME, F) \
    if (json.empty()) \
      return 0; \
 \
    try \
      { \
        return F(json); \
      } \
    catch (std::invalid_argument const &ex) \
      { \
        std::ostringstream text; \
        text << "cannot read " << NAME << " from \"" << json << "\" caused by invalid_argument: " << ex.what(); \
        throw(daq::config::Generic( ERS_HERE, text.str().c_str())); \
      } \
    catch (std::out_of_range const &ex) \
      { \
        std::ostringstream text; \
        text << "cannot read " << NAME << " from \"" << json << "\" caused by out of range: " << ex.what(); \
        throw(daq::config::Generic( ERS_HERE, text.str().c_str())); \
      }

template<>
  int32_t
  read_json<int32_t>(const std::string &json)
  {
    READ_NUM("a signed integer value", std::stol)
  }

template<>
  uint32_t
  read_json<uint32_t>(const std::string &json)
  {
    READ_NUM("an unsigned integer value", std::stoul)
  }

template<>
  int8_t
  read_json<int8_t>(const std::string &json)
  {
    return static_cast<int8_t>(read_json<int32_t>(json));
  }

template<>
  uint8_t
  read_json<uint8_t>(const std::string &json)
  {
    return static_cast<uint8_t>(read_json<uint32_t>(json));
  }

template<>
  int16_t
  read_json<int16_t>(const std::string &json)
  {
    return static_cast<int16_t>(read_json<int32_t>(json));
  }

template<>
  uint16_t
  read_json<uint16_t>(const std::string &json)
  {
    return static_cast<uint16_t>(read_json<uint32_t>(json));
  }

template<>
  int64_t
  read_json<int64_t>(const std::string &json)
  {
    READ_NUM("a signed integer value", std::stoll)
  }

template<>
  uint64_t
  read_json<uint64_t>(const std::string &json)
  {
    READ_NUM("an unsigned integer value", std::stoull)
  }

template<>
  double
  read_json<double>(const std::string &json)
  {
    READ_NUM("a floating point value", std::stold)
  }

template<>
  float
  read_json<float>(const std::string &json)
  {
    return static_cast<double>(read_json<double>(json));
  }


template<>
  std::string
  read_json<std::string>(const std::string &json)
  {
    return json.substr(1, json.size()-2);
  }


template<class T>
  daq::config::type_t type()
{
  std::ostringstream text;
  text << "unexpected c++ type " << typeid(T).name();
  throw std::runtime_error(text.str().c_str());
}

template<> daq::config::type_t type<bool>() { return daq::config::bool_type; }
template<> daq::config::type_t type<int8_t>() { return daq::config::s8_type; }
template<> daq::config::type_t type<uint8_t>() { return daq::config::u8_type; }
template<> daq::config::type_t type<int16_t>() { return daq::config::s16_type; }
template<> daq::config::type_t type<uint16_t>() { return daq::config::u16_type; }
template<> daq::config::type_t type<int32_t>() { return daq::config::s32_type; }
template<> daq::config::type_t type<uint32_t>() { return daq::config::u32_type; }
template<> daq::config::type_t type<int64_t>() { return daq::config::s64_type; }
template<> daq::config::type_t type<uint64_t>() { return daq::config::u64_type; }
template<> daq::config::type_t type<float>() { return daq::config::float_type; }
template<> daq::config::type_t type<double>() { return daq::config::double_type; }
template<> daq::config::type_t type<std::string>() { return daq::config::string_type; }
template<> daq::config::type_t type<pbeast::Time>() { return daq::config::time_type; }
template<> daq::config::type_t type<pbeast::Date>() { return daq::config::date_type; }
template<> daq::config::type_t type<pbeast::Enum>() { return daq::config::enum_type; }

static void
validate_type(daq::config::type_t expected_type, daq::config::type_t used_type, const std::string& attribute_name)
{
  if (expected_type != used_type)
    {
      std::ostringstream text;
      text << "the \"" << daq::config::attribute_t::type2str(expected_type) << "\" type of attribute \"" << attribute_name << "\" does not match to used \"" << daq::config::attribute_t::type2str(used_type) << "\" type";
      throw daq::config::Generic( ERS_HERE, text.str().c_str() );
    }
}


// helper function to extract value out of OKS database
template<class T>
  void
  PBeastConfigObject::get_value(const std::string& name, T& value)
  {
    static const std::string s_empty;

    std::lock_guard<std::mutex> scoped_lock(m_mutex);

    auto a = m_attributes_info.m_simple_attributes.find(name);

    if (a == m_attributes_info.m_simple_attributes.end())
      throw ( daq::config::NotFound( ERS_HERE, "single-value attribute", name.c_str() ) );

    auto it = m_data.find(name);

    const std::string& data(it != m_data.end() && !it->second.empty() ? it->second : s_empty);

    // try to convert enum, date and time types to string
    if constexpr (std::is_same<T,std::string>::value)
      {
        if (a->second != type<T>())
          {
            if (a->second == daq::config::enum_type)
              {
                value = data;
              }
            else if (a->second == daq::config::date_type)
              {
                m_buf.str(std::string());
                pbeast::print<pbeast::Date>(m_buf, pbeast::Date(read_json<uint32_t>(data)));
                value = m_buf.str();
              }
            else if (a->second == daq::config::time_type)
              {
                m_buf.str(std::string());
                pbeast::print<pbeast::Time>(m_buf, pbeast::Time(read_json<uint64_t>(data)));
                value = m_buf.str();
              }
            else
              {
                std::ostringstream text;
                text << "there is no known conversion of \"" << daq::config::attribute_t::type2str(a->second) << "\" type of attribute \"" << name << "\" to \"string\" type";
                throw daq::config::Generic( ERS_HERE, text.str().c_str() );
              }

            return;
          }
      }

    validate_type(a->second, type<T>(), name);

    if (!data.empty())
      value = read_json<T>(data);
    else
      value = {};
  }


template<class T>
  void
  read_vector(const std::string &data, std::vector<T> &value)
  {
    char *endptr;
    JsonValue array;
    JsonAllocator allocator;
    char *source = const_cast<char*>(data.c_str());
    int status = jsonParse(source, &endptr, &array, allocator);
    if (status != JSON_OK)
      {
        std::ostringstream text;
        text << "failed to parse json \"" << data << "\": " << jsonStrError(status) << " at " << (endptr - source);
        throw daq::config::Generic(ERS_HERE, text.str().c_str());
      }

    if (array.getTag() != JSON_ARRAY)
      {
        std::ostringstream text;
        text << "json \"" << data << "\" value us not an array";
        throw daq::config::Generic(ERS_HERE, text.str().c_str());
      }

    try
      {
        array.get<T> (value);
      }
    catch (const std::exception &ex)
      {
        std::ostringstream text;
        text << "failed to read json \"" << data << "\": " << ex.what();
        throw daq::config::Generic(ERS_HERE, text.str().c_str());
      }
  }


// helper function to extract vector of values out of OKS database
template<class T>
  void
  PBeastConfigObject::get_vector(const std::string &name, std::vector<T> &value)
  {
    static const std::string s_empty;

    std::lock_guard<std::mutex> scoped_lock(m_mutex);

    auto a = m_attributes_info.m_vector_attributes.find(name);

    if (a == m_attributes_info.m_vector_attributes.end())
      throw ( daq::config::NotFound( ERS_HERE, "multi-value attribute", name.c_str() ) );

    auto it = m_data.find(name);

    const std::string& data(it != m_data.end() && !it->second.empty() ? it->second : s_empty);

    // try to convert enum, date and time types to string
    if constexpr (std::is_same<T,std::string>::value)
      {
        if (a->second != type<T>())
          {
            if (a->second == daq::config::enum_type)
              {
                if (!data.empty())
                  {
                    std::vector<int32_t> vec;
                    read_vector(data, vec);
                    value.reserve(vec.size());

                    for (const auto& x : vec)
                      {
                        m_buf.str(std::string());
                        m_buf << x;
                        value.emplace_back(m_buf.str());
                      }
                  }
              }
              else if (a->second == daq::config::date_type)
                {
                  if (!data.empty())
                    {
                      std::vector<uint32_t> vec;
                      read_vector(data, vec);
                      value.reserve(vec.size());

                      for (const auto &x : vec)
                        {
                          m_buf.str(std::string());
                          pbeast::print<pbeast::Date>(m_buf, pbeast::Date(x));
                          value.emplace_back(m_buf.str());
                        }
                    }
                }
              else if (a->second == daq::config::time_type)
                {
                  if (!data.empty())
                    {
                      std::vector<uint64_t> vec;
                      read_vector(data, vec);
                      value.reserve(vec.size());

                      for (const auto& x : vec)
                        {
                          m_buf.str(std::string());
                          pbeast::print<pbeast::Time>(m_buf, pbeast::Time(x));
                          value.emplace_back(m_buf.str());
                        }
                    }
                }
              else
                {
                  std::ostringstream text;
                  text << "there is no known conversion of \"" << daq::config::attribute_t::type2str(a->second) << "\" type of attribute \"" << name << "\" to \"string\" type";
                  throw daq::config::Generic( ERS_HERE, text.str().c_str() );
                }

            return;
          }
      }

    validate_type(a->second, type<T>(), name);

    if (!data.empty())
      read_vector<T>(data, value);
  }



PBeastConfigObject::PBeastConfigObject(PBeastConfigObjectNames& obj, ConfigurationImpl * impl) noexcept :
    ConfigObjectImpl(impl, obj.m_object_id),
    m_attributes_info(obj.m_attributes_info),
    m_ts (0),
    m_state (Inited)
{
}

PBeastConfigObject::~PBeastConfigObject() noexcept
{
}

const std::string
PBeastConfigObject::contained_in() const
{
  return static_cast<PBeastConfiguration *>(m_impl)->m_partition;
}

void
PBeastConfigObject::get(const std::string& name, bool& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, uint8_t& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, int8_t& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, uint16_t& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, int16_t& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, uint32_t& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, int32_t& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, uint64_t& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, int64_t& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, float& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, double& value)
{
  get_value(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::string& value)
{
  get_value(name, value);
}

static void
validate_cardinality(daq::config::cardinality_t expected, bool is_mv, const std::string& relationship_name)
{
  if ((expected == daq::config::zero_or_one || expected == daq::config::only_one) && is_mv)
    {
      std::ostringstream text;
      text << "the \"" << daq::config::relationship_t::card2str(expected) << "\" cardinality of relationship \"" << relationship_name << "\" does not match used method for " << (is_mv ? "many objects" : "single object");
      throw daq::config::Generic( ERS_HERE, text.str().c_str() );
    }
}

void
PBeastConfigObject::get(const std::string& name, ConfigObject& value)
{
  std::lock_guard<std::mutex> scoped_lock(m_mutex);

  auto x = m_attributes_info.m_relationships.find(name);

  if (x == m_attributes_info.m_relationships.end())
     throw ( daq::config::NotFound( ERS_HERE, "relationship", name.c_str() ) );
  else
    validate_cardinality(x->second, false, name);

  auto it = m_relationships.find(name);

  if (it != m_relationships.end() && it->second.empty() == false)
    value = ConfigObject(it->second[0]);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<bool>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<uint8_t>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<int8_t>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<uint16_t>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<int16_t>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<uint32_t>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<int32_t>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<uint64_t>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<int64_t>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<float>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<double>& value)
{
  get_vector(name, value);
}

void
PBeastConfigObject::get(const std::string& name, std::vector<std::string>& value)
{
  get_vector(name, value);
}


void
PBeastConfigObject::get(const std::string& name, std::vector<ConfigObject>& value)
{
  std::lock_guard<std::mutex> scoped_lock(m_mutex);

  auto x = m_attributes_info.m_relationships.find(name);

  if (x == m_attributes_info.m_relationships.end())
     throw ( daq::config::NotFound( ERS_HERE, "relationship", name.c_str() ) );
  else
    validate_cardinality(x->second, true, name);

  auto it = m_relationships.find(name);

  if (it == m_relationships.end())
    return;

  for (const auto& x : it->second)
    value.push_back(ConfigObject(x));
}

bool
PBeastConfigObject::rel(const std::string& name, std::vector<ConfigObject>& value)
{
  return true;
 // TODO
}

void
PBeastConfigObject::referenced_by(std::vector<ConfigObject>& value, const std::string& rname, bool check_composite_only, unsigned long /* rlevel */, const std::vector<std::string> * /* rclasses */) const
{
  // TODO
}

   /////////////////////////////////////////////////////////////////////////////
   //
   //  set functions
   //
   /////////////////////////////////////////////////////////////////////////////


void PBeastConfigObject::update_ts()
{
  timeval tv;
  gettimeofday(&tv, nullptr);
  m_ts = pbeast::mk_ts(tv.tv_sec, tv.tv_usec);
  m_state = Updated;
}

std::timespec
PBeastConfigObject::time() const noexcept
{
  return {pbeast::ts2secs(m_ts),pbeast::ts2mksecs(m_ts)*1000};
}

template<class T, class V>
  void
  PBeastConfigObject::set_value(const std::string &name, const T &value, bool update)
  {
    std::lock_guard<std::mutex> scoped_lock(m_mutex);

    auto a = m_attributes_info.m_simple_attributes.find(name);

    if (a == m_attributes_info.m_simple_attributes.end())
      throw ( daq::config::NotFound( ERS_HERE, "single-value attribute", name.c_str() ) );
    else
      validate_type(a->second, type<V>(), name);

    m_buf.str(std::string());
    pbeast::print_json<T>(m_buf, value);
    m_data[name] = m_buf.str();

    if (update)
      update_ts();
  }

template<>
  void
  PBeastConfigObject::set_value<pbeast::Time,uint64_t>(const std::string &name, const pbeast::Time &value, bool update)
  {
    set_value<uint64_t,pbeast::Time>(name, value.v, update);
  }

template<>
  void
  PBeastConfigObject::set_value<pbeast::Date,uint32_t>(const std::string &name, const pbeast::Date &value, bool update)
  {
    set_value<uint32_t,pbeast::Date>(name, value.v, update);
  }

template<class T, class V>
  void
  PBeastConfigObject::set_vector(const std::string &name, const std::vector<T> &value, bool update)
  {
    std::lock_guard<std::mutex> scoped_lock(m_mutex);

    auto a = m_attributes_info.m_vector_attributes.find(name);

    if (a == m_attributes_info.m_vector_attributes.end())
      throw ( daq::config::NotFound( ERS_HERE, "multi-value attribute", name.c_str() ) );
    else
      validate_type(a->second, type<V>(), name);

    m_buf.str(std::string());
    pbeast::print_json<T>(m_buf, value);
    m_data[name] = m_buf.str();

    if (update)
      update_ts();
  }

template<>
  void
  PBeastConfigObject::set_vector<pbeast::Time,uint64_t>(const std::string &name, const std::vector<pbeast::Time> &value, bool update)
  {
    std::vector<uint64_t> vec;
    vec.reserve(value.size());

      {
        std::lock_guard<std::mutex> scoped_lock(m_mutex);

        for (const auto &x : value)
          vec.emplace_back(x.v);
      }

    set_vector<uint64_t,pbeast::Time>(name, vec, false);
  }

template<>
  void
  PBeastConfigObject::set_vector<pbeast::Date,uint32_t>(const std::string &name, const std::vector<pbeast::Date> &value, bool update)
  {
    std::vector<uint32_t> vec;
    vec.reserve(value.size());

      {
        std::lock_guard<std::mutex> scoped_lock(m_mutex);

        for (const auto &x : value)
          vec.emplace_back(x.v);
      }

    set_vector<uint32_t,pbeast::Time>(name, vec, false);
  }

void PBeastConfigObject::set(const std::string& name, bool value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, uint8_t value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, int8_t value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, uint16_t value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, int16_t value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, uint32_t value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, int32_t value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, uint64_t value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, int64_t value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, float value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, double value)
{
  set_value(name, value, true);
}

void PBeastConfigObject::set(const std::string& name, const std::string& value)
{
  set_value(name, value, true);
}

void
PBeastConfigObject::set_enum(const std::string& name, const std::string& value)
{
  set_value<int32_t,pbeast::Enum>(name, read_json<int32_t>(value), true);
}

void PBeastConfigObject::set_class(const std::string& name, const std::string& value)
{
  set_value(name, value, true);
}

void
PBeastConfigObject::set_date(const std::string& name, const std::string& value)
{
  set_value<uint64_t,pbeast::Date>(name, read_json<uint32_t>(value), true);
}

void
PBeastConfigObject::set_time(const std::string& name, const std::string& value)
{
  set_value<uint64_t,pbeast::Time>(name, read_json<uint64_t>(value), true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<bool>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<uint8_t>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<int8_t>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<uint16_t>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<int16_t>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<uint32_t>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<int32_t>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<uint64_t>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<int64_t>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<float>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<double>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<std::string>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set_enum(const std::string& name, const std::vector<std::string>& value)
{
  std::vector<int32_t> vec;
  vec.reserve(value.size());

  for(const auto& x : value)
    vec.push_back(read_json<int32_t>(x));

  set_vector<int32_t,pbeast::Enum>(name, vec, true);
}

void
PBeastConfigObject::set_class(const std::string& name, const std::vector<std::string>& value)
{
  set_vector(name, value, true);
}

void
PBeastConfigObject::set_date(const std::string& name, const std::vector<std::string>& value)
{
  std::vector<uint32_t> vec;
  vec.reserve(value.size());

  for(const auto& x : value)
    vec.push_back(read_json<uint32_t>(x));

  set_vector<uint32_t,pbeast::Date>(name, vec, true);
}

void
PBeastConfigObject::set_time(const std::string& name, const std::vector<std::string>& value)
{
  std::vector<uint64_t> vec;
  vec.reserve(value.size());

  for(const auto& x : value)
    vec.push_back(read_json<uint64_t>(x));

  set_vector<uint64_t,pbeast::Time>(name, vec, true);
}

void
PBeastConfigObject::set(const std::string& name, const ConfigObject * value, bool skip_non_null_check)
{
  std::lock_guard<std::mutex> scoped_lock(m_mutex);

  auto x = m_attributes_info.m_relationships.find(name);

  if (x == m_attributes_info.m_relationships.end())
     throw ( daq::config::NotFound( ERS_HERE, "relationship", name.c_str() ) );
  else
    validate_cardinality(x->second, false, name);

  auto& val = m_relationships[name];

  val.clear();

  if (value && !value->is_null())
    val.push_back(static_cast<PBeastConfigObject *>(const_cast<ConfigObjectImpl *>(value->implementation())));

  update_ts();
}

void
PBeastConfigObject::set(const std::string& name, const std::vector<const ConfigObject*>& value, bool skip_non_null_check)
{
  std::lock_guard<std::mutex> scoped_lock(m_mutex);

  auto x = m_attributes_info.m_relationships.find(name);

  if (x == m_attributes_info.m_relationships.end())
     throw ( daq::config::NotFound( ERS_HERE, "relationship", name.c_str() ) );
  else
    validate_cardinality(x->second, true, name);

  auto& val = m_relationships[name];

  val.clear();

  for (const auto& x : value)
    val.push_back(static_cast<PBeastConfigObject *>(const_cast<ConfigObjectImpl *>(x->implementation())));

  update_ts();
}

void
PBeastConfigObject::move(const std::string& at)
{
  // TODO
}

void
PBeastConfigObject::rename(const std::string& new_id)
{
  // TODO
}

void
PBeastConfigObject::reset()
{
  // TODO
}
