#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>

#include <algorithm>
#include <memory>
#include <mutex>

#include <gason.h>

#include <ers/ers.h>

#include <config/Configuration.h>
#include <config/ConfigObject.h>
#include <config/Change.h>
#include <config/Schema.h>

#include "pbeast/curl-request.h"
#include "pbeast/series-print.h"
#include "pbeast/json-serialize.h"
#include "bin/utils.h"

#include "bstconfig/PBeastConfiguration.h"
#include "bstconfig/PBeastConfigObject.h"

std::string PBeastConfiguration::s_receiver_url;
std::string PBeastConfiguration::s_server_url;


  // to be used as plug-in

extern "C" ConfigurationImpl*
_bstconfig_creator_(const std::string &spec, Configuration * db)
{
  try
    {
      static std::once_flag flag;

      std::call_once(flag, []
      ()
        {
          if (const char *val = getenv(pbeast::url_var::s_receiver))
            PBeastConfiguration::s_receiver_url = val;

          if (const char *val = getenv(pbeast::url_var::s_server))
            PBeastConfiguration::s_server_url = val;
        }
      );

      auto mk_no_var = [](const char * v)
      {
        return std::string(v) + " process environment variable is not set";
      };

      if (PBeastConfiguration::s_receiver_url.empty())
        throw daq::config::Generic( ERS_HERE, mk_no_var(pbeast::url_var::s_receiver).c_str());

      if (PBeastConfiguration::s_server_url.empty())
        throw daq::config::Generic( ERS_HERE, mk_no_var(pbeast::url_var::s_server).c_str());

      std::unique_ptr<PBeastConfiguration> impl(new PBeastConfiguration(db));

      if (!spec.empty())
        impl->open_db(spec);

      return impl.release();
    }
  catch (daq::config::Exception &ex)
    {
      throw daq::config::Generic(ERS_HERE, "bstconfig initialization error", ex);
    }
  catch (...)
    {
      throw daq::config::Generic(ERS_HERE, "bstconfig initialization error:\n***** caught unknown exception *****");
    }
}


PBeastConfiguration::PBeastConfiguration(Configuration * db) noexcept :
  ConfigurationImpl(db),
  m_web_receiver(s_receiver_url),
  m_web_server(s_server_url)
{
  ;
}

template<class T>
static void
get_attribute_param(std::size_t num, const char * key, const std::string& name, JsonValue& val, T& out)
{
  try
  {
    val.get(out);
  }
  catch (std::exception& ex)
  {
    std::ostringstream text;
    text << "cannot read value of key \"" << key << "\" of attribute  # " << num << " in class \"" << name << "\": " << ex.what();
    throw std::runtime_error(text.str().c_str());
  }
}

static daq::config::type_t
pb2conf(pbeast::DataType type)
{
  switch (type)
  {
    case pbeast::BOOL: return daq::config::bool_type;
    case pbeast::S8: return daq::config::s8_type;
    case pbeast::U8: return daq::config::u8_type;
    case pbeast::S16: return daq::config::s16_type;
    case pbeast::U16: return daq::config::u16_type;
    case pbeast::S32: return daq::config::s32_type;
    case pbeast::U32: return daq::config::u32_type;
    case pbeast::S64: return daq::config::s64_type;
    case pbeast::U64: return daq::config::u64_type;
    case pbeast::FLOAT: return daq::config::float_type;
    case pbeast::DOUBLE: return daq::config::double_type;
    case pbeast::ENUM: return daq::config::enum_type;
    case pbeast::DATE: return daq::config::date_type;
    case pbeast::TIME: return daq::config::time_type;
    default: return daq::config::string_type;
  }
}

static daq::config::int_format_t
pb2fmt(pbeast::DataType type)
{
  return (type <= pbeast::U64 ? daq::config::dec_int_format : daq::config::na_int_format);
}

void
PBeastConfiguration::open_db(const std::string &spec_params)
{
  // destroy all previous caches if any
  close_db();

  config::map<daq::config::class_t*>& schema = get_schema(false);

  try
    {
      pbeast::SimpleRequest obj(m_web_server);

      obj.request() << "/tdaq/pbeast/getSchema?partition=" << pbeast::url_encode(pbeast::curl::get_handle(), spec_params);

      obj.execute(pbeast::CurlSimpleBuffer::get);

      char *endptr;
      JsonValue value;
      JsonAllocator allocator;

      char * source = const_cast<char *>(obj.m_out.c_str());

      int status = jsonParse(source, &endptr, &value, allocator);

      if (status != JSON_OK)
        {
          std::ostringstream text;
          text << "failed to parse json: " << jsonStrError(status) << " at " << (endptr - source);
          throw daq::pbeast::BadHttpResponse(ERS_HERE, text.str());
        }

      if (value.getTag() != JSON_ARRAY)
        {
          throw daq::pbeast::BadHttpResponse(ERS_HERE, "top-level json item is not an array");
        }
      else if (value.get_array_len() == 0)
        {
          throw std::runtime_error("empty schema (possibly repository servers are not running yet)");
        }

      std::size_t class_count(1);

      for (auto i : value)
        {
          daq::config::class_t * cl = new daq::config::class_t();
          cl->p_abstract = false;

          for (auto j : i->value)
            {
              if (!strcmp(j->key, "name"))
                {
                  if (j->value.getTag() != JSON_STRING)
                    {
                      std::ostringstream text;
                      text << "the value of \"name\" key of class #" << class_count << " is not a string";
                      throw std::runtime_error(text.str().c_str());
                    }

                  cl->p_name = j->value.toString();
                }
              else if (!strcmp(j->key, "attributes"))
                {
                  if (j->value.getTag() != JSON_ARRAY)
                    {
                      std::ostringstream text;
                      text << "the value of \"attributes\" key of class #" << class_count << " (" << cl->p_name << ") is not an array";
                      throw std::runtime_error(text.str().c_str());
                    }

                  std::size_t attribute_count(1);
                  for (auto x : j->value)
                    {
                      if (x->value.getTag() != JSON_OBJECT)
                        {
                          std::ostringstream text;
                          text << "the item # " << attribute_count << " of \"attributes\" of class #" << class_count << " (" << cl->p_name << ") is not an object";
                          throw std::runtime_error(text.str().c_str());
                        }

                      std::string name, type, description;
                      bool is_array(false);

                      for (auto a : x->value)
                        {
                          if (!strcmp(a->key, "name"))
                            {
                              get_attribute_param(attribute_count, "name", cl->p_name, a->value, name);
                            }
                          else if (!strcmp(a->key, "type"))
                            {
                              get_attribute_param(attribute_count, "type", cl->p_name, a->value, type);
                            }
                          else if (!strcmp(a->key, "description"))
                            {
                              get_attribute_param(attribute_count, "description", cl->p_name, a->value, description);
                            }
                          else if (!strcmp(a->key, "is-array"))
                            {
                              get_attribute_param(attribute_count, "is-array", cl->p_name, a->value, is_array);
                            }
                        }

                      const pbeast::DataType data_type = pbeast::str2type(type);

                      if (data_type != pbeast::OBJECT)
                        const_cast< std::vector<daq::config::attribute_t>& >(cl->p_attributes).push_back(daq::config::attribute_t(name, pb2conf(data_type), "", pb2fmt(data_type), false, is_array, "", description));
                      else
                        const_cast< std::vector<daq::config::relationship_t>& >(cl->p_relationships).push_back(daq::config::relationship_t(name, type, true, is_array, true, description));
                    }

                  attribute_count++;
                }


            }

          class_count++;

          schema.emplace(cl->p_name, cl);
       }
    }
  catch (std::exception &ex)
    {
      std::ostringstream text;
      text << "cannot access P-BEAST partition \"" << spec_params << "\"\n\twas caused by: " << ex.what();
      throw daq::config::Generic( ERS_HERE, text.str().c_str());
    }

  for (const auto &c : schema)
    {
      pbeast::AttributesInfo& info = m_attributes_info[c.first];

      for (const auto &a : c.second->p_attributes)
        {
          if (a.p_is_multi_value)
            info.m_vector_attributes.emplace(a.p_name, a.p_type);
          else
            info.m_simple_attributes.emplace(a.p_name, a.p_type);
        }

      for (const auto &r : c.second->p_relationships)
        info.m_relationships.emplace(r.p_name, r.p_cardinality);
    }

  m_partition = spec_params;
}

void
PBeastConfiguration::close_db()
{
  clean(); // clean implementation cache
  clean_schema();
  m_partition.clear();
  m_attributes_info.clear();
}

bool
PBeastConfiguration::loaded() const noexcept
{
  return (!m_partition.empty());
}

void
PBeastConfiguration::create(const std::string& /*db_name*/, const std::list<std::string>& /*includes*/)
{
  throw ( daq::config::Generic( ERS_HERE, "method create(db, includes) is not supported by bstconfig" ) );
}

void
PBeastConfiguration::get_updated_dbs(std::list<std::string>& dbs) const
{
  // TODO: check updated objects
}

void
PBeastConfiguration::set_commit_credentials(const std::string& user, const std::string& password)
{
  throw ( daq::config::Generic( ERS_HERE, "method set_commit_credentials() is not supported by bstconfig" ) );
}


void
PBeastConfiguration::commit(const std::string& /*log_message*/)
{
  // try postponed requests first

  struct
  {
    std::list<std::string>* list;
    std::string action;
  } postponed_requests[] =
    {
      { &m_postponed_requests_add, "add" },
      { &m_postponed_requests_close, "close" }
    };

  unsigned int postponed_requests_couldnt_connect_count = 0;
  unsigned int postponed_requests_committed = 0;

  for (auto & it : postponed_requests)
    {
      for (auto x = it.list->cbegin(); x != it.list->end();)
        {
          try
            {
              ERS_DEBUG(1, "try postponed request " << it.action << " \"" << *x << '\"');

              pbeast::SimpleRequest obj(m_web_receiver);
              obj.request() << "/tdaq/pbeast/" << it.action;
              obj.add_post_multipart("data", x->c_str(), x->size());
              obj.execute(pbeast::CurlSimpleBuffer::get);
              postponed_requests_committed++;
            }
          catch (daq::pbeast::HttpRequestFailure& ex)
            {
              if (ex.get_code() == CURLE_COULDNT_CONNECT)
                {
                  // only report first debug message per commit method invocation
                  int debug_level = (postponed_requests_couldnt_connect_count++ == 0 ? 0 : 1);
                  ers::debug(ex, debug_level);
                  x = std::next(x);
                  continue;
                }
              else
                {
                  ers::error(daq::config::Generic( ERS_HERE, "failed to commit postponed request", ex));
                }
            }
          catch (const std::exception &ex)
            {
              std::ostringstream text;
              text << "Failed to commit postponed request\n\twas caused by: " << ex.what();
              ers::error(daq::config::Generic( ERS_HERE, text.str().c_str()));
            }

          x = it.list->erase(x);
        }
    }

  if (auto len = m_postponed_requests_add.size() + m_postponed_requests_close.size())
    ERS_LOG("Failed to commit " << len << " postponed requests");

  if (postponed_requests_committed)
    ERS_LOG("Committed " << postponed_requests_committed << " postponed requests");

  std::ostringstream buf_updated, buf_deleted;

  timeval tv;
  gettimeofday(&tv, nullptr);
  const uint64_t now = pbeast::mk_ts(tv.tv_sec, tv.tv_usec);

  for (auto &i : m_impl_objects)
    {
      uint64_t count_updated = 0;
      uint64_t count_deleted = 0;

      for (auto &j : *i.second)
        {
          PBeastConfigObject * obj = static_cast<PBeastConfigObject *>(j.second);

          std::lock_guard<std::mutex> scoped_lock(obj->m_mutex);

          if (obj->m_state == PBeastConfigObject::Updated)
            {
              if (!count_updated)
                {
                  buf_updated.str(std::string());
                  buf_updated <<
                      "[\n"
                      " {\n"
                      "  \"partition\":" << pbeast::json::serisalize(m_partition) << ",\n"
                      "  \"name\":" << pbeast::json::serisalize(*i.first) << ",\n"
                      "  \"objects\":[\n";
                }

              buf_updated <<
                  "   {\n"
                  "    \"id\":" << pbeast::json::serisalize(j.first) << ",\n"
                  "    \"ts\":" << obj->m_ts << ",\n";

              if (!obj->m_data.empty())
                {
                  for (const auto& x : obj->m_data)
                    buf_updated << "    \"" << x.first << "\":" << x.second << ",\n";

                  // remove trailing comma
                  buf_updated.seekp(-2, std::ios_base::end);
                  buf_updated << '\n';
                }

              buf_updated << "   }\n";

              count_updated++;
            }
          else if (obj->m_state == PBeastConfigObject::Deleted)
            {
              if (!count_deleted)
                {
                  buf_deleted.str(std::string());
                  buf_deleted <<
                      "{\n"
                      " \"ts\":" << now << ",\n"
                      " \"partition\":" << pbeast::json::serisalize(m_partition) << ",\n"
                      "  \"classes\":[\n"
                      "   [" << pbeast::json::serisalize(*i.first) << ", [";
                }

              buf_deleted << pbeast::json::serisalize(j.first) << ", ";

              count_deleted++;
            }
        }

      if (count_updated)
        {
          buf_updated <<
              "  ]\n"
              " }\n"
              "]";

          const std::string json = buf_updated.str();

          try
            {
              pbeast::SimpleRequest obj(m_web_receiver);

              ERS_DEBUG(1, "add\n" << json);

              obj.request() << "/tdaq/pbeast/add";
              obj.add_post_multipart("data", json.c_str(), json.size());
              obj.execute(pbeast::CurlSimpleBuffer::get);
            }
          catch (daq::pbeast::HttpRequestFailure& ex)
            {
              if (ex.get_code() == CURLE_COULDNT_CONNECT)
                {
                  m_postponed_requests_add.push_back(json);
                  ers::warning(daq::config::Generic( ERS_HERE, "pbeast service is not available", ex));
                }
              else
                {
                  throw daq::config::Generic( ERS_HERE, "failed to commit updated objects", ex);
                }
            }
          catch (const std::exception &ex)
            {
              std::ostringstream text;
              text << "Failed to commit updated objects of class \"" << i.first << "\"\n\twas caused by: " << ex.what();
              throw daq::config::Generic( ERS_HERE, text.str().c_str());
            }

          for (auto &j : *i.second)
            {
              PBeastConfigObject * obj = static_cast<PBeastConfigObject *>(j.second);
              std::lock_guard<std::mutex> scoped_lock(obj->m_mutex);
              if (obj->m_state == PBeastConfigObject::Updated)
                obj->m_state = PBeastConfigObject::Inited;
            }
        }

      if (count_deleted)
        {
          buf_deleted.seekp(-2, std::ios_base::end);
          buf_deleted <<
              "]]\n"
              " ]\n"
              "}";

          const std::string json = buf_deleted.str();

          try
            {
              pbeast::SimpleRequest obj(m_web_receiver);

              ERS_DEBUG(1, "close\n" << json);

              obj.request() << "/tdaq/pbeast/close";
              obj.add_post_multipart("data", json.c_str(), json.size());
              obj.execute(pbeast::CurlSimpleBuffer::get);
            }
          catch (daq::pbeast::HttpRequestFailure& ex)
            {
              if (ex.get_code() == CURLE_COULDNT_CONNECT)
                {
                  m_postponed_requests_close.push_back(json);
                  ers::warning(daq::config::Generic( ERS_HERE, "pbeast service is not available", ex));
                }
              else
                {
                  throw daq::config::Generic( ERS_HERE, "failed to commit deleted objects", ex);
                }
            }
          catch (const std::exception &ex)
            {
              std::ostringstream text;
              text << "Failed to commit deleted objects of class \"" << i.first << "\"\n\twas caused by: " << ex.what();
              throw daq::config::Generic( ERS_HERE, text.str().c_str());
            }

          for (config::map<ConfigObjectImpl *>::const_iterator itr = i.second->cbegin() ; itr != i.second->cend() ; )
            itr = (static_cast<PBeastConfigObject*>(itr->second)->m_state == PBeastConfigObject::Deleted ? i.second->erase(itr) : std::next(itr));
        }
    }
}

void
PBeastConfiguration::abort()
{
  m_impl_objects.clear();
}


std::vector<daq::config::Version>
PBeastConfiguration::get_changes()
{
  std::vector<daq::config::Version> empty;
  return empty;
}


std::vector<daq::config::Version>
PBeastConfiguration::get_versions(const std::string& since, const std::string& until, daq::config::Version::QueryType type, bool skip_irrelevant)
{
  std::vector<daq::config::Version> empty;
  return empty;
}


bool
PBeastConfiguration::test_object(const std::string& class_name, const std::string& name, unsigned long rlevel, const std::vector<std::string> * rclasses)
{
  auto it = m_impl_objects.find(&class_name);

  if (it != m_impl_objects.end())
   {
      auto it2 = it->second->find(name);

      if (it2 != it->second->end())
        return true;
    }

  std::vector<ConfigObject> objects;
  get(class_name, objects, name, rlevel, rclasses);

  return (objects.size() == 1);
}

void
PBeastConfiguration::get(const std::string &class_name, const std::string &name, ConfigObject &object, unsigned long rlevel, const std::vector<std::string>* rclasses)
{
  std::vector<ConfigObject> objects;
  get(class_name, objects, name, rlevel, rclasses);

  if (objects.size() == 1)
    object = objects[0];
  else
    {
      const std::string id(name + '@' + class_name);
      throw daq::config::NotFound(ERS_HERE, "object", id.c_str());
    }
}


template<class T, class V>
  void
  PBeastConfiguration::read_attribute_values(pbeast::QueryDataBase *val, const std::string &class_name, const std::string &attribute_name, std::map<std::string, PBeastConfigObject *>& objects)
  {
    if (val->is_array() == false)
      {
        const auto &data = static_cast<pbeast::QueryData<pbeast::DataPoint<T>>*>(val)->get_data();

        for (const auto &x : data)
          {
            if (!x.second.back().is_null())
              {
                PBeastConfigObject *&o(objects[x.first]);

                if (o == nullptr)
                  {
                    o = new_object(class_name, x.first);
                    o->m_ts = x.second.back().ts();
                  }

                o->set_value<T,V>(attribute_name, x.second.back().value(), false);
                o->m_state = PBeastConfigObject::Inited;
              }
          }
      }
    else
      {
        const auto &data = static_cast<pbeast::QueryData<pbeast::DataPoint<std::vector<T>>>*>(val)->get_data();

        for (const auto &x : data)
          {
            if (!x.second.back().is_null())
              {
                PBeastConfigObject *&o(objects[x.first]);

                if (o == nullptr)
                  {
                    o = new_object(class_name, x.first);
                    o->m_ts = x.second.back().ts();
                  }

                o->set_vector<T,V>(attribute_name, x.second.back().value(), false);
                o->m_state = PBeastConfigObject::Inited;
              }
          }
      }
  }


struct PBeastQueryInfo
{
  const std::string& m_class_name;
  const std::string& m_query;
  uint64_t m_since;
  uint64_t m_until;
  static uint64_t m_prev_interval;

  static void
  set(uint64_t& param, const char * name, uint64_t value)
  {
    if (const char* env = getenv(name))
      {
        try
          {
            value = pbeast::utils::str2size(env, name);
          }
        catch (std::exception& ex)
          {
            ers::error(daq::config::Generic(ERS_HERE, ex.what()));
          }
      }

      param = pbeast::mk_ts(value);
  }

  PBeastQueryInfo(const std::string& class_name, const std::string& query) :
    m_class_name(class_name),
    m_query(query)
  {
    timeval tv;
    gettimeofday(&tv, nullptr);

    set(m_until, "TDAQ_PBEAST_QUERY_UNTIL", tv.tv_sec);
    set(m_since, "TDAQ_PBEAST_QUERY_SINCE", tv.tv_sec-60);

    static std::once_flag flag;
    std::call_once(flag, []
    ()
      {
        set(m_prev_interval, "TDAQ_PBEAST_QUERY_PREV_INTERVAL", 3600 * 24);
      }
    );

    ERS_DEBUG(1, "query [" << m_since << ',' << m_until << "] with prev interval = " << m_prev_interval);
  }
};

uint64_t PBeastQueryInfo::m_prev_interval = 0;

void
PBeastConfiguration::run_query(const PBeastQueryInfo& query, const std::string& attribute_path, const std::string& attribute_name, const std::string& type, std::map<std::string, PBeastConfigObject *>& out)
{
  std::string path(attribute_path + attribute_name);

  try
    {
      std::vector<std::shared_ptr<pbeast::QueryDataBase>> result = m_web_server.get_data(m_partition, query.m_class_name, path, query.m_query, query.m_query.empty(), query.m_since, query.m_until, query.m_prev_interval, 0, true);

      ERS_DEBUG(1, "query \"" << query.m_query << "\" on " << path << '@' << query.m_class_name << " returns " << result.size() << " results");

      if (!result.empty())
        {
          pbeast::QueryDataBase *val = result[0].get();

          switch (val->type())
            {
              case pbeast::BOOL:
                read_attribute_values<bool>(val, type, attribute_name, out); break;
              case pbeast::S8:
                read_attribute_values<int8_t>(val, type, attribute_name, out); break;
              case pbeast::U8:
                read_attribute_values<uint8_t>(val, type, attribute_name, out); break;
              case pbeast::S16:
                read_attribute_values<int16_t>(val, type, attribute_name, out); break;
              case pbeast::U16:
                read_attribute_values<uint16_t>(val, type, attribute_name, out); break;
              case pbeast::S32:
                read_attribute_values<int32_t>(val, type, attribute_name, out); break;
              case pbeast::U32:
                read_attribute_values<uint32_t>(val, type, attribute_name, out); break;
              case pbeast::S64:
                read_attribute_values<int64_t>(val, type, attribute_name, out); break;
              case pbeast::U64:
                read_attribute_values<uint64_t>(val, type, attribute_name, out); break;
              case pbeast::FLOAT:
                read_attribute_values<float>(val, type, attribute_name, out); break;
              case pbeast::DOUBLE:
                read_attribute_values<double>(val, type, attribute_name, out); break;
              case pbeast::DATE:
                read_attribute_values<uint32_t,pbeast::Date>(val, type, attribute_name, out); break;
              case pbeast::TIME:
                read_attribute_values<uint64_t,pbeast::Time>(val, type, attribute_name, out); break;
              case pbeast::ENUM:
                read_attribute_values<int32_t,pbeast::Enum>(val, type, attribute_name, out); break;
              case pbeast::STRING:
                read_attribute_values<std::string>(val, type, attribute_name, out); break;
              default:
                {
                  std::ostringstream text;
                  text << "data type " << val->type() << " is not expected for attribute \"" << attribute_name << '@' << type << '\"';
                  throw std::runtime_error(text.str().c_str());

                }
            }
        }
    }
  catch (std::exception& ex)
    {
      std::ostringstream text;
      text << "cannot query attribute \"" << path << '@' << query.m_class_name << "\": " << ex.what();
      throw std::runtime_error(text.str().c_str());
    }
}

std::string
build_path(const std::string& path, const std::string& name)
{
  if (path.empty())
    return name;
  else
    return (path + '/' + name);
}

void
PBeastConfiguration::get(const PBeastQueryInfo& info, const std::string& attribute_path, const std::string& type, std::map<std::string, PBeastConfigObject *>& objects)
{
  config::map<daq::config::class_t*>& schema = get_schema(false);

  auto it = schema.find(type);

  if (it == schema.end())
    throw ( daq::config::NotFound( ERS_HERE, "class", type.c_str() ) );

  for (const auto &a : it->second->p_attributes)
    {
      run_query(info, attribute_path, a.p_name, type, objects);
    }

  for (const auto &x : it->second->p_relationships)
    {
      std::map<std::string, PBeastConfigObject *> nested;

      auto it2 = schema.find(x.p_type);

      if (it2 == schema.end())
        throw(daq::config::NotFound( ERS_HERE, "class", x.p_type.c_str()));

      auto it3 = m_impl_objects.find(&x.p_name);

      if (it3 != m_impl_objects.end())
        for(auto& o : *it3->second)
          nested[o.first] = static_cast<PBeastConfigObject *>(o.second);

      get(info, attribute_path + x.p_name + '/' + x.p_type + '/', it2->second->p_name, nested);

      for (const auto& x2 : nested)
        {
          std::string o = x2.first.substr(0, x2.first.rfind('/'));

          PBeastConfigObject *&p(objects[o]);

          if (p == nullptr)
            {
              ERS_DEBUG(1, "create an intermediate object \"" << o << "\" in class \"" << type << "\" ");
              p = new_object(type, o);
              p->m_ts = x2.second->m_ts;
            }

          p->m_relationships[x.p_name].push_back(x2.second);

          ERS_DEBUG(1, "link nested object \"" << x2.first << "\" to " << x.p_name << " of " << o );
        }
    }
}


void
PBeastConfiguration::get(const std::string& class_name, std::vector<ConfigObject>& objects, const std::string& query, unsigned long /*rlevel*/, const std::vector<std::string> * /* rclasses */)
{
  config::map<daq::config::class_t*>& schema = get_schema(false);

  auto it = schema.find(class_name);

  if (it == schema.end())
    throw ( daq::config::NotFound( ERS_HERE, "class", class_name.c_str() ) );

  PBeastQueryInfo info(class_name, query);

  std::map<std::string, PBeastConfigObject *> out;

  try
    {
      get(info, "", class_name, out);
    }
  catch (const std::exception &ex)
    {
      std::ostringstream text;
      text << "cannot get objects of class \"" << class_name << "\"\n\twas caused by: " << ex.what();
      throw ( daq::config::Generic( ERS_HERE, text.str().c_str() ) );
    }

  objects.reserve(out.size());

  for (const auto& x : out)
    objects.emplace_back(x.second);
}

void
PBeastConfiguration::get(const ConfigObject& obj_from, const std::string& query, std::vector<ConfigObject>& objects, unsigned long /*rlevel*/, const std::vector<std::string> * /* rclasses */)
{
  throw ( daq::config::Generic( ERS_HERE, "path query method get(obj, query) is not supported by bstconfig" ) );
}


daq::config::class_t *
PBeastConfiguration::get(const std::string& class_name, bool direct_only)
{
  config::map<daq::config::class_t*>& schema = get_schema(false);

  auto it = schema.find(class_name);

  if (it != schema.end())
    return new daq::config::class_t(*it->second);
  else
    throw daq::config::NotFound(ERS_HERE, "class", class_name.c_str());
    //return nullptr;
}

void
PBeastConfiguration::get_superclasses(config::fmap<config::fset>& schema)
{
  schema.clear();

  for(const auto& x : get_schema(false))
    {
      const std::string* name = &DalFactory::instance().get_known_class_name_ref(x.first);
      schema[name] = config::fset();
    }
}


PBeastConfigObject *
PBeastConfiguration::new_object(const std::string& name, const std::string& id)
{
  PBeastConfigObjectNames info(m_attributes_info[name], id);
  return insert_object<PBeastConfigObject>(info, id, name);
}

void
PBeastConfiguration::create(const std::string& at, const std::string& class_name, const std::string& id, ::ConfigObject& object)
{
  if (!at.empty())
    check_db_name(at);

  object = new_object(class_name, id);
}

void
PBeastConfiguration::create(const ConfigObject& at, const std::string& class_name, const std::string& id, ConfigObject& object)
{
  create(m_partition, class_name, id, object);
}

void
PBeastConfiguration::destroy(::ConfigObject& obj)
{
  static_cast<PBeastConfigObject *>(const_cast<ConfigObjectImpl *>(obj.implementation()))->m_state = PBeastConfigObject::Deleted;
  // TODO
}

bool
PBeastConfiguration::is_writable(const std::string& db_name)
{
  check_db_name(db_name);

  // TODO
  return true;
}

void
PBeastConfiguration::add_include(const std::string& db_name, const std::string& include)
{
  throw ( daq::config::Generic( ERS_HERE, "method add_include() is not supported by bstconfig" ) );
}

void
PBeastConfiguration::remove_include(const std::string& db_name, const std::string& include)
{
  throw ( daq::config::Generic( ERS_HERE, "method remove_include() is not supported by bstconfig" ) );
}


void
PBeastConfiguration::get_includes(const std::string& db_name, std::list<std::string>& /*includes*/) const
{
  check_db_name(db_name);
}

void
PBeastConfiguration::subscribe(const std::set<std::string>& class_names, const SMap& objs, ConfigurationImpl::notify cb, ConfigurationImpl::pre_notify pre_cb)
{
  // TODO
  throw ( daq::config::Generic( ERS_HERE, "method subscribe() is not yet supported by bstconfig" ) );
}

void
PBeastConfiguration::unsubscribe()
{
  // TODO
}


void
PBeastConfiguration::print_profiling_info() noexcept
{
}

void
PBeastConfiguration::check_db_name(const std::string& db_name) const
{
  if (db_name != m_partition)
    {
      std::ostringstream text;
      text << "the database name \"" << db_name << "\" is different from expected partition \"" << m_partition << "\"";
      throw ( daq::config::Generic( ERS_HERE, text.str().c_str() ) );
    }
}
