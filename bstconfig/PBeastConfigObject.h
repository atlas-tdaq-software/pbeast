// this is -*- c++ -*-
#ifndef BSTCONFIG_PBEASTCONFIGOBJECT_H_
#define BSTCONFIG_PBEASTCONFIGOBJECT_H_

#include <string>
#include <vector>

#include "config/ConfigObjectImpl.h"

class ConfigurationImpl;
class PBeastConfiguration;

namespace daq {
  namespace config {
    struct class_t;
  }
}

namespace pbeast {
  struct AttributesInfo;
}

struct PBeastConfigObjectNames
{
  const pbeast::AttributesInfo& m_attributes_info;
  const std::string& m_object_id;

  PBeastConfigObjectNames(const pbeast::AttributesInfo& attributes_info, const std::string& object_id) :
    m_attributes_info(attributes_info), m_object_id(object_id)
  {
    ;
  }
};

class PBeastConfigObject :  public ConfigObjectImpl {

  friend class ConfigurationImpl;
  friend class PBeastConfiguration;

  enum State {
    Inited,
    Updated,
    Deleted
  };

  public:

    PBeastConfigObject(PBeastConfigObjectNames& obj, ConfigurationImpl * impl) noexcept;
    virtual ~PBeastConfigObject() noexcept;


  public:

    virtual const std::string contained_in() const;

    virtual std::timespec time() const noexcept;

    virtual void get(const std::string& name, bool&           value);
    virtual void get(const std::string& name, uint8_t&        value);
    virtual void get(const std::string& name, int8_t&         value);
    virtual void get(const std::string& name, uint16_t&       value);
    virtual void get(const std::string& name, int16_t&        value);
    virtual void get(const std::string& name, uint32_t&       value);
    virtual void get(const std::string& name, int32_t&        value);
    virtual void get(const std::string& name, uint64_t&       value);
    virtual void get(const std::string& name, int64_t&        value);
    virtual void get(const std::string& name, float&          value);
    virtual void get(const std::string& name, double&         value);
    virtual void get(const std::string& name, std::string&    value);
    virtual void get(const std::string& name, ConfigObject&   value);

    virtual void get(const std::string& name, std::vector<bool>&           value);
    virtual void get(const std::string& name, std::vector<uint8_t>&        value);
    virtual void get(const std::string& name, std::vector<int8_t>&         value);
    virtual void get(const std::string& name, std::vector<uint16_t>&       value);
    virtual void get(const std::string& name, std::vector<int16_t>&        value);
    virtual void get(const std::string& name, std::vector<uint32_t>&       value);
    virtual void get(const std::string& name, std::vector<int32_t>&        value);
    virtual void get(const std::string& name, std::vector<uint64_t>&       value);
    virtual void get(const std::string& name, std::vector<int64_t>&        value);
    virtual void get(const std::string& name, std::vector<float>&          value);
    virtual void get(const std::string& name, std::vector<double>&         value);
    virtual void get(const std::string& name, std::vector<std::string>&    value);
    virtual void get(const std::string& name, std::vector<ConfigObject>&   value);

    virtual bool rel(const std::string& name, std::vector<ConfigObject>& value);
    virtual void referenced_by(std::vector<ConfigObject>& value, const std::string& association, bool check_composite_only, unsigned long rlevel, const std::vector<std::string> * rclasses) const;


    virtual void set(const std::string& name, bool                value);
    virtual void set(const std::string& name, uint8_t             value);
    virtual void set(const std::string& name, int8_t              value);
    virtual void set(const std::string& name, uint16_t            value);
    virtual void set(const std::string& name, int16_t             value);
    virtual void set(const std::string& name, uint32_t            value);
    virtual void set(const std::string& name, int32_t             value);
    virtual void set(const std::string& name, uint64_t            value);
    virtual void set(const std::string& name, int64_t             value);
    virtual void set(const std::string& name, float               value);
    virtual void set(const std::string& name, double              value);
    virtual void set(const std::string& name, const std::string&  value);

    virtual void set_enum(const std::string& attribute, const std::string& value);
    virtual void set_date(const std::string& attribute, const std::string& value);
    virtual void set_time(const std::string& attribute, const std::string& value);

    virtual void set_class(const std::string& attribute, const std::string& value);

    virtual void set(const std::string& name, const std::vector<bool>&           value);
    virtual void set(const std::string& name, const std::vector<uint8_t>&        value);
    virtual void set(const std::string& name, const std::vector<int8_t>&         value);
    virtual void set(const std::string& name, const std::vector<uint16_t>&       value);
    virtual void set(const std::string& name, const std::vector<int16_t>&        value);
    virtual void set(const std::string& name, const std::vector<uint32_t>&       value);
    virtual void set(const std::string& name, const std::vector<int32_t>&        value);
    virtual void set(const std::string& name, const std::vector<uint64_t>&       value);
    virtual void set(const std::string& name, const std::vector<int64_t>&        value);
    virtual void set(const std::string& name, const std::vector<float>&          value);
    virtual void set(const std::string& name, const std::vector<double>&         value);
    virtual void set(const std::string& name, const std::vector<std::string>&    value);

    virtual void set_enum(const std::string& attribute, const std::vector<std::string>& value);
    virtual void set_date(const std::string& attribute, const std::vector<std::string>& value);
    virtual void set_time(const std::string& attribute, const std::vector<std::string>& value);

    virtual void set_class(const std::string& attribute, const std::vector<std::string>& value);

    virtual void set(const std::string& name, const ConfigObject*                     value, bool skip_non_null_check);
    virtual void set(const std::string& name, const std::vector<const ConfigObject*>& value, bool skip_non_null_check);

    virtual void move(const std::string& at);
    virtual void rename(const std::string& new_id);

    virtual void reset();

    // helper functions

  private:

    template<class T> void get_value(const std::string& name, T& value);
    template<class T> void get_vector(const std::string& name, std::vector<T>& value);

    template<class T, class V = T> void set_value(const std::string& name, const T& value, bool update);
    template<class T, class V = T> void set_vector(const std::string& name, const std::vector<T>& value, bool update);

    void update_ts();

    // required by template method insert_object
    void set(const PBeastConfigObjectNames&)
      {
        ;
      }

  private:

    const pbeast::AttributesInfo& m_attributes_info;
    std::map<std::string, std::string> m_data;
    std::map<std::string, std::vector<PBeastConfigObject *>> m_relationships;
    std::ostringstream m_buf;
    uint64_t m_ts;
    State m_state;
};

#endif // BSTCONFIG_PBEASTCONFIGOBJECT_H_
