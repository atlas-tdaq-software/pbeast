# Persistent Back-End for the Atlas Information System of TDAQ

The P-BEAST is a software service for archiving operational monitoring information.

### Developers Documentation

* JIRA project [ADAMATLAS](https://its.cern.ch/jira/projects/ADAMATLAS/summary)
* P-BEAST Service TWiki [DaqHltPBeast](https://twiki.cern.ch/twiki/bin/view/Atlas/DaqHltPBeast)
* P-BEAST Grafana Dashboards TWiki [PBeastDashboard](https://twiki.cern.ch/twiki/bin/view/Atlas/PBeastDashboard)

### Dashboards

Grafana [Point-1](https://atlasop.cern.ch/tdaq/pbeastDashboard/?orgId=1)
Grafana [Test Bed](https://vm-atlas-tdaq-cc.cern.ch/tbed/pbeast/grafana/)
